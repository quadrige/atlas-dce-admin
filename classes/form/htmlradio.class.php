<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief classe d'affichage d'un composant de saisie radio
 *        g�n�re : <input type=radio>
 */
class HtmlRadio extends HtmlCtrlBase
{
  /** Valeur d'un groupe de radio bouton */
  var $valueGroup;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode      Mode du controle de saisie : =0 modif, =1 lecture
   * @param name       Nom du controle de saisie
   * @param value      Valeur par d�faut du controle de saisie
   * @param valueGroup Valeur du groupe de bouton radio
   * @param label      Etiquette texte associ�e au controle de saisie
   */
  function HtmlRadio($iMode, $name, $value=0, $valueGroup=0, $label="")
  {
    parent::HtmlCtrlBase($iMode, $name, $value, $label);
    $this->valueGroup = $valueGroup;
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetCtrlHtml()
  {
    // contruit le controle
    $strHtml = "";
    if( $this->iMode == "1" ) {
      // mode lecture
      $strHtml = "<span class=\"".$this->cssCtrl."\">".
        ( $this->value == $this->valueGroup ? "X" : "")."</span>";
    } else {
      // mode modif
      // input type=radio
      $strHtml = "<input type=\"radio\" ".
        "name=\"".$this->name."\" ".
        "value=\"".$this->value."\"".
        ( $this->value == $this->valueGroup ? " checked" : "").
        $this->GetHtmlEvent().
        ">";
    }
    
    return $strHtml;
  }
}

?>