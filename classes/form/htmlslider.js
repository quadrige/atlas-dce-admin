var MWJ_slider_controls = 0;

function getRefToDivNest( divID, oDoc ) {
  //get a reference to the slider control, even through nested layers
  if( !oDoc ) { oDoc = document; }
  if( document.layers ) {
    if( oDoc.layers[divID] ) { return oDoc.layers[divID]; } else {
      for( var x = 0, y; !y && x < oDoc.layers.length; x++ ) {
        y = getRefToDivNest(divID, oDoc.layers[x].document); }
      return y; } }
  if( document.getElementById ) { return document.getElementById(divID); }
  if( document.all ) { return document.all[divID]; }
  return document[divID];
}

function sliderMousePos(e, bPageCoord) {
  //get the position of the mouse
  if( !e ) { e = window.event; } 
  if ( !bPageCoord ){
    if( !e || ( typeof( e.layerX ) != 'number' && typeof( e.offsetX ) != 'number' ) ) { return [0, 0]; }
    if( typeof( e.offsetX ) == 'number' ) { var xcoord = e.offsetX; var ycoord = e.offsetX; } 
    else { var xcoord = e.layerX; var ycoord = e.layerY; }
  }
  else {
    if( !e ) { e = window.event; } if( !e || ( typeof( e.pageX ) != 'number' && typeof( e.clientX ) != 'number' ) ) { return [0, 0]; }
    if( typeof( e.pageX ) == 'number' ) { var xcoord = e.pageX; var ycoord = e.pageY; } 
    else {
      var xcoord = e.clientX; var ycoord = e.clientY;
      if( !( ( window.navigator.userAgent.indexOf( 'Opera' ) + 1 ) || ( window.ScriptEngine && ScriptEngine().indexOf( 'InScript' ) + 1 ) || window.navigator.vendor == 'KDE' ) ) {
        if( document.documentElement && ( document.documentElement.scrollTop || document.documentElement.scrollLeft ) ) {
          xcoord += document.documentElement.scrollLeft; ycoord += document.documentElement.scrollTop;
        } else if( document.body && ( document.body.scrollTop || document.body.scrollLeft ) ) {
          xcoord += document.body.scrollLeft; ycoord += document.body.scrollTop; 
        } 
      } 
    }
  }
  return [xcoord, ycoord];
}

function slideIsDown(e) {
  //make note of starting positions and detect mouse movements
  if ( this.isCursor ){
    this.msStartCoord = sliderMousePos(e, this.isCursor); 
    this.lyStartCoord = this.style?[parseInt(this.style.left.replace("px", "")), parseInt(this.style.top.replace("px", ""))]:[parseInt(this.left.replace("px", "")), parseInt(this.top.replace("px", ""))];
    this.onmousemove = slideIsMove; this.onmouseup = slideIsMove; 
  }
  else {
    this.slideIsMove = slideIsMove;
    this.slideIsMove(e);
  }
  return false;
}

function slideIsMove(e) {
  //move the slider to its newest position
  var msMvCo = sliderMousePos(e, this.isCursor); 
  if( !e ) { e = window.event ? window.event : ( new Object() ); }
  var theLayer = this.style ? this.style : this;
  if( this.hor ) {
    var theNewPos;
    if ( this.isCursor ){
      theNewPos = this.lyStartCoord[0] + ( msMvCo[0] - this.msStartCoord[0] ); 
    }
    else {
      theNewPos = msMvCo[0];
    }
    if( theNewPos < 0 ) { theNewPos = 0; } 
    if( theNewPos > this.maxLength ) { theNewPos = this.maxLength; }
    if ( this.isCursor ) theLayer.left = theNewPos+"px";
  } else {
    var theNewPos;
    if ( this.isCursor ){
      theNewPos = this.lyStartCoord[1] + ( msMvCo[1] - this.msStartCoord[1] ); 
    }
    else {
      theNewPos = msMvCo[1];
    }
    if( theNewPos < 0 ) { theNewPos = 0; } if( theNewPos > this.maxLength ) { theNewPos = this.maxLength; }
    if ( this.isCursor ) theLayer.top = theNewPos+"px";
  }
  
  //run the user's functions and reset the mouse monitoring as before
  if( e.type && e.type.toLowerCase() == 'mousemove' ) {
    if( this.moveFunc ) { this.moveFunc(this, theNewPos/this.maxLength); }
  } else {
    if( this.stopFunc ) { 
      this.stopFunc(this, theNewPos/this.maxLength); 
      if ( this.oCursor && this.oSlider )
        this.oSlider.setPosition(theNewPos/this.maxLength);
    }
    this.onmousemove = null; this.onmouseup = null; 
  }
  this.onmouseout = function()
  {
    this.onmousemove = null; this.onmouseup = null; 
  }
}

function setSliderPosition(oPortion) {
  //set the slider's position
  if( isNaN( oPortion ) || oPortion < 0 ) { oPortion = 0; } if( oPortion > 1 ) { oPortion = 1; }
  var theDiv = getRefToDivNest(this.id); if( theDiv.style ) { theDiv = theDiv.style; }
  oPortion = Math.round( oPortion * this.maxLength );
  if( this.align ) { theDiv.left = oPortion+"px"; } else { theDiv.top = oPortion+"px"; }
}



function slider(idContainer, oThght, oTwdth, oTcol, oTBthk, oTBcol, oTRthk, oTRcol, oBhght, oBwdth, oBcol, oBthk, oBtxt, oAlgn, oMf, oSf) {
  //draw the slider using huge amounts of nested layers (makes the borders look normal in as many browsers as possible)
  var strHtml = "";
  if( document.layers ) {
    strHtml += 
      '<ilayer left="0" top="0" height="'+(oThght+(2*oTBthk))+'" width="'+(oTwdth+(2*oTBthk))+'" bgcolor="'+oTBcol+'">'+
      '<ilayer left="'+oTBthk+'" top="'+oTBthk+'" height="'+oThght+'" width="'+oTwdth+'" bgcolor="'+oTcol+'">'+
      '<layer left="'+(oAlgn?0:Math.floor((oTwdth-oTRthk)/2))+'" top="'+(oAlgn?Math.floor((oThght-oTRthk)/2):0)+'" height="'+(oAlgn?oTRthk:oThght)+'" width="'+(oAlgn?oTwdth:oTRthk)+'" bgcolor="'+oTRcol+'"></layer>'+
      '<layer left="'+(oAlgn?0:Math.floor((oTwdth-(oBwdth+(2*oBthk)))/2))+'" top="'+(oAlgn?Math.floor((oThght-(oBhght+(2*oBthk)))/2):0)+'" height="'+(oBhght+(2*oBthk))+'" width="'+(oBwdth+(2*oBthk))+'" bgcolor="#000000" onmouseover="this.captureEvents(Event.MOUSEDOWN);this.hor='+oAlgn+';this.maxLength='+((oAlgn?oTwdth:oThght)-((oAlgn?oBwdth:oBhght)+(2*oBthk)))+';this.moveFunc='+oMf+';this.stopFunc='+oSf+';this.onmousedown=slideIsDown;" name="MWJ_slider_controls'+MWJ_slider_controls+'">'+
      '<layer left="0" top="0" height="'+(oBhght+oBthk)+'" width="'+(oBwdth+oBthk)+'" bgcolor="#ffffff"></layer>'+
      '<layer left="'+oBthk+'" top="'+oBthk+'" height="'+oBhght+'" width="'+oBwdth+'" bgcolor="'+oBcol+'">'+
      oBtxt+'</layer></layer></ilayer></ilayer>'
    ;
  } else {
    strHtml += 
      '<div style="cursor:pointer;position:relative;left:0px;top:0px;height:'+(oThght+(2*oTBthk))+'px;width:'+(oTwdth+(2*oTBthk))+'px;background-color:'+oTBcol+';font-size:0px;">'+
        '<div style="float:left;left:'+oTBthk+'px;top:'+oTBthk+'px;height:'+oThght+'px;width:'+oTwdth+'px;background-color:'+oTcol+';font-size:0px;">'+
          '<div id="MWJ_slider_controls'+MWJ_slider_controls+'_bar"' +
            ' onmouseover="this.isCursor=false;this.hor='+oAlgn+';this.maxLength='+((oAlgn?oTwdth:oThght)-((oAlgn?oBwdth:oBhght)+(2*oBthk)))+';this.stopFunc='+oSf+';this.onmousedown=slideIsDown;"' +
            ' style="position:relative;height:'+(oThght+4)+'px;">' +
            '<div onmouseover="this.onmousedown=function (e){this.parentNode.onmousedown(e);};"' +
            ' style="height:1px;border:none;' +
            '  position:absolute;left:'+(oAlgn?0:Math.floor((oTwdth-oTRthk)/2))+'px;'+
            ' top:'+(oAlgn?Math.floor((oThght-oTRthk))/2:0)+'px;height:'+oTRthk+'px;width:'+(oAlgn?oTwdth:oTRthk)+'px;'+
            ' background-color:'+oTRcol+';font-size:0px;"'+
            '></div>' +
          '</div>'+
          '<div style="position:absolute;left:'+(oAlgn?0:Math.floor((oTwdth-(oBwdth+(2*oBthk)))/2))+'px;top:'+(oAlgn?Math.floor((oThght-(oBhght+(2*oBthk)))/2):0)+'px;height:'+(oBhght+(2*oBthk))+'px;width:'+(oBwdth+(2*oBthk))+'px;font-size:0px;"' +
          ' ondragstart="return false;" onselectstart="return false;"' +
          ' onmouseover="this.isCursor=true;this.hor='+oAlgn+';this.maxLength='+((oAlgn?oTwdth:oThght)-((oAlgn?oBwdth:oBhght)+(2*oBthk)))+';this.moveFunc='+oMf+';this.stopFunc='+oSf+';this.onmousedown=slideIsDown;"' +
          ' id="MWJ_slider_controls'+MWJ_slider_controls+'">'+
            '<div style="border-top:'+oBthk+'px solid #ffffff;border-left:'+oBthk+'px solid #ffffff;border-right:'+oBthk+'px solid #000000;border-bottom:'+oBthk+'px solid #000000;">'+
              '<div style="height:'+oBhght+'px;width:'+oBwdth+'px;font-size:0px;background-color:'+oBcol+';">'+
                '<span style="width:100%;text-align:center;">'+oBtxt+'</span>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>'
    ;
  }
  if ( document.getElementById && document.getElementById(idContainer) ){
    document.getElementById(idContainer).innerHTML = strHtml;
  }
  else {
    document.write(strHtml);
  }
  this.idContainer = idContainer;
  this.id = 'MWJ_slider_controls'+MWJ_slider_controls; 
  this.maxLength = (oAlgn?oTwdth:oThght)-((oAlgn?oBwdth:oBhght)+(2*oBthk));
  this.align = oAlgn; 
  this.setPosition = setSliderPosition; 
  MWJ_slider_controls++;
  
  this.setProperties = function(idSlider, strFctIncrement, iDepart, iFin)
  {
    this.idSlider = idSlider; 
    this.strFctIncrement = strFctIncrement; 
    this.iDepart = iDepart; 
    this.iFin = iFin;
  }
  
  if ( document.getElementById(this.id) ){
    document.getElementById(this.id).oSlider = this;
    if ( document.getElementById(this.id+"_bar") ){
      document.getElementById(this.id+"_bar").oSlider = this;
      document.getElementById(this.id+"_bar").oCursor = document.getElementById(this.id);
    }
  }
}


///////////////////////////////////////// Fonctions utilisateur ////////////////////////////////

var tabSliders = new Array() ;
function MoveSlider(objSlider, iPosition)
{
  StopSlider(objSlider, iPosition)
}

function StopSlider(objSlider, iPosition)
{
  //on force la valeur � maximum 2 chiffres derri�re la virgule      
  if ( !objSlider.oSlider ) return;
  var iValeur = eval(objSlider.oSlider.strFctIncrement+'(iPosition, '+objSlider.oSlider.iDepart+', '+objSlider.oSlider.iFin+')') ;

  if ( document.getElementById && document.getElementById(objSlider.oSlider.idSlider) ){
    var oCtrl = document.getElementById(objSlider.oSlider.idSlider);
    if ( oCtrl.tagName=="INPUT" || oCtrl.tagName=="SELECT" )
      oCtrl.value = iValeur;
    else
      oCtrl.innerHTML = iValeur;
  }
}

function CreateSlider(idSlider, iDefaut, strFctIncrement, iDepart, iFin, iWidthTrack)
{
  var objSlider = new slider(
              "slider_"+idSlider,
              14,             //height of track (excluding border)
              parseInt(iWidthTrack),            //width of track (excluding border)
              '#E7E3E7',      //colour of track
              1,              //thickness of track border
              '#E7E3E7',      //colour of track border
              1,              //thickness of runner (in the middle of the track)
              '#8C888C',      //colour of runner
              14,             //height of button (excluding border)
              parseInt(iWidthTrack)/15,             //width of button (excluding border)
              '#ACA8AC',      //colour of button
              2,              //thickness of button border (shaded to give 3D effect)
              '',             //text of button (if any) - the font declaration is important - size it to suit your slider
              true,           //direction of travel (true = horizontal [0 is left], false = vertical [0 is top])
              'MoveSlider',   //the name of the function to execute as the slider moves (null if none)
              'StopSlider'    //the name of the function to execute when the slider stops (null if none)
                              //the functions must have already been defined (or use null for none)
  );
  objSlider.setProperties(idSlider, strFctIncrement, iDepart, iFin);
  objSlider.setPosition(parseInt(iDefaut/iFin)) ;
  return objSlider;
}

function numericIncrementSlider(iPosition, iDepart, iFin)
{
  var iValeur = 0;
  iValeur = (parseInt(iPosition*100)/100) ;
  iValeur = Math.ceil(parseFloat(iDepart) + parseFloat(iValeur*(iFin-iDepart))) ;
  return iValeur;
}
