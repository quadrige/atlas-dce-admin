<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief Ecrit le code javascript n�cessaire pour afficher la palette de couleur
 *        dans une fen�tre popup
 *
 * @param strFormNam Nom du formulaire
 * @param strPopup   Nom de la popup
 * @param strImg     Nom de l'image utilis�e pour afficher les cases couleurs (url complet)
 * @param strImgNoColor     Nom de l'image utilis�e pour afficher la cases sans couleur (url complet)
 * @param strStyle   Nom de la classe de style css
 */
function writeJsColor($strFormName, $strPopup, $strImg, $strImgNoColor, $strStyle)
{
  echo getJsColor($strFormName, $strPopup, $strImg, $strImgNoColor, $strStyle);
}

function getJsColor($strFormName, $strPopup, $strImg, $strImgNoColor, $strStyle)
{
  return "\nvar strFormName = '".$strFormName."';\n".
    "var strImgColor = '".$strImg."';\n".
    "var strStyleColor = '".$strStyle."';\n".
    "function openPopupColor(strPage,strNom,strProprietes,iWidth,iHeight)\n".
    "{\n".
    "  var H = (screen.height - iHeight) / 2;\n".
    "  var L = (screen.width - iWidth) / 2;\n".
    "  popupCarte = window.open(strPage,strNom,strProprietes+',height='+iHeight+',width='+iWidth+',top='+H+',left='+L);\n".
    "  popupCarte.focus();\n".
    "}\n".
    "function changeColor(strName,strNoneAllowed)\n".
    "{\n".
    "  openPopupColor('".$strPopup."?'+strFormName+'&'+strName+'&'+strNoneAllowed,'Colors','scrollbars=0,resizable=0',500,400);\n".
    "}\n".
    "function SetColor(oCtrl){".
    "var td=document.getElementById('td_'+oCtrl.name);\n".
    "if (td){\n".
    "   td.style.backgroundColor=oCtrl.value;\n".
    "   var Img=new Image();\n" .
    "   if (oCtrl.value!=''){\n".
    "       Img.src='".$strImg."';\n".
    "       document.images['img_'+oCtrl.name].src=Img.src;\n".
    "       td.style.backgroundColor=oCtrl.value;\n".
    "  } else {\n" .
    "       Img.src='".$strImgNoColor."';\n" .
    "       document.images['img_'+oCtrl.name].src=Img.src;\n".
    "   }\n".
    " }\n".
    "}\n".
    "function writeColor(strName, strColor, iWidth, iHeight, strNoneAllowed, bBorderColor) {\n".
    "  if ((strColor=='') || (strColor=='AUCUNE')) {\n" .
    "  var strImage='".$strImgNoColor."';".
    "  } else {\n" .
    "  var strImage = strImgColor;".
    "    }\n" .
    "    strHtml = '<table border=0 cellpadding=0 cellspacing=0 width='+iWidth+'>';\n".
    "    strHtml = strHtml + '<tr><td width='+iWidth+' height='+iHeight+' bgcolor=\"'+strColor+'\" id=\"td_'+strName+'\" '+" .
    "      (bBorderColor ? 'style=\"border:#000000 1px solid\"' : '')+'>';\n" .
    "    strHtml = strHtml + '<a href=\"javascript:changeColor(\''+strName+'\',\''+strNoneAllowed+'\');\">';\n".
    "    strHtml = strHtml + '<img name=\"img_'+strName+'\" src=\"'+strImage+'\" alt =\'aucune\' width='+iWidth+' height='+iHeight+' border=0>';\n".
    "    strHtml = strHtml + '</a></td></tr>';\n".
    "    strHtml = strHtml + '</table>';\n".
    "  document.write(strHtml);\n".
    "}\n";
}

/**
 * @brief Classe d'affichage d'un composant de s�lection de couleur dans une palette pr�d�finie
 */
class HtmlColor extends HtmlCtrlBase
{
  // attributs
  var $iWidth;
  var $iHeight;
  var $strNoneAllowed;
  var $bBorderColor = false;

  /**
   * @brief Constructeur par d�faut
   *
   * @param strName        Nom du controle
   * @param strValue       Valeur par d�faut
   * @param strLabel       Etiquette texte associ�e au controle de saisie
   * @param iWidth         largeur du controle
   * @param iHeight        hauteur du controle
   * @param strNoneAllowed contient "true" ou "false" �crit dans une cha�ne et sp�cifie si 'aucune couleur' est autoris�
   */
  function HtmlColor($strName, $strValue, $strLabel, $iWidth, $iHeight, $strNoneAllowed)
  {
    parent::HtmlCtrlBase(0, $strName, $strValue, $strLabel);
    $this->iWidth = $iWidth;
    $this->iHeight = $iHeight;
    $this->strNoneAllowed = $strNoneAllowed;
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetCtrlHtml()
  {
    $strEventOnChange = "";
    $strEventOthers = "";

    foreach($this->tabEvents as $strEvent => $strFunctionJs){
      if ($strEvent=="onchange")
        $strEventOnChange .= $strFunctionJs;
      else
        $strEventOthers .= " ".$strEvent."=\"javascript:".$strFunctionJs."\"";
    }

    $strHtml = "<script language='JavaScript'>".
      "window.writeColor('".$this->name."', '".$this->value."', ".$this->iWidth.", ".$this->iHeight.", '".$this->strNoneAllowed."', ".($this->bBorderColor ? "true" : "false").");".
      "</script>".
      "<input id='inputcolor_".$this->name."' type='hidden' name='".$this->name."' value='".$this->value."' onchange=\"SetColor(this);".$strEventOnChange."\" ".$strEventOthers.">";
    
    return $strHtml;
  }
}

?>