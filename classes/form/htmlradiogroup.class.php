<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief classe d'affichage d'un composant groupe de radio
 *        g�n�re : <input type=radio>
 */
class HtmlRadioGroup extends HtmlCtrlBase
{
  /** liste des boutons radios */
  var $tabRadios;

  /** S�parateur bouton radio */
  var $separator;
  
  /** vrai si label � droite du bouton radio */
  var $bLabelRight;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode      Mode du controle de saisie : =0 modif, =1 lecture
   * @param name       Nom du controle de saisie
   * @param value      Valeur du groupe de boutons radio
   * @param label      Etiquette texte associ�e au groupe
   */
  function HtmlRadioGroup($iMode, $name, $value=0, $label="")
  {
    parent::HtmlCtrlBase($iMode, $name, $value, $label);
    $this->tabRadios = array();
    $this->separator = "&nbsp;&nbsp;";
    $this->bLabelRight = true;
  }

  /**
   * @brief Ajoute un bouton radio au groupe
   * 
   * @param strValue  Valeur du radio bouton
   * @param strLabel  Etiquette texte associ� au bouton radio
   */
  function AddRadio($strValue, $strLabel)
  {
    $this->tabRadios[count($this->tabRadios)] = array("value" => $strValue, 
                                                      "label" => $strLabel,
                                                      "event" => array());
  }

  /**
   * @brief Ajoute un evenement sur un bouton radio
   * 
   * @param strValue      Valeur du radio bouton
   * @param strEvent      Nom de l'�v�nement
   * @param strFunctionJS Nom de la fonction javascript appel�e (pas n�cessaire d'ajouter 'javascript:')
   */
  function AddEventToRadio($strValue, $strEvent, $strFunctionJS)
  {
    // recherche l'indice de la valeur
    $iPos = -1;
    while( ++$iPos < count($this->tabRadios) ) {
      if( $this->tabRadios[$iPos]["value"] == $strValue )
        break;
    }

    if( $iPos == -1 || $iPos>=count($this->tabRadios) ) 
      return;

    $this->tabRadios[$iPos]["event"] = array_merge( $this->tabRadios[$iPos]["event"], array($strEvent => $strFunctionJS));
  }
  
  /**
   * @brief Genere puis retourne tous les evenements li�s au controle html
   *
   * @return Retourne les �v�nements concat�n�s dans une chaine pour �tre inclus dans
   *         dans un tag html de type controle de saisie
   */
  function GetHtmlEventRadio($iPos)
  {
    $strHtml = "";
    while( list($strEvent, $strFunctionJs) = each($this->tabRadios[$iPos]["event"]) )
      $strHtml .= " ".$strEvent."=\"javascript:".$strFunctionJs."\"";
    return $strHtml;
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetCtrlHtml()
  {
    // contruit le controle
    $strHtml = "";
    if( $this->iMode == "1" ) {
      // mode lecture
      // recherche puis affiche le label s�lectionn�
      $iPos = -1;
      while( ++$iPos < count($this->tabRadios) ) {
        if( $this->tabRadios[$iPos]["value"] == $this->value ) break;
      }

      if( $iPos != -1 && $iPos<count($this->tabRadios) )
        $strHtml = "<span class=\"".$this->cssCtrl."\">".$this->tabRadios[$iPos]["label"]."</span>";

    } else {

      // mode modif
      // input type=radio
      for($i=0; $i<count($this->tabRadios); $i++) {
        if( $this->bLabelRight == false )
          $strHtml .= "<label class=\"".$this->cssCtrl."\" for=\"".$this->name."_".$i."\">".$this->tabRadios[$i]["label"]."</label>";

        $strHtml .= "<input type=\"radio\" ".
          "name=\"".$this->name."\" id=\"".$this->name."_".$i."\" ".
          "value=\"".$this->tabRadios[$i]["value"]."\"".
          ( $this->value == $this->tabRadios[$i]["value"] ? " checked" : "").
          $this->GetHtmlEventRadio($i).
          ">";

        if( $this->bLabelRight == true )
          $strHtml .= "<label class=\"".$this->cssCtrl."\" for=\"".$this->name."_".$i."\"> ".$this->tabRadios[$i]["label"]."</label>";

        $strHtml .= $this->separator;
      }
    }
    
    return $strHtml;
  }
}

?>