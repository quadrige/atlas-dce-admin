<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief Classe d'affichage d'un composant de saisie case � cocher
 *        g�n�re : <input type=checkbox>
 */
class HtmlCheckbox extends HtmlCtrlBase
{
  /** Texte associ� � la valeur de la case en mode lecture = oui, non par d�faut */
  var $tabValLecture;

  /** valeur de la case pour qu'elle soit coch�e =1 par d�faut */
  var $checkboxValue;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode  Mode du controle de saisie : =0 modif, =1 lecture
   * @param name   Nom du controle de saisie
   * @param value  Valeur du controle de saisie
   * @param label  Etiquette texte associ�e au controle de saisie
   */
  function HtmlCheckbox($iMode, $name, $value=0, $label="")
  {
    parent::HtmlCtrlBase($iMode, $name, $value, $label);

    $this->checkboxValue = "1";

    $this->tabValLecture = array();
    $this->tabValLecture["checked"] = "Oui";
    $this->tabValLecture["notchecked"] = "Non";
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetCtrlHtml()
  {
    // contruit le controle
    $strHtml = "";
    if( $this->iMode == "1" ) {
      // mode lecture
      $strHtml = "<span class=\"".$this->cssCtrl."\">".
        ( $this->value == $this->checkboxValue
          ? $this->tabValLecture["checked"]
          : $this->tabValLecture["notchecked"] ).
        "</span>";
    } else {
      // mode modif
      // input type=checkbox
      $strHtml = "<input type=\"checkbox\" ".
        "name=\"".$this->name."\" ".
        "value=\"".$this->checkboxValue."\" ".
        ( $this->value == $this->checkboxValue ? " checked" : "").
        ( $this->bDisabled == true ? " disabled" : "").
        $this->GetHtmlEvent().
        ">";
    }
      
    return $strHtml;
  }
}

?>