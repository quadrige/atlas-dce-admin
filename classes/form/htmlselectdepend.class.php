<?php
include_once("htmlselect.class.php");

/**
 * @brief Classe d'affichage d'un composant de saisie combobox ou liste de selection
 *        g�n�re : <select >
 */
class HtmlSelectDepend extends HtmlSelect
{
  /** tableau des d�pendances */
  var $tabDependances;
  
  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode       Mode du controle de saisie : =0 modif, =1 lecture
   * @param name        Nom du controle de saisie
   * @param value       Valeur par d�faut du controle de saisie. Dans le cas d'un select multiple, value peut �tre un tableau contenant les valeurs s�lectionn�es par d�faut
   * @param label       Etiquette texte associ�e au controle de saisie
   * @param size        hauteur de la zone s�lection (=1 : comboBox, >1 : liste s�lection). =1 par d�faut
   * @param widthSelect Largeur en pixel de la saisie de s�lection
   * @param widthOption largeur en pixel de la dropdown list box
   */
  function HtmlSelectDepend($iMode, $name, $value="", $label="" ,$size="1", $widthSelect="", $widthOption="")
  {
    parent::HtmlSelect($iMode, $name, $value, $label, $size, $widthSelect, $widthOption);
    $this->tabDependances = array();
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetCtrlHtml()
  {
    // contruit le controle
    $strHtml = parent::GetCtrlHtml();

    if( $this->iMode == "0" ) {
      $strHtml .= $this->_getHtmlDependances();
    }
    
    return $strHtml;
  }

  /**
   * @brief D�termine une d�pendance de ce controle sur un autre controle htmlSelect (l'autre d�pend du contr�le)
   *        En base de donn�es ces deux controles sont li� par une relation de type 1:n ou n:m
   * 
   * @param formName nom du formulaire contenant les 2 s�lect
   * @param ctrlDest nom du controle receveur
   * @param tabSql   tableau contenant les param�tres de la requete SQL � ex�cuter : 
   *                 Requete :
   *                   "select t1.<chpText> as VALUE, t1.<chpJoin> as ID from <tableDest> as t1, <joinTable> as t2
   *                    where t1.<chpJoin>=t2.<chpJoin> and t2.<chpVariable>=<valueJoin> order by VALUE"
   *                 Valeurs attendues :
   *                   tableau de 5 champs index� ou associatif avec dans l'ordre les index : "joinTable", "tableDest", "chpJoin", "chpVariable", "chpText"
   * @param tabDef   tableau contenant la valeur par d�faut du ctrl source (-1000 = la src n'a pas de valeur par d�faut)
   *                 et les valeurs et textes par d�faut (OK : src!=default, NOK : src=default)  du ctrl receveur
   *                 (valDest=-1000 : le receveur n'a pas de valeur par d�faut) 
   *                 Valeurs attendues :
   *                   tableau de 4 champs index� ou associatif avec dans l'ordre les index : "defaultValDest", "defaultValSrc", "strDefaultOK", "strDefaultNOK"
   * @param bRemplir Pour remplir ou vider le receveur (mise des valeurs par d�faut si non -1000)
   * @param jsCondition Ajout d'une condition javascript avant l'ex�cution
   */
  function AddDependance($formName, $ctrlDest, $tabSql, $tabDefault, $bRemplir=true, $jsCondition="")
  {
    /* * V�rification des param�tres * */
    $tabKeySql = array("joinTable", "tableDest", "chpJoin", "chpVariable", "chpText");
    $tabKeyDef = array("defaultValDest", "defaultValSrc", "strDefaultOK", "strDefaultNOK", );
    
    $strMsgErrSql = "<div align=left>" .
                    "M�thode AddDependance de HtmlSelect (".$this->name.").<br>" .
                    "Le tableau de description de la requ�te SQL � ex�cuter doit avoir l'un des formats suivants :<br>" .
                    "<li>Tableau associatif dont les cl�s sont, dans l'ordre : <b>'".implode("', '", $tabKeySql)."'</b></li>" .
                    "<li>Tableau index� de longueur ".count($tabKeySql)." dont les index correspondent respectivement aux cl�s pr�c�demment cit�es.</li>" .
                    "<br>" .
                    "Les valeurs du tableau sont, respectivement pour les cl�s cit�es :<br>" .
                    "<li>La table SQL de jointure (peut �tre identique � tableDest si relation 1:n)</li>" .
                    "<li>La table SQL contenant les donn�es du ctrlDest</li>" .
                    "<li>Le champ de jointure (Champ ID de ctrlDest)</li>" .
                    "<li>Le champ variable correspondant � la valeur (ID) de ce contr�le</li>" .
                    "<li>Le champ correspondant au texte affich� dans le ctrlDest</li>" .
                    "</div>";
    $strMsgErrDef = "<div align=left>" .
                    "M�thode AddDependance de HtmlSelect (".$this->name.").<br>" .
                    "Le tableau de description des valeurs par d�faut doit avoir l'un des formats suivants :<br>" .
                    "<li>Tableau associatif dont les cl�s sont, dans l'ordre : <b>'".implode("', '", $tabKeyDef)."'</b></li>" .
                    "<li>Tableau index� de longueur ".count($tabKeyDef)." dont les index correspondent respectivement aux cl�s pr�c�demment cit�es.</li>" .
                    "<br>" .
                    "Les valeurs du tableau sont, respectivement pour les cl�s cit�es :<br>" .
                    "<li>La valeur par d�faut de ctrlDest. Si vaut <b>-1000</b> : il n'y aura pas de valeur par d�faut dans le contr�le d'arriv�e</li>" .
                    "<li>La valeur par d�faut de ce contr�le : affiche dans ctrlDest strDefaultNOK. Si vaut <b>-1000</b> : il n'y a pas de valeur par d�faut</li>" .
                    "<li>Le texte affich� par d�faut dans ctrlDest si la valeur du contr�le de d�part est diff�rente de defaultValSrc</li>" .
                    "<li>Le texte affich� par d�faut dans ctrlDest si la valeur du contr�le de d�part est �gale � defaultValSrc</li>" .
                    "</div>";
   
    /* V�rification des param�tres de la requete */
    $tmpSql = array();
    if (count($tabSql)<count($tabKeySql)){
      trigger_error($strMsgErrSql, E_USER_ERROR);
    }
    foreach ($tabKeySql as $index=>$key){
      if (isset($tabSql[$key]) && !($bRemplir && $tabSql[$key]=="")) {
        $tmpSql[$key] = $tabSql[$key];
      } else if (isset($tabSql[$index]) && !($bRemplir && $tabSql[$index]=="")) {
        $tmpSql[$key] = $tabSql[$index];
      } else { 
        trigger_error($strMsgErrSql, E_USER_ERROR);
      }
    }
    $tabSql = $tmpSql;
    /* V�rification des param�tres par d�fauts */
    $tmpDef = array();
    if (count($tabDefault)<count($tabKeyDef)){
      trigger_error($strMsgErrDef, E_USER_ERROR);
    }
    foreach ($tabKeyDef as $index=>$key){
      if (isset($tabDefault[$key])) {
        $tmpDef[$key] = $tabDefault[$key];
      } else if (isset($tabDefault[$index])) {
        $tmpDef[$key] = $tabDefault[$index];
      } else {
        trigger_error($strMsgErrDef, E_USER_ERROR);
      }
    }
    $tabDefault = $tmpDef;
    
    /* * Ecriture des elements du code HTML * */
    $frameName = "maj_".$ctrlDest."_from_".$this->name;
    
    /* Construction de l'url � ex�cuter */
    $strUrl = ALK_SIALKE_URL."scripts/espace/maj_select_js.php?formName=".$formName."&ctrlDest=".$ctrlDest;
    $strUrl .= "&bRemplir=".($bRemplir ? "1" : "0");
    foreach ($tabSql as $key=>$value){
      $strUrl .= "&".$key."=".$value;
    }
    foreach ($tabDefault as $key=>$value){
      $strUrl .= "&".$key."=".str_replace(" ", "_", $value);
    }
    $strUrl .= "&valueJoin=";
    
    /* * Ajout de l'�venement onchange et de l'iframe * */
    $this->AddEvent("onchange", $jsCondition." if (this.form.elements['".$ctrlDest."']) window.frames['".$frameName."'].location = '".$strUrl."'+this.options[this.selectedIndex].value;");
    $this->tabDependances[$frameName] = "<iframe name='".$frameName."' width='0' height='0' frameborder='0' src='".ALK_SIALKE_URL."blank.htm'></iframe>"; 
  }
  
  /**
   * @brief retourne le code HTML de toutes les iframes ajout�es lors de l'appel � AddDependance
   * 
   * @return strHtml  code HTML de toutes les iframes ajout�es lors de l'appel � AddDependance
   */
  function _getHtmlDependances()
  {
    $strHtml = "";
    foreach ($this->tabDependances as $iframe){
      $strHtml .= $iframe;
    }
    return $strHtml;
  }
}

?>