<?php
include_once("htmlpanel.class.php");
include_once("htmlblock.class.php");

/** constante sur la m�thode du formulaire */
if( !defined("ALK_FORM_METHOD_GET") )  define("ALK_FORM_METHOD_GET", 0);
if( !defined("ALK_FORM_METHOD_POST") ) define("ALK_FORM_METHOD_POST", 1);

/** constante sur le mode du formulaire ajout / modif / lecture */
if( !defined("ALK_FORM_MODE_READ") )   define("ALK_FORM_MODE_READ", 0);
if( !defined("ALK_FORM_MODE_ADD") )    define("ALK_FORM_MODE_ADD", 1);
if( !defined("ALK_FORM_MODE_UPDATE") ) define("ALK_FORM_MODE_UPDATE", 2);
if( !defined("ALK_FORM_MODE_DEL") )    define("ALK_FORM_MODE_DEL", 3);


/**
 * @brief classe d'affichage des blocs de controle d'un formulaire
 */
class HtmlForm extends HtmlPanel
{
  /** mode du formulaire */
  var $iMode;

  /** Ensemble des blocs constituant le formulaire rang� par niveau : 0 = niveau formulaire, i>0 = ieme onglet */
  var $tabHtmlPanel; 

  /** tableau des boutons li�s au formulaire (validation, suppression, annulation, etc...) rang� par niveau */
  var $tabHtmlLink;

  /** tableau de constante li�e aux scripts js pour �viter de les charger n fois */
  var $tabConstJs;

  /** Action du formulaire */
  var $strAction;

  /** M�thode du formulaire */
  var $iMethod; 

  /** vrai si upload n�cessaire sur le formulaire */
  var $bUpload;

  /** cible du formulaire */
  var $target;

  /** vrai si v�rification des droits sur le formulaire : si vrai et constante n'existe pas => pas de droit, =false par d�faut */
  var $bVerifFieldRight;

  /** classe css sur le formulaire */
  var $cssForm;
  
  /** Dimension de la table contenant le formulaire */
  var $strTableWidth;
  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode     Mode du formulaire : =1 ajout, =2 maj, =3 lecture
   * @param name      Nom du formulaire
   * @param strAction Action du formulaire (url appel�e sur validation)
   * @param iMethod   ALK_FORM_METHOD_GET (par d�faut) ou ALK_FORM_METHOD_POST
   * @apram bUpload   faux par d�faut, vrai si upload n�cessaire
   */
  function HtmlForm($iMode, $name, $strAction="", $iMethod=ALK_FORM_METHOD_GET, $bUpload=false)
  {
    parent::HtmlPanel($name);
    $this->iMode     = $iMode;
    $this->strAction = $strAction;
    $this->iMethod   = $iMethod; 
    $this->bUpload   = $bUpload;
    $this->target    = "";

    $this->bVerifFieldRight = false;
    $this->bTemplate        = false;

    $this->tabHtmlPanel = array(array());
    $this->tabHtmlLink = array(array());

    $this->cssForm = "formForm";
    $this->strTableWidth = "";
    
    $this->tabConstJs = array("TXT"    => "../../lib/lib_formTxt.js",
                              "SELECT" => "../../lib/lib_formSelect.js",
                              "CHECK"  => "../../lib/lib_formCheck.js",
                              "DATE"   => "../../lib/lib_formDate.js",
                              "NUMBER" => "../../lib/lib_formNumber.js",
                              "COLOR"  => "../../lib/lib_formColor.js",
                              "FORM"   => "../../lib/lib_form.js",
                              "HELP"   => "../../classes/form/htmlctrlbase.js");
  }

  /**
   * @brief Construit un bloc, l'ajoute au formulaire puis retourne une r�f�rence sur l'objet cr��
   *
   * @param idBlock     identifiant du bloc
   * @param strTitle    titre du bloc (= chaine vide par d�faut, dans ce cas, le cadre titre n'est pas affich�)
   * @param strDesc     description du bloc (= chaine vide par d�faut)     
   * @param iWidthLabel largeur en px de la colonne label (=180 par d�faut)
   * @param iWidthCtrl  largeur en px de la colonne ctrl  (=390 par d�faut)
   * @param bMaintitle  vrai si titre principal, faux si titre de bloc classique (par d�faut)
   * @return Retourne une r�f�rence sur le bloc cr��
   */
  function AddBlock($idSheet, $idBlock, $strTitle="", $strDesc="", $iWidthLabel="180", $iWidthCtrl="390", $bMaintitle=false)
  {
    $oBlock = new HtmlBlock($this, $this->iMode, $idBlock, $strTitle, $strDesc, $iWidthLabel, $iWidthCtrl, $bMaintitle);
    $this->tabHtmlPanel[0][count($this->tabHtmlPanel[0])] =& $oBlock;
    return $oBlock;
  }

  /**
   * @brief Construit un lien, l'ajoute au formulaire puis retourne une r�f�rence sur l'objet cr��
   *
   * @param idLink        identifiant du lien
   * @param strlink       Lien plac� sur le texte ou l'image
   * @param strTxt        Texte associ� au lien ou � l'image en info-bulle
   * @param strImg        Nom de l'image
   * @param strImgRol     Nom de l'image roll
   * @return Retourne une r�f�rence sur le lien cr��
   */
  function AddLink($idSheet, $idLink, $strLink, $strTxt, $strImg="", $strImgRol="")
  {
    $oLink = new HtmlLink($strLink, $strTxt, $strImg, $strImgRol);
    $oLink->name = $idLink;
    $this->tabHtmlLink[0][count($this->tabHtmlLink[0])] =& $oLink;
    return $oLink;
  }
  
  /**
   * @brief M�morise les types de controle js n�cessaire au formulaire
   *        M�thode appel�e uniquement le HtmlPanel->addCtrl()
   *
   * @param oCtrl  r�f�rence du ctrl de saisie
   */
  function _SetValidator($oCtrl)
  {
    if( count($oCtrl->tabValidator) > 0 ) {
      switch( $oCtrl->tabValidator["type"] ) {
      case "memo": case "text": 
      case "mail": case "textnum": 
      case "textalpha":             $this->AddScriptJs($this->tabConstJs["TXT"]);    $this->AddScriptJs($this->tabConstJs["FORM"]); break;
      case "heure5": case "date10":
      case "datetime16":            $this->AddScriptJs($this->tabConstJs["DATE"]);   $this->AddScriptJs($this->tabConstJs["FORM"]); break;
      case "int":                   $this->AddScriptJs($this->tabConstJs["NUMBER"]); $this->AddScriptJs($this->tabConstJs["FORM"]); break;
      case "radio":                                                                  $this->AddScriptJs($this->tabConstJs["FORM"]); break;
      case "checkgroup":            $this->AddScriptJs($this->tabConstJs["CHECK"]);  $this->AddScriptJs($this->tabConstJs["FORM"]); break;
      case "select":                $this->AddScriptJs($this->tabConstJs["SELECT"]); $this->AddScriptJs($this->tabConstJs["FORM"]); break;
      default : break;
      }
    }
    if ( get_class($oCtrl)=="htmlgroup" ){
      foreach ($oCtrl->tabCtrl as $oSubCtrl){
        $this->_SetValidator($oSubCtrl);
      }
    }
    
    if( get_class($oCtrl) == "htmlcolor" ) {
      $this->AddScriptJs($this->tabConstJs["COLOR"]);
    }
  }
  
  /**
   * @brief Retourne le code html l'ent�te du formulaire
   *
   * @return Retourne un string
   */
  function GetHtmlHeader()
  {
    $strHtml = "<form name=\"".$this->name."\"".
      ( $this->cssForm != "" ? " class=\"".$this->cssForm."\"" : "" ).
      ( $this->cssStyle != "" ? " style=\"".$this->cssStyle."\"" : "" ).
      " action=\"".$this->strAction."\"".
      " method=\"".( $this->iMethod == ALK_FORM_METHOD_GET ? "get" : "post" )."\"".
      ( $this->bUpload == true ? " enctype=\"multipart/form-data\"" : "" ).
      ( $this->target != "" ? " target=\"".$this->target."\"" : "" ).
      $this->GetHtmlEvent().
      ">";
    return $strHtml;
  }

  /**
   * @brief Retourne le code html du bloc et des controles qu'il contient
   *
   * @return Retourne un string
   */
  function GetHtmlPanels($iSheet=0, $strTemplateFile=null, $strCss="")
  {
    if ($strCss=="")
      $strCss = " style='padding-top:8px'";
    if ($strTemplateFile==null)
      $strTemplateFile = $this->strTemplateFile;
      
    $strHtml = "";
    if( $strTemplateFile == "" ) {
      $strHtml .= "<table border='0' cellpadding='0' cellspacing='0' ".$strCss." align='center'".($this->strTableWidth!="" ? " width='".$this->strTableWidth."'" : "").">";
      for($i=0; $i< count($this->tabHtmlPanel[$iSheet]); $i++) {
        $strHtml .= "<tr><td valign='top'>".$this->tabHtmlPanel[$iSheet][$i]->getHtml()."</td></tr>";
      }
      if( count($this->tabHtmlLink[$iSheet]) > 0 ) {
        $strHtml .= "<tr><td align='center' style='padding-top:16px; padding-bottom:16px'>";
        $nbHtmlLink = count($this->tabHtmlLink[$iSheet]);
        for($i=0; $i < $nbHtmlLink; $i++) {
          $strHtml .= $this->tabHtmlLink[$iSheet][$i]->getHtml().
            ( $nbHtmlLink> 4 && $i >0 && ($i % 3) == 0 ? "<br>" : "" ).
            ( $i < $nbHtmlLink-1 ? "&nbsp;&nbsp;" : "" );
        }
        $strHtml .= "</td></tr>";
      }
      $strHtml .= "</table>";
    } else {
      for($i=0; $i< count($this->tabHtmlPanel[$iSheet]); $i++) {
        if( $this->tabHtmlPanel[$iSheet][$i]->name == "" ) 
          $this->tabHtmlPanel[$iSheet][$i]->name = "block_".$i;
        $strBlockName = $this->tabHtmlPanel[$iSheet][$i]->name;
        $strHtmlPanel = $this->tabHtmlPanel[$iSheet][$i]->getHtml();
        $this->oTemplate->assign($strBlockName, $strHtmlPanel);
      }
      $nbHtmlLink = count($this->tabHtmlLink[$iSheet]);
      for($i=0; $i < $nbHtmlLink; $i++) {
        if( $this->tabHtmlLink[$iSheet][$i]->name == "" ) 
          $this->tabHtmlLink[$iSheet][$i]->name = "bt_".$i;
        $this->oTemplate->assign($this->tabHtmlLink[$iSheet][$i]->name, $this->tabHtmlLink[$iSheet][$i]->getHtml());
      }
      $strHtml .= $this->oTemplate->fetch($strTemplateFile);
    }
    return $strHtml;
  }

  /**
   * @brief Retourne le code html de la fin du formulaire
   *
   * @brief Retourne un string
   */
  function GetHtmlFooter()
  {
    return "</form>";
  }

  /**
   * @brief Retourne le code html du formulaire complet
   *
   * @return Retourne un string
   */
  function GetHtml()
  {
    // v�rifie si des controles ont des aides
    $bUseHelp = false;
    for($p=0; $p< count($this->tabHtmlPanel[0]); $p++) {
      if( get_class($this->tabHtmlPanel[0][$p]) == "htmlblock" ) {
        for($c=0; $c< count($this->tabHtmlPanel[0][$p]->tabHtmlCtrl); $c++) {
          if( isset($this->tabHtmlPanel[0][$p]->tabHtmlCtrl[$c]->labelHelp) && 
              $this->tabHtmlPanel[0][$p]->tabHtmlCtrl[$c]->labelHelp != "" ) {
            $bUseHelp = true;
            break;
          }
        }
      }
      if( $bUseHelp = true )
        break;
    }

    if( $bUseHelp == true ) {
      $this->AddScriptJs($this->tabConstJs["HELP"]);
    }
    return $this->GetHtmlJs().
      $this->GetHtmlHeader().
      $this->GetHtmlPanels().
      $this->GetHtmlFooter();
  }

}

?>