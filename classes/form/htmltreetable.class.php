<?php
include_once("htmlbase.class.php");
/**
 * @package Alkanet_Class_Form
 * @class AlkHtmlTreeTable
 * 
 * Classe affichant un arbre de donn�es sous un format d'affichage d'arbre par d�faut
 *        mais �galement sous des formats d�finis par l'utilisateur (d�finition d'un template pour l'arbre).
 *        Chaque noeud de l'arbre (TreeData) est caract�ris� par son noeud parent et
 *        optionnellement) par une cat�gorie d'affichage. 
 *        A chaque cat�gorie d'affichage (TreeCategorie), on peut associer diff�rentes propri�t�s 
 *        d'affichage :
 *          <ul><li>une icone</li>
 *          <li>un template</li><ul>
 *        Si aucun template n'est d�fini pour l'arbre ou ses noeuds, l'affichage sera celui d'un arbre Javascript
 *        Sinon, l'arbre ou ses noeuds suivront l'affichage d�termin� par leur template
 */
class HtmlTreeTable extends HtmlBase
{
  var $name;
  var $tabProperties;
  var $tabCategorieName;
  var $tabCategorie;
  var $tabData;
  var $oRoot;
  var $bOpenAll;
  var $opened_id;
  
  /**
   *  Constructeur
   * @param oForm   Formulaire propri�taire
   * @param name    Nom de l'arbre
   * @param strTitle    titre du bloc
   * @param strDesc     descriptif du bloc
   * @param iWidthLabel largeur d�di�e au label du controle (=180 par d�faut)
   * @param iWidthCtrl  largeur d�di�e au controle de saisie (=390 par d�faut)
   */
  function HtmlTreeTable( $name, $strTitle="", $strDesc="", $iWidthLabel="180", $iWidthCtrl="390")
  {
    parent::HtmlBase($name);
    $this->oNull = null;
    $this->tabProperties = array();
    $this->tabCategorie = array();
    $this->tabCategorieName = array();
    $this->tabData = array();
    $this->oRoot =& new TreeData($this, "root", $this->oNull, $this->oNull);
    $this->bOpenAll = true;
    $this->opened_id = "-1";
  }
  
  /**
   *  Ajoute une propri�t� � l'arbre qui sera exploit�e lors de l'affichage
   * @param name    Nom de la propri�t�
   * @param value   Valeur de la propri�t�
   * @param bForce  Pour �craser la valeur d'une propri�t� de m�me nom d�j� d�finie (true par d�faut)
   */
  function addProperty($name, $value, $bForce=true)
  {
    if ( $bForce || !array_key_exists($name, $this->tabProperties) ){
      $this->tabProperties[$name] = $value;
    }
  }
  
  /**
   *  Retourne la valeur d'une propri�t� d'affichage d�finie par la m�thode addProperty
   * @param name        Nom de la propri�t�
   * @param strDefault  Valeur par d�faut attendu en retour
   * @return mixed    Valeur de la propri�t� d'affichage
   */
  function getPropertyValue($name, $strDefault="")
  {
    if ( array_key_exists($name, $this->tabProperties) ){
      return $this->tabProperties[$name];
    }
    return $strDefault;
  }
  
  
  /**
   *  Cr�e une cat�gorie d'affichage et en retourne une r�f�rence
   * @param name      Nom de la cat�gorie
   * @param icone     Image repr�sentant les �l�ments de cette cat�gorie
   * @param template  Fichier template pour repr�senter les �l�ments de cette cat�gorie
   * @return & TreeCategorie
   */
  function &addCategorie($name, $icone="", $template="")
  {
    $oCategorie = new TreeCategorie($name, $icone, $template);
    $this->addCategorieByObj($oCategorie);
    return $oCategorie;
  }
  
  /**
   *  Ajoute une instance cat�gorie d'affichage
   * @param & oCategorie   Cat�gorie d'affichage
   */
  function addCategorieByObj(&$oCategorie)
  {
    if ( is_null($oCategorie) ) return;
    if ( in_array($oCategorie->name, $this->tabCategorieName) ) return;
    $this->tabCategorie[] =& $oCategorie;
    $this->tabCategorieName[] = $oCategorie->name;
  }
  
  
  
  /**
   *  Cr�e un "noeud de donn�es" de parent donn� et en retourne une r�f�rence 
   * @param oParent     TreeData ou null, Noeud parent du nouveau noeud
   * @param oCategorie  Cat�gorie de repr�sentation du noeud
   * @param node_id     Identifiant du noeud
   * @param tabData     Tableau de donn�e du noeud
   * @param tabActions  Tableau des actions du noeud
   * @return TreeData
   */
  function addNode($oParent, $oCategorie=null, $node_id="", $tabData=array(), $tabActions=array(), $strIcone="")
  {
    $this->addCategorieByObj($oCategorie);    
    if ( is_null($oParent) ){
      if ( $node_id=="" ) $node_id = $this->oRoot->getNextNodeId();
      $oData = new TreeData($this, $node_id, $this->oRoot, $oCategorie, $tabData, $tabActions, $strIcone);
    }
    else if ( is_object($oParent) && is_a($oParent, "TreeData") && $oParent->isInTree($this) ){
      if ( $node_id=="" ) $node_id = $oParent->getNextNodeId();
      $oData = new TreeData($this, $node_id, $oParent, $oCategorie, $tabData, $tabActions, $strIcone);
    }
    else 
      return $this->oNull;
    $this->tabData[] =& $oData;
    return $oData;
  }
  
  /**
   *  D�finit les propri�t�s par d�faut d'un arbre de type Treeview (@see /lib/lib_arbre.js)
   */
  function setDefaultProperties()
  {
    $this->addProperty("iTabLg",           "'".$this->iWidthCtrl."'", false);
    $this->addProperty("classLinkFolder",  "''", false);
    $this->addProperty("classTextFolder",  "''", false);
    $this->addProperty("classLinkDoc",     "''", false);
    $this->addProperty("classTextDoc",     "''", false);
    $this->addProperty("imgNodeOpen",      "'".ALK_URL_SI_MEDIA."images/icon_dossier_close.gif'", false);
    $this->addProperty("imgNodeClose",     "'".ALK_URL_SI_MEDIA."images/icon_dossier_cant_open.gif'", false);
    $this->addProperty("imgNodeLineClose", "'".ALK_URL_SI_MEDIA."images/icon_arbo_open.gif'");
    $this->addProperty("imgNode",          "'".ALK_URL_SI_MEDIA."images/icon_dossier_cant_open.gif'", false);
    $this->addProperty("imgIconC",         "'".ALK_URL_SI_MEDIA."images/icon_arbo_open.gif'", false);
    $this->addProperty("imgIconO",         "'".ALK_URL_SI_MEDIA."images/icon_arbo_close.gif'", false);
    $this->addProperty("imgIconCOC",       "''", false);
  }
  
  /**
   *  Retourne le contenu html de l'arbre de donn�es
   *        Si aucun template n'est d�fini pour l'arbre ou ses noeuds, l'affichage sera celui 
   *        d'un arbre Javascript (@see /lib/lib_arbre.js)
   *        Sinon, l'arbre ou ses noeuds suivront l'affichage d�termin� par leur template
   * @return string html
   */
  function getHtml()
  {
    $strHtml = "";
    $nbTemplate = 0;
    foreach ($this->tabCategorie as $oCategorie){
      if ( $oCategorie->template!="" ){
        $nbTemplate++;
      }
    }
    
    // Template sur l'arbre d�finit
    if ( $this->strTemplateFile!="" ){
      foreach ($this->tabProperties as $property=>$value){
        $this->oTemplate->assign($property, $value);
      }
      $contents = $this->oRoot->getHtml();
      if ( $contents!="" ){
        $this->oTemplate->assign("contents", $contents);
        $strHtml .= $this->oTemplate->fetch($this->strTemplateFile);
      }
    }
    
    // Aucun template : arbre javascript
    else if ( !($nbTemplate==count($this->tabCategorie) && $nbTemplate>0) ){
      $this->addScriptJs(ALK_ROOT_URL."lib/lib_layer.js");
      $this->addScriptJs(ALK_ROOT_URL."lib/lib_arbre.js");
      $strJs = "var obj_".$this->name." = new Treeview(document, 'layer".$this->name."', 1, '".$this->name."');";
      $this->setDefaultProperties();
      foreach ($this->tabProperties as $property=>$value){
        $strJs .= "obj_".$this->name.".".$property." = ".$value.";";
      }
      $this->addJs($strJs);

      if ($this->title!=""){
        $strHtml .= "<table summary='treeTable' class='".$this->cssBlock."' id='".$this->name."'>" .
                    "<thead>" .
                    "<tr><td class='rowDimension' style='width:".$this->iWidthCtrl."px;font-size:0px;' height='0'><div style='width:".$this->iWidthCtrl."px; height:0px;font-size:0px;'></div></td></tr>".
                    "<tr><td class='main'>".$this->title."</td></tr>" .
                    "</thead>" .
                    "</table>";
      }
      $strHtml .= "<div id='layer".$this->name."' align='center' style='overflow:auto;height:".$this->iWidthLabel."px;width:".$this->iWidthCtrl."px;'></div>";
      $strHtml .= $this->oRoot->getHtml();
      $strHtml .= "<script language='javascript'>";
      if ( $this->bOpenAll )
        $strHtml .= "obj_".$this->name.".OpenAll();";
      else if ( $this->opened_id!=-1 ){
        $strHtml .= "obj_".$this->name.".OpenNode('".$this->opened_id."');";
      }
      $strHtml .= "obj_".$this->name.".Draw();";
      $strHtml .= "</scipt>";
    }
    
    // Pas de template sur l'arbre d�fini mais noeuds en templates : Donne les propri�t�s de l'arbre aux noeuds
    else {
      global $oSpace;
      $this->oTemplate =& $oSpace->oTemplate;
      foreach ($this->tabProperties as $property=>$value){
        $this->oTemplate->assign($property, $value);
      }
      $strHtml .= $this->oRoot->getHtml();
    }
    return $strHtml;
  }
  

}
/**
 * @file alkhtmltreetable.class.php
 * @Class TreeData
 * @package Alkanet_Class_Form 
 *  Noeud d'un arbre AlkHtmlTreeTable caract�ris� par son noeud parent et
 *        optionnellement) par une cat�gorie d'affichage. 
 */
class TreeData extends HtmlPanel 
{
  var $oTree;
  
  var $node_id;
  
  var $node_niveau;
  
  var $tabData;
  
  var $oParent;
  
  var $oCategorie;
  
  var $tabChildren;
  
  var $tabActions; 
  
  var $bSelected;
  
  var $icone;
  
  /**
   *  Constructeur
   * @param AlkHtmlTreeTable oTree  Arbre possesseur du noeud
   * @param node_id     Identifiant du noeud
   * @param oParent     Parent du noeud
   * @param oCategorie  Cat�gorie du noeud
   * @param tabData     Tableau des donn�es du noeud
   * @param tabAction   Tableau des actions du noeud
   */
  function TreeData(&$oTree, $node_id, &$oParent, &$oCategorie, $tabData=array(), $tabActions=array(), $icone="")
  {
    $this->oTree = $oTree;
    $this->node_id      = $node_id;
    $this->tabChildren  = array();
    $this->tabData      = $tabData;
    $this->tabActions   = $tabActions;
    $this->icone        = $icone;
    $this->oCategorie   =& $oCategorie;
    if (!is_null($this->oCategorie)){
      $this->oCategorie->addData($this);
    } 
    $this->oParent =& $oParent;
    $this->node_niveau = 0;
    if (!is_null($this->oParent)){
      $this->node_niveau = $this->oParent->node_niveau+1;
      $this->oParent->addChildren($this);
    }
    $this->bSelected = false;
  }
  
  /**
   *  D�termine si un noeud est poss�d� par l'arbre donn�
   * @param AlkHtmlTreeTable oTree  Arbre � �valuer en possesseur du noeud
   * @param boolean   L'arbre du noeud est bien celui pass� en param�tre
   */
  function isInTree($oTree)
  {
    return $this->oTree->equals($oTree);
  }
  
  /**
   *  Ajoute une donn�e au tableau des donn�es
   * @param oData (mixed) Donn�e � ajouter
   * @param index         Index de la donn�e ("" par d�faut)
   */
  function addData($oData, $index="")
  {
    if ($index==""){
      $this->tabData[] = $oData;
    }
    else if ($index=="node_id"){
      $this->node_id = $oData;
    }
    else {
      $this->tabData[$index] = $oData;
    }
  }
  
  /**
   *  Ajoute une action au tableau des actions
   * @param index       Index de l'action
   * @param strAction   Valeur de l'action
   */
  function addAction($index, $strAction)
  {
    $this->tabActions[$index] = $strAction;
  }
  
  
  /**
   *  Ajoute un fils au noeud courant
   * @param oChild   TreeData   Objet fils � ajouter
   */
  function addChildren($oChild)
  {
    $this->tabChildren[] =& $oChild;
  }  
  
  /**
   *  Retire un fils au noeud courant
   * @param oChild   TreeData   Objet fils � retirer
   */
  function removeChildren($oChild)
  {
    $tmp = $this->tabChildren;
    foreach ($this->tabChildren as $iChild=>$oChildren){
      if ( $oChildren->equals($oChild) ){
        unset($tmp[$iChild]);
      }
    }
    $this->tabChildren = $tmp;
  }  
  
  /**
   *  S�lectionne le noeud courant
   */
  function selectNode()
  {
    $this->bSelected = true;
  }
  /**
   *  D�s�lectionne le noeud courant
   */
  function unselectNode()
  {
    $this->bSelected = false;
  }
  /**
   *  Retourne le contenu html du noeud
   *        Si la cat�gorie est stricte, on n'affiche le noeud que s'il poss�de des fils
   *        Si aucun template n'est d�fini sur la cat�gorie, on cr�e un noeud dans un  
   *        arbre Javascript (@see /lib/lib_arbre.js)
   *        Sinon, l'affichage est celui d�termin� par le template de cat�gorie
   * @return string html
   */
  function getHtml()
  {
    
    $strHtml = "";
    if ( !is_null($this->oCategorie) && $this->oCategorie->isStrict() ){
      if ( empty($this->tabChildren) ){
        if ( !is_null($this->oParent) ){
          $this->oParent->removeChildren($this);
        }
        return "";
      }
    }
    if ( !is_null($this->oParent) ){
      if ( is_null($this->oCategorie) || $this->oCategorie->template==""){
        $strData = "";
        foreach ($this->tabData as $oData){
          $strGlue = ($strData=="" ? "" : " ");
          if (is_object($oData) ){//&& $oData->isTypeOf(self::ALK_CLASS_HTMLPANEL)){
            $strData .= $strGlue.$oData->getHtml();
          }
          else {
            $strData .= $strGlue.$oData;          
          }
        }
        $strJs = 
          " \n var oNode = obj_".$this->oTree->name.".AddFolder(\"_".$this->node_id."\"" .
            ", \"_".(is_null($this->oParent) ? "-1" : $this->oParent->node_id)."\"" .
            ", \"" .($this->bSelected ? "<b>" : "").$strData .($this->bSelected ? "</b>" : "")."\"".
            ", ".$this->node_niveau.
            ", ".(array_key_exists("iDroitLink", $this->tabActions) ? $this->tabActions["iDroitLink"] : "0").
            ", ".(array_key_exists("iDroitAction", $this->tabActions) ? $this->tabActions["iDroitAction"] : "0").
            ", \"".(array_key_exists(ALK_TREEACTION_URL, $this->tabActions) ? $this->tabActions[ALK_TREEACTION_URL] : "")."\"".
            ", \"\"".
            ");";
        if ( !is_null($this->oCategorie) && $this->oCategorie->getProperty("icone")!="" ){
          $strJs .= 
          " \n oNode.SetIcone('".($this->icone!="" ? $this->icone : $this->oCategorie->getProperty("icone"))."');";
        }
        $this->addJs($strJs);
      }
      else if ( !is_null($this->oCategorie) ){
        global $oSpace;
        $this->oTemplate =& $oSpace->oTemplate;
        $strChildren = ""; 
        foreach ($this->tabChildren as $oChild){
          $strChildren .= $oChild->getHtml();
        } 
        $this->oTemplate->assign_by_ref("oNodeObj", $this);
        $this->oTemplate->assign("oNode", $this->tabData);
        $this->oTemplate->assign("strChildren", $strChildren);
        $strHtml .= $this->oTemplate->fetch($this->oCategorie->template); 
           
      }
    }
    if ( !isset($strChildren) ){
      foreach ($this->tabChildren as $oChild){
        $strHtml .= $oChild->getHtml();
      }
    }
    return $strHtml;
  }
  
  /**
   *  Retourne le prochain identifiant d'un fils de l'arbre
   * @return string
   */
  function getNextNodeId()
  {
    return ($this->node_id!="" ? $this->node_id."_" : "").count($this->tabChildren);
  }
}

$iActions = 0;
define("ALK_TREEACTION_URL", ++$iActions);
define("ALK_TREEACTION_RADIO", ++$iActions);
define("ALK_TREEACTION_CHECK", ++$iActions);
define("ALK_TREEACTION_CUT", ++$iActions);
define("ALK_TREEACTION_COPY", ++$iActions);
define("ALK_TREEACTION_PASTE", ++$iActions);

define("ALK_TREEACTIONPOS_ALL", ++$iActions);
define("ALK_TREEACTIONPOS_BEFORE", ++$iActions);
define("ALK_TREEACTIONPOS_AFTER", ++$iActions);

/**
 * @file alkhtmltreetable.class.php
 * @Class TreeCategorie
 * @package Alkanet_Class_Form 
 *  Cat�gorie d'affichage et de gestion de noeuds d'un arbre AlkHtmlTreeTable
 */
class TreeCategorie extends AlkObject
{
  var $bStrict;
  
  var $tabData;
  
  var $name;
  
  var $icone;
  
  var $template;
  
  var $tabActions;
  
  /**
   *  Constructeur
   * @param name      Nom de la cat�gorie
   * @param icone     Icone repr�sentant la cat�gorie ("" par d�faut)
   * @param template  Template repr�sentant la cat�gorie ("" par d�faut)
   */
  function TreeCategorie($name, $icone="", $template="")
  {
    $this->name = $name;
    $this->icone = $icone;
    $this->template = $template;
    
    $this->bStrict = false;
    $this->tabActions = array(ALK_TREEACTIONPOS_ALL=>array(), ALK_TREEACTIONPOS_BEFORE=>array(), ALK_TREEACTIONPOS_AFTER=>array(), );
    $this->tabData = array();
  }
  /**
   *  Ajoute un noeud dans la cat�gorie
   * @param oData TreeData   noeud � ajouter
   */
  function addData($oData)
  {
    $this->tabData[] = $oData;
  }
  /**
   *  Ajoute une action pour les noeuds dans la cat�gorie
   * @param iAction     Type d'action (ALK_TREEACTION_xxx)
   * @param nameAction  Nom de l'action
   * @param actionPos   Position de l'action vis � vis du noeud (ALK_TREEACTIONPOS _ALL=le noeud, _BEFORE=Avant le noeud, _AFTER=Apr�s le noeud)
   */
  function addAction($iAction, $nameAction, $actionPos=ALK_TREEACTIONPOS_ALL)
  {
    $this->tabActions[$actionPos][$iAction] = $nameAction;
  }
  
  /**
   *  Retourne le prochain identifiant d'un noeud de cette cat�gorie
   * @return string
   */
  function getNextNodeId()
  {
    return ($this->name!="" ? $this->name."_" : "").count($this->tabData);
  }
  
  /**
   *  Indique si la cat�gorie est stricte (noeuds fils obligatoires)
   * @return boolean
   */
  function isStrict()
  {
    return $this->bStrict;
  }
  
  /**
   *  Caract�rise la cat�gorie en cat�gorie stricte (noeuds fils obligatoires)
   * @param bStrict boolean
   */
  function setIsStrict($bStrict)
  {
    $this->bStrict = $bStrict;
  }
}
?>