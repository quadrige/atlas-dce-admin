<?php
include_once("htmlbase.class.php");

/**
 * @brief D�finition des constantes de type controle
 *
 * memo   : champ texte alphanum�rique pour un texte long (avec min et max sur la longueur du texte requis)
 * text   : champ texte alphanum�rique pour un texte <255 caract�res (avec min et max sur la longueur du texte requis)
 * int    : champ texte v�rifiant le bon format d'un nombre entier ou r�el, n�gatif ou positif (avec min et max sur la valeur)
 * date10 : champ texte v�rifiant le bon format de la date JJ/MM/AAAA (avec min et le max sur la valeur)
 * radio  : pas de v�rif
 * check  : pas de v�rif
 * select : pas de v�rif
 * mail   : champ texte v�rifiant le bon format email
 *
 * textnum     : champ texte num�rique n'acceptant que des chiffres, exemple : code postal, siret, siren... acceptant
 *               le z�ro en premier caract�re (avec min et max sur la longueur du texte requis)
 * textalpha   : champ texte alphab�tique n'acceptant que les lettres, apostrophes, tirets, 
 *               espace, exemple : Nom, pr�nom (avec min et max sur la longueur du texte requis)
 * heure245    : champ texte v�rifiant le bon format de l'heure HH24:MN (avec min et le max sur la valeur, et 0<=HH24<24)
 * heure5      : champ texte v�rifiant le bon format de l'heure HH24:MN (avec min et le max sur la valeur, et 0<=HH)
 * url         : champ texte v�rifiant une adresse internet  http, file ou ftp.
 */

/**
 * @brief Classe de base pour l'affichage d'un composant html
 */
class HtmlCtrlBase extends HtmlBase
{
  /** r�f�rence vers le block conteneur */
  var $oBlock;

  /** Mode du controle de saisie : =1 en lecture, =0 en modification */
  var $iMode; 

  /** Valeur du controle de saisie par d�faut */
  var $value;

  /** Etiquette texte associ�e au controle de saisie */
  var $label;

  /** = vrai si en lecture seul, faux par d�faut */
  var $bReadOnly;

  /** =vrai si desactiv� */
  var $bDisabled;

  /** Etiquette texte plac�e devant le controle dans la cellule du ctrl */
  var $labelBeforeCtrl;

  /** Etiquette texte plac�e apr�s le controle dans la cellule du ctrl */
  var $labelAfterCtrl;

  /** label texte contenant l'aide affich� lorsque le picto aide est actif */
  var $labelHelp = "";

  /** url de base des images */
  var $urlBaseImg;

  /** liste des commandes de validation */
  var $tabValidator;

  /** Nom des classes CSS pour respectivement : label, ctrl de saisie, picto aide */
  var $cssLabel;
  var $cssCtrl;
  var $cssLayerHelp;
  var $cssTdCtrl;
  var $cssTableCtrl;

  /** Si vrai, �criture de l'attribut id sur l'input */
  var $bWriteId;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode    Mode du controle de saisie : =0 modif, =1 lecture
   * @param name  Nom du controle de saisie
   * @param value Valeur par d�faut du controle de saisie
   * @param label Etiquette texte associ�e au controle de saisie
   */
  function HtmlCtrlBase($iMode, $name, $value="", $label="")
  {
    parent::HtmlBase($name);

    $this->iMode        = $iMode;
    $this->name         = $name;
    $this->value        = $value;
    $this->label        = $label;
    $this->tabValidator = array();
    $this->bDisabled    = false;
    $this->bReadOnly    = false;
    $this->oBlock       = null;
    $this->bWriteId = false;

    $this->labelBeforeCtrl = "";
    $this->labelAfterCtrl  = "";
    $this->labelHelp       = "";

    $this->urlBaseImg = "";
    if( defined("ALK_URL_SI_IMAGES") ==true ) {
      $this->urlBaseImg = ALK_URL_SI_IMAGES;
    }

    $this->cssLabel     = "formLabel";
    $this->cssCtrl      = "formCtrl";
    $this->cssLayerHelp = "formLayerHelp";
    $this->cssTdCtrl    = "formTdCtrl";
    $this->cssTableCtrl = "formTableCtrl";
  }

  /**
   * @brief Ajoute une v�rification sur un champ
   * 
   * @param strFormName nom du formulaire
   * @param strType     type du controle de saisie : 'memo', 'text', 'int', 'date10', 'radio', 'check', 'select', 'mail', 'checkgroup',...
   * @param bRequire    =true si champ obligatoire, =false sinon
   * @param oMin        valeur minimale requise pour le controle (ou longueur min en fonction de strType)
   * @param oMax        valeur maximale requise pour le controle (ou longueur max en fonction de strType)
   * @param strExcept  valeurx qui font exceptions pour la v�rification (s�par�es par "|" si plusieurs)
   * @param bStrict    true si la comparaison entre min et max est stricte, faux si �ventuellement �gale (=true par d�faut)
   * @param iSheet     indice du layer htmlFormSheet (=-1 par d�faut)
   */
  function AddValidator($strFormName, $strType, $bRequire, $oMin="", $oMax="", $strMsgErr="", $strExcept="", $bStrict=true, $iSheet="-1")
  {
    $this->tabValidator["form"] = $strFormName;
    $this->tabValidator["type"] = $strType;
    $this->tabValidator["require"] = $bRequire;
    $this->tabValidator["min"] = $oMin;
    $this->tabValidator["max"] = $oMax;
    $this->tabValidator["msgerr"] = $strMsgErr;
    $this->tabValidator["except"] = $strExcept;
    $this->tabValidator["strict"] = $bStrict;
    $this->tabValidator["iSheet"] = $iSheet;

    // place une �toile apr�s le label si obligatoire
    if( $bRequire == true && $this->iMode=="0" ) {
      $this->label .= "*";
    }
  }

  /**
   * @brief Met � jour iMode et �ventuellement le label si ctrl obligatoire
   *
   * @param iMode  Nouveau Mode
   */
  function setMode($iMode)
  {
    $aIMode = $this->iMode;
    $this->iMode = ($aIMode==1 ? $this->iMode : $iMode);
    if( count($this->tabValidator)>0 && $this->tabValidator["require"]==true ) {
      if( $aIMode==0 && $this->iMode==1 && $this->label!="" && substr($this->label, -1, 1)=="*" )
        $this->label = substr($this->label, 0, -1);
      elseif( $aIMode==1 && $this->iMode==0 )
        $this->label .= "*";
    }
  }

  /**
   * @brief Retourne le code html li� au bouton d'aide pr�sent apr�s le controle et le labelBeforeCtrl
   *        Le code js est automatiquement charg� par la classe htmlForm
   *        Retourne un tableau � 2 �l�ments indic� :
   *         btHelp    => code html du bouton
   *         layerHelp => code html du layer affichant l'aide
   *
   * @return Retourne un tableau
   */
  function GetTabHtmlHelp()
  {
    global $__iNumImgHelp__;

    $strBtHelp = "";
    $strLayerHelp = "";
    if( $this->labelHelp != "" ) {
      $__iNumImgHelp__ = ( !isset($__iNumImgHelp__) ? 0 : $__iNumImgHelp__+1 );
      $iWidth = ( $this->oBlock != null ? $this->oBlock->iWidthCtrl : 400 );
      $strBtHelp = "&nbsp;<a href=\"javascript:HelpShowHideLayer('help', '".$__iNumImgHelp__."', '".$iWidth."')\">".
        "<img name='imghelp".$__iNumImgHelp__."' border='0' src='".$this->urlBaseImg."help.gif'></a>";
      $strLayerHelp = "<div id='help".$__iNumImgHelp__."' class='".$this->cssLayerHelp."'>".$this->labelHelp."</div>";
    }
    return array("btHelp" => $strBtHelp, "layerHelp"  => $strLayerHelp);
  }

  /**
   * @brief Genere puis retourne tous les controles de saisie
   *
   * @return Retourne un string
   */
  function GetHtmlValidator()
  {
    $nb = count($this->tabValidator);
    if( $nb == 0 ) return "";
    
    $strHtml = "<script language='javascript'>";
    if( $this->bMultiLangue == true ) {
      reset($this->tabLangue);
      while( list($key, $tabLg) = each($this->tabLangue) ) {
        $strName = $this->name.$this->tabLangue[$key]["bdd"];
        $strHtml .= " AlkAddCtrl(\"".$this->tabValidator['form']."\"".
          ", \"".$strName."\"".
          ", \"".$this->tabValidator['type']."\"".
          ", ".($this->tabValidator['require']==true ? "true" : "false").
          ", \"".html_entity_decode($this->label)."\"".
          ", \"".$this->tabValidator['min']."\"".
          ", \"".$this->tabValidator['max']."\"".
          ", \"".html_entity_decode($this->tabValidator['msgerr'])."\"".
          ", \"".html_entity_decode($this->tabValidator['except'])."\"".
          ", ".($this->tabValidator['strict']==true ? "true" : "false").
          ", \"".$this->tabValidator["iSheet"]."\"".
          "); ";
        if( $this->bShowMultiLangue == false ) 
          break;
      }
    } else {
      $strHtml .= " AlkAddCtrl(\"".$this->tabValidator['form']."\"".
        ", \"".$this->name."\"".
        ", \"".$this->tabValidator['type']."\"".
        ", ".($this->tabValidator['require']==true ? "true" : "false").
        ", \"".html_entity_decode($this->label)."\"".
        ", \"".$this->tabValidator['min']."\"".
        ", \"".$this->tabValidator['max']."\"".
        ", \"".html_entity_decode($this->tabValidator['msgerr'])."\"".
        ", \"".html_entity_decode($this->tabValidator['except'])."\"".
        ", ".($this->tabValidator['strict']==true ? "true" : "false").
        ", \"".$this->tabValidator["iSheet"]."\"".
        ");";
    }
    $strHtml .= "</script>";
    return $strHtml;
  }
  
  /**
   * @brief M�thode abstraite non impl�ment�e
   *        qui se charge de retourner le code html du ctrl de saisie seul
   *        M�thode appel�e par GetHtml qui se chargera d'ajouter le code JS
   *        les textes before et after, les drapeaux, le bouton d'aide
   */
  function GetCtrlHtml()
  {
    return "";
  }

  /**
   * @brief M�thode virtuelle. Retourne le code html du ctrl :
   *        si iMode = 0 : dans un tableau |textBefore |Ctrl|textAfter|imgFlag|btHelp|
   *        si iMode = 1 : retourne le
   *
   * @return Retourne une cha�ne vide
   */
  function GetHtml()
  {
    $strCtrlHtml = $this->GetCtrlHtml();
    if ( $this->bWriteId ){
      $strCtrlHtml = preg_replace("!name=\"([^\"]+)\"!", "id=\"$1\" name=\"$1\"", $strCtrlHtml);
    }
    $tabHelp = $this->GetTabHtmlHelp();

    $strHtml = "";

    if( $this->iMode == "0" ) {
      if( $tabHelp["btHelp"] == "" && $this->labelBeforeCtrl == "" && $this->labelAfterCtrl == "" ) {
        $strHtml = $strCtrlHtml;
      } else {
        $strHtml = "<table class='".$this->cssTableCtrl."'><tr>".
          ( $this->labelBeforeCtrl != ""
            ? "<td class='".$this->cssTdCtrl."'><span class='".$this->cssText."'>".$this->labelBeforeCtrl."</span></td>"
            : "" ).
          "<td class='".$this->cssTdCtrl."'>".$strCtrlHtml."</td>".
          ( $this->labelAfterCtrl != ""
            ? "<td class='".$this->cssTdCtrl."'><span class='".$this->cssText."'>".$this->labelAfterCtrl."</span></td>"
            : "" ).
          ( $tabHelp["btHelp"] != "" ? "<td class='".$this->cssTdCtrl."'>".$tabHelp["btHelp"]."</td>" : "" ).
          "</tr>".
          "</table>".
          ( $tabHelp["layerHelp"] != "" ? $tabHelp["layerHelp"] : "" );
      }
      $strHtml .= $this->GetHtmlValidator();
    } else {
      $strHtml = $strCtrlHtml;
    }

    return $strHtml;
  }
}

?>