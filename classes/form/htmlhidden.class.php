<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief Classe d'affichage d'un composant de saisie cach�
 *        g�n�re : <input type=hidden>
 */
class HtmlHidden extends HtmlCtrlBase
{
  /** Ensemble des valeurs cach�es d'un formulaire html */
  var $tabKeyValue;
  /**
   * @brief Constructeur par d�faut
   *
   * @param strName  Nom du controle de saisie
   * @param strValue Valeur par d�faut du controle de saisie
   */
  function HtmlHidden($name, $value="")
  {
    parent::HtmlCtrlBase(0, $name, $value, "");
    $this->tabKeyValue = array();
    $this->AddHidden($name, $value);
  }

  /**
   * @brief Ajoute dans la liste, un couple (Variable, valeur)
   *
   * @param name  Nom du controle de saisie
   * @param value Valeur par d�faut du controle de saisie
   */
  function AddHidden($name, $value="")
  {
    $this->tabKeyValue = array_merge($this->tabKeyValue, array($name => $value));
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetCtrlHtml()
  {
    $strHtml = "";
    foreach($this->tabKeyValue as $strName => $strValue) {
      $strValue = htmlspecialchars($strValue);
      $strHtml .= "<input type=\"hidden\"".($this->bWriteId ? " id=\"".$strName."\"" : "")." name=\"".$strName."\" value=\"".$strValue."\">";
    }
    return $strHtml;
  }
}

?>