<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief Classe d'affichage d'un composant de saisie combobox ou liste de selection
 *        g�n�re : <input type=checkbox>
 */
class HtmlSelect extends HtmlCtrlBase
{
  /** hauteur de la liste s�lection ou =1 pour un combobox */
  var $size;

  /** largeur en pixels du controle */
  var $widthSelect;
  var $widthOption;

  /** association (valeur par d�faut, texte) inscrit en premier dans la liste */
  var $tabValTxtDefault;

  /** association (valeur par d�faut, texte) inscrit en dernier dans la liste */
  var $tabValTxtLast;

  /** liste des couples (Valeur, Texte) */
  var $tabValTxt;

  /** object contenant la liste des couples (texte, valeur) contenant un iterateur getRowIter() */
  var $oValTxt;

  /** =vrai si selection multiple, =faux par d�faut */
  var $bMultiple;

  /** =vrai si suppression automatique du premier caract�re pour les cl�s num�riques contenus dans les tableaux */
  /** utile car php ne maintient pas les cl�s num�riques pour un tableau associatif */
  var $bSuppFirstCharId;
  
  /** vrai : inverse les indices des champs servant � remplir les options � la lecture d'un dataset : 0 puis 1 au lieu de 1 puis 0 (situation par d�faut)*/
  var $bInverseFieldsOptions = false;
  
  /** Nom des colonnes du dataset servant respectivement � remplir les attributs value et text des options du select */
  var $strFieldValue = null;
  var $strFieldName = null;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode       Mode du controle de saisie : =0 modif, =1 lecture
   * @param name        Nom du controle de saisie
   * @param value       Valeur par d�faut du controle de saisie. Dans le cas d'un select multiple, value peut �tre un tableau contenant les valeurs s�lectionn�es par d�faut
   * @param label       Etiquette texte associ�e au controle de saisie
   * @param size        hauteur de la zone s�lection (=1 : comboBox, >1 : liste s�lection). =1 par d�faut
   * @param widthSelect Largeur en pixel de la saisie de s�lection
   * @param widthOption largeur en pixel de la dropdown list box
   */
  function HtmlSelect($iMode, $name, $value="", $label="" ,$size="1", $widthSelect="", $widthOption="")
  {
    parent::HtmlCtrlBase($iMode, $name, $value, $label);
    $this->size = $size;
    $this->widthSelect = $widthSelect;
    $this->widthOption = $widthOption;
    $this->bMultiple = false;
    $this->tabValTxtDefault = array();
    $this->tabValTxtLast = array();
    $this->tabValTxt = array();
    $this->oValTxt = null;
    $this->bSuppFirstCharId = false;
  }

  /**
   * @brief V�rifie si l'�l�ment de la liste est s�lectionn�
   *        Retourne true si s�lectionn�, retourne false sinon
   * 
   * @param strVal   Valeur de l'�l�ment en cours d'affichage
   * @param bDefault vrai si la valeur provient de tabValTxtDefault, false par d�faut
   * @return Retourne un booleen
   */
  function isSelected($strVal, $bDefault=false)
  {
    $bRes = false;
    if( $this->bMultiple==true && is_array($this->value) ) {
      if( $bDefault == true ) {
        $bRes = ( (count($this->value)==0) || in_array($strVal, $this->value) );
      } else {
        $bRes = in_array($strVal, $this->value);
      }
    } elseif( !is_array($this->value) ) {
      if( $bDefault == true )
        $bRes = ( ($this->value==="") || (strcmp($strVal, $this->value)==0) );
      else
        $bRes = ( strcmp($strVal, $this->value)==0 );
    }
    return $bRes;
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetCtrlHtml()
  {
    // contruit le controle
    $strHtml = "";

    $strDisabled = "";
    $strReadOnly = "";
    if( $this->bReadOnly == true ) $strReadOnly = " readonly ";
    if( $this->bDisabled == true ) $strDisabled = " disabled ";

    if( $this->iMode == "1" ) {
      $strListTxt = "";

      // affiche le ou les �l�ments s�lectionn�s � partir du tableau de valeur
      if( is_array($this->tabValTxt) && count($this->tabValTxt)>0 ) {
        $strListTxt .= $this->_getSelected($this->tabValTxt);
      } 

      // affiche le ou les �l�ments s�lectionn�s � partir du dataSet
      if( $this->oValTxt!=null &&
          is_object($this->oValTxt) && 
          method_exists($this->oValTxt, "getRowIter")==true ) {
        while( $drSelect = $this->oValTxt->getRowIter() ) {
          $strVal = $drSelect->getValueNum(1);
          $strTxt = $drSelect->getValueNum(0);
          if( $this->isSelected($strVal) ) {
            $strListTxt .= ($strListTxt!="" ? ", " : "").$strTxt;
            if( $this->bMultiple==false ) 
              // stop la boucle while
              break;
          }
        }
      }

      // mode lecture : affiche le texte associ� � la valeur par d�faut
      $strHtml = "<span class=\"".$this->cssCtrl."\">".$strListTxt."</span>";

    } else {

      // mode modif : tag select
      $strClass = "class=\"".$this->cssCtrl.rtrim($strDisabled).rtrim($strReadOnly)."\"";
      $bIE = true;
      if( $this->strNavigateur == "Netscape4" ) {
        $strClass = "";
        $bIE = false;
      }

      $strHtml = "<select ".
        $strClass.
        $strDisabled.
        " name=\"".$this->name."\"".
        ($this->bWriteId ? " id=\"".$this->name."\"" : "").
        " size=".$this->size.
        ( $this->bMultiple==true ? " multiple" : "").
        ( $this->widthSelect != "" ? " style=\"width: ".$this->widthSelect."px\"" : "").
        $this->GetHtmlEvent().
        ">";
          
      // valeur par d�faut au d�but
      if( is_array($this->tabValTxtDefault) && count($this->tabValTxtDefault)>0 ) {
        $strHtml .= "<option value=\"".$this->tabValTxtDefault[0]."\"".
          ( $this->isSelected($this->tabValTxtDefault[0], true) ? " selected" : "").
          ( $this->widthOption != "" ? " style=\"width: ".$this->widthOption."px\"" : "").
          ">".$this->tabValTxtDefault[1]."</option>";
      }

      // liste de valeurs par tableau associatif
      if( is_array($this->tabValTxt) && count($this->tabValTxt)>0 ) {
        foreach($this->tabValTxt as $strVal => $strTxt) {
          if( is_array($strTxt) ) {
            $strHtml .= $this->_getHtmlOptGroup($strVal, $strTxt);
          } else {
            $strValAff = $strVal;
            if( $this->bSuppFirstCharId == true ) $strValAff = substr($strValAff, 1);
            $strHtml .= "<option value=\"".$strValAff."\"".
              ( $this->isSelected($strValAff) ? " selected" : "").
              ( $this->widthOption != "" ? " style=\"width: ".$this->widthOption."px\"" : "").
              ">".$strTxt."</option>";
          }
        }
      }
        
      // liste de valeurs par dataSet
      if( $this->oValTxt!=null &&
          is_object($this->oValTxt) &&
          method_exists($this->oValTxt, "getRowIter")==true ) {
        $functionValue = (!is_null($this->strFieldValue) ? "getValueName" : "getValueNum");
        $functionText = (!is_null($this->strFieldName) ? "getValueName" : "getValueNum");
        
        $indiceValue = (!is_null($this->strFieldValue) ? $this->strFieldValue : ($this->bInverseFieldsOptions ? 0 : 1));
        $indiceText = (!is_null($this->strFieldName) ? $this->strFieldName : ($this->bInverseFieldsOptions ? 1 : 0));
        while( $drSelect = $this->oValTxt->getRowIter() ) {
          $strVal = $drSelect->$functionValue($indiceValue);
          $strTxt = $drSelect->$functionText($indiceText);
          $strHtml .= "<option value=\"".$strVal."\"".
            ( $this->isSelected($strVal) ? " selected" : "").
            ( $this->widthOption != "" ? " style=\"width: ".$this->widthOption."px\"" : "").
            ">".$strTxt."</option>";
        }
      }

      // Derni�re valeur
      if( is_array($this->tabValTxtLast) && count($this->tabValTxtLast)>0 ) {
        $strHtml .= "<option value=\"".$this->tabValTxtLast[0]."\"".
          ( $this->isSelected($this->tabValTxtLast[0]) ? " selected" : "").
          ( $this->widthOption != "" ? " style=\"width: ".$this->widthOption."px\"" : "").
          ">".$this->tabValTxtLast[1]."</option>";
      }
      $strHtml .= "</select>";
      $strHtml .= $this->GetHtmlValidator();
    }

    return $strHtml;
  }
  
  /**
   * @brief Cr�e une balise OPTGROUP dont le label est optGroupLabel et le contenu est tabValues={value, text}
   *        Si text est un tableau, appelle r�cursivement la fonction, sinon construit une OPTION
   * 
   * @param optGroupLabel   chaine donnant le label de l'OPTGROUP
   * @param tabValues       tableau (value => text) contenant les options associ�es � l'OPTGROUP ou les sous-OPTGROUP (si text tableau)
   * @param iDepth          profondeur de l'optgroup permettant de d�caler les sous-optgroup
   * 
   * @return code html de OPTGROUP 
   */
  function _getHtmlOptGroup($optGroupLabel, $tabValues, $iDepth=0)
  {
    $strHtml = "<optgroup label=\"".$optGroupLabel."\"".
      " class=\"".$this->cssCtrl."\"".
      " style='padding-left:".($iDepth*10)."px'>";
    foreach($tabValues as $value=>$text){
      if( is_array($text) ) {
        $strHtml .= $this->_getHtmlOptGroup($value, $text, $iDepth+1);
      } else {
        $strValAff = $value;
        if( $this->bSuppFirstCharId == true ) {
          $strValAff = substr($strValAff, 1);
        }
        $strHtml .= "<option value=\"".$strValAff."\"".
          ( $this->isSelected($strValAff) ? " selected" : "").
          ( $this->widthOption != "" ? " style=\"width: ".$this->widthOption."px\"" : "").
          ">".$text."</option>";
      }
    }
    $strHtml .= "</optgroup>";
    return $strHtml;
  }
  
  /**
   * @brief Retourne la ou les valeurs s�lectionn�es de la liste
   *        que l'on soit avec des optgroup (tableaux de tableaux) ou en fonctionnement normale
   *
   * @param tabValues  Tableaux de valeurs de la liste
   * @return Retourne un string
   */
  function _getSelected($tabValues)
  {
    $strListTxt = "";
    foreach($tabValues as $strVal=>$strTxt) {
      if( is_array($strTxt) ) {
        $selected = $this->_getSelected($strTxt);
        $strListTxt .= ( $strListTxt!="" && $selected!="" ? ", " : "" ).$selected;
      } else {
        if( $this->bSuppFirstCharId == true )
          $strVal = substr($strVal, 1);
        if( $this->isSelected($strVal) ) {
          $strListTxt .= ($strListTxt!="" ? ", " : "").$strTxt;
          if( $this->bMultiple==false ) 
            // stop la boucle while
            break;
        }
      }
    }
    return $strListTxt;
  }
  
}

?>