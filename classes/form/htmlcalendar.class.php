<?php
include_once("htmlpanel.class.php");

if( !defined("ALK_MODE_JOUR_CAL") )      define("ALK_MODE_JOUR_CAL",      1);
if( !defined("ALK_MODE_HEBDO_CAL") )     define("ALK_MODE_HEBDO_CAL",     2);
if( !defined("ALK_MODE_MOIS_CAL") )      define("ALK_MODE_MOIS_CAL",      3);
if( !defined("ALK_MODE_TRIMESTRE_CAL") ) define("ALK_MODE_TRIMESTRE_CAL", 4);
if( !defined("ALK_MODE_SEMESTRE_CAL") )  define("ALK_MODE_SEMESTRE_CAL",  5);
if( !defined("ALK_MODE_ANNUEL_CAL") )    define("ALK_MODE_ANNUEL_CAL",    6);

/**
 * @brief classe d'affichage d'un calendier
 */
class HtmlCalendar extends HtmlPanel
{
  /** mode du calendrier 
   *  ALK_MODE_JOUR_CAL  : journalier     
   *  ALK_MODE_HEBDO_CAL : hebdomadaire
   *  ALK_MODE_MOIS_CAL  : mensuel
   *  ALK_MODE_ANNUEL_CAL: annuel          */
  var $iMode;
  
  /** param�trage du calendrier */
  var $iHeureDebut;             // heure de d�but pour affichage journalier ou hebdomadaire 0 � 24 heure 
  var $iHeureFin;               // heure de fin pour affichage journalier ou hebdomadaire 0 � 24 heure
  var $iPrecision;              // d�coupage de l'heure en d�cimal : 25, 50 (par d�faut) ou 100
  var $iPrecisionCell;          // d�coupage de l'heure pour l'affichage en d�cimal : 25, 50 (par d�faut) ou 100
  var $iNbJourVisible;          // nombre de jours visibles (� partir du lundi)
  var $iNbMoisVisible;          // nombre de mois visibles (� partir du mois courant) pour affichage annuel
  var $bHeureVisible;           // affichage de la colonne gauche des heures pour affichage journalier ou hebdomadaire 0 � 24 heure
  var $bSeparateurDHeure;       // Autorise l'affichage des heures de type hh:00 avec un style diff�rent : classe CSS + " CalendarSep"
  var $bFerieHorsBorne;         // Affichage des jours hors bornes comme des jours f�ri�s 
  var $bChangeCssToday;         // Affichage du jour courant diff�remment 
  var $iCalWidth;               // largeur du calendrier
  var $iCalCellHeight;          // hauteur d'une cellule
  var $iCalCellSpacing;         // cellspacing des cellules
  var $bAffSelectMode;          // affichage de la s�lection de mode d'affichage
  var $strCalTitre;             // Titre � ajouter au calendrier
  var $bAffTitre;               // affichage du titre
  var $bAffTitreAuto;           // affichage du titre automatique
  var $bAffMoisAnnee;           // affichage de l'annee dans l'entete du calendrier
  var $bAffJourMois;            // affichage du mois dans l'entete du calendrier
  var $bAffJourSemaine;         // affichage du num�ro de jour dans l'entete du calendrier
  var $bLinkJourSemaine;        // date dans entete cliquable
  var $bAffHeureInterval;       // affichage la valeur des minutes sur l'intervalle d'une heure
  var $bAffBarreMenu;           // affichage de la barre de menu permettant � l'utilisateur de chosir son mode d'affichage
  var $strCalAlign;             // alignement du calendrier
  var $strUrlMenuImage;         // url des images de la barre de menu
  var $tabJrFeries;             // tableau associatif contenant les jours f�ri�s (cl� = JJ_MM_AAAA) => true si jour f�ri�
  
  /** param�trage des �venements */
  var $bDesccInLayer;           // affichage du dec court dans un layer
  var $bDesccVisible;           // affichage du dec court
  var $bDesclInLayer;           // affichage du dec long dans un layer
  var $bDesclVisible;           // affichage du dec long
  var $bImgVisibleInLayer;      // affichage de l'image sur le layer
  var $bHeureEvtVisible;        // affichage des heures de l'�venement
  var $iWidthDescLayer;         // largeur des layers des descriptifs
  var $iHeightDescLayer;        // hauteur des layers des descriptifs
  var $strBgColorDescLayer;     // couleur de fond des layers des descriptifs
  
  var $iJourCur;                // jour courant
  var $iMoisCur;                // mois courant
  var $iAnneeCur;               // ann�e courante
  
  var $iJourMin;                // date minimale du calendrier : valeur du jour (prise en compte si <>-1)
  var $iMoisMin;                // date minimale du calendrier : valeur du mois  (prise en compte si <>-1)
  var $iAnneeMin;               // date minimale du calendrier : valeur de l'ann�e (prise en compte si <>-1)
  
  var $iJourMax;                // date maximale du calendrier : valeur du jour (prise en compte si <>-1)
  var $iMoisMax;                // date maximale du calendrier : valeur du mois (prise en compte si <>-1)
  var $iAnneeMax;               // date maximale du calendrier : valeur de l'ann�e (prise en compte si <>-1)
  
  var $tabCalEvt;               // tableau contenant les �venements � afficher
  var $tabCSSCell;              // tableau contenant la classe CSS des cellules d'un intervalle de temps
  
  /** classe css sur les cellules du calendrier */
  var $cssCalCell;              // style de la cellule
  var $cssCalCellRoll;          // style de la cellule roll�e
  var $cssCalCellEntete;        // style des cellules d'entete
  var $cssCalCellPagination;    // style des cellules de la pagination
  var $cssCalCellPaginationLien;// styles du lien de la pagination
  var $cssCalCellDescc;         // style du desc court
  var $cssCalCellDescl;        // style du desc long
  var $cssCalCellNull;          // style des cellules n'existant pas dans le mois
  var $cssCalCellCur;           // style de la cellule du jour en cours
  var $cssCalCellFerie;         // style de cellule jour f�ri�
  var $cssCalCellEvtHeure;      // style de l'heure de l'�venement
  var $cssCalCellJour;          // style du jour dans la cellule
  var $cssCalTextEvt;           // style du texte de l'�venement
  var $cssCalCellTitre;         // style de la cellule de titre 
  var $cssCalTitre;             // style du titre

  
  /** url de pagination */
  var $strUrlPagine;            
  
  /** vrai si la cellule du calendrier est cliquable : 
   *  si oui, impl�menter la fonction js onClickCalCell[appli->name]()
   *  avec les param�tres (day, month, year, hour, minute)
   */
  var $bOnClickCell;
  
  /** vrai si seules les cellules post�rieures � la date courante sont cliquables
   */
  var $bOnClickOnlyAfter;

  /** info bulle sur le lien onClickCalCell[appli->name]() */
  var $titleOnClickCell;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode     Mode du calendrier : ALK_MODE_JOUR_CAL, ALK_MODE_HEBDO_CAL, ALK_MODE_MOIS_CAL, ALK_MODE_ANNUEL_CAL
   * @param name      Nom du calendrier
   * @param iJourCur  jour courant (1� 31)
   * @param iMoisCur  mois courant 
   * @param iAnneeCur ann�e courante
   * @param strUrlPagine 
   * @param iCalWidth
   * @param iCalCellHeight
   */
  function HtmlCalendar($iMode, $strName, $iJourCur, $iMoisCur, $iAnneeCur, $strUrlPagine, $iCalWidth=650, $iCalCellHeight=50)
  {
    parent::HtmlPanel($strName);

    $this->iMode     = $iMode;
    
    $this->iHeureDebut = 800; 
    $this->iHeureFin = 2000;
    $this->iPrecision = 50;
    $this->iPrecisionCell = 50;
    $this->iNbJourVisible = 7;
    $this->iNbMoisVisible = 3;
    $this->iCalWidth = $iCalWidth;
    $this->iCalCellHeight = $iCalCellHeight; 
    $this->iCalCellSpacing = 2;
    $this->bAffSelectMode = false;
    $this->strCalTitre = "";
    $this->bHeureVisible = true;
    $this->bAffBarreMenu = false;
    $this->strCalAlign = "center";
    $this->bAffTitre = true;
    $this->bAffTitreAuto = true;
    $this->bAffMoisAnnee = true;
    $this->bAffJourMois = true;
    $this->bAffJourSemaine = true;
    $this->bLinkJourSemaine = true;
    $this->bChangeCssToday = true;
    $this->bAffHeureInterval = false;
    $this->bSeparateurDHeure = false;
    $this->bFerieHorsBorne = false;
    $this->strUrlMenuImage = ALK_URL_SI_IMAGES;
    $this->tabJrFeries = array();
    
    $this->bHeureEvtVisible = false;
    $this->bDesccInLayer = true;
    $this->bDesccVisible = true;
    $this->bDesclInLayer = false;
    $this->bDesclVisible = false;
    $this->bImgVisibleInLayer = false;
    $this->iWidthDescLayer = 250;
    $this->iHeightDescLayer = 100;
    
    $this->iJourCur = $iJourCur;
    $this->iMoisCur = $iMoisCur;
    $this->iAnneeCur = $iAnneeCur;
    
    $this->iJourMin = -1;
    $this->iMoisMin = -1;
    $this->iAnneeMin = -1;
    
    $this->iJourMax = -1;
    $this->iMoisMax = -1;
    $this->iAnneeMax = -1;
    
    $this->strUrlPagine  = $strUrlPagine;

    $this->bOnClickCell = false;
    $this->bOnClickOnlyAfter = false;
    $this->titleOnClickCell = "";

    $this->bTemplate = false;

    $this->tabCalEvt = array();
    $this->tabCSSCell = array();
    $this->tabVarJs["tabEvt"] = "new Array()";
    $this->tabSrcJs[] = "../../lib/lib_layerjs.php";
    $this->tabSrcJs[] = "../../lib/lib_layerdescjs.php";

    $this->cssCalCell = "tdPair1";
    $this->cssCalCellRoll = "tdPair2";
    $this->cssCalCellEntete = "tdEntete1";
    $this->cssCalCellPagination = "tdEntete1";
    $this->cssCalCellPaginationLien = "divTabEntete1";
    $this->cssCalCellDescc = "divContenuTexte";
    $this->cssCalCellDescl = "divContenuTexte";
    $this->cssCalCellCur = "tdPair2";
    $this->cssCalCellFerie = "tdImpair1";
    $this->cssCalCellNull = "tdImpair1";
    $this->cssCalCellEvtHeure = "divContenuConseil";
    $this->cssCalCellJour = "divTabEntete1";
    $this->cssCalTextEvt = "divContenuTexte";
    $this->cssCalCellTitre = "tdEntete1";
    $this->cssCalTitre = "formBlockTitle";
    
    $this->strBgColorDescLayer = "#ffffe0";
    
    switch ($this->iMode){
      case ALK_MODE_TRIMESTRE_CAL : $this->iNbMoisVisible = 3;  break;
      case ALK_MODE_SEMESTRE_CAL  : $this->iNbMoisVisible = 6;  break;
      case ALK_MODE_ANNUEL_CAL    : $this->iNbMoisVisible = 12; break;
    }
  }

  /**
   * @brief Retourne les dates de d�but et de fin affich�s sur le calendrier
   *        Permet de filtrer les �venements avant de les ajouter au calendrier
   *        Retourne le r�sultat dans un tableau de type ("start" => timestamp, "end" => timestamp)
   *
   * @return Retourne un array
   */
  function GetLimitCalendar()
  {
    $hStart = floor($this->iHeureDebut/100);
    $mStart = ($this->iHeureDebut-($hStart*100))*60/100;
    $hEnd   = floor($this->iHeureFin/100);
    $mEnd   = ($this->iHeureFin-($hEnd*100))*60/100;

    // ALK_MODE_JOUR_CAL par d�faut
    $dStart = mktime($hStart, $mStart, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur);
    $dEnd = mktime($hEnd, $mEnd, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur);
    switch( $this->iMode ) {
    case ALK_MODE_HEBDO_CAL:
      $iNumDay = date("w", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
      if( $iNumDay == 0 ) $iNumDay = 7;
      $iFirstDay = $this->iJourCur - ($iNumDay-1);
      $dStart = mktime($hStart, $mStart, 0, $this->iMoisCur, $iFirstDay, $this->iAnneeCur);
      $dEnd = mktime($hEnd, $mEnd, 0, $this->iMoisCur, $iFirstDay+6, $this->iAnneeCur);
      break;
      
    case ALK_MODE_MOIS_CAL:
      $dStart = mktime($hStart, $mStart, 0, $this->iMoisCur, 1, $this->iAnneeCur);
      $dEnd = mktime($hEnd, $mEnd, 0, $this->iMoisCur, date("t", mktime(0, 0, 0, $this->iMoisCur, 1, $this->iAnneeCur)), $this->iAnneeCur);
      break;
      
    case  ALK_MODE_TRIMESTRE_CAL:
    case  ALK_MODE_SEMESTRE_CAL:
    case  ALK_MODE_ANNUEL_CAL:
      switch ($this->iMode){
        case ALK_MODE_TRIMESTRE_CAL : $this->iNbMoisVisible = 3;  break;
        case ALK_MODE_SEMESTRE_CAL  : $this->iNbMoisVisible = 6;  break;
        case ALK_MODE_ANNUEL_CAL    : $this->iNbMoisVisible = 12; break;
      }
      $mStart = $this->iMoisCur;
      $mEnd = $this->iMoisCur+$this->iNbMoisVisible-1;

      $dStart = mktime($hStart, $mStart, 0, $this->iMoisCur, 1, $this->iAnneeCur);
      $dEnd = mktime($hEnd, $mEnd, 0, $mEnd, date("t", mktime(0, 0, 0, $mEnd, 1, $this->iAnneeCur)), $this->iAnneeCur);
      break;
    }
    return array("start" => $dStart, "end" => $dEnd);
  }
  
  /**
   * @brief Evalue si la date de destination est bien dans l'intervalle du calendrier si celui-ci est donn�
   * @param iJourDest   jour de la date de destination
   * @param iMoisDest   mois de la date de destination
   * @param iAnneeDest  ann�e de la date de destination
   * @param bIncrement  si true, comparaison avec la date maximale, si false, comparaison avec la date minimale
   * @return boolean  vrai si la date de destination est dans l'intervalle (inclus) ou si pas d'intervalle donn� (valeurs � -1)
   */
  function CanPagineTo($iJourDest, $iMoisDest, $iAnneeDest, $bIncrement)
  {
    if ( $bIncrement ){
      if ( !($this->iJourMax!=-1 && $this->iMoisMax!=-1 && $this->iAnneeMax!=-1) ) return true;
      return strtotime($iAnneeDest."-".$iMoisDest."-".$iJourDest)<=strtotime($this->iAnneeMax."-".$this->iMoisMax."-".$this->iJourMax);
    } else {
      if ( !($this->iJourMin!=-1 && $this->iMoisMin!=-1 && $this->iAnneeMin!=-1) ) return true;
      return strtotime($iAnneeDest."-".$iMoisDest."-".$iJourDest)>=strtotime($this->iAnneeMin."-".$this->iMoisMin."-".$this->iJourMin);     
    }
  }
  
  /**
   * @brief Affecte la date minimale d'affichage du calendrier
   * @param iJourMin   jour de la date de minimale
   * @param iMoisMin   mois de la date de minimale
   * @param iAnneeMin  ann�e de la date de minimale 
   */
  function SetDateLimitMin($iJourMin, $iMoisMin, $iAnneeMin)
  {
    $this->iJourMin = $iJourMin;
    $this->iMoisMin = $iMoisMin;
    $this->iAnneeMin = $iAnneeMin;
  }
  
  /**
   * @brief Affecte la date maximale d'affichage du calendrier
   * @param iJourMax   jour de la date de maximale
   * @param iMoisMax   mois de la date de maximale
   * @param iAnneeMax  ann�e de la date de maximale 
   */
  function SetDateLimitMax($iJourMax, $iMoisMax, $iAnneeMax)
  {
    $this->iJourMax = $iJourMax;
    $this->iMoisMax = $iMoisMax;
    $this->iAnneeMax = $iAnneeMax;
  }
  /**
   * @brief Affecte la pr�cision du calendrier : 15mn, 30mn ou 1h
   *
   * @param iPrecision  intervalle de temps en minutes 
   */
  function SetPrecision($iPrecision=30)
  {
    $this->iPrecision = 50;

    if( $iPrecision == 15 )
      $this->iPrecision = 25;
    elseif( $iPrecision == 60 )
      $this->iPrecision = 100;
  }

  /**
   * @brief Affecte l'heure de d�but de journ�e
   *
   * @param strHour  Heure au format hh:mn
   */
  function SetHourStartDay($strHour)
  {
    $this->iHeureDebut = $this->_Hour2Int($strHour); 
  }

  /**
   * @brief Affecte l'heure de fin de journ�e
   *
   * @param strHour  Heure au format hh:mn
   */
  function SetHourEndDay($strHour)
  {
    $this->iHeureFin = $this->_Hour2Int($strHour);
  }

  /**
   * @brief Convertit puis retourne une heure au format entier = hh*100+(mn*100/60) interne � l'objet
   *
   * @param strHour   heure au format hh:mn
   * @return Retourne un entier
   */
  function _Hour2Int($strHour, $precision=-1)
  {
    if ( $precision==-1 )
      $precision = $this->iPrecision;
      
    $iHour = 0;
    $tabHour = explode(":", $strHour);
    if( count($tabHour) == 2 ) {
      if( $tabHour[1] % ($precision*60/100) !=0 ) {
        $iMinute = round($tabHour[1]/15)*15;
        if( $iMinute == 60 ) {
          $tabHour[1] = 0;
          $tabHour[0]++;
        } else {
          $tabHour[1] = $iMinute;
        }
      }
      $iHour = intval($tabHour[0])*100+(intval($tabHour[1])*100/60);
    }
    return $iHour;
  }

  /**
   * @brief Convertit puis retourne une heure au format hh:mn � partir d'un entier interne � l'objet
   *
   * @param iHour  entier repr�sentant une heure = hh*100+(mn*100/60)
   * @param bAll   =true pour affichage hh:mn, =false pour affichage [h]h:mn (n'affiche pas le premier caract�re z�ro de l'heure)
   * @return Retourne un string
   */
  function _Int2Hour($iHour, $bAll=false, $hour_separator=":", $minute_suffixe="")
  {
    $hh = floor($iHour/100);
    $mn = ($iHour-($hh*100))*60/100;
    
    if( $bAll==true && $hh<10 )
      $hh = "0".$hh;

    if( $mn<10 )
      $mn = "0".$mn;

    $strHour = $hh.$hour_separator.$mn.$minute_suffixe;
    return $strHour;
  }

  /**
   * @brief Retourne le timestamp d�but d'un �v�nement
   *
   * @param evt        Tableau contenant les infos d'un �v�nement
   * @param strType    type d'heure = deb ou fin
   * @return Retourne un timestamp
   */
  function _GetEvtTimestamp($evt, $strType)
  {
    $d = substr($evt["date".$strType], 0, 2);
    $m = substr($evt["date".$strType], 3, 2);
    $y = substr($evt["date".$strType], 6, 4);

    // l'heure est d�cimale
    $h = floor($evt["heure".$strType]/100);
    $mn = ($evt["heure".$strType]-($h*100))*60/100;
    return mktime($h, $mn, 0, $m, $d, $y);
  }

  /**
   * @brief Ajoute un jour f�ri� au tableau
   *
   * @param strDate  date f�ri�e au format dd/mm/yyyy
   */
  function AddIgnoreDay($strDate)
  {
    $strKey = str_replace("/", "_", $strDate);
    $this->tabJrFeries[$strKey] = true;
  }

  /**
   * @brief Retourne vrai si le jour est f�ri�, faux sinon
   *
   * @param iDay   num�ro du jour
   * @param iMonth num�ro du mois
   * @param iYear  ann�e sur 4 chiffres
   * @return Retourne un bool
   */
  function isIgnoreDay($iDay, $iMonth, $iYear)
  {
    if( strlen($iDay)<2 ) $iDay = "0".$iDay;
    if( strlen($iMonth)<2 ) $iMonth = "0".$iMonth;

    return array_key_exists($iDay."_".$iMonth."_".$iYear, $this->tabJrFeries);
  }

  /**
   * @brief Recup�re les variables post�es n�cessaires au ctrl calendar
   *
   * @param reqMethod  M�thode de r�cup�ration (REQ_POST, REQ_GET, REQ_POST_GET (par d�faut), REQ_GET_POST)
   */
  function GetRequest($reqMethod=REQ_GET)
  {
    $this->iJourCur       = Request("j", $reqMethod, $this->iJourCur, "is_numeric");
    $this->iMoisCur       = Request("m", $reqMethod, $this->iMoisCur, "is_numeric");
    $this->iAnneeCur      = Request("a", $reqMethod, $this->iAnneeCur, "is_numeric");
    $this->iMode          = Request("imodeaff", $reqMethod, $this->iMode, "is_numeric");
    switch ($this->iMode){
      case ALK_MODE_TRIMESTRE_CAL : $this->iNbMoisVisible = 3;  break;
      case ALK_MODE_SEMESTRE_CAL  : $this->iNbMoisVisible = 6;  break;
      case ALK_MODE_ANNUEL_CAL    : $this->iNbMoisVisible = 12; break;
    }
    $this->iNbMoisVisible = Request("nbmois", $reqMethod, $this->iNbMoisVisible, "is_numeric");
  }

  /**
   * @brief Retourne les param�tres n�cessaires � une url pour r�afficher le calendrier
   *        dans le m�me �tat. Ne retourne pas un & en premier caract�re.
   *
   * @return Retourne un string
   */
  function GetRequestParam()
  {
    return "imodaff=".$this->iMode."&j=".$this->iJourCur."&m=".$this->iMoisCur."&a=".$this->iAnneeCur;
  }

  /**
   * @brief Ajoute un �venement au calendrier
   *
   * @param strLien     lien de l'evt
   * @param idEnt       identifiant de l'evt
   * @param strTitre    titre
   * @param dDateDeb    date de d�but (jj/mm/aaaa)
   * @param dDateFin    date de fin   (jj/mm/aaaa)
   * @param dHeureDeb   heure de d�but (11=11:00, hh:mn)
   * @param dHeureDeb   heure de fin  (11=11:00, hh:mn)
   * @param strDescc    desc courte (= chaine vide par d�faut)     
   * @param strDescl    desc longue (= chaine vide par d�faut)
   * @param pj          pi�ce jointe (lien complet)
   * @param strcssTxt   style du titre
   * @param strBgColor  couleur de fond de l'evt
   * @param strImg      url de l'image correspondant � l'�v�nement (ratio 4x3)
   */
  function AddCalEvt($strLien, $idEnt, $strTitre, $dDateDeb, $dDateFin, $dHeureDeb, 
                     $dHeureFin, $strDescc="", $strDescl="", $pj="", $strCssTxt="", $strBgColor="", $strImg="")
  {
    if( strlen($dHeureDeb)<=2 )
      $dHeureDeb .= ":00";
    if( strlen($dHeureFin)<=2 )
      $dHeureFin .= ":00";

    if( $dHeureDeb != ":00" )
      $dHeureDeb = $this->_Hour2Int($dHeureDeb);
    else 
      $dHeureDeb = $this->iHeureDebut;
      
    if( $dHeureFin != ":00" )
      $dHeureFin = $this->_Hour2Int($dHeureFin);
    else
      $dHeureFin = $this->iHeureFin;

    $this->tabCalEvt[count($this->tabCalEvt)] = 
      array("lien"     => $strLien, 
            "id"       => $idEnt,
            "titre"    => $strTitre,
            "datedeb"  => $dDateDeb,
            "datefin"  => $dDateFin,
            "heuredeb" => $dHeureDeb, /* d�cimal */
            "heurefin" => $dHeureFin, /* d�cimal */
            "descc"    => $strDescc,
            "descl"    => $strDescl,
            "pj"       => $pj,
            "csstxt"   => $strCssTxt,
            "bgcolor"  => $strBgColor,
            "img"      => $strImg);
  }

  /**
   * @brief Ajoute un �venement au calendrier
   *
   * @param strLien     lien de l'evt
   * @param idEnt       identifiant de l'evt
   * @param strTitre    titre
   * @param dDateDeb    date de d�but (jj/mm/aaaa)
   * @param dDateFin    date de fin   (jj/mm/aaaa)
   * @param dHeureDeb   heure de d�but (11=11:00, hh:mn)
   * @param dHeureDeb   heure de fin  (11=11:00, hh:mn)
   * @param strDescc    desc courte (= chaine vide par d�faut)     
   * @param strDescl    desc longue (= chaine vide par d�faut)
   * @param pj          pi�ce jointe (lien complet)
   * @param strcssTxt   style du titre
   * @param strBgColor  couleur de fond de l'evt
   * @param strImg      url de l'image correspondant � l'�v�nement (ratio 4x3)
   */
  function SetCellCSSClass($dDateDeb, $dDateFin, $dHeureDeb, $dHeureFin, $cssClass)
  {
    if( strlen($dHeureDeb)<=2 )
      $dHeureDeb .= ":00";
    if( strlen($dHeureFin)<=2 )
      $dHeureFin .= ":00";

    if( $dHeureDeb != ":00" )
      $dHeureDeb = $this->_Hour2Int($dHeureDeb);
    else 
      $dHeureDeb = $this->iHeureDebut;
      
    if( $dHeureFin != ":00" )
      $dHeureFin = $this->_Hour2Int($dHeureFin);
    else
      $dHeureFin = $this->iHeureFin;

    $tabDateDeb = explode("/", $dDateDeb);
    $tabDateFin = explode("/", $dDateFin);
    $timeDeb = strtotime($tabDateDeb[2]."-".$tabDateDeb[1]."-".$tabDateDeb[0]);
    $timeFin = strtotime($tabDateFin[2]."-".$tabDateFin[1]."-".$tabDateFin[0]);
    for ($time=$timeDeb; $time<=$timeFin; $time=strtotime("+1 day", $time)){
      for ($heure=$dHeureDeb; $heure<$dHeureFin; $heure+=$this->iPrecisionCell){
        $this->tabCSSCell[$time][$heure] = $cssClass;
      }
    }
  }

  /**
   * @brief Retourne le nombre de jour dans un mois
   * 
   * @param iMonth  num�ro du mois (de 1 � 12)
   * @param iYear   ann�e sur 4 chiffres
   * @return Retourne un integer
   */
  function GetDaysInMonth($iMonth, $iYear) 
  {
    $tabDays = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    $iRes = ( $iMonth>0 && $iMonth<13 ? $tabDays[$iMonth - 1] : -1 );
    if( $iMonth == 2 && checkdate(2, 29, $iYear) ) 
      $iRes = 29;
    return $iRes;
  }
  
  /**
   * @brief Convertit un jour ou un mois en fran�ais
   * 
   * @param iMode   0->jour   1->mois
   * @param iValue  Valeur � traduire
   * @param bUpperCase vrai si texte en majuscule, faux texte normal (par d�faut)
   * 
   * @return la valeur traduite
   */
  function ConvertDateToFrench($iMode, $iValue, $bUpperCase=false, $bAbbrev=false)
  {
    $tabJour = array(0 => "Dimanche",
                     1 => "Lundi",
                     2 => "Mardi",
                     3 => "Mercredi",
                     4 => "Jeudi",
                     5 => "Vendredi",
                     6 => "Samedi",
                     7 => "Dimanche");

     $tabMois = array(1 => "janvier",
                      2 => "f�vrier",
                      3 => "mars",
                      4 => "avril",
                      5 => "mai",
                      6 => "juin",
                      7 => "juillet",
                      8 => "ao�t",
                      9 => "septembre",
                      10 => "octobre",
                      11 => "novembre",
                      12 => "d�cembre");
     // afin d'�viter les jours ou mois en 0x pour x<=9
     $iValue = $iValue*1;
     if( $iMode == 1 ) {
       if( $iValue>0 && $iValue<13 ) {
         $strMois = $tabMois[$iValue];
         if( $bAbbrev == true ) {
           $strMois = substr($strMois, 0, 3);
           if( $iValue == 6 ) $strMois = "jun";
           if( $iValue == 7 ) $strMois = "jul";
           $strMois .= ".";
         }
         if( $bUpperCase == true ) {
           $strMois = strtr($strMois, "��", "eu");
           $strMois = strtoupper($strMois);
         }
         return $strMois;
       }
     } else {
       if( $iValue>=0 && $iValue<8 ) {
         $strJr = ( $bAbbrev == true ? substr($tabJour[$iValue], 0, 3) : $tabJour[$iValue] );
         if( $bUpperCase == true ) {
           $strJr = strtoupper($strJr);
         }
         return $strJr;
       }
     }
     return "";
  }

  /**
   * @brief Retourne le code html du layer affich�
   * 
   * @return Retourne 
   */
  function GetHtmlCalEvtLayer($tabCalEvt)
  {
    $strDescc = str_replace(chr(13).chr(10), "", nl2br(htmlentities($tabCalEvt["descc"])));
    $strDescl = str_replace(chr(13).chr(10), "", nl2br(htmlentities($tabCalEvt["descl"])));
    $strImg = $tabCalEvt["img"];

    $strHtml = "<div>".
      ( $this->bImgVisibleInLayer==true && $strImg!=""
        ? "<img border='0' src='".$strImg."' width='60' vspace='1' hspace='2' align='left'>"
        : "" ).
      ( $this->bDesccVisible==true && $strDescc!=""
        ? "<div style='display:inline' class='".$this->cssCalCellDescc."'>".$strDescc."</div><br>"
        : "" ).
      ( $this->bDesclVisible==true && $strDescl!=""
        ? "<div style='display:inline' class='".$this->cssCalCellDescl."'>".$strDescl."</div>"
        : "" ).
      "</div>";
    
    $strHtml = addslashes($strHtml);

    return $strHtml;
  }
  
  /**
   * @brief Retourne le code html d'un �venement d'une cellule
   *
   * @param tabCalEvt tableau de l'�venement
   * @return Retourne un string
   */
  function GetHtmlCalEvt($tabCalEvt, $strDivParam="")
  {
    $strHtml = "<table width=\"100%\" cellpadding=\"1\" cellspacing=\"0\" ".
      ( $tabCalEvt["bgcolor"] != ""
        ? " bgcolor=\"".$tabCalEvt["bgcolor"]."\""
        : "" ).">".
      "<tr><td width=\"100%\">".
      "<a class=\"".( $tabCalEvt["csstxt"]=="" 
                      ? $this->cssCalTextEvt
                      : $tabCalEvt["csstxt"] )."\" ";
    
    if( ($this->bDesccVisible==true && $this->bDesccInLayer==true) || 
        ($this->bDesclVisible==true && $this->bDesclInLayer==true) ||
        $this->bImgVisibleInLayer==true ) {
      $strDivParam .= " onMouseOver=\"ShowDesc(".
        "   '".$this->GetHtmlCalEvtLayer($tabCalEvt).
        "', '".$this->strBgColorDescLayer.
        "', '".$this->strBgColorDescLayer."')\"".
        " onMouseOut=\"HideDesc()\"";
    }
    
    $strHtml .= " href=\"".$tabCalEvt["lien"]."\">".
      ( $tabCalEvt["heuredeb"]<>"00:00" && $this->bHeureEvtVisible==true
        ? "<span class=\"".$this->cssCalCellEvtHeure."\">".
          ( $tabCalEvt["datedeb"]!=$tabCalEvt["datefin"] ? $tabCalEvt["datedeb"]." " : "" ).$this->_Int2Hour($tabCalEvt["heuredeb"]).
          ( $tabCalEvt["heurefin"]!="0" 
            ? " - ".( $tabCalEvt["datedeb"]!=$tabCalEvt["datefin"] ? $tabCalEvt["datefin"]." " : "" ).$this->_Int2Hour($tabCalEvt["heurefin"])
            : "")."</span><br>"
        : "" ).
      $tabCalEvt["titre"]."</a>".
      ( $this->bDesccVisible==true && $this->bDesccInLayer==false ? "<br/>".$tabCalEvt["descc"] : "").
      ( $this->bDesclVisible==true && $this->bDesclInLayer==false ? "<br/>".$tabCalEvt["descl"] : "").
      ( $tabCalEvt["pj"] != ""
        ? "<a href=\"".$tabCalEvt["pj"]."\" target=\"_blank\">".
          "<img align=\"absmiddle\" src=\"".ALK_URL_SI_IMAGES."gen/pj.gif\" border=\"0\"></a>"
        : "").
      "</td></tr></table>";

    // 
    if( $tabCalEvt["lien"] != "" ) {
      $strDivParam .= " onclick=\"".$tabCalEvt["lien"]."\" title=".($this->strNavigateur == "IE" ? "\"\"" : "\" \"");
    }

    // ajoute le div autour du tableau
    $strHtml = "<div ".$strDivParam.">".$strHtml."</div>";

    return $strHtml;
  }
  
  /**
   * @brief Retourne le code html de l'ent�te du calendrier : 
   *        mois, semaine ou ann�e suivi des liens pour afficher les diff�rents modes
   *
   * @return Retourne un string
   */
  function GetHtmlCalMenu()
  {
    $createLink = create_function('$oCal, $name, $mode, $icon', 
      'return "<td width=\'16\'><a title=\'".$name."\' href=\"".$oCal->getUrlPagine($mode)."\">".'.
      '"<img src=\'".$oCal->strUrlMenuImage.$icon.(($oCal->iMode==$mode)?"_rol":"").".gif\' border=\'0\' alt=\'".$name."\'></a></td>"' .
      ';');
      
    $strHtml = "<table cellpadding='0' cellspacing='1' border='0' class='".$this->cssCalCell."'><tr>".
      $createLink($this, "Journalier",   ALK_MODE_JOUR_CAL,       "calendrierJ") .
      $createLink($this, "Hebdomadaire", ALK_MODE_HEBDO_CAL,      "calendrierH") .
      $createLink($this, "Mensuel",      ALK_MODE_MOIS_CAL,       "calendrierM") .
      $createLink($this, "Trimestriel",  ALK_MODE_TRIMESTRE_CAL,  "calendrierA3") .
      $createLink($this, "Semestriel",   ALK_MODE_SEMESTRE_CAL,   "calendrierA6") .
      $createLink($this, "Annuel",       ALK_MODE_ANNUEL_CAL,     "calendrierA12") .
      "</tr></table>";    
    return $strHtml;
  }

  /**
   * @brief Retourne le code html du layer affichant l'info bulle
   *
   * @return Retourne un string
   */
  function GetHtmlLayerDesc()
  {
    return "<div id=\"layerDesc\" style=\"position:absolute; width:".$this->iWidthDescLayer.
      "px; height:".$this->iHeightDescLayer."px; z-index:2; left: 0px; top: 0px; visibility: hidden\">".
      "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">".
      "<tr><td valign=\"top\"></td></tr>".
      "</table></div>";
  }

  /**
   * @brief Retourne le code html du calendrier journalier
   *
   * @return Retourne un string
   */
  function GetHtmlCalendarJournalier()
  {
    $tabIdEvt = array();
    $tabH = array();
    $tabNbH = array();

    $iBorderWidth = ( $this->strNavigateur == "IE" ? 0 : 4 );
    
    $iLargeurCell = ( $this->bHeureVisible==true 
                      ? $this->iCalWidth-40-5*$this->iCalCellSpacing
                      : $this->iCalWidth-4*$this->iCalCellSpacing );
    $iLargeurCellCal = $iLargeurCell-44-2*$this->iCalCellSpacing;
    
    $iNumJour = date("w", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
    if( $iNumJour == 0 ) 
      $iNumJour = 7;
    
    $strHtmlJs = "";
    $strHtmlLayer = "";
    
    // entete titre
    $strHtml = "<table class='calendar' cellpadding='0' cellspacing='".$this->iCalCellSpacing."' border='0' align='".$this->strCalAlign."'>".
      "<tr>".
      ( $this->bHeureVisible==true ? "<td width='40' height='0'>&nbsp;</td>" : "").
      "<td width='22' height='0'></td>".
      "<td width='".$iLargeurCellCal."'></td>".
      "<td width='22'></td>".
      "</tr>".

      ( $this->bHeureVisible==true ? "<td>&nbsp;</td>" : "").
      "<td class='".$this->cssCalCellTitre."' colspan='3'>".

      "<table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td class='".$this->cssCalTitre."'>".
      ( $this->strCalTitre!="" 
        ? $this->strCalTitre.( $this->bAffTitreAuto==true ? " - " : "" )
        : "").
      ( $this->bAffTitreAuto==true 
        ? $this->ConvertDateToFrench(1, $this->iMoisCur, true)." / ".$this->iAnneeCur
        : "").
      ( $this->bAffBarreMenu==true 
        ? "</td><td align='right'>".$this->GetHtmlCalMenu()
        : "" ).
      "<td></tr></table>".

      "</td></tr>";
    
    $strHtml .= "<tr>";
         
    $iJourPrec = date("j", strtotime("-1 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iMoisPrec = date("n", strtotime("-1 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iAnneePrec = date("Y", strtotime("-1 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    
    $iJourSuiv = date("j", strtotime("+1 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iMoisSuiv = date("n", strtotime("+1 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iAnneeSuiv = date("Y", strtotime("+1 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
         
    $iJourCur = ( $this->iJourCur[0] == "0" ? substr($this->iJourCur, 1) : $this->iJourCur);

    $bJrFerie = $this->isIgnoreDay($this->iJourCur, $this->iMoisCur, $this->iAnneeCur);

    $strFerie = "";
    /*    if( $bJrFerie == true && $iJourCur>0 && $iJourCur<6 ) {
      $strFerie = " - <b>F�ri�</b>";
    }*/

    if( $this->bHeureVisible==true )
      $strHtml .= "<td>&nbsp;</td>";
      
    $strUrlPrec = $this->getUrlPagine($this->iMode, $iJourPrec, $iMoisPrec, $iAnneePrec);
    $strUrlSuiv = $this->getUrlPagine($this->iMode, $iJourSuiv, $iMoisSuiv, $iAnneeSuiv);

    // entete calendrier
    $strHtml .= "<td align='left' class='".$this->cssCalCellEntete."'>".
      "<a href=\"".$strUrlPrec."\" title='Jour pr�c�dent' class='".$this->cssCalCellPaginationLien."'><<</a></td>".
      "<td align='center' class='".$this->cssCalCellEntete."'>".
      $this->ConvertDateToFrench(0, $iNumJour)." ".$iJourCur.( $iJourCur==1 ? "er" : "" ).
      ( $this->bAffJourMois==true 
        ? " ".$this->ConvertDateToFrench(1, date("n", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)))
        : "" ).
      ( $this->bAffMoisAnnee==true 
        ? " ".$this->iAnneeCur
        : "").$strFerie."</td>".
      "<td align='right' class=\"".$this->cssCalCellEntete."\">".
      "<a title='Jour suivant' href=\"".$strUrlSuiv."\" class=\"".$this->cssCalCellPaginationLien."\">>></a></td>".
      "</tr>";
         
    // Ajuste l'heure de d�but et de fin de journ�e en fonction de la pr�cision d'affichage.
    // Exemple : Afficher le cadre 8h pour une journ�e qui d�marre � 8h15 avec 30mn de pr�cision d'affichage.
    $hStart = ( $this->iHeureDebut % $this->iPrecisionCell == 0
                ? $this->iHeureDebut 
                :  $this->iHeureDebut - ($this->iHeureDebut % $this->iPrecisionCell) );
    $hEnd  = ( $this->iHeureFin % $this->iPrecisionCell == 0
               ? $this->iHeureFin 
               : $this->iHeureFin + ($this->iHeureFin % $this->iPrecisionCell) );
    $iRowSpanFerie = ($hEnd-$hStart)/$this->iPrecisionCell;

    for($h = $hStart; $h < $hEnd; $h += $this->iPrecision) {
      $tabNbH[$h] = 0;
      $tabH[$h] = 0;
    }
    
    // m�morise le nombre d'�venements par tranche horaire
    if( !$bJrFerie ) {
      for($h = $hStart; $h < $hEnd; $h += $this->iPrecision) {
        $hh = floor($h/100);
        $mn = (($h-($hh*100))*60)/100;
        $dateJourCal = mktime($hh, $mn, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur);
        
        foreach($this->tabCalEvt as $evt) {
          if( $evt["datedeb"] != "" )
            $dateDebutEvt = $this->_GetEvtTimestamp($evt, "deb");
          else
            $dateDebutEvt = 0;
          
          if( $evt["datefin"] != "" )
            $dateFinEvt = $this->_GetEvtTimestamp($evt, "fin");
          else
            $dateFinEvt = $dateDebutEvt;
          if( ($dateDebutEvt<=$dateJourCal && $dateJourCal<=$dateFinEvt) ||
              ($dateDebutEvt==$dateJourCal && $dateFinEvt=="") ) {
            $tabNbH[$h] = $tabNbH[$h]+1;
            $tabH[$h] = $tabH[$h]+1;
          }
        }
      }
    }

    // cr�ation des plages horaires et calcul du positionnement
    $bRowSpan = false;
    $cptLayer = 0;
    for($h = $hStart; $h < $hEnd; $h += $this->iPrecision) {
      $hh = floor($h/100);
      $mn = ($h-($hh*100))*60/100;
      $dateJourCal = mktime($hh, $mn, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur);
      $strHour = $this->_Int2Hour($h);
      
      if( $h % $this->iPrecisionCell == 0 ) {
        $strHtml .= "<tr>";
      
        if( $this->bHeureVisible==true )
          $strHtml .= "<td class=\"".$this->cssCalCellEntete."\">".
            ($mn == 0 ? $strHour : "&nbsp;" ).
            "</td>";
        
        $bOnClickCell = $this->bOnClickCell == true && ($this->bOnClickOnlyAfter == true ? $dateJourCal >= time() : true);
      
        if( !$bJrFerie ) {
          $strHtml .= "<td class='".$this->cssCalCell."' id='cell".$h."' valign='top' height='".$this->iCalCellHeight."' colspan='3'".
            " onmouseover=\"onMouseOverCalCell(this, '".$this->cssCalCellRoll."', ".($bOnClickCell == true ? "true" : "false").")\"".
            " onmouseout=\"onMouseOutCalCell(this)\"".
            ( $bOnClickCell == true
              ? " onclick=\"onClickCalCell".$this->name."(".$this->iMode.", ".$this->iJourCur.",".$this->iMoisCur.",".$this->iAnneeCur.",".$hh.",".$mn.")\""
              : "" ).
            ( $bOnClickCell == true
              ? " title=\"".$this->titleOnClickCell."\""
              : "" ).
            ">&nbsp;";
        } else {
          if( !$bRowSpan ) {
            $bRowSpan = true;
            $strHtml .= "<td class='".$this->cssCalCellFerie."' id='cell".$h."' rowspan='".$iRowSpanFerie.
              "' align='center' colspan='3'>Ferm�";
          }
        }
      }

      if( !$bJrFerie ) {
        foreach($this->tabCalEvt as $evt) {
          $dateDebutEvt = $this->_GetEvtTimestamp($evt, "deb");
          if( $evt["datefin"] != "" )
            $dateFinEvt = $this->_GetEvtTimestamp($evt, "fin");
          else
            $dateFinEvt = $dateDebutEvt;
        
          if( ( ($dateDebutEvt<=$dateJourCal && $dateJourCal<=$dateFinEvt) || 
                ($dateDebutEvt==$dateJourCal && $dateFinEvt=="") ) && in_array($evt["id"], $tabIdEvt)==false ) {

            $dateHier = strtotime("-1 day", mktime(23, 59, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
            $dateDemain = strtotime("+1 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
          
            if( $dateDebutEvt <= $dateHier ) {
              $hDebEvt = $this->iHeureDebut;
            } else {
              $hDebEvt = ( $evt["heuredeb"] < $this->iHeureDebut ? $this->iHeureDebut : $evt["heuredeb"] );
            }
            
            if( $dateFinEvt >= $dateDemain ) {
              $hFinEvt = $this->iHeureFin;
            } else {
              $hFinEvt = ( $evt["heurefin"] > $this->iHeureFin ? $this->iHeureFin : $evt["heurefin"] );
            }

            $iMaxNbEvt = max(array_slice($tabNbH, ($hDebEvt-$hStart)/$this->iPrecision, 
                                         max(1, ($hFinEvt-$hDebEvt)/$this->iPrecision)));
            $iWidthEvt = floor($iLargeurCell / $iMaxNbEvt) - $iBorderWidth;
            $iHeightEvt = ( $hFinEvt-$hDebEvt > 0 
                            ? floor($this->iCalCellHeight*(($hFinEvt-$hDebEvt)/$this->iPrecisionCell) + 
                                    $this->iCalCellSpacing*(($hFinEvt-$hDebEvt-1)/$this->iPrecisionCell))
                            : $this->iCalCellHeight );
            $iTopEvt = floor($this->iCalCellHeight*(($hDebEvt-$hStart)/$this->iPrecisionCell) + 
                             $this->iCalCellSpacing*(($hDebEvt-$hStart)/$this->iPrecisionCell));
            $iLeftEvt = ($tabNbH[$h]-$tabH[$h]) * $iWidthEvt;
          
            $strParamDiv = "id=\"layer".$evt["id"]."\" align=\"right\"".
              " style=\"position:absolute; background-color:".$evt["bgcolor"].";".
              " width:".$iWidthEvt."px; height:".$iHeightEvt."px; overflow: hidden;".
              " z-index:1; border:2px solid #222222; visibility: hidden\"";

            $strHtmlLayer .= $this->GetHtmlCalEvt($evt, $strParamDiv);
            $strHtmlJs .= " var layerEvt = new LayerEvt('layer".$evt["id"]."', 'cell".$hStart."', ".$iLeftEvt.", ".$iTopEvt.");".
              " tabEvt.push(layerEvt);";
            array_push($tabIdEvt, $evt["id"]);     
          
            for($t=$hDebEvt; $t<=$hFinEvt; $t++) {
              if( array_key_exists($t, $tabH)==true ) {
                $tabH[$t] = $tabH[$t]-1;
              } else {
                $tabH[$t] = 0;
              }
            }
          }
        }
      }
        
      if( $cptLayer==0 ) {
        $strHtmlLayer .= $this->GetHtmlLayerDesc();
        $cptLayer++;
      }
      
      if( $h % $this->iPrecisionCell == 0 ) {
        $strHtml .= "</td></tr>";
      }
    }
    
    $strHtml .= "<tr>";
    if( $this->bHeureVisible==true )
      $strHtml .= "<td>&nbsp;</td>";
      
    $strHtml .= "<td class='".$this->cssCalCellEntete."'>".
      "<a href=\"".$strUrlPrec."\" class='".$this->cssCalCellPaginationLien."'></a></td>".
      "<td class='".$this->cssCalCellEntete."'>&nbsp;</td>".
      "<td align='right' class='".$this->cssCalCellEntete."'>".
      "<a href=\"".$strUrlSuiv."\" class='".$this->cssCalCellPaginationLien."'>>></a></td>".
      "</tr></table>";
          
    $strHtmlJs .= " window.onresize=winReload;".
      " function onLoadCal".$this->name."() {".
      "   if( typeof bWinLoaded != 'undefined' ) { if ( bWinLoaded == 1 ) winReload(); else setTimeout('onLoadCal".$this->name."', 500);}".
      " }".
      " setTimeout('onLoadCal".$this->name."()', 500);";

    $this->AddJs($strHtmlJs);
    $this->AddScriptJs(ALK_SIALKE_DIR."classes/form/htmlcalendar.js");

    return $strHtml.$strHtmlLayer;
  }
  
  /**
   * @brief Retourne le code html du calendrier hebdomadaire
   *
   * @return Retourne un string
   */
  function GetHtmlCalendarHebdomadaire()
  {
    $tabIdEvt = array();
    $tabH = array();
    $tabNbH = array();

    $iBorderWidth = ( $this->strNavigateur == "IE" ? 0 : 4 );
    
    $strHtmlJs = "";
    $strHtmlLayer = "";
    
    $iLargeurCell = ( $this->bHeureVisible==true 
                      ? floor(($this->iCalWidth-40-(2+$this->iNbJourVisible)*$this->iCalCellSpacing)/$this->iNbJourVisible)
                      : floor(($this->iCalWidth-(1+$this->iNbJourVisible)*$this->iCalCellSpacing)/$this->iNbJourVisible) );
    
    $iNumWeek = date("W", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
    $iNumJour = date("w", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
    if( $iNumJour==0 ) $iNumJour=7;
    
    // entete titre
    $strHtml = "<table class='calendar' cellpadding='0' cellspacing='".$this->iCalCellSpacing."' border='0' align='".$this->strCalAlign."'>".
      "<tr>".
      ( $this->bHeureVisible==true ? "<td width='40' height='0'></td>" : "");
    for($i=0; $i<$this->iNbJourVisible; $i++) {
      $strHtml .= "<td width='".$iLargeurCell."' style='height:0px'></td>";
    }
    $strHtml .= "</tr>".
      ($this->bAffTitre
      ? "<tr>".
      ( $this->bHeureVisible==true
        ? "<td width='40'>&nbsp;</td>"
        : "" ).
      "<td class='".$this->cssCalCellTitre."' colspan='".$this->iNbJourVisible."'>".
        "<table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td class='".$this->cssCalTitre."'>".
        ( $this->strCalTitre != "" 
          ? $this->strCalTitre.( $this->bAffTitreAuto==true ? " - " : "" ) 
          : "").
        ( $this->bAffTitreAuto==true
          ? "Semaine ".$iNumWeek." ".$this->iAnneeCur
          : "" ).
        ( $this->bAffBarreMenu==true 
          ? "</td><td align='right'>".$this->GetHtmlCalMenu()
          : "" ).
        "<td></tr></table>".

        "</td>" .
      "</tr>"
      : "");
    
    $strHtml .= "<tr>";
    
    if ($this->bHeureVisible==true)
      $strHtml .= "<td width='40'>&nbsp;</td>";
    
    $iDelta = 1 - $iNumJour;
    $mondayCur = strtotime(($iDelta < 0 ? $iDelta : "+".$iDelta)." day", 
                             mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
    
    if ( ($this->iJourMin!=-1 && $this->iMoisMin!=-1 && $this->iAnneeMin!=-1) ){
      $iJourPrec  = date("j", strtotime("-1 day", mktime(0, 0, 0, date("n", $mondayCur), date("j", $mondayCur), date("Y", $mondayCur))));
      $iMoisPrec  = date("n", strtotime("-1 day", mktime(0, 0, 0, date("n", $mondayCur), date("j", $mondayCur), date("Y", $mondayCur))));
      $iAnneePrec = date("Y", strtotime("-1 day", mktime(0, 0, 0, date("n", $mondayCur), date("j", $mondayCur), date("Y", $mondayCur))));      
    }
    else {
      $iJourPrec  = date("j", strtotime("-7 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
      $iMoisPrec  = date("n", strtotime("-7 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
      $iAnneePrec = date("Y", strtotime("-7 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    }
    
    
    if ( ($this->iJourMax!=-1 && $this->iMoisMax!=-1 && $this->iAnneeMax!=-1) ){
      $iJourSuiv  = date("j", strtotime("+8 day", mktime(0, 0, 0, date("n", $mondayCur), date("j", $mondayCur), date("Y", $mondayCur))));
      $iMoisSuiv  = date("n", strtotime("+8 day", mktime(0, 0, 0, date("n", $mondayCur), date("j", $mondayCur), date("Y", $mondayCur))));
      $iAnneeSuiv = date("Y", strtotime("+8 day", mktime(0, 0, 0, date("n", $mondayCur), date("j", $mondayCur), date("Y", $mondayCur))));      
    }
    else {
      $iJourSuiv  = date("j", strtotime("+7 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
      $iMoisSuiv  = date("n", strtotime("+7 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
      $iAnneeSuiv = date("Y", strtotime("+7 day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    }
    
    $strUrlPrec = $this->getUrlPagine($this->iMode, $iJourPrec, $iMoisPrec, $iAnneePrec);
    $bUrlPrec = $this->CanPagineTo($iJourPrec, $iMoisPrec, $iAnneePrec, false);
    
    $strUrlSuiv = $this->getUrlPagine($this->iMode, $iJourSuiv, $iMoisSuiv, $iAnneeSuiv);
    $bUrlSuiv = $this->CanPagineTo($iJourSuiv, $iMoisSuiv, $iAnneeSuiv, true);
    

    for($k=1; $k<=$this->iNbJourVisible; $k++) {
      $iDelta = $k - $iNumJour;
      $dateJourCal = strtotime(($iDelta < 0 ? $iDelta : "+".$iDelta)." day", 
                               mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
      $iNumJr = date("j", $dateJourCal);
      
      $strHtml .= "<td align='center' class='".$this->cssCalCellEntete."'>&nbsp;".
        ( $k == 1 && $this->bAffJourSemaine && $bUrlPrec
          ? "<a title='Semaine pr�c�dente' href=\"".$strUrlPrec."\" class='".$this->cssCalCellPaginationLien."'></a>&nbsp;&nbsp;"
          : "").
        ( $k == $this->iNbJourVisible && $this->bAffJourSemaine && $bUrlSuiv
          ? "<a title='Semaine suivante' href=\"".$strUrlSuiv."\" class='".$this->cssCalCellPaginationLien."'>>></a>"
          : "" ).
        "<br/>".
        ($this->bAffJourSemaine && $this->bLinkJourSemaine
        ? "<a style='text-decoration:none; border:none; padding:none' class='".$this->cssCalCellEntete."' title='Afficher le calendrier de ce jour'".
          "   href='".$this->getUrlPagine(ALK_MODE_JOUR_CAL, date("d", $dateJourCal), date("m", $dateJourCal), date("Y", $dateJourCal))."'>"
        : "").
        $this->ConvertDateToFrench(0, $k, false, $this->bAffJourSemaine)." ".     
        ($this->bAffJourSemaine
        ? $iNumJr.( $iNumJr=="1" ? "er" : "" )
        : "").
        ( $this->bAffJourMois == true 
          ? " ".$this->ConvertDateToFrench(1, date("n", $dateJourCal), false, true)
          : "" ).
        ( $this->bAffMoisAnnee == true 
          ? " ".date("y", $dateJourCal)
          : "" ).
        ($this->bAffJourSemaine && $this->bLinkJourSemaine
        ? "</a>"
        : "").
        "</td>";
    }
    $strHtml .= "</tr>";
         
    $cptLayer = 0;
    
    // Ajuste l'heure de d�but et de fin de journ�e en fonction de la pr�cision d'affichage.
    // Exemple : Afficher le cadre 8h pour une journ�e qui d�marre � 8h15 avec 30mn de pr�cision d'affichage.
    $hStart = ( $this->iHeureDebut % $this->iPrecisionCell == 0
                ? $this->iHeureDebut 
                :  $this->iHeureDebut - ($this->iHeureDebut % $this->iPrecisionCell) );
    $hEnd  = ( $this->iHeureFin % $this->iPrecisionCell == 0
               ? $this->iHeureFin 
               : $this->iHeureFin + ($this->iHeureFin % $this->iPrecisionCell) );
    $iRowSpanFerie = ($hEnd-$hStart)/$this->iPrecisionCell;

    $tabJrFerie = array_fill(1, $this->iNbJourVisible, false);
    $tabIdEvt = array_fill(1, $this->iNbJourVisible, array());
    for($i=0; $i<=$this->iNbJourVisible; $i++) {
      for($h = $hStart; $h < $hEnd; $h += $this->iPrecision) {
        $tabNbH[$i][$h] = 0;
        $tabH[$i][$h] = 0;
      }
    }
    
    // m�morise le nombre d'�venements par tranche horaire et par jour de la semaine
    for($i=1; $i<=$this->iNbJourVisible; $i++) {
      $iDelta = $i - $iNumJour;
      $dateJourCal = strtotime(($iDelta < 0 ? $iDelta : "+".$iDelta)." day", 
                               mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
      $tabJrFerie[$i] = $this->isIgnoreDay(date("d", $dateJourCal), date("m", $dateJourCal), date("Y", $dateJourCal));
      if( !$tabJrFerie[$i] ) {
        for($h = $hStart; $h < $hEnd; $h += $this->iPrecision) {
          $hh = floor($h/100);
          $mn = (($h-($hh*100))*60)/100;
          $dateJourCal = strtotime(($iDelta < 0 ? $iDelta : "+".$iDelta)." day", 
                                   mktime($hh, $mn, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
        
          foreach($this->tabCalEvt as $evt) {
            if( $evt["datedeb"] != "" )
              $dateDebutEvt = $this->_GetEvtTimestamp($evt, "deb");
            else
              $dateDebutEvt = 0;
            
            if( $evt["datefin"] != "" )
              $dateFinEvt = $this->_GetEvtTimestamp($evt, "fin");
            else
              $dateFinEvt = $dateDebutEvt;
            
            if( ($dateDebutEvt<=$dateJourCal && $dateJourCal<=$dateFinEvt) ||
                ($dateDebutEvt==$dateJourCal && $dateFinEvt=="") ) {
              $tabNbH[$i][$h] = $tabNbH[$i][$h]+1;
              $tabH[$i][$h] = $tabH[$i][$h]+1;
            }
          }
        }
      }
    }
    $dateToday = date("d/m/Y");

    // cr�ation des plages horaires et calcul du positionnement
    $tabRowSpan = array_fill(1, $this->iNbJourVisible, array(false, false));
    $cptLayer = 0;
    for($h = $hStart; $h < $hEnd; $h += $this->iPrecision) {
      $hh = floor($h/100);
      $mn = ($h-($hh*100))*60/100;
      $strHour = $this->_Int2Hour($h);

      if( $h % $this->iPrecisionCell == 0 ) {
        $strHtml .= "<tr>";
      
        if( $this->bHeureVisible==true )
          $strHtml .= "<td class='".$this->cssCalCellEntete.($this->bSeparateurDHeure && $mn==0 ? " CalendarSep" : "")."' align='center'>".
            ($mn == 0 ? $strHour : ($this->bAffHeureInterval ? str_replace($hh.":", str_repeat("&nbsp;", strlen($hh)+1).":", $strHour) : "&nbsp;" )).
            "</td>";
      }
        
      for($i=1; $i<=$this->iNbJourVisible; $i++) {
        $iDelta = $i - $iNumJour;
        $dateJourCal = strtotime(($iDelta < 0 ? $iDelta : "+".$iDelta)." day", mktime($hh, $mn, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));

        if( $h % $this->iPrecisionCell == 0 ) {
          if( $tabJrFerie[$i] == true ) {
            if( !$tabRowSpan[$i][0] ) {
              $tabRowSpan[$i][0] = true;
              $strHtml .= "<td class='".$this->cssCalCellFerie."' ".( $h==$hStart ? "id='cell".$h.$i : "" ).
                "' align='center' rowspan='".$iRowSpanFerie."'>Ferm�";
            }
          } else {
            $strClass = $this->cssCalCell;
            if( $this->bChangeCssToday && $dateToday == date("d/m/Y", $dateJourCal) ) {
              $strClass = $this->cssCalCellCur;
            }
            
            $dateJour = strtotime(($iDelta < 0 ? $iDelta : "+".$iDelta)." day", strtotime($this->iAnneeCur."-".$this->iMoisCur."-".$this->iJourCur));
            
            if ( array_key_exists($dateJour, $this->tabCSSCell) && array_key_exists($h, $this->tabCSSCell[$dateJour]) )
              $strClass = $this->tabCSSCell[$dateJour][$h];
            
            $bHorsBorne = false;
            if ( $this->bFerieHorsBorne ){
              $bHorsBorne = !(   $this->CanPagineTo(date("d", $dateJour), date("m", $dateJour), date("Y", $dateJour), false)
                              && $this->CanPagineTo(date("d", $dateJour), date("m", $dateJour), date("Y", $dateJour), true));
              if ( $bHorsBorne )
                $strClass = $this->cssCalCellFerie;
            }
            $bOnClickCell = $this->bOnClickCell == true && ($this->bOnClickOnlyAfter == true ? $dateJourCal >= time() : true);
      
            $strHtml .= "<td id='cell".$h.$i."'\" width='".$iLargeurCell."px'".
              " class='".$strClass.($this->bSeparateurDHeure && $mn==0 ? " CalendarSep" : "")."' valign='top' height='".$this->iCalCellHeight."'".
              ( !$bHorsBorne 
                ? " onmouseover=\"if (window.onMouseOverCalCell) onMouseOverCalCell(this, '".$this->cssCalCellRoll."', ".($bOnClickCell == true ? "true" : "false").")\"".
                  " onmouseout=\"if (window.onMouseOutCalCell) onMouseOutCalCell(this)\"".
                  ( $bOnClickCell == true 
                    ? " onclick=\"if (window.onClickCalCell".$this->name.") onClickCalCell".$this->name."(".$this->iMode.", ".
                      ($this->bAffJourSemaine
                      ? date("d", $dateJourCal).", ".
                        date("m", $dateJourCal).", ".date("Y", $dateJourCal).",".$hh.",".$mn
                      : $i.", ".$hh.",".$mn.",".$this->iPrecisionCell).")\""
                    : "" )
                : "").
              ( $bOnClickCell == true
                ? " title=\"".$this->titleOnClickCell."\""
                : "" ).
              ">&nbsp;";
          }
        }
        
        if( !$tabJrFerie[$i]) {
          foreach($this->tabCalEvt as $evt) {
            $dateDebutEvt = $this->_GetEvtTimestamp($evt, "deb");
            if( $evt["datefin"] != "" )
              $dateFinEvt = $this->_GetEvtTimestamp($evt, "fin");
            else
              $dateFinEvt = $dateDebutEvt;
              
            if( ( ($dateDebutEvt<=$dateJourCal && $dateJourCal<=$dateFinEvt) || 
                  ($dateDebutEvt==$dateJourCal && $dateFinEvt=="") ) && in_array($evt["id"], $tabIdEvt[$i])==false ) {

              $dateHier = strtotime(($iDelta-1 < 0 ? $iDelta-1 : "+".($iDelta-1))." day", mktime(23, 59, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
              $dateDemain = strtotime(($iDelta+1 < 0 ? $iDelta+1 : "+".($iDelta+1))." day", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur));
          
              if( $dateDebutEvt <= $dateHier ) {
                $hDebEvt = $this->iHeureDebut;
              } else {
                $hDebEvt = ( $evt["heuredeb"] < $this->iHeureDebut ? $this->iHeureDebut : $evt["heuredeb"] );
              }
            
              if( $dateFinEvt >= $dateDemain ) {
                $hFinEvt = $this->iHeureFin;
              } else {
                $hFinEvt = ( $evt["heurefin"] > $this->iHeureFin ? $this->iHeureFin : $evt["heurefin"] );
              }

              $iMaxNbEvt = max(array_slice($tabNbH[$i], ($hDebEvt-$hStart)/$this->iPrecision, 
                                           max(1, ($hFinEvt-$hDebEvt)/$this->iPrecision)));
              $iWidthEvt = floor($iLargeurCell / $iMaxNbEvt)-$iBorderWidth;
              $iHeightEvt = ( $hFinEvt-$hDebEvt > 0 
                              ? floor($this->iCalCellHeight*(($hFinEvt-$hDebEvt)/$this->iPrecisionCell) + 
                                      $this->iCalCellSpacing*(($hFinEvt-$hDebEvt-1)/$this->iPrecisionCell))
                              : $this->iCalCellHeight );
              $iTopEvt = floor($this->iCalCellHeight*(($hDebEvt-$hStart)/$this->iPrecisionCell) + 
                               $this->iCalCellSpacing*(($hDebEvt-$hStart)/$this->iPrecisionCell));
              $iLeftEvt = ($tabNbH[$i][$h]-$tabH[$i][$h]) * ($iWidthEvt + $this->iCalCellSpacing);

              $strParamDiv = "id=\"layer".$evt["id"].$i."\" align=\"right\" class=\"calendarEvt\" style=\"position:absolute; background-color:".$evt["bgcolor"].";".
                " width:".$iWidthEvt."px; height:".$iHeightEvt."px; overflow: hidden;".
                " z-index:1; border:2px solid #222222; visibility: hidden;\"";

              $strHtmlLayer .= $this->GetHtmlCalEvt($evt, $strParamDiv);
              $strHtmlJs .= " var layerEvt = new LayerEvt('layer".$evt["id"].$i."', 'cell".$hStart.$i."', ".$iLeftEvt.", ".$iTopEvt.");".
                " tabEvt.push(layerEvt);";
              array_push($tabIdEvt[$i], $evt["id"]);
            
              for($t=$hDebEvt; $t<=$hFinEvt; $t++) {
                if( array_key_exists($t, $tabH[$i])==true ) {
                  $tabH[$i][$t] = $tabH[$i][$t]-1;
                } else {
                  $tabH[$i][$t] = 0;
                }
              }
            }
          }
        }

        if( $h % $this->iPrecisionCell == 0 ) {
          if( $tabJrFerie[$i] == true ) {
            if( !$tabRowSpan[$i][1] ) {
              $tabRowSpan[$i][1] = true;
              $strHtml .= "</td>";
            }
          } else {
            $strHtml .= "</td>";
          }
        }
      }
      
      if( $cptLayer==0 ) {
        $strHtmlLayer .= $this->GetHtmlLayerDesc();
        $cptLayer++;
      }

      $strHtml .= "</tr>";
    }

    if ( $this->bAffJourSemaine ){
      $strHtml .= "<tr>";
      if( $this->bHeureVisible == true )
        $strHtml .= "<td width='40'>&nbsp;</td>";
      
      for($k=1; $k<=$this->iNbJourVisible; $k++)
        $strHtml .= "<td align='center' class='".$this->cssCalCellEntete."'>".
          ( $k==1 && $bUrlPrec
            ? "<a title='Semaine pr�c�dente' href=\"".$strUrlPrec."\" class='".$this->cssCalCellPaginationLien."'> pr�c�dente</a>"
            : "").
          ( $k==$this->iNbJourVisible && $bUrlSuiv
            ? "<a title='Semaine suivante' href=\"".$strUrlSuiv."\" class='".$this->cssCalCellPaginationLien."'>suivante >></a>"
            : "").
          "</td>";
       
      $strHtml .= "</tr>";
    }
    $strHtml .= "</table>";

    $strHtmlJs .= " window.onresize=winReload;".
      " function onLoadCal".$this->name."() {".
      "   if( typeof bWinLoaded != 'undefined' ) { if ( bWinLoaded == 1 ) winReload(); else setTimeout('onLoadCal".$this->name."', 500);}".
      " }".
      " setTimeout('onLoadCal".$this->name."()', 500);";

    $this->AddJs($strHtmlJs);
    $this->AddScriptJs(ALK_SIALKE_DIR."classes/form/htmlcalendar.js");

    return $strHtml.$strHtmlLayer;
  }
  
  /**
   * @brief Retourne le code html du calendrier mensuel
   *
   * @return Retourne un string
   */
  function GetHtmlCalendarMensuel()
  {
    $iBorderWidth = 9;

    $iCalWidth = ( $this->bHeureVisible==true 
                   ? $this->iCalWidth-42
                   : $this->iCalWidth );
    
    $iLargeurCellCal = floor(($iCalWidth-($this->iNbJourVisible-1)*2) / $this->iNbJourVisible);

    $dayone = date("w", mktime(0, 0, 0, $this->iMoisCur, 1, $this->iAnneeCur));
    if( $dayone == 0 )
      $dayone = 7;
    
    $iJourPrec  = date("j", strtotime("-1 month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iMoisPrec  = date("n", strtotime("-1 month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iAnneePrec = date("Y", strtotime("-1 month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    
    $iJourSuiv  = date("j", strtotime("+1 month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iMoisSuiv  = date("n", strtotime("+1 month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iAnneeSuiv = date("Y", strtotime("+1 month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    
    $strHtml = "<table class='calendar' cellpadding='0' cellspacing='".$this->iCalCellSpacing."' border='0' align='".$this->strCalAlign."'>".
      "<tr>".
      ( $this->bHeureVisible == true ? "<td width='40' height='0'></td>" : "" );
    
    for($k=0; $k<$this->iNbJourVisible; $k++) {
      $strHtml .= "<td width='".$iLargeurCellCal."'></td>";
    }
    
    $strHtml .= "</tr>".
      "<tr>".
      ( $this->bHeureVisible == true ? "<td></td>" : "" ).
      "<td class='".$this->cssCalCellTitre."' colspan='".$this->iNbJourVisible."'>".
      "<table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td class='".$this->cssCalTitre."'>".
      ( $this->strCalTitre!=""
        ? $this->strCalTitre.( $this->bAffTitreAuto==true ? " - " : "" )
        : "" ).
      ( $this->bAffTitreAuto==true
        ? $this->ConvertDateToFrench(1, $this->iMoisCur, true)." ".$this->iAnneeCur
        : "" ).
      ( $this->bAffBarreMenu==true
        ? "</td><td align='right'>".$this->GetHtmlCalMenu()
        : "" ).
      "</td></tr></table>".
      "</td></tr>".
      "<tr>".
      ( $this->bHeureVisible == true ? "<td></td>" : "" );
  
    $strUrlPrec = $this->getUrlPagine($this->iMode, $iJourPrec, $iMoisPrec, $iAnneePrec);
    $strUrlSuiv = $this->getUrlPagine($this->iMode, $iJourSuiv, $iMoisSuiv, $iAnneeSuiv);
      
    for($k=1; $k<=$this->iNbJourVisible; $k++) {
      $strHtml .= "<td align='center' class='".$this->cssCalCellEntete."'>".
        ( $k==1
          ? "<a href='".$strUrlPrec."' class='".$this->cssCalCellPaginationLien."' title='Mois pr�c�dent'><<</a>&nbsp;&nbsp;"
          : "" ).
        ( $k==$this->iNbJourVisible
          ? "<a href='".$strUrlSuiv."' class='".$this->cssCalCellPaginationLien."' title='Mois suivant'>>></a>&nbsp;&nbsp;"
          : "" ).
        $this->ConvertDateToFrench(0, $k).
        "</td>";
    }

    $cptLayer = 0;
    $iNbCellCalendar = 35;
    if( $dayone==7 && $this->GetDaysInMonth($this->iMoisCur, $this->iAnneeCur)>=30 )
      $iNbCellCalendar = 42;
    if( $dayone==1 && $this->GetDaysInMonth($this->iMoisCur, $this->iAnneeCur)==28 )
      $iNbCellCalendar = 28;
    
    for($i=1; $i<=$iNbCellCalendar; $i++) {
      $iDelta = -$dayone+1;
      $iJourTest = date("w", strtotime(($iDelta<0 ? $iDelta : "+".$iDelta)." day", mktime(0, 0, 0, $this->iMoisCur, $i, $this->iAnneeCur)));
      if( $iJourTest==0 )
        $iJourTest = 7;
        
        
      if( $iJourTest <= $this->iNbJourVisible ) {
        if( $i % 7 == 1 ) {
          $strUrlWeek = $this->getUrlPagine(ALK_MODE_HEBDO_CAL, ($i-$dayone+1));
          $strHtml .= "</tr><tr>".
            ( $this->bHeureVisible == true
              ? "<td class='".$this->cssCalCellEntete."'>".
                "<a href='".$strUrlWeek."' style='text-decoration: none'".
                " title='Afficher le calendrier de cette semaine' class='".$this->cssCalCellPaginationLien."'>".
                "S ".date("W", strtotime(($iDelta<0 ? $iDelta : "+".$iDelta)." day", mktime(0, 0, 0, $this->iMoisCur, $i, $this->iAnneeCur))).
                "</a></td>"
              : "" );
        }
        $iNumDay = $i-$dayone+1;
        $bJrFerie = $this->isIgnoreDay($iNumDay, $this->iMoisCur, $this->iAnneeCur);
        if( !$bJrFerie && $i < $this->GetDaysInMonth($this->iMoisCur, $this->iAnneeCur)+$dayone && $i >= $dayone ) {
          $strUrlDay = $this->getUrlPagine(ALK_MODE_JOUR_CAL, $iNumDay);
          $dateJourCalStart = mktime(0, 0, 0, $this->iMoisCur, $iNumDay, $this->iAnneeCur);
          $dateJourCalEnd = mktime(23, 59, 0, $this->iMoisCur, $iNumDay, $this->iAnneeCur);
          
          $bOnClickCell = $this->bOnClickCell == true && ($this->bOnClickOnlyAfter == true ? $dateJourCalStart >= mktime(0, 0, 0, date("m"), date("d"), date("Y")) : true);
      
          $strHtml .= "<td ".
            " class=\"".( date("d/m/Y", $dateJourCalStart)===date("d/m/Y") ? $this->cssCalCellCur : $this->cssCalCell )."\"".
            " valign='top' align='left' height='".$this->iCalCellHeight."'".
            " onmouseover=\"onMouseOverCalCell(this, '".$this->cssCalCellRoll."', ".($bOnClickCell ? "true" : "false").")\"".
            " onmouseout=\"onMouseOutCalCell(this)\"".
            ( $bOnClickCell
              ? " onclick=\"onClickCalCell".$this->name."(".$this->iMode.", ".$iNumDay.", ".$this->iMoisCur.", ".$this->iAnneeCur.", 12, 0)\""
              : "" ).
            ( $bOnClickCell
              ? " title=\"".$this->titleOnClickCell."\""
              : "" ).
            ">".
            "<div><a href='".$strUrlDay."' style='text-decoration: none'".
            "  title='Afficher le calendrier de ce jour' class='".$this->cssCalCellPaginationLien."'>".
            ( $iNumDay < 10 ? "0" : "" ).$iNumDay."</a></div>";
          
          $strEvtJour="";
          foreach ($this->tabCalEvt as $evt) {
            $dateDebutEvt = $this->_GetEvtTimestamp($evt, "deb");
            if( $evt["datefin"] != "" )
              $dateFinEvt = $this->_GetEvtTimestamp($evt, "fin");
            else
              $dateFinEvt = $dateDebutEvt;

            if( ($dateDebutEvt<=$dateJourCalStart and $dateJourCalEnd<=$dateFinEvt) ||
                ($dateJourCalStart<=$dateDebutEvt && $dateDebutEvt<=$dateJourCalEnd) ||
                ($dateJourCalStart<=$dateFinEvt && $dateFinEvt<=$dateJourCalEnd) ) {
              $strParamDiv = "style='width:".($iLargeurCellCal-$iBorderWidth)."; margin-bottom:1px'";
              $strEvtJour .= $this->GetHtmlCalEvt($evt, $strParamDiv);
            }
          }
          
          $strHtml .=  $strEvtJour;
          
          if( $cptLayer == 0 ) {
            $strHtml .= $this->GetHtmlLayerDesc();
            $cptLayer++;
          }
           
          $strHtml .= "</td>";
        } else {
          $strHtml .= "<td valign='top'".
            ( $bJrFerie
              ? " class='".$this->cssCalCellFerie."'>".
                "<span class='".$this->cssCalCellJour."'>".
                ( $iNumDay < 10 ? "0" : "" ).$iNumDay."</span><br><br>Ferm�"
              : " class='".$this->cssCalCellNull."'>&nbsp;" ).
            "</td>";
        }
      }
    }
    
    $strHtml .= "</tr><tr>".
      ( $this->bHeureVisible == true ? "<td></td>" : "" );
    
    for($k=1; $k<=$this->iNbJourVisible; $k++) {
      $strHtml .= "<td align='center' class='".$this->cssCalCellEntete."'>".
        ( $k==1
          ? "<a href='".$strUrlPrec."' class='".$this->cssCalCellPaginationLien."' title='Mois pr�c�dent'><< ".
            $this->ConvertDateToFrench(1, ($this->iMoisCur-1==0 ? 12 : $this->iMoisCur-1))."</a>"
          : "" ).
        ( $k==$this->iNbJourVisible
          ? "<a href='".$strUrlSuiv."' class='".$this->cssCalCellPaginationLien."' title='Mois suivant'>".
            $this->ConvertDateToFrench(1, ($this->iMoisCur+1==13 ? 1 : $this->iMoisCur+1))." >></a>"
          : "" ).
        "</td>";
    }
    $strHtml .= "</tr></table>";
    
    $this->AddScriptJs(ALK_SIALKE_DIR."classes/form/htmlcalendar.js");

    return $strHtml;
  }
  
  /**
   * @brief Retourne le code html du calendrier annuel
   *
   * @return Retourne un string
   */
  function GetHtmlCalendarAnnuel()
  {
    $cptLayer = 0;

    $iBorderWidth = 9;

    switch ($this->iMode){
      case ALK_MODE_TRIMESTRE_CAL : $this->iNbMoisVisible = 3;  break;
      case ALK_MODE_SEMESTRE_CAL  : $this->iNbMoisVisible = 6;  break;
      case ALK_MODE_ANNUEL_CAL    : $this->iNbMoisVisible = 12; break;
    }
    $iCalWidth = ( $this->bHeureVisible==true 
                   ? $this->iCalWidth-22
                   : $this->iCalWidth );
    
    $iLargeurCellCal = floor(($iCalWidth-($this->iNbMoisVisible-1)*2) / $this->iNbMoisVisible);
    
    $iJourPrec  = date("j", strtotime(-$this->iNbMoisVisible." month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iMoisPrec  = date("n", strtotime(-$this->iNbMoisVisible." month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iAnneePrec = date("Y", strtotime(-$this->iNbMoisVisible." month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    
    $iJourSuiv  = date("j", strtotime("+".$this->iNbMoisVisible." month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iMoisSuiv  = date("n", strtotime("+".$this->iNbMoisVisible." month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    $iAnneeSuiv = date("Y", strtotime("+".$this->iNbMoisVisible." month", mktime(0, 0, 0, $this->iMoisCur, $this->iJourCur, $this->iAnneeCur)));
    
    $strHtml = "<table class='calendar' cellpadding='0' cellspacing='".$this->iCalCellSpacing."' border='0' align='".$this->strCalAlign."'>".
      "<tr>".
      ( $this->bHeureVisible == true ? "<td width='20' height='0'></td>" : "" );

    for($k=0; $k<$this->iNbMoisVisible; $k++) {
      $strHtml .= "<td width='".$iLargeurCellCal."'></td>";
    }

    $strHtml .= "</tr>".
      "<tr>".
      ( $this->bHeureVisible == true ? "<td></td>" : "" ).
      "<td class='".$this->cssCalCellTitre."' colspan='".$this->iNbMoisVisible."'>".
      "<table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td class='".$this->cssCalTitre."'>".
      ( $this->strCalTitre != ""
        ? $this->strCalTitre.( $this->bAffTitreAuto==true ? " - " : "" ) 
        : "" ).
      $this->iAnneeCur.
      ( $this->bAffBarreMenu==true 
        ? "</td><td align='right'>".$this->GetHtmlCalMenu()
        : "" ).
      "<td></tr></table>".
      "</td></tr>".
      "<tr>".
      ( $this->bHeureVisible == true ? "<td></td>" : "" );
         
    $strUrlPrec = $this->getUrlPagine($this->iMode, $iJourPrec, $iMoisPrec, $iAnneePrec);
    $strUrlSuiv = $this->getUrlPagine($this->iMode, $iJourSuiv, $iMoisSuiv, $iAnneeSuiv);
    
    for ($k=0; $k<$this->iNbMoisVisible; $k++) {
      $iM = date("n", strtotime("+".$k." month", mktime(0, 0, 0, $this->iMoisCur, 1, $this->iAnneeCur)));
      $iA = date("Y", strtotime("+".$k." month", mktime(0, 0, 0, $this->iMoisCur, 1, $this->iAnneeCur)));

      $strHtml .= "<td align='center' valign='top' class='".$this->cssCalCellEntete."'>".
        ( $k==0 
          ? "<a class='".$this->cssCalCellPaginationLien."' href='".$strUrlPrec."' title='".$this->iNbMoisVisible." mois pr�c�dents'><<</a>"
          : "").
        ( $k==$this->iNbMoisVisible-1
          ? "<a class='".$this->cssCalCellPaginationLien."' href='".$strUrlSuiv."' title='".$this->iNbMoisVisible." mois suivants'>>></a>"
          : "" ).
        "<br/><a style='text-decoration:none; border:none; padding:none' class='".$this->cssCalCellEntete."' title='Afficher le calendrier de ce mois'".
        " href='".$this->getUrlPagine(ALK_MODE_MOIS_CAL, "1", $iM, $iA)."'>".
        $this->ConvertDateToFrench(1, $iM).
        ( $this->bAffMoisAnnee==true ? "<br/>".$iA : "" ).
        "</a>".
        "</td>";
    }
        
    $strHtml .= "</tr>";

    // m�morise le nombre de jour par mois, le mois et l'ann�e pour chaque colonne affich�e
    $tabNbDays = array();
    for($k=0; $k<$this->iNbMoisVisible; $k++) {
      $iM = date("n", strtotime("+".$k." month", mktime(0, 0, 0, $this->iMoisCur, 1, $this->iAnneeCur)));
      $iA = date("Y", strtotime("+".$k." month", mktime(0, 0, 0, $this->iMoisCur, 1, $this->iAnneeCur)));
      $tabNbDays[$k]["nb"] = $this->GetDaysInMonth($iM, $iA);
      $tabNbDays[$k]["iM"] = $iM;
      $tabNbDays[$k]["iA"] = $iA;
    }    

    for($i=1; $i<=31; $i++) {
      $strHtml .= "<tr>".
        ( $this->bHeureVisible == true 
          ? "<td class='".$this->cssCalCellEntete."' valign='top' align='left' height='".$this->iCalCellHeight."'>".
            "<span class='".$this->cssCalCellJour."'>".(strlen($i)<2 ? "0" : "" ).$i."</span></td>"
          : "" );
      
      for ($k=0; $k<$this->iNbMoisVisible; $k++) {
        $iM = $tabNbDays[$k]["iM"];
        $iA = $tabNbDays[$k]["iA"];
        $nbDaysInMonth = $tabNbDays[$k]["nb"];

        if( $i<=$nbDaysInMonth ) {
          $iJs = date("w", mktime(0, 0, 0, $iM, $i, $iA));
          $bJrFerie = $this->isIgnoreDay($i, $iM, $iA);
          
          if( !$bJrFerie ) {
            $strUrlDay = $this->getUrlPagine(ALK_MODE_JOUR_CAL, $i, $iM, $iA);
            $dateJourCalStart = mktime(0, 0, 0, $iM, $i, $iA);
            $dateJourCalEnd = mktime(23, 59, 0, $iM, $i, $iA);
            
            $bOnClickCell = $this->bOnClickCell == true && ($this->bOnClickOnlyAfter == true ? $dateJourCalStart >= mktime(0, 0, 0, date("m"), date("d"), date("Y")) : true);
      
            $strHtml .= "<td".
              " class=\"".( date("d/m/Y", $dateJourCalStart)===date("d/m/Y") ? $this->cssCalCellCur : $this->cssCalCell )."\"".
              " valign='top' align='left' height='".$this->iCalCellHeight."'".
              " onmouseover=\"onMouseOverCalCell(this, '".$this->cssCalCellRoll."', ".($bOnClickCell == true ? "true" : "false").")\"".
              " onmouseout=\"onMouseOutCalCell(this)\"".
              ( $bOnClickCell == true
                ? " onclick=\"onClickCalCell".$this->name."(".$this->iMode.", ".$i.", ".$iM.", ".$iA.", 12, 0)\""
                : "" ).
              ( $bOnClickCell == true
                ? " title=\"".$this->titleOnClickCell."\""
                : "" ).
              ">".
              "<div><a href='".$strUrlDay."' style='text-decoration: none'".
              "  title='Afficher le calendrier de ce jour' class='".$this->cssCalCellPaginationLien."'>".
              $this->ConvertDateToFrench(2, $iJs, false, true).($this->bHeureVisible == true ? "" : " ".$i)."</a></div>";
          
            $strEvtJour="";
            foreach ($this->tabCalEvt as $evt) {
              $dateDebutEvt = $this->_GetEvtTimestamp($evt, "deb");
              if( $evt["datefin"] != "" )
                $dateFinEvt = $this->_GetEvtTimestamp($evt, "fin");
              else
                $dateFinEvt = $dateDebutEvt;

              if( ($dateDebutEvt<=$dateJourCalStart and $dateJourCalEnd<=$dateFinEvt) ||
                  ($dateJourCalStart<=$dateDebutEvt && $dateDebutEvt<=$dateJourCalEnd) ||
                  ($dateJourCalStart<=$dateFinEvt && $dateFinEvt<=$dateJourCalEnd) ) {
                $strParamDiv = "style='width:".($iLargeurCellCal-$iBorderWidth)."; margin-bottom:1px'";
                $strEvtJour .= $this->GetHtmlCalEvt($evt, $strParamDiv);
              }
            }
          
            $strHtml .=  $strEvtJour;
          
            if( $cptLayer == 0 ) {
              $strHtml .= $this->GetHtmlLayerDesc();
              $cptLayer++;
            }
           
            $strHtml .= "</td>";
          } else {
            $strHtml .= "<td valign='top' class='".$this->cssCalCellFerie."'><span class='".$this->cssCalCellJour."'>".
              $this->ConvertDateToFrench(2, $iJs, false, true).($this->bHeureVisible == true ? "" : " ".$i)."</span><br><br>Ferm�</td>";
          }
        } else {
          $strHtml .= "<td valign='top' class='".$this->cssCalCellNull."'>&nbsp;</td>";
        }
      }
      $strHtml .= "</tr>";
    }

    $strHtml .= "<tr>".
      ( $this->bHeureVisible == true ? "<td></td>" : "" );

    for($k=0; $k<$this->iNbMoisVisible; $k++) {
      $iM = date("n", strtotime("+".$k." month", mktime(0, 0, 0, $this->iMoisCur, 1, $this->iAnneeCur)));
      $iA = date("Y", strtotime("+".$k." month", mktime(0, 0, 0, $this->iMoisCur, 1, $this->iAnneeCur)));

      $strHtml .= "<td align='center' valign='top' class='".$this->cssCalCellEntete."'>".
        ( $k==0
          ? "<a class='".$this->cssCalCellPaginationLien."' href='".$strUrlPrec."' title='".$this->iNbMoisVisible." mois pr�c�dents'><<</a>&nbsp;&nbsp;&nbsp; "
          : "" ).
        "<a style='text-decoration:none; border:none; padding:none' class='".$this->cssCalCellEntete."' title='Afficher le calendrier de ce mois'".
        " href='".$this->getUrlPagine(ALK_MODE_MOIS_CAL, "1", $iM, $iA)."'>".
        $this->ConvertDateToFrench(1, $iM).
        ( $this->bAffMoisAnnee==true ? " ".$iA : "" ).
        "</a>".
        ( $k==$this->iNbMoisVisible-1
          ? "&nbsp;&nbsp;&nbsp;<a class=\"".$this->cssCalCellPaginationLien."\" href='".$strUrlSuiv."' title='".$this->iNbMoisVisible." mois suivants'>>></a>"
          : "" ).
        "</td>";
    }
    $strHtml .= "</tr></table>";

    $this->AddScriptJs(ALK_SIALKE_DIR."classes/form/htmlcalendar.js");
    
    return $strHtml;
  }
   
  /**
   * @brief Retourne le code html du formulaire complet
   *
   * @return Retourne un string
   */
  function GetHtml()
  {
    $strHtml = "";
    switch( $this->iMode ) {
    case ALK_MODE_JOUR_CAL:
      $strHtml .= $this->GetHtmlCalendarJournalier();
      break;
      
    case ALK_MODE_HEBDO_CAL:
      $strHtml .= $this->GetHtmlCalendarHebdomadaire();
      break;
      
    case ALK_MODE_MOIS_CAL:
      $strHtml .= $this->GetHtmlCalendarMensuel();
      break;
      
    case ALK_MODE_TRIMESTRE_CAL:
    case ALK_MODE_SEMESTRE_CAL:
    case ALK_MODE_ANNUEL_CAL:
      $strHtml .= $this->GetHtmlCalendarAnnuel();
      break; 
    } 
    $strHtml .= $this->GetHtmlJs();
    return $strHtml;      
  }
  
  function getUrlPagine($iModeAff, $iJour="", $iMois="", $iAnnee="", $args=""){
    if ( $iModeAff=="" ) $iModeAff = $this->iMode;
    if ( $iJour=="" ) $iJour = $this->iJourCur;
    if ( $iMois=="" ) $iMois = $this->iMoisCur;
    if ( $iAnnee=="" ) $iAnnee = $this->iAnneeCur;
    $tabFormat = array(
      "%jour%" => $iJour,
      "%mois%" => $iMois,
      "%annee%" => $iAnnee,
      "%imodeaff%" => $iModeAff,
    );
    $url_get = "&j=%jour%&m=%mois%&a=%annee%&imodeaff=%imodeaff%";
    if ( preg_match("!^javascript:!", $this->strUrlPagine) ){
      $strUrl = str_replace(")", ($args!="" ? ", '".$args."'" : "").")", 
                preg_replace(array_keys($tabFormat), array_values($tabFormat), $this->strUrlPagine));
    } else {
      $strUrl = $this->strUrlPagine.str_replace(array_keys($tabFormat), array_values($tabFormat), $url_get).$args;
    }
    return $strUrl;
  }

}

?>