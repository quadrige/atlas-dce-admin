<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief classe permettant d'afficher dans une m�me cellule un ensemble de ctrl
 *        Les ctrl contenus sont affich�s dans l'ordre d'ajout.
 *        Utiliser la classe Html entre les ctrl de saisie pour caler l'ensemble
 *        En utilisant cette classe, on permmet la gestion des droits en profondeur
 *        lorsque le mode du formulaire change.
 *        Les labelText after et before, ainsi que l'aide doivent �tre associ�s � cet objet
 *        et non aux objets contenus.
 */
class HtmlGroup extends HtmlCtrlBase
{
  /** tableau contenant un ensemble de controle html */
  var $tabCtrl;

  /** vrai si le couple (name, value) est g�n�r� en tant que htmlHidden, = faux par d�faut */
  var $bHidden;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode  Mode du controle de saisie : =0 modif, =1 lecture
   * @param name   Nom du controle de saisie
   * @param value  Valeur du controle de saisie
   * @param label  Etiquette texte associ�e au controle de saisie
   */
  function HtmlGroup($iMode, $name, $value="", $label="")
  {
    parent::HtmlCtrlBase($iMode, $name, $value, $label);
    $this->bHidden = false;
    $this->tabCtrl = array();
  }
  
  /**
   * @brief Ajoute un controle � l'ensemble
   *
   * @param oCtrl  R�f�rence vers l'objet � int�grer
   */
  function AddCtrl(&$oCtrl, &$oBlock)
  {
    $bAddCtrl = true;
    if( $oBlock != null ) {
      $bAddCtrl = $oBlock->verifFieldRight($oCtrl, $oBlock->iMode);

      // n'ajoute pas de hidden en mode lecture
      if( $oBlock->iMode>ALK_FORM_MODE_READ && $bAddCtrl==false ) {
        $oCtrl = new HtmlHidden($oCtrl->name, ALK_FIELD_NOT_VIEW);
        $bAddCtrl = true;
      }
    }
    if( $bAddCtrl == true ) {
      $this->tabCtrl[count($this->tabCtrl)] =& $oCtrl;
    }
  }

  /**
   * @brief Met � jour iMode et �ventuellement le label si ctrl obligatoire
   *
   * @param iMode  Nouveau Mode
   */
  function SetMode($iMode)
  {
    parent::setMode($iMode);
    for($i=0; $i<count($this->tabCtrl); $i++) {
      $this->tabCtrl[$i]->setMode($iMode);
    }
  }

  /**
   * @brief Retourne le nombre de ctrl affich� par le groupe de ctrl
   * 
   * @return Retourne un int 
   */
  function GetNbCtrl()
  {
  	return count($this->tabCtrl);
  }

  /**
   * @brief Retourne le code html associ� � ce controle
   */
  function GetCtrlHtml()
  {
    $strHtml = "";
    if( $this->bHidden == true ) {
      $strHtml = "<input type=\"hidden\" name=\"".$this->name."\" value=\"".$this->value."\">";
    }

    for($i=0; $i<count($this->tabCtrl); $i++) {
      $strHtml .= $this->tabCtrl[$i]->getCtrlHtml();
    }

    return $strHtml;
  }
}

?>