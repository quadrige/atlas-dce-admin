/**
 * Constructor AlkFormSheets
 * @brief Manager d'un groupe de TD repr�sentant des onglets d'un m�me panel
 * 
 * @param iSheet  Indice de l'onglet s�lectionn�
 */
AlkFormSheets.prototype.constructor = AlkFormSheets;
function AlkFormSheets(iSheet, strBaseName)
{
	this.tabTd = new Array();
	this.tabLayer = new Array();
	this.nbTd = 0;
	this.iSheet = iSheet;
	this.strBaseName = strBaseName;
}

/**
 * @method AlkFormSheets::AddTd
 * @brief Ajoute un objet TD � la collection
 * 
 * @param idTd         Identifiant du td utilis� pour l'affichage des onglets
 * @param strOtherImg  url de l'image utilis� pour l'autre �tat donn� par bSelected
 * @param bSelected    true = le td appartient � l'onglet s�lectionn�, faux sinon
 * @param iSheet       Indice de l'onglet auquel appartient le td
 */
AlkFormSheets.prototype.AddTd = function (idTd, strOtherImg, bSelected, iSheet)
{
	this.nbTd = this.nbTd + 1;

	if( !this.tabLayer[iSheet] ) {
		this.tabLayer[iSheet] = MM_findObj("sheet_"+this.strBaseName+"_"+iSheet, '');
	}

  var oTd = MM_findObj(idTd, '');
	if( !oTd ) {
	  this.tabTd[idTd] = null;
		return;
  }

  var oStyle = ( oTd.style ? oTd.style : oTd );
	if( bSelected ) {
		oTd.img = "url("+strOtherImg+")";
		oTd.imgRol = oStyle.backgroundImage;
	} else {
		oTd.img = oStyle.backgroundImage;
		oTd.imgRol = "url("+strOtherImg+")";
	}
	oTd.bSelected = bSelected;

  this.tabTd[idTd] = oTd;
}

/**
 * @method AlkFormSheets::AddTd
 * @brief permute les images src du TD pour modifier l'�tat de l'onglet associ�
 * 
 * @param idTd  Identifiant du td utilis� pour l'affichage des onglets
 */
AlkFormSheets.prototype.SwitchSrcImg = function (idTd)
{
	var oTd = this.tabTd[idTd];
	if( !oTd ) return;

  var oStyle = ( oTd.style ? oTd.style : oTd );
	if( oTd.bSelected ) {
		oStyle.backgroundImage = oTd.img;
	  oStyle.fontWeight = "normal"; 
		oTd.bSelected = false;
	} else {
		oStyle.backgroundImage = oTd.imgRol;
	  oStyle.fontWeight = "bold"; 
		oTd.bSelected = true;
	}
}

/**
 * @method AlkFormSheets::SwitchSheet
 * @brief permute l'�tat de l'onglet courant et celui donn� en param qui devient la s�lection
 * 
 * @param iSheet  Identifiant de l'onglet s�lectionn�
 */
AlkFormSheets.prototype.SwitchSheet = function(iSheet)
{
	var idTd0 = "td_"+this.strBaseName+"_"+(this.iSheet*4);
	var idTd1 = "td_"+this.strBaseName+"_"+(this.iSheet*4+1);
	var idTd2 = "td_"+this.strBaseName+"_"+(this.iSheet*4+2);
	var idTd3 = "td_"+this.strBaseName+"_"+(this.iSheet*4+3);

	this.SwitchSrcImg(idTd0);
	this.SwitchSrcImg(idTd1);
	this.SwitchSrcImg(idTd2);
	this.SwitchSrcImg(idTd3);
	this.ShowHideLayerSheet(this.iSheet, false);

	idTd0 = "td_"+this.strBaseName+"_"+(iSheet*4);
	idTd1 = "td_"+this.strBaseName+"_"+(iSheet*4+1);
	idTd2 = "td_"+this.strBaseName+"_"+(iSheet*4+2);
	idTd3 = "td_"+this.strBaseName+"_"+(iSheet*4+3);
	
	this.SwitchSrcImg(idTd0);
	this.SwitchSrcImg(idTd1);
	this.SwitchSrcImg(idTd2);
	this.SwitchSrcImg(idTd3);
	this.ShowHideLayerSheet(iSheet, true);

	this.iSheet = iSheet;	
}

/**
 * @method AlkTdSheets::ShowHideLayerSheet
 * @brief Rend visible ou non un layer associ� � l'onglet s�lectionn�
 * 
 * @param iSheet  Identifiant de l'onglet s�lectionn�
 * @param bShow   true pour rendre visible le layer, false pour le cacher
 */
AlkFormSheets.prototype.ShowHideLayerSheet = function(iSheet, bShow)
{
	var oLayer = this.tabLayer[iSheet];
	if( !oLayer ) return;
  var oStyle = ( oLayer.style ? oLayer.style : oLayer );
  if( bShow ) {
    oStyle.display = "block"; 
    oStyle.visibility = ( oLayer.style ? "visible" : "show");
  } else {
    oStyle.display = "none";
    oStyle.visibility = ( oLayer.style ? "hidden" : "hide"  );
  }
}

function AlkSheetOnMouseOver(oObject)
{
  if( oObject ) {
    if( oObject.style )
      oObject.style.cursor = 'pointer';
    else
      oObject.cursor = 'pointer';
  }
}
function AlkSheetOnMouseOut(oObject)
{
  if( oObject ) {
    if( oObject.style )
      oObject.style.cursor = '';
    else
      oObject.cursor = '';
  }
}

function AlkSheetOnClick(strName, iSheet)
{
	var oFormSheets = eval("oFormSheets_"+strName);
  if( oFormSheets.nbTd == 0 ) {
    eval("AlkFormSheetsInit_"+strName+"()");
  }
	oFormSheets.SwitchSheet(iSheet);
}
