/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Form
Module fournissant les classes d'affichage Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

/* AUTEUR: BERNARD MARTIN-RABAUD */
/* DATE DE CREATION: 25/06/00 */

// *****************************************************************
// AFFICHAGE DES COULEURS V2.0
// *****************************************************************
// Modifications par rapport à V1.6 :
// @ écriture en objets (objets principaux : palette et interface)
// @ paramétrage (palette, descro, lignes x cellules/ligne, largeur x hauteur cellules)
// + liste permettant de modifier la saturation pour obtenir des mélanges couleur-gris (palette graphique uniquement) 
// @ intégré les palettes dans PaletteCouleurs en tant qu'objet ListePalettes
// + sélectionner la saturation à la réouverture, en fonction de la couleur (uniquement palette graphique)
//
// NOTE POUR LA DÉCLARATION DES MÉTHODES D'OBJET
// - on ne peut évidemment pas appeler une méthode avant d'avoir créé l'objet (var xxx = new YYY())
// - lorsque la méthode est appelée après la déclaration du contructeur de la classe, on peut la prototyper, 
//   cad Classe.prototype.methode = function() (ce sont des méthodes de classe et non d'objet = plus léger)
// - lorsque la méthode est appelée avant la déclaration du contructeur de la classe, on ne peut pas la prototyper,
//   il faut la déclarer dans le constructeur : this.methode = une_fonction_quelconque;

// PARAMETRES DU SCRIPT (niveau webmestre)

// on peut aussi les indiquer dans couleurs.php (en ce cas, ceux-ci ont priorité sur les mêmes paramètres indiqués ici)
// palette prédéfinie, choix entre "graphique", "216" (216 couleurs HTML), "256" (256 couleurs) et "gris (dégradés du noir au blanc)
var g_palette = "256";

//var g_url_img_vide déclarés dans la classe php

// marges horizontales de la palette de couleurs
// espace entre cellules en pixels
var g_esp_cellules = 1;
// chemin et fichier image vide (relatif à couleurs.php)
var g_marge_palette = 0;
// true : affiche un message d'alerte en cas d'erreur
var g_alertes = true;

// création des 2 objets de base
//var ui;
//var pal;


// Voici 4 palettes prédéfinies, avec 4 arguments pour chaque palette :
// son nom, un descriptif pour afficher dans la liste, 
// les dimensions de la palette (lignes x cellules par ligne), les dimensions de chaque cellule (largeur x hauteur en pixels)
//pal.ajouterPalette("graphique", "palette graphique", "24x16", "20x15");
//pal.ajouterPalette("216", "216 couleurs HTML", "18x12", "17x11");
//pal.ajouterPalette("256", "256 couleurs", "18x16", "13x11");
//pal.ajouterPalette("gris", "dégradés de gris", "24x16", "20x15");

// FONCTION PRINCIPALE D'AFFICHAGE



// CLASSE PALETTECOULEURS

// Cette classe a pour but de définir et d'afficher la palette de couleurs dans la partie gauche de la page.
// Elle communique avec le script pour :
// - savoir quelle doit être la palette utilisée,
// - recevoir les paramètres de la palette,
// - afficher la palette
// Elle communique avec l'objet InterfaceCouleurs pour :
// - communiquer ou recevoir la couleur sélectionnée,
// - communiquer la couleur de survol,
// - recevoir le numéro de palette en cas de changement de palette

PaletteCouleurs.prototype.constructor = PaletteCouleurs;

function PaletteCouleurs(guid, strNom, noneAllowed, strCouleur) {
  this.nom = strNom;
  this.guid = guid;
  this.nblignes = null;
  this.cellsparligne = null;
  this.largcellule = 20;
  this.hautcellule = 12;
  this.saturation = null;
  this.liste_palettes = new Array();
  this.erreur = false;
  this.ajouterPalette = PaletteCouleurs_ajouterPalette;
  this.oDiv = document.getElementById("allColorPallet_"+this.guid);
  this.bOpen = false;
  this.ui = new InterfaceCouleurs(this, this.guid, this.nom, noneAllowed, strCouleur);
  eval("oColorInter_"+guid+" = this.ui;");

  this.ajouterPalette("graphique", "palette graphique", "24x16", "12x8");
  //this.ajouterPalette("216", "216 couleurs HTML", "18x12", "17x11");
  //this.ajouterPalette("256", "256 couleurs", "18x16", "13x11");
  //this.ajouterPalette("gris", "dégradés de gris", "24x16", "20x15");
}


  // méthodes publiques
  


  function PaletteCouleurs_ajouterPalette(nom, descro, dims_palette, dims_cellules) {
    // ajoute une palette prédéfinie
    this.liste_palettes[this.liste_palettes.length] = new PalettePredefinie(nom, descro, dims_palette, dims_cellules);
  }

  PaletteCouleurs.prototype.OpenCloseColorPallet = function (){
    if (this.bOpen) this.HideColorPallet();
    else this.OpenColorPallet();
  } 

  PaletteCouleurs.prototype.HideColorPallet = function () {
    this.bOpen = false;
    var oStyle = ( this.oDiv.style ? this.oDiv.style : this.oDiv );
    oStyle.display = "none"; 
  }

  PaletteCouleurs.prototype.OpenColorPallet = function () {

    // sélection de la palette
    this.selectionnerPalette();
    // fonctions d'affichage
    var strHtml = this.ui.afficher() + this.afficher();
    // corrige la largeur de la palette (dans le style css)
    this.corrigerLargeur();
    // si le champ d'appel (fenêtre appelante) avait déjà une couleur,
    // on pré-sélectionne cette couleur (initPage() a affecté g_couleur)
    this.ui.choisirCouleur();

    OpenWriteToLayer(document, "colorPallet_"+this.guid);
    WriteToLayer(document, "colorPallet_"+this.guid, strHtml);
    CloseWriteToLayer(document, "colorPallet_"+this.guid);

    var oStyle = ( this.oDiv.style ? this.oDiv.style : this.oDiv );
    oStyle.display = "block"; 
    this.bOpen = true;
  }

  PaletteCouleurs.prototype.selectionnerPalette = function() {
    // sélectionne la palette et définit ses paramètres
    
    // en cas de changement de palette, c'est ui (InterfaceCouleurs) 
    // qui sait quelle est la palette à charger
    this.palette = this.ui.donnerPalette();
    // si ui ne sait pas, c'est que la fenêtre vient d'être ouverte,
    if (this.palette == null) {
      // s'il n'y a qu'une seule palette prédéfinie, on prend celle-là  
      if (this.liste_palettes.length == 1) this.palette = 0;
      // sinon, on prend celle qui est définie par la variable globale g_palette
      else if (g_palette) this.palette = this.chercherIdPalette(g_palette);
      // sinon, message d'erreur
      else this.erreur = "aucune_palette";
    }
    
    // si on a une palette, on lit ses dimensions et celles des cellules et éventuellement sa saturation
    if (this.palette != null) {
      // récupère la saturation dans l'objet ui (InterfaceCouleurs), en cas de changement de palette
      //this.saturation = this.ui.donnerSaturation();
      // si pas de changement de palette, ou pas de saturation, elle vaudra 255
      if (this.saturation == null) this.saturation = 255;
      var dims = this.liste_palettes[this.palette].extraireDimsPalette();
      if (dims) {
        this.nblignes = dims[0];
        this.cellsparligne = dims[1];
      
        dims = this.liste_palettes[this.palette].extraireDimsCellules();
        if (dims) {
          this.largcellule = dims[0];
          this.hautcellule = dims[1];
        }
        else this.erreur = "dims_cellules";
      }
      else this.erreur = "dims_palette";
    }
  }  


  PaletteCouleurs.prototype.afficher = function() {
    // affiche la palette de couleurs à gauche
    var html = "";
    if( this.erreur ) {
      html = this.getError();
    } else {
      // création de la palette
      var palette = this.creationPalette();
      if( this.erreur ) {
        html = this.getError();
      } else {
        // affichage de la palette
        for (var i=0;i<this.nblignes;i++) {
          html += "<div>";
          for (var j=0;j<this.cellsparligne;j++) 
            html += this.afficherCellule(palette[i][j]);
          html += "</div>";
        }
      }
    }
    return html;
  }

  PaletteCouleurs.prototype.corrigerLargeur = function() {
    // corrige la largeur de la palette (dans le style css)
    if( this.palette != null ) { 
      var oWriteDiv = document.getElementById("colorPallet_"+this.guid);
      var iWidthPallet = this.cellsparligne * (this.largcellule+g_esp_cellules);
      var iHeightPallet = this.nblignes * (this.hautcellule+g_esp_cellules) + (g_marge_palette * 2);
      var oStyle = ( oWriteDiv.style ? oWriteDiv.style : oWriteDiv );
      oStyle.width = iWidthPallet + "px";

      /*var oPosDiv = document.getElementById("allColorPallet_"+this.nom);
      oStyle = ( oPosDiv.style ? oPosDiv.style : oPosDiv );
      var iTop = oPosDiv.offsetTop;
      var iLeft = oPosDiv.offsetLeft;

      var iTopNew = iTop;
      if( iTop+iHeightPallet > window.innerHeight ) {
        iTopNew = Math.max(0, window.innerHeight-iHeightPallet);
      }
      var iLeftNew = iLeft;
      if( iLeft+iWidthPallet > window.innerWidth ) {
        iLeftNew = Math.max(0, window.innerWidth-iWidthPallet);
      }
      oStyle.top = (parseInt(oStyle.top)-(iTop-iTopNew))+"px";
      oStyle.left = (parseInt(oStyle.left)-(iLeft-iLeftNew))+"px";
      oStyle.width = (iWidthPallet + 4) + "px";
      oStyle.height = ( iHeightPallet + 4) + "px";*/
  
    }
  } 
  
  
  // méthodes publiques appelées par l'objet InterfaceCouleurs
  
  PaletteCouleurs.prototype.afficherListePalettes = function() {
    // affiche (dans la partie droite de la page) une liste des palettes prédéfinies
    // cette méthode est appelée par l'objet InterfaceCouleurs)
    
    var html = "";
    if (this.liste_palettes.length > 1) {
      html += "<div><select name=\"palettes\" size=\"1\" onchange=\"oColorInter_"+this.guid+".rechargerPage(this.value, 100)\">\n";
      for (var i=0;i<this.liste_palettes.length;i++) 
        html += this.liste_palettes[i].ecrireOption(i, this.palette);
      html += "</select></div>";
    }
    return html;
  }

  PaletteCouleurs.prototype.estPaletteGraphique = function() {
    // indique si la palette courante est la palette graphique
    return (this.liste_palettes[this.palette].nom == "graphique");
  }


  // méthodes privées de création de palette
  
  PaletteCouleurs.prototype.chercherIdPalette = function(nom_palette) {
    // cherche l'indice d'une palette dans les palettes prédéfinies connaissant son nom
    for (var i=0;i<this.liste_palettes.length;i++) {
      if (nom_palette == this.liste_palettes[i].nom) return i;
    }
    return null;
  }

  
  PaletteCouleurs.prototype.creationPalette = function() {
    // création d'une palette prédéfinie par le paramètre g_palette
    // retourne la palette sous forme de tableau à 2 dimensions

    switch (this.liste_palettes[this.palette].nom) {
      case "graphique" :  var palette = this.creationPaletteGraphique(); break;
      case "216" :    var palette = this.creation216Couleurs(); break;
      case "256" :    var palette = this.creation256Couleurs(); break;
      case "gris" :    var palette = this.creationPaletteGris(); break;
      default :  var palette = null; this.erreur = "palette_nulle";
    }
    return palette;
  }

  PaletteCouleurs.prototype.creationPaletteGraphique = function() {
    // création d'une palette graphique par teintes et luminosités progressives
    // la saturation est fixe et donnée par this.saturation
  
    var palette = new Array();
    var tnt = 0;
    // calcul du pas de la teinte et de la luminosité
    var dt = Math.round(255/(this.nblignes-1));
    var dl = Math.round(255/this.cellsparligne);
    // nblignes-1 lignes de couleurs par teintes progressives
    for (var i=0;i<this.nblignes-1;i++) {
      palette[i] = new Array();
      var lum = 0;
      // cellsparligne cellules de couleur par luminosités progressives
      for (var j=0;j<this.cellsparligne;j++) {
        var rvb = TSLenRVB(tnt, this.saturation, lum);
        palette[i][j] = RVBenCodeCouleur(rvb.r, rvb.v, rvb.b);
        lum += dl;
        if (lum > 255) lum = 255;
      }
      tnt += dt;
      if (tnt > 255) tnt = 255;
    }
    // plus une ligne de gris, du noir au blanc
    i = this.nblignes-1;
    var dl = Math.round(255/(this.cellsparligne-1));
    palette[i] = new Array();
    lum = 0;
    for (j=0;j<this.cellsparligne;j++) {
      rvb = TSLenRVB(0, 0, lum); 
      palette[i][j] = RVBenCodeCouleur(rvb.r, rvb.v, rvb.b);
      lum += dl;
      if (lum > 255) lum = 255;
    }
    return palette;
  }

  PaletteCouleurs.prototype.creation216Couleurs = function() {
    // création de la palette de 216 couleurs HTML (avec 18 lignes x 12 cellules)

    var palette = new Array();
    var pas = 51;
    var rouge = 0;
    var vert = 0;
    var bleu = 0;
    // nb_lignes lignes de couleurs
    for (var i=0;i<this.nblignes;i++) {
      palette[i] = new Array();
      // cellsparligne cellules de couleur par luminosités progressives
      for (var j=0;j<this.cellsparligne;j++) {
        palette[i][j] = RVBenCodeCouleur(rouge, vert, bleu);
        if (bleu == 255) {
          if (vert == 255) {
            rouge += pas; vert = 0; bleu = 0;
          }
          else {
            vert += pas; bleu = 0;
          }
        }
        else bleu += pas;
      }
    }
    return palette;
  }

  PaletteCouleurs.prototype.creation256Couleurs = function() {
    // création de la palette de 256 couleurs Windows (avec 16 lignes x 16 cellules)

    var palette = new Array();
    var pasb = 85;
    var pasrv = 255/7;
    var rouge = 0;
    var vert = 0;
    var bleu = 0;
    // nblignes lignes de couleurs
    for (var i=0;i<this.nblignes;i++) {
      palette[i] = new Array();
      // cellsparligne cellules de couleur par luminosités progressives
      for (var j=0;j<this.cellsparligne;j++) {
        palette[i][j] = RVBenCodeCouleur(Math.round(rouge), Math.round(vert), bleu);
        if (bleu == 255) {
          if (Math.round(vert) == 255) {
            rouge += pasrv; vert = 0; bleu = 0;
          }
          else {
            vert += pasrv; bleu = 0;
          }
        }
        else bleu += pasb;
      }
    }
    return palette;
  }

  PaletteCouleurs.prototype.creationPaletteGris = function() {
    // création de la palette de dégradés de gris 

    var palette = new Array();
    var pas = 256 / (this.nblignes * this.cellsparligne -1);
    var rouge = 0;
    var vert = 0;
    var bleu = 0;
    // nblignes lignes de couleurs
    for (var i=0;i<this.nblignes;i++) {
      palette[i] = new Array();
      // cellsparligne cellules de couleur par luminosités progressives
      for (var j=0;j<this.cellsparligne;j++) {
        palette[i][j] = RVBenCodeCouleur(Math.round(rouge), Math.round(vert), Math.round(bleu));
        rouge += pas;
        vert += pas;
        bleu += pas;
        if (rouge > 255) {
          rouge = vert = bleu = 255;
        }
      }
    }
    return palette;
  }
  
  
  // méthodes privées d'affichage

  PaletteCouleurs.prototype.afficherCellule = function(rvb) {
    // affiche une case couleur de la palette de couleurs

    var texte = "<div style='width:"+this.largcellule+"px; height:"+this.hautcellule+"px; margin-right:"+g_esp_cellules+"px; margin-bottom:"+g_esp_cellules+"px; background-color:#"+rvb+"; float:left'>";
    texte += "<a href=\"javascript:oColorInter_"+this.guid+".choisirCouleur('"+rvb+"')\" title='#" + rvb + "'";
    texte += " onmouseover=\"oColorInter_"+this.guid+".modifierTemoin('temoin_survol_"+this.guid+"', '" + rvb + "')\"";
    texte += " onmouseout=\"oColorInter_"+this.guid+".modifierTemoin('temoin_survol_"+this.guid+"', 'ffffff')\">";
    texte += "<img  src='"+g_url_img_vide+"' width='"+this.largcellule+"' height='"+this.hautcellule+"' border='0' alt='#"+rvb+"'/>";
    texte += "</a></div>"; 
    return texte;
  }
  

  PaletteCouleurs.prototype.getError = function() {
    // affiche l'erreur trouvée
    var html = "";
    switch (this.erreur) {
      case "dims_palette"    : html += "Erreur dans les dimensions de la palette (lignes, cellules par ligne)."; break;
      case "dims_cellules"  : html += "Erreur dans les dimensions des cellules."; break;
      case "palette_nulle"  : html += "Erreur : la palette sélectionnée n'existe pas."; break;
      case "aucune_palette"  : html += "Erreur : aucune palette sélectionnée."; break;
    }
   
    return html;
  }


// FIN DE LA CLASSE PALETTECOULEURS


// CLASSE PALETTEPREDEFINIE

function PalettePredefinie(nom, descro, dims_palette, dims_cellules) {
  this.nom = nom;
  this.descro = descro;
  this.dims_palette = dims_palette;
  this.dims_cellules = dims_cellules;
}

  PalettePredefinie.prototype.extraireDimsPalette = function() {
    // extrait les dimensions de la palette de la propriété dims_palette
    var dims = this.dims_palette.match(/^(\d+)\s?x\s?(\d+)$/);
    if (dims) return Array(parseInt(dims[1]), parseInt(dims[2]));
  }

  PalettePredefinie.prototype.extraireDimsCellules = function() {
    // extrait les dimensions des cellules de la propriété dims_cellules
    var dims = this.dims_cellules.match(/^(\d+)\s?x\s?(\d+)$/);
    if (dims) return Array(parseInt(dims[1]), parseInt(dims[2]));
  }

  PalettePredefinie.prototype.ecrireOption = function(index, palette_cour) {
    // écrire une option de liste dans la liste des palettes
    return "<option value=\"" + index + "\"" + ((index == palette_cour) ? " selected" : "") + "> " + this.descro + "</option>\n";
  }
    
// FIN DE LA CLASSE PALETTEPREDEFINIE

    

// CLASSE INTERFACECOULEURS

// Cette classe fait l'interface entre la fenêtre, la fenêtre appelante et l'utilisateur.
// Elle affiche aussi les éléments du formulaire à droite de la fenêtre.
// Elle communique avec le script pour :
// - récupérer les paramètres de l'url au chargement de la page,
// - afficher les boîtes,
// - renvoyer la couleur sélectionnée à la fenêtre appelante,
// - recharger la page avec les paramètres dans l'url en cas de changement de palette
// Elle communique avec l'objet PaletteCouleurs pour :
// - communiquer ou recevoir la couleur sélectionnée,
// - recevoir la couleur de survol 

InterfaceCouleurs.prototype.constructor = InterfaceCouleurs;

function InterfaceCouleurs(oPallet, guid, strNom, noneAllowed, strCouleur ) {
  this.guid = guid;
  this.nom = strNom;
  //this.opener_form = null;
  //this.opener_input = null;
  this.oPallet = oPallet;
  this.oCtrlHidden = document.getElementById(guid);
  this.oCtrlTxt    = document.getElementById("value_"+guid);
  this.oCtrlColor  = document.getElementById("color_"+guid);
  this.couleur = strCouleur; // donnée importante : c'est la couleur sélectionnée par le script
  this.palette = null;       // non null, si on recharge la page après avoir sélectionné une palette
  this.saturation = 100;
  this.noneAllowed = noneAllowed;
  this.change_palette = false;  // vrai si on a changé de palette
  this.donnerPalette = InterfaceCouleurs_donnerPalette;
}


  // méthodes publiques
  
  InterfaceCouleurs.prototype.afficher = function() {
    // affiche les temoins de sélection
    var RVB = codeCouleurEnRVB(this.couleur);
    var rouge = (new String(RVB.rouge).length==1 ? "00" : (new String(RVB.rouge).length==2 ? "0" : ""))+RVB.rouge;
    var vert = (new String(RVB.vert).length==1 ? "00" : (new String(RVB.vert).length==2 ? "0" : ""))+RVB.vert;
    var bleu = (new String(RVB.bleu).length==1 ? "00" : (new String(RVB.bleu).length==2 ? "0" : ""))+RVB.bleu;
    
    var html = "<div id='boites"+this.guid+"' style='text-align:center;margin-bottom:2px; border-bottom:1px solid #999999'>";
    // la boîte indiquant le code couleur de la couleur sélectionnée
    html += "<div align='center'>";
    html += "Hexa&nbsp;#<input id='temoin_txt_"+this.guid+"' type='text' class='formCtrl' style='font-size:9px;height:11px;vertical-align:top; width:40px;text-align:center'" +
        " value='"+this.couleur+"'" +
        " onblur=\"this.value = this.value.toUpperCase();oColorInter_"+this.guid+".choisirCouleur(this.value);\">";
    
    html += "&nbsp;&nbsp;&nbsp;&nbsp;RVB&nbsp;" +
        "<input id='temoin_rouge_"+this.guid+"' type='text' class='formCtrl' style='color:red;font-size:9px;height:11px;vertical-align:top; width:20px;text-align:center'" +
        " maxlength='3' value='"+rouge+"'" +
        " onblur=\"oColorInter_"+this.guid+".calculerCouleurRVB('temoin_rouge_"+this.guid+"', 'temoin_vert_"+this.guid+"', 'temoin_bleu_"+this.guid+"');\">";
    html += "&nbsp;&nbsp;" +
        "<input id='temoin_vert_"+this.guid+"' type='text' class='formCtrl' style='color:green;font-size:9px;height:11px;vertical-align:top; width:20px;text-align:center'" +
        " maxlength='3' value='"+vert+"'" +
        " onblur=\"oColorInter_"+this.guid+".calculerCouleurRVB('temoin_rouge_"+this.guid+"', 'temoin_vert_"+this.guid+"', 'temoin_bleu_"+this.guid+"');\">";
    html += "&nbsp;&nbsp;" +
        "<input id='temoin_bleu_"+this.guid+"' type='text' class='formCtrl' style='color:blue;font-size:9px;height:11px;vertical-align:top; width:20px;text-align:center'" +
        " maxlength='3' value='"+bleu+"'" +
        " onblur=\"oColorInter_"+this.guid+".calculerCouleurRVB('temoin_rouge_"+this.guid+"', 'temoin_vert_"+this.guid+"', 'temoin_bleu_"+this.guid+"');\">";
    
    html += "</div>";
    html += "<div align='center' style='margin:auto;margin-top:3px;margin-bottom:3px;width:150px;'>";
    html += "<div id='temoin_survol_"+this.guid+"'" +
      " style='width:30px; height:12px; background-color:#ffffff; border:1px solid #000000; float:left;'></div>";
    html += "<div id='temoin_"+this.guid+"'" +
      " style='width:30px; height:12px; background-color:#"+this.couleur+"; border:1px solid #000000; margin-left:4px; float:left;'>" +
      "</div>";
    html += "&nbsp;&nbsp;<a href='javascript:oColorInter_"+this.guid+".envoyerCouleur()'>Valid</a>";
    if( this.noneAllowed ) {
      html += "&nbsp;&nbsp;<a href='javascript:oColorInter_"+this.guid+".aucuneCouleur()'>None</a>";
    }
    html += "</div>";
    html += "</div>";

    return html;
  }
  
  function InterfaceCouleurs_donnerPalette() {
    // communique la palette, dans le cas où on a changé de palette, sinon communique null
    if (this.change_palette) return this.palette;
    else return null;
  }
  
  
  // méthodes publiques événementielles  
  
  InterfaceCouleurs.prototype.envoyerCouleur = function() {
    // transmet la couleur sélectionnée au formulaire appelant
    // appelée par le bouton "Envoyer"
    //
    if (this.couleur == null) {
      alert("No color selected.");
    } else if( this.oCtrlHidden == null ) {
      alert("Le contrôle caché n'a pas été transmis.");
    } else {
      this.SetValues(this.couleur);
      this.oPallet.HideColorPallet();
    }
    this.oPallet.HideColorPallet();
  }

  InterfaceCouleurs.prototype.aucuneCouleur = function() {
    // fait un reset et remet le temoin en gris
    // appelée par le bouton "Annuler"
    this.couleur = "";

    this.SetValues(this.couleur);
    this.oPallet.HideColorPallet();
  }

  InterfaceCouleurs.prototype.SetValues = function(rvb) {
    if( this.oCtrlHidden ){
      this.oCtrlHidden.value = ( rvb == "" ? "" : "#"+rvb );
      if ( this.oCtrlHidden.onchange )
        this.oCtrlHidden.onchange();
    }

    if( this.oCtrlTxt ) {
      var strHtml = ( rvb == "" ? "None" : "#"+rvb );
      strHtml = "<a href='javascript:OpenColorPallet_"+this.guid+"()'>"+strHtml+"</a>";
      OpenWriteToLayer(document, this.oCtrlTxt.id);
      WriteToLayer(document, this.oCtrlTxt.id, strHtml);
      CloseWriteToLayer(document, this.oCtrlTxt.id);
    }
    
    if( this.oCtrlColor ) {
      var oStyle = ( this.oCtrlColor.style ? this.oCtrlColor.style : this.oCtrlColor );
      oStyle.backgroundColor = ( rvb == "" ? "#ffffff" : "#"+rvb );
    }
  } 

  InterfaceCouleurs.prototype.choisirCouleur = function() {
    // affiche le code de la couleur rvb dans la boîte code_couleur
    // ainsi que ses composantes rouge, vert, bleu et le temoin couleur
    // appelée lors d'un clic sur une cellule de la palette de gauche
    
    // on vérifie s'il y a un argument
    if (arguments.length) var rvb = arguments[0];
    // sinon on prend this.couleur
    else rvb = this.couleur;
    if (rvb != null) {
      this.couleur = rvb;
      this.modifierTemoin("temoin_"+this.guid, rvb);
      this.modifierTemoin("temoin_survol_"+this.guid, rvb);
      var oCtrlTxt = document.getElementById("temoin_txt_"+this.guid);
      if( oCtrlTxt )
        oCtrlTxt.value = rvb;
      var RVB = codeCouleurEnRVB(this.couleur);
      var rouge, vert, bleu;
      if ( rouge = document.getElementById("temoin_rouge_"+this.guid) ){
        rouge.value = (new String(RVB.rouge).length<3 ? (new String(RVB.rouge).length<2 ? "00" : "0") : "")+new String(RVB.rouge);
      }
      if ( vert = document.getElementById("temoin_vert_"+this.guid) ){
        vert.value = (new String(RVB.vert).length<3 ? (new String(RVB.vert).length<2 ? "00" : "0") : "")+new String(RVB.vert);
      }
      if ( bleu = document.getElementById("temoin_bleu_"+this.guid) ){
        bleu.value = (new String(RVB.bleu).length<3 ? (new String(RVB.bleu).length<2 ? "00" : "0") : "")+new String(RVB.bleu);
      }
    }
  }

  // méthodes privées de lecture / écriture / contrôle
  
  InterfaceCouleurs.prototype.modifierTemoin = function(id) {
    // modifie la couleur des boîtes temoin
    // s'il y a qu'un seul argument, on prendra la couleur g_couleur
    // sinon on prendra la couleur donnée par ce 2e argument
    var couleur = this.couleur;
    if (arguments.length == 2) {
      couleur = arguments[1];
    }
    var oDivColor = document.getElementById(id);
    if( oDivColor ) {
      var oStyle = ( oDivColor.style ? oDivColor.style : oDivColor );
      oStyle.backgroundColor = "#"+couleur;
    }
  }

  InterfaceCouleurs.prototype.calculerCouleurRVB = function(idRouge, idVert, idBleu) {
    var oRouge = document.getElementById(idRouge);
    var oVert = document.getElementById(idVert);
    var oBleu = document.getElementById(idBleu);
    if( !(oRouge && oVert && oBleu) )
      return;
    if ( isNaN(parseInt(oRouge.value)) || parseInt(oRouge.value)<0 || parseInt(oRouge.value)>255 ){
      oRouge.value = "000";
    }
    if ( isNaN(parseInt(oVert.value)) || parseInt(oVert.value)<0 || parseInt(oVert.value)>255 ){
      oVert.value = "000";
    }
    if ( isNaN(parseInt(oBleu.value)) || parseInt(oBleu.value)<0 || parseInt(oBleu.value)>255 ){
      oBleu.value = "000";
    }
    var couleurHexa = RVBenCodeCouleur(parseInt(oRouge.value), parseInt(oVert.value), parseInt(oBleu.value));
    var oCtrlTxt = document.getElementById("temoin_txt_"+this.guid);
    if ( oCtrlTxt ){
      oCtrlTxt.value = couleurHexa;
      if ( oCtrlTxt.onblur )
        oCtrlTxt.onblur();
    }
  }
  
// FIN DE LA CLASSE INTERFACECOULEURS



// FONCTIONS UTILITAIRES POUR LA CLASSE PALETTECOULEURS

function TSLenRVB(tnt, sat, lum) {
  // conversion TSL -> RVB - arguments : teinte, saturation, luminosité
  // retourne des composantes rouge, vert, bleu (en 0 - 255) sous forme d'objet
  // adaptation du script http://membres.lycos.fr/interaction/Package2/Couleur/couleurs5.html
  
  // convertit teinte en float [0-6[ et saturation, luminosité en float [0-1[
  tnt = (tnt == 255) ? 0 : tnt * 6 / 255;
  sat = sat / 255;
  lum = lum / 255;
  var r = 0;
  var v = 0;
  var b = 0;
  
  var w = (lum <= 0.5) ? lum * (1 + sat) : lum + sat - lum * sat;
  // si w = 0, ça correspond à #000000 (w < 0 ne peut pas exister)
  if (w > 0) {
    var m = lum * 2 - w;
    var sv = (w - m) / w;
    var sext = Math.floor(tnt);
    var fract = tnt - sext;
    var vsf = w * sv * fract;
    var mid1 = m + vsf;
    var mid2 = w - vsf;
    
    switch (sext) {
      case 0 :  r = w;    v = mid1;  b = m; break;
      case 1 :  r = mid2;  v = w;    b = m; break;
      case 2 :  r = m;    v = w;    b = mid1; break;
      case 3 :  r = m;    v = mid2;  b = w; break;
      case 4 :  r = mid1;  v = m;    b = w; break;
      case 5 :  r = w;    v = m;    b = mid2;
    }
    
    // convertit r, v, b en entiers [0-255]
    r = Math.round(r * 255);
    v = Math.round(v * 255);
    b = Math.round(b * 255);
  }

  return {r:r, v:v, b:b};
}

// FONCTIONS UTILITAIRES POUR LA CLASSE INTERFACECOULEURS

function changerClasse(id, classe) {
  // change la classe d'une image identifiée par son id
  if (document.getElementById && (img = document.getElementById(id))) img.className = classe;
}

function estCodeCouleur(couleur) {
  // contrôle un code couleur (hexa HHHHHH)
  return (couleur.search(/[0-9A-Fa-f]{6}/) != -1);
}

function octetEnHexa(nombre) {
  // convertit un nombre décimal [0, 255] en une chaine hexadécimale ("00" à "FF")
  var cars_hexa = "0123456789ABCDEF";
  return cars_hexa.charAt(Math.floor(nombre / 16)) + cars_hexa.charAt(nombre % 16);
}

function RVBenCodeCouleur(rouge, vert, bleu) {
  // crée un code couleur à partir des trois couleurs rouge, vert, bleu
  var cars_hexa = "0123456789ABCDEF";
  var code = cars_hexa.charAt(Math.floor(rouge / 16)) + cars_hexa.charAt(rouge % 16);
  code += cars_hexa.charAt(Math.floor(vert / 16)) + cars_hexa.charAt(vert % 16);
  code += cars_hexa.charAt(Math.floor(bleu / 16)) + cars_hexa.charAt(bleu % 16);
  return code;
}

function estOctet(valeur) {
  // vérifie si une valeur est un nombre compris entre 0 et 255
  if (isNaN(valeur) || (valeur < 0) || (valeur > 255)) return false;
  else return true;
}

function alerte(message) {
  // affiche un alert si g_alerts = true
  if (g_alertes) alert(message);
}

function codeCouleurEnRVB(couleur) {
  // convertit un code couleur en composantes RVB
  // retourne un objet {rouge, vert, bleu}
  if (couleur==""){
    return {rouge: "", vert: "", bleu: ""};
  }
  var composantes = couleur.match(/(#?[0-9A-F]{2})([0-9A-F]{2})([0-9A-F]{2})/i);
  return {rouge: parseInt(composantes[1], 16), vert: parseInt(composantes[2], 16), bleu: parseInt(composantes[3], 16)};
}

function calculSaturation(code_couleur) {
  // calcul la saturation d'un code couleur
  // retourne une saturation entre 0 et 100 (0, 10, 20...100)
  
  var RVB = codeCouleurEnRVB(code_couleur);
  var r = RVB.rouge / 255;
  var v = RVB.vert / 255;
  var b = RVB.bleu / 255;
  
  var max = Math.max(r, v);
  max = Math.max(max, b);
  var min = Math.min(r, v);
  min = Math.min(min, b);

  var lum = (min + max) / 2;
  if (lum < 0) return;
  
  var diff = max - min;
  var sat = 0;
  // si diff = 0, c'est le cas du gris (tnt=0, sat=0)
  if (diff > 0) {
    if (lum < 0.5) sat = diff / (min + max);
    else sat = diff / (2 - min - max);
    sat = Math.round(sat * 10) * 10;
  }
  return sat;
}
