<?php
include_once("htmlpanel.class.php");

if( !defined("ALK_HTMLFORM_1L2C") ) { define("ALK_HTMLFORM_1L2C", 0); }
if( !defined("ALK_HTMLFORM_2L1C") ) { define("ALK_HTMLFORM_2L1C", 1); }
if( !defined("ALK_HTMLFORM_1L1C") ) { define("ALK_HTMLFORM_1L1C", 2); }

/**
 * @brief classe d'affichage des formulaires
 */
class HtmlBlock extends HtmlPanel
{
  /** mode du bloc : =1 ajout, =2 modi, =3 lecture */
  var $iMode;

  /** titre du bloc */
  var $title;

  /** description du bloc */
  var $desc;

  /** R�f�rence vers le formulaire contenant ce bloc*/
  var $oForm;

  /** Indice du layerForm contenant ce block (=-1 par d�faut) */
  var $iSheet;
  
  /** Ensemble des controles constituant le bloc */
  var $tabHtmlCtrl; 

  /** largeur d�di�e au label du controle  */
  var $iWidthLabel;
  
  /** largeur d�di�e au controle de saisie */
  var $iWidthCtrl;

  /** vrai si le titre du bloc est principal, faux pour un titre normal (par d�faut) */
  var $bMainTitle;

  /** classe css sur les titres de bloc */
  var $cssBlockTitle;
  var $cssBlockMainTitle;
  var $cssBlockLabel;
  var $cssBlockCtrl;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode       Mode du controle de saisie : =1 ajout, =2 modi, =3 lecture
   * @param name        identifiant du bloc
   * @param strTitle    titre du bloc
   * @param strDesc     descriptif du bloc
   * @param iWidthLabel largeur d�di�e au label du controle (=180 par d�faut)
   * @param iWidthCtrl  largeur d�di�e au controle de saisie (=390 par d�faut)
   * @param bMaintitle  vrai si le titre du bloc est principal, faux pour un titre normal (par d�faut)
   * @param iSheet      Indice de l'onglet contenant ce block (=-1 par d�faut)
   */
  function HtmlBlock(&$oForm, $iMode, $name, $strTitle="", $strDesc="",
                    $iWidthLabel="180", $iWidthCtrl="390", $bMaintitle=false, $iSheet=0)
  {
    parent::HtmlPanel($name);

    $this->iMode  = $iMode;
    $this->title  = $strTitle;
    $this->desc   = $strDesc;
    $this->iSheet = $iSheet;

    $this->oForm =& $oForm;
    $this->tabHtmlCtrl = array();

    $this->iWidthLabel      = $iWidthLabel;
    $this->iAlign           = "";
    $this->iWidthInside     = "";
    $this->iWidthCtrl       = $iWidthCtrl;
    $this->bMainTitle       = $bMaintitle;

    $this->cssBlockTitle     = "formBlockTitle";
    $this->cssBlockMainTitle = "formBlockMainTitle";
    $this->cssBlockLabel     = "formBlockLabel";
    $this->cssBlockCtrl      = "formBlockCtrl";
  }

  /**
   * @brief Verifie le droit de l'utilisateur pour le champ actuel
   *
   * @return Retourne un bool
   */
  function VerifFieldRight(&$oCtrl, $iModeConsult)
  {
    // regarde la valeur de la constante construite de la mani�re suivante :
    // majuscule([nom formulaire]_[nom champ])
    // valeur possible de la constante : 
    //   0 = aucun droit (le champ n'est pas visible)
    //   1 = lecture
    //   2 = modif (le champ est accessible en saisie pour la modification si ce bit est � 1)
    //   4 = ajout (le champ est accessible en saisie pour l'ajout si ce bit est � 1)
    $bAddCtrl = false;
    $strDroitChamp = "";
    if( $this->oForm != null ) {
      $strDroitChamp = strtoupper($this->oForm->name."_".$this->iSheet."_".$oCtrl->name);
    } else {
      // le champ est visible, avec le mode fix� par d�faut � la construction du controle
      return true;
    }

    // Si la constante n'existe pas => pas de gestion de droit donc tous les droits
    $iDroitAgent = ( defined($strDroitChamp) ? constant($strDroitChamp) : ( $this->oForm->bVerifFieldRight ? 0 : 7) );
    switch( $iModeConsult ) {
    case ALK_FORM_MODE_READ: // mode lecture
      if( $iDroitAgent & 1 == 1 ) {
        $oCtrl->setMode(1);
        $bAddCtrl = true;
      }
      break;
      
    case ALK_FORM_MODE_UPDATE: // mode modif
      if( ($iDroitAgent & 2) == 2 ) {
        $oCtrl->setMode(0);
        $bAddCtrl = true;
      } elseif( ($iDroitAgent & 1) == 1 ) {
        $oCtrl->setMode(1);
        $bAddCtrl = true;
      }
      break;
      
    case ALK_FORM_MODE_ADD: // mode ajout
      if( ($iDroitAgent & 2) == 2 || ($iDroitAgent & 4) == 4 ) {
        $oCtrl->setMode(0);
        $bAddCtrl = true;
      } elseif( ($iDroitAgent & 1) == 1 ) {
        $oCtrl->setMode(1);
        $bAddCtrl = true;
      }
      break;
    }
    return $bAddCtrl;
  }

  /**
   * @brief Ajoute un controle html au bloc
   *
   * @param oCtrl
   * @param strCodeChamp  Code du champ utilis� pour tester les droits [NOM FORM]_[NOM TABLE]_[NOM_CHAMP]
   * @param iTypeAff      Prend les valeurs de ALK_HTMLFORM_1L2C, ALK_HTMLFORM_2L1C ou ALK_HTMLFORM_1L1C
   */
  function AddCtrl(&$oCtrl, $iTypeAff=ALK_HTMLFORM_1L2C)
  {
    // mode de consultation du formulaire=mode de consultation du bloc
    $iModeConsult = $this->iMode;
    $bAddCtrl = false;
    if( is_array($oCtrl) ) {
      if( is_object($oCtrl[0]) && get_class($oCtrl[0]) == "htmlradio" ) {
        while( list($strKey, $oSubCtrl) = each($oCtrl) ) {
          if( is_object($oSubCtrl) ) {
            // verifie les droits sur tous les boutons radios du control
            $bAddCtrl = $this->verifFieldRight($oSubCtrl, $iModeConsult);            
          }
        }
        // tous les boutons radios ont le m�me $bAddCtrl
        // n'ajoute pas de hidden en mode lecture
        if( $iModeConsult>ALK_FORM_MODE_READ && $bAddCtrl==false ) {
          $oCtrl = new HtmlHidden($oSubCtrl->name, ALK_FIELD_NOT_VIEW);
          $bAddCtrl = true;
        }
      } elseif( is_object($oCtrl[0]) && get_class($oCtrl[0]) == "htmlcheckbox" ) {
        $oCtrlH = null;
        while( list($strKey, $oSubCtrl) = each($oCtrl) ) {
          if( is_object($oSubCtrl) ) {
            // verifie les droits sur tous les checkbox du control
            $bAddCtrl = $this->verifFieldRight($oSubCtrl, $iModeConsult);            

            // n'ajoute pas de hidden en mode lecture
            if( $iModeConsult>ALK_FORM_MODE_READ && $bAddCtrl==false ) {
              if( $oCtrlH )
                $oCtrlH->addHidden($oSubCtrl->name, ALK_FIELD_NOT_VIEW);
              else
                $oCtrlH = new HtmlHidden($oSubCtrl->name, ALK_FIELD_NOT_VIEW);
              $bAddCtrl = true;
            }
          }
        }
        if( $oCtrlH ) {
          $oCtrl = $oCtrlH;
        }
      }
    } elseif( is_object($oCtrl) ) {
      if( get_class($oCtrl) == "htmlgroup" ) {
      	$bAddCtrl = true;
      }	else {
        $bAddCtrl = $this->verifFieldRight($oCtrl, $iModeConsult);
      }

      // n'ajoute pas de hidden en mode lecture
      if( $iModeConsult>ALK_FORM_MODE_READ && $bAddCtrl==false ) {
        $oCtrl = new HtmlHidden($oCtrl->name, ALK_FIELD_NOT_VIEW);
        $bAddCtrl = true;
      }
    }
    if( $bAddCtrl == true ) {
      $iIndice = count($this->tabHtmlCtrl);
      $this->tabHtmlCtrl[$iIndice]["ctrl"] =& $oCtrl;
      $oCtrl->oBlock =& $this;
      $this->tabHtmlCtrl[$iIndice]["iTypeAff"] = $iTypeAff;

      // indique au formulaire le type de controle js n�cessaire
      $this->oForm->_SetValidator($oCtrl);
    }
  }

  /**
   * @brief Ajoute un controle � un bloc puis retourne une r�f�rence sur l'objet ctrl cr��
   *        afin de personnaliser ses propri�t�s
   *
   * @param oDataFormAttrib  R�f�rence vers l'objet de type AlkDataFormAttrib
   * @param iTypeAff         Prend les valeurs de ALK_HTMLFORM_1L2C, ALK_HTMLFORM_2L1C ou ALK_HTMLFORM_1L1C
   * @param strLabel         Etiquette texte associ� au controle (= chaine vide par d�faut)
   * @param iHeight          Hauteur du controle (=1 par d�faut)
   *                         = strPathBase pour htmlUpload
   * @param iWidth           Largeur du controle (= chaine vide par d�faut)
   *                         = strUrlBase pour htmlUpload
   * @param oParam1          = iWidthOption   pour HtmlSelect
   *                         = strNoneAllowed pour HtmlColor
   *                         = strUrlIcone pour HtmlUpload
   * @param oParam2          = nbLg pour HtmlUpload
   */
  function &AddHtmlCtrl(&$oDataFormAttrib, $iTypeAff=ALK_HTMLFORM_1L2C, $strLabel="", $iHeight="1", $iWidth="", $oParam1="", $oParam2="")
  {
    switch( strtolower($oDataFormAttrib->htmlCtrlName) ) {
    case "html":
      $oCtrl = new Html($oDataFormAttrib->value, $strLabel);
      break;
    case "htmlhidden":
      $oCtrl = new HtmlHidden($oDataFormAttrib->name, $oDataFormAttrib->value);
      break;
    case "htmltext":
      $oCtrl = new HtmlText(0, $oDataFormAttrib->name, $oDataFormAttrib->value,
                            $strLabel, $iHeight, $iWidth, $oDataFormAttrib->getMaxLength());
      break;
    case "htmlfile":
      $oCtrl = new HtmlFile(0, $oDataFormAttrib->name, $oDataFormAttrib->value,
                            $strLabel, $iHeight, $iWidth, $oDataFormAttrib->getMaxLength());
      $oCtrl->bDualMode = true;
      $this->oForm->bUpload = true;
      break;
    case "htmlselect":
      $oCtrl = new HtmlSelect(0, $oDataFormAttrib->name, $oDataFormAttrib->value, $strLabel, $iHeight, $iWidth, $oParam1);
      break;
    case "htmlcheckbox":
      $oCtrl = new HtmlCheckbox(0, $oDataFormAttrib->name, $oDataFormAttrib->value, $strLabel);
      break;
    case "htmlcheckboxgroup":
      $oCtrl = new HtmlCheckboxGroup(0, $oDataFormAttrib->name, $oDataFormAttrib->value, $strLabel);
      break;
    case "htmlradio":
      // controle non compatible et obsol�te
      die("Ne plus utiliser la classe HtmlRadio.");
      break;
    case "htmlradiogroup":
      $oCtrl = new HtmlRadioGroup(0, $oDataFormAttrib->name, $oDataFormAttrib->value, $strLabel);
      break;
    case "htmldate":
      $oCtrl = new HtmlDate(0, $oDataFormAttrib->name, $oDataFormAttrib->value, $strLabel, "/");
      break;
    case "htmlcolor":
      $oCtrl = new HtmlColor($oDataFormAttrib->name, $oDataFormAttrib->value, $strLabel, $iWidth, $iHeight, $oParam1);
      break;
    case "htmlupload":
      $strPathBase = $iHeight;
      $strUrlBase = $iWidth;
      $strUrlIcone = $oParam1;
      $nbLg = ( $oParam2 == "" ? 1 : $oParam2 );
      $oCtrl = new HtmlUpload(0, $oDataFormAttrib->name, $strLabel, $strPathBase, $strUrlBase, $strUrlIcone, $nbLg);
      $this->oForm->bUpload = true;
      break;
    case "htmldatetime":
      $oCtrl = new HtmlDateTime(0, $oDataFormAttrib->name, $oDataFormAttrib->value, $strLabel);
      break;
    case "htmlgroup":
      $oCtrl = new HtmlGroup(0, $oDataFormAttrib->name, $oDataFormAttrib->value, $strLabel);
      break;
    default:
      die("La classe ".$oDataFormAttrib->htmlCtrlName." n'est pas prise en compte par HtmlBlock::AddHtmlCtrl().");
      break;
    }
    
    if ( isset($oDataFormAttrib->label) ){
      $oCtrl->label = $oDataFormAttrib->label;
    
      if ( isset($oDataFormAttrib->glueLabel) ){
        $oCtrl->label .= $oDataFormAttrib->glueLabel;
      }
    
      if ( isset($oDataFormAttrib->label2) ){
        $oCtrl->label .= $oDataFormAttrib->label2;
      }
    }
    if ( isset($oDataFormAttrib->labelHelp) ){
      $oCtrl->labelHelp = $oDataFormAttrib->labelHelp;
    }
    
    $oCtrl->bMultiLangue = $oDataFormAttrib->bMultilingue;
    $oCtrl->tabValidator = $oDataFormAttrib->tabValidator;
    if( count($oCtrl->tabValidator)>0 && $oCtrl->tabValidator["require"] == true ) {
      $oCtrl->label .= "*";
    }
    if( count($oCtrl->tabValidator) > 0 ) {
      $oCtrl->tabValidator["form"] = $this->oForm->name;
      $oCtrl->tabValidator["iSheet"] = $this->iSheet;
    }
    $this->AddCtrl($oCtrl, $iTypeAff);

    return $oCtrl;
  }

  /**
   * @brief Retourne le code html du block complet
   *
   * @return Retourne un string
   */
  function GetHtml()
  {
    $strHtml = $this->GetHtmlTableCtrlHeader().      
      $this->GetHtmlTableCtrlTitle().
      $this->GetHtmlTableCtrlSpaceLine(4);

    for($i=0; $i< count($this->tabHtmlCtrl); $i++) {
      $oCtrl =& $this->tabHtmlCtrl[$i]["ctrl"];
      if( strtolower(get_class($oCtrl)) == "htmlhidden" ) {
        $strHtml .= $oCtrl->getHtml();
      } else {
        switch( $this->tabHtmlCtrl[$i]["iTypeAff"] ) {
        case ALK_HTMLFORM_1L2C: $strHtml .= $this->GetHtmlTableCtrlLine($oCtrl);   break;
        case ALK_HTMLFORM_2L1C: $strHtml .= $this->GetHtmlTableCtrl2Lines($oCtrl); break;
        case ALK_HTMLFORM_1L1C: $strHtml .= $this->GetHtmlTableCtrlColumn($oCtrl); break;
        }
      }
    }

    $strHtml .= $this->GetHtmlTableCtrlSpaceLine(8).
      $this->GetHtmlTableCtrlFooter();

    return $strHtml;
  }
  
  /**
   * @brief Retourne le code HTML API v2 de l'entete d'un tableau 
   *        formulaire de saisie  avec une largeur d�finie par 
   *        d�faut � 570 = 180+390
   *        deux colonnes : label - valeur suivi d'un trait
   *
   * @return Retourne un string
   */
  function GetHtmlTableCtrlHeader()
  {
    $iWidth       = ( $this->iWidthLabel=="" ? "100%" : $this->iWidthLabel );
    $iAlign       = $this->iAlign;
    $iWidthInside = $this->iWidthInside;
    return "<table border='0' cellpadding='0' cellspacing='0'".
      ($iWidth=="100%" ? " width='100%'" : "")." ".($iAlign!="" ? " align='".$iAlign."'" : "")." ".
      ($iWidthInside!="" ? " width='".$iWidthInside."'" : "")." >".
      "<tr>".
      "<td width='".$iWidth."' height='0'>" .
      "<div style='width:".$iWidth."; height:0; font-size:0'></div></td>".
      //"<img width='".$iWidth."' height='0'></td>".
      "<td width='".$this->iWidthCtrl."'></td>".
      "</tr>";
  }

  /**
   * @brief Retourne le code HTML de la fin de tableau du bloc de saisie
   *
   * @return Retourne un string
   */
  function GetHtmlTableCtrlFooter()
  {
    return "</table>";
  }
  
  /**
   * @brief Retourne le titre du bloc
   *
   * @return Retourne un string
   */
  function GetHtmlTableCtrlTitle()
  {
    return 
      ( $this->title != ""
        ? "<tr><td colspan='2' class='".
          ( $this->bMainTitle ? $this->cssBlockMainTitle : $this->cssBlockTitle )."'>".$this->title."</td></tr>"
        : "" ).
      ( $this->desc != ""
        ? "<tr><td colspan='2' class='formBlockDesc'>".$this->desc."</td></tr>"
        : "" );
  }

  /**
   * @brief Retourne le code HTML d'une ligne vide de hauteur
   *        param�trable d'un tableau formulaire de saisie
   *
   * @param iHeight Hauteur en pixels de la ligne vide (=5 par d�faut)
   * @return Retourne un string
   */
  function GetHtmlTableCtrlSpaceLine($iHeight=4)
  {
    return "<tr><td colspan='2' height='".$iHeight."'></td></tr>";
  }

  /**
   * @brief Retourne le code HTML d'une ligne de tableau � 2 colonnes
   *        contenant : label - controle de saisie
   *
   * @param oCtrl  r�f�rence vers l'objet ctrl � afficher
   * @return Retourne un string
   */
  function GetHtmlTableCtrlLine(&$oCtrl)
  {
    return "<tr>".
      "<td class='".$this->cssBlockLabel."'>".$oCtrl->label."</td>".
      "<td class='".$this->cssBlockCtrl."'>".$oCtrl->getHtml()."</td>".
      "</tr>";
  }

  /**
   * @brief Retourne le code HTML d'une ligne de tableau � 1 colonne 
   *        contenant : controle de saisie
   *
   * @param oCtrl  r�f�rence vers l'objet ctrl � afficher
   * @return Retourne un string
   */
  function GetHtmlTableCtrlColumn(&$oCtrl)
  {
    return "<tr>".
      "<td class='".$this->cssBlockCtrl."' colspan='2' width='100%'>".$oCtrl->getHtml()."</td>".
      "</tr>";
  }

  /**
   * @brief Retourne le code HTML d'une ligne de tableau � 2 lignes et 1 colonne 
   *        contenant : label / controle de saisie
   *
   * @param oCtrl  r�f�rence vers l'objet ctrl � afficher
   * @return Retourne un string
   */
  function GetHtmlTableCtrl2Lines(&$oCtrl)
  {
    return "<tr>".
      "<td class='".$this->cssBlockLabel."' colspan='2'>".$oCtrl->label."</td>".
      "</tr>".
      "<tr>".
      "<td class='".$this->cssBlockCtrl."' colspan='2'>".$oCtrl->getHtml()."</td>".
      "</tr>";
  }

}
?>