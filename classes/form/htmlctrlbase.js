// anciennement ShowHideDiv
function HelpShowHideLayer(strBaseName, iNum, iWidth)
{
  var strLayerName = strBaseName + iNum;
  var oLayer = MM_findObj(strLayerName, "");
  var oStyle = ( oLayer.style ? oLayer.style : oLayer);
  if( oStyle.visibility == "visible" || oStyle.visibility == "show" ) {
    oStyle.visibility = ( oLayer.style ? "hidden" : "hide");
    oStyle.width  = "0px";
    oStyle.position = "absolute";
  } else {
    oStyle.position = "relative";
    oStyle.visibility = ( oLayer.style ? "visible" : "show");
    oStyle.width  = iWidth+"px";
  }
}

/* anciennement ShowAllDiv et HideAllDiv
// non implémenté
function HelpShowHideAllLayer(strBaseName, bHide, iWidth)
{	
	var numDiv = 1;
  var strClassName = ( bHide == true ? "cachediv" : "" );
	if( document.getElementById ) {
		while( document.getElementById(strBaseName + numDiv) ) {
			SetDiv = document.getElementById(strBaseName + numDiv );
			if( SetDiv && SetDiv.className != strClassName )	{
				HelpShowHideLayer(strBaseName, numDiv, iWidth);
			}
			numDiv++;
		}
	}	else if( document.all ) {
		while( document.all[strBaseName + numDiv] ) {
			SetDiv = document.all[strBaseName + numDiv ];
			if( SetDiv && SetDiv.className != strClassName )	{
				HelpShowHideLayer(strBaseName, numDiv, iWidth);
			}
			numDiv++;
		}
	}	else if( document.layers ) {
    while ( document.layers[strBaseName + numDiv] )	{
			SetDiv = document.layers[strBaseName + numDiv];
			if ( SetDiv && SetDiv.className != strClassName )	{
				HelpShowHideLayer(strBaseName, numDiv, iWidth);
			}
			numDiv++;
		}
	}
}
*/
