<?php
include_once("htmlctrlbase.class.php");
include_once("htmllink.class.php");

/**
 * @brief classe d'affichage d'un composant avanc�
 *        g�n�re un bloc de fichiers joint disponible selon 2 modes :
 *         1=lecture
 *         2=modif
 *         Le mode 2 affiche une case � cocher par fichier pour le supprimer
 *         un bouton ajout qui appelle une fonction javascript AjouterPJ()
 */
class HtmlUpload extends HtmlCtrlBase
{
  /** 
   * dataSet contenant les pi�ces jointes
   * N�cessite d'avoir les alias : ID, FILENAME, FILENAME_AFF, DATE_CREA, AUTEUR_MAIL
   */
  var $dsPJ;

  /** = vrai si affichage en popup, faux sinon (par d�faut) */
  var $bPopup;

  /** largeur du controle en pixels, = 380 par d�faut */
  var $width;

  /** chemin physique de base vers les pi�ces jointes */
  var $strPathBase;

  /** chemin uri de base vers les icones des pieces jointes */
  var $strUrlIcone;

  /** nb de langues prises en compte */
  var $nbLg;

  /** bouton de validation pour valider l'�tape */
  var $buttonAdd;
  var $buttonAddRol;

  /** bouton de suppression */
  var $buttonDel;
  var $buttonDelRol;

  /** 
   * url de t�l�chargement, 
   * =vide, t�l�chargement en direct, 
   * sinon t�l�chargement control�, url de type : http://www.domaine.ext/..../fichier.ext?param1=val1&...&paramPJ_ID=
   * pour permettre la concatenation de l'identifiant du fichier joint
   */
  var $strUrlDownload;
  
  /** Nom du formulaire (optionnel) */
  var $formName;
  
  /** Etat coch� si =1 ou d�coch� si =0 des cases � cocher permettant la suppression */
  var $defaultStateCheck;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode       Mode du controle de saisie : =1 lecture, =0 ou =2 case � cocher + bouton ajouter
   * @param name        Nom du controle de saisie
   * @param label       Etiquette texte associ�e au controle de saisie
   * @param strPathBase Chemin de base vers les fichiers joints
   * @param strUrlBase  Url de base vers les fichiers joints
   * @param strUrlIcone Url de base vers les icones des fichiers joints
   * @param nbLg        nombre de langue prise en compte par l'extranet (=1 par defaut)
   */
  function HtmlUpload($iMode, $name, $label, $strPathBase, $strUrlBase, $strUrlIcone, $nbLg=1)
  {
    parent::HtmlCtrlBase($iMode, $name, "", $label);
    $this->width = 380;
    $this->dsPJ = null;
    $this->bPopup = false;
    $this->strPathBase = $strPathBase;
    $this->strUrlBase = $strUrlBase;
    $this->strUrlIcone = $strUrlIcone;
    $this->strUrlDownload = "";
    $this->formName = "";
    $this->defaultStateCheck = "1";
    $this->nbLg = $nbLg;
    $this->buttonAdd = "tab_ajouter.gif";  
    $this->buttonAddRol = "tab_ajouter_rol.gif";
    $this->buttonDel = "tab_supprimer.gif";  
    $this->buttonDelRol = "tab_supprimer_rol.gif";
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   *        Construit le tableau html de la liste des fichiers joints d'un document en mode modification
   */
  function GetHtml()
  {
    $tabHelp = $this->GetTabHtmlHelp();
    if( $this->iMode == "0" ) 
      $this->iMode = "2";

    $strAlign = "left";
    if( $this->bPopup == true ) $strAlign = "right";

    $strHtml = "";
    if( $this->strUrlDownload != "" ) {
      $strHtml = "<script language='javascript'>".
        " function DownloadPJ(pj_id) {".
        " var iHeight = 100; var iWidth = 400;".
        " var iTop = (screen.height - iHeight) / 2;".
        " var iLeft = (screen.width - iWidth) / 2;".
        " window.open('".$this->strUrlDownload."'+pj_id, 'footerExec',".
        " 'status=yes,menubar=no,scrollbars=yes,resizable=yes,height='+iHeight+',".
        " width='+iWidth+',top='+iTop+',left='+iLeft); } </script>";
    }

    if( $this->iMode == "3" ) {
      return $this->GetDynamicHtml();
    }
    
    $strHtml .= "<table border='0' cellpadding='0' cellspacing='0'>";
    $iWidthLg = 0;
    $iWidthMail = 0;
    $iColSpan = 3;
    $cpt = 0;
    $lg_id = "1";
    if( $this->dsPJ!=null )
      while( $drPJ = $this->dsPJ->GetRowIter() ) {
        $id = $drPJ->getValueName("ID");
        $strDate = $drPJ->getValueName("DATE_CREA");
        $strFileName = $drPJ->getValueName("FILENAME");
        $strFileNameAff = $drPJ->getValueName("FILENAME_AFF");
        $strMail = $drPJ->getValueName("AUTEUR_MAIL");
        if( $this->nbLg >  1 ) {
          $lg_id = $drPJ->getValueName("LG_ID");
          if( !($lg_id!="" && is_numeric($lg_id) && $lg_id>0 && $lg_id<=$this->nbLg) ) 
            $lg_id = "1";
        }

        $strUrl = $this->strUrlBase.$strFileName;
        $strPathFile = $this->strPathBase.$strFileName;
        if( file_exists($strPathFile) && is_file($strPathFile) ) {
          $iFileSize = filesize($strPathFile);
          $strTaille = ( $iFileSize > 1024 ? (floor($iFileSize/1024))." ko" : $iFileSize." o");
          
          $strHtml .= "<tr>";
          
          if( $this->iMode=="2" ) {
            // case a cocher
            $strHtml .= "<td width='25' align='center'>".
              "<input type='checkbox' name='suppj_".$cpt."' checked>".
              "<input type='hidden' name='id_".$cpt."' value='".$id."'>".
              "<input type='hidden' name='fileName_".$cpt."' value='".$strFileName."'>".
              "</td>";
          } else {
            // email du publicateur
            if( $strMail != "" ) {
              $iWidthMail = 25; 
              $strHtml .= "<td width='25' align='center'>".
                "<a title=\"".$strMail."\" href=\"mailto:".$strMail."\">".
                "<img src='".$this->strUrlIcone."icon_mail.gif'  border='0' alt=''>".
                "</a></td>";  
            }
          }

          if( $this->nbLg > 1 ) {
            $strHtml .= "<td width='20' align='center'>".
              "<img src='".$this->strUrlIcone."drapeau_".$lg_id.".gif' width='17' height='12' border='0' alt=''>".
              "</td>";  
            $iWidthLg = 20;
            $iColSpan = 4;
          }
       
          // nom du fichier + date + taille en ko
          $strHtml .=  "<td width='20' valign='middle' align='center' height='20'>".
            ( $this->strUrlDownload != "" && !$this->isFileMultimedia($strFileNameAff)
              ? "<a class='".$this->cssLink."' href=\"javascript:DownloadPJ(".$id.")\">"
              : "<a class='".$this->cssLink."' target='_blank' href=\"".$strUrl."\">").
            "<img src='".$this->strUrlIcone.$this->_GetPictoFile($strFileName)."'".
            " border='0' title='T�l�charger'></a></td>". 
            "<td width='".($this->width-20-$iWidthMail-$iWidthLg)."' align='left'>&nbsp;". 
            ( $this->strUrlDownload != "" && !$this->isFileMultimedia($strFileNameAff)
              ? "<a class='".$this->cssLink."' href=\"javascript:DownloadPJ(".$id.")\">"
              : "<a class='".$this->cssLink."' target='_blank' href=\"".$strUrl."\">").
            $strFileNameAff."</a>".
            "<span class='".$this->cssText."'>". 
            ", ".$strTaille. 
            ", ".$strDate."</span></td>".
            "</tr>";

          $cpt++;
        }
      }

    // m�morise le nombre de pieces jointes
    $strHtml .= "<input type='hidden' name='nbPJ' value=\"".$cpt."\">";
    
    if( $this->iMode == "2" ) {
      // ajoute le bouton ajouter
      $oCtrlBt = new HtmlLink("javascript:AjouterPJ()", "Ajouter un fichier joint", 
                              $this->buttonAdd ,  $this->buttonAddRol);

      $strHtml .= "<tr><td width='".$this->width."' height='5' colspan='".$iColSpan."'></tr>".
        "<tr><td align='".$strAlign."' colspan='".$iColSpan."'>".$oCtrlBt->getHtml()."</td></tr>";
    }
    
    $strHtml .= "<tr><td width='".$this->width."' height='2' colspan='".$iColSpan."'></td></tr></table>";
    
    return $strHtml;
  }

  /**
   * @brief Retourne le nom du fichier icone associ� au fichier pass� en param�tre
   *
   * @param strFileName Nom du fichier
   */
  function _GetPictoFile($strFileName)
  { 
    $strPicto = "icon_doc_0.gif";
    $iLg = strlen($strFileName);
    if( $iLg > 2 ) {
      $iPos = strrpos($strFileName, ".");
      if( !($iPos===false) ) {
        $strExt = strtolower(substr($strFileName, $iPos+1));

        switch( $strExt ) {
          case "xls": case "csv":  $strPicto = "icon_doc_1.gif"; break;
          case "doc": case "rtf":  $strPicto = "icon_doc_2.gif"; break;
          case "pdf":              $strPicto = "icon_doc_3.gif"; break;
          case "png": case "tif":
          case "jpg": case "gif":
          case "bmp": case "jpeg": $strPicto = "icon_doc_4.gif"; break;
          case "txt":              $strPicto = "icon_doc_5.gif"; break;
          case "htm": case "html": $strPicto = "icon_doc_6.gif"; break;
          case "zip":              $strPicto = "icon_doc_7.gif"; break;
          case "svg": case "jsvg": 
          case "js":  case "frm":   
          case "style":            $strPicto = "icon_doc_8.gif"; break;
          case "ppt":              $strPicto = "icon_doc_10.gif"; break;
          case "mdb":              $strPicto = "icon_doc_11.gif"; break;
          case "mp3": case "mp2":
          case "mp4": case "mpg": 
          case "mpeg":case "avi":  
          case "wma": case "wmv":
          case "midi":case "mid":
          case "rmi": case "wav":
          case "m3u": case "asf":  $strPicto = "icon_doc_12.gif"; break;
          case "ram": case "rm":   $strPicto = "icon_doc_13.gif"; break;
          case "mov":              $strPicto = "icon_doc_14.gif"; break;
        }
      }
    }

    return $strPicto;
  }
  
  /**
   * @brief v�rifie si le fichier est du type multim�dia. Retourne vrai ou faux
   * 
   * @param strFileName  nom du fichier avec extension
   * 
   * @return Retourne un booleen
   */
  function isFileMultimedia($strFileName)
  { 
    $bOk=false;
    $iFWidth = strlen($strFileName);
    if( $iFWidth > 2 ) {
      $iPos = strrpos($strFileName, ".");
      if( !($iPos===false) ) {
        $strExt = strtolower(substr($strFileName, $iPos+1));
        switch( $strExt ) {
          case "mp3": case "mp2":
          case "mp4": case "mpg": 
          case "mpeg":case "avi":  
          case "wma": case "wmv":
          case "midi":case "mid":
          case "rmi": case "wav":
          case "m3u": case "asf":  
          case "ram": case "rm":   
          case "mov":              $bOk=true; break;
        }
      }
    }
    return $bOk;
  }
  
  
  /**
   * @brief Retourne le code html de ce controle sous la nouvelle g�n�ration :
   *        - Liste des fichiers joints avec en face lien de suppression
   *        - Enregistrement et actualisation transparents � l'utilisateur
   * @return string html  
   */
  function GetListDynamicHtml()
  {
    $strHtml = "";
    
    $strHtml .= "<table id='tab_upload_".$this->name."' cellspacing='2' cellpadding='0' border='0'>";    
    
    $iColSpan = 3;
    $cpt = 0;
    $lg_id = "1";
    if( $this->dsPJ!=null )
      while( $drPJ = $this->dsPJ->GetRowIter() ) {
        $iWidthMail = 0; 
        $iWidthLg = 0; 
        $id = $drPJ->getValueName("ID");
        $strDate = $drPJ->getValueName("DATE_CREA");
        $strFileName = $drPJ->getValueName("FILENAME");
        $strFileNameAff = $drPJ->getValueName("FILENAME_AFF");
        $strMail = $drPJ->getValueName("AUTEUR_MAIL");
        if( $this->nbLg >  1 ) {
          $lg_id = $drPJ->getValueName("LG_ID");
          if( !($lg_id!="" && is_numeric($lg_id) && $lg_id>0 && $lg_id<=$this->nbLg) ) 
            $lg_id = "1";
        }

        $strUrl = $this->strUrlBase.$strFileName;
        $strPathFile = $this->strPathBase.$strFileName;
        if( file_exists($strPathFile) && is_file($strPathFile) ) {
          $iFileSize = filesize($strPathFile);
          $strTaille = ( $iFileSize > 1024 ? (floor($iFileSize/1024))." ko" : $iFileSize." o");
          
          $strHtml .= "<tr>";
          
          if( $this->iMode=="3" ) {
            // case a cocher
            $strHtml .= "<td width='25' align='center'>".
              "<input type='checkbox' name='suppj_".$cpt."' ".($this->defaultStateCheck=="1" ? "checked" : "value='1'").">".
              "<input type='hidden' name='id_".$cpt."' value='".$id."'>".
              "<input type='hidden' name='fileName_".$cpt."' value='".$strFileName."'>".
              "</td>";
          } else {
            // email du publicateur
            if( $strMail != "" ) {
              $iWidthMail = 25; 
              $strHtml .= "<td width='25' align='center'>".
                "<a title=\"".$strMail."\" href=\"mailto:".$strMail."\">".
                "<img src='".$this->strUrlIcone."icon_mail.gif'  border='0' alt=''>".
                "</a></td>";  
            }
          }

          if( $this->nbLg > 1 ) {
            $strHtml .= "<td width='20' align='center'>".
              "<img src='".$this->strUrlIcone."drapeau_".$lg_id.".gif' width='17' height='12' border='0' alt=''>".
              "</td>";  
            $iWidthLg = 20;
            $iColSpan = 4;
          }
       
          // nom du fichier + date + taille en ko
          $strHtml .=  "<td width='20' valign='middle' align='center' height='20'>".
            ( $this->strUrlDownload != "" && !$this->isFileMultimedia($strFileNameAff)
              ? "<a class='".$this->cssLink."' href=\"javascript:DownloadPJ(".$id.")\">"
              : "<a class='".$this->cssLink."' target='_blank' href=\"".$strUrl."\">").
            "<img src='".$this->strUrlIcone.$this->_GetPictoFile($strFileName)."'".
            " border='0' title='T�l�charger'></a></td>". 
            "<td width='".($this->width-20-$iWidthMail-$iWidthLg)."' align='left'>&nbsp;". 
            ( $this->strUrlDownload != "" && !$this->isFileMultimedia($strFileNameAff)
              ? "<a class='".$this->cssLink."' href=\"javascript:DownloadPJ(".$id.")\">"
              : "<a class='".$this->cssLink."' target='_blank' href=\"".$strUrl."\">").
            $strFileNameAff."</a>".
            "<span class='".$this->cssText."'>". 
            ", ".$strTaille. 
            ", ".$strDate."</span></td>".
            "</tr>";

          $cpt++;
        }
      }
    
    // m�morise le nombre de pieces jointes
    $strHtml .= "<input type='hidden' name='nbPJ' value=\"".$cpt."\">";
    $strHtml .= "<tr><td width='".$this->width."' ".($cpt==0 ? "height='0'" : "")." colspan='".$iColSpan."'>" .
        ($cpt>0 ? /*$oDel->GetHtml()*/"" : "")."</td></tr></table>";
    
    return $strHtml;
  }
  
  /**
   * @brief Retourne le code html de ce controle sous la nouvelle g�n�ration :
   *        - Liste des fichiers joints avec en face lien de suppression
   *        - Un champ de s�lection de fichier avec en face lien d'ajout
   *        - Enregistrement et actualisation transparents � l'utilisateur
   * @return string html  
   */
  function GetDynamicHtml()
  {
    $strFctAdd = "AjouterPJ('".$this->name."'".($this->formName=="" ? "" : ", '".$this->formName."'").");";
    $strFctDel = "SupprimerPJ('".$this->name."'".($this->formName=="" ? "" : ", '".$this->formName."'").");";
    $oDel = new HtmlLink("javascript:".$strFctDel, "Supprimer les fichiers joints s�lectionn�s", $this->buttonDel, $this->buttonDelRol);
    $oAdd = new HtmlLink("javascript:".$strFctAdd, "Ajouter un fichier joint", $this->buttonAdd, $this->buttonAddRol);

    $oFile = new HtmlFile(0, $this->name, "", $this->label, substr($this->width, 0, -1), 255);
    
    $tabHelp = $this->GetTabHtmlHelp();
    $strHelp = ( $tabHelp["btHelp"] != "" ? "<td class='".$this->cssTdCtrl."'>".$tabHelp["btHelp"]."</td>" : "" );
    $nbCol = ( $tabHelp["btHelp"] != "" ? 3 : 2);
    
    $strHtml = "";
    $strHtml .= "<table border='0' cellspacing='0' cellpadding='0' width='100%'>" .
            "<tr>" .
            "<td>".$oAdd->GetHtml()."</td>" .
            "<td>".$oFile->GetHtml()."</td>" .
            $strHelp.
            "</tr>" .
            "<tr>" .
            "<td>".$oDel->GetHtml()."</td>" .
            "<td colspan='".($nbCol-1)."'></td>" .
            "</tr>" .
            "<tr>" .
            "<td colspan='".$nbCol."'>" .
          ( $tabHelp["layerHelp"] != "" ? $tabHelp["layerHelp"] : "" ) .
            "</td>" .
            "</tr>" .
            "<tr>" .
            "<td colspan='".$nbCol."'><div id='list_upload_".$this->name."'>".$this->GetListDynamicHtml()."</div></td>" .
            "</tr>" .
            "</table>";
    $strHtml .= "<iframe name='ifr_upload_".$this->name."' style='display:none'></iframe>";
    
    return $strHtml;
  }
}

?>