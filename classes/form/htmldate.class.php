<?php

/**
 * @class HtmlDate
 * @brief
 */
class HtmlDate extends HtmlText
{
  
  var $defaultValue;
  var $oCtrlDay;
  var $oCtrlMonth;
  var $oCtrlYear;
  var $date_min;
  var $date_max;
  var $urlBaseImg;
  var $bWithCalendar;
  var $bWithCtrl;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode     Mode du controle de saisie : =0 modif, =1 lecture
   * @param name      Nom du controle de saisie
   * @param value     Valeur par d�faut du controle de saisie
   * @param label     Etiquette texte associ�e au controle de saisie
   * @param separator caract�re s�parateur entre jour mois et ann�e ( / par d�faut)
   */
  function HtmlDate($iMode, $name, $value="jj/mm/aaaa", $label="", $date_min="", $date_max="")
  {
    parent::HtmlText($iMode, $name, $value, $label, 1, 10, 10);
    
    $this->defaultValue = $value;
    $this->date_min = $date_min;
    if ($this->date_min==""){
      $this->date_min = date("d/m/Y");
    }
    $this->date_max = $date_max;
    $this->urlBaseImg = "";
    if( defined("ALK_URL_SI_IMAGES") == true ) {
      $this->urlBaseImg = ALK_URL_SI_IMAGES;
    }
    $this->bReadOnly = true;
    $this->bWithCalendar = true;
    $this->bWithCtrl = true;
  }
  
  /**
   * @brief Retourne le code html du controle de saisie date
   *
   * @return Retourne un string
   */
  function GetHtml()
  {    
    if ( !$this->bReadOnly ){
      $this->AddEvent("onFocus", "FocusTexte(this)");
      $this->AddEvent("onBlur", "BlurTexte(this)");
    }
    $oLink = new HtmlLink("javascript:OpenCalendar_".$this->name."();",
                          "Ouvrir / fermer le calendrier",
                          "calendar.gif", "", $this->urlBaseImg);
    
    $strHtml = $this->GetCtrlHtml();
    $strHtml = str_replace(" name=", " id=\"input_".$this->name."\" name=", $strHtml);
    if( $this->bWithCalendar && $this->iMode == 0 ) {
      $strHtml .= $this->GetCalendarCss();
      $strHtml .= "<script language='javascript'>" .
                  " var bCalendar_".$this->name." = false;" .
                  "function DefineCalendar_".$this->name."(){" .
                  " if (bCalendar_".$this->name.") return; delete(oCalendar_".$this->name.");" .
                  " if ( typeof oCalendar_".$this->name." == 'undefined' ) \n" .
                  "   oCalendar_".$this->name." = new Calendar('".$this->name."', '".$this->date_min."', '".$this->date_max."', true);" .
                  " bCalendar_".$this->name." = true;" .
                  "}" .
                  "function OpenCalendar_".$this->name."(){" .
                  " if (!bCalendar_".$this->name." || typeof oCalendar_".$this->name." == 'undefined' ) \n" .
                  "   DefineCalendar_".$this->name."();" .
                  " if (oCalendar_".$this->name.".oDiv){" .
                  "   oCalendar_".$this->name.".OpenCloseCalendar();" .
                  " }" .
                  "}" .
                  "</script>";
      $strHtml .= "&nbsp;".$oLink->GetHtml();
      $strHtml .= "<div style='position:relative;z-index: 10000;'><div id='".$this->name."_calendarDiv' style='display:none;position:absolute'></div></div>";
    }
    return $strHtml;    
  }
  
  
  /**
   * @brief Retourne le code html et la fonction permettant de cr�er un calendrier ouvert
   *
   * @return Retourne un array = {"strCalHtml"=>strCalHtml, "strCtrlHtml"=>strCtrlHtml, "strJsFct"=>strJsFct}
   */
  function GetTabHtmlCalendar($align="")
  {
    $strCalHtml = "";
    $strCtrlHtml = "";
    $strJsFct = "";
    
    $strJsFct = "DrawCalendar_".$this->name."();";
    $strCalHtml = $this->GetCalendarCss() .
        "<script language='javascript'>" .
        "function DrawCalendar_".$this->name."(){" .
        " delete(oCalendar_".$this->name.");" .
        " oCalendar_".$this->name." = new Calendar('".$this->name."', '".$this->date_min."', '".$this->date_max."', false);" .
        " oCalendar_".$this->name.".OpenCloseCalendar();" .
        "}" .
        "</script>" .
        "<div id='".$this->name."_calendarDiv' ".($align!="" ? "align='".$align."'" : "")."></div>";
    if ($this->bWithCtrl)
      $strCtrlHtml = str_replace(" name=", " id=\"input_".$this->name."\" name=", parent::GetHtml());
    return array("strCalHtml"=>$strCalHtml, "strCtrlHtml"=>$strCtrlHtml, "strJsFct"=>$strJsFct);
  }

  /**
   * @brief Retourne le code Html d'�criture des CSS et d'inclusion du fichier calendrier.js du calendrier 
   * @return string html
   */
  function GetCalendarCss(){
    global $__bCssCalendarWrite__;
    
    $strHtml = "";
    if (!(isset($__bCssCalendarWrite__) && $__bCssCalendarWrite__==true)){
      $calStyle = "<STYLE TYPE=\"text/css\">";
      $calStyle .= "body.popcal {";
      $calStyle .= " background-color: #f0f0f0;";
      $calStyle .= "}";
      $calStyle .= "table.poptitle {";
      $calStyle .= "background-color: #ffffff;";
      $calStyle .= "}";
      $calStyle .= "table.poptitle td {";
      $calStyle .= " background-color: #ffffff;";
      $calStyle .= " color: #742A2A;";
      $calStyle .= " font-family: Osaka,helvetica,arial,sans-serif;";
      $calStyle .= " font-size: 12px;";
      $calStyle .= " font-weight: bold;";
      $calStyle .= "}";
      $calStyle .= "table.popcal {";
      $calStyle .= " background-color: #cccccc;";
      $calStyle .= " border:1px solid #999999;";
      $calStyle .= "}";
      $calStyle .= "table.popcal table.popcalext {";
      $calStyle .= " background-color: #cccccc;";
      $calStyle .= "}";
      $calStyle .= "table.popcal td {";
      $calStyle .= " background-color: #ffffff;";
      $calStyle .= " font-family: Osaka,helvetica,arial,sans-serif;";
      $calStyle .= " font-size: 11px;";
      $calStyle .= " text-align: center;";
      $calStyle .= "}";
      $calStyle .= "table.popcal td.daytitle {";
      $calStyle .= " background-color: #ffffff;";
      $calStyle .= " color: #742A2A;";
      $calStyle .= " font-family: Osaka,helvetica,arial,sans-serif;";
      $calStyle .= " font-size: 11px;";
      $calStyle .= "}";
      
      $calStyle .= "table.popcal td.empty {";
      $calStyle .= " font-family: Osaka,helvetica,arial,sans-serif;";
      $calStyle .= " background-color: #e0e0e0;";
      $calStyle .= " color: #666666;";
      $calStyle .= "}";
      
      $calStyle .= "table.popcal td.day {";
      $calStyle .= " font-family: Osaka,helvetica,arial,sans-serif;";
      $calStyle .= " background-color: FBE6CB;";
      $calStyle .= " text-align: center;";
      $calStyle .= " color: #666666;";
      $calStyle .= "}";
      $calStyle .= "table.popcal td.weekend {";
      $calStyle .= " font-family: Osaka,helvetica,arial,sans-serif;";
      $calStyle .= " background-color: #f0f0f0;";
      $calStyle .= " color: #666666;";
      $calStyle .= "}";
      $calStyle .= "table.popcal td.today {";
      $calStyle .= " font-family: Osaka,helvetica,arial,sans-serif;";
      $calStyle .= " color: #44aa44;";
      $calStyle .= "}";
      
      $calStyle .= "table.popcal td.invalid {";
      $calStyle .= " font-family: Osaka,helvetica,arial,sans-serif;";
      $calStyle .= " background-color: #e0e0e0;";
      $calStyle .= " color: #aaaaaa;";
      $calStyle .= "}";
      
      $calStyle .= "table.popcal td.over  {";
      $calStyle .= " background-color: #E4E1DE;";
      $calStyle .= " color: #FF0000;";
      $calStyle .= "}";
      
      $calStyle .= "table.popcal td.select  {";
      $calStyle .= " background-color: #FCD6BB;";
      $calStyle .= " color: #FF0000;";
      $calStyle .= "}";
      
      $calStyle .= "table.popcal a:link,";
      $calStyle .= "table.popcal a:visited {";
      $calStyle .= " color: #666666;";
      $calStyle .= " text-decoration: none;";
      $calStyle .= "}";
      $calStyle .= "table.popcal a.today:link,";
      $calStyle .= "table.popcal a.today:visited {";
      $calStyle .= " color: #666666;";
      $calStyle .= "}";
      $calStyle .= "table.popcal a.select:link,";
      $calStyle .= "table.popcal a.select:visited {";
      $calStyle .= " color: #ff0000;";
      $calStyle .= " font-weight: bold;";
      $calStyle .= "}";
      $calStyle .= "table.popcal a.weekend:link,";
      $calStyle .= "table.popcal a.weekend:visited {";
      $calStyle .= "color: #0000aa;";
      $calStyle .= "}";
      $calStyle .= "table.popcal a.empty:link,";
      $calStyle .= "table.popcal a.empty:visited {";
      $calStyle .= " color: #777777;";
      $calStyle .= "}";
      $calStyle .= "table.popcal a.lienmois {";
      $calStyle .= " font-family:Osaka,helvetica,arial,sans-serif;";
      $calStyle .= " font-size: 11px;";
      $calStyle .= " font-weight: bold;";
      $calStyle .= " color: #742A2A;";
      $calStyle .= "}";
      $calStyle .= "table.popcal td.tdLienMoisL {";
      $calStyle .= " text-align: left;";
      $calStyle .= "}";
      $calStyle .= "table.popcal td.tdLienMoisR {";
      $calStyle .= " text-align: right;";
      $calStyle .= "}";
      
      $calStyle .= "</STYLE>";
      
      $calJavascript = "<script language='javascript' src='../../lib/calendrier.js'></script>";
      
      $strHtml .= $calStyle.$calJavascript;
      $__bCssCalendarWrite__ = true;
    }
    return $strHtml;
  }
}
?>