<?php

/**
 * @brief classe de base aux composants d'affichage
 */
class HtmlBase extends AlkObject
{
  /** Identifiant ou non de l'objet */
  var $name;

  /** liste d'�v�nements associ�s */
  var $tabEvents;

  /** vrai si multilingue pris en compte */
  var $bMultiLangue;

  /** url de base pour les images drapeaux*/
  var $urlBaseDrapeau;

  /** vrai = champ langue �trang�re affich�e, faux = champs cach�s */
  var $bShowMultiLangue;

  /** r�f�rence vers l'objet Smarty utilis� pour les templates */
  var $oTemplate;

  /** fichier template utilis� */
  var $strTemplateFile;

  /** type de navigateur : IE, Opera, netscape4, netscape6 */
  var $strNavigateur;

  /** Nom des classes CSS pour respectivement : texte courant, lien courant */
  var $cssText;
  var $cssLink;

  /** contenu de l'attribut style d'un tag html */
  var $cssStyle;

  /**
   * @brief Constructeur par d�faut
   */
  function HtmlBase($name)
  {
    parent::AlkObject();

    $this->name = $name;

    $this->cssText  = "divContenuTexte";
    $this->cssLink  = "aContenuLien";
    $this->cssStyle = "";

    $this->bMultiLangue = false;
    $this->urlBaseDrapeau = ( defined("ALK_URL_SI_IMAGES") ? ALK_URL_SI_IMAGES : "/media/images/" );
    $this->bShowMultiLangue = false;

    $this->tabEvents = array();

    $this->oTemplate = null;
    $this->strTemplateFile = "";

    // d�tection du navigateur client
    $strNav = "";
    $strUserAgent = (isset($_SERVER["HTTP_USER_AGENT"])?$_SERVER["HTTP_USER_AGENT"]:"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
    if ( (preg_match('/msie/i', $strUserAgent)) && (!preg_match('/opera/i', $strUserAgent)) ) {
      $strNav = "IE";
    } elseif( (preg_match('/opera/i', $strUserAgent)) ) {
      $strNav = "Opera";
    } elseif( (preg_match('/Mozilla\/4./i', $strUserAgent)) ) {
      $strNav = "Netscape4";
    } elseif( (preg_match('/Mozilla\/5.0/i', $strUserAgent)) && (!preg_match('/Konqueror/i', $strUserAgent)) ) {
      $strNav = "Netscape6";
    }
    $this->strNavigateur = $strNav;
  }

  /** 
   * @brief Affecte un template � l'objet
   * 
   * @param strAppliPath     nom du r�pertoire de l'appli utilisant le template pour former le chemin ../[strPathAppli]/templates
   * @param strTemplateFile  nom du fichier xxx.tpl
   */
  function SetTemplate($strPathAppli, $strTemplateFile)
  {
    global $oSpace;

    $this->strTemplateFile = "";
    if( $this->oTemplate == null && isset($oSpace) && is_object($oSpace) && isset($oSpace->oTemplate) )
      $this->oTemplate =& $oSpace->oTemplate;
    if( $this->oTemplate != null ) 
      $this->strTemplateFile = "../".$strPathAppli."/templates/".$strTemplateFile;
  }

  /**
   * @brief Ajoute un �v�nement dans la liste
   * 
   * @param strEvent      Nom de l'�v�nement
   * @param strFunctionJS Nom de la fonction javascript appel�e (pas n�cessaire d'ajouter 'javascript:')
   */
  function AddEvent($strEvent, $strFunctionJS)
  {
    $_strEvents = strtolower($strEvent);
    if ( array_key_exists($_strEvents, $this->tabEvents) ){
      $this->tabEvents[$_strEvents] .= $strFunctionJS;
    } else {
      $this->tabEvents = array_merge($this->tabEvents, array($_strEvents => $strFunctionJS));
    }
  }

  /**
   * @brief Genere puis retourne tous les evenements li�s au controle html
   *
   * @return Retourne les �v�nements concat�n�s dans une chaine pour �tre inclus dans
   *         dans un tag html de type controle de saisie
   */
  function GetHtmlEvent()
  {
    $strHtml = "";
    foreach($this->tabEvents as $strEvent => $strFunctionJs)
      $strHtml .= " ".$strEvent."=\"javascript:".$strFunctionJs."\"";
    return $strHtml;
  }

}

?>