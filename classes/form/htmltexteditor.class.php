<?
include_once("htmlctrlbase.class.php");
include_once("htmltext.class.php");

/**
 * @brief classe d'affichage d'un composant de saisie texte
 *        g�n�re : <input type=text>
 *                 <input type=password>
 *                 <textarea></textarea>
 *        classe multilingue : passer un tableau sur param value
 *                             renseigner les attributs tabLangue et urlBaseDrapeau
 */
class HtmlTextEditor extends HtmlText
{
  /** Nom de la table associ�e */
  var $tableName;
  
  /** Nom du champ associ� */
  var $fieldName;

  /** Nom du champ id */
  var $idFieldName;

  /** Valeur id */
  var $idValue;
  
  /** chemin vers rep upload pour l'�diteur */
  var $pathUpload;
  
  /** strParam */
  var $strParam;
    
   /** url de base pour l'image Modifier */
  var $urlBaseImg;
  
  /** image Modifier */
  var $urlImg;
  
  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode     Mode du controle de saisie : =0 modif, =1 lecture
   * @param name      Nom du controle de saisie
   * @param value     Valeur par d�faut du controle de saisie
   * @param label     Etiquette texte associ�e au controle de saisie
   * @param row       Nombre de ligne (=1 : <input type=text>, >1 : <textarea>). =1 par d�faut
   * @param column    Nombre de colonnes. =0 par d�faut
   * @param maxlength Longueur max du texte saisi. =0 par d�faut
   */
  function HtmlTextEditor($iMode, $name, $value="", $label="", $row=1, $column=0, $tableName="", $fieldName, $idFieldName="", $idValue="", $pathUpload="", $strParam="")
  {
    parent::HtmlText($iMode, $name, $value, $label, $row, $column, 0);
   
    $this->tableName = $tableName;
    $this->fieldName = $fieldName;
    $this->idFieldName = $idFieldName;
    $this->idValue = $idValue;    
    $this->pathUpload = $pathUpload;
    $this->strParam = $strParam;
   
    $this->urlBaseImg = "";
    if( defined("ALK_URL_SI_IMAGES") == true ) {
      $this->urlBaseImg = ALK_URL_SI_IMAGES;
    }
    
    $this->urlImg = "mod.gif";
    $this->urlImgRol = "mod_rol.gif";
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetHtml()
  {  
  	$strHtml = parent::GetHtml();	
  	
  	if ($this->iMode == "0") {
  		$strHtml = "<script language='javascript'>".
									"function OpenPopupEditeur".$this->name."(strParam, idEnt, table, champsHtml, champsId, repUpload)" .
									"{" .
									"var ht = 580;" .
									"var lg = 788;" .
									"var t = (screen.height - ht) / 2;" .
									"var l = (screen.width - lg) / 2;" .
									"var strUrl = \"../editeur/01_editeur.php?\"+strParam+\"&idEnt=\"+" .
									"idEnt+\"&table=\"+table+\"&champsHtml=\"+champsHtml+\"&champsId=\"+champsId+\"&repUpload=\"+repUpload+\"&reload=1\";" .
									"window.open(strUrl, \"_windEditeur\", \"status=yes,scrollbars=no,resizable=yes,height=\"+ht+\",width=\"+lg+\",top=\"+t+\",left=\"+l);" .
									"}" .
									"</script>".$strHtml;
			
  	}
    return $strHtml;
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au mode modification
   */
  function _GetHtmlUpdateMode($iLangue=ALK_LG_FR)
  {
    $strHtml = "";
    
    $bReadOnly = $this->bReadOnly;
    
    $this->bReadOnly = true;
    
    $strHtml = parent::_GetHtmlUpdateMode($iLangue);
    
    $this->bReadOnly = $bReadOnly;
    
    $strField = $this->fieldName;
    if ($this->bShowMultiLangue)
    	$strField = $this->fieldName.$this->tabLangue[$iLangue]["bdd"];
    	
    if( $this->bReadOnly == false ) {
    	$strHtml .= "<a ".            
            " href=\"javascript:OpenPopupEditeur".$this->name."('".$this->strParam."','".$this->idValue."', '".
            	$this->tableName."','".$strField."', '".$this->idFieldName."', '".$this->pathUpload."');\">".
            "<img border=0".
            " src=\"".$this->urlBaseImg.$this->urlImg."\">".
            "</a>&nbsp;";
    }

    return $strHtml;
  }

}
?>