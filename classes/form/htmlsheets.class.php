<?php
include_once("htmlbase.class.php");

/**
 * @brief Classe affichant un panel sous forme d'onglets 
 *        G�re le contenu sous forme de layer visible ou invisible
 */
class HtmlSheets extends HtmlBase
{
  /** ensemble des onglets */
  var $tabSheets;

  /** identifiant de l'onglet s�lectionn� */
  var $idSheet;

  /** largeur du panel */
  var $iWidth;

  /** hauteur du panel */
  var $iHeight;

  /** url de base pour les images d'onglets =  ALK_URL_SI_IMAGES par d�faut */
  var $urlBaseImg;

  /** classe css appliqu�e au tableau contenant les onglets */
  var $cssFormSheets;

  /** classe css appliqu�e � chaque layer sur lequel on pose le contenu d'un onglet (visibility=hidden obligatoirement) */
  var $cssFormLayerSheet;

  /** hauteur d'un onglet en px */
  var $iSheetHeight;
  /** largeur d'une image de bordure d'un onglet */
  var $iSheetWidthBorder;
  /** largeur d'une image de transition entre onglet */
  var $iSheetWidthTransition;
  /** tableau des noms d'images utilis�s */
  var $tabImgSheet;
  
  var $tabHtmlContent;

  /**
   * @brief Constructeur par d�faut
   *
	 * @param name      Nom du groupe d'onglets
   * @param tabSheets    tableau de d�finition du groupe d'onglets 
   * @param $iTypeSheet  type du groupe d'onglets courant : ALK_CONSULT_TYPESHEET, ALK_ADMIN_TYPESHEET, ALK_PROPRIETE_TYPESHEET 
   * @param idSheet   Identifiant de l'onglet s�lectionn�
   * @param iWidth    Largeur du panel en pixel (-1 : largeur non prise en compte, valeur par d�faut)
   * @param iHeight   Hauteur du panel en pixel (-1 : hauteur non prise en compte, valeur par d�faut)
   */
  function HtmlSheets($name, $tabSheets, $iTypeSheet="0", $idSheet="0", $iWidth="-1", $iHeight="-1")
  {
    parent::HtmlBase($name);

    $this->tabSheets = $tabSheets;
    $this->iTypeSheet = $iTypeSheet;    
    $this->idSheet = $idSheet;
    $this->iWidth = $iWidth;
    $this->iHeight = $iHeight;

    $this->urlBaseImg = ( defined("ALK_URL_SI_IMAGES") ? ALK_URL_SI_IMAGES : "/media/images/" );

    $this->cssFormSheets = "Sheets";
    $this->cssFormLayerSheet = "SheetLayer";

    $this->iSheetHeight = 19;
    $this->iSheetWidthBorder = 1;
    $this->iSheetWidthTransition = 8;
    $this->tabImgSheet = array("SheetDeb"        => "sheetdeb.gif",
                               "SheetDebRol"     => "sheetdeb.gif",
                               "Sheet"           => "sheet_".$iTypeSheet.".gif",
                               "SheetRol"        => "sheetrol_".$iTypeSheet.".gif",
                               "SheetToSheet"    => "sheet2sheet.gif",
                               "SheetRolToSheet" => "sheetrol2sheet.gif",
                               "SheetToSheetRol" => "sheet2sheetrol.gif",
                               "SheetEnd"        => "sheetend.gif",
                               "SheetEndRol"     => "sheetendrol.gif",
                               "SheetLineDeb"    => "sheetlinedeb.gif",
                               "SheetLineEnd"    => "sheetlineend.gif");
                               
    $this->tabHtmlContent = array();                                                    
  }
  
  /**
   * @brief Ajoute le contenu html de l'onglet
   *
   * @param strHtml          contenu html de l'onglet 
   * @param idSheet     		 identifiant de l'onglet dont on met � jour le contenu si -1, contenu correspond � l'onglet courant
   * @return Retourne un int
   */
  function AddHtml($strHtml, $idSheet=-1)
  {   
  	$idSheet = ($idSheet == -1 ? $this->idSheet : $idSheet);
  	if (array_key_exists($idSheet, $this->tabHtmlContent))
    	$this->tabHtmlContent[$idSheet] .= $strHtml;
    else
    	$this->tabHtmlContent[$idSheet] = $strHtml;
    	
    return $idSheet;
  }  

  /**
   * @brief Retourne le code html d'un onglet
   * @param strText         texte de l'onglet
   * @param tabSheet        tableau contenant les propri�t�s de l'onglet 
   * @return Retourne un string
   */
  function _GetHtmlOneSheet($strText, $tabSheet) 
  {  	  	    
	
    $strHtmlPointer = "onmouseover=\"this.style.cursor=".
    ( STRNAVIGATOR==NAV_IE ? "'hand'" : "'pointer'" )."\"";
				
    return "<td height='".$this->iSheetHeight."' width='1'><img src='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetDeb"]."'></td>".
      "<td height='".$this->iSheetHeight."' background='".ALK_URL_SI_IMAGES.$this->tabImgSheet["Sheet"]."'".
      " style='padding-left:10px; padding-right:10px' onclick='document.location.href=\"".$tabSheet["url"]."\"' ".
      $strHtmlPointer."><a class='aSheetRol' title=\"".$tabSheet["title"]."\" href=\"".$tabSheet["url"]."\"".
      ">".$strText."</a></td>".
      "<td background='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetEnd"]."' width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>".
      "<img src='".ALK_URL_SI_MEDIA."transp.gif' width='".$this->iSheetWidthTransition."' height='1'></td>";
  }
  
  /**
   * @brief Retourne le code html de la barre d'onglets
   *
   * @return Retourne un string
   */
  function _GetHtmlSheet()
  {
	  $tabSheet = $this->tabSheets[$this->iTypeSheet];

	  $strHtmlPointer = "onmouseover=\"this.style.cursor=".
    ( STRNAVIGATOR==NAV_IE ? "'hand'" : "'pointer'" )."\"";

	  $strHtml = "<table border='0' cellpadding='0' cellspacing='0' width='".$this->iWidth."' align='center'".
	    " style='margin-top:5px; margin-left:10px;'><tr><td valign='top'>";
	
	  if( ALK_B_PRINT==true && isset($_GET["print"]) && $_GET["print"]=="1" ) {
	    $strHtml .= "<tr><td valign='top' style='padding-top:10px'>";
	    return $strHtml;
	  }
	
	  $strHtml .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='left'>";
	
	  $iDelta = 0;
	  $nbSheet = count($tabSheet);
	  reset($tabSheet);
	  while( list($strKey, $tabSheetInfo) = each($tabSheet) ) {
	    if( !is_numeric($strKey) ) 
	      $nbSheet--;
	  }
	
	  $i = 0;
	  reset($tabSheet);
	  while( list($strKey, $tabSheetInfo) = each($tabSheet) ) {
	    if( !is_numeric($strKey) ) {
	      continue;
	    }
	
	    if( $i == 0 ) {
	      // bordure du premier onglet
	      if( $this->idSheet == $tabSheetInfo["idSheet"] ) {
	        $strHtml .= "<td height='".$this->iSheetHeight."' width='1'><img src='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetDebRol"]."'></td>";
	      } else {
	        $strHtml .= "<td height='".$this->iSheetHeight."' width='1'><img src='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetDeb"]."'></td>";
	      }
	    }
	    if( $this->idSheet == $tabSheetInfo["idSheet"] ) {
	      $strHtml .= "<td height='".$this->iSheetHeight."' background='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetRol"]."' '".
	        " style='padding-left:10px; padding-right:10px'>".
	        "<span class='aSheet'>".$tabSheetInfo["text"]."</span></td>";
	    } else {
	      $strHtml .= "<td height='".$this->iSheetHeight."' background='".ALK_URL_SI_IMAGES.$this->tabImgSheet["Sheet"]."'".
	        " style='padding-left:10px; padding-right:10px' onclick=\"document.location.href='".$tabSheetInfo["url"]."'\" ".
	        $strHtmlPointer."><a class='aSheetRol' title=\"".$tabSheetInfo["title"]."\" href=\"".$tabSheetInfo["url"]."\"".
	        ">".$tabSheetInfo["text"]."</a></td>";
	    }
	
	    // transition avec l'onglet suivant
	    if( ($i+1) == $nbSheet ) {
	      // dernier onglet
	      if( $this->idSheet == $tabSheetInfo["idSheet"] )
	        // dernier select
	        $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetEndRol"]."' width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>";
	      else
	        //dernier normal
	        $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetEnd"]."' width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>";
	      $strHtml .= "<img src='".ALK_URL_SI_MEDIA."transp.gif' width='".$this->iSheetWidthTransition."' height='1'></td>";
	    } else {
	      // transition intermediaire
	      if( $this->idSheet == $tabSheetInfo["idSheet"] ) {
	        // select puis normal
	        $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetRolToSheet"]."' width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>";
	      } elseif( $i+1<$nbSheet && $this->idSheet == $tabSheet[$i+1]["idSheet"]) {
	        // normal puis select
	        $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetToSheetRol"]."' width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>";
	      } else {
	        // normal puis normal
	        $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetToSheet"]."' width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>";
	      }
	      $strHtml .= "<img src='".ALK_URL_SI_MEDIA."transp.gif' width='".$this->iSheetWidthTransition."' height='1'></td>";
	    }
	    $i++;
	  }
	
	  // transition elastique entre le dernier onglet et la fin de tableau
	  $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$this->tabImgSheet["SheetLineEnd"]."' width='100%'><img width='1' height='1'></td>";
	
	  $nbTypeSheet = count($this->tabSheets);
	  if( $this->iTypeSheet == ALK_CONSULT_TYPESHEET && $nbTypeSheet>1 && count($this->tabSheets[ALK_ADMIN_TYPESHEET])>0 ) {
	    // partie consultation avec onglet admin
	    $strHtml .= $this->_GetHtmlOneSheet($this->tabSheets[ALK_ADMIN_TYPESHEET]["text"], $this->tabSheets[ALK_ADMIN_TYPESHEET][0], ALK_ADMIN_TYPESHEET);
	    if( $this->iTypeSheet == ALK_CONSULT_TYPESHEET && $nbTypeSheet>2 && count($this->tabSheets[2])>0 ) {
	      // partie consultation avec onglet propri�t�s
	      $strHtml .= $this->_GetHtmlOneSheet($this->tabSheets[ALK_PROPRIETE_TYPESHEET]["text"], $this->tabSheets[ALK_PROPRIETE_TYPESHEET][0], ALK_PROPRIETE_TYPESHEET);
	    }
	  } elseif( $this->iTypeSheet == ALK_ADMIN_TYPESHEET && $nbTypeSheet>1 && count($this->tabSheets[ALK_CONSULT_TYPESHEET])>0) {
	    // partie admin avec onglet consultation
	    $strHtml .= $this->_GetHtmlOneSheet($this->tabSheets[ALK_CONSULT_TYPESHEET]["text"], $this->tabSheets[ALK_CONSULT_TYPESHEET][0], ALK_CONSULT_TYPESHEET);
	    if( $this->iTypeSheet == ALK_ADMIN_TYPESHEET && $nbTypeSheet>2 && count($this->tabSheets[ALK_PROPRIETE_TYPESHEET])>0) {
	      // partie admin avec onglet propri�t�s
	      $strHtml .= $this->_GetHtmlOneSheet($this->tabSheets[ALK_PROPRIETE_TYPESHEET]["text"], $this->tabSheets[ALK_PROPRIETE_TYPESHEET][0], ALK_PROPRIETE_TYPESHEET);
	    }
	
	  } elseif( $this->iTypeSheet == ALK_PROPRIETE_TYPESHEET && $nbTypeSheet>2 && count($this->tabSheets[ALK_CONSULT_TYPESHEET])>0) {
	    // partie propri�t�s avec onglet consultation
	    $strHtml .= $this->_GetHtmlOneSheet($this->tabSheets[0]["text"], $this->tabSheets[ALK_CONSULT_TYPESHEET][0], ALK_CONSULT_TYPESHEET);
	    if( $this->iTypeSheet == ALK_PROPRIETE_TYPESHEET && $nbTypeSheet>2 && count($this->tabSheets[ALK_ADMIN_TYPESHEET])>0) {
	      // partie propri�t�s avec onglet admin
	      $strHtml .= $this->_GetHtmlOneSheet($this->tabSheets[ALK_ADMIN_TYPESHEET]["text"], $this->tabSheets[ALK_ADMIN_TYPESHEET][0], ALK_ADMIN_TYPESHEET);
	    }
	  }
	
	
	  $strHtml .= "</tr></table>".
	    "</td></tr>";
	
	  /* subsheet : non utilis�, non test�
	  if( array_key_exists("subSheet", $tabSheet) && count($tabSheet["subSheet"])>0 ) {
	    $tabSubSheet = $tabSheet["subSheet"];
	    $strHtml = "<tr><td>".
	      "<table border='0' cellpadding='0' cellspacing='2' class='tableSubSheet' width='100%'><tr>";
	    for($i=0; $i<count($tabSubSheet); $i++) {
	      if( $tabSubSheet[$i]["visible"] == true ) {
	        if( $tabSubSheet[$i]["idSSheet"] == $this->iSSheet )
	          $strHtml .= "<td class='tdSubSheetRol'><a class='aSSheetRol'";
	        else
	          $strHtml .= "<td class='tdSubSheet'><a class='aSSheet'";
	        
	        $strHtml .= "title=\"".$tabSubSheet[$i]["title"]."\"".
	          " href=\"".$tabSubSheet[$i]["url"].
	          ( $tabSubSheet[$i]["target"] != "" ? " target=\"".$tabSubSheet[$i]["target"]."\"" : "" ).
	          "\">".$tabSubSheet[$i]["text"]."</a></td>";
	      }
	    }
	    $strHtml .= "<td width='100%'></td></tr></table>".
	      "</td></tr>";
	  }*/
	
	  $strHtml .= "<tr><td valign='top' style='padding-top:10px'>";

    return $strHtml;
  }

  /**
   * @brief Retourne le code html l'ent�te du panel onglets
   *
   * @return Retourne un string
   */
  function _GetHtmlHeader()
  {
    $strHtml = "";

    $strHtml .= "<table cellspacing='0' cellpadding='0' border='0'>".
      "<tr><td".( $this->iWidth != "-1" ? " width='".$this->iWidth."'" : "" ).">".
      $this->_GetHtmlSheet().
      "</td></tr>".
      "<tr><td".( $this->iHeight != "-1" ? " height='".$this->iHeight."'" : "" ).">";

    return $strHtml;
  }

  /**
   * @brief Retourne le code html de la fin du panel onglets
   *
   * @brief Retourne un string
   */
  function _GetHtmlFooter()
  {
    $strHtml = "</td></tr></table>";      

    return $strHtml;
  }

  /**
   * @brief Retourne le code html du panel d'onglets
   *
   * @brief Retourne un string
   */
  function GetHtml()
  {
    $strHtml = $this->_GetHtmlHeader().
    					 $this->tabHtmlContent[$this->idSheet].
    					 $this->_GetHtmlFooter();
    					 	
    return $strHtml;
  }
}

?>