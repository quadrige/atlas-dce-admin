<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief classe d'affichage d'un composant de saisie texte
 *        g�n�re : <input type=text>
 *                 <input type=password>
 *                 <textarea></textarea>
 *        classe multilingue : passer un tableau sur param value
 *                             renseigner les attributs tabLangue et urlBaseDrapeau
 */
class HtmlText extends HtmlCtrlBase
{

  /** = vrai si champ mot de passe, faux par d�faut */
  var $bPassword;
  
  /** nombre de lignes, = 1 par d�faut */
  var $row;

  /** nombre de colonnes, = 0 par d�faut */
  var $colomn;
  
  /** longueur max, = 0 par d�faut */
  var $maxLength;

  /** largeur en pixels, = vide par d�faut */
  var $width;

  /** hauteur en pixels, = vide par d�faut */
  var $height;

  /** vrai si memo repr�sent� par fckeditor, =faux par d�faut */
  var $bEditor;

  /** param�tres de fckeditor */
  var $tabParamEditor;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode     Mode du controle de saisie : =0 modif, =1 lecture
   * @param name      Nom du controle de saisie
   * @param value     Valeur par d�faut du controle de saisie
   * @param label     Etiquette texte associ�e au controle de saisie
   * @param row       Nombre de ligne (=1 : <input type=text>, >1 : <textarea>). =1 par d�faut
   * @param column    Nombre de colonnes. =0 par d�faut
   * @param maxlength Longueur max du texte saisi. =0 par d�faut
   */
  function HtmlText($iMode, $name, $value="", $label="", $row=1, $column=0, $maxlength=0 )
  {
    parent::HtmlCtrlBase($iMode, $name, $value, $label);
    
    $this->bReadOnly = false;
    $this->bPassword = false;
    $this->row = $row;
    $this->column = $column;
    $this->maxLength = $maxlength;
    $this->width = "";
    $this->height = "";
    $this->bEditor = false;
    $this->strQuot = "&#34;";
    $this->tabParamEditor = array("pathUpload"  => "",
                                  "styleCss"    => "site_gen.css",
                                  "styleXml"    => "fckstyles.xml",
                                  "templateXml" => "fcktemplates.xml",
                                  "lg"          => "fr");

    $this->bMultiLangue = ( is_array($value) ? true : false );    
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   *
   * @return Retourne un string dans le cas 1 langue
   *         Retourne un tableau dans le cas n langues
   */
  function GetCtrlHtml()
  {
    // contruit le controle
    $strHtml = "";
    if( $this->iMode == "1" ) {
      // mode lecture
      if( $this->bMultiLangue == false ) {
        $strHtml = "<span class=\"".$this->cssCtrl."\">".$this->value."</span>";
      } else {
        foreach($this->tabLangue as $key => $tabLg) {
          $strImgDrap = "";
          if( $this->nbLangue > 1 )
            $strImgDrap = "<img src=\"".$this->urlBaseDrapeau."drapeau_".$key.".gif\"".
              " alt=\"".$tabLg["rep"]."\" title=\"".$tabLg["rep"]."\" align='top'>";
          $strHtml .= "<div class=\"".$this->cssCtrl."\">".$strImgDrap.$this->value[$key]."</div>";
          if( $this->bShowMultiLangue == false )
            break;
        }
      }
    } else {
      if( $this->bMultiLangue == false ) {
        $strHtml .= $this->_GetHtmlUpdateMode();
      } else {
        foreach($this->tabLangue as $key => $tabLg) {
          $strImgDrap = "";
          if( $this->nbLangue > 1 &&  $this->bShowMultiLangue==true ) {
            $strImgDrap = "<img src=\"".$this->urlBaseDrapeau."drapeau_".$key.".gif\"".
              " alt=\"".$tabLg["rep"]."\" title=\"".$tabLg["rep"]."\" align='top'>";
          }
          $strHtml .= $this->_GetHtmlUpdateMode($key).$strImgDrap.
            ( $this->bShowMultiLangue==false && $key>1
              ? ""
              : "<br>");
        }
      }
    }

    if( $this->iMode == "0" )
      $strHtml .= $this->GetHtmlValidator();

    return $strHtml;
  }

  /**
   * @brief G�n�re puis retourne le code html du ctrl texte dans la langue sp�cifi�e.
   *        si multilingue :
   *          - g�n�re 1 hidden suppl�mentaires : ancienne valeur
   *          - si showMultilingue=false,
   *              - retourne la valeur fran�aise en text,
   *              - retourne les valeurs �trang�res en hidden
   *
   * @param $iLangue  Identifiant de la langue
   * @return Retourne un string
   */
  function _GetHtmlUpdateMode($iLangue="0")
  {
    $strHtml = "";
    $strReadOnly = "";
    $strDisabled = "";
    if( $this->bReadOnly == true ) $strReadOnly = " readonly ";
    if( $this->bDisabled == true ) $strDisabled = " disabled ";

    $strName = $this->name;
    if( $this->bMultiLangue == true ) {
      $strName = $this->name.$this->tabLangue[$iLangue]["bdd"];
      $strValue =  str_replace("\"", $this->strQuot, $this->value[$iLangue]);
      $strHtml .= "<input type=\"hidden\" name=\"a".$strName."\" value=\"".$strValue."\">";
      if( $this->bShowMultiLangue == false && $iLangue>1 ) {
        $strHtml .= "<input type=\"hidden\" name=\"".$strName."\" value=\"".$strValue."\">";
        return $strHtml;
      }
    } else {
      $strValue = str_replace("\"", $this->strQuot, $this->value); 
    }

    $iDelta = 0;
    if( $this->column>30 ) $iDelta = -24;
    if( $this->width=="" ) $this->width = ($this->column + ($this->row=="1" ? 0 : 2))*8+$iDelta;

    $strClass = "class=\"".$this->cssCtrl.rtrim($strDisabled).rtrim($strReadOnly)."\"";
    $bIE = true;
    if( $this->strNavigateur == "Netscape4" ) {
      $strClass = "";
      $bIE = false;
    }
    if( $bIE && ($this->width!="" || $this->height!="") ) {
      $strClass .= " style=\"";
      if( $this->width!="" ) $strClass .= "width: ".$this->width."px;";
      if( $this->height!="" ) $strClass .= "height: ".$this->height."px;";
      $strClass .= "\"";
    }

    if( $this->row == 1 ) {
      // input type=text
      $strHtml .= "<input ".
        "type=\"".($this->bPassword==true ? "password" : "text")."\" ".
        "name=\"".$strName."\" ".
        "value=\"".$strValue."\" ".
        $strReadOnly.
        $strDisabled.
        $strClass.
        ( $this->column!="0" ? " size=\"".$this->column."\"" : "").
        ( $this->maxLength!="0" ? " maxlength=\"".$this->maxLength."\"" : "").
        $this->GetHtmlEvent().
        ">";
    } else {
      // textarea
      if( $this->bEditor==true ) {

        $strConstJs = "___EDITEUR_FCKEDITOR_JS";
        if( $this->bEditor==true && !defined($strConstJs) ) {
          define($strConstJs, true);
          $strHtml .= "<script language='javascript' src='../editeur/fckeditor.js'></script>".
            "<script language='javascript' src='../../classes/form/htmltext.js'></script>";
        }
        $strHtml .= $this->_GetFCKEditorJS($strName, ($this->bMultiLangue ? $this->value[$iLangue] : $this->value));

      } else {
        $strHtml .= "<textarea ".
          "name=\"".$strName."\" ".
          $strDisabled.
          $strReadOnly.
          $strClass.
          ( $this->column!="0" ? " cols=\"".$this->column."\"" : "").
          ( $this->row!="" ? " rows=\"".$this->row."\"" : "").
          $this->GetHtmlEvent().
          ">".
          $strValue.
          "</textarea>";
      }
    }

    return $strHtml;
  }

  /**
   * @brief Retourne le code javascript utile pour ouvrir un �diteur m�mo
   *        de type fckeditor avec une barre outils minimale
   *
   * @param strValue  Valeur du controle
   * @return Retourne un string
   */
  function _GetFCKEditorJS($strName, $strValue)
  {
    $iHeight = ( $this->row!="" ? 40+$this->row*16 : 104 );
    $iWidth = $this->width;

    $strParam = "&ServerPath=".ALK_SIALKE_DIR.$this->tabParamEditor["pathUpload"]."&UrlRoot=".ALK_ROOT_URL;

    $strHtml = "<script language='javascript'>".
      " var sBasePath = '".ALK_ROOT_URL.ALK_SIALKE_DIR."scripts/editeur/';".

      " var oFCKeditor = new FCKeditor('".$strName."');".
      " oFCKeditor.BasePath   = sBasePath;".
      " oFCKeditor.ToolbarSet = 'Basic';".
      " oFCKeditor.Height     = ".$iHeight.";".
      " oFCKeditor.Width      = ".$iWidth.";".

      " oFCKeditor.Config['EditorAreaCSS']      = '".$this->tabParamEditor["styleCss"]."';".
      " oFCKeditor.Config['StylesXmlPath']      = '".ALK_ROOT_URL.ALK_SIALKE_DIR."styles/".$this->tabParamEditor["styleXml"]."';".
      " oFCKeditor.Config['TemplatesXmlPath']   = '".ALK_ROOT_URL.ALK_SIALKE_DIR."styles/".$this->tabParamEditor["templateXml"]."';".
      " oFCKeditor.Config['AutoDetectLanguage'] = false;".
      " oFCKeditor.Config['ToolbarStartExpanded'] = false;".
      " oFCKeditor.Config['DefaultLanguage']    = '".$this->tabParamEditor["lg"]."';".

      " oFCKeditor.Config['ImageBrowserURL'] = sBasePath+".
      "   'editor/filemanager/browser/default/browser.html?Type=&Connector=connectors/php/connector.php".$strParam."';".
      " oFCKeditor.Config['LinkBrowserURL']  = sBasePath+".
      "   'editor/filemanager/browser/default/browser.html?Type=&Connector=connectors/php/connector.php".$strParam."';".

      " oFCKeditor.Config['ImageUploadURL'] = sBasePath+'editor/filemanager/upload/php/upload.php?Type=".$strParam."';".
      " oFCKeditor.Config['LinkUploadURL']  = sBasePath+'editor/filemanager/upload/php/upload.php?Type=".$strParam."';".

      " oFCKeditor.Value	= '".str_replace(chr(13).chr(10), "", addslashes($strValue))."';".

      " oFCKeditor.Create();".

      "</script>";

    return $strHtml;
  }

  /**
   * @brief Met � jour un param�tre de l'�diteur
   *
   * @param strParamName  Nom du param�tre
   * @param strParamValue Valeur du param�tre
   */
  function SetParamEditor($strParamName, $strParamValue)
  {
    if( array_key_exists($strParamName, $this->tabParamEditor) ) {
      $this->tabParamEditor[$strParamName] = $strParamValue;
    }
  }
  
  /**
   * @brief Passe le controle en mode �diteur
   *
   * @param 
   */
  function SetEditor()
  {
    $this->bEditor = true;
  }

}
?>