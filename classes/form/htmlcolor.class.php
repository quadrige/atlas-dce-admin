<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief Ecrit le code javascript n�cessaire pour afficher la palette de couleur
 *        dans une fen�tre popup
 *
 * @param strFormNam Nom du formulaire
 * @param strPopup   Nom de la popup
 * @param strImg     Nom de l'image utilis�e pour afficher les cases couleurs (url complet)
 * @param strImgNoColor     Nom de l'image utilis�e pour afficher la cases sans couleur (url complet)
 * @param strStyle   Nom de la classe de style css
 */
function writeJsColor($strFormName, $strPopup, $strImg, $strImgNoColor, $strStyle, $bEcho=true)
{
	if ($bEcho)
  	echo getJsColor($strFormName, $strPopup, $strImg, $strImgNoColor, $strStyle);
  else
  	return getJsColor($strFormName, $strPopup, $strImg, $strImgNoColor, $strStyle);
}

function getJsColor($strFormName, $strPopup, $strImg, $strImgNoColor, $strStyle)
{
  return "\nvar strFormName = '".$strFormName."';\n".
    "var strImgColor = '".$strImg."';\n".
    "var strStyleColor = '".$strStyle."';\n".
    "function openPopupColor(strPage,strNom,strProprietes,iWidth,iHeight)\n".
    "{\n".
    "  var H = (screen.height - iHeight) / 2;\n".
    "  var L = (screen.width - iWidth) / 2;\n".
    "  popupCarte = window.open(strPage,strNom,strProprietes+',height='+iHeight+',width='+iWidth+',top='+H+',left='+L);\n".
    "  popupCarte.focus();\n".
    "}\n".
    "function changeColor(strName,strNoneAllowed)\n".
    "{\n".
    "  openPopupColor('".$strPopup."?'+strFormName+'&'+strName+'&'+strNoneAllowed,'Colors','scrollbars=0,resizable=0',500,400);\n".
    "}\n".
    "function SetColor(oCtrl){".
    "var td=document.getElementById('td_'+oCtrl.name);\n".
    "if (td){\n".
    "   td.style.backgroundColor=oCtrl.value;\n".
    "   var Img=new Image();\n" .
    "   if (oCtrl.value!=''){\n".
    "       Img.src='".$strImg."';\n".
    "       document.images['img_'+oCtrl.name].src=Img.src;\n".
    "       td.style.backgroundColor=oCtrl.value;\n".
    "  } else {\n" .
    "       Img.src='".$strImgNoColor."';\n" .
    "       document.images['img_'+oCtrl.name].src=Img.src;\n".
    "   }\n".
    " }\n".
    "}\n".
    "function writeColor(strName, strColor, iWidth, iHeight, strNoneAllowed, bBorderColor) {\n".
    "  if ((strColor=='') || (strColor=='AUCUNE')) {\n" .
    "  var strImage='".$strImgNoColor."';".
    "  } else {\n" .
    "  var strImage = strImgColor;".
    "    }\n" .
    "    strHtml = '<table border=0 cellpadding=0 cellspacing=0 width='+iWidth+'>';\n".
    "    strHtml = strHtml + '<tr><td width='+iWidth+' height='+iHeight+' bgcolor=\"'+strColor+'\" id=\"td_'+strName+'\" '+" .
    "      (bBorderColor ? 'style=\"border:#000000 1px solid\"' : '')+'>';\n" .
    "    strHtml = strHtml + '<a href=\"javascript:changeColor(\''+strName+'\',\''+strNoneAllowed+'\');\">';\n".
    "    strHtml = strHtml + '<img name=\"img_'+strName+'\" src=\"'+strImage+'\" alt =\'aucune\' width='+iWidth+' height='+iHeight+' border=0>';\n".
    "    strHtml = strHtml + '</a></td></tr>';\n".
    "    strHtml = strHtml + '</table>';\n".
    "  document.write(strHtml);\n".
    "}\n";
}

/**
 * @brief Classe d'affichage d'un composant de s�lection de couleur dans une palette pr�d�finie
 */
class HtmlColor extends HtmlCtrlBase
{
  // attributs
  var $iWidth;
  var $iHeight;
  var $strNoneAllowed;
  var $bBorderColor = false;
  
  var $JS_ISSET = false;

  /**
   * @brief Constructeur par d�faut
   *
   * @param strName        Nom du controle
   * @param strValue       Valeur par d�faut
   * @param strLabel       Etiquette texte associ�e au controle de saisie
   * @param iWidth         largeur du controle
   * @param iHeight        hauteur du controle
   * @param strNoneAllowed contient "true" ou "false" �crit dans une cha�ne et sp�cifie si 'aucune couleur' est autoris�
   */
  function HtmlColor($strName, $strValue, $strLabel, $iWidth, $iHeight, $strNoneAllowed, $bPopup=true)
  {
    parent::HtmlCtrlBase(0, $strName, $strValue, $strLabel);
    $this->iWidth = $iWidth;
    $this->iHeight = $iHeight;
    $this->strNoneAllowed = $strNoneAllowed;
    $this->bPopup = $bPopup;
  }
  
  function getHtmlEvent(){
    $strEventOnChange = "";
    $strEventOthers = "";
    
    foreach($this->tabEvents as $strEvent => $strFunctionJs){
      if ($strEvent=="onchange")
        $strEventOnChange .= $strFunctionJs;
      else
        $strEventOthers .= " ".$strEvent."=\"javascript:".$strFunctionJs."\"";
    }
    return array($strEventOnChange, $strEventOthers);
  }
  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetCtrlHtml()
  {
    if ( !$this->bPopup ) return $this->getCtrlNoPopup();
    
    list($strEventOnChange, $strEventOthers) = $this->getHtmlEvent();
    $strHtml = HtmlColor::setJs();
    $strHtml .= 
      "<input id='inputcolor_".$this->name."' type='hidden' name='".$this->name."' value='".$this->value."' onchange=\"SetColor(this);".$strEventOnChange."\" ".$strEventOthers.">";
    
    return $strHtml;
  }
  function getCtrlNoPopup(){
    list($strEventOnChange, $strEventOthers) = $this->getHtmlEvent();
    $strHtml = HtmlColor::setJs().
               "<input type='hidden' id='".$this->name."' name='".$this->name."' value='".$this->value."' onchange=\"".$strEventOnChange."\" ".$strEventOthers."/>";
    
    $strHtml .= "<div id='color_".$this->name."'".
      " style='width:".$this->iWidth."px; padding-left:".$this->iWidth."px; height:".$this->iHeight."px; border:1px solid #000000; background-color:".$this->value."; display:inline;'".
      " onclick='javascript:OpenColorPallet_".$this->name."()'>&nbsp;</div>".
      "<span id='value_".$this->name."'".
      " style='width:50px; margin-left:4px;'>".
      "<a href='javascript:OpenColorPallet_".$this->name."()'>".$this->value."</a></span>";
      
    $strHtml .= "<div id='allColorPallet_".$this->name."' style='display:none;position:relative;float:left;z-index:1000'>".
      "<div id='colorPallet_".$this->name."' style='position:absolute;float:left; background-color:#ffffff; border:1px solid #999999; padding:2px;'></div></div>";

    return $strHtml;
  
  }
  function setJs(){
    if ( $this->JS_ISSET ) return "";
    $strJs = "";
    $tabLibJs = array();
    if ( !$this->bPopup ){
      $strJs .= "var g_url_img_vide = '".ALK_URL_SI_IMAGES."transp.gif';";
      
      $strJs .= " var bColorPalett_".$this->name." = false;" .
        " var oColorPalett_".$this->name." = null;" .
        " var oColorInter_".$this->name." = null;" .
        " function DefineColorPalett_".$this->name."(){" .
        "  delete(oColorPalett_".$this->name.");" .
        "  if ( typeof oColorPalett_".$this->name." == 'undefined' || oColorPalett_".$this->name." == null) {" .
        "    oColorPalett_".$this->name." = new PaletteCouleurs('".$this->name."', '".$this->name."', ".($this->strNoneAllowed ? "true" : "false").", '".substr($this->value, 1)."');".
        "    oColorInter_".$this->name." = oColorPalett_".$this->name.".ui;".
        "  }".
        "  bColorPalett_".$this->name." = true;" .
        " }" .
        " function OpenColorPallet_".$this->name."(){" .
        " if (!(bColorPalett_".$this->name." && typeof oColorPalett_".$this->name." != 'undefined' && oColorPalett_".$this->name." != null) ) " .
        "   DefineColorPalett_".$this->name."();".
        " oColorPalett_".$this->name.".OpenCloseColorPallet();" .
        "}" .
        "var ol_".$this->name." = window.onload || function(){};" .
        "window.onload = function(){ol_".$this->name."();DefineColorPalett_".$this->name."();}" ;
      
      $tabLibJs[] = ALK_SIALKE_URL."classes/form/htmlcolor.js";
      $strLibJs[] = ALK_SIALKE_URL."lib/lib_layerjs.php";
    } else {
      $strJs .= "window.writeColor('".$this->name."', '".$this->value."', ".$this->iWidth.", ".$this->iHeight.", '".$this->strNoneAllowed."', ".($this->bBorderColor ? "true" : "false").");";
    }
    if ( $strJs != "")
      $strJs = "<script type='text/javascript'>".$strJs."</script>";
    foreach($tabLibJs as $file){$strJs .= "<script type='text/javascript' src='".$file."'></script>";}
    $this->JS_ISSET = true;
    return $strJs;
    
  }
}

?>