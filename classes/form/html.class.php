<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief classe d'affichage de code html brute
 */
class Html extends HtmlCtrlBase
{
  /** code html brute */
  var $strHtml; 

  /**
   * @brief Constructeur par d�faut
   *
   * @param strHtml Code html
   */
  function Html($strHtml, $label="")
  {
    parent::HtmlCtrlBase(0, "", "", $label);
    $this->strHtml = $strHtml;
  }

  /**
   * @brief Retourne le code html associ� � ce controle
   */
  function GetCtrlHtml()
  {
    return $this->strHtml;
  }
}

?>