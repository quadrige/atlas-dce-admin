function getLeft(l)
{
  if (l.offsetParent) return (l.offsetLeft + getLeft(l.offsetParent));
  else return (l.offsetLeft);
}
function getTop(l)
{
  if (l.offsetParent) return (l.offsetTop + getTop(l.offsetParent));
  else return (l.offsetTop);
}
function LayerEvt(idLayerToMove,idTdRef,iX, iY) {
    this.idLayerToMove=idLayerToMove;
    this.idTdRef=idTdRef;
    this.iX=iX;
    this.iY=iY;
}
function MoveLayers()
{
  for (i=0; i<tabEvt.length; i++){
    if( document.all ) { 
      var objTdRef = eval("document.all."+tabEvt[i].idTdRef);
      var objLayer = eval("document.all."+tabEvt[i].idLayerToMove);
      objLayer.style.visibility = "visible";
      objLayer.style.top = (tabEvt[i].iY + getTop(objTdRef)) + "px";
      objLayer.style.left = (tabEvt[i].iX + getLeft(objTdRef)) + "px";
    } else if( document.layers ) {
      var objTdRef = document.layers[tabEvt[i].idTdRef];
      var objLayer = document.layers[tabEvt[i].idLayerToMove];
      objLayer.style.visibility = "show";
      objLayer.style.top = (tabEvt[i].iY + getTop(objTdRef)) + "px";
      objLayer.style.left = (tabEvt[i].iX + getLeft(objTdRef)) + "px";
    } else  {
      var objTdRef = document.getElementById(tabEvt[i].idTdRef);
      var objLayer = document.getElementById(tabEvt[i].idLayerToMove);
      objLayer.style.visibility = "visible";
      objLayer.style.top = (tabEvt[i].iY + getTop(objTdRef)) + "px";
      objLayer.style.left = (tabEvt[i].iX + getLeft(objTdRef)) + "px";
    }
  } 
}
function winReload() 
{
  MoveLayers();  
}
function onMouseOverCalCell(oTd, strCssRoll, bCanClick)
{
  if( !oTd ) return;
  if( oTd.isSelected && oTd.isSelected>0 ) return;
  oTd.isSelected = 1;
  oTd.old_className = oTd.className;
  oTd.className = strCssRoll;
  if( bCanClick )
    SetCursorHand(oTd);
}
function onMouseOutCalCell(oTd)
{
  if( !oTd ) return;
  if( !oTd.isSelected || oTd.isSelected && oTd.isSelected==0 ) return;
  oTd.isSelected = 0;
  oTd.className = oTd.old_className;
  if( oTd.old_cursor ) {
    if( oTd.style )
      oTd.style.cursor = oTd.old_cursor;
    else
      oTd.cursor = oTd.old_cursor;
  }
}
function SetCursorHand(oObj)
{
  oObj.old_cursor = oObj.style.cursor;
  oObj.style.cursor = 'hand';
  if( oObj.style.cursor != 'hand' )
    oObj.style.cursor = 'pointer';
}
