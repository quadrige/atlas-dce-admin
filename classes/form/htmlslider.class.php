<?php
include_once("htmltext.class.php");

/**
 * @brief classe d'affichage d'un composant de type slider accompagn� d'une zone de saisie texte
 */
class HtmlSlider extends HtmlText
{
  /** Largeur du slider dessin� */
  var $iWidthSlider;
  
  /** Valeur minimale du slider */
  var $min_value;
  
  /** Valeur maximale du slider */
  var $max_value;
  
  /** Fonction JS de calcul de la nouvelle valeur */
  var $strIncrementFunction;
  
  
  /**
   *  Constructeur par d�faut
   *
   * @param iMode     Mode du controle de saisie : =0 modif, =1 lecture
   * @param name      Nom du controle de saisie
   * @param value     Valeur par d�faut du controle de saisie
   * @param label     Etiquette texte associ�e au controle de saisie
   * @param separator caract�re s�parateur entre jour mois et ann�e ( / par d�faut)
   */
  function HtmlSlider($iMode, $name, $value="", $label="", $row=1, $column=0, $maxlength=0, $iWidthSlider=150)
  {
    parent::HtmlText($iMode, $name, $value, $label, $row, $column, $maxlength);
    
    $this->iWidthSlider = $iWidthSlider;
    $this->min_value = 0;
    $this->max_value = 100;
    $this->strIncrementFunction = "numericIncrementSlider";
    $this->bWriteId = true;
  }
  
  
  /**
   *  G�n�re puis retourne le code html associ� au controle de saisie
   *
   * @return Retourne un string dans le cas 1 langue
   *         Retourne un tableau dans le cas n langues
   */
  function getCtrlHtml()
  {
    $this->addEvent("onblur", "objSlider_".$this->name.".setPosition(this.value/100);");
    
    
    $strHtml = "<div style='float:left'>" .
                "<div id='slider_".$this->name."' style='float:left;margin-left:5px;margin-right:5px;'></div>" .
                "<div style='float:left'>".parent::getCtrlHtml()."</div>" .
               "</div>";
               
    global $__bLoadSliderJs__;
    
    if (!(isset($__bLoadSliderJs__) && $__bLoadSliderJs__==true)){
      $strHtml .= "<script language='javascript' src='".ALK_SIALKE_URL."classes/form/htmlslider.js'></script>";
      $__bLoadSliderJs__ = true;
    }
    
    $strHtml .= 
        "<script language='javascript'>" .
        "objSlider_".$this->name." = CreateSlider(" .
            "'".$this->name."', " .
            $this->value.", " .
            "'".$this->strIncrementFunction."'," .
            "'".$this->min_value."'," .
            "'".$this->max_value."'," .
            "'".$this->iWidthSlider."'" .
        ");" .
        "objSlider_".$this->name.".setPosition(".$this->value."/100);" .
        "</script>";
    return $strHtml;
  }
  
  /**
   * Param�tre le slider
   * @param min_value               Valeur minimale du slider 
   * @param max_value               Valeur maximale du slider 
   * @param strIncrementFunction    Fonction JS de calcul de la nouvelle valeur
   */
  function setParametersSlider($min_value, $max_value, $strIncrementFunction)
  {
    $this->min_value = $min_value;
    $this->max_value = $max_value;
    $this->strIncrementFunction = $strIncrementFunction;
  }
}
?>