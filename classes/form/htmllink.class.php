<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief Classe d'affichage d'un lien texte ou image
 *        g�n�re : <a href=></a>
 *                 <a href=><img /></a>
 */
class HtmlLink extends HtmlCtrlBase
{
  /** texte info bulle, par d�faut = $strTxt */
  var $toolTip;

  /** classe CSS sur le lien texte, par d�faut aContenuLien */
  var $class;

  /** url de l'image */
  var $strUrlImg;

  /** url de l'image roll */
  var $strUrlImgRol;

  /** fenetre cible */
  var $target;

  /** nom de l'image */
  var $strImgName;

  /** alignement vertical de l'image */
  var $imgVerticalAlign;

  /**
   * @brief Constructeur par d�faut
   *
   * @param strlink       Lien plac� sur le texte ou l'image
   * @param strTxt        Texte associ� au lien ou � l'image en info-bulle
   * @param strImg        Nom de l'image
   * @param strImgRol     Nom de l'image roll
   * @param strUrlBaseImg Url de base des images
   */
  function HtmlLink($strLink, $strTxt, $strImg="", $strImgRol="", $strUrlBaseImg="")
  {
    parent::HtmlCtrlBase(0, "", $strLink, $strTxt);
    $this->tooTip = "";
    $this->class = "";
    $this->urlImg = $strImg;
    $this->urlImgRol = $strImgRol;
    $this->target = "";
    $this->urlBaseImg = "";
    if(  $strUrlBaseImg != "" ) {
      $this->urlBaseImg = $strUrlBaseImg;
    } elseif( defined("ALK_URL_SI_IMAGES") ==true ) {
      $this->urlBaseImg = ALK_URL_SI_IMAGES;
    }
    if( $strImg != "" )
      $this->tooTip = $strTxt;
    $this->strImgName = "";
    $this->imgVerticalAlign = "absmiddle";
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetCtrlHtml()
  {
    // compteur pour identifier les images
    global $_aff_bouton_;
    
    if( $this->class != "" ) $this->cssLink = $this->class;

    // contruit le controle
    $strHtml = "";
    if( $this->urlImg == "" &&  $this->urlImgRol == "" ) {
      // lien hypertexte
      $strHtml = "<a class=\"".$this->cssLink."\" href=\"".$this->value."\"".
        ( $this->target != "" ? " target=\"".$this->target."\"" : "" ).
        ( $this->tooTip != "" ? " title=\"".$this->tooTip."\"" : "" ).
        $this->GetHtmlEvent().
        ">".
        $this->label.
        "</a>";
    } else {
      // lien image
      if( isset($_GET["print"]) && $_GET["print"]=="1" ) return "&nbsp;";
      
      if( !isset($_aff_bouton_) ) $_aff_bouton_ = 0;
      $_aff_bouton_++;
      
      $strImgName = $this->urlImg.$_aff_bouton_;
      $this->strImgName = $strImgName;

      $strToolTip = "";
      if( $this->tooTip != "" ) $strToolTip = " title=\"".$this->tooTip."\"";
      $strTarget = "";
      if( $this->target != "" ) $strTarget = " target=\"".$this->target."\"";

      if( $this->urlImg != "#" && $this->urlImg != "" ) {
        if( $this->urlImgRol != "" ) {
          $strHtml = "<a ".
            $strTarget.
            $strToolTip.
            " href=\"".$this->value."\"".
            $this->GetHtmlEvent().
            " onMouseOut=\"MM_swapImgRestore()\"". 
            " onMouseOver=\"MM_swapImage('".$strImgName."', '', '". $this->urlBaseImg.$this->urlImgRol."', 1)\">".
            "<img ".
            " align=\"".$this->imgVerticalAlign."\"".
            " name=\"".$strImgName."\"".
            " border=\"0\"".
            " src=\"".$this->urlBaseImg.$this->urlImg."\" />".
            "</a>";
        } else {
          $strHtml = "<a ".
            $strTarget.
            $this->GetHtmlEvent().
            $strToolTip.
            " href=\"".$this->value."\">".
            "<img ".
            " align=\"".$this->imgVerticalAlign."\"".
            " name=\"".$strImgName."\"".
            " border=\"0\"".
            " src=\"".$this->urlBaseImg.$this->urlImg."\" />".
            "</a>";
        }
      } else {
        $strHtml = "<img ".
          $strToolTip.
          " align=\"".$this->imgVerticalAlign."\"".
          " name=\"".$strImgName."\"".
          " border=\"0\"".
          " src=\"".$this->urlBaseImg.$this->urlImg."\" />";
      }
    }
    return $strHtml;
  }

  /**
   * @brief Retourne le nom de l'image normale
   *
   * @return Retourne un string
   */
  function getImageName()
  {
    return $this->strImgName;
  }

}

?>