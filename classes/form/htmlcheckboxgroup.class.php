<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief classe d'affichage d'un composant groupe de case � cocher
 *        g�n�re : <input type=checkbox>
 */
class HtmlCheckboxGroup extends HtmlCtrlBase
{
  /** Valeurs d'un groupe de cases � cocher */
  var $tabValue;
  
  /** liste des cases � cocher */
  var $tabCheckbox;

  /** S�parateur cases � cocher (non utilis�) */
  var $separator;
  
  /** vrai si label � droite de la case � cocher */
  var $bLabelRight;
  
  /** nombre d'�l�ments par ligne (si 0 alors affichage sur 1 ligne, si 1 alors affichage sur 1 colonne, autrement iNbItem par ligne) */
  var $iNbItem;
  
  /** vrai si renommage des checkbox , pas de renommage pour les controles nomm�s name[] */
  var $bRename;

  /**
   * @brief Constructeur par d�faut
   * Chaque checkbox a pour nom $this->name concat�n� avec son index dans la liste
   *
   * @param iMode      Mode du controle de saisie : =0 modif, =1 lecture
   * @param name       Nom du controle de saisie
   * @param value      Valeur du groupe de cases � cocher
   * @param label      Etiquette texte associ�e au groupe
   */
  function HtmlCheckboxGroup($iMode, $name, $value, $label="")
  {
    parent::HtmlCtrlBase($iMode, $name, $value, $label);
    $this->tabCheckbox = array();
    $this->tabValue = ( is_array($value) ? $value : array() );
    $this->bLabelRight = true;
    $this->iNbItem = 0;
    $this->bRename = !preg_match("!\[.*\]!", $name);
  }

  /**
   * @brief Ajoute une case � cocher au groupe
   * 
   * @param strValue  Valeur de la case � cocher si coch�e
   * @param strLabel  Etiquette texte associ�e � la case � cocher
   */
  function AddCheckBox($strValue, $strLabel)
  {
    $this->tabCheckbox[count($this->tabCheckbox)] = array("value" => $strValue, 
                                                          "label" => $strLabel,
                                                          "event" => array());
  }

  /**
   * @brief Ajoute un evenement sur une case � cocher
   * 
   * @param strValue      Valeur de la case � cocher
   * @param strEvent      Nom de l'�v�nement
   * @param strFunctionJS Nom de la fonction javascript appel�e (pas n�cessaire d'ajouter 'javascript:')
   */
  function AddEventToCheckBox($strValue, $strEvent, $strFunctionJS)
  {
    // recherche l'indice de la valeur
    $iPos = -1;
    while( ++$iPos < count($this->tabCheckbox) ) {
      if( $this->tabCheckbox[$iPos]["value"] == $strValue ) break;
    }

    if( $iPos == -1 || $iPos>=count($this->tabCheckbox) ) 
      return;

    $this->tabCheckbox[$iPos]["event"] = array_merge( $this->tabCheckbox[$iPos]["event"], array($strEvent => $strFunctionJS));
  }
  
  /**
   * @brief Genere puis retourne tous les evenements li�s au controle html
   *
   * @param iPos  Indice dans la liste de cases � cocher
   * @return Retourne les �v�nements concat�n�s dans une chaine pour �tre inclus dans
   *         dans un tag html de type controle de saisie
   */
  function GetHtmlEventCheckBox($iPos)
  {
    $strHtml = "";
    while( list($strEvent, $strFunctionJs) = each($this->tabCheckbox[$iPos]["event"]) )
      $strHtml .= " ".$strEvent."=\"javascript:".$strFunctionJs."\"";
    return $strHtml;
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetCtrlHtml()
  {
    // contruit le controle
    $strHtml = "";
    if( count($this->tabCheckbox)>0 ) {
      if( $this->iMode == "1" ) {
        // mode lecture
        // affiche les labels s�lectionn�s
        for($i=0; $i<count($this->tabCheckbox); $i++) {
          if( in_array($this->tabCheckbox[$i]["value"], $this->tabValue) ) {
            $strHtml .= ( $strHtml != "" ? ", " : "" ).$this->tabCheckbox[$i]["label"];
          }
        }
        if( $strHtml != "" ) {
          $strHtml = "<span class=\"".$this->cssCtrl."\">".$strHtml."</span>";
        }
      } else {
  
        // mode modif
        // input type=CheckBox
        if ($this->bRename)
          $strHtml .= "<input type='hidden' name='".$this->name."' value='".count($this->tabCheckbox)."'>";
        for($i=0; $i<count($this->tabCheckbox); $i++) {
          if ( $i==0 )
            $strHtml .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
          
          if ( $this->iNbItem!=0 && $i%$this->iNbItem==0 ) 
            $strHtml .= "<tr>";
          
          $strHtml .= "<td class=\"".$this->cssCtrl."\">";
          
          if( $this->bLabelRight == false )
            $strHtml .= "<span class=\"".$this->cssCtrl."\">".$this->tabCheckbox[$i]["label"]." </span>";
  
          $strHtml .= "<input type=\"CheckBox\" ".
            "name=\"".$this->name. ($this->bRename ? "_".$i : "") ."\" ".
            "value=\"".$this->tabCheckbox[$i]["value"]."\"".
            ( in_array($this->tabCheckbox[$i]["value"], $this->tabValue) ? " checked" : "").
            $this->GetHtmlEventCheckBox($i).
            ">";
  
          if( $this->bLabelRight == true )
            $strHtml .= "<span class=\"".$this->cssCtrl."\"> ".$this->tabCheckbox[$i]["label"]."</span>";
  
          $strHtml .= "</td>";
          
          if ( $this->iNbItem!=0 && (($i%$this->iNbItem)-$this->iNbItem==1 )) 
            $strHtml .= "</tr>";
          
          if ( $i==count($this->tabCheckbox)-1)
            $strHtml .= "</table>";
        }
      }
    } else {
      $strHtml = "<span class=\"".$this->cssCtrl."\"> Aucun(e)</span>";
    }
    if( $this->iMode == "0" )
      $strHtml .= $this->GetHtmlValidator();

    return $strHtml;
  }
}

?>