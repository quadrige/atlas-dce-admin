<?php
include_once("htmlform.class.php");

/**
 * @brief Classe affichant un panel sous forme d'onglets 
 *        G�re le contenu sous forme de layer visible ou invisible
 */
class HtmlFormSheets extends HtmlForm
{
  /** ensemble des onglet */
  var $tabSheets;

  /** tableau d'index permettant de retourver l'indice d'un onglet � partir de son identifiant */
  var $tabSheetsIndex;
  
  /** identifiant de l'onglet s�lectionn� */
  var $idSheet;

  /** largeur du panel */
  var $iWidth;

  /** hauteur du panel */
  var $iHeight;

  /** url de base pour les images d'onglets =  ALK_URL_SI_IMAGES par d�faut */
  var $urlBaseImg;

  /** classe css appliqu�e au tableau contenant les onglets */
  var $cssFormSheets;

  /** classe css appliqu�e � chaque layer sur lequel on pose le contenu d'un onglet (visibility=hidden obligatoirement) */
  var $cssFormLayerSheet;

  /** hauteur d'un onglet en px */
  var $iSheetHeight;
  /** largeur d'une image de bordure d'un onglet */
  var $iSheetWidthBorder;
  /** largeur d'une image de transition entre onglet */
  var $iSheetWidthTransition;
  /** tableau des noms d'images utilis�s */
  var $tabImgSheet;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode     Mode du formulaire : =1 ajout, =2 maj, =3 lecture
   * @param name      Nom du formulaire
   * @param strAction Action du formulaire (url appel�e sur validation)
   * @param iMethod   ALK_FORM_METHOD_GET (par d�faut) ou ALK_FORM_METHOD_POST
   * @param idSheet   Identifiant de l'onglet s�lectionn�
   * @param iWidth    Largeur du panel en pixel (-1 : largeur non prise en compte, valeur par d�faut)
   * @param iHeight   Hauteur du panel en pixel (-1 : hauteur non prise en compte, valeur par d�faut)
   */
  function HtmlFormSheets($iMode, $name, $strAction="", $iMethod=ALK_FORM_METHOD_GET, $idSheet="0", $iWidth="-1", $iHeight="-1")
  {
    parent::HtmlForm($iMode, $name, $strAction, $iMethod);

    $this->tabSheets = array();
    $this->tabSheetsIndex = array();
    $this->idSheet = $idSheet;
    $this->iWidth = $iWidth;
    $this->iHeight = $iHeight;

    $this->urlBaseImg = ( defined("ALK_URL_SI_IMAGES") ? ALK_URL_SI_IMAGES : "/media/images/" );

    $this->cssFormSheets = "formSheets";
    $this->cssFormLayerSheet = "formSheetLayer";

    $this->iSheetHeight = 19;
    $this->iSheetWidthBorder = 1;
    $this->iSheetWidthTransition = 8;
    $this->tabImgSheet = array("SheetDeb"        => "formsheetdeb.gif",
                               "SheetDebRol"     => "formsheetdebrol.gif",
                               "Sheet"           => "formsheet.gif",
                               "SheetRol"        => "formsheetrol.gif",
                               "SheetToSheet"    => "formsheet2sheet.gif",
                               "SheetRolToSheet" => "formsheetrol2sheet.gif",
                               "SheetToSheetRol" => "formsheet2sheetrol.gif",
                               "SheetEnd"        => "formsheetend.gif",
                               "SheetEndRol"     => "formsheetendrol.gif",
                               "SheetLineDeb"    => "formsheetlinedeb.gif",
                               "SheetLineEnd"    => "formsheetlineend.gif");

    $this->AddScriptJs("../../classes/form/htmlformsheets.js");
  }

  /**
   * @brief Ajoute un onglet � la liste
   *
   * @param strTitle         Titre de l'onglet
   * @param strTitleInfo     Info bulle sur le titre
   * @param strTitleUrl      Url sp�cifique sur le titre (="" par d�faut)
   * @param strFileTemplate  nom du fichier template utilis� pour afficher les blocs sur l'onglet (=vide par d�faut)
   * @return Retourne un int
   */
  function AddSheet($idSheet, $strTitle, $strTitleInfo, $strTitleUrl="", $strFileTemplate="", $strClassCss="")
  {
    $iSheet = count($this->tabSheets);
    $this->tabSheets[$iSheet] = array("idSheet"   => $idSheet,
                                      "title"     => $strTitle,
                                      "titleInfo" => $strTitleInfo,
                                      "titleUrl"  => $strTitleUrl,
                                      "template"  => $strFileTemplate,
                                      "css"       => ($strClassCss=="" ? $this->cssFormSheets : $strClassCss));
    $this->tabSheetsIndex[$idSheet] = $iSheet;
    $this->tabHtmlPanel[$iSheet] = array();
    $this->tabHtmlLink[$iSheet] = array();
    return $idSheet;
  }

  /**
   * @brief Construit un bloc, l'ajoute au formulaire sur l'onglet sp�cifi� puis retourne une r�f�rence sur l'objet cr��
   *
   * @param idSheet     Identifiant de l'onglet
   * @param idBlock     Identifiant du bloc
   * @param strTitle    Titre du bloc (= chaine vide par d�faut, dans ce cas, le cadre titre n'est pas affich�)
   * @param strDesc     Description du bloc (= chaine vide par d�faut)     
   * @param iWidthLabel Largeur en px de la colonne label (=180 par d�faut)
   * @param iWidthCtrl  Largeur en px de la colonne ctrl  (=390 par d�faut)
   * @param bMaintitle  Vrai si titre principal, faux si titre de bloc classique (par d�faut)
   * @return Retourne une r�f�rence sur le bloc cr��
   */
  function AddBlock($idSheet, $idBlock, $strTitle="", $strDesc="", $iWidthLabel="180", $iWidthCtrl="390", $bMaintitle=false)
  {
    $iSheet = (isset($this->tabSheetsIndex[$idSheet]) ? $this->tabSheetsIndex[$idSheet] : 0);
    $oBlock = new HtmlBlock($this, $this->iMode, $idBlock, $strTitle, $strDesc, $iWidthLabel, $iWidthCtrl, $bMaintitle, $iSheet);
    if( !array_key_exists($iSheet, $this->tabHtmlPanel) ) {
      $this->tabHtmlPanel[$iSheet] = array();
    }
    if( !array_key_exists($iSheet, $this->tabHtmlLink) ) {
      $this->tabHtmlLink[$iSheet] = array();
    }
    $this->tabHtmlPanel[$iSheet][count($this->tabHtmlPanel[$iSheet])] =& $oBlock;
    return $oBlock;
  }

  /**
   * @brief Construit un bloc, l'ajoute au formulaire sur l'onglet sp�cifi� puis retourne une r�f�rence sur l'objet cr��
   *
   * @param idSheet     Identifiant de l'onglet
   * @param idBlock     Identifiant du bloc
   * @param strTitle    Titre du bloc (= chaine vide par d�faut, dans ce cas, le cadre titre n'est pas affich�)
   * @param strDesc     Description du bloc (= chaine vide par d�faut)     
   * @param iWidthLabel Largeur en px de la colonne label (=180 par d�faut)
   * @param iWidthCtrl  Largeur en px de la colonne ctrl  (=390 par d�faut)
   * @param bMaintitle  Vrai si titre principal, faux si titre de bloc classique (par d�faut)
   * @return Retourne une r�f�rence sur le bloc cr��
   */
  function AddBlockNoSheet($idBlock, $strTitle="", $strDesc="", $iWidthLabel="180", $iWidthCtrl="390", $bMaintitle=false)
  {
    $iSheet = "-1";
    $this->tabSheetsIndex[$iSheet] = $iSheet;
    $oBlock = new HtmlBlock($this, $this->iMode, $idBlock, $strTitle, $strDesc, $iWidthLabel, $iWidthCtrl, $bMaintitle, $iSheet);
    if( !array_key_exists($iSheet, $this->tabHtmlPanel) ) {
      $this->tabHtmlPanel[$iSheet] = array();
    }
    if( !array_key_exists($iSheet, $this->tabHtmlLink) ) {
      $this->tabHtmlLink[$iSheet] = array();
    }
    $this->tabHtmlPanel[$iSheet][count($this->tabHtmlPanel[$iSheet])] =& $oBlock;
    return $oBlock;
  }

  /**
   * @brief Construit un lien, l'ajoute au formulaire sur l'onglet sp�cifi� puis retourne une r�f�rence sur l'objet cr��
   *
   * @param idSheet       Identifiant de l'onglet
   * @param idLink        identifiant du lien
   * @param strlink       Lien plac� sur le texte ou l'image
   * @param strTxt        Texte associ� au lien ou � l'image en info-bulle
   * @param strImg        Nom de l'image
   * @param strImgRol     Nom de l'image roll
   * @return Retourne une r�f�rence sur le lien cr��
   */
  function &AddLink($idSheet, $idLink, $strLink, $strTxt, $strImg="", $strImgRol="")
  {
    $iSheet = $this->tabSheetsIndex[$idSheet];
    $oLink = new HtmlLink($strLink, $strTxt, $strImg, $strImgRol);
    $oLink->name = $idLink;
    if( !array_key_exists($iSheet, $this->tabHtmlPanel) ) {
      $this->tabHtmlPanel[$iSheet] = array();
    }
    if( !array_key_exists($iSheet, $this->tabHtmlLink) ) {
      $this->tabHtmlLink[$iSheet] = array();
    }
    $this->tabHtmlLink[$iSheet][count($this->tabHtmlLink[$iSheet])] =& $oLink;
    return $oLink;
  }

  /**
   * @brief Retourne le code html de la barre d'onglets
   *
   * @return Retourne un string
   */
  function _GetHtmlSheet()
  {
    $strHtml = "<table cellspacing='0' cellpadding='0' border='0' class='".$this->cssFormSheets."'><tr>";
    $iPosSheet = -1;
    $nbSheet = count($this->tabSheets);
    $tabImgRoll = array();
    foreach($this->tabSheets as $iSheet => $tabSheet) {
      $idTd0 = "td_".$this->name."_".($iSheet*4);
      $idTd1 = "td_".$this->name."_".($iSheet*4+1);
      $idTd2 = "td_".$this->name."_".($iSheet*4+2);
      $idTd3 = "td_".$this->name."_".($iSheet*4+3);

      if( $iSheet==0 ) {
        // bordure du premier onglet
        if( $iSheet == $tabSheet["idSheet"] ) {
          $strHtml .= "<td id='".$idTd0."' background='".$this->urlBaseImg.$this->tabImgSheet["SheetDebRol"]."'".
            " height='".$this->iSheetHeight."' width='".$this->iSheetWidthBorder."'>";
          $tabImgRoll[$idTd0] = array($this->urlBaseImg.$this->tabImgSheet["SheetDeb"], true);
        } else {
          $strHtml .= "<td id='".$idTd0."' background='".$this->urlBaseImg.$this->tabImgSheet["SheetDeb"]."'".
            " height='".$this->iSheetHeight."' width='".$this->iSheetWidthBorder."'>";
          $tabImgRoll[$idTd0] = array($this->urlBaseImg.$this->tabImgSheet["SheetDebRol"], false);
        }
        $strHtml .= "<img src='".$this->urlBaseImg."transp.gif' width='".$this->iSheetWidthBorder."' height='1'></td>";
      }
      
      // corps de l'onglet
      if( $this->idSheet == $tabSheet["idSheet"] ) {
        $iPosSheet = $iSheet;
        $strHtml .= "<td id='".$idTd1."' background='".$this->urlBaseImg.$this->tabImgSheet["SheetRol"]."' style='font-weight:bold'";
        $tabImgRoll[$idTd1] = array($this->urlBaseImg.$this->tabImgSheet["Sheet"], true);
      } else {
        $strHtml .= "<td id='".$idTd1."' background='".$this->urlBaseImg.$this->tabImgSheet["Sheet"]."'";
        $tabImgRoll[$idTd1] = array($this->urlBaseImg.$this->tabImgSheet["SheetRol"], false);
      }
      $strHtml .= " height='".$this->iSheetHeight."' class='".$tabSheet["css"]."'".
        " title=\"".$tabSheet["titleInfo"]."\"".
        " onmouseover='AlkSheetOnMouseOver(this)'".
        " onmouseout='AlkSheetOnMouseOut(this)'".
        ( $tabSheet["titleUrl"] == "" 
          ? " onclick=\"AlkSheetOnClick('".$this->name."', '".$iSheet."')\">"
          : " onclick=\"".$tabSheet["titleUrl"]."\">" ).
        $tabSheet["title"]."</td>";
      
      // transition avec l'onglet suivant
      if( ($iSheet+1) == $nbSheet ) {
        // dernier onglet
        if( $iSheet == $tabSheet["idSheet"] ) {
          // dernier select
          $strHtml .= "<td id='".$idTd3."' background='".$this->urlBaseImg.$this->tabImgSheet["SheetEndRol"]."'".
            " width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>";
          $tabImgRoll[$idTd3] = array($this->urlBaseImg.$this->tabImgSheet["SheetEnd"], true);
        } else {
          //dernier normal
          $strHtml .= "<td id='".$idTd3."' background='".$this->urlBaseImg.$this->tabImgSheet["SheetEnd"]."'".
            " width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>";
          $tabImgRoll[$idTd3] = array($this->urlBaseImg.$this->tabImgSheet["SheetEndRol"], false);
        }
        $strHtml .= "<img src='".$this->urlBaseImg."transp.gif' width='".$this->iSheetWidthTransition."' height='1'></td>";
      } else {
        // transition intermediaire
        if( $this->idSheet == $tabSheet["idSheet"] ) {
          // select puis normal
          $strHtml .= "<td id='".$idTd2."' background='".$this->urlBaseImg.$this->tabImgSheet["SheetRolToSheet"]."'".
            " width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>";
          $tabImgRoll[$idTd2] = array($this->urlBaseImg.$this->tabImgSheet["SheetToSheet"], true);
        } elseif( $iSheet+1<$nbSheet && $this->idSheet == $this->tabSheets[$iSheet+1]["idSheet"] ) {
          // normal puis select
          $strHtml .= "<td id='".$idTd2."' background='".$this->urlBaseImg.$this->tabImgSheet["SheetToSheetRol"]."'".
            " width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>";
          $tabImgRoll[$idTd2] = array($this->urlBaseImg.$this->tabImgSheet["SheetToSheet"], true);
        } else {
          // normal puis normal
          $strHtml .= "<td id='".$idTd2."' background='".$this->urlBaseImg.$this->tabImgSheet["SheetToSheet"]."'".
            " width='".$this->iSheetWidthTransition."' height='".$this->iSheetHeight."'>";
          $tabImgRoll[$idTd2] = array($this->urlBaseImg.$this->tabImgSheet["SheetRolToSheet"], false);
        }
        $strHtml .= "<img src='".$this->urlBaseImg."transp.gif' width='".$this->iSheetWidthTransition."' height='1'></td>";
      }
      $iSheet++;
    }

    // transition elastique entre le dernier onglet et la fin de tableau
    $strHtml .= "<td background='".$this->urlBaseImg.$this->tabImgSheet["SheetLineEnd"]."' width='100%'><img width='1' height='1'></td>".
      "</tr></table>";
    
    $strHtml .= "<script language='javascript'>".
      " var oFormSheets_".$this->name." = new AlkFormSheets('".$iPosSheet."', '".$this->name."');".
      " function AlkFormSheetsInit_".$this->name."() {";
    foreach($tabImgRoll as $idTd => $tabImgRol) {
      $tabId = explode("_", $idTd);
      $iSheet = floor($tabId[count($tabId)-1] / 4);
      $strHtml .= " oFormSheets_".$this->name.".AddTd('".$idTd."', '".$tabImgRol[0]."', ".($tabImgRol[1] ? "true" : "false").", '".$iSheet."');";
    }
    $strHtml .=  "} </script>";

    return $strHtml;
  }
 
  /**
   * @brief Retourne le code html l'ent�te du panel onglets
   *
   * @return Retourne un string
   */
  function GetHtmlHeader()
  {
    $strHtml = parent::GetHtmlHeader();

    $strHtml .= "<table cellspacing='0' cellpadding='0' border='0' align='center' width='100%'>".
      "<tr><td".( $this->iWidth != "-1" ? " width='".$this->iWidth."'" : "" ).">";
    
    // �criture des blocks sans onglets
    if ( array_key_exists("-1", $this->tabHtmlPanel) ){
      /*foreach ( $this->tabHtmlPanel["-1"] as $iBlock=>$obj ){
        $strHtml .= "<tr><td valign='top'>". $this->tabHtmlPanel["-1"][$iBlock]->getHtml()."</td></tr>";      
      }*/
      
      $strHtml .= parent::GetHtmlPanels(-1, $this->strTemplateFile, "class='".$this->cssFormLayerSheet."'");   
    }
    
    $strHtml .=
      // ecriture de l'entete des onglets 
      $this->_GetHtmlSheet().
      "</td></tr>".
      "<tr><td".( $this->iHeight != "-1" ? " height='".$this->iHeight."'" : "" ).">";

    return $strHtml;
  }

  /**
   * @brief Retourne le code html du bloc et des controles qu'il contient
   *
   * @return Retourne un string
   */
  function GetHtmlPanels($iSheet=0, $strTemplateFile=null, $strCss="")
  {
    $strHtml = "";
    // ecriture du contenu de chaque onglet
    foreach($this->tabSheets as $iSheet => $tabSheet) {
      $strHtml .= "<div id='sheet_".$this->name."_".$iSheet."'".
        ( $this->idSheet == $tabSheet["idSheet"] ? "" : " class='".$this->cssFormLayerSheet."'" ).
        ">";
      $strTemplateFile = $tabSheet["template"];
      $strHtml .= parent::GetHtmlPanels($iSheet, $strTemplateFile, "class='".$this->cssFormLayerSheet."'");
      $strHtml .= "</div>";
    }
    return $strHtml;
  }

  /**
   * @brief Retourne le code html de la fin du panel onglets
   *
   * @brief Retourne un string
   */
  function GetHtmlFooter()
  {
    $strHtml = "</td></tr></table>".
      parent::GetHtmlFooter();

    return $strHtml;
  }

}

?>