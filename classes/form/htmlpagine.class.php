<?php
include_once("htmlpanel.class.php");

/**
 * @brief Classe affichant une liste pagin�e
 */
class HtmlPagine extends HtmlPanel
{
  /** Nombre d'�l�ment au total (hors pagination) */
  var $iNbElt;
  
  /** num�ro de page actuel (=1 par d�faut) */
  var $iNumPage;

  /** nombre �l�ments par page (=20 par d�faut) */
  var $iNbEltParPage;

  /** vrai si r�sultat pagin�, faux sinon (true par d�faut) */
  var $bPagine;

  /** tableau contenant l'alignement et la largeur de chaque colonne */
  var $tabAlign;

  /** nombre de colonnes */
  var $iNbCol;

  /** tableau contenant les titres de chaque colonne */
  var $tabTitle;

  /** tableau contenant les droits de chaque colonne */
  var $tabRight;

  /** tableau contenant les infos de chaque ligne */
  var $tabData;

  /** url de pagination */
  var $strUrlPagine;

  /** vrai= tabData contient toutes les donn�es, faux = tabData ne contient que les donn�es de la page (faux par d�faut) */
  var $bAllData;

  /** Message affich� pour le cas o� pas de r�sultat */
  var $strMsgNoResult;

  /** Message affich� pour le cas o� pas de colonne affich�e (pour cause de droit) */
  var $strMsgNoColumn;

  /** Vrai si le compteur des r�sultats ("indiceMin - indiceMax / iNbElt") est affich� */
  var $bAffCompteur;

  /** Nombre r�el de ligne de r�sultats hors rowspan */
  var $iNbRealElt;

  /** Vrai si le combo de choix du nombre de r�sultats par page est affich� */
  var $bSelectNbElts;

 /** Tableau du combo de choix du nombre de r�sultats */
  var $tabValSelect;

  /** Largeur de la table */
  var $iWidth;
  
  /**
   * @brief Constructeur par d�faut
   * 
   * @param iNbElt         Nombre d'�l�ment au total (hors pagination)
   * @param iNumPage       Num�ro de page actuel (=1 par d�faut)
   * @param iNbEltParPage  Nombre �l�ments par page (=20 par d�faut)
   * @param bPagine        Vrai si r�sultat pagin�, faux sinon (true par d�faut)
   * @param strUrlPagine   url de pagination
   * @param bAllData       vrai= tabData contient toutes les donn�es, faux = tabData ne contient que les donn�es de la page (faux par d�faut)
   */
  function HtmlPagine($iNbElt=0, $iNumPage=1, $iNbEltParPage=20, $bPagine=true, $strUrlPagine="", $strMsgNoResult="", $bAllData=false, $strName="tabPagine", $bSelectNbElts = false, $tabValSelect = array())
  {
    parent::HtmlPanel($strName);

    $this->iNbElt        = $iNbElt;
    $this->iNumPage      = $iNumPage;
    $this->iNbEltParPage = $iNbEltParPage;
    $this->bPagine       = $bPagine;
    $this->strUrlPagine  = $strUrlPagine;
    $this->bAllData      = $bAllData;
    $this->strMsgNoResult= $strMsgNoResult;
    $this->strMsgNoColumn= "";
    $this->iNbCol        = 0;
    $this->tabAlign      = array();
    $this->tabTitle      = array();
    $this->tabRight      = array();
    $this->tabData       = array();
    $this->bAffCompteur  = false; 
    $this->iNbRealElt    = -1;
    $this->bSelectNbElts = $bSelectNbElts;
    $this->tabValSelect = $tabValSelect;
    $this->iWidth        = "";
  }

  /**
   * @brief D�finit l'alignement et la largeur de chaque colonne
   *        Un param�tre est du type array(align, width) 
   *        Le nombre de param�tre n'est pas limit�. 
   *        Ce nombre est la borne sup � ne pas d�passer lors des appels des autres m�thodes
   */
  function SetColumns()
  {
    $this->SetColumnsByArray(func_get_args());
  }

  /**
   * @brief D�finit l'alignement et la largeur de chaque colonne
   *        Un param�tre est du type array(align, width) 
   *        Le nombre de param�tre n'est pas limit�. 
   *        Ce nombre est la borne sup � ne pas d�passer lors des appels des autres m�thodes
   *
   * @param tabParam Param�tres du tableau : align et width 
   */
  function SetColumnsByArray($tabParam)
  {
    $this->iNbCol = count($tabParam);
    $this->tabAlign = $tabParam;
  }

  /**
   * @brief Affecte les titres aux colonnes.
   *        Possibilit� de colspan sur les colonnes.
   *        Un param�tre est du type array(title, colspan) ou title equivalent � array(title, 1)
   */
  function AddTitleRow()
  {
    $this->AddTitleRowByArray(func_get_args());
  }

  /**
   * @brief Affecte les titres aux colonnes.
   *        Possibilit� de colspan sur les colonnes.
   *        Un param�tre est du type array(title, colspan, rowspan) ou title equivalent � array(title, 1, 1)
   *
   * @param tabParam Tableau contenant les titres de chaque colonne
   */
  function AddTitleRowByArray($tabParam)
  {
    if( count($tabParam) > $this->iNbCol ) 
      return;

    $tabTitle = array();
    for($i=0; $i<count($tabParam); $i++) {
      $oElt = $tabParam[$i];
      if( is_array($oElt) ){
        if ( count($oElt)==2 )
           $oElt[] = 1;
        $tabTitle[] = $oElt;
      }
      else
        $tabTitle[] = array($oElt, 1, 1);
    }
    $this->tabTitle[count($this->tabTitle)] = $tabTitle;
  }
   
  /**
   * @brief Affecte les droits aux colonnes (pas de colspan)
   *        Valeur de right : 0=aucun, 1=visible
   */
  function AddRightRow()
  {
    $this->AddRightRowByArray(func_get_args());
  }

  /**
   * @brief Affecte les droits aux colonnes (pas de colspan)
   *        Valeur de right : 0=aucun, 1=visible
   * @param tabRightRow tableau contenant les droits des colonnes 
   */
  function AddRightRowByArray($tabRightRow)
  {
    if( count($tabRightRow) != $this->iNbCol ) 
      return;

    for($i=0; $i<count($tabRightRow); $i++) {
      $oElt = $tabRightRow[$i];
      if( is_numeric($oElt) && $oElt==1 )
        $this->tabRight[] = 1;
      else
        $this->tabRight[] = 0;
    }
  }
  /**
   * @brief Ajoute une ligne au tableau. 
   *        Chaque param�tre correspondant � une colonne. 
   *        Un param�tre est du type array(htmldata, colspan, rowspan) ou htmldata equivalent � array(htmldata, 1, 1)
   *        Le nombre de param�tre n'est pas limit�
   */
  function AddRow()
  {
    $this->AddRowByArray(func_get_args());
  }

  /**
   * @brief Ajoute une ligne au tableau. 
   *        Chaque param�tre correspondant � une colonne. 
   *        Un param�tre est du type array(htmldata, colspan, rowspan) ou htmldata equivalent � array(htmldata, 1, 1)
   *        Le nombre de param�tre n'est pas limit�
   *
   * @param tabRow Tableau contenant les donn�es d'une ligne
   */
  function AddRowByArray($tabRow)
  {
    if( count($tabRow) > $this->iNbCol ) 
      return;

    $iRow = count($this->tabData);
    for($i=0; $i<count($tabRow); $i++) {
      $oElt = $tabRow[$i];
      if( is_array($oElt) && count($oElt)>=3 )
        $this->tabData[$iRow][$i] = $oElt;
      else
        $this->tabData[$iRow][$i] = array($oElt, 1, 1);
    }
  }
  

  /**
   * @brief Retourne l'entete du tableau de la page courante
   */
  function _GetHtmlHeader()
  {
    $strHtml = "<table class='table1' align='center' id='".$this->name."'".($this->iWidth!="" ? " width='".$this->iWidth."'" : "")."><tr>";
    $nbCol = 0;
    for($i=0; $i< $this->iNbCol; $i++) {
      if( count($this->tabRight) == 0 || 
          count($this->tabRight) > 0 && $this->tabRight[$i] > 0 ) {    
        $strHtml .= "<td ".
          ($i==0 ? "height='0' " : "").
          " width='".$this->tabAlign[$i][1]."'></td>";
        $nbCol++;
      }
    }
    if ($this->bSelectNbElts == true){
      $strCodeJs = "var nbClickValid = 0;" .
        "function SetPagine(nbResults){".
        " if( nbClickValid ==0 ) {".
        "   nbClickValid++;".
        "   var strUrl = '". $this->strUrlPagine."&nbElts='+nbResults;".
        "   document.location.href = strUrl;".      
        " }" .
        "}";     
      $this->AddJs($strCodeJs);
      $strHtml .= "<tr><td colspan='".$this->iNbCol."'>";
      $strHtml .= $this->_getHtmlSelectResults();
      $strHtml .="</td></tr>";
    }
    if( $nbCol == 0 ) {
      $this->iNbCol = 0;
      $strHtml = "<table class='table1' align='center'>";
      return $strHtml;
    }

    $strHtml .= "</tr>";
    
    for($j=0; $j< count($this->tabTitle); $j++) {
      $iRealCol = 0;
      $strHtml .= "<tr class='trEntete1'>";
      for($i=0; $i< count($this->tabTitle[$j]); $i++) {
        if( count($this->tabRight) == 0 || 
            count($this->tabRight) > 0 && $this->tabRight[$iRealCol] > 0 ) {
          $strTitle = "";
          
          if ( $this->bAffCompteur && $i==0 && $j==0 && $this->iNbRealElt!=-1 ){
            $indiceMin = ($this->iNumPage-1) * $this->iNbEltParPage+1;
            $indiceMax = $this->iNbRealElt+$indiceMin-1; 
            
            $strTitle = "<table border='0' cellspacing='0' cellpadding='0' width='100%'>" .
                "<tr><td align='left' class='divTabEntete1'>".$this->tabTitle[$j][$i][0]."</td>" .
                "<td align='right' class='divTabEntete1'>".
                ( !$this->bPagine && $this->bAllData 
                  ? $this->iNbElt." �l�ment".($this->iNbElt>1 ? "s" : "")
                  : ( $this->iNbElt==0 
                    ? "0" 
                    : $indiceMin." - ".$indiceMax ).
                    " / ".$this->iNbElt ).
                "</td>" .
                "</tr></table>";
                
          } else {
            $strTitle = $this->tabTitle[$j][$i][0];
          }
          
          $strHtml .= "<td".
            " class='tdEntete1'".
            " align='".$this->tabAlign[$i][0]."'".
            ( $this->tabTitle[$j][$i][1] > 1 ? " colspan='".$this->tabTitle[$j][$i][1]."'" : "" ).
            ( $this->tabTitle[$j][$i][2] > 1 ? " rowspan='".$this->tabTitle[$j][$i][2]."'" : "" ).
            ">".$strTitle."</td>";
        }
        $iRealCol += $this->tabTitle[$j][$i][1];
      }
    }
    $strHtml .= "</tr>";
    return $strHtml;
  }

  /**
   * @brief Retourne le contenu de la page courante du tableau
   */
  function _GetHtmlBody()
  {
    $strHtml = "";

    if( ALK_B_PRINT==true && isset($_GET["print"]) && $_GET["print"] == "1" ) {
      $this->bPagine = false;
    }

    if( $this->bPagine == false ) {
      $this->iNbEltParPage = $this->iNbElt;
      $this->iNumPage = 1;      
    }

    $iFirst = 0;
    $iLast = min(count($this->tabData)-1, $this->iNbEltParPage-1);

    if( $this->bAllData == true && $this->bPagine == true ) {
      $iFirst = ($this->iNumPage-1)*$this->iNbEltParPage;
      $iLast = min($this->iNbElt-1, $iFirst+$this->iNbEltParPage-1);
    }
    
    if ( !is_null($this->oTemplate) ){
      $this->oTemplate->assign("iFirst", $iFirst);
      $this->oTemplate->assign("iLast", $iLast);
      $this->oTemplate->assign("tabData", $this->tabData);
      $this->oTemplate->assign("tabAlign", $this->tabAlign);
      $this->oTemplate->assign("tabRight", $this->tabRight);
      
      return $this->oTemplate->fetch($this->strTemplateFile);
    }
    
    for($iRow=$iFirst; $iRow<=$iLast; $iRow++) {
      $strHtml .= "<tr id='tr".$this->name.$iRow."' class='" . ( ($iRow % 2) == 0 ? "trPair1" : "trImpair1" )."'>";

      $iRealCol = 0;
      for($iCol=0; $iCol<count($this->tabData[$iRow]); $iCol++) {
        if( count($this->tabRight) == 0 || 
            count($this->tabRight) > 0 && $this->tabRight[$iRealCol] > 0 ) {
          $strHtml .= "<td id='td".$this->name.$iRow."_".$iCol."'".
            " class='".( isset($this->tabData[$iRow][$iCol][4]) 
                        ? $this->tabData[$iRow][$iCol][4] 
                        : (($iRow % 2) == 0 ? "tdPair1" : "tdImpair1") )."'".
            ( $this->tabData[$iRow][$iCol][1] > 1 ? " colspan='".$this->tabData[$iRow][$iCol][1]."'" : "" ).
            ( $this->tabData[$iRow][$iCol][2] > 1 ? " rowspan='".$this->tabData[$iRow][$iCol][2]."'" : "" ).
            " align='".$this->tabAlign[$iRealCol][0]."'". 
            " valign='".(array_key_exists(3, $this->tabData[$iRow][$iCol]) ? $this->tabData[$iRow][$iCol][3] : "top")."'".
            ">".$this->tabData[$iRow][$iCol][0]."</td>";
        }
        $iRealCol += $this->tabData[$iRow][$iCol][1];
      }

      $strHtml .= "</tr>";
    }
    
    return $strHtml;
  }

  /**
   * @brief Retourne la fin de la page courante
   */
  function _GetHtmlFooter()
  {
    return "</table>";
  }


  /**
   * @brief Retourne les liens de pagination
   */
	function _GetHtmlPagination()
  {
    $strHtml = "";

    $bUrlJs = ( strtolower(substr($this->strUrlPagine, 0, 11)) == "javascript:" ? true : false );
	
    $noPageSuiv = $this->iNumPage + 1;
    $noPagePrec = $this->iNumPage - 1;
    $nbTotalPages = floor($this->iNbElt / $this->iNbEltParPage) + ($this->iNbElt % $this->iNbEltParPage==0 ? 0 : 1);

    $noPageDebut = max($this->iNumPage-5, 1);
    $noPageFin   = min($this->iNumPage+5, $nbTotalPages);
    
    // affiche la pagination si resultat sur plusieurs pages
    if( $nbTotalPages>1 && $this->iNumPage<=$nbTotalPages && $this->iNumPage>0 ) {
      $strHtml .= "<div align='center' style='margin-top:16px; margin-bottom:16px'>";
      
      if( $noPagePrec > 0 ) {
        $strBtUrl = $this->strUrlPagine;
        $strBtUrl = ( $bUrlJs == true
                      ? str_replace("page", $noPagePrec, $strBtUrl)
                      : $this->strUrlPagine."&page=".$noPagePrec);
        $oBtPrec = new HtmlLink($strBtUrl, "Page Pr�c�dente", "precedent.gif", "precedent.gif");
        $strHtml .= $oBtPrec->getHtml();
      }
      
      if( $nbTotalPages>1 ) {
        $strHtml .= "&nbsp;&nbsp;&nbsp;<span class='divNavPage'>Pages&nbsp;:&nbsp;</span>";
        for($i=$noPageDebut; $i<=$noPageFin; $i++)
          if( $this->iNumPage == $i ) {
            $strHtml .= " <span class='divNavPage'>".$i."</span>";
          } else {
            $strBtUrl = $this->strUrlPagine;
            $strBtUrl = ( $bUrlJs == true
                          ? str_replace("page", $i, $strBtUrl)
                          : $this->strUrlPagine."&page=".$i);
            $strHtml .= " <a class='aNavPage' href=\"".$strBtUrl."\">".$i."</a>";
          }
        $strHtml .= "&nbsp;&nbsp;&nbsp;";
      }
      
      if( $noPageSuiv <= $nbTotalPages ) {
        $strBtUrl = $this->strUrlPagine;
        $strBtUrl = ( $bUrlJs == true
                      ? str_replace("page", $noPageSuiv, $strBtUrl)
                      : $this->strUrlPagine."&page=".$noPageSuiv);
        $oBtSuiv = new HtmlLink($strBtUrl, "Page Suivante", "suivant.gif", "suivant.gif");
        $strHtml .= $oBtSuiv->getHtml();
      }
      $strHtml .= "</div>";
      
      return $strHtml;
    }  
  }
  
  /**
   * @brief Retourne le contenu html du combo de s�lection du nombre de r�sultats
   */
  function  _getHtmlSelectResults(){
  	
    $oSelect = new HtmlSelect(0, "iNbResults", $this->iNbEltParPage, "Afficher ");
    $oSelect->tabValTxt = $this->tabValSelect;
    $oSelect->AddEvent("onchange", "SetPagine(this.value)");                            
    return "<table><tr><td><span class='formLabel'>". $oSelect->label."</span></td><td>".$oSelect->getHtml()."</td></tr></table>";                            
    
  }

  /**
   * @brief Retourne le contenu html du tableau pagin� 
   */
  function GetHtml()
  {
    $strHtml = "";
    $strHeader = $this->_GetHtmlHeader();
    if( $this->iNbCol == 0 ) {
      $strHtml = "<div class='divContenuTexte' align='center' style='margin-top:30px; margin-bottom:30px'>".
          ( $this->strMsgNoColumn == "" ? "Vous n'avez pas les droits n�cessaires pour consulter cette liste." : $this->strMsgNoColumn ).
          "</div>";
    } else {
      if( $this->iNbElt <= 0 ) {
        $strHtml = $this->GetHtmlJs().
          $strHeader.
          $this->_GetHtmlFooter().
          "<div class='divContenuTexte' align='center' style='margin-top:30px; margin-bottom:30px'>".
          ( $this->strMsgNoResult == "" ? "Aucun �l�ment dans la liste." : $this->strMsgNoResult ).
          "</div>";
      } else {
        $strHtml = $this->GetHtmlJs().
          $strHeader.
          $this->_GetHtmlBody().
          $this->_GetHtmlFooter().
          $this->_GetHtmlPagination();
      }
    }

    return $strHtml;
  }
  

}
?>