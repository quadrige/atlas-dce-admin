<?php
include_once("htmlctrlbase.class.php");

/**
 * @brief Classe d'affichage d'un composant de saisie texte
 *        g�n�re : <input type=file>
 *                 ou une ligne proposant le fichier en t�l�chargement 
 *                 avec case � cocher pour le supprimer
 *                 Nom de la case � cocher : 
 */
class HtmlFile extends HtmlCtrlBase
{
  /** = vrai par d�faut pour afficher la case � cocher de suppression en mode 1 */
  var $bDel;

  /** adresse du fichier � t�l�charger en mode 1 */
  var $urlFile;

  /** nombre de colonnes, =0 par d�faut */
  var $column;

  /** longueur max, =0 par d�faut */
  var $maxlength;

  /** largeur en pixels, = vide par d�faut */
  var $width;

  /** Fen�tre cible sur ouverture du fichier existant */
  var $target;

  /** =true pour affichage simultan� du mode lecture et modif, false sinon (par d�faut) */
  var $bDualMode;

  /** nom du checkbox : par d�faut = del_[name] si bDualMode=false, =[name] sinon */
  var $cbName;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode     Mode du controle de saisie : =0 modif, =1 lecture
   * @param name      Nom du controle de saisie
   * @param value     Valeur par d�faut du controle de saisie
   * @param label     Etiquette texte associ�e au controle de saisie
   * @param column    Taille en caract�re de la zone de saisie (sans prendre en compte la taille du bouton)
   * @param maxlength Longueur max du nom de fichier
   */
  function HtmlFile($iMode, $name, $value="", $label="", $column=0, $maxlength=0)
  {
    parent::HtmlCtrlBase($iMode, $name, $value, $label);
    $this->bDel = true;
    $this->urlFile = "";
    $this->column = $column;
    $this->maxlength = $maxlength;
    $this->width = "";
    $this->target = "_blank";
    $this->bDualMode = false;
    $this->cbName = "del_".$this->name;
    
    $this->bMultiLangue = ( is_array($value) ? true : false );  
  }

  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function GetHtml()
  {
    // contruit le controle
    $strHtml = "";

    // v�rifie la valeur de cbName si cbName poss�de toujours un nom par d�faut
    // sinon on laisse la valeur personnalis�e
    if( $this->cbName == $this->name || $this->cbName == "del_".$this->name ) {
      $this->cbName = ( $this->bDualMode == true ? $this->name : "del_".$this->name );
    } 
    
    if( $this->bDualMode == false || ($this->bDualMode==true && $this->iMode=="1") ) {
      $strHtml = $this->_GetHtml($this->iMode);
    } else {
      // this->bDualMode==true && this->iMode==2
      if( $this->bMultiLangue == false ) {
        $strHtml = "<table border='0' cellpadding='0' cellspacing='0' width='100%'>".
            "<tr><td>".$this->_GetHtml("1")."</td></tr>".
            "<tr><td>".$this->_GetHtml("2")."</td></tr>".
            "</table>";
      } else {
        foreach($this->tabLangue as $key => $tabLg) {
          $strHtml .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>".
            "<tr><td>".$this->_GetHtml("1", $key)."</td></tr>".
            "<tr><td>".$this->_GetHtml("2", $key)."</td></tr>".
            "</table>";
        }
      }
    }
    return $strHtml;
  }
  
  /**
   * @brief G�n�re puis retourne le code html associ� au controle de saisie
   */
  function _GetHtml($iMode, $key="")
  {
    // contruit le controle
    $strHtml = "";
    
    if( $iMode == "1" ) {
      // mode lecture, propose de lire le fichier, de cocher une case pour le supprimer
      if($key!=""){
        $strImgDrap = "";
        if( $this->nbLangue > 1 &&  $this->bShowMultiLangue==true ) {
          $strImgDrap = "<img src=\"".$this->urlBaseDrapeau."drapeau_".$key.".gif\"".
            " alt=\"".$this->tabLangue[$key]["rep"]."\" title=\"".$this->tabLangue[$key]["rep"]."\" align='top'>&nbsp;";
        }
        if( $this->value[$key] != "" ) {
          $strHtml .= $strImgDrap."<a class=\"".$this->cssLink."\" target=\"".$this->target."\"".
            " href=\"".$this->urlFile.$this->value[$key]."\">".$this->value[$key]."</a>";
          if( $this->bDel == true ) {
            $strHtml .= "&nbsp;&nbsp;<input type=\"checkbox\" name=\"".$this->cbName.$this->tabLangue[$key]["bdd"]."\" value=\"1\"".
              $this->GetHtmlEvent().
              ">".
              "<span class=".$this->cssText.">&nbsp;Effacer</span>";
          }
        } else {
          $strImgDrap = "";
          if( $this->nbLangue > 1 &&  $this->bShowMultiLangue==true ) {
            $strImgDrap = "<img src=\"".$this->urlBaseDrapeau."drapeau_".$key.".gif\"".
              " alt=\"".$this->tabLangue[$key]["rep"]."\" title=\"".$this->tabLangue[$key]["rep"]."\" align='top'>&nbsp;";
          }
          $strHtml .= $strImgDrap."<span class=\"".$this->cssText."\">(pas de fichier disponible)</span>";
        }
      } else {
        if( $this->value != "" ) {
          $strHtml .= "<a class=\"".$this->cssLink."\" target=\"".$this->target."\"".
            " href=\"".$this->urlFile."\">".$this->value."</a>";
          if( $this->bDel == true ) {
            $strHtml .= "&nbsp;&nbsp;<input type=\"checkbox\" name=\"".$this->cbName."\" value=\"1\"".
              $this->GetHtmlEvent().
              ">".
              "<span class=".$this->cssText.">&nbsp;Effacer</span>";
          }
        } else {
          $strHtml .= "<span class=\"".$this->cssText."\">(pas de fichier disponible)</span>";
        }
      }      
    } else {
      // mode modif
      $iDeltaColumn = ( strpos($_SERVER["HTTP_USER_AGENT"], "Linux") === false ? 0 : -7 );

      $iDelta = 0;
      if( $this->column>30 ) $iDelta = -24;
      if( $this->width=="" ) $this->width = $this->column*8+$iDelta;
      
      $strClass = " class='".$this->cssCtrl."'";
      $bIE = true;
      if( $this->strNavigateur == "Netscape4" ) {
        $strClass = "";
        $bIE = false;
      }
      
      if( $bIE && $this->width!="" ) $strClass .= " style='width: ".$this->width."px;'";
      
      // input type=file
      if($key!=""){
        $strHtml = "<input type='file' name='".$this->name.$this->tabLangue[$key]["bdd"]."' value=\"".$this->value[$key]."\"";
      } else {
        $strHtml = "<input type='file' name='".$this->name."' value=\"".$this->value."\"";
      }
      $strHtml .= $strClass.
      ( $this->column!="0" ? " size='".($this->column+$iDeltaColumn)."'" : "").
      $this->GetHtmlEvent().
      ">".$this->GetHtmlValidator();
    }
    return $strHtml;
  }

}

?>