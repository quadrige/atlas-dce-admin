<?php
include_once("htmlbase.class.php");

/**
 * @class HtmlPanel
 * @brief Classe repr�sentant un panneau d'affichage html
 */
class HtmlPanel extends HtmlBase
{
  /** tableau contenant du code js */
  var $tabJs;

  /** tableau contenant l'ensemble des variables js */
  var $tabVarJs;

  /** tableau contenant l'ensemble des sources js n�cessaire au formulaire */
  var $tabSrcJs;

  /**
   * @brief Constructeur par d�faut
   * 
   * @param name  Identifiant ou non du panel
   */
  function HtmlPanel($name)
  {
    parent::HtmlBase($name);

    $this->tabJs = array();
    $this->tabVarJs = array();
    $this->tabSrcJs = array();
  }

  /**
   * @brief Ajoute une variable globale javascript � l'ensemble
   *        Permet de passer la valeur d'une variable php � une variable js
   * 
   * @param strVarName nom de la variable
   * @param strValue   valeur de la variable
   */
  function AddVarJs($strVarName, $strValue)
  {
    if( !array_key_exists($strVarName, $this->tabVarJs) )
      $this->tabVarJs[$strVarName] = $strValue;
  }

  /**
   * @brief Ajoute du code javascript au formulaire
   * 
   * @param strCodeJs  code javascript
   */
  function AddJs($strCodeJs)
  {
    $this->tabJs[count($this->tabJs)] = $strCodeJs;
  }

  /**
   * @brief Ajoute une source javascript au formulaire
   *        Se charge de v�rifier si le script n'est pas d�j� demand�
   *
   * @brief strSourceJs  Nom de la source javascript
   */
  function AddScriptJs($strSourceJs)
  {
    $strTmp = preg_replace("/([^_a-zA-Z0-9\%])/", "_", $strSourceJs);
    $strTmp = str_replace("\\", "", $strTmp);
    if( ord($strTmp[0]) >=48 && ord($strTmp[0])<=57 )
      $strTmp = "_".$strTmp;
    $strConstJs = strtoupper($strTmp);
    if( !defined($strConstJs) ) {
      define($strConstJs, true);
      $this->tabSrcJs[] = $strSourceJs;
    }
  }

  /**
   * @brief Retourne le code html du code javascript n�cessaire au formulaire
   *
   * @return Retourne un string
   */
  function GetHtmlJs()
  {
    $strHtml = "";
    if( count($this->tabVarJs) > 0 ) {
      $strHtml .= "<script language='javascript'>";
      reset($this->tabVarJs);
      while( list($strParamName, $strValue) = each($this->tabVarJs) ) {
        $strHtml .= " var ".$strParamName." = ".$strValue.";";
      }
      $strHtml .= " </script>";
    }
    
    for($i=0; $i < count($this->tabSrcJs); $i++) {
      $strHtml .= "<script language='javascript' src='".$this->tabSrcJs[$i]."'></script>";
    } 

    if( count($this->tabJs)>0 ) {
      $strHtml .= "<script language='javascript'>";
      for($i=0; $i < count($this->tabJs); $i++) {
        $strHtml .= $this->tabJs[$i];
      }
      $strHtml .= " </script>";
    }

    return $strHtml;
  }

  /**
   * @brief Retourne le code html du panel
   */
  function GetHtml()
  {
    return "";
  }
}
?>