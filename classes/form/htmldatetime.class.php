<?php
include_once("htmlctrlbase.class.php");

if( !defined("ALK_FORM_TYPEDATE_DATE") )     define("ALK_FORM_TYPEDATE_DATE", 0);
if( !defined("ALK_FORM_TYPEDATE_TIME") )     define("ALK_FORM_TYPEDATE_TIME", 1);
if( !defined("ALK_FORM_TYPEDATE_DATETIME") ) define("ALK_FORM_TYPEDATE_DATETIME", 2);

/**
 * @class HtmlDateTime
 * @brief
 */
class HtmlDateTime extends HtmlCtrlBase
{
  /** type de date : ALK_FORM_TYPEDATE_DATE (par d�faut), ALK_FORM_TYPEDATE_TIME, ALK_FORM_TYPEDATE_DATETIME */
  var $typeDate;

  /** vrai si le calendrier est accessible */
  var $bCalendar;

  /** vrai si affichage de la date du jour si la valeur est null, faux pour afficher chaine vide */
  var $bTodayIfNull;

  /**
   * @brief Constructeur par d�faut
   *
   * @param iMode     Mode du controle de saisie : =0 modif, =1 lecture
   * @param name      Nom du controle de saisie
   * @param value     Valeur par d�faut du controle de saisie. La valeur doit �tre du format dateFormat
   * @param label     Etiquette texte associ�e au controle de saisie
   */
  function HtmlDateTime($iMode, $name, $value="", $label="")
  {
    parent::HtmlCtrlBase($iMode, $name, $value, $label);
    $this->typeDate = ALK_FORM_TYPEDATE_DATE;
    $this->bTodayIfNull = true;
  }
  
  /**
   * @brief Retourne la valeur � afficher dans le format donn� � partir de la donn�e fournie
   *        Cette m�thode transforme l'attribut value en timestamp
   *
   * @return Retourne un array of string : [0] : date time au format donn�, [1] date, [2] time
   */
  function GetDateFormat()
  {
    if( $this->bTodayIfNull && ($this->value=="" || $this->value=="0") ) {
      $this->value = mktime(date("G"), date("i"), 0, date("n"), date("j"), date("Y"));
    } else {
      if( !is_numeric($this->value) ) {
        // [aaaa/mm/jj hh:mn]
        $strPatternDate = '!^([012][0-9]|3[01])/([012][0-9]|3[01])/(20[0-9][0-9])$!';
        $strPatternTime = '!^([0-1][0-9]|2[0-3]):([0-5][0-9])$!';
        $strPatternDateTime = '!^([012][0-9]|3[01])/([012][0-9]|3[01])/(20[0-9][0-9]) ([0-1][0-9]|2[0-3]):([0-5][0-9])$!';
        $tabDateTime = array();
        $iRes = preg_match($strPatternDate, $this->value, $tabDateTime);
        if( $iRes == 0 ) {
          $iRes = preg_match($strPatternTime, $this->value, $tabDateTime);
          if( $iRes == 0 ) {
            $iRes = preg_match($strPatternDateTime, $this->value, $tabDateTime);
            if( $iRes == 0 ) {
              if( $this->bTodayIfNull )
                $this->value = mktime(date("H"), date("i"), 0, date("m"), date("d"), date("Y"));
              else
                $this->value = 0;
            } else {
              $this->value = mktime($tabDateTime[4], $tabDateTime[5], 0, $tabDateTime[2], $tabDateTime[1], $tabDateTime[3]);
            }
          } else {
            $this->value = mktime($tabDateTime[4], $tabDateTime[5], 0, date("m"), date("d"), date("Y"));
          }
        } else {
          $this->value = mktime(0, 0, 0, $tabDateTime[2], $tabDateTime[1], $tabDateTime[3]);
        }
      }
    }

    $tabRes = array("", "", "");
    if( $this->value > 0 ) {
      switch( $this->typeDate ) {
      case ALK_FORM_TYPEDATE_DATE:
        $tabRes[0] = date("d/m/Y", $this->value);
        $tabRes[1] = date("d/m/Y", $this->value);
        break;
      case ALK_FORM_TYPEDATE_TIME:
        $tabRes[0] = date("H:i", $this->value);
        $tabRes[2] = date("H:i", $this->value);
        break;
      case ALK_FORM_TYPEDATE_DATETIME:
        $tabRes[0] = date("d/m/Y H:i", $this->value);
        $tabRes[1] = date("d/m/Y", $this->value);
        $tabRes[2] = date("H:i", $this->value);
        break;
      }
    }
    return $tabRes;
  }

  /**
   * @brief Retourne le code html du controle de saisie date
   *
   * @return Retourne un string
   */
  function GetCtrlHtml()
  {
    $strHtml = "";
    $strReadOnly = "";
    $strDisabled = "";
    if( $this->bReadOnly == true ) $strReadOnly = " readonly ";
    if( $this->bDisabled == true ) $strDisabled = " disabled ";

    $tabDateTime = $this->GetDateFormat();

    if( $this->iMode == "1" ) {
      $strHtml = "<span class=\"".$this->cssCtrl."\">".$tabDateTime[0]."</span>";
    } else {
      $MaxLength = ( $this->typeDate == ALK_FORM_TYPEDATE_DATE 
                     ? "10"
                     : ( $this->typeDate == ALK_FORM_TYPEDATE_TIME
                         ? "5"
                         : "16" ));
      // input type=date
      $strHtml .= "<input ".
        "type=\"text\" ".
        "class=\"".$this->cssCtrl."\" ".
        "style=\"width:".($MaxLength*7)."\" ".
        "name=\"".$this->name."\" ".
        "value=\"".$tabDateTime[0]."\" ".
        $strReadOnly.
        $strDisabled.
        " size=\"".$MaxLength."\"".
        " maxlength=\"".$MaxLength."\"".
        $this->GetHtmlEvent().
        ">";
    }
    
    return $strHtml;    
  }
}
?>