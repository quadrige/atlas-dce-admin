<?php
include_once("htmlpanel.class.php");

/**
 * @brief Classe affichant un panel contenant un titre suivi d'une liste 
 *        de liens permettant de filtrer une liste
 */
class HtmlPanelFilterLink extends HtmlPanel
{
  /** titre de la liste */
  var $strTitle;

  /** identifiant de l'�l�ment s�lectionn� */
  var $idSelect;

  /** tableau contenant les �l�ments � afficher */
  var $tabItems;

  /** dataSet contenant les l'id et le texte � afficher */
  var $oDsItem;

  /** Nom du champ permettant de r�cup�rer l'id de l'�l�ment dans le dataset */
  var $strFieldId;
  
  /** Nom du champ permettant de r�cup�rer le texte de l'�l�ment dans le dataset */
  var $strFieldTxt;

  /** url sur le lien de l'item auquel il faudra ajouter l'id au moment de l'affichage */
  var $strUrlBaseItem;

  /** css sur le titre */
  var $cssTitle;

  /** css sur le lien d'un �l�ment  et d'un �l�ment s�lectionn� */
  var $cssItem;
  var $cssItemSelect;

  /** css sur un les separateurs */
  var $cssText;

  /** alignement du panel dans la page : left (par d�faut), center, right */
  var $strAlign;

  /** marge en pixel plac� apr�s le panel (=8 par d�faut) */
  var $marginBottom;

  /** texte sur le lien permettant de ne pas appliquer de filtre : vide (filtre obligatoire), tous(par d�faut) ou toutes */
  var $strAll;

	/** indice pass�e dans l'url correspondant au lien strAll */
  var $iValueAll;

  /** text affich� lorsque aucun �l�ment n'est pr�sent = Aucun par d�faut */
  var $strNothing;

  /** =true si affichage de strAll avec 1 seul �l�ment dans la liste, =false sinon (par d�faut) */
  var $bViewAll;

  /** caract�re s�parateur entre �l�ments de la liste */
  var $strSeparator;

  /**
   * @brief Constructeur par d�faut
   *
   * @param strTitle  Titre de la liste
   * @param idSelect  Identiifant de l'�l�ment s�lectionn�
   */
  function HtmlPanelFilterLink($strTitle, $idSelect)
  {
    parent::HtmlPanel("");
    
    $this->strTitle = $strTitle;
    $this->idSelect = $idSelect;

    $this->strAll   = "Tous";
    $this->iValueAll = "-1";
    $this->bViewAll = true;
    $this->strNothing = "Aucun";
    $this->strAlign = "left";
    $this->strSeparator = " | ";
    $this->marginBottom = "8";

    $this->tabItems = array();

    $this->oDsItem = null;
    $this->strFieldId = "";
    $this->strFieldTxt = "";
    $this->strUrlBaseItem = "";

    $this->cssTitle       = "formLabel";
    $this->cssItem        = "aContenuLien";
    $this->cssItemSelect  = "divContenuTexteGras";
    $this->cssTxt         = "divContenuTexte";
  }
  
  /**
   * @brief Ajoute un �l�ment � la liste
   *
   * @param idItem      Identifiant de l'�l�ment
   * @param strItem     Intitul� de l'�l�ment
   * @param strItemUrl  Url sur l'�l�ment (l'url ne doit contenir l'id, plac� &paramId= en fin)
   */
  function AddItem($idItem, $strItem, $strItemUrl)
  {
    $this->strUrlBaseItem = $strItemUrl;    
    $this->tabItems[count($this->tabItems)] = array("id" => $idItem, "txt" => $strItem);
  }

  /**
   * @brief Associe au panel un dataset, le nom des champs permettant de r�cup�rer chaque information
   *        et l'url de base permettant de cr�er l'url du lien uniquement en y ajoutant l'identifiant
   *        strUrlBase doit se terminer par &param=
   *
   * @param oDs            dataSet contenant les l'id et le texte � afficher
   * @param strFieldId     Nom du champ permettant de r�cup�rer l'id de l'�l�ment dans le dataset
   * @param strFieldTxt    Nom du champ permettant de r�cup�rer le texte de l'�l�ment dans le dataset
   * @param strUrlBaseItem url sur le lien de l'item auquel il faudra ajouter l'id au moment de l'affichage
   */
  function SetDataset(&$oDs, $strFieldId, $strFieldTxt, $strUrlBaseItem)
  {
    $this->oDsItem =& $oDs;
    $this->strFieldId = $strFieldId;
    $this->strFieldTxt = $strFieldTxt;
    $this->strUrlBaseItem = $strUrlBaseItem;    
  }

  /**
   * @brief Retourne le code html du panel
   *
   * @return Retourne un string
   */
  function GetHtml()
  {
    $strHtml = "";

    $nbItem = 0;
    $strItemUrl = $this->strUrlBaseItem;
    if( $this->oDsItem != null ) {
      // cas du dataset
      $this->oDsItem->moveFirst();
      $nbItem = $this->oDsItem->iCountTotDr;
      while( $drItem = $this->oDsItem->getRowIter() ) {
        $idItem  = $drItem->getValueName($this->strFieldId);
        $strItem = $drItem->getValueName($this->strFieldTxt);

        $strHtml .= ( $strHtml != "" ? $this->strSeparator : "" ).
          "<a class='".( $this->idSelect == $idItem ? $this->cssItemSelect : $this->cssItem )."'".
          " href=\"".$strItemUrl.$idItem."\">".$strItem."</a>";
      }
    } else {
      // cas du tableau
      $nbItem = count($this->tabItems);
      foreach($this->tabItems as $tabItem) {
        $idItem  = $tabItem["id"];
        $strItem = $tabItem["txt"];

        $strHtml .= ( $strHtml != "" ? $this->strSeparator : "" ).
          "<a class='".( $this->idSelect == $idItem ? $this->cssItemSelect : $this->cssItem )."'".
          " href=\"".$strItemUrl.$idItem."\">".$strItem."</a>";
      }
    }

    $strHtml = "<div align='".$this->strAlign."'".
      " style='margin-bottom:".$this->marginBottom."px;".
      ( $this->strAlign == "left" ? " margin-left:10px;" : "" ).
      ( $this->strAlign == "right" ? " margin-right:10px" : "" ).
      "'>".
      "<span class='".$this->cssTitle."'>".$this->strTitle.( $nbItem > 1 ? "s" : "" )." : </span>".
      ( $this->strAll != ""
        ? ( $nbItem > 1 && $this->bViewAll
            ? "<a class='".( $this->idSelect == $this->iValueAll ? $this->cssItemSelect : $this->cssItem )."'".
              " href=\"".$strItemUrl.$this->iValueAll."\">".$this->strAll."</a>".$this->strSeparator
            : "" )
        : "" ).
      "<span class='".$this->cssText."'>".( $strHtml != "" ? $strHtml : $this->strNothing )."</span>".
      "</div>";
    
    return $strHtml;
  }

}
?>