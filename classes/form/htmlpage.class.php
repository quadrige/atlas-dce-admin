<?php
include_once("htmlbase.class.php");

/**
 * @class HtmlPage
 * @brief Classe repr�sentant la page html affich�e
 */
class HtmlPage extends HtmlBase
{

  /**
   * @brief Constructeur par d�faut
   * 
   * @param name Identifiant ou non de la page
   */
  function HtmlPage($name)
  {
    parent::HtmlBase($name);
  }

  /**
   * @brief Retourne le code html de la page
   */
  function GetHtml()
  {
    return "";
  }

}
?>