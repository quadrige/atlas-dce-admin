<?php
include_once("ds.class.php");

/**
 * @class DsPgSql
 *
 * @brief DataSet bas� sur postgresql, h�rite de la classe ds
 */
class DsPgSql extends Ds
{
  /** Objet contenant les donn�es du dataset */
  var $oRes;

  /**
   * @brief Constructeur par d�faut de la classe
   *
   * @param db      r�f�rence sur la connexion en cours
   * @param strSQL  requ�te sql
   * @param idFirst indice de d�but pour la pagination (=0 par d�faut)
   * @param idLast  indice de fin pour la pagination (=-1 par d�faut)
   * @param bErr    true si gestion des erreurs, false pour passer sous silence l'erreur
   */
  function DsPgSql(&$db, $strSQL, $idFirst=0, $idLast=-1, $bErr=true)
  {
    if( $bErr == true )
      startErrorHandlerPgsql();
    else
      ob_start();
    $this->iCountDr = 0;
    $this->bEof = true;
    $this->iCurDr = -1;
    $iStart = 0;
    $iStop = -1;
    if( $idFirst!=0 || $idLast != -1 ) {
      if( $idFirst > 0 ) {
        $iStart = $idFirst;
      }
      if( $idLast !=-1 && $idLast>$iStart ) {
        if( $idLast > $iStop ) {
              $iStop = $idLast;
        }
      }
    }
    $strSqlMax = "";
    
    if( $iStart==$idFirst && $iStop==$idLast && $iStop!=-1) {
      // pagination
      $strSqlMax = "select count(*) as nb from (".$strSQL.") subQuery";
      
      $nbEltParPage = ($idLast-$idFirst)+1;
      $strSQL .= " limit ".$nbEltParPage." offset ".$idFirst;
    }
    $this->oRes = pg_query($db->conn, $strSQL);
    $this->tabDr = array();
    $this->tabType = array();
    if( $this->oRes ) {
      $this->iCountDr = pg_num_rows($this->oRes);

      if( $strSqlMax == "" || $this->iCountDr == 0 ) {
        // pas de pagination ou pas de resultat a la requete
        $this->iCountTotDr = $this->iCountDr;
      } else {
        // calcul le nombre total � partir d'une nouvelle requete
        $this->oResCount = pg_query($db->conn, $strSqlMax);
        if( $this->oResCount ) {
          if( $oDrCount = pg_fetch_object($this->oResCount) ) {
            $this->iCountTotDr = $oDrCount->nb;
            if( !is_numeric($this->iCountTotDr) ) {
              $this->iCountTotDr = 0;
            }
          }
          
        }
      }
        
      if( $this->iCountDr > 0 ) {
        $this->iCurDr = 0;
        $this->bEof = false;
        for($i=0; $i<$this->iCountDr; $i++) {
          $tabDrTmp = pg_fetch_array($this->oRes);
          if( is_bool($tabDrTmp) ) {
            if( $bErr == true ) {
              trigger_error("DsPgSql - pg_fetch_array error (PgSql : ".pg_last_error($db->conn).")", E_USER_ERROR);
            } else {
              $this->_SetError("DsPgSql - pg_fetch_array error (PgSql : ".pg_last_error($db->conn).")");
            }
          } else {
            $this->tabDr[$i] = $tabDrTmp;
          }
        }
      }
    } else {
      if( $bErr == true ) {
        trigger_error("dsPgsql - Erreur requ�te (".$strSQL.", PgSql : ".pg_last_error($db->conn).")", E_USER_ERROR);
      } else {
        $this->_SetError("dsPgsql - Erreur requ�te (".$strSQL.", PgSql : ".pg_last_error($db->conn).")");
      }
    }
     
    if( $this->iCountDr > 0 ) {
      $this->iCurDr = 0;
      $this->bEof = false;
    } else {
      $this->tabDr = array();
      $this->iCountDr = 0;
      $this->iCountTotDr = 0;
    }

    if( $bErr == true )
      endErrorHandlerPgsql();
    else
      ob_clean();
  }

  /**
   * @brief Retourne le datarow (dr) correspondant � l'indice courant ($iCurDr)
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRow()
  {
    $drRes = false;
    if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drPgsqltmp = $this->tabDr[$this->iCurDr];
      $drRes = new DrPgSql($drPgsqltmp);
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }

    return $drRes;
  }

  /**
   * @brief Retourne le datarow (dr) correspondant � l'indice courant ($iCurDr) puis passe au suivant
   *        Retourne false si EOF est atteint
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRowIter()
  {
    $drRes = false;
    if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drPgsqltmp = $this->tabDr[$this->iCurDr];
      $this->iCurDr++;
      $drRes = new DrPgSql($drPgsqltmp);
    }

    return $drRes;
  }

  /**
   * @brief Place le pointeur du dataset sur le premier datarow
   *        Met � jour l'indice courant du dataset
   */
  function MoveFirst() 
  {
    if( $this->iCountDr > 0 ) {
      $this->iCurDr = 0;
      $this->bEof = false;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   * @brief Place le pointeur du dataset sur le datarow pr�c�dent
   *        Met � jour l'indice courant du dataset
   */
  function MovePrev() 
  {
    if( $this->iCountDr>0 && $this->iCurDr>0 ) {
      $this->iCurDr--;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   * @brief Place le pointeur du dataset sur le datarow suivant
   *        Met � jour l'indice courant du dataset
   */
  function MoveNext() 
  {
    if( $this->iCountDr>0 && $this->iCurDr<$this->iCountDr ) {
      $this->iCurDr++;
      
      if( $this->iCurDr >= $this->iCountDr ) {
        $this->bEof = true;
      }
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   * @brief Place le pointeur du dataset sur le dernier datarow
   *        Met � jour l'indice courant du dataset
   */
  function MoveLast() 
  {
    if ( $this->iCountDr > 0 ) {
      $this->iCurDr = $this->iCountDr-1;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   * @brief Place le pointeur du dataset sur le (iCurDr+iNb) i�me datarow
   *        Met � jour l'indice courant du dataset
   *
   * @param iNb D�calage positive ou n�gatif par rapport au dataRow courant
   */
  function Move($iNb) 
  {
    if( $this->iCountDr > 0 && $this->iCurDr+$iNb>=0 && $this->iCurDr+$iNb<$this->iCountDr ) {
      $this->iCurDr += $iNb;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   * @brief D�truit l'objet dataset()
   */
  function Close() 
  {
    $this->oRes = "";
  }
    
  /**
   * @brief Tri le dataSet apr�s lecture dans la source donn�e
   *        Effectue un tri � plusieurs niveaux
   *        N�cessite de connaite les noms de champ : ID et ID_PERE
   *        le dataset doit �tre d�j� tri� par niv puis par rang
   *
   * @param strFieldId  Nom du champ ID dans la reque�te
   * @param strFieldIdp Nom du champ ID_PERE dans la requ�te
   */
  function setTree($strFieldId, $strFieldIdp)
  {
    $tabNoeud = array();   // contient l'ensemble des noeuds
    $tabIndex = array();   // contient l'index inverse id -> cpt
    $stack = array();
    
    for ($i=0; $i< $this->iCountTotDr; $i++) {
      //$drPgsqltmp = pgsql_fetch_array($this->oRes);
      $drPgsqltmp = $this->tabDr[$i];
      
      $idFils = $drPgsqltmp[strtolower($strFieldId)];
      $idPere = $drPgsqltmp[strtolower($strFieldIdp)];
      $tabNoeud[$i]["FILS"] = array();
      $tabNoeud[$i]["ID"] = $idFils;
      $tabNoeud[$i]["OK"] = false;

      // memorize the first level id in a stack
      array_push($stack, $i);

      // met a jour la table index secondaire
      $tabIndex[$idFils] = $i;

      // ajoute l'id fils a l'id pere
      if( $idPere > 0 )
        if( array_key_exists($idPere, $tabIndex) == true ) {
          $iPere = $tabIndex[$idPere];
          array_push($tabNoeud[$iPere]["FILS"], $i);
        }
    }

    // parse the sons
    $tabDr2 = array();
    $j = 0;
    while( count($stack) > 0 ) {
      $i = array_shift($stack);
      if( $tabNoeud[$i]["OK"] == false ) {
        $id = $tabNoeud[$i]["ID"];
        $tabNoeud[$i]["OK"] = true;
        $tabDr2[$j] = $this->tabDr[$i];
        $j++;
        
        $nbFils = count($tabNoeud[$i]["FILS"]);
        for ($k=$nbFils-1; $k>=0; $k--) {
          $if = $tabNoeud[$i]["FILS"][$k];
          array_unshift($stack, $if);
        }
      }
      $i++;
    }

    $tabNoeud = null;
    $tabIndex = null;
    $this->tabDr = $tabDr2;
  }
  
}
?>