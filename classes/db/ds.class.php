<?php

/**
 * @class ds
 * @brief Classe abstraite de base repr�sentant un dataSet
 */
class Ds
{
	/** Indice du datarow(dr) courant */
  var $iCurDr;

	/** Nombre de datarow (dr) dans le dataset */
  var $iCountDr;

	/** Nombre total de datarow(dr) dans le dataset pagin� */
  var $iCountTotDr;

	/** Bool�en indiquant si l'on se trouve � la fin du dataset */
  var $bEof;

	/** Objet contenant les donn�es du dataset */
  var $tabDr;

	/** Message de la derni�re erreur rencontr�e */
	var $lastErr;

  /** tableau des types de champ */
  var $tabType;

  /**
   * @brief Constructeur par d�faut de la classe
   *
   * @param db      r�f�rence sur la connexion en cours
   * @param strSQL  requ�te sql
   * @param idFirst indice de d�but pour la pagination (=0 par d�faut)
   * @param idLast  indice de fin pour la pagination (=-1 par d�faut)
   * @param bErr    true si gestion des erreurs, false pour passer sous silence l'erreur
   */
  function Ds(&$db, $strSQL, $idFirst=0, $idLast=-1, $bErr=true)
  {		
    $this->iCurDr = -1;
    $this->iCountDr = 0;
    $this->iCountTotDr = 0;
    $this->bEof = true;
    $this->tabDr = array();
    $this->lastErr = "";
    $this->tabType = array();
  }

	/**
   * @brief Fixe la nouvelle erreur rencontr�e
   *
   * @param err false ou tableau associatif contenant l'erreur
   */
	function _SetError($err)
  {
    $this->lastErr = $err;
  }

  /**
   * @ @brief Retourne la derni�re erreur rencontr�e
   *
   * @return Retourne une chaine, vide si pas d'erreur, texte sinon
   */
	function GetLastError()
  {
    if( !is_string($this->lastErr) ) 
      $this->lastErr = "";
    return $this->lastErr;
  }

  /**
   * @brief Retourne le datarow (dr) correspondant � l'indice courant ($iCurDr)
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRow()	{	}

  /**
   * @brief Retourne le datarow (dr) correspondant � l'indice courant ($iCurDr) puis passe au suivant
   *        Retourne false si EOF est atteint
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRowIter( ) { }

  /**
   * @brief Retourne le datarow (dr) positionn� � l'indice id
   *        Retourne false si l'indice n'est pas satisfaisant
   *         n'agit ni sur bEOF, ni sur le pointeur 
   *
   * @param id Indice du dataRow
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRowAt($id) { }

  /**
   * @brief Place le pointeur du dataset sur le premier datarow
   *        Met � jour l'indice courant du dataset
   */
  function MoveFirst() { }

  /**
   * @brief Place le pointeur du dataset sur le datarow pr�c�dent
   *        Met � jour l'indice courant du dataset
   */
	function MovePrev()	{	}

  /**
   * @brief Place le pointeur du dataset sur le datarow suivant
   *        Met � jour l'indice courant du dataset
   */
  function MoveNext() { }

  /**
   * @brief Place le pointeur du dataset sur le dernier datarow
   *        Met � jour l'indice courant du dataset
   */
  function MoveLast( ) { }

  /**
   * @brief Place le pointeur du dataset sur le (iCurDr+iNb) i�me datarow
   *        Met � jour l'indice courant du dataset
   *
   * @param iNb D�calage positive ou n�gatif par rapport au dataRow courant
   */
  function Move($iNb) {	}

  /**
   * @brief D�truit l'objet dataset()
   */
  function Close() { }

  /**
   * @brief Tri le dataSet apr�s lecture dans la source donn�e
   *        Effectue un tri � plusieurs niveaux
   *        N�cessite de connaite les noms de champ : ID et ID_PERE
   *        le dataset doit �tre d�j� tri� par niv puis par rang
   *
   * @param strFieldId  Nom du champ ID dans la reque�te
   * @param strFieldIdp Nom du champ ID_PERE dans la requ�te
   */
  function setTree($strFieldId, $strFieldIdp) { }

  /**
   * @brief Retourne le type du num�ro du champ
   *        Retourne 0 si chaine, 1 si nombre, 2 si date
   *
   * @return Retourne un nombre
   */
  function GetFieldType($iFieldPosition)
  {
    if( $iFieldPosition >= 0 && $iFieldPosition < count($this->tabType) )
      return $this->tabType[$iFieldPosition];
    return "";
  }
}
?>
