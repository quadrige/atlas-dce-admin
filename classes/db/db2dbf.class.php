<?php

/**
* @file db2dbf.class.php.
* @author S.G.
* @date 26/08/2004.
* @class db2dbf.
* @brief Conversion d'une table  en un fichier dbf
* 
* Classe qui converti enti�rement une table en un fichier dbf.
*/

include_once("dbf.class.php");

class db2dbf extends Dbf {

  //Variables priv�es � la classe
  var $_tabFields = array();
  var $_dbConn = null;
  var $_strSQL = "";
  var $_strDbfFile = "";
  
  /**
  * @brief constructeur de la classe dbfConverter.
  *
  * initialisation de l'objet dbfConverter.
  *
  * @param dbConn : Objet de type DB de connection � la base de donn�es.
  * @param strFileName : chemin complet du fichier dbf � convertir (case sensitive)
  * @public
  */
  function db2dbf() {
    parent::Dbf();
  }
  
  /**
  * @brief Modificateur du tableau contenant les champs.
  *
  * @param tabFields : Tableau contenant les champs.
  * @public
  */
  function setTabFields($tabFields) {
    $this->_tabFields = $tabFields;
  }  

  /**
  * @brief Modificateur de l'objet dataset contenant les donn�es.
  *
  * @param dsRows : Dataset contenant les donn�es.
  * @public
  */
  function setDbConn($dbConn) {
    $this->_dbConn =& $dbConn;
  }  

  /**
  * @brief Modificateur de l'objet dataset contenant les donn�es.
  *
  * @param dsRows : Dataset contenant les donn�es.
  * @public
  */
  function setSQL($strSQL) {
    $this->_strSQL = $strSQL;
  }  

  /**
  * @brief Modificateur du nom de la table.
  *
  * Permet d'indiquer le nom de la table qui sera cr��e.
  *
  * @param strTableName : Nom de la table.
  * @public
  */
  function setDbfFile($strDbfFile) {
    $this->_strDbfFile = $strDbfFile;
  }
  
  /**
  * @brief Accesseur du nom de la table.
  *
  * Retourne le nom de la table qui sera cr��e.
  * @public
  */
  function getDbfFile() {
    return $this->_strDbfFile;
  }

  /**
  * @brief Fonction d'ex�cution de la conversion.
  *
  * Lancement de la conversion en fonction des param�tres sp�cifi�s pr�c�demment.
  * @public
  */
  function doConversion() {
    $tabFields = $this->_getDescFields();
    $this->createTable($this->_strDbfFile,$tabFields);
    $this->_addDatasFromSQL();
  }

  function _error($strMessage) {
    echo $strMessage;
  }
}
?>