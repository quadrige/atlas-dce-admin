<?php
include_once("dr.class.php");

/**
 * @class DrPgSql
 *
 * @brief Classe qui h�rite de la class dr (datarow)
 */
class DrPgSql extends Dr
{

  /**
   * @brief Constructeur par d�faut
   *
   * @param tabRow Tableau associatif d'un enregistrement
   */
  function DrPgSql(&$oRow)
  {
    if( isset($oRow) && is_array($oRow) ) {
      $this->oRow = $oRow;
      
      //Le nombre de champ est le nombre de colonnes du tableau divis� par 2
      //car chaque champ pred 2 colonnes : 1 colonne pour son indice, et une colonne
      //pour sa cl� ( on a donc l'information en double).
      $this->iCountField = count($oRow)/2;
    } else {
      $this->oRow = -1;
      $this->iCountField = 0;
    }
  }

  /**
   * @brief  Retourne la valeur du champs pass� en param�tre
   *         Retourne une chaine vide si le champ n'existe pas
   *
   * @param strField  nom du champs
   * @param bToUpper  vrai=force strField en majuscule (par d�faut), faux=ne modifie pas strField
   * @return Retourne une chaine
   */
  function getValueName($strField, $bToUpper=true)
  {
    $strField = strtolower($strField);
    $strRes = "";
    if( is_array($this->oRow) && array_key_exists($strField, $this->oRow) ) {
      $strRes = $this->oRow[$strField];
    }
    return $strRes;
  }

  /**
   * @brief Retourne la valeur du champs identifiant par son num�ro d'ordre dans la requete
   *
   * @param iNumField num�ro du champs (indic� � 0 � n-1)
   * @return Retourne une chaine : valeur du champs
   */
  function getValueNum($iNumField)
  {
    $strRes = "";
    if( is_array($this->oRow) ) {
      if( is_int($iNumField) && array_key_exists($iNumField, $this->oRow) )
        $strRes =  $this->oRow[$iNumField];
    }
    return $strRes;
  }
  
  /**
   * @brief Retourne dans un tableau la liste des champs
   *
   * @return Retoune un array
   */
  function getFields()
  {
    $tabRes = array();
    $indice=0 ;
    foreach ( $this->oRow as $key => $value) {
      if ( !is_int($key) ) {
        $tabRes[$indice] = $key;
        $indice++ ;
      }
    }
    return $tabRes;
  }

  /**
   * @brief R�initialise l'objet
   */
  function close( )
  {
    $this->oRow = array();
    $this->iCountField = 0;
  }
}
?>