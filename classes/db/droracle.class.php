<?php
include_once("dr.class.php");

/**
 * @class drOracle
 *
 * @brief Classe qui h�rite de la class dr (datarow)
 */
class DrOracle extends Dr
{
  /**
   * @brief Constructeur par d�faut
   *
   * @param oRow     Objet contenant l'enregistrement provenant du dataset
   * @param dsParent R�f�rence vers le dataset associ�
   */
  function DrOracle($oRow, &$dsParent)
  {
  	$this->dsParent  = &$dsParent ;
    if( isset($oRow) && is_array($oRow) ) {
      $this->oRow = $oRow;
      $this->iCountField = count($this->oRow);
    } else {
      $this->oRow = array();
      $this->iCountField = 0;
    }
  }

  /**
   * @brief  Retourne la valeur du champs pass� en param�tre
   *         Retourne une chaine vide si le champ n'existe pas
   *
   * @param strField  nom du champs
   * @param bToUpper  vrai=force strField en majuscule (par d�faut), faux=ne modifie pas strField
   * @return Retourne une chaine
   */
  function GetValueName($strField, $bToUpper=true)
  {
    $strRes = "";
    if( $bToUpper == true ) 
      $strField = strtoupper($strField);
    if( is_array($this->oRow) && array_key_exists($strField, $this->oRow) ) {
      $strRes = $this->oRow[$strField];

      if( is_object($strRes) && method_exists($strRes, "load") ) {
        $strRes = $strRes->load();
      }
    }
    
    $tabDr = $this->dsParent->tabDr[0];
    $tabType = $this->dsParent->tabType;
    $tabCle = array_keys($tabDr);
    $val = array_search($strField, $tabCle);
    
    if( $tabType[$val] == '1' ) {
	    $strRes = str_replace(",", ".", $strRes);
	    $strRes = preg_replace("/^\./", "0.", $strRes);
	    return $strRes;
    } else {
    	return $strRes;
    }
  }

  /**
   * @brief Retourne la valeur du champs identifiant par son num�ro d'ordre dans la requete
   *
   * @param iNumField num�ro du champs (indic� � 0 � n-1)
   * @return Retourne une chaine : valeur du champs
   */
  function GetValueNum($iNumField)
  {
    $strRes = "";
    if( is_array($this->oRow) ) {
      $tabTmp = array_values($this->oRow);
      if( is_int($iNumField) && $iNumField>=0 && $iNumField<count($tabTmp) ) {
        $strRes = $tabTmp[$iNumField];

        if( is_object($strRes) && method_exists($strRes, "load") ) {
          $strRes = $strRes->load();
        }
      }
    }

    $tabType = $this->dsParent->tabType;
    
    if( $tabType[$iNumField] == '1' ) {
	    $strRes = str_replace(",", ".", $strRes);
	    $strRes = preg_replace("/^\./", "0.", $strRes);
	    return $strRes;
    } else {
    	return $strRes;
    }
  }
	
  /**
   * @brief Retourne dans un tableau la liste des champs
   *
   * @return Retoune un array
   */
	function GetFields()
  {
    $tabRes = array();
    if( is_array($this->oRow) && count($this->oRow) > 0 ) {
      $tabRes = array_keys($this->oRow);
    }
    return $tabRes;
  }

  /**
   * @brief R�initialise l'objet
   */
  function Close( )
  {
    $this->oRow = array();
    $this->iCountField = 0;
  }
}
?>