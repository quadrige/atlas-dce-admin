<?php

/**
 * @class dr
 *
 * @brief Classe abstraite repr�sentant un dataRow
 */
class Dr
{
	/** Objet contenant les donn�es du datarow */
	var $oRow;
	
  /** Nombre de champs dans le datarow courant */
	var $iCountField;
	
	/** R�f�rence vers le dataSet associ� */
	var $dsParent;
	
  /**
   * @brief Constructeur par d�faut
   *
   * @param oRow     Objet contenant l'enregistrement provenant du dataset
   * @param dsParent R�f�rence vers le dataset associ�
   */
  function Dr($oRow, &$dsParent)
	{
		 $this->dsParent  = &$dsParent;		
	}

  /**
   * @brief  Retourne la valeur du champs pass� en param�tre
   *         Retourne une chaine vide si le champ n'existe pas
   *
   * @param strField  nom du champs
   * @param bToUpper  vrai=force strField en majuscule (par d�faut), faux=ne modifie pas strField
   * @return Retourne une chaine
   */
  function GetValueName($strField, $bToUpper=true) { }

  /**
   * @brief Retourne la valeur du champs identifiant par son num�ro d'ordre dans la requete
   *
   * @param iNumField num�ro du champs (indic� � 0 � n-1)
   * @return Retourne une chaine : valeur du champs
   */
  function GetValueNum( $iNumField ) { }

  /**
   * @brief Retourne dans un tableau la liste des champs
   *
   * @return Retoune un array
   */
	function GetFields() { }

  /**
   * @brief R�initialise l'objet
   */
  function Close( )	{ }
}
?>
