<?php
include_once("ds.class.php");

/**
 * @class DsMysql
 *
 * @brief DataSet bas� sur Mysql, h�rite de la classe ds
 */
class DsMySql extends Ds
{
  /** Objet contenant les donn�es du dataset */
  var $oRes;

  /**
   * @brief Constructeur par d�faut de la classe
   *
   * @param db      r�f�rence sur la connexion en cours
   * @param strSQL  requ�te sql
   * @param idFirst indice de d�but pour la pagination (=0 par d�faut)
   * @param idLast  indice de fin pour la pagination (=-1 par d�faut)
   * @param bErr    true si gestion des erreurs, false pour passer sous silence l'erreur
   */
  function DsMySql(&$db, $strSQL, $idFirst=0, $idLast=-1, $bErr=true)
  {
    if( $bErr == true )
      startErrorHandlerMysql();
    else
      ob_start();

    $this->iCountDr = 0;
    $this->bEof = true;
    $this->iCurDr = -1;
    $this->tabDr = array();
    $this->tabType = array();

    // ajout du code sp�cifique mysql pour la pagination
    $strSql = $strSQL;
    if( $idLast >= $idFirst ) {
      $strLimit = " LIMIT ".$idFirst.", ".($idLast-$idFirst+1);
      $strSql = $strSQL.$strLimit;
    
    
      // Sur une requete de type select : 
      // utilisation du mot-cl� SQL_CALC_FOUND_ROWS m�morisant
      // le nombre r�el de r�sultats qui seraient retourn�s sans utilisation de limites
      $bFoundRows = false;
      $bSelect = preg_match("!^(\s*SELECT)!si", $strSql);
      if ( $bSelect ){
        $strSql = preg_replace( "!^(\s*SELECT)!si", "$1 SQL_CALC_FOUND_ROWS", $strSql);
        $bFoundRows = !( strpos($strSql, "SQL_CALC_FOUND_ROWS") === false );
      }
    
      if ( $bSelect && !$bFoundRows ) {
        $strErr = "dsMysql[".__LINE__."] - Erreur requ�te (".$strSql."," .
          " DsMysql : Impossible de calculer le nombre total de r�sultat sur votre requ�te" .
          " [Structure requ�te incorrecte ; Pas d'ajout du mot cl� SQL_CALC_FOUND_ROWS])";
        if( $bErr == true )
          trigger_error($strErr, E_USER_ERROR);
        else
          $this->_SetError($strErr);
      }
    }
    $this->oRes = mysql_query($strSql, $db->conn);
      
    if( mysql_errno() == 0 ) {
      $nbFields = mysql_num_fields($this->oRes);
      for($i=0; $i<$nbFields; $i++) {
        $this->tabType[$i] = mysql_field_type($this->oRes, $i);
      }
      $this->iCountDr = mysql_num_rows($this->oRes);
      if( mysql_errno() != 0 ) {
        if( $bErr == true )
          trigger_error("dsMysql - Erreur requ�te (".$strSQL.", MySql : ".
                        mysql_errno().", ".mysql_error().")",E_USER_ERROR);
        else
          $this->_SetError(mysql_error());
      }
      $this->iCountTotDr = $this->iCountDr;
      
      if( $this->iCountDr > 0 ) {
        $this->iCurDr = 0;
        $this->bEof = false;
        for ($i=0; $i<$this->iCountDr; $i++){
          $this->tabDr[$i] = mysql_fetch_array($this->oRes);
          if( mysql_errno() != 0 ) {
            if( $bErr == true ) {
              trigger_error("DsMySql[".__LINE__."] - mysql_fetch_array error (Indice :".
                            $this->oRes." , MySql : ".mysql_errno().", ".
                            mysql_error().")", E_USER_ERROR);
            } else {
              $this->_SetError(mysql_error());
            }
          }
        }

        if( $idLast >= $idFirst ) {
          $strSqlTot = "SELECT FOUND_ROWS()";
          $oRes = mysql_query($strSqlTot);
          if( mysql_errno() != 0 ){
            if( $bErr == true ) {
              trigger_error("dsMysql[".__LINE__."] - Erreur requ�te (".$strTotDr.", MySql : ".
                            mysql_errno().", ".mysql_error().")",E_USER_ERROR);
            } else {
              $this->_SetError(mysql_error());
            }
          } else {
            $tabTotDr = mysql_fetch_row($oRes);        
            if( mysql_errno() != 0 ){
              if( $bErr == true ) {
                trigger_error("DsMySql[".__LINE__."] - mysql_fetch_row error (Indice :".
                              $oRes." , MySql : ".mysql_errno().", ".
                              mysql_error().")",E_USER_ERROR);
              } else {
                $this->_SetError(mysql_error());
              }
            } else {
              $this->iCountTotDr = $tabTotDr[0];
            }
          }
        }
      }
    } else {
      if( $bErr == true ) {
        trigger_error("dsMysql - Erreur requ�te (".$strSQL.", MySql : ".
                      mysql_errno().", ".mysql_error().")",E_USER_ERROR);
      } else {
        $this->_SetError(mysql_error());
      }
    }

    if( $this->iCountDr > 0 ) {
      $this->iCurDr = 0;
      $this->bEof = false;
    } else {
      $this->tabDr = array();
      $this->iCountDr = 0;
      $this->iCountTotDr = 0;
    }
    
    if( $bErr == true )
      endErrorHandlerMysql();
    else
      ob_clean();
  }

  /**
   * @brief Retourne le datarow (dr) correspondant � l'indice courant ($iCurDr)
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRow()
  {
    startErrorHandlerMysql();
    
    $drRes = false;
      
    if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drMysqltmp = $this->tabDr[$this->iCurDr];
      $drRes = new DrMySql($drMysqltmp);
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
      trigger_error("getRow - Pointer Error",E_USER_ERROR);
    }
    
    endErrorHandlerMysql();
    return $drRes;
  }

  /**
   * @brief Retourne le datarow (dr) correspondant � l'indice courant ($iCurDr) puis passe au suivant
   *        Retourne false si EOF est atteint
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRowIter()
  {
    startErrorHandlerMysql();

    $drRes = false;
    if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drMysqltmp = $this->tabDr[$this->iCurDr];
      $this->iCurDr++;
      $drRes = new DrMySql($drMysqltmp);
    }

    endErrorHandlerMysql();
    return $drRes;
  }
    
  /**
   * @brief Retourne le datarow (dr) positionn� � l'indice id
   *        Retourne false si l'indice n'est pas satisfaisant
   *         n'agit ni sur bEOF, ni sur le pointeur 
   *
   * @param id Indice du dataRow
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRowAt( $id )
  {
    startErrorHandlerMysql();

    $drRes = false;      
    if( $this->iCountDr>0 && $id>=0 && $id<$this->iCountDr ){
      $drMysqltmp = $this->tabDr[$id];
      $drRes = new DrMySql($drMysqltmp);
    }
  
    endErrorHandlerMysql();
    return $drRes;
  }

  /**
   * @brief Place le pointeur du dataset sur le premier datarow
   *        Met � jour l'indice courant du dataset
   */
  function MoveFirst()
  {
    if( $this->iCountDr > 0 ) {
      $this->iCurDr = 0;
      $this->bEof = false;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   * @brief Place le pointeur du dataset sur le datarow pr�c�dent
   *        Met � jour l'indice courant du dataset
   */
  function MovePrev()
  {
    if( $this->iCountDr>0 && $this->iCurDr>0 ) {
      $this->iCurDr--;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }


  /**
   * @brief Place le pointeur du dataset sur le datarow suivant
   *        Met � jour l'indice courant du dataset
   */
  function MoveNext()
  {
    if( $this->iCountDr>0 && $this->iCurDr<$this->iCountDr ) {
      $this->iCurDr++;
      
      if( $this->iCurDr >= $this->iCountDr ) {
        $this->bEof = true;
      }
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }


  /**
   * @brief Place le pointeur du dataset sur le dernier datarow
   *        Met � jour l'indice courant du dataset
   */
  function MoveLast( )
  {
    if ( $this->iCountDr > 0 ) {
      $this->iCurDr = $this->iCountDr-1;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   * @brief Place le pointeur du dataset sur le (iCurDr+iNb) i�me datarow
   *        Met � jour l'indice courant du dataset
   *
   * @param iNb D�calage positive ou n�gatif par rapport au dataRow courant
   */
  function Move($iNb)
  {
    if( $this->iCountDr > 0 && $this->iCurDr+$iNb>=0 && $this->iCurDr+$iNb<$this->iCountDr ) {
      $this->iCurDr += $iNb;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   * @brief D�truit l'objet dataset()
   */
  function Close()
  {
    $this->oRes = "";
  }
    
  /**
   * @brief Tri le dataSet apr�s lecture dans la source donn�e
   *        Effectue un tri � plusieurs niveaux
   *        N�cessite de connaite les noms de champ : ID et ID_PERE
   *        le dataset doit �tre d�j� tri� par niv puis par rang
   *
   * @param strFieldId  Nom du champ ID dans la reque�te
   * @param strFieldIdp Nom du champ ID_PERE dans la requ�te
   */
  function setTree($strFieldId, $strFieldIdp)
  {
    $tabNoeud = array();   // contient l'ensemble des noeuds
    $tabIndex = array();   // contient l'index inverse id -> cpt
    $stack = array();
    
    for ($i=0; $i< $this->iCountTotDr; $i++) {
      //$drMysqltmp = mysql_fetch_array($this->oRes);
      $drMysqltmp = $this->tabDr[$i];
      
      $idFils = $drMysqltmp[$strFieldId];
      $idPere = $drMysqltmp[$strFieldIdp];
      $tabNoeud[$i]["FILS"] = array();
      $tabNoeud[$i]["ID"] = $idFils;
      $tabNoeud[$i]["OK"] = false;

      // memorize the first level id in a stack
      array_push($stack, $i);

      // met a jour la table index secondaire
      $tabIndex[$idFils] = $i;

      // ajoute l'id fils a l'id pere
      if( $idPere > 0 )
        if( array_key_exists($idPere, $tabIndex) == true ) {
          $iPere = $tabIndex[$idPere];
          array_push($tabNoeud[$iPere]["FILS"], $i);
        }
    }

    // parse the sons
    $tabDr2 = array();
    $j = 0;
    while( count($stack) > 0 ) {
      $i = array_shift($stack);
      if( $tabNoeud[$i]["OK"] == false ) {
        $id = $tabNoeud[$i]["ID"];
        $tabNoeud[$i]["OK"] = true;
        $tabDr2[$j] = $this->tabDr[$i];
        $j++;
        
        $nbFils = count($tabNoeud[$i]["FILS"]);
        for ($k=$nbFils-1; $k>=0; $k--) {
          $if = $tabNoeud[$i]["FILS"][$k];
          array_unshift($stack, $if);
        }
      }
      $i++;
    }

    $tabNoeud = null;
    $tabIndex = null;
    $this->tabDr = $tabDr2;
  }
}
?>