<?php

/**
* @file dbf.class.php.
* @author S.G.
* @date 13/08/2004.
* @class dbf.
* @brief Classes d'acc�s aux donn�es d'un fichier dbf.
* 
* Classe qui permet de lire un dbf (ent�tes et donn�es).
* Usage :
* $file= "your_file.dbf"; //WARNING !!! CASE SENSITIVE APPLIED !!!!!
* $dbf        = new Dbf($file);
* $num_rec    = $dbf->dbf_rec_size;
* $field_num  = $dbf->dbf_field_size;
* $arrRec     = $dbf->dbf_record;
* $arrField   = $dbf->dbf_field;
*
* for($i=0; $i<$num_rec; $i++){
*   for($j=0; $j<$field_num; $j++){
*     echo($arrRec[$i][ $arrField[$j] ].' ');
*   }
*   echo('<br>');
* }
*/

set_time_limit(300);
class Dbf {

  var $_filename        = "";       //Nom du fichier dbf source
  var $_tab_fields      = array();  //tableau contenant les champs
  var $_db              = "";       //fichier dbf ouvert
  var $_iNbRecords      = 0;        //Nombre d'enregistrements
  var $_iNbFields       = 0;        //Nombre de champs
  var $_iNumRecord      = 0;        //Enregistrement actuel

  /**
  * @brief constructeur de la classe Dbf.
  *
  * initialisation de l'objet Dbf.
  * Enregistrement de l'ent�te et des donn�es dans un tableau
  *
  * @param filename : Chemin complet d'acc�s au fichier dbf (case sensitive).
  * @public
  */
  function Dbf() {
  }
  
  /**
  * @brief Lecture du fichier DBF.
  *
  * Enregistrement de l'ent�te et des donn�es dans un tableau
  *
  * @param bReadHeader : lecture ou non de l'ent�te (vrai par d�faut)
  * @param bReadDatas  : lecture ou non des enregistrements (vrai par d�faut)
  * @public
  */
  function open($filename) {
    $this->_filename = $filename;
    if (file_exists($this->_filename)) {
      $this->db = @dbase_open($this->_filename, 0);
      if ($this->db) {
        $this->_iNbRecords = dbase_numrecords($this->db);
        $this->_iNbFields = dbase_numfields($this->db);
        
        if ($this->_iNbRecords>0) {
          $this->move_first();
        }
        $this->_read_header();
      } else {
        return(0);
      }
      return(1);
    }
    return(0);
  }
  
  function close() {
    dbase_close($this->db);
  }
  
  function _read_header() {
    $fdbf = fopen($this->_filename,'r');

    $this->_tab_fields = array();
    $buff32 = array();
    $i = 1;
    $goon = true;

    while ($goon) {
      if (!feof($fdbf)) {
        $buff32 = fread($fdbf,32);
        if ($i > 1) {
          if (substr($buff32,0,1) == chr(13)) {
            $goon = false;
          } else {
            $pos = strpos(substr($buff32,0,10),chr(0));
            $pos = ($pos == 0?10:$pos);

            $fieldname = substr($buff32,0,$pos);
            $fieldtype = substr($buff32,11,1);
            $fieldlen = ord(substr($buff32,16,1));
            $fielddec = ord(substr($buff32,17,1));

            array_push($this->_tab_fields, array("name" => $fieldname, "type" => $fieldtype, "length" => $fieldlen, "precision" => $fielddec));
          }
        }
        $i++;
      } else {
        $goon = false;
      }
    }

    fclose($fdbf);
  }
  
  function get_fields() {
    return $this->_tab_fields;
  }
  
  function move_first() {
    $this->_iNumRecord = 1;
  }
  
  function move_next() {
    $this->_iNumRecord ++;
  }
  
  function bBOF() {
    if ($this->_iNumRecord<1) {
      return true;
    } else {
      return false;
    }
  }
  
  function bEOF() {
    if ($this->_iNumRecord>$this->_iNbRecords) {
      return true;
    } else {
      return false;
    }
  }
  
  function get_record() {
    if (!($this->bBOF()) && !($this->bEOF())) {
      return dbase_get_record($this->db, $this->_iNumRecord); // get the actual record
    } else {
      return false;
    }
  }
  
  function get_recordWithNames() {
    if (!($this->bBOF()) && !($this->bEOF())) {
      return dbase_get_record_with_names($this->db, $this->_iNumRecord); // get the actual record
    } else {
      return false;
    }
  }
  
  function getRecordIter() {
    $tabRecord = $this->get_record();
    $this->move_next();
    return $tabRecord;
  }

  function getRecordWithNamesIter() {
    $tabRecord = $this->get_recordWithNames();
    $this->move_next();
    return $tabRecord;
  }
  
  function createTable($filename, $tabFields) {
    $this->_filename = $filename;
    $this->_db = dbase_create($this->_filename, $tabFields);
    if (!$this->_db) {
      echo '<strong>Erreur lors de la cr�ation du DBF!</strong>';
    }
  }
  
  function insertRow($tabRow) {
    dbase_add_record ( $this->_db, $tabRow);
  }
}
?>