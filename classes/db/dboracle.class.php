<?php

include_once 'db.class.php';

/**
 * @class DbOracle
 * @copyright Alkante
 * @brief Classe de connexion � une base Oracle 8i
 */
class DbOracle extends Db
{
  var $bAllClob;

	/**
   * @brief Constructeur par d�faut
   *
   * @param strLogin Identifiant de l'utilisateur
   * @param strPwd   Mot de passe
   * @param strAlias Alias de la base de donnees
   */
  function DbOracle($strLogin, $strPwd, $strAlias)
  {
    parent::db($strLogin, "", $strPwd, "", $strAlias, "");
    $this->bAllClob = true;
  }

  /**
   * @brief �tablit la connection avec la base de donn�es
   */
  function Connect( )
  {
    startErrorHandlerOracle();

    $this->conn = ociLogon($this->strLogin, $this->strPwd, $this->strAlias);
      
    if( !$this->conn ) {
      // ne pas divulguer les param�tres de connexion
      $err = ociError();
      $this->_SetError($err["MESSAGE"]);
      trigger_error("Impossible de se connecter � la base Oracle (connect)", E_USER_ERROR);
    } else {
      $oClobTest = OCINewDescriptor($this->conn, OCI_D_LOB);
      $this->bAllClob = ( method_exists($oClobTest, "WriteTemporary") ? false : false );
      $oClobTest = null;
    }

    endErrorHandlerOracle();
  }

  /**
   * @brief D�connection avec la base de donnees
   */
  function Disconnect( )
  {
    startErrorHandlerOracle();

    $numargs = func_num_args();
    if( $numargs>=1 ) $this->conn = func_get_arg(0);
  
    if( !ociLogOff($this->conn) )
      trigger_error("Impossible de se deconnecter de la base Oracle (disconnect)", E_USER_WARNING);
    unset($this->conn);
  
    endErrorHandlerOracle();
  }

  /**
   * @brief Retourne le dataset correspondant � la requete strSql
   *
   * @param strSql  Requete SQL
   * @param idFirst Indice de pagination : premier �l�ment
   * @param idLast  Indice de pagination : dernier �l�ment
   * @return Retourne un dataSet
   */
  function InitDataset($strSql, $idFirst=0, $idLast=-1, $bErr=true)
  {
    $ds = new DsOracle($this, $strSql, $idFirst, $idLast, $bErr);
    return $ds;
  }

  /**
   * @brief Retourne le dataset correspondant � la requete strSql
   *        Capte l'affichage des eventuelles erreurs
   *
   * @param strSql  Requete SQL
   * @param idFirst Indice de pagination : premier �l�ment
   * @param idLast  Indice de pagination : dernier �l�ment
   * @return Retourne un dataSet
   */
  function InitDatasetSilent($strSql, $idFirst=0, $idLast=-1)
  {
    $ds = new DsOracle($this, $strSql, $idFirst, $idLast, false);
    return $ds;
  }

  /**
   * @brief Ex�cute la requ�te SQL = insert, update, delete
   *
   * @param strSql  Requ�te SQL
   * @param bErr    Type de gestion d'erreur :
   *                  = false pour capter les erreurs
   *                  = true stop l'ex�cution sur erreur
   * @param tabCLob Tableau associant les champs de type clob � leur valeur
   * @return Retourne un entier : 0 si KO, 1 si OK
   */
  function ExecuteSql($strSQL, $bErr=true, $tabCLob=array())
  {
    if( $this->bUseCommit == true && $this->bResExecuteInTransaction == false ) {
      // inutile de continuer la transaction puisque la pr�c�dente � �chou�
      return false;
    }

    if( $bErr == true ) 
      startErrorHandlerOracle();
    else
      ob_start();

    // initialisation des descripteurs clob
    $tabOCLob = array();
    $nbCLob = count($tabCLob);
    $strReturningField = "";
    $strReturningDescr = "";
    reset($tabCLob);
    $i = 0;
    while( list($strField, $strValue) = each($tabCLob) ) {
      $tabOCLob[$i] = OCINewDescriptor($this->conn, OCI_D_LOB);
      $strReturningField = ( $strReturningField!="" ? "," : "" ).$strField;
      $strReturningDescr = ( $strReturningDescr!="" ? "," : "" ).":CLOB_".$strField;
      $i++;
    }

    if( $this->bAllClob==false && $i > 0 ) {
      $strSQL .= " returning ".$strReturningField." into ".$strReturningDescr;
    }

    $bRes = true;
    if( $resAct = ociparse($this->conn, $strSQL) ) {

      // r�cup�ration des pointeurs clob
      reset($tabCLob);
      $i = 0;
      while( list($strField, $strValue) = each($tabCLob) ) {
        OCIBindByName($resAct, ":CLOB_".$strField, &$tabOCLob[$i], -1, OCI_B_CLOB);
        if( $this->bAllClob == true ) 
          $tabOCLob[$i]->WriteTemporary($strValue);
        $i++;
      }

      $bNeedCommit = false;
      if( $i > 0 || $this->bUseCommit == true ) {
        $oRes = ociexecute($resAct, OCI_DEFAULT);
        if( $this->bUseCommit == false )
          $bNeedCommit = true;
      } else {
        $oRes = ociexecute($resAct, OCI_COMMIT_ON_SUCCESS);
      }
      
      if( !$oRes ) {
        $bRes = false;
        if( $bErr == true )
          trigger_error("Erreur dans la requete SQL (executeSql - ".$strSQL.")", E_USER_WARNING);
        else {
          $err = OCIError($resAct);
          $this->_SetError($err["message"]);
        }
      } else {
        reset($tabCLob);
        $i = 0;
        while( list($strField, $strValue) = each($tabCLob) ) {
          $bRes = $tabOCLob[$i]->save($strValue);
          $i++;
        }
      }

      reset($tabCLob);
      $i = 0;
      while( list($strField, $strValue) = each($tabCLob) ) {
        $bRes = $tabOCLob[$i]->free();
        $i++;
      }
      if( $bNeedCommit == true && $this->bUseCommit == false ) {
        $bRes = ocicommit($this->conn);
      }
    }  else {
      $bRes = false;
      if( $bErr == true )
        trigger_error("Erreur dans la requete SQL (executeSql - ".$strSQL.")", E_USER_WARNING);
      else {
        $err = OCIError($this->conn);
        $this->_SetError($err["message"]);
      }
    }

    if( $bErr == true ) 
      endErrorHandlerOracle();
    else
      ob_clean();
    
    $this->bResExecuteInTransaction = $this->bResExecuteInTransaction | $bRes;

    return $bRes;
  }

  /**
   * @brief initialise une transaction sur la connexion courante
   *
   */
  function InitTransaction()
  {
    $this->bUseCommit = true;
    $this->bResExecuteInTransaction = true;
  }

  /**
   * @brief Effectue un commit sur l'ensemble des requ�tes ex�cut�es
   *
   * @return Retourne un bool : true si ok, false sinon
   */
  function CommitTransaction()
  {
    $bRes = false;
    if( $this->bUseCommit == true ) {
      $bRes = ocicommit($this->conn);
      $this->bUseCommit = false;
    }
    return $bRes;
  }

  /**
   * @brief Effectue un rollback pour annuler l'ensemble des requ�tes ex�cut�es
   *
   * @return Retourne un bool : true si ok, false sinon
   */
  function RollBackTransaction()
  {
    $bRes = false;
    if( $this->bUseCommit == true ) {
      $bRes = ocirollback($this->conn);
      $this->bUseCommit = false;
    }
    return $bRes;
  }

  /**
   * @brief Remplace les caract�res sp�ciaux d'un champ texte d'une requete SQL
   *
   * @param strString Valeur du champ texte d'une requete
   * @return Retourne une chaine obtenue apr�s traitement
   */
  function AnalyseSql($strString)
  {
    startErrorHandlerOracle();
      
    $strTmp = stripslashes($strString);
    $varRetour = preg_replace("/'/", "''", $strTmp);
    
    endErrorHandlerOracle();
    return $varRetour;
  }

  /**
   * @brief Retourne le code sql �quivalent pour un clob
   * 
   * @param strField  nom du champ clob
   * @param strValue  valeur du champ clob
   * @param bInsert   booleen : true pour une requete insert, false pour une requete autre
   *                  Pris en compte si $this->bAllClob==false
   * @return Retourne un string
   */
  function getCLob($strField, $strValue) 
  {
    if( $this->bAllClob == true ) {
      return ":CLOB_".$strField;
    }
    return "empty_clob()";
  }

  /**
   * @brief Retourne une chaine de comparaison dans une requete SQL
   *
   * @param strField   Nom du champ dont la valeur est � tester
   * @param strCompare Op�rateur de comparaison
   * @param strValeur  Valeur � comparer
   * @param strCaseOk  Valeur retourn�e si comparaison vraie
   * @param strCaseNok Valeur retourn�e si comparaison fausse
   * @return Retourne une chaine : l'expression SQL associ�e � la comparaison
   */
  function CompareSql($strField, $strCompare, $strValue, $strCaseOK, $strCaseNok)
  {
    startErrorHandlerOracle();
    
    if( !(is_string($strField) && is_string($strCompare) && 
          is_string($strValue) && is_string($strCaseOK) && is_string($strCaseNok)) && 
        $strCompare!="="  ) {
      trigger_error("Erreur dans la chaine (compareSql)", E_USER_WARNING);
      $varRetour = FALSE;
    } else {     
      $strTmp=" decode(".$strField.", ".$strValue.", ".$strCaseOK.", ".$strCaseNok.")";
      $varRetour = $strTmp;
    }

    endErrorHandlerOracle();
    return $varRetour;
  }
  
  /**
   * @brief Obtenir le prochain identifiant � inserer dans la table strTable
   *
   * @param strTable    Nom de la table
   * @param strField    Nom du champ id
   * @param strSequence Nom de la sequence associ�e
   * @return Retourne un entier : le prochain id
   */
  function GetNextId($strTable,  $strField, $strSequence="") 
  {
    $id = 1;
    if( strToUpper($strTable) == "SEQUENCE" || $strSequence!="" ) {
      $strSequence = ( $strSequence != ""
                       ? $strSequence
                       : $strField);

      $strSql = "select ".$strSequence.".nextval id_Next from dual";
      $ds = $this->initDataset($strSql);
      if( $dr = $ds->getRowIter() )
        $id = $dr->getValueName("ID_NEXT");
    } else {
      $strSql = "select max(".$strField.") as idMax from ".$strTable;
      $ds = $this->initDataset($strSql);
      if( $dr = $ds->getRowIter() )
        $id = $dr->getValueName("IDMAX");
      else
        $id = 0;
      $id++;
    }
    return $id;
  }
 
  /**
   * @brief Formate une date au format SQL da la base de donnees
   *
   * @param strFormat Format de la date passee en parametre
   * @param strDate   Valeur de la date �quivalente au format ou dans son expression enti�re
   * @param bToDate   Identifie l'expression � retourner : 
   *                  = true  : l'expression retourn�e par la requete est une date
   *                  = false : l'expression retourn�e par la requete est une chaine
   * @note Format : 
   *       - SS    : secondes
   *       - MI    : Minute
   *       - HH    : Heure du jour
   *       - D     : Num�ro du jour dans la semaine
   *       - DAY   : Nom du jour
   *       - DD    : Num�ro du jour dans le mois
   *       - DDD   : Num�ro du jour dans l'ann�e
   *       - IW    : Num�ro de la semaine dans l'ann�e (Norme iso)
   *       - WW    : Num�ro de la semaine dans l'ann�e
   *       - MM    : Num�ro du mois 
   *       - MONTH : Nom du mois
   *       - YYYY  : ann�e sur 4 chiffres
   *       - YY    : ann�e sur 2 chiffres
   * @param Retourne une chaine : l'expression SQL associ�e
   */
  function GetDateFormat($strFormat, $strDate, $bToDate=true) 
  {
    startErrorHandlerOracle();

    $strFunction = "to_date";
    if( $bToDate == false ) 
      $strFunction = "to_char";
      
    if( !(is_string($strFormat) && is_string($strDate)) ) {
      trigger_error("Erreur dans la chaine (getDateFormat)", E_USER_WARNING);
      $varRetour = "NULL";
    } else {
      if( $strFormat!="" && $strDate!="" && (strpos(strtolower($strDate), "null")===false) ) {
        $strTempFormat = $strFormat;
        $strFormat = str_replace("HH","HH24", $strTempFormat);

        $varRetour = $strFunction."(".$strDate.", '".$strFormat."')";
      } else {
        $varRetour = "NULL" ;
      }
    }
    endErrorHandlerOracle();
    return $varRetour ;
  }

  /**
   * @brief Retourne l'expression SQL permettant d'additionner des intervalles de temps � une date
   *
   * @param strChamp    Nom du champ ou expression sql � traiter
   * @param iNb         Nombre d'intervalles � ajouter
   * @param strInterval Type d'intervalle : Y=ann�e, M=mois, D=jour
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetDateAdd($strChamp, $iNb, $strInterval) 
  {
    $strRes = "";
    switch( $strInterval ) {
    case "Y": $strRes = "ADD_MONTHS(".$strChamp.", 12*".$iNb.")"; break;
    case "M": $strRes = "ADD_MONTHS(".$strChamp.", ".$iNb.")"; break;
    case "D": $strRes = $strChamp."+".$iNb; break;
    }
    return $strRes;
  }

  /**
   * @brief Retourne l'expression SQL qui fournit la concatenation d'un nombre ind�finit de param�tres
   *
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetConcat()
  {
    $strRes = "";
    $nbParam = func_num_args();
    if( $nbParam > 1 ) {
      for($i=0; $i<$nbParam; $i++) {
        $strParam = func_get_arg($i);
        $strRes .= $strParam."||";
      }
      if( substr($strRes, -2) == "||" )
        $strRes = substr($strRes, 0, -2);
    } else {
      $strRes = "null";
    }
    return $strRes;
  }

  /**
   * @brief Retourne l'expression SQL qui fournit la concatenation d'un nombre ind�finit de param�tres
   *
   * @param strChamp Nom du champ ou expression sql � traiter
   * @param iPos     Position de d�part (premier caract�re = 0)
   * @param iLength  Longueur de la sous-chaine (facultatif)
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetSubstring($strChamp, $iPos, $iLength=-1)
  {
    if( $iLength == -1 )
      return "substr(".$strChamp.", ".$iPos."+1)";
    return "substr(".$strChamp.", ".$iPos."+1, ".$iLength.")";
  }
  
  /**
   * @brief Retourne l'expression SQL correspondant � un substring pour des param�tres de limite de type Expression SQL
   *
   * @param strChamp    Nom du champ ou expression sql � traiter
   * @param strExprDep  Expression SQL ou valeur donnant la position de d�part
   * @param strExprArr  Expression SQL ou valeur donnant la position d'arrivee ("" ou -1 par d�faut)
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetSubstringExpr($strChamp, $strExprDep, $strExprArr="")
  {
    if( $strExprArr == "" || $strExprArr == -1 )
      return "substr(".$strChamp.", ".$strExprDep.")";
    return "substr(".$strChamp.", ".$strExprDep.", ".$strExprArr.")";
  }

  /**
   * @brief Retourne l'expression SQL qui fournit la date-heure syst�me
   *
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetDateCur()
  {
    return "SYSDATE";
  }

  /**
   * @brief Retourne l'expression SQL qui transforme en minuscules une expression
   *
   * @param strChamp Nom du champ ou expression sql � traiter
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetLowerCase($strChamp)
  {
    return "lower(".$strChamp.")";
  }

  /**
   * @brief Retourne l'expression SQL qui transforme en majuscules une expression
   *
   * @param strChamp Nom du champ ou expression sql � traiter
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetUpperCase($strChamp)
  {
    return "upper(".$strChamp.")";
  }

  /** @brief Met a jour le champ rang d'une table en fonction des parametres
   *
   * @param strTable   Nom de la table
   * @param strChamp   Nom du champ
   * @param strNewRang Indice du nouveau rang
   * @param bAdd       =true si ajout, false si suppression
   * @param strWhere   Condition supplementaire pour la selection du rang
   * @return Retourne un booleen : toujours true
   */
  function UpdateRang($strTable, $strChamp, $strNewRang, $bAdd, $strWhere)
  {
    startErrorHandlerOracle();

    $bRes = true;
    $bExist = true;
    $strSigne = "+";
    $strComp = ">=";
    if( $bAdd == false ) { $strSigne = "-"; $strComp=">"; }
    if( $strWhere == "" ) $strWhere = "1=1";
    if( $bAdd == true ) {
      $strSql = "select ".$strChamp.
        " from ".$strTable.
        " where ".$strWhere." and ".$strChamp."=".$strNewRang;
      $dsRg = $this->initDataset($strSql);
      if( $dsRg->bEof )
        $bExist = false;
    }
    if( $bExist == true ) {
      $strSql = "update ".$strTable." set ".$strChamp."=".$strChamp.$strSigne."1 where ".
        $strWhere." and ".$strChamp.$strComp.$strNewRang;
      $this->executeSql($strSql);
    }

    endErrorHandlerOracle();
    return $bRes;
  }

  /** @brief Retourne l'indice de rang suivant
   *
   * @param strTable   Nom de la table
   * @param strChamp   Nom du champ
   * @param strWhere   Condition supplementaire pour la selection du rang
   * @return Retourne un entier
   */
  function getNextRang($strTable, $strChamp, $strWhere)
  {
    $iRes = 1;
    startErrorHandlerOracle();

    if( $strWhere == "" ) $strWhere = "1=1";
      
    $strSql = "select decode(max(".$strChamp."), NULL, 0, max(".$strChamp."))+1 MAX_RG".
      " from ".$strTable." where ".$strWhere;
      
    $dsRg = $this->initDataset($strSql);
    if( $drRg = $dsRg->getRow() )
      $iRes = $drRg->getValueName("MAX_RG");

    endErrorHandlerOracle();
    return $iRes;
  }
    
  /**
   * @brief Retourne une chaine contenant la fonctionnalite Oracle de comparaison
   *        de chaine sans tenir compte des caracteres francais (accent, etc...)
   *
   * @param strChamp Nom du champ de la table
   * @param strOp    Operateur de test SQL : like, =
   * @param strVal   Chaine de comparaison qui doit etre traitee par ora_analyseSQL auparavant
   * @return Retourne la chaine apr�s traitement
   */
  function GetStrConvert2ASCII7($strChamp, $strOp, $strVal)
  {
    $strTmp = strtolower($strVal);
    $strTmp = strtr($strTmp, "���������������", "eeeeaaauuuiiooc");
    return "lower(convert(".$strChamp.", 'us7ascii','WE8ISO8859P1')) ".$strOp." ".$strTmp;
  }
}
?>