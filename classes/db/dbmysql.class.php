<?php
include_once 'db.class.php';

/**
 * @class DbMysql
 *
 * @brief Classe de connexion � une base Mysql
 */
class DbMySql extends Db
{

  /**
   * @brief Constructeur de la classe db : initialisation des attributs de l'objet db
   *
   * @param strLogin Identifiant de l'utilisateur
   * @param strHost  Adresse IP du serveur de base de donnees
   * @param strPwd   Mot de passe
   * @param strDb    Nom de la base de donnees
   * @param strAlias Alias de la base de donnees
   * @param strPort  Num�ro de port
   */
  function DbMySql($strLogin, $strHost, $strPwd, $strDb, $strAlias, $strPort)
  {
    parent::db($strLogin,  $strHost,  $strPwd,  $strDb,  $strAlias,  $strPort);
  }

  /**
   * @brief �tablit la connection avec la base de donn�es
   */
  function Connect()
  {
    startErrorHandlerMysql();

    $strConn = $this->strHost;
    if( $this->strPort !="" )
      $strConn = $this->strHost.":".$this->strPort;

    $this->conn = mysql_connect($strConn, $this->strLogin, $this->strPwd, true);

    if( !$this->conn )
      trigger_error("Impossible de se connecter au serveur MySQL (".$this->strHost.")", E_USER_ERROR);

    if( !mysql_select_db($this->strDb,$this->conn) )
      trigger_error("Impossible de selectionner la base MySQL (".$this->strDb.")", E_USER_ERROR);

    endErrorHandlerMysql();
  }
  
  /**
   * @brief D�connection avec la base de donnees
   */
  function Disconnect()
  {
    startErrorHandlerMysql();

    if( !mysql_close($this->conn) )
      trigger_error("Impossible de fermer la base MySQL (disconnect)", E_USER_WARNING);
    unset($this->conn);

    endErrorHandlerMysql();
  }

  /**
   * @brief Retourne le dataset correspondant � la requete strSql
   *        Capte l'affichage des eventuelles erreurs
   *
   * @param strSql  Requete SQL
   * @param idFirst Indice de pagination : premier �l�ment
   * @param idLast  Indice de pagination : dernier �l�ment
   * @return Retourne un dataSet
   */
  function InitDataset($strSQL, $idFirst=0, $idLast=-1, $bErr=true)
  {
    $ds = new DsMySql($this, $strSQL, $idFirst, $idLast, $bErr);
    return $ds;
  }

  /**
   * @brief Ex�cute la requ�te SQL = insert, update, delete
   *
   * @param strSql Requ�te SQL
   * @param bErr   Type de gestion d'erreur :
   *                 = false pour capter les erreurs
   *                 = true stop l'ex�cution sur erreur
   * @return Retourne un entier : 0 si KO, 1 si OK
   */
  function ExecuteSql($strSQL , $bErr=true)
  {
    if( $bErr == true ) 
      startErrorHandlerMysql();
    else
      ob_start();

    $boolret = mysql_query($strSQL, $this->conn);
    if( !$boolret ) {
      if( $bErr == true )
        trigger_error("Erreur dans la requete SQL (executeSql - ".$strSQL.")",E_USER_WARNING);
      else {
        $this->_SetError(mysql_error());
      }
    }
      
    if( $bErr == true )
      endErrorHandlerMysql();
    else
      ob_clean();
    return $boolret;
  }

  /**
   * @brief Remplace les caract�res sp�ciaux d'un champ texte d'une requete SQL
   *
   * @param strString Valeur du champ texte d'une requete
   * @return Retourne une chaine obtenue apr�s traitement
   */
  function AnalyseSql($strString)
  {
    startErrorHandlerMysql();
    
    $strTmp = stripslashes($strString);
    $varRetour = preg_replace("/'/", "\'", $strTmp);
    
    endErrorHandlerMysql();
    return $varRetour;
  }

  /**
   * @brief Retourne le code sql �quivalent pour un clob : champ de type TEXT (equivalent varchar)
   * 
   * @param strField  nom du champ clob
   * @param strValue  valeur du champ clob
   * @return Retourne un string
   */
  function getCLob($strField, $strValue) 
  { 
    return "'".$this->AnalyseSql($strValue)."'";
  }
  
  /**
   * @brief Retourne une chaine de comparaison dans une requete SQL
   *
   * @param strField   Nom du champ dont la valeur est � tester
   * @param strCompare Op�rateur de comparaison
   * @param strValeur  Valeur � comparer
   * @param strCaseOk  Valeur retourn�e si comparaison vraie
   * @param strCaseNok Valeur retourn�e si comparaison fausse
   * @return Retourne une chaine : l'expression SQL associ�e � la comparaison
   */
  function CompareSql($strField, $strCompare, $strValue, $strCaseOK, $strCaseNok)
  {
    startErrorHandlerMysql();
    
    if( !(is_string($strField) && is_string($strCompare) && 
          is_string($strValue) && is_string($strCaseOK) && is_string($strCaseNok)) ) {
      trigger_error("Erreur dans la chaine (compareSql)", E_USER_WARNING);
      $varRetour = FALSE;
    } else {
      $varRetour = " IF (".$strField." ".$strCompare." ".$strValue.", ".$strCaseOK.", ".$strCaseNok.")";
    }
    endErrorHandlerMysql();
    return $varRetour;
  }

  /**
   * @brief Obtenir le prochain identifiant � inserer dans la table strTable
   *
   * @param strTable    Nom de la table
   * @param strField    Nom du champ id
   * @param strSequence Nom de la sequence associ�e
   * @return Retourne un entier : le prochain id
   */
  function GetNextId($strTable, $strField, $strSequence="")
  {
    $id = 1;
    if( strToUpper($strTable) == "SEQUENCE" || $strSequence!="" ) {
      $strSequence = ( $strSequence != ""
                       ? $strSequence
                       : $strField);

      // s�quence Mysql 4
      $strSql = "UPDATE SEQUENCE set ".$strSequence."=LAST_INSERT_ID(".$strSequence."+1)";
      $this->executeSql($strSql);
  
      $id = mysql_insert_id($this->conn);
    } else {
      $strSQL = "select max(".$strField.") as idMax from ".$strTable;
      $ds = $this->initDataset($strSQL);
      if( $dr = $ds->getRowIter() )
        $id = $dr->getValueName("IDMAX");
      else
        $id = 0;
      $id++;
    }
    return $id;
  }

  /**
   * @brief Formate une date au format SQL da la base de donnees
   *
   * @param strFormat Format de la date passee en parametre
   * @param strDate   Valeur de la date �quivalente au format ou dans son expression enti�re
   * @param bToDate   Identifie l'expression � retourner : 
   *                  = true  : l'expression retourn�e par la requete est une date
   *                  = false : l'expression retourn�e par la requete est une chaine
   * @note Format : 
   *       - SS    : secondes
   *       - MI    : Minute
   *       - HH    : Heure du jour
   *       - D     : Num�ro du jour dans la semaine
   *       - DAY   : Nom du jour
   *       - DD    : Num�ro du jour dans le mois
   *       - DDD   : Num�ro du jour dans l'ann�e
   *       - IW    : Num�ro de la semaine dans l'ann�e (Norme iso)
   *       - WW    : Num�ro de la semaine dans l'ann�e
   *       - MM    : Num�ro du mois 
   *       - MONTH : Nom du mois
   *       - YYYY  : ann�e sur 4 chiffres
   *       - YY    : ann�e sur 2 chiffres
   * @param Retourne une chaine : l'expression SQL associ�e
   */
  function GetDateFormat($strFormat,  $strDate, $bToDate=true)
  {
    startErrorHandlerMysql();

    $strRes = "NULL";
    if( !(is_string($strFormat) && is_string($strDate)) ) {

    } else
      if( $bToDate==false ) {
        if( $strFormat!="" && ( $strDate!="" || strtolower($strDate)!="'null'") ) {
          $strTempFormat = $strFormat ;
          $strTempFormat = preg_replace("/SS/","%S",$strTempFormat) ;
          $strTempFormat = preg_replace("/MI/","%i",$strTempFormat) ;
          $strTempFormat = preg_replace("/HH/","%H",$strTempFormat) ;
          $strTempFormat = preg_replace("/DDD/","%j",$strTempFormat) ;
          $strTempFormat = preg_replace("/DD/","%d",$strTempFormat) ;
          $strTempFormat = preg_replace("/DAY/","%W",$strTempFormat) ;
          $strTempFormat = preg_replace("/D/","%w",$strTempFormat) ;
          $strTempFormat = preg_replace("/WW/","%u",$strTempFormat) ;
          $strTempFormat = preg_replace("/IW/","%U",$strTempFormat) ;
          $strTempFormat = preg_replace("/MM/","%m",$strTempFormat) ;
          $strTempFormat = preg_replace("/MONTH/","%M",$strTempFormat) ;
          $strTempFormat = preg_replace("/YYYY/","%Y",$strTempFormat) ;
          $strTempFormat = preg_replace("/YY/","%y",$strTempFormat) ;
          $strTempFormat = preg_replace("/W/","%v",$strTempFormat) ;
              
          $strRes = "DATE_FORMAT(".$strDate.",'".$strTempFormat."')" ;
        }
      } else {
        if( ( $strDate!="" || strtolower($strDate)!="'null'") ) {
          if (substr($strDate, 0, 1)=="'")
            $strDate = substr($strDate, 1, -1);
          $tabDate = explode (" ",$strDate);
          $tabDate2 = explode ("/",$tabDate[0]);
          if( count($tabDate2)!=3 ) {
            trigger_error("Erreur format date (getDateFormat)", E_USER_WARNING);
          } else {
            $strRes = "";
            for ($i=2; $i>=0; $i--) {
              if ($i>0)
                $strRes .= $tabDate2[$i]."-";
              else
                $strRes .= $tabDate2[$i];
            }
            $strRes = "'".$strRes.(count($tabDate)>1?" ".$tabDate[1]:"")."'";
          }
        }
      }
    
    endErrorHandlerMysql();
    return $strRes;
  }
    
  /**
   * @brief Retourne l'expression SQL permettant d'additionner des intervalles de temps � une date
   *
   * @param strChamp    Nom du champ ou expression sql � traiter
   * @param iNb         Nombre d'intervalles � ajouter
   * @param strInterval Type d'intervalle : Y=ann�e, M=mois, D=jour
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetDateAdd($strChamp, $iNb, $strInterval) 
  {
    $strRes = "";
    switch( $strInterval ) {
    case "Y": $strRes = "DATE_ADD(".$strChamp.", INTERVAL ".$iNb." YEAR)"; break;
    case "M": $strRes = "DATE_ADD(".$strChamp.", INTERVAL ".$iNb." MONTH)"; break;
    case "D": $strRes = "DATE_ADD(".$strChamp.", INTERVAL ".$iNb." DAY)"; break;
    case "H": $strRes = "DATE_ADD(".$strChamp.", INTERVAL ".$iNb." HOUR)"; break;
    }
    return $strRes;
  }

  /** M�thode � supprimer */
  function getDateInsert($strDate)
  {
    return GetDateFormat("DD/MM/YYYY",  $strDate);
  }

  /**
   * @brief Retourne l'expression SQL qui fournit la concatenation d'un nombre ind�finit de param�tres
   *
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetConcat()
  {
    $strRes = "";
    $nbParam = func_num_args();
    if( $nbParam>1  ) {
      $strRes = "concat(";
      for($i=0; $i<$nbParam; $i++) {
        $strParam = func_get_arg($i);
        $strRes .= $strParam.",";
      }
      if( substr($strRes, -1) == "," )
        $strRes = substr($strRes, 0, -1);
      $strRes .= ")";
    } else {
      $strRes = "null";
    }
    return $strRes;
  }
  
  /**
   * @brief Retourne l'expression SQL qui fournit la concatenation d'un nombre ind�finit de param�tres
   *
   * @param strChamp Nom du champ ou expression sql � traiter
   * @param iPos     Position de d�part (premier caract�re = 0)
   * @param iLength  Longueur de la sous-chaine (facultatif)
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetSubstring($strChamp, $iPos, $iLength=-1)
  {
    if( $iLength == -1 )
      return "substring(".$strChamp.", ".$iPos."+1)";
    return "substring(".$strChamp.", ".$iPos."+1, ".$iLength.")";
  }
  
  /**
   * @brief Retourne l'expression SQL correspondant � un substring pour des param�tres de limite de type Expression SQL
   *
   * @param strChamp    Nom du champ ou expression sql � traiter
   * @param strExprDep  Expression SQL ou valeur donnant la position de d�part
   * @param strExprArr  Expression SQL ou valeur donnant la position d'arrivee ("" ou -1 par d�faut)
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetSubstringExpr($strChamp, $strExprDep, $strExprArr="")
  {
    if( $strExprArr == "" || $strExprArr == -1 )
      return "substring(".$strChamp.", ".$strExprDep.")";
    return "substring(".$strChamp.", ".$strExprDep.", ".$strExprArr.")";
  }

  /**
   * @brief Retourne l'expression SQL qui fournit la date-heure syst�me
   *
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function getDateCur( )
  {
    return "SYSDATE()";
  }

  /**
   * @brief Retourne l'expression SQL qui transforme en minuscules une expression
   *
   * @param strChamp Nom du champ ou expression sql � traiter
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetLowerCase($strChamp)
  {
    return "lcase(".$strChamp.")";
  }

  /**
   * @brief Retourne l'expression SQL qui transforme en majuscules une expression
   *
   * @param strChamp Nom du champ ou expression sql � traiter
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetUpperCase($strChamp)
  {
    return "ucase(".$strChamp.")";
  }

  /** @brief Met a jour le champ rang d'une table en fonction des parametres
   *
   * @param strTable   Nom de la table
   * @param strChamp   Nom du champ
   * @param strNewRang Indice du nouveau rang
   * @param bAdd       =true si ajout, false si suppression
   * @param strWhere   Condition supplementaire pour la selection du rang
   * @return Retourne un booleen : toujours true
   */
  function UpdateRang($strTable, $strChamp, $strNewRang, $bAdd, $strWhere)
  {
    startErrorHandlerMysql();

    $bRes = true;
    $bExist = true;
    $strSigne = "+";
    $strComp = ">=";
    if( $bAdd == false ) { $strSigne = "-"; $strComp=">"; }
    if( $strWhere == "" ) $strWhere = "1=1";
    if( $bAdd == true ) {
      $strSql = "select ".$strChamp." from ".$strTable." where ".$strWhere." and ".$strChamp."=".$strNewRang;
      $dsRg = $this->initDataset($strSql);
      if( $dsRg->bEof )
        $bExist = false;
    }
    if( $bExist == true ) {
      $strSql = "update ".$strTable." set ".$strChamp."=".$strChamp.$strSigne."1 where ".
        $strWhere." and ".$strChamp.$strComp.$strNewRang;
      $this->executeSql($strSql);
    }

    endErrorHandlerMysql();
    return $bRes;
  }

  /** @brief Retourne l'indice de rang suivant
   *
   * @param strTable   Nom de la table
   * @param strChamp   Nom du champ
   * @param strWhere   Condition supplementaire pour la selection du rang
   * @return Retourne un entier
   */
  function GetNextRang($strTable, $strChamp, $strWhere)
  {
    startErrorHandlerMysql();

    $iRes = 1;
    if( $strWhere == "" ) $strWhere = "1=1";
      
    $strSql = "select IF(max(".$strChamp.") is NULL, 1, max(".$strChamp.")+1) as MAX_RG".
      " from ".$strTable." where ".$strWhere;
      
    $dsRg = $this->initDataset($strSql);
    if( $drRg = $dsRg->getRow() )
      $iRes = $drRg->getValueName("MAX_RG");
 
    endErrorHandlerMysql();
    return $iRes;
  }
    
  /**
   * @brief Retourne une chaine contenant la fonctionnalite Oracle de comparaison
   *        de chaine sans tenir compte des caracteres francais (accent, etc...)
   *
   * @param strChamp Nom du champ de la table
   * @param strOp    Operateur de test SQL : like, =
   * @param strVal   Chaine de comparaison qui doit etre traitee par ora_analyseSQL auparavant
   * @return Retourne la chaine apr�s traitement
   */
  function GetStrConvert2ASCII7($strChamp, $strOp, $strVal)
  {
    //$strTmp = strtolower($strVal);
    //sttrTmp = strtr($strTmp, "e����a���u���i���o�c�", "eeeeeaaaauuuuiiiooocc");
    $strRes = "lcase(".$strChamp.") ".$strOp." lcase(".$strVal.")";
    return $strRes;
  }

  /**
   * Retourne la description des colonnes d'une table
   * @param strTableName    Nom de la table
   * @return dataset
   */
  function getDsTableColumns($strTableName)
  {
    $strSql = "desc " . $strTableName;
    return $this->initDataset($strSql);
  }
}
?>