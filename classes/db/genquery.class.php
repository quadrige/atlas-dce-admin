<?php
/******************************* Class genquery****************************

Classe permettant de construire des requetes action : insert, update, delete � partir de tableaux

*****************************************************************************/


class genQuery
{

  /**Attributes: */

    ///<comment><summary>tableau � 2 dimensions des champs : (0,i) Nom du champ,(1,i) Valeur du champ, (2,i) Type du champ </summary></comment>
    var $arrayField;

    ///<comment><summary>tableau � 2 dimensions des champs situ�s dans la clause where (pr update ou delete) : (0,i) Nom du champ,(1,i) Valeur du champ, (2,i) Type du champ </summary></comment>
    var $arrayFieldWhere;
    
    ///<comment><summary>Nom de la table</summary></comment>
    var $strTableName;
    
     ///<comment><summary>Connexion</summary></comment>
    var $dbConn;


    ///<comment>
	///<summary>
	/// Constructeur de l'objet genQuery : initialisation des attributs
	///</summary>
	///<params name="arrayFieldName"></params>
 	///<params name="arrayFieldWhere"></params>
 	///<params name="strTableName"></params>
  ///<params name="dbConn"></params>
	///<return></params>
	///</comment>
  function genQuery($dbConn,$strTableName,$arrayField = '',$arrayFieldWhere = '')
  {
		startErrorHandlerMysql();
		if(!(is_string($strTableName) && $strTableName <> '')){
			trigger_error("genQuery - Nom de table invalide",E_USER_ERROR);
		}

    $this->arrayField=$arrayField;
    $this->arrayFieldWhere=$arrayFieldWhere;
    $this->strTableName=$strTableName;
    $this->dbConn=$dbConn;
	 
		endErrorHandlerMysql();
 }
  
   ///<comment>
	///<summary>
	/// retourne la requete d'insertion construite
	///</summary>
	///<return>chaine de requ�te</params>
	///</comment>
  function getInsertQuery( )
  {
		startErrorHandlerMysql();
		// controle pr�-requis 
		if(!(is_array($this->arrayField[0]))){
			trigger_error("getInsertQuery - Tableau Field invalide",E_USER_ERROR);
		}

		$strNom = "";
    $strVal = "" ;

    for($i = 0;$i < count($this->arrayField);$i++){
      if ($this->arrayField[$i][0] != "")
      {
         if ($strNom != "")
         {
           $strNom = $strNom.",";
           $strVal = $strVal.",";
         }

         $strNom = $strNom.$this->arrayField[$i][0];
         if ($this->arrayField[$i][2]==1)
           $strVal = $strVal."'".$this->dbConn->analyseSQL($this->arrayField[$i][1]). "'";
         else
           $strVal = $strVal.$this->arrayField[$i][1];
      }
    }
    if ($strVal != "")
       return "INSERT INTO ".$this->strTableName." (". $strNom .") VALUES (".$strVal.")";
    else
       return "";
		endErrorHandlerMysql();
  }


	///<comment>
	///<summary>
	/// retourne la requete de modification construite
	///</summary>
	///<return>chaine de requ�te</params>
	///</comment>
  function getUpdateQuery( )
  {
		startErrorHandlerMysql();
		// controle pr�-requis 
		if( !( is_array($this->arrayField[0]) ) ){
			trigger_error("getUpdateQuery - Tableau Field invalide",E_USER_ERROR);
		}
		else {
			if( !(is_array($this->arrayField[0]) ) || ($this->arrayField == '') ){
				trigger_error("getUpdateQuery - Tableau Where invalide",E_USER_ERROR);
			}
		}

    $strVal="";
    $strWhere="";

    for($i = 0;$i < count($this->arrayField);$i++){
      if ($this->arrayField[$i][0] != "")
      {
         if ($strVal != "")
            $strVal = $strVal.",";

         if ($this->arrayField[$i][2]==1)
            $strVal = $strVal.$this->arrayField[$i][0]."='".$this->dbConn->analyseSQL($this->arrayField[$i][1]). "'";
         else
            $strVal = $strVal.$this->arrayField[$i][0]."=".$this->arrayField[$i][1];
      }
    }
    for($i = 0;$i < count($this->arrayFieldWhere);$i++){
       if ($this->arrayFieldWhere[$i][0] != "")
       {
         if ($strWhere != "")
            $strWhere = $strWhere." and ";
         else
            $strWhere = " WHERE ";

         if ($this->arrayFieldWhere[$i][2]==1)
           $strWhere = $strWhere.$this->arrayFieldWhere[$i][0]."='".$this->dbConn->analyseSQL($this->arrayFieldWhere[$i][1]). "'";
         else
           $strWhere = $strWhere.$this->arrayFieldWhere[$i][0]."=".$this->arrayFieldWhere[$i][1];
       }
     }
    
    if ($strVal != "")
       return "UPDATE ".$this->strTableName." SET ". $strVal.$strWhere;
    else
       return "";
  }

    ///<comment>
	///<summary>
	/// retourne la requete de destruction construite
	///</summary>
	///<return>chaine de requ�te</params>
	///</comment>
  function getDeleteQuery( )
  {
		//if(!((is_array($this->arrayFieldWhere[0]) && is_array($this->arrayFieldWhere[1] && is_array($this->arrayFieldWhere[2])) || ($this->arrayFieldWhere == '')))){
		if( !(is_array($this->arrayFieldWhere[0]) ) || ($this->arrayFieldWhere == '') ){
			trigger_error("getDeleteQuery - Tableau Where invalide",E_USER_ERROR);
		}

     $strWhere = "";
     for($i = 0;$i < count($this->arrayFieldWhere);$i++){
       if ($this->arrayFieldWhere[$i][0] != "")
       {
         if ($strWhere != "")
            $strWhere = $strWhere." and ";
         else
            $strWhere = " WHERE ";
            
         if ($this->arrayFieldWhere[$i][2]==1)
           $strWhere = $strWhere.$this->arrayFieldWhere[$i][0]."='".$this->dbConn->analyseSQL($this->arrayFieldWhere[$i][1]). "'";
         else
           $strWhere = $strWhere.$this->arrayFieldWhere[$i][0]."=".$this->arrayFieldWhere[$i][1];
       }
     }
     return "DELETE FROM ".$this->strTableName.$strWhere;
  }
}
?>
