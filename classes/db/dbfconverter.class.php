<?php

/**
* @file dbfConverterMySql.class.php.
* @author S.G.
* @date 10/08/2004.
* @class dbfConverterMySql.
* @brief Conversion d'une table dbf en table MySQL.
* 
* Classe qui converti enti�rement une table dbf en une table MySQL.
*/

include_once 'dbf.class.php';

class dbfConverter extends Dbf {

  //Variables priv�es � la classe
  var $_dbConn = null;
  var $_strTableName = "";
  var $_strMode = "";
  var $_strOutputFile = "";
  var $_bPromptSWarnings = false;
  
  /**
  * @brief Constantes de sortie de conversion.
  *
  * MODE_OUTPUT_TABLE : cr�ation directe de la table dans MySQL.
  * MODE_OUTPUT_SQL : Sortie dans un fichier SQL.
  * MODE_OUTPUT_HTML : Sortie sur la page Web.
  */
  var $MODE_OUTPUT_TABLE = "table";
  var $MODE_OUTPUT_SQL = "sql";
  var $MODE_OUTPUT_HTML = "html";

  /**
  * @brief constructeur de la classe dbfConverter.
  *
  * initialisation de l'objet dbfConverter.
  *
  * @param dbConn : Objet de type DB de connection � la base de donn�es.
  * @param strFileName : chemin complet du fichier dbf � convertir (case sensitive)
  * @public
  */
  function dbfConverter() {
    parent::Dbf();
  }
  
  /**
  * @brief Modificateur de l'objet de connection � la table.
  *
  * @param dbConn : Connection � la base de donn�es.
  * @public
  */
  function setConnection($dbConn) {
    $this->_dbConn = $dbConn;
  }  
  /**
  * @brief Modificateur du nom de la table.
  *
  * Permet d'indiquer le nom de la table qui sera cr��e.
  *
  * @param strTableName : Nom de la table.
  * @public
  */
  function setTableName($strTableName) {
    $this->_strTableName = $strTableName;
  }
  
  /**
  * @brief Accesseur du nom de la table.
  *
  * Retourne le nom de la table qui sera cr��e.
  * @public
  */
  function getTableName() {
    return $this->_strTableName;
  }
  
  /**
  * @brief Modificateur du fichier SQL de sortie.
  *
  * Permet d'indiquer le chemin complet du fichier sql qui doit �tre g�n�r�.
  * Ce fichier est utile en mode MODE_OUTPUT_SQL.
  *
  * @param strOutputFile : Chemin complet du fichier sql.
  * @public
  */
  function setOutputFile($strOutputFile) {
    $this->_strOutputFile = $strOutputFile;
  }
  
  /**
  * @brief Accesseur du fichier SQL de sortie.
  *
  * Retourne le chemin complet du fichier SQL de sortie.
  * @public
  */
  function getOutputFile() {
    return $this->_strOutputFile;
  }

  /**
  * @brief Modificateur du mode de sortie de la conversion.
  *
  * Permet d'indiquer le mode de sortie de la conversion. 3 modes sont disponibles :
  * MODE_OUTPUT_TABLE : cr�ation directe de la table dans MySQL.
  * MODE_OUTPUT_SQL : Sortie dans un fichier SQL.
  * MODE_OUTPUT_HTML : Sortie sur la page Web.
  *
  * @param strMode : Mode de sortie.
  * @public
  */
  function setMode($strMode) {
    $this->_strMode = $strMode;
  }
  
  /**
  * @brief Modificateur du mode warning.
  *
  * Permet d'indiquer si l'on veut que les warnings s'affichent lors de l'ex�cution.
  *
  * @param bPromptWarnings : Affichage des warnings ou non.
  * @public
  */
  function setWarnings($bPromptWarnings) {
    $this->_bPromptSWarnings = $bPromptWarnings;
  }

  /**
  * @brief Fonction d'ex�cution de la conversion.
  *
  * Lancement de la conversion en fonction des param�tres sp�cifi�s pr�c�demment.
  * @public
  */
  function doConversion() {
    $this->_createTable();
    $this->_insertData();
  }

  /**
  * @brief Fonction d'�criture du fichier SQL.
  *
  * @param strSql : requ�te sql � �crire dans le fichier.
  * @private
  */
  function _outputSql($strSql) {
    if (file_exists($this->strOutputFile)) {
      $fp = fopen($this->_strOutputFile,"a");
    } else {
      $fp = fopen($this->_strOutputFile,"w");
    }

    $bytes = fwrite($fp,$strSql);
    fclose($fp);
  }

  function _error($strMessage) {
    echo $strMessage;
  }
}
?>