<?php
include_once "ds.class.php";

/**
 * @class DsOracle
 *
 * @brief DataSet bas� sur Oracle, h�rite de la classe ds
 */
class DsOracle extends Ds
{
  /**
   * @brief Constructeur par d�faut de la classe
   *
   * @param db      r�f�rence sur la connexion en cours
   * @param strSQL  requ�te sql
   * @param idFirst indice de d�but pour la pagination (=0 par d�faut)
   * @param idLast  indice de fin pour la pagination (=-1 par d�faut)
   * @param bErr    true si gestion des erreurs, false pour passer sous silence l'erreur
   */
  function DsOracle( &$db, $strSql, $idFirst=0, $idLast=-1, $bErr=true )
	{
	  // liste des champs reconnu par Oracle
	  $tabType = array("VARCHAR" => 0, "NUMBER" => 1, "DATE" => 2);
	
	  $this->lastErr = "";
	
	  if( $bErr == true )
	    startErrorHandlerOracle();
	  else
	    ob_start();
	  
	  $this->iCountDr = 0;
	  $this->bEof = true;
	  $this->iCurDr = -1;
    $this->tabDr = array();
    $this->tabType = array();
	  
	  $resAct = ociparse($db->conn, $strSql);
	  $res = ociexecute($resAct);
	  if( $error = ocierror($resAct) ) {
	    if( $bErr == true ) {
	      trigger_error("dsOracle - Erreur requ�te (".$strSql.", Oracle : ".
	                    $error['code'].", ".$error['message'].")", E_USER_ERROR);
	    } else {
	      $this->_SetError($error['message']);
	    }
	  }
	
	  $nbFields = OCINumCols($resAct);
	  for($i=0; $i<$nbFields; $i++) {
	    $strType = OCIColumnType($resAct, $i+1);
	    $this->tabType[$i] = ( array_key_exists($strType, $tabType) == true ? $tabType[$strType] : 0 );
	  }
	
	  $this->iCurDr = -1;
	  $nbElt = -1;
	  if( $idLast != -1 )
	    $nbElt = $idLast-$idFirst+1;
	  $this->iCountDr = ocifetchstatement($resAct, $this->tabDr, $idFirst, $nbElt, 
	                                      OCI_ASSOC+OCI_FETCHSTATEMENT_BY_ROW);
	
	  $this->iCountTotDr = $this->iCountDr;      
	  if( $idFirst!=0 || $idLast != -1 ) {
      // pagination en cours : calcul le nombre total
      $strSql = "select count(*) nbTot from (".$strSql.") r";
      $resAct = ociparse($db->conn, $strSql);
      if( !ociexecute($resAct) ) {
	      $error = ocierror($resAct);
	      if( $bErr == true )
	        trigger_error("dsOracle - Erreur requ�te (".$strSql.", Oracle : ".
	                      $error['code'].", ".$error['message'].")", E_USER_ERROR);
	      else
	        $this->_SetError($error['message']);
	    }
      if( ociFetch($resAct) )
        $this->iCountTotDr = ociResult($resAct, "NBTOT");
    }
	
	  if( $this->iCountDr > 0 ) {
	    $this->iCurDr = 0;
	    $this->bEof = false;
	  } else {
	    $this->tabDr = array();
	    $this->iCountDr = 0;
	    $this->iCountTotDr = 0;
	  }
	
	  if( $bErr == true )
	    endErrorHandlerOracle();
	  else
	    ob_clean();
	}

  /**
   * @brief Retourne le datarow (dr) correspondant � l'indice courant ($iCurDr)
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRow( )
	{
	  startErrorHandlerOracle();
	  
	  if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
	    $drRes = new DrOracle($this->tabDr[$this->iCurDr],$this);
	  } else {
      $drRes = false;
      trigger_error("getRow - Erreur indice", E_USER_ERROR);
    }
	  
	  endErrorHandlerOracle();
	  return $drRes;
	}

   /**
   * @brief Retourne le datarow (dr) correspondant � l'indice courant ($iCurDr) puis passe au suivant
   *        Retourne false si EOF est atteint
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRowIter( )
	{
	  startErrorHandlerOracle();
	  
	  if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drRes = new DrOracle($this->tabDr[$this->iCurDr],$this);
      $this->moveNext();
	  } else {
	    $drRes = false;
	  }

	  endErrorHandlerOracle();
	  return $drRes;
	}

  /**
   * @brief Retourne le datarow (dr) positionn� � l'indice id
   *        Retourne false si l'indice n'est pas satisfaisant
   *         n'agit ni sur bEOF, ni sur le pointeur 
   *
   * @param id Indice du dataRow
   * @return Retourne un dataRow si ok, false sinon
   */
  function GetRowAt( $id )
	{
	  startErrorHandlerOracle();
	  
	  if( $this->iCountDr>0 && $id>=0 && $id<$this->iCountDr )
	    $drRes = new DrOracle($this->tabDr[$id],$this);
	  else
	    $drRes = false;
	  
	  endErrorHandlerOracle();
	  return $drRes;
	}

  /**
   * @brief Place le pointeur du dataset sur le premier datarow
   *        Met � jour l'indice courant du dataset
   */
  function MoveFirst( )
	{
	  if( $this->iCountDr>0 ) {
		  $this->iCurDr = 0;
		  $this->bEof = false;
	  } else {
      $this->iCurDr = -1;
      $this->bEof = true;          
	  }
	}

  /**
   * @brief Place le pointeur du dataset sur le datarow pr�c�dent
   *        Met � jour l'indice courant du dataset
   */
  function MovePrev( )
	{
	  if( $this->iCurDr>=0 && $this->iCountDr>0 ) {
      $this->iCurDr--;
      if( $this->iCurDr<0 )
        $this->bEof = true;
    }
	}

  /**
   * @brief Place le pointeur du dataset sur le datarow suivant
   *        Met � jour l'indice courant du dataset
   */
  function MoveNext( )
	{
	  if( $this->iCountDr>0 && $this->iCurDr<$this->iCountDr ) {
      $this->iCurDr++;
      if( $this->iCurDr >= $this->iCountDr )
        $this->bEof = true;
    }
	}

  /**
   * @brief Place le pointeur du dataset sur le dernier datarow
   *        Met � jour l'indice courant du dataset
   */
  function MoveLast( )
	{
	  if( $this->iCountDr>0 ) {
      $this->iCurDr = $this->iCountDr - 1;
      $this->bEof = false;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
	}

  /**
   * @brief Place le pointeur du dataset sur le (iCurDr+iNb) i�me datarow
   *        Met � jour l'indice courant du dataset
   *
   * @param iNb D�calage positive ou n�gatif par rapport au dataRow courant
   */
  function Move( $nb )
	{
	  if( $this->iCountDr>0 && $this->iCurDr+$iNb>=0 && $this->iCurDr+$iNb<$this->iCountDr ) {
	    $this->iCurDr += $iNb;
		} else {
	    $this->bEof = true;
	    $this->iCurDr = -1;
	  }
	}

  /**
   * @brief D�truit l'objet dataset()
   */
  function Close()
  {
	  $this->tabDr = array();
	  $this->iCurDr = -1;
	  $this->iCountDr = -1;
	  $this->iCountTotDr = -1;
	  $this->bEof = true;
	}

  /**
   * @brief Tri le dataSet apr�s lecture dans la source donn�e
   *        Effectue un tri � plusieurs niveaux
   *        N�cessite de connaite les noms de champ : ID et ID_PERE
   *        le dataset doit �tre d�j� tri� par niv puis par rang
   *
   * @param strFieldId  Nom du champ ID dans la reque�te
   * @param strFieldIdp Nom du champ ID_PERE dans la requ�te
   */
  function SetTree($strFielId, $strFieldIdp)
  {
    $tabNoeud = array();   // contient l'ensemble des noeuds
    $tabIndex = array();   // contient l'index inverse id -> cpt
    $stack = array();
    
    for ($i=0; $i< $this->iCountTotDr; $i++) {
      $drMysqltmp = $this->tabDr[$i];
      
      $idFils = $drMysqltmp[$strFielId];
      $idPere = $drMysqltmp[$strFieldIdp];
      $tabNoeud[$i]["FILS"] = array();
      $tabNoeud[$i]["ID"] = $idFils;
      $tabNoeud[$i]["OK"] = false;

      // memorise le premier niveau
      array_push($stack, $i);

      // met a jour la table index secondaire
      $tabIndex[$idFils] = $i;

      // ajoute l'id fils a l'id pere
      if( $idPere > 0 )
        if( array_key_exists($idPere, $tabIndex) == true ) {
          $iPere = $tabIndex[$idPere];
          array_push($tabNoeud[$iPere]["FILS"], $i);
        }
    }

    $tabDr2 = array();
    $j = 0;
    while( count($stack) > 0 ) {
      $i = array_shift($stack);
      if( $tabNoeud[$i]["OK"] == false ) {
        $id = $tabNoeud[$i]["ID"];
        $tabNoeud[$i]["OK"] = true;
        $tabDr2[$j] = $this->tabDr[$i];
        $j++;
        
        $nbFils = count($tabNoeud[$i]["FILS"]);
        for ($k=$nbFils-1; $k>=0; $k--) {
          $if = $tabNoeud[$i]["FILS"][$k];
          array_unshift($stack, $if);
        }
      }
      $i++;
    }

    $tabNoeud = null;
    $tabIndex = null;
    $this->tabDr = $tabDr2;
  }
}


?>