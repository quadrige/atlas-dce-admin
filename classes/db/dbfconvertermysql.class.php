<?php
/**
* @file dbfConverterMySql.class.php.
* @author S.G.
* @date 10/08/2004.
* @class dbfConverterMySql.
* @brief Conversion d'une table dbf en table MySQL.
* 
* Classe qui converti enti�rement une table dbf en une table MySQL.
*/

include_once 'dbfconverter.class.php';

class dbfConverterMySql extends dbfConverter {

  /**
  * @brief constructeur de la classe dbfConverterMySql.
  *
  * initialisation de l'objet dbfConverterMySql.
  * @param dbConn : Objet de type DBMYSQL de connection � la base de donn�es MySql.
  * @param strFileName : chemin complet du fichier dbf � convertir (case sensitive)
  */
  function dbfConverterMySql() {
    parent::dbfConverter();
  }
  
  /**
  * @brief Accesseur du type de champ MySql.
  *
  * Permet de convertir un type de champ DBF en type de champ MySql.
  * @param type : type de champ dbf.
  * @param length : longueur du champ dbf.
  * @param precision : pr�cision du champ dbf.
  * @private
  */
  function _getType($type, $length, $precision){
    if ($type=="N" && $precision==0) return "int(".$length.")";
    if ($type=="N" && $precision>=1) return "double(".$length.",".$precision.")";
    if ($type=="C" && $length>=4) return "varchar(".$length.")";
    if ($type=="C" && $length<=3) return "char(".$length.")";
    if ($type=="D") return "date";
    if ($type=="L") return "char(".$length.")";
    if ($type=="F" && $precision==0) return "float(".$length.")";
    if ($type=="F" && $precision>=1) return "float(".$length.",".$precision.")";
    if ($type=="N/A" && $length>=4) return "varchar(".$length.")";
    if ($type=="N/A" && $length<=3) return "char(".$length.")";
    if ($type=="M" || $type=="G") return "longtext";
    if ($type=="P" || $type=="B") return "longblob";
    if ($type=="T") return "datetime";
    if ($type=="I") return "int(".$length.")";
    if ($type=="Y") return "double(".$length.",".$precision.")";
  }

  /**
  * @brief Fonction qui lance la cr�ation de la table.
  *
  * @param tabFields : tableau contenant la liste des champs de la table
  * @private
  */
  function _createTable()
  {
    $tabFields = $this->get_fields();
    $strSql = "CREATE TABLE ".$this->getStr_nomTableSansCarSpec($this->getTableName())." (\n";
    foreach ($tabFields as $key => $field){
      $strSql .= "`".strtoupper($field["name"])."` ".$this->_getType($field["type"],$field["length"],$field["precision"]).",\n";
    }
    $len = strlen($strSql);
    $strSql = substr($strSql,0,$len-2).") TYPE=InnoDB CHARSET=latin1; \n";
      
    if ($this->_strMode == $this->MODE_OUTPUT_SQL) {
        $this->_outputSql($strSql);
    } elseif ($this->_strMode == $this->MODE_OUTPUT_HTML) {
        $strSql = preg_replace("/\n/","<br>",$strSql);
        echo ($strSql);
        echo "<br>";
    } else {
      $strSql = preg_replace("/;\n/","",$strSql);
      $strSql = preg_replace("/\n/","",$strSql);
      $this->_dbConn->executeSql($strSql);
    }
  }
  
  /**
  * @brief Fonction qui lance l'insertion des donn�es dans la table.
  *
  * @param tabFields : tableau contenant la liste des champs de la table
  * @param tabRecords : tableau contenant la liste des enregistrements � ins�rer
  * @private
  */
  function _insertData()
  {
    ini_set("max_execution_time",300);
 
    $tabFields = $this->get_fields();
    while($record = $this->getRecordWithNamesIter()) {
      
      // Ignore deleted fields
      if ($record["deleted"] != "1") {

        $strSql = "insert into ".$this->getStr_nomTableSansCarSpec($this->_strTableName)." ( \n";

        // get the column names
        for($count=0; $count<count($tabFields); $count++){
          $strSql .= "`".strtoupper($tabFields[$count]["name"]) . "`, \n";
        }
        $strSql = substr($strSql,0, strlen($strSql)-3)."\n";
        $strSql .= " ) values (\n";
	
        // get the column values
        for($count=0; $count<count($tabFields); $count++) {
	  // set_time_limit();
          //if (connection_status()==2)echo  "stop";
          $type = $tabFields[$count]["type"];
          if ($type=="N" || $type=="F" ||$type=="I" ||$type=="Y") {
            $strSql .= $record[$tabFields[$count]["name"]].", \n";
          } else {
            $strSql .= "'".$this->_dbConn->analyseSql(trim($record[$tabFields[$count]["name"]]))."', \n";
          }
        }
        
        $strSql = substr($strSql,0, strlen($strSql)-3);
        $strSql .= " );\n";

        if ($this->_strMode == $this->MODE_OUTPUT_SQL) {
          $this->_outputSql($strSql);
        } elseif ($this->_strMode == $this->MODE_OUTPUT_HTML) {
            $strSql = preg_replace("/\n/","<br>",$strSql);
            echo ($strSql);
            echo "<br>";
        } else {
          $strSql = preg_replace("/;\n/","",$strSql);
          $strSql = preg_replace("/\n/","",$strSql);
          $this->_dbConn->executeSql($strSql);
        }
      }
    }

  }
  
  /**
  * @brief Renvoi le nom de la table en ayant remplac� les caract�res sp�ciaux.
  *
  * @param strNomTable : Nom de la table
  */
  function getStr_nomTableSansCarSpec($strNomTable) {
    $strReturn = preg_replace("/-/","_",$strNomTable);
    $strReturn = preg_replace("/'/","",$strReturn);
    return $strReturn;
  }

}

?>