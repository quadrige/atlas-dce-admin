<?php
include_once("dr.class.php");

/**
 * @class drMySql
 *
 * @brief Classe qui h�rite de la class dr (datarow)
 */
class DrMySql extends Dr
{
  /**
   * @brief Constructeur par d�faut
   *
   * @param tabRow Tableau associatif d'un enregistrement
   */
  function DrMySql(&$oRow)
  {
    if( isset($oRow) && is_array($oRow) ) {
      // met en majuscule toutes les cl�s
      while( list($strKey, $strVal) = each($oRow) ) {
        $this->oRow[strtoupper($strKey)] = $strVal;
      }
      $this->iCountField = count($this->oRow)/2;
    } else {
      $this->oRow = array();
      $this->iCountField = 0;
    }
  }

  /**
   * @brief  Retourne la valeur du champs pass� en param�tre
   *         Retourne une chaine vide si le champ n'existe pas
   *
   * @param strField  nom du champs
   * @param bToUpper  vrai=force strField en majuscule (par d�faut), faux=ne modifie pas strField
   * @return Retourne une chaine
   */
  function getValueName($strField, $bToUpper=true)
  {
    $strRes = "";
    if( $bToUpper == true ) 
      $strField = strtoupper($strField);
    if( is_array($this->oRow) && array_key_exists($strField, $this->oRow) ) {
      $strRes = $this->oRow[$strField];
    }
    return $strRes;
  }

  /**
   * @brief Retourne la valeur du champs identifiant par son num�ro d'ordre dans la requete
   *
   * @param iNumField num�ro du champs (indic� � 0 � n-1)
   * @return Retourne une chaine : valeur du champs
   */
  function getValueNum($iNumField)
  {
    $strRes = "";
    if( is_array($this->oRow) ) {
      if( is_int($iNumField) && array_key_exists($iNumField, $this->oRow) )
        $strRes =  $this->oRow[$iNumField];
    }
    return $strRes;
  }
	
  /**
   * @brief Retourne dans un tableau la liste des champs
   *
   * @return Retoune un array
   */
	function getFields()
  {
    $tabRes = array();
    $indice=0 ;
    foreach ( $this->oRow as $key => $value) {
      if ( !is_int($key) ) {
        $tabRes[$indice] = $key;
        $indice++ ;
      }
    }
    return $tabRes;
  }

  /**
   * @brief R�initialise l'objet
   */
  function Close( )
  {
    $this->oRow = array();
    $this->iCountField = 0;
  }

}
?>