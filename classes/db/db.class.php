<?php

/**
 * @class Classe abstraite Db
 *
 * @brief Classe de base pour la connexion � une base de donn�es
 */
class Db
{
  /** Adresse IP du serveur de base de donnees */
  var $strHost;
    
  /** Identifiant de l'utilisateur */
  var $strLogin;

  /** Nom de la base de donnees */
  var $strDb;

  /** Mot de passe */
  var $strPwd;

  /** Alias */
  var $strAlias;

  /** num�ro de port */
  var $strPort;
    
  /** connection */
  var $conn;

  /** Message de la derni�re erreur rencontr�e */
  var $lastErr;

  /** true si utilisation d'une transaction, false sinon (par d�faut) */
  var $bUseCommit;

  /** true tant que les ex�cutions r�ussissent avec succ�s, false d�s qu'une erreur est d�tect�e */
  var $bResExecuteInTransaction;

  /**
   * @brief Constructeur de la classe db : initialisation des attributs de l'objet db
   *
   * @param strLogin Identifiant de l'utilisateur
   * @param strHost  Adresse IP du serveur de base de donnees
   * @param strPwd   Mot de passe
   * @param strDb    Nom de la base de donnees
   * @param strAlias Alias de la base de donnees
   * @param strPort  Num�ro de port
   */
  function Db( $strLogin,  $strHost,  $strPwd,  $strDb,  $strAlias,  $strPort )
  {
    $this->strLogin = $strLogin;
    $this->strHost = $strHost;
    $this->strPwd = $strPwd;
    $this->strDb = $strDb;
    $this->strAlias = $strAlias;
    $this->strPort = $strPort;
    $this->lastErr = false;
    $this->bUseCommit = false;
    $this->bResExecuteInTransaction = true;
  }

  /**
   * @brief Fixe la nouvelle erreur rencontr�e
   *
   * @param err false ou tableau associatif contenant l'erreur
   */
  function _SetError($err)
  {
    $this->lastErr = $err;
  }

  /**
   * @brief Retourne la derni�re erreur rencontr�e
   *
   * @return Retourne une chaine, vide si pas d'erreur, texte sinon
   */
  function GetLastError()
  {
    if( !is_string($this->lastErr) ) $this->lastErr = "";
    return $this->lastErr;
  }

  /**
   * @brief �tablit la connection avec la base de donn�es
   */
  function Connect( ) { }

  /**
   * @brief D�connection avec la base de donnees
   */
  function Disconnect( ) { }

  /**
   * @brief Retourne le dataset correspondant � la requete strSql
   *
   * @param strSql  Requete SQL
   * @param idFirst Indice de pagination : premier �l�ment
   * @param idLast  Indice de pagination : dernier �l�ment
   * @return Retourne un dataSet
   */
  function InitDataset($strSql, $idFirst=0, $idLast=-1) { }

  /**
   * @brief Ex�cute la requ�te SQL = insert, update, delete
   *
   * @param strSql Requ�te SQL
   * @return Retourne un entier : 0 si KO, 1 si OK
   */
  function ExecuteSql($strSql) { }

  /**
   * @brief initialise une transaction sur la connexion courante
   *        Ouvre la transaction (bUseCommit = true)
   */
  function InitTransaction() { }

  /**
   * @brief initialise une transaction sur la connexion courante
   *
   * @return Retourne un bool : true si commit, false si rollback
   */
  function DoneTransaction()
  {
    if( $this->bResExecuteInTransaction == true ) {
      $this->CommitTransaction();
      return true;
    }

    $this->RollBackTransaction();
    return false;
  }

  /**
   * @brief Effectue un commit sur l'ensemble des requ�tes ex�cut�es sur la transaction en cours
   *        Ferme la transaction apr�s le commit (bUseCommit = false)
   *
   * @return Retourne un bool : true si ok, false sinon
   */
  function CommitTransaction() { }

  /**
   * @brief Effectue un rollback pour annuler l'ensemble des requ�tes ex�cut�es sur la transaction en cours
   *        Ferme la transaction apr�s le commit (bUseCommit = false)
   *
   * @return Retourne un bool : true si ok, false sinon
   */
  function RollBackTransaction() { }

  /**
   * @brief Remplace les caract�res sp�ciaux d'un champ texte d'une requete SQL
   *
   * @param strString Valeur du champ texte d'une requete
   * @return Retourne une chaine obtenue apr�s traitement
   */
  function AnalyseSql($strString)
  {
    $strTmp=stripslashes($strString);
    return preg_replace("/'/", "''", $strTmp);
  }

  /**
   * @brief Retourne le code sql qui va bien pour un clob
   * 
   * @param strField  nom du champ clob
   * @param strValue  valeur du champ clob
   * @return Retourne un string
   */
  function getCLob($strField, $strValue) { }
  
  /**
   * @brief Remplace les caract�res sp�ciaux d'un champ texte d'une requete SQL
   *
   * @param strString Valeur du champ texte d'une requete
   * @return Retourne une chaine obtenue apr�s traitement
   */
  function GetCheckbox($strString)
  {
    if( $strString != "" && $strString != "0" )
      return 1;
    return 0;
  }

  /**
   * @brief Retourne une chaine de comparaison dans une requete SQL
   *
   * @param strField   Nom du champ dont la valeur est � tester
   * @param strCompare Op�rateur de comparaison
   * @param strValeur  Valeur � comparer
   * @param strCaseOk  Valeur retourn�e si comparaison vraie
   * @param strCaseNok Valeur retourn�e si comparaison fausse
   * @return Retourne une chaine : l'expression SQL associ�e � la comparaison
   */
  function CompareSql($strField, $strCompare, $strValue, $strCaseOK, $strCaseNok) { }

  /**
   * @brief Obtenir le prochain identifiant � inserer dans la table strTable
   *
   * @param strTable    Nom de la table
   * @param strField    Nom du champ id
   * @param strSequence Nom de la sequence associ�e
   * @return Retourne un entier : le prochain id
   */
  function GetNextId($strTable, $strField, $strSequence="") { }

  /**
   * @brief Formate une date au format SQL da la base de donnees
   *
   * @param strFormat Format de la date passee en parametre
   * @param strDate   Valeur de la date �quivalente au format ou dans son expression enti�re
   * @param bToDate   Identifie l'expression � retourner : 
   *                  = true  : l'expression retourn�e par la requete est une date
   *                  = false : l'expression retourn�e par la requete est une chaine
   * @param Retourne une chaine : l'expression SQL associ�e
   */
  function GetDateFormat($strFormat, $strDate, $bToDate=true) { }

  /**
   * @brief Retourne l'expression SQL permettant d'additionner des intervalles de temps � une date
   *
   * @param strChamp    Nom du champ ou expression sql � traiter
   * @param iNb         Nombre d'intervalles � ajouter
   * @param strInterval Type d'intervalle : Y=ann�e, M=mois, D=jour
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetDateAdd($strChamp, $iNb, $strInterval) {}

  /**
   * @brief Retourne l'expression SQL qui fournit la concatenation d'un nombre ind�finit de param�tres
   *
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetConcat() { }

  /**
   * @brief Retourne l'expression SQL qui fournit la concatenation d'un nombre ind�finit de param�tres
   *
   * @param strChamp Nom du champ ou expression sql � traiter
   * @param iPos     Position de d�part (premier caract�re = 0)
   * @param iLength  Longueur de la sous-chaine (facultatif)
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetSubstring($strChamp, $iPos, $iLength=-1) { }

  /**
   * @brief Retourne l'expression SQL qui fournit la date-heure syst�me
   *
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetDateCur() { }

  /**
   * @brief Retourne l'expression SQL qui transforme en minuscules une expression
   *
   * @param strChamp Nom du champ ou expression sql � traiter
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetLowerCase($strChamp) { }

  /**
   * @brief Retourne l'expression SQL qui transforme en majuscules une expression
   *
   * @param strChamp Nom du champ ou expression sql � traiter
   * @return Retourne une chaine : l'expression SQL associ�e
   */
  function GetUpperCase($strChamp) { }

  /**
   * @brief Met a jour le champ rang d'une table en fonction des parametres
   *
   * @param strTable   Nom de la table
   * @param strChamp   Nom du champ
   * @param strNewRang Indice du nouveau rang
   * @param bAdd       =true si ajout, false si suppression
   * @param strWhere   Condition supplementaire pour la selection du rang
   * @return Retourne un booleen : toujours true
   */
  function UpdateRang($strTable, $strChamp, $strNewRang, $bAdd, $strWhere) { }

  /** @brief Retourne l'indice de rang suivant
   *
   * @param strTable   Nom de la table
   * @param strChamp   Nom du champ
   * @param strWhere   Condition supplementaire pour la selection du rang
   * @return Retourne un entier
   */
  function getNextRang($strTable, $strChamp, $strWhere) { }

/** @brief Permute les rangs de 2 occurences
   *
   * @param strTable   Nom de la table
   * @param strChampRang   Nom du champ rang
   * @param strWhere   Condition supplementaire pour la selection du rang
   * @return Retourne un entier
   */
 function PermuteRang($strTable, $strChampRang, $strChampId, $id, $iRang, $iDelta, $strWhere)
  {
    // ne fait rien physiquement
    // met � jour la m�ta donn�e
    $champPermute_rang = $iRang + $iDelta;
    if( $champPermute_rang < 1 ) return;
    if ($strWhere=="")
      $strWhere="1=1";
    
    $strSql = "update ".$strTable." set".
      " ".$strChampRang."=".$iRang.
      " where ".$strWhere." and ".$strChampRang."=".$champPermute_rang;
    $this->executeSql($strSql);
    
    $strSql = "update ".$strTable." set".
      " ".$strChampRang."=".$champPermute_rang.
      " where ".$strChampId."=".$id." and ".$strWhere;
    $this->executeSql($strSql);
  }

  /**
   * @brief Retourne une chaine contenant la fonctionnalite Oracle de comparaison
   *        de chaine sans tenir compte des caracteres francais (accent, etc...)
   *
   * @param strChamp Nom du champ de la table
   * @param strOp    Operateur de test SQL : like, =
   * @param strVal   Chaine de comparaison qui doit etre traitee par ora_analyseSQL auparavant
   * @return Retourne la chaine apr�s traitement
   */
  function GetStrConvert2ASCII7($strChamp, $strOp, $strVal) { }


  /**
   * @brief Enleve les doublons d'une liste s�par�e par une virgule
   *
   * @param strListId  liste d'identifiants s�par�s par une virgule
   * @return Retourne un string : chaine sans les doublons
   */
  function delDoublon($strListId)
  {
    $tabId = explode(",", $strListId);
    $tabNewId = array_unique($tabId);
    $strListId = implode(",", $tabNewId);
    return $strListId;
  }
}
?>