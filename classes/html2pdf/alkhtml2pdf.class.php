<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Html2Pdf
Module fournissant une interface de convertion html2pdf
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

/**
 * @file alkhtml2pdf.class.php
 * @class AlkHtml2Pdf
 * @package Alkanet_Class_Html2Pdf
 * @brief Classe de base pour la génération de pdf par shell.
 *        Attention : le nom du fichier pdf est donné par le tag <title>XXX</title> => XXX.pdf
 * Pour le mode FILE, le fichier PDF est généré dans le cache ou strPathDest si non vide
 *                    l'attribut bSupprFichierSource n'est pris en compte qu'en mode FILE
 * Dans les autres modes, pas de fichier générer dans le cache ou strPathDest si non vide
 *                        il reste le fichier $strPathSource.$strFichierSource
 */
class AlkHtml2Pdf extends AlkObject
{
  /** Url du service, valeur par défaut :
   *  - ALK_SIALKE_URL."services/html2pdf/" 
   *  - "http://html2pdf.alkante.com/" 
   * en fonction du param bUseServLocal du constructeur 
   */
  protected $strUrlService;
  
  /** Url de base du fichier source html */
  protected $strUrlSource;
  
  /** Emplacement du fichier source html */
  protected $strPathSource;

  /** Nom du fichier source html */
  protected $strFichierSource;

  /** Emplacement  du fichier PDF généré */
  protected $strPathDest;

  /** Nom du fichier html créé au moment de la génération, si =vide par défaut*/
  protected $strFichierDest;
  
  /** Contenu HTML à générer en PDF */
  protected $strHtml;

  /** vrai si suppression du fichier source avant et après le processus */
  protected $bSupprFichierSource;

  /** vrai si on utilise la génération pdf locale, faux si service web alkante */
  protected $bUseServLocal;

  /** tableau de configuration pour la génération du PDF */
  protected $tabConfig = array();

  /** =true si passage des paramètres en session, =false par défaut avec passage sur l'url */
  protected $bEncode;

  /** true pour un envoi des données sérialisé (sess=1), false pour un envoi des données en url (sess=0) */
  protected $bSendSerialized;

  /** true pour une execution en mode PHP CLI */
  protected $bExecuteByPHPCli;

  /** true pour une execution avec l'outil WkHtmlToPdf */
  protected $bExecuteByWkHtml;
  /**
   * @brief Constructeur de la classe Import : initialisation des attributs de Import.
   *
   * @param strUrlSource          Url complete du fichier html source
   * @param strPathSource         Chemin complet du fichier html source
   * @param strFichierSource      Nom du fichier html source
   * @param strHtml               Contenu html
   * @param strPathDest           Chemin complet du fichier pdf générer
   * @param strFichierDest        Nom du fichier pdf générer
   * @param bSupprFichierSource   supprimer le fichier html source avant la génération (dans le cas ou le contenu est passé en paramètre), false par défaut
   */
  public function __construct($strUrlSource, $strPathSource="", $strFichierSource="", $strHtml="",
                              $strPathDest="", $strFichierDest="", $bSupprFichierSource=false)
  {
    parent::__construct();
    
    $this->bSendSerialized = true;
    $this->bExecuteByPHPCli = false;
    $this->bExecuteByWkHtml = false;

    $this->strUrlService       = ALK_ALKANET_ROOT_URL."services/html2pdf/";
    $this->bUseServLocal       = true;
    
    $this->strUrlSource        = $strUrlSource;
    $this->strPathSource       = $strPathSource;
    $this->strFichierSource    = $strFichierSource;

    $this->strPathDest         = $strPathDest;
    $this->strFichierDest      = $this->filename_escape($strFichierDest);

    $this->strHtml             = $strHtml;

    $this->bSupprFichierSource = $bSupprFichierSource;
    
    $this->tabConfig = 
      array('compress'                => "",
            'cssmedia'                => "screen",           // handheld, print, projection, screen, tty, tv
            'debugbox'                => "",                 // 1 ou vide
            'debugnoclip'             => "",                 // 1 ou vide : 1 disabled clipping
            'pageborder'              => "",                 // 1 ou vide
            'encoding'                => "",                 // utf-8, iso-8859-1..15, windows-1250..1252
            'html2xhtml'              => "",                 // vide ou 1 : transformation auto html -> xhtml
            'imagequality_workaround' => "",                 // vide ou 1 : Use PS2PDF image quality problem workaround
            'landscape'               => "",                 // 1 pour paysage, vide pour portrait
            'leftmargin'              => 10,                // en mm
            'rightmargin'             => 10,                // en mm
            'topmargin'               => 10,                // en mm
            'bottommargin'             => 10,                // en mm
            'media'                   => "A4",   // Letter, Legal, A0Oversize, A0..A5, B5, Folio, A6..A10, Screenshot640, Screenshot800,Screenshot1024
            'method'                  => "fpdf", // fastps, pdflib, fpdf, png
            'mode'                    => 'html',
            'output'                  => 1,      // 0=PDF will be opened in browser, 1=Browser download as file, 2=File on server
            'pixels'                  => 800,    // largeur page en pixel : 640, 800, 1024, 1280
            'pdfversion'              => "1.4",  // 1.3=Reader 4, 1.4=Reader 5, 1.5=Reader 6
            'ps2pdf'                  => 0, 
            'pslevel'                 => 3,  // 2 ou 3 dans le cas Postcript
            'renderfields'            => 1,  // vide ou 1
            'renderforms'             => "",  // vide ou 1
            'renderimages'            => 1,  // vide ou 1
            'renderlinks'             => 1,  // vide ou 1
            'scalepoints'             => 1,  // vide ou 1 : Keep screen pixel/point ratio
            'smartpagebreak'          => 1,  // vide ou 1 : Use "smart" pagebreaking algorith
            'transparency_workaround' => "",  // vide ou 1 : Use PS2PDF transparency problem workaround
            'headerhtml'              => "",
            'footerhtml'              => "",
            'watermarkhtml'           => "",
            'toc'                     => "",   // vide ou 1 : table des matières
            'toc-location'            => "before",  // before (1ere page), after (dernière page), placeholder
            
            'URL'                     => $this->strUrlSource.$this->strFichierSource,
            'bHote'                   => "ALK",
            'fileName'                => $this->strFichierDest, 
            'output_file_directory'   => ( $strPathDest != "" ? $strPathDest : ALK_ALKANET_ROOT_PATH.ALK_ROOT_UPLOAD.ALK_UPLOAD_CACHE ),
            );
  }
  
  /**
   * Change le mode d'envoi des paramètres : 
   * @param bSendSerialized true=> sess=1&tab=[encodeParam(serialize(tabConfig))], false=sess=1&{key_tabConfig=value_tabConfig}
   */
  public function setSendSerializedMode($bSendSerialized)
  {
    $this->bSendSerialized = $bSendSerialized;
  }
  
  /**
   * Change le mode d'exécution de la génération
   * @param bExecuteByPHPCli true=> exécution en mode cli
   */
  public function setExecuteByPHPCli($bExecuteByPHPCli)
  {
    $this->bExecuteByPHPCli = $bExecuteByPHPCli;
    if ( !defined("ALK_CMD_PHP") ){
      trigger_error(__CLASS__."[line ".__LINE__."] : Veuillez définir la constante ALK_CMD_PHP donnant le chemin physique de l'exécutable php (exemple : /usr/bin/php)", E_USER_ERROR);
    }
  }
  
  /**
   * Change le mode d'exécution de la génération
   * @param bExecuteByWkHtml true=> exécution avec l'outil wkhtmltopdf
   */
  public function setExecuteByWkHtml($bExecuteByWkHtml)
  {
    $this->bExecuteByWkHtml = $bExecuteByWkHtml;
    if ( !defined("ALK_CMD_WKHTMLTOPDF") ){
      trigger_error(__CLASS__."[line ".__LINE__."] : Veuillez définir la constante ALK_CMD_WKHTMLTOPDF donnant le chemin physique de l'exécutable wkhtmltopdf (exemple : /usr/bin/wkhtmltopdf)", E_USER_ERROR);
    }
  }
  /**
   * Modifie le paramétrage du pdf par réception de soumission (GET ou POST) ou décodage de token
   */
  public function requestParam()
  {
    foreach( $this->tabConfig as $strParamName=>$strDefaultValue) {
      if ( is_numeric($strDefaultValue) ) {
        $this->tabConfig[$strParamName] = AlkRequest::getToken($strParamName, AlkRequest::_REQUESTint($strParamName, $strDefaultValue));
      }
      else {
        $this->tabConfig[$strParamName] = AlkRequest::getToken($strParamName, AlkRequest::_REQUEST($strParamName, $strDefaultValue));
      } 
    }
  }
  
  /**
   * Modifie le paramétrage du pdf
   * @param strParamName nom du paramètre pdf
   * @param strValue     valeur du paramètre
   */
  public function setParam($strParamName, $strValue)
  {
    $this->tabConfig[$strParamName] = $strValue; 
  }
  
  
  /**
   * @brief Fonction qui prépare puis exécute la génération du pdf
   *        Retourne 0 si ok, ko sinon
   *    
   * @return Retourne un entier
   */
  public function getPdf()
  {
    $dateFic = date("dmy_his");
    
    if( @file_exists($this->strPathDest.$this->strFichierDest) && 
        @is_file($this->strPathDest.$this->strFichierDest) ) {
      // suppression du fichier pdf si il existe
      @unlink($this->strPathDest.$this->strFichierDest);
    }
    if( $this->bSupprFichierSource &&
        @file_exists($this->strPathSource.$this->strFichierSource) && 
        @is_file($this->strPathSource.$this->strFichierSource) ) {
      // suppression du fichier html source
      @unlink($this->strPathSource.$this->strFichierSource);
    }
    if( $this->strHtml != "" ) {
      //génération d'un fichier html temporaire
      $this->strFichierSource = ( $this->strFichierSource != "" ? $this->strFichierSource : "doc".$dateFic.".htm" );

      $hTemp = @fopen($this->strPathSource.$this->strFichierSource, "w");
      if( $hTemp ) {
        @fwrite($hTemp, $this->strHtml);
        @fclose($hTemp);
      }
    }
    $this->tabConfig["URL"] = $this->strUrlSource.$this->strFichierSource;
    if ( $this->bSendSerialized ){
      $strParam = "sess=1&tab=".AlkRequest::getEncodeParam(serialize($this->tabConfig));
    }
    else {
      $strParam = "sess=0&".http_build_query($this->tabConfig);
    }
    $strUrlServ = $this->strUrlService."alk_html2ps.php?".$strParam;
    $strRet = $this->_executePdf($strUrlServ);
    return $strRet;
  }
  
  protected function getConfigWKHtmlToPdf()
  {
    $tabTraduction = array(
      'compress'                => null,
      'cssmedia'                => array(
        "opt" => "print-media-type",
        "values" => array("print" => "_ON_", "_ELSE_" => "_ON_")
      ),           // handheld, print, projection, screen, tty, tv
      'debugbox'                => null,                 // 1 ou vide
      'debugnoclip'             => null,                 // 1 ou vide : 1 disabled clipping
      'pageborder'              => null,                 // 1 ou vide
      'encoding'                => array(
        "opt" => "encoding",
        "values" => array("_ANY_"=>"_SAME_")
      ),                 // utf-8, iso-8859-1..15, windows-1250..1252
      'html2xhtml'              => null,                 // vide ou 1 : transformation auto html -> xhtml
      'imagequality_workaround' => null,                 // vide ou 1 : Use PS2PDF image quality problem workaround
      'landscape'               => array(
        "opt" => "orientation",
        "values" => array("1" => "Landscape", "" => "Portrait")
      ),                 // 1 pour paysage, vide pour portrait
      'leftmargin'              => array(
        "opt" => "margin-left",   "values" => array("_ANY_"=>"_SAME_")
      ),                // en mm
      'rightmargin'             => array(
        "opt" => "margin-right",  "values" => array("_ANY_"=>"_SAME_")
      ),                // en mm
      'topmargin'               => array(
        "opt" => "margin-top",    "values" => array("_ANY_"=>"_SAME_")
      ),                // en mm
      'bottommargin'            => array(
        "opt" => "margin-bottom", "values" => array("_ANY_"=>"_SAME_")
      ),                // en mm
      'media'                   => array(
        "opt" => "page-size", "values" => array("_ANY_"=>"_SAME_")
      ),   // Letter, Legal, A0Oversize, A0..A5, B5, Folio, A6..A10, Screenshot640, Screenshot800,Screenshot1024
      'method'                  => null, // fastps, pdflib, fpdf, png
      'mode'                    => null,
      'output'                  => array(
        "conf" => "output", "values" => array("_ANY_"=>"_SAME_")
      ),      // 0=PDF will be opened in browser, 1=Browser download as file, 2=File on server
      'pixels'                  => null,    // largeur page en pixel : 640, 800, 1024, 1280
      'pdfversion'              => null,  // 1.3=Reader 4, 1.4=Reader 5, 1.5=Reader 6
      'ps2pdf'                  => null, 
      'pslevel'                 => null,  // 2 ou 3 dans le cas Postcript
      'renderfields'            => null,  // vide ou 1
      'renderforms'             => null,  // vide ou 1
      'renderimages'            => null,  // vide ou 1
      'renderlinks'             => null,  // vide ou 1
      'scalepoints'             => array(
        "opt" => "zoom",
        "values" => array("" => "_OFF_", "_ELSE_" => "_SAME_")
      ),  // vide ou 1 : Keep screen pixel/point ratio
      'smartpagebreak'          => null,  // vide ou 1 : Use "smart" pagebreaking algorith
      'transparency_workaround' => null,  // vide ou 1 : Use PS2PDF transparency problem workaround
      'headerhtml'              => array(
        "opt" => "header-center", "values" => array("_ANY_"=>"_SAME_")
      ),
      'footerhtml'              =>  array(
        "opt" => "footer-center", "values" => array("_ANY_"=>"_SAME_")
      ),
      'watermarkhtml'           => null,
      'toc'                     => array(
        "opt" => "toc",
        "values" => array("1" => "_ON_", "_ELSE_" => "_OFF_")
      ),   // vide ou 1 : table des matières
      'toc-location'            => null,  // before (1ere page), after (dernière page), placeholder
      'toc-title'            => array(
        "opt" => "toc-header-text", "values" => array("_ANY_"=>"_SAME_")
      ),  // before (1ere page), after (dernière page), placeholder
      
      'URL'                     => array(
        "arg" => "2", "values" => array("_ANY_"=>"_SAME_"), "after"=>" "
      ),
      'bHote'                   => null,
      'fileName'                => array(
        "arg" => "4", "values" => array("_ANY_"=>"_SAME_"), "after"=>".pdf"
      ),
      'output_file_directory'   => array(
        "arg" => "3", "values" => array("_ANY_"=>"_SAME_"), "after"=>""
      ),
    ) ;
    
    $tabOldConfig = $this->tabConfig;
    $tabNewConfig = array("opt"=>array(), "arg"=>array(), "conf"=>array());
    foreach ($tabOldConfig as $key=>$value){
      if ( !array_key_exists($key, $tabTraduction) ){
        $tabNewConfig["opt"][$key] = $value;
        continue;
      }
      
      $tabTrad = $tabTraduction[$key];
      if ( is_null($tabTrad) ) continue;
      
      $part = "opt";
      if ( array_key_exists("opt", $tabTrad)){ 
        $key = $tabTrad["opt"];
        $part = "opt"; 
      }
      else if ( array_key_exists("arg", $tabTrad)) {
        $part = "arg";
        $key = $tabTrad["arg"];
      }
      else if ( array_key_exists("conf", $tabTrad)) {
        $part = "conf";
        $key = $tabTrad["conf"];
      }
      $after = (array_key_exists("after", $tabTrad) ? $tabTrad["after"] : "");
      
      $tabValueUsed = array();
      foreach ($tabTrad["values"] as $value_input=>$value_output){
        if ( $value=="_OFF_" ) continue;
        switch( $value_output ){
          case "_ON_" :
            switch ($value_input){
              case $value :
              case "_ANY_" :
                $tabValueUsed[] = $value_input; 
                $tabNewConfig[$part][$key] = "";
              break;
              case "_ELSE_" :
                if ( !in_array($value, $tabValueUsed) )
                  $tabNewConfig[$part][$key] = "";
              break;
            }
          break;
          
          case "_OFF_" ://not added
            $tabValueUsed[] = $value_input; 
          break;
          
          case "_SAME_" :
            switch ($value_input){
              case $value :
              case "_ANY_" :
                $tabValueUsed[] = $value_input; 
                if  ( $value!="" ) 
                  $tabNewConfig[$part][$key] = $value.$after;
              break;
              case "_ELSE_" :
                if ( !in_array($value, $tabValueUsed) && $value!="") 
                  $tabNewConfig[$part][$key] = $value.$after;
              break;
            }
          break;
          
          default :
            switch ($value_input){
              case $value :
              case "_ANY_" :
                $tabValueUsed[] = $value_input; 
                $tabNewConfig[$part][$key] = $value_output.$after;
              break;
              case "_ELSE_" :
                if ( !in_array($value, $tabValueUsed) )
                  $tabNewConfig[$part][$key] = $value_output.$after;
              break;
            }
          break;
        }
      }
    }
    if ( array_key_exists("fileName", $this->tabConfig) && $this->tabConfig["fileName"]!="" ){
      $tabNewConfig["opt"]["title"] = preg_replace("!\.\w+$!", "", $this->tabConfig["fileName"]);
    }
    $tabNewConfig["opt"]["disable-smart-shrinking"] = "";
    
    $strArgv = "";
    foreach ($tabNewConfig["opt"] as $option=>$value){
      $strArgv .= " --".$option.($value!="" ? " \"".$value."\"" : "");
    }
    $strArgv .= " "; 
    ksort($tabNewConfig["arg"]);
    foreach ($tabNewConfig["arg"] as $argument){
      $strArgv .= $argument;
    }
    $this->tabConfig = $tabNewConfig["conf"];    
    return $strArgv;
  }

  /**
   * @brief Lance l'exécution du pdf, 
   *        Evalue le mode output :  0=PDF will be opened in browser, 1=Browser download as file, 2=File on server
   *        Retourne 0 si ok, ko sinon
   * 
   * @param strUrl  Url du service avec ses paramètres
   * @return Retourne un entier
   */
  protected function _executePdf($strUrl)
  {
    $strRet = "0";
    if ( $this->bExecuteByWkHtml ){
      $strArgv = "";
      $strArgv = $this->getConfigWKHtmlToPdf();
      echo $cmd_service_html2pdf = ALK_CMD_WKHTMLTOPDF .$strArgv;
      $tabRes = array();
      exec($cmd_service_html2pdf, $tabRes);
      
      $strPathFilePDF = $this->strPathDest."/".$this->strFichierDest.".pdf";
      $bRes = file_exists($strPathFilePDF) && is_file($strPathFilePDF);
          
      
      $strRet = implode("\r\n", $tabRes).($bRes ? "\r\nLe fichier ".$strPathFilePDF." a été généré" : "\r\nLe fichier ".$strPathFilePDF." n'a pas été généré");
      if ( !$bRes )
        return $strRet;
      @chmod($strPathFilePDF, 0775);

      switch( $this->tabConfig['output'] ) {
        case '0'://PDF will be opened in browser
          $strUrl = str_replace(ALK_ROOT_PATH, ALK_ROOT_URL, $strPathFilePDF);
          echo "<html><head><script type='text/javascript'>".
            " function onLoadWindPdf() {".
            "   this.document.location='".$strUrl."'; ".
            " } </script></head>".
            "<body onload='onLoadWindPdf()'><!--a href='".$strUrl."'>go</a--></body></html>";          
        break;
        case '1'://Browser download as file
          include_once(ALK_ALKANET_ROOT_PATH."lib/lib_file.php");
          AffHeaderFileDownload($strPathFilePDF, $this->strFichierDest.".pdf");
          readfile($strPathFilePDF);
          @unlink($strPathFilePDF);
        break;
        case '2'://File on server
          // return $strRet
        break;
      }
    }

    else if ( $this->bExecuteByPHPCli ){
      $this->strUrlService = str_replace(ALK_ALKANET_ROOT_URL, ALK_ALKANET_ROOT_PATH, $this->strUrlService);
      $strArgv = "";
      $old_output = $this->tabConfig['output'];
      $this->tabConfig['output'] = 2;//nécessaire pour mode cli
      foreach( $this->tabConfig as $strParam => $valParam ) {
        //$strArgv .= " ".$strParam."=\"".str_replace(ALK_ALKANET_ROOT_URL, "file://".ALK_ALKANET_ROOT_PATH, $valParam)."\"";
        $strParamValue = str_replace(ALK_ALKANET_ROOT_URL, "file://".ALK_ALKANET_ROOT_PATH, $valParam);
        if( !(strpos($strParamValue, " ") === false) ) {
          $strParamValue = "alktoken".AlkRequest::getEncodeParam($strParamValue);
        }
        $strArgv .= " ".$strParam."=".$strParamValue;     
      }

      $cmd_service_html2pdf = ALK_CMD_PHP." -d memory_limit=512M ".$this->strUrlService."alkcli_html2ps.php".$strArgv;
      $tabRes = array();
      exec($cmd_service_html2pdf, $tabRes);
      
      $this->tabConfig['output'] = $old_output;
      
      $strPathFilePDF = $this->strPathDest."/".$this->strFichierDest.".pdf";
      $bRes = file_exists($strPathFilePDF) && is_file($strPathFilePDF);
      
      $strRet = implode("\r\n", $tabRes).($bRes ? "\r\nLe fichier ".$strPathFilePDF." a été généré" : "\r\nLe fichier ".$strPathFilePDF." n'a pas été généré");
      if ( !$bRes )
        return $strRet;
      @chmod($strPathFilePDF, 0775);
      
      switch( $this->tabConfig['output'] ) {
        case '0'://PDF will be opened in browser
          $strUrl = str_replace(ALK_ROOT_PATH, ALK_ROOT_URL, $strPathFilePDF);
          echo "<html><head><script type='text/javascript'>".
            " function onLoadWindPdf() {".
            "   this.document.location='".$strUrl."'; ".
            " } </script></head>".
            "<body onload='onLoadWindPdf()'><!--a href='".$strUrl."'>go</a--></body></html>";          
        break;
        case '1'://Browser download as file
          include_once(ALK_ALKANET_ROOT_PATH."lib/lib_file.php");
          AffHeaderFileDownload($strPathFilePDF, $this->strFichierDest.".pdf");
          readfile($strPathFilePDF);
          @unlink($strPathFilePDF);
        break;
        case '2'://File on server
          // return $strRet
        break;
      }
    }
    else {
      switch( $this->tabConfig['output'] ) {
      case '0'://PDF will be opened in browser
      case '1'://Browser download as file
          echo "<html><head><script type='text/javascript'>".
            " function onLoadWindPdf() {".
            "   this.document.location='".$strUrl."'; ".
            " } </script></head>".
            "<body onload='onLoadWindPdf()'><!--a href='".$strUrl."'>go</a--></body></html>";
        break;
      
      case '2'://File on server
        // appel le service de génération du pdf
        $contents = @file_get_contents($strUrl);
        // suppression du fichier html temporaire
        if( $this->bSupprFichierSource &&
            @file_exists($this->strPathSource.$this->strFichierSource) && 
            @is_file($this->strPathSource.$this->strFichierSource) ) {
          // suppression du fichier html temporaire
          @unlink($this->strPathSource.$this->strFichierSource);
        }
  
        // $contents contient  = "File saved as: <a href="file:///[chemin][nom du pdf]">nompdf</a>"
        if( strpos($contents, "File saved as: ") === false ) {
          $strRet = "1";
        }
        break;
      }    
    }

    return $strRet;
  }

  /**
   * @brief Fixe de type de sortie :
   *         BROWSERATTACHMENT = pdf à ouverir par le navigateur 
   *         BROWSER           = pdf à télécharger par le navigateur
   *         FILE              = le fichier est générer dans un fichier
   *                             (le fichier généré se nomme : )
   *
   * @param strMode  Type de sortie = BROWSER (par défaut), BROWSERATTACHMENT ou FILE
   */
  public function setModeOutput($strMode)
  {
    switch ($strMode) {
    case "BROWSERATTACHMENT" :
      $this->tabConfig['output'] = '1';
      break;

    case "FILE" :
      $this->tabConfig['output'] = '2';
      break;

    case "BROWSER" :
    default:
      $this->tabConfig['output'] = '0';
      break;
    }
  }

  function filename_escape($filename) { return preg_replace("/[^a-z0-9-]/i","_",$filename); }
}
?>
