<?php

/**
 *  @brief Classe d'un espace de travail de base
 */
class AlkWorkSpaceBase extends AlkObject
{
  /** R�f�rence vers l'ojet interface source de donn�es espace */
  var $oQuery;

  /** R�f�rence vers l'ojet interface action source de donn�es espace */
  var $oQueryAction;

  /** R�f�rence vers l'ojet interface source de donn�es annuaire */
  var $oQueryAnnu;

  /** R�f�rence ver l'objet interface actions courantes sur annuaire et espace */
  var $oQueryContAnnuAction;

  /** Indentifiant de l'espace */
  var $cont_id;

  /** Identifiant de l'application en cours */
  var $appli_id;

  /** DataRow associ� � l'espace en cours */
  var $oDrCont;
  
  /** Type de la base de donn�es */
  var $typeDB;
  
  /** Connexion � la base de donn�es */
  var $dbConn;
  
  /** Profil de l'utilisateur connect� : 1=admin principal, 2=animateur, 3=utilisateur */
  var $iProfilCont;

  /** Droit de l'utilisateur sur l'application en cours : 0=aucun, 1=utilisation, 2>=util + admin */
  var $iDroitAppli;

  /** Identifiant de l'utilisateur connect� */
  var $agent_id;

  /** Identifiant du profil utilisateur connect� */
  var $profil_id;

  /** tableau contenant les informations relatives � l'application en cours */
  var $tabUrl;
  var $tabAppli;
  var $tabMenuAppli;
  var $tabSsMenuAppli;
  var $tabMenuEspace;
  var $tabSsMenuEspace;

  /**
   * @brief Constructeur par d�faut
   *
   * @param cont_id   Identifiant de l'espace en cours
   * @param appli_id  Identifiant de l'application en cours
   * @param dbConn    Classe de connexion � la source de donn�es
   * @param typeDB    Type de source de donn��s
   * @param queryCont R�f�rence vers l'interface dataSrc de l'espace
   * @param queryAnnu R�f�rence vers l'interface dataSrc de l'annuaire
   * @param agent_id  Identifiant de l'utilisateur connect�
   * @param profil_id Identifiant du profil utilisateur connect�
   */
  function AlkWorkSpaceBase($cont_id, $appli_id, &$dbConn, $typeDB, &$queryCont, 
                        &$queryAnnu, &$queryContAnnuAction, $agent_id="-1", 
                        $profil_id="-1", $ALK_B_REMOVE_PRIV_PROFIL1_SPACE=false)
  {
    $this->cont_id = $cont_id;
    $this->appli_id = $appli_id;
    $this->dbConn = &$dbConn;
    $this->typeDB = $typeDB;
    $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE = $ALK_B_REMOVE_PRIV_PROFIL1_SPACE;

    $this->agent_id = $agent_id;
    $this->profil_id = $profil_id;
    
    $this->oQuery =& $queryCont;
    $this->oQueryAction = null;
    $this->oQueryAnnu =& $queryAnnu;
    $this->oQueryContAnnuAction =& $queryContAnnuAction;

    $this->tabUrl = array();
    $this->tabAppli = array();
    $this->tabMenuAppli = array();
    $this->tabSsMenuAppli = array();
    $this->tabMenuEspace = array();
    $this->tabSsMenuEspace = array();

    $this->iProfilCont = 99;
    $this->iDroitAppli = 0;
    
    $dsCont = $this->oQuery->GetDs_ficheEspaceById($this->cont_id);
    $this->oDrCont = $dsCont->getRowIter();
    
    $iTypeCont = $this->getProperty("CONT_TYPE");
    if( $agent_id == ALK_I_AGENT_INTERNET && $iTypeCont == 1 || $this->appli_id==-1) {
      $this->iDroitAppli = 2;
      return;
    }

    if( $agent_id != "-1" )
      $this->_SetProfil();
  }
  
  /**
   * @brief Retourne une propri�t� de l'espace
   *
   * @param strPropertyName Nom de la propri�t�
   * @return Retourne une chaine
   */
  function getProperty($strPropertyName)
  {
    if( $this->oDrCont )
      return $this->oDrCont->getValueName($strPropertyName);
  }

  /**
   * @brief Affecte � la propri�t� iProfilCont l'identifiant du profil utilisateur par rapport � l'espace courant
   */
  function _setProfil()
  {
    // profil inexistant par d�faut
    $this->iProfilCont = 99;
    if( $this->profil_id == "1" ) {
      // administrateur principal
      $this->iProfilCont = 1;
    } else {
      $dsAgent = $this->oQuery->GetDs_accesEspaceAgentById($this->cont_id, $this->agent_id);
      if( $drAgent = $dsAgent->getRowIter() ) {
        $cont_admin = $drAgent->getValueName("CONT_ADMIN");
        // 2=animateur, 3=utilisateur
        $this->iProfilCont = ( $cont_admin == "1" ? "2" : "3" );
      }
    }

    // aucun droit par d�faut
    $this->iDroitAppli = 0;
    if( $this->profil_id == "1" ) {
      $this->iDroitAppli = 4;
    } else {
      $dsAgent = $this->oQuery->GetDs_accesAppliAgentById($this->appli_id, $this->agent_id);
      if( $drAgent = $dsAgent->getRowIter() )
        $this->iDroitAppli = $drAgent->getValueName("APPLI_DROIT_ID");
    }
  }
  
  /**
   * @brief Retourne le tableau des �lts de menu : liste des applications
   *
   * @param tabAppliMenu Tableau contenant les elts de menu pour l'application en cours
   * @return Retourne un tableau contenant le couple (tableau Appli, tableau sous appli)
   */
  function getTabMenuAppli($tabAppliMenu, $ALK_SIALKE_URL, $ALK_PATH_UPLOAD_APPLI_LOGO)
  {
    if( is_array($this->tabMenuAppli) && count($this->tabMenuAppli)>0 )
      return array($this->tabMenuAppli, $this->tabSsMenuAppli);

    $this->tabMenuAppli = array();
    $this->tabSsMenuAppli = array();

    // cas de l'acc�s r�serv�
    $strCondProfil = "";
    if( $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE == true ) {
      if( $this->profil_id == "1" )
        $strCondProfil = " and c.CONT_RESERVE=0 ";
    }
   
    if( !is_array($tabAppliMenu) ) $tabAppliMenu = array();

    $dsAppli = $this->oQuery->GetDs_listeAppliAgentByEspace($this->agent_id, $this->cont_id, $strCondProfil);
    $cpt = 0;
    while( $drAppli = $dsAppli->GetRowIter() ) {
      $cpt++;
      $_appli_id = $drAppli->getValueName("APPLI_ID");
      $_strUrlAppli = $drAppli->getValueName("ATYPE_URL_SITE");
      $_idType = $drAppli->getValueName("ATYPE_ID");
      $_strAppliIntitule = $drAppli->getValueName("APPLI_INTITULE");
      
      //appli prend la main
      $this->tabMenuAppli[$cpt] =
          array("url"  => ( $this->_testUrlJs($_strUrlAppli)
                            ? $this->_getUrlJS($_strUrlAppli, $this->cont_id, $_appli_id)
                            : $_strUrlAppli),
                "titre"  => $this->_tronqueItemMenu($_strAppliIntitule),
                "id_type"  => $_idType,
                "id_appli"  => $_appli_id,
                "nbFils" => 0);
     }

    return array($this->tabMenuAppli, $this->tabSsMenuAppli);
  }
  
  /**
   * @brief Retourne une propri�t� de l'application
   *
   * @param atype_id  Type de l'application recherch�e si application transverse aux espaces
   * @return Retourne un tableau
   */
  function setAppliProperty($atype_id="")
  {
    if( is_array($this->tabAppli) && count($this->tabAppli)>0 ) return $this->tabAppli;
    
    $this->tabAppli = array();

    // cas de l'acc�s r�serv�
    $strCondProfil = "";
    if( $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE == true ) {
      if( $this->profil_id == "1" )
        $strCondProfil = " and c.CONT_RESERVE=0 ";
    }

    if( $atype_id != "" ) {
      // recherche les infos par le type de l'application : cas annuaire
      $dsAppli = $this->oQuery->GetDs_listeTypeAppliByListId($atype_id);
      if( $drAppli = $dsAppli->GetRowIter() )
        $this->tabAppli = array("APPLI_INTITULE"   => $drAppli->getValueName("ATYPE_INTITULE"),
                                "APPLI_DEFAUT"     => "0",
                                "ATYPE_ID"         => $drAppli->getValueName("ATYPE_ID"),
                                "ATYPE_URL"        => $drAppli->getValueName("ATYPE_URL_ADMIN"),
                                "ATYPE_LOGO"       => $drAppli->getValueName("ATYPE_ADMIN_LOGO"),
                                "ATYPE_URL_ADMIN"  => $drAppli->getValueName("ATYPE_URL_ADMIN"),
                                "ATYPE_ADMIN_LOGO" => $drAppli->getValueName("ATYPE_ADMIN_LOGO"));
    } else {
      if( $this->appli_id != "-1" ) {
        // recherche les infos sur l'application
        $dsAppli = $this->oQuery->GetDs_ficheEspaceAppliById($this->cont_id, $this->appli_id, $strCondProfil);
        if( $drAppli = $dsAppli->GetRowIter() )
          $this->tabAppli = array("APPLI_INTITULE"   => $drAppli->getValueName("APPLI_INTITULE"),
                                  "APPLI_DEFAUT"     => $drAppli->getValueName("DEFAUT"),
                                  "ATYPE_ID"         => $drAppli->getValueName("ATYPE_ID"),
                                  "ATYPE_URL"        => $drAppli->getValueName("ATYPE_URL_ADMIN"),
                                  "ATYPE_LOGO"       => $drAppli->getValueName("ATYPE_ADMIN_LOGO"),
                                  "ATYPE_URL_ADMIN"  => $drAppli->getValueName("ATYPE_URL_ADMIN"),
                                  "ATYPE_ADMIN_LOGO" => $drAppli->getValueName("ATYPE_ADMIN_LOGO"));
      }
    }
    
    if( count($this->tabAppli) == 0 )
      $this->tabAppli = array("APPLI_INTITULE"   => "",
                              "APPLI_DEFAUT"     => "",
                              "ATYPE_ID"         => "",
                              "ATYPE_URL"        => "",
                              "ATYPE_LOGO"       => "",
                              "ATYPE_URL_ADMIN"  => "",
                              "ATYPE_ADMIN_LOGO" => "");

    return $this->tabAppli;
  }

  /**
   * @brief Retourne une propri�t� de l'application
   *
   * @param strPropertyName Nom de la propri�t�
   * @return Retourne une chaine
   */
  function getAppliProperty($strPropertyName)
  {
    if( is_array($this->tabAppli) && array_key_exists($strPropertyName, $this->tabAppli) )
      return $this->tabAppli[$strPropertyName];
    return "";
  }

  /**
   * @brief Retourne le tableau des �lts de menu : liste des espaces et ss-espaces
   *
   * @return Retourne un tableau contenant le couple (tableau espace fils, tableau espace petit fils)
   */
  function getTabMenuEspace($ALK_SIALKE_URL, $ALK_DIR_ESPACE, $ALK_PATH_UPLOAD_SPACE_LOGO, $strDefaultTarget="")
  {
    if( is_array($this->tabMenuEspace) && count($this->tabMenuEspace)>0 )
      return array($this->tabMenuEspace, $this->tabSsMenuEspace);
    
    $this->tabMenuEspace = array();
    $this->tabSsMenuEspace = array();

    // cas de l'acc�s r�serv�
    $strCondProfil = "";
    if( $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE == true ) {
      if( $this->profil_id == "1" )
        $strCondProfil = " and c.CONT_RESERVE=0 ";
    }
    
    // selection des espaces fils accessibles avec d�termination de l'appli par defaut
    $cpt=0;
    $dsCont = $this->oQuery->GetDs_listeSousEspaceByAgent($this->agent_id, $this->cont_id, $strCondProfil);
    while( $drCont = $dsCont->GetRowIter() ) {
      $cpt++;
      $_cont_id         = $drCont->getValueName("CONT_ID");
      $_iAppliDefaut    = $drCont->getValueName("DEFAUT");
      $_appli_id        = $drCont->getValueName("APPLI_ID");
      $_strUrlAppli     = $drCont->getValueName("ATYPE_URL_SITE");
      $_strTargetAppli  = $drCont->getValueName("ATYPE_URL_TARGET");
      $_strContIntitule = $drCont->getValueName("CONT_INTITULE");
      $_iContPublic     = $drCont->getValueName("CONT_PUBLIC");

      if( $_iAppliDefaut == 1 ) {
        if( $this->_testUrlJs($_strUrlAppli) ) {
          $strUrl = $this->_getUrlJS($_strUrlAppli, $_cont_id, $_appli_id, "'".$_strTargetAppli."'");
          $strUrlTab = $strUrl;
          $strTargetTab = $_strTargetAppli;
        } else {
          $strUrl = $_strUrlAppli."?cont_appli_id=".$_appli_id;
          $strUrlTab = ( substr($strUrl, 0, 4) != "" 
                         ? $ALK_SIALKE_URL."scripts/".$strUrl."&cont_id=".$_cont_id
                         : $strUrl."&cont_id=".$_cont_id );
          $strTargetTab = $_strTargetAppli;
        }
      } else {
        $strUrl = "site/01_extranet_acc.php?cont_appli_id=0";
        $strUrlTab = $ALK_SIALKE_URL."scripts/".$strUrl."&cont_id=".$_cont_id;
        $strTargetTab = $strDefaultTarget;
      }

      $this->tabMenuEspace[$cpt] =
        array("url"    => $strUrlTab,
              "target" => $strTargetTab,
              "titre"  => $this->_tronqueItemMenu($_strContIntitule),
              "public" => $_iContPublic,
              "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO.($_iContPublic==1 ? "cont_public.gif" : "cont_prive.gif"));
    
      // select des conteneurs fils visible de l'utilisateur avec
      // d�termination de l'appli par defaut
      $cptFils = 0;
      $dsContFils = $this->oQuery->GetDs_listeSousEspaceByAgent($this->agent_id, $_cont_id, $strCondProfil);
      while( $drContFils = $dsContFils->GetRowIter() ) {
        $cptFils++;
        $__cont_id         = $drContFils->getValueName("CONT_ID");
        $__iAppliDefaut    = $drContFils->getValueName("DEFAUT");
        $__appli_id        = $drContFils->getValueName("APPLI_ID");
        $__strUrlAppli     = $drContFils->getValueName("ATYPE_URL_SITE");
        $__strTargetAppli  = $drContFils->getValueName("ATYPE_URL_TARGET");
        $__strContIntitule = $drContFils->getValueName("CONT_INTITULE");
        $__iContPublic     = $drContFils->getValueName("CONT_PUBLIC");

        $this->tabSsMenuEspace[$cpt][$cptFils] =
          array("url"  => ( $__iAppliDefaut==0 
                            ? $ALK_SIALKE_URL."scripts/site/01_extranet_acc.php?cont_id=".$__cont_id
                            : ( $this->_testUrlJs($__strUrlAppli) 
                                ? $this->_getUrlJS($__strUrlAppli, $__cont_id, $__appli_id)
                                : ( substr($__strUrlAppli, 0, 4) != "http"
                                    ? $ALK_SIALKE_URL."scripts/".$__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id
                                    : $__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id ))),
                "target" => $__strTargetAppli,
                "titre"  => $this->_tronqueItemSsMenu($__strContIntitule),
                "public" => $__iContPublic,
                "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO.($__iContPublic==1 ? "cont_public.gif" : "cont_prive.gif"));
      }
    
      $this->tabMenuEspace[$cpt]["nbFils"] = $cptFils;
    }

    //-------------------------------------------------
    // Recherche les espaces non connect�s directement
    //-------------------------------------------------

    // liste des espaces dont le pere n'est pas accessible
    $dsCont = $this->oQuery->GetDs_listeSousEspaceOrphelinsByAgent($this->agent_id, $this->cont_id, $strCondProfil);
    if( $drCont = $dsCont->GetRowIter()) {
      $cpt++;
      $this->tabMenuEspace[$cpt] =  
        array("url"    => "#",
              "target" => "",
              "titre"  => "Autres espaces",
              "public" => "1",
              "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO."cont_public.gif");
      $cptFils = 0;
      while( $drCont ) {
        $cptFils++;
        $__cont_id         = $drCont->getValueName("CONT_ID");
        $__iAppliDefaut    = $drCont->getValueName("DEFAUT");
        $__appli_id        = $drCont->getValueName("APPLI_ID");
        $__atype_id        = $drCont->getValueName("ATYPE_ID");
        $__strUrlAppli     = $drCont->getValueName("ATYPE_URL_SITE");
        $__strTargetAppli  = $drCont->getValueName("ATYPE_URL_TARGET");
        $__strContIntitule = $drCont->getValueName("CONT_INTITULE");
        $__iContPublic     = $drCont->getValueName("CONT_PUBLIC");

        $this->tabSsMenuEspace[$cpt][$cptFils] =
          array("url"  => ( $__iAppliDefaut==0 
                            ? $ALK_SIALKE_URL."scripts/site/01_extranet_acc.php?cont_id=".$__cont_id                              
                            : (  $this->_testUrlJs($__strUrlAppli) 
                                 ?  $this->_getUrlJS($__strUrlAppli, $__cont_id, $__appli_id)
                                 : ( substr($__strUrlAppli, 0, 4) != "http"
                                     ? $ALK_SIALKE_URL."scripts/".$__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id
                                     : $__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id ))),
                "target" => $__strTargetAppli,
                "titre"  => $this->_tronqueItemSsMenu($__strContIntitule),
                "public" => $__iContPublic,
                "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO.($__iContPublic==1 ? "cont_public.gif" : "cont_prive.gif"));
        $drCont = $dsCont->GetRowIter();
      }
      $this->tabMenuEspace[$cpt]["nbFils"] = $cptFils;
    }
   
    return array($this->tabMenuEspace, $this->tabSsMenuEspace);
  }

  /**
   * @brief Retourne le tableau des �lts de menu : liste des espaces et ss-espaces
   *
   * @return Retourne un tableau contenant le couple (tableau espace fr�res, tableau espace fils)
   */
  function getTabMenuEspaceFrere($ALK_SIALKE_URL, $ALK_DIR_ESPACE, $ALK_PATH_UPLOAD_SPACE_LOGO, $strDefaultTarget="")
  {
    if( is_array($this->tabMenuEspaceFrere) && count($this->tabMenuEspaceFrere)>0 )
      return array($this->tabMenuEspaceFrere, $this->tabSsMenuEspaceFrere);
    
    $this->tabMenuEspaceFrere = array();
    $this->tabSsMenuEspaceFrere = array();

    // cas de l'acc�s r�serv�
    $strCondProfil = "";
    if( $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE == true ) {
      if( $this->profil_id == "1" )
        $strCondProfil = " and c.CONT_RESERVE=0 ";
    }
    
    $dsCont = $this->oQuery->GetDs_ficheEspaceById($this->cont_id);
    if( $drCont = $dsCont->GetRowIter() ) {
      $idContPere = $drCont->getValueName("CONT_PERE");
    }
    
    // selection des espaces fils accessibles avec d�termination de l'appli par defaut
    $cpt=0;
    $dsCont = $this->oQuery->GetDs_listeSousEspaceByAgent($this->agent_id, $idContPere, $strCondProfil);
    while( $drCont = $dsCont->GetRowIter() ) {
      $cpt++;
      $_cont_id = $drCont->getValueName("CONT_ID");
      $_iAppliDefaut = $drCont->getValueName("DEFAUT");
      $_appli_id = $drCont->getValueName("APPLI_ID");
      $_strUrlAppli = $drCont->getValueName("ATYPE_URL_SITE");
      $_strTargetAppli = $drCont->getValueName("ATYPE_URL_TARGET");
      $_strContIntitule = $drCont->getValueName("CONT_INTITULE");
      $_iContPublic = $drCont->getValueName("CONT_PUBLIC");

      if( $_iAppliDefaut == 1 ) {
        if( $this->_testUrlJs($_strUrlAppli) ) {
          $strUrl = $this->_getUrlJS($_strUrlAppli, $_cont_id, $_appli_id, "'".$_strTargetAppli."'");
          $strUrlTab = $strUrl;
          $strTargetTab = $_strTargetAppli;
        } else {
          $strUrl = $_strUrlAppli."?cont_appli_id=".$_appli_id;
          $strUrlTab = ( substr($strUrl, 0, 4) != "http"
                         ? $ALK_SIALKE_URL."scripts/".$strUrl."&cont_id=".$_cont_id
                         : $strUrl."&cont_id=".$_cont_id );
          $strTargetTab = $_strTargetAppli;
        }
      } else {
        $strUrl = "site/01_extranet_acc.php?cont_appli_id=0";
        $strUrlTab = $ALK_SIALKE_URL."scripts/".$strUrl."&cont_id=".$_cont_id;
        $strTargetTab = $strDefaultTarget;
      }

      $this->tabMenuEspaceFrere[$cpt] =
        array("url"    => $strUrlTab,
              "target" => $strTargetTab,
              "titre"  => $this->_tronqueItemMenu($_strContIntitule),
              "public" => $_iContPublic,
              "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO.($_iContPublic==1 ? "cont_public.gif" : "cont_prive.gif"));
    
      // select des conteneurs fils visible de l'utilisateur avec
      // d�termination de l'appli par defaut
      $cptFils = 0;
      $dsContFils = $this->oQuery->GetDs_listeSousEspaceByAgent($this->agent_id, $_cont_id, $strCondProfil);
      while( $drContFils = $dsContFils->GetRowIter() ) {
        $cptFils++;
        $__cont_id = $drContFils->getValueName("CONT_ID");
        $__iAppliDefaut = $drContFils->getValueName("DEFAUT");
        $__appli_id = $drContFils->getValueName("APPLI_ID");
        $__strUrlAppli = $drContFils->getValueName("ATYPE_URL_SITE");
        $__strTargetAppli = $drContFils->getValueName("ATYPE_URL_TARGET");
        $__strContIntitule = $drContFils->getValueName("CONT_INTITULE");
        $__iContPublic     = $drCont->getValueName("CONT_PUBLIC");

        $this->tabSsMenuEspaceFrere[$cpt][$cptFils] =
          array("url"  => ( $__iAppliDefaut==0 
                            ? $ALK_SIALKE_URL."scripts/site/01_extranet_acc.php?cont_id=".$__cont_id
                            : ( $this->_testUrlJs($__strUrlAppli) 
                                ? $this->_getUrlJS($__strUrlAppli, $__cont_id, $__appli_id)
                                : ( substr($__strUrlAppli, 0, 4) != "http"
                                    ? $ALK_SIALKE_URL."scripts/".$__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id
                                    : $__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id))),
                "target" => $__strTargetAppli,
                "titre"  => $this->_tronqueItemSsMenu($__strContIntitule),
                "public" => $__iContPublic,
                "logo"   => "");
      }
    
      $this->tabMenuEspaceFrere[$cpt]["nbFils"] = $cptFils;
    }

    //-------------------------------------------------
    // Recherche les espaces non connect�s directement
    //-------------------------------------------------

    // liste des espaces dont le pere n'est pas accessible
    $dsCont = $this->oQuery->GetDs_listeSousEspaceOrphelinsByAgent($this->agent_id, $this->cont_id, $strCondProfil);
    if( $drCont = $dsCont->GetRowIter()) {
      $cpt++;
      $this->tabMenuEspaceFrere[$cpt] =  
        array("url"    => "#",
              "target" => "",
              "titre"  => "Autres espaces",
              "public" => "1",
              "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO."cont_public.gif");
      $cptFils = 0;
      while( $drCont ) {
        $cptFils++;
        $__cont_id = $drCont->getValueName("CONT_ID");
        $__iAppliDefaut = $drCont->getValueName("DEFAUT");
        $__appli_id = $drCont->getValueName("APPLI_ID");
        $__strUrlAppli = $drCont->getValueName("ATYPE_URL");
        $__strTargetAppli = $drCont->getValueName("ATYPE_URL_TARGET");
        $__strContIntitule = $drCont->getValueName("CONT_INTITULE");
        $__iContPublic     = $drCont->getValueName("CONT_PUBLIC");

        $this->tabSsMenuEspaceFrere[$cpt][$cptFils] =
          array("url"  => ( $__iAppliDefaut==0 
                            ? ( defined("ALK_B_ACCUEIL_SPACE") && ALK_B_ACCUEIL_SPACE==true
                                ? $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."00_accueil.php?cont_id=".$__cont_id
                                : $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."sommaire.php?cont_id=".$__cont_id )
                            : (  $this->_testUrlJs($__strUrlAppli) 
                                 ?  $this->_getUrlJS($__strUrlAppli, $__cont_id, $__appli_id)
                                 : ( substr($__strUrlAppli, 0, 4) != "http"
                                     ? $ALK_SIALKE_URL."scripts/".$__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id
                                     : $__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id))),
                "target" => $__strTargetAppli,
                "titre"  => $this->_tronqueItemSsMenu($__strContIntitule),
                "public" => $__iContPublic,
                "logo"   => "");
        $drCont = $dsCont->GetRowIter();
      }
      $this->tabMenuEspaceFrere[$cpt]["nbFils"] = $cptFils;
    }
   
    return array($this->tabMenuEspaceFrere, $this->tabSsMenuEspaceFrere);
  }

/**
   * @brief Test si l'url est un appel a une fonction javascript
   *
   * @param strUrl url a tester
   * @return Retourne vrai si l'url est un appel a une fonction javascript, faux sinon
   */
  function _testUrlJs($strUrl)
  {
    if( $strUrl == "" )
      return false;
    if( strtolower(substr($strUrl, 0, 10)) == "javascript" )
      return true;
    return false;
  }


  /**
   * @brief ajoute les param�tres php � la fonction javascript
   *
   * @param strNomJS Nom de la fonction javascript
   * @param N parametres optionnels qui seront ajoutes a la fonction php
   * @return La chaine de caract�res contenant l'appel complet de la fonction javascript
   */
  function _getUrlJs($strNomJS)
  {
    $strParam = "";
    $numargs = func_num_args();
    for($i=1; $i<$numargs; $i++)
      if( $strParam == "" )
        $strParam = func_get_arg($i);
      else
        $strParam .= ",".func_get_arg($i);
    
    $strTmp = str_replace("()", "($strParam)", $strNomJS);
    
    return $strTmp;
  }
  
/**
   * @brief Tronque l'intitul� du menu sur 2 lignes de 15 caract�res
   *
   * @param strItemMenu Intitul� du menu
   * @return Retourne l'intitul� correctement format�
   */
  function _tronqueItemMenu($strItemMenu)
  {
    // effectue des cesures tous les 15 caract�res
    $strItemMenu = wordwrap($strItemMenu, 32, "<br>", 1);

    // coupe la chaine avant le second <br>
    $tabStrMenu = explode("<br>", $strItemMenu);
    $strRes = $tabStrMenu[0];
    /*if( count($tabStrMenu) >= 2 )
      $strRes .= "<br>".$tabStrMenu[1];
    */
    return $strRes;
  }

  /**
   * @brief Tronque l'intitul� du sous-menu sur 1 ligne de 32 caract�res
   *
   * @param strItemMenu Intitul� du sous-menu
   * @return Retourne l'intitul� correctement format�
   */
  function _tronqueItemSsMenu($strItemMenu)
  {
    // effectue des cesures tous les 15 caract�res
    $strItemMenu = wordwrap($strItemMenu, 32, "<br>", 1);
    
    // coupe la chaine avant le second <br>
    $tabStrMenu = explode("<br>", $strItemMenu);
    $strRes = $tabStrMenu[0];
    //if( count($tabStrMenu) >= 2 )
    //  $strRes .= "<br>".$tabStrMenu[1];
    return $strRes;
  }
  
  /**
   * @brief Retourne un tableau de valeurs n�cessaires � l'affichage du menu espace
   *
   * @param strUrlAccueil  Url de la page accueil si d�j� calcul�e
   * @return Retourne un tableau
   */
  function getTabUrl($ALK_SIALKE_URL, $ALK_DIR_ESPACE,$ALK_DIR_ANNU, 
                     $ALK_SI_MAIL, $ALK_URL_APPLI, $ALK_URL_ON_LOGO, $strUrlAccueil, $strTargetAccueil="")
  {
    if( is_array($this->tabUrl) && count($this->tabUrl)>0 ) return $this->tabUrl;

    // cas de l'acc�s r�serv�
    $strCondProfil = "";
    if( $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE == true ) {
      if( $this->profil_id == "1" )
        $strCondProfil = " and c.CONT_RESERVE=0 ";
    }

    $strUrlAcc = "";
    $strTargetAcc = $strTargetAccueil;
    if( $strUrlAccueil == "" ) {
      // page d'accueil
      // selection du conteneur de niveau de plus faible auquel l'utilisateur � droit de connexion
      $dsCont = $this->oQuery->GetDs_appliDefautByAgent($this->agent_id, "", $strCondProfil);
      if( $drCont = $dsCont->GetRowIter() ) {
        $_iAppliDefaut = $drCont->getValueName("DEFAUT");
        $_strUrlAppli = $drCont->getValueName("ATYPE_URL");
        $_strUrlTargetAppli =  $drCont->getValueName("ATYPE_URL_TARGET");
        $_cont_id = $drCont->getValueName("CONT_ID");
        $_appli_id = $drCont->getValueName("APPLI_ID");
        
        if( $_iAppliDefaut == 1 ) {
          if( $this->_testUrlJs($_strUrlAppli) )
            $strUrlAcc = $this->_getUrlJS($_strUrlAppli, $_cont_id, $_appli_id, "'".$_strUrlTargetAppli."'");
          else {
            $strUrlAcc = ( substr($_strUrlAppli, 0, 4) != "http"
                           ? $ALK_SIALKE_URL."scripts/".$_strUrlAppli."?cont_id=".$_cont_id."&cont_appli_id=".$_appli_id
                           : $_strUrlAppli."?cont_id=".$_cont_id."&cont_appli_id=".$_appli_id );
          }
        } else {
          $strUrlAcc = ( defined("ALK_B_ACCUEIL_SPACE") && ALK_B_ACCUEIL_SPACE==true
                         ? $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."00_accueil.php?cont_id=".$_cont_id
                         : $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."sommaire.php?cont_id=".$_cont_id );
          
          $_strUrlTargetAppli = $strTargetAccueil;
        }
      }
    } else {
      $strUrlAcc = $strUrlAccueil;
    }
  
    // nom de l'agent
    $agent_nom = "";
    $agent_prenom = "";
    $agent_login = "";
    $dsAgent = $this->oQueryAnnu->GetDs_ficheAgent($this->agent_id);
    if( $drAgent = $dsAgent->GetRowIter() ) {
      $agent_nom    = $drAgent->getValueName("AGENT_NOM");
      $agent_prenom = $drAgent->getValueName("AGENT_PRENOM");
      $agent_login  = $drAgent->getValueName("AGENT_LOGIN");
    }

    // autres url utiles
    $strUrlAdmin = "";
    if( $this->iProfilCont <= 2 )
      $strUrlAdmin = $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."04_conteneur_form.php?cont_id=".$this->cont_id;

    $strUrlPlan =  $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."sommaire.php?cont_id=".$this->cont_id;
    $strUrlAnnuaire = $ALK_URL_APPLI."?cont_id=".$this->cont_id;
    $strUrlOnLogo = ( $ALK_URL_ON_LOGO == "" ? $strUrlAcc : $ALK_URL_ON_LOGO );

    $this->tabUrl = array("logo"      => $strUrlOnLogo,
                          "accueil"   => $strUrlAcc,
                          "targetAcc" => $strTargetAcc,
                          "admin"     => $strUrlAdmin,
                          "plan"      => $strUrlPlan,
                          "annuaire"  => $strUrlAnnuaire,
                          "agent"     => array("nom"    => $agent_nom,
                                               "prenom" => $agent_prenom,
                                               "login"  => $agent_login));

    return $this->tabUrl;
  }
  
}
?>