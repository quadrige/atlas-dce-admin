<?php

/**
 *  Classe abstraite
 *
 * Classe permettant d'obtenir des r�f�rences vers les applications Alkanet g�n�rique
 * Cette classe ne doit pas �tre directement instanci�e. 
 * 
 */
class AlkAppliFactory extends AlkObject
{
  /** R�f�rence vers l'objet espace de travail qui contient cette application */
  var $oSpace;

  /** Ensemble des applis instancies sur la page 
   *  Gestion des index : 
   *   - les applis cr��es � partir de atype_id : indice = _[atype_id]
   *   - les applis cr��es � partir de appli_id : indice = __[appli_id]
   */
  var $tabAppli;

  /**
   * @brief Constructeur par d�faut
   *
   * @param oSpace R�f�rence vers l'objet de l'espace en cours
   * @param 
   */
  function AlkAppliFactoryBase(&$oSpace)
  {
    parent::AlkObject();

    $this->oSpace =& $oSpace;
    $this->tabAppli = array();
  }

  /**
   * @brief Retourne une r�f�rence sur une appli
   *
   * @param oAppli    R�f�rence vers l'appli cr��e
   * @param atype_id  Type de l'appli � cr�er
   * @param appli_id  Identifiant de l'appli � cr�er (=-1 par d�faut)
   */
  function GetAppli(&$oAppli, $atype_id, $appli_id=-1)
  {
    $oAppli = null;

    $strKeyType  = "_".$atype_id;
    $strKeyAppli = "__".$appli_id;
    $strKey = ( $appli_id == -1 ? $strKeyType : $strKeyAppli );

    if( array_key_exists($strKey, $this->tabAppli) ) {
      $oAppli =& $this->tabAppli[$strKey];
      return;
    }

    $this->SetAppli($atype_id, $appli_id, $strKey);
    
    if( array_key_exists($strKey, $this->tabAppli) ) {
      $oAppli =& $this->tabAppli[$strKey];
    }
  }

  /**
   * @brief M�thde virtuelle contenant les applis g�n�riques Alkanet possibles d'instancier sur le projet
   *        Dans le cas d'appli sp�cifique � un client, il faut les d�clarer dans la m�thode SetAppli de la classe fille
   *        AlkFactory
   *
   * @param atype_id  Type de l'appli � cr�er
   * @param appli_id  Identifiant de l'appli � cr�er (=-1 par d�faut)
   * @param strKe     Cl� du tableau tabAppli
   */
  function SetAppli($atype_id, $appli_id, $strKey)
  {
    $agent_id = $_SESSION["sit_idUser"];

    switch( $atype_id ) {
    case ALK_ATYPE_ID_ANNU:
      if( defined("ALK_B_APPLI_ANNU") && ALK_B_APPLI_ANNU==true ) {
        include_once("../".ALK_DIR_ANNU."classes/alkappliannu.class.php");
        $this->tabAppli[$strKey] = new AlkAppliAnnu($this->oSpace, 0, $agent_id,  
                                                    ALK_SIALKE_URL.ALK_PATH_UPLOAD_ANNU,
                                                    ALK_SIALKE_PATH.ALK_PATH_UPLOAD_ANNU,
                                                    $this);
      }
      break;

    case 2:
    case ALK_ATYPE_ID_FDOC: // Fonds documentaires
      if( defined("ALK_B_APPLI_FDOC") && ALK_B_APPLI_FDOC == true ) {
        include_once("../fdoc_01/classes/alkapplifdoc.class.php");
        $this->tabAppli[$strKey] = new AlkAppliFDoc($this->oSpace, $appli_id, $agent_id,  
                                                    ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC_IMG,
                                                    ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_IMG,
                                                    $this);
      }
      break;

    case ALK_ATYPE_ID_ACTUSIT: // Actualit�s
      if( defined("ALK_B_APPLI_ACTU") && ALK_B_APPLI_ACTU == true ) {
        include_once("../actu_01/classes/alkappliactu.class.php");
        $this->tabAppli[$strKey] = new AlkAppliActu01($this->oSpace, $appli_id, $agent_id,
                                                      ALK_SIALKE_URL.ALK_PATH_UPLOAD_ACTU_LOGO, 
                                                      ALK_SIALKE_PATH.ALK_PATH_UPLOAD_ACTU_LOGO,
                                                      $this);
      }
      break;

    case ALK_ATYPE_ID_AGENDA: // Agenda
      if( defined("ALK_B_APPLI_AGENDA") && ALK_B_APPLI_AGENDA == true ) {
        include_once("../agenda_01/classes/alkappliagenda.class.php");
        $this->tabAppli[$strKey] = new AlkAppliAgenda($this->oSpace, $appli_id, $agent_id,
                                                      ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC_AGENDA, 
                                                      ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_AGENDA,
                                                      $this);
      }
      break;

    case ALK_ATYPE_ID_LSDIF: // Liste de diffusion
      if( defined("ALK_B_APPLI_LSDIF") && ALK_B_APPLI_LSDIF == true ) {
        include_once("../lsdif_01/classes/alkapplilsdif.class.php");
        $this->tabAppli[$strKey] = new AlkAppliLsDif($this->oSpace, $appli_id, $agent_id,
                                                     ALK_SIALKE_URL.ALK_PATH_UPLOAD_LS_DIF_PJ, 
                                                     ALK_SIALKE_PATH.ALK_PATH_UPLOAD_LS_DIF_PJ,
                                                     $this);
      }
      break;

    case ALK_ATYPE_ID_FORUM: // Forum
      if( defined("ALK_B_APPLI_FORUM") && ALK_B_APPLI_FORUM == true ) {
        include_once("../forum_01/classes/alkappliforum.class.php");
        $this->tabAppli[$strKey] = new AlkAppliForum($this->oSpace, $appli_id, $agent_id,
                                                     ALK_SIALKE_URL.ALK_PATH_UPLOAD_FORUM_IMG, 
                                                     ALK_SIALKE_PATH.ALK_PATH_UPLOAD_FORUM_IMG,
                                                     $this);
      }
      break;

    case ALK_ATYPE_ID_CARTO: // SIG
      if( defined("ALK_B_APPLI_SIG") && ALK_B_APPLI_SIG == true ) {
        include_once("../sig_admin/classes/alkapplisig.class.phtml");
        $this->tabAppli[$strKey] = new AlkAppliSig($this->oSpace, $appli_id, $agent_id,
                                                   ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC_SIG, 
                                                   ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_SIG,
                                                   $this);
      }
      break;

    case ALK_ATYPE_ID_FAVORIS: // Favoris
      if( defined("ALK_B_APPLI_FAVORIS") && ALK_B_APPLI_FAVORIS == true ) {
        include_once("../bookmark_01/classes/alkapplifavoris.class.php");
        $this->tabAppli[$strKey] = new AlkAppliFavoris($this->oSpace, $appli_id, $agent_id, "", "", $this);
      }
      break;
    
    case ALK_ATYPE_ID_BEF: // Bourse d'�changes'
      if( defined("ALK_B_APPLI_BEF") && ALK_B_APPLI_BEF == true ) {
        include_once("../bef/classes/alkapplibef.class.php");
        $this->tabAppli[$strKey] = new AlkAppliBef($this->oSpace, $appli_id, $agent_id, "", "", $this);
      }
      break;  
    case ALK_ATYPE_ID_ACTU: // Actualit�s
      if( defined("ALK_B_APPLI_ACTUSITE") && ALK_B_APPLI_ACTUSITE == true ) {
        include_once("../actu/classes/alkappliactu.class.php");
        $this->tabAppli[$strKey] = new AlkAppliActu($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;

    case ALK_ATYPE_ID_LIEN: // Liens utiles
      if( defined("ALK_B_APPLI_LIEN") && ALK_B_APPLI_LIEN == true ) {
        include_once("../lien/classes/alkapplilien.class.php");
        $this->tabAppli[$strKey] = new AlkAppliLien($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case ALK_ATYPE_ID_GEDIT: // Gestion �ditoriale
      if( defined("ALK_B_APPLI_GEDIT") && ALK_B_APPLI_GEDIT == true ) {
        include_once("../gedit/classes/alkappligedit.class.php");
        $this->tabAppli[$strKey] = new AlkAppliGEdit($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case ALK_ATYPE_ID_GLOS: // Glossaire
      if( defined("ALK_B_APPLI_GLOS") && ALK_B_APPLI_GLOS == true ) {
        include_once("../glos/classes/alkappliglos.class.php");
        $this->tabAppli[$strKey] = new AlkAppliGlos($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;  
      
    case ALK_ATYPE_ID_FAQS: // Faqs
      if( defined("ALK_B_APPLI_FAQS") && ALK_B_APPLI_FAQS == true ) {
        include_once("../faqs/classes/alkapplifaq.class.php");
        $this->tabAppli[$strKey] = new AlkAppliFaq($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;

    case ALK_ATYPE_ID_SYND: // Syndication Alkanet
      if( defined("ALK_B_APPLI_SYNDICATION") && ALK_B_APPLI_SYNDICATION == true ) {
        include_once("../synd/classes/alkapplisynd.class.php");
        $this->tabAppli[$strKey] = new AlkAppliSynd($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    
    case ALK_ATYPE_ID_LETTRE: // Abonnement newsletter
      if( defined("ALK_B_GEDIT_LETTRE") && ALK_B_GEDIT_LETTRE == true ) {
        include_once("../newsletter/classes/alkapplinewsletter.class.php");
        $this->tabAppli[$strKey] = new AlkAppliNewsletter($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
   case ALK_ATYPE_ID_TRAD: // Traduction
      if( defined("ALK_B_APPLI_TRADUCTION") && ALK_B_APPLI_TRADUCTION == true ) {
        include_once("../trad/classes/alkapplitrad.class.php");
        $this->tabAppli[$strKey] = new AlkAppliTraduction($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
   case ALK_ATYPE_ID_SIGZA: // sigza
      if( defined("ALK_B_APPLI_SIGZA") && ALK_B_APPLI_SIGZA == true ) {
        include_once("../sigza/classes/alkapplisigza.class.php");
        $this->tabAppli[$strKey] = new AlkAppliSigZa($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;   
      
   case ALK_ATYPE_ID_RECETTE: // Recettes
      if( defined("ALK_B_APPLI_RECETTE") && ALK_B_APPLI_RECETTE == true ) {
        include_once("../recette/classes/alkapplirecette.class.php");
        $this->tabAppli[$strKey] = new AlkAppliRecette($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    
     case ALK_ATYPE_ID_FORM: // Formulaire
      if( defined("ALK_B_APPLI_FORMULAIRE") && ALK_B_APPLI_FORMULAIRE == true ) {
        include_once("../form/classes/alkappliformulaire.class.php");
        $this->tabAppli[$strKey] = new AlkAppliFormulaire($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case ALK_ATYPE_ID_SONDAGE: // Sondage
      if( defined("ALK_B_APPLI_SONDAGE") && ALK_B_APPLI_SONDAGE == true ) {
        include_once("../sondage/classes/alkapplisondage.class.php");
        $this->tabAppli[$strKey] = new AlkAppliSondage($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case ALK_ATYPE_ID_EXPLORER: // explorateur
      if( defined("ALK_B_APPLI_EXPLORER") && ALK_B_APPLI_EXPLORER==true ) {
        include_once("../explorer/classes/alkappliexplorer.class.php");
        $this->tabAppli[$strKey] = new AlkAppliExplorer($this->oSpace, $appli_id, $agent_id, "", "");        
      }

    case ALK_ATYPE_ID_FORMATION: // Formations
      if( defined("ALK_B_APPLI_FORMATION") && ALK_B_APPLI_FORMATION == true ) {
        include_once("../formation/classes/alkappliformation.class.php");
        $this->tabAppli[$strKey] = new AlkAppliFormation($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    
    case ALK_ATYPE_ID_FORUMSITE: // Forum site' .
        if( defined("ALK_B_APPLI_FORUMSITE") && ALK_B_APPLI_FORUMSITE == true ) {
        include_once("../forum/classes/alkappliforum.class.php");
        $this->tabAppli[$strKey] = new AlkAppliForum($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    
    
    case ALK_ATYPE_ID_GCOEDIT: // Edition cooperative' .
        if( defined("ALK_B_APPLI_GCOEDIT") && ALK_B_APPLI_GCOEDIT== true ) {
        include_once("../gcoedit/classes/alkappligcoedit.class.php");
        $this->tabAppli[$strKey] = new AlkAppliGCoEdit($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case ALK_ATYPE_ID_PELERINAGE: // P�lerinage .
        if( defined("ALK_B_APPLI_PELERINAGE") && ALK_B_APPLI_PELERINAGE== true ) {
        include_once("../pelerinage/classes/alkapplipelerinage.class.php");
        $this->tabAppli[$strKey] = new AlkAppliPelerinage($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
         
    case ALK_ATYPE_ID_BILLETTERIE: // Billetterie .
        if( defined("ALK_B_APPLI_BILLETTERIE") && ALK_B_APPLI_BILLETTERIE== true ) {
        include_once("../billetterie/classes/alkapplibilletterie.class.php");
        $this->tabAppli[$strKey] = new AlkAppliBilletterie($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case ALK_ATYPE_ID_FDOC_DIST: // Fond doc distant
      if( defined("ALK_B_APPLI_FDOC_DIST") && ALK_B_APPLI_FDOC_DIST== true ) {
        include_once("../fdoc_distant/classes/alkapplifdocdist.class.php");
        $this->tabAppli[$strKey] = new AlkAppliFDocDist($this->oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    }
  }

  /**
   * @brief Appel d'un service offert par une application
   *
   * @param idServ Num�ro du service demand�
   * @param oRes  Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @return Retourne le r�sultat du service demand�, false si aucun service.
   */
  function CallService($idServ, &$oRes)
  {
    if( $this->oAppli != null ) {
      $tabArg = func_get_args();
      $nbArg = count($tabArg);
      $strArgs = "";
      for($i=2; $i<$nbArg; $i++)
        $strArgs .= ($strArgs=="" ? "" : ", ")."\"".$tabArg[$i]."\"";

      if( $strArgs!="" )
        eval("$"."result = "."$"."this->oAppli->CallService".$idServ."("."$"."oRes, ".$strArgs.");");
      else
        eval("$"."result = "."$"."this->oAppli->CallService".$idServ."("."$"."oRes);");

      return $result;
    }
    return false;
  }

  /**
   * @brief Appel d'un service g�n�rique permettant d'obtenir l'url pour l'ajout d'un lien inter appli
   *
   * @param oRes    non utilis�, permet de retourner une info compl�mentaire
   * @param strForm nom du formulaire de destination
   * @param eltForm nom de l'�l�ment (text ou textarea) du formulaire de destination
   */
  function CallServiceUrlAjoutLienInterAppli(&$oRes, $strForm, $strEltForm)
  {
    $strUrl = ALK_SIALKE_URL."scripts/".ALK_DIR_ESPACE."serv_inter_appli.php?form=".$strForm."&elt=".$strEltForm;
    return $strUrl;
  }
  /**
   * @brief permet d'obtenir l'url d�cod� d'un lien inter appli
   *
   * @param oRes    non utilis�, permet de retourner une info compl�mentaire
   * @param strCode lien cod� (ex ##fdoc_id_texte du lien##)
   * @param strCss  Nom du classe css pour le lien g�n�r�
   */
  function GetHtmlDecodeLienInterAppli(&$oRes, $strCode, $strCss)
  {
    global $queryCont;
    $strHtml = $strCode;

    //Supprime les #
    $strCode = substr($strCode,2,strlen($strHtml)-4);
    $tabCode = explode("_",$strCode,3);

    //Recherche du type d'application demand�e
    $dsTypeAppli = $queryCont->GetDs_ficheTypeAppliByAbrev($tabCode[0]);
    if( $drTypeAppli = $dsTypeAppli->getRowIter() ) {
      $atype_id = $drTypeAppli->getValueName("ATYPE_ID");
      $oService = new AlkServiceAppli($atype_id, $this->oSpace, "-1", $this->agent_id);
      $oRet = null;
      $strTemp = $oService->CallService("HtmlDecodeLienInterAppli", $oRet, 
                                        $tabCode[1], $tabCode[2], $strCss);

      if( $strTemp != "" )
        $strHtml = $strTemp;
    }
    return $strHtml;
  }

  /**
   * @brief Appel d'un service g�n�rique permettant de parcourir et remplacer les �ventuels liens inter-appli
   *        cod�s dans le contenu par [diese][diese]fdoc_id_texte du lien[diese][diese]
   *        renvoie le contenu html apr�s avoir effectu� les remplacements n�cessaires
   * 
   * @param oRes       non utilis�, permet de retourner une info complementaire
   * @param strContenu contenu html � parcourir
   * @param strCss     classe Css � appliquer sur les liens transform�s
   */
  function CallServiceHtmlReplaceLienInterAppli(&$oRes, $strContenu, $strCss)
  {
    $strHtml = $strContenu;
    preg_match_all("(##.[^##_]*_.[^##_]*_.[^##]*##)", $strHtml, $tabTemp, PREG_PATTERN_ORDER);
    foreach($tabTemp as $tabCode) {
      foreach ($tabCode as $strCode) {
        $oRes = null;
        $strHtml = str_replace($strCode, 
                               $this->GetHtmlDecodeLienInterAppli($oRes, $strCode, $strCss), 
                               $strHtml);
      }
    }
    return $strHtml;
  }

  /**
   * @brief Appel d'un service g�n�rique permettant d'obtenir le code html de la page de s�lection
   *        d'un type d'appli pour l'insertion d'un lien r�f�rence vers une appli
   *
   * @param strForm nom du formulaire de destination
   * @param eltForm nom de l'�l�ment (text ou textarea) du formulaire de destination
   */
  function CallServiceHtmlAjoutLienInterAppli(&$oRes, &$queryCont, $strForm, $strEltForm, $strAction)
  {
    $strTmp = substr(ALK_S_TYPEAPPLI_INTER_APPLI,0,strlen(ALK_S_TYPEAPPLI_INTER_APPLI)-1);
    $dsTypeAppli = $queryCont->GetDs_listeTypeAppliByListId($strTmp);
    $iLarg = "606";

    $strHtml = "<form name='formTypeAppli' action=\"".$strAction."?form=".
      $strForm."&elt=".$strEltForm."\" method=post>".
      getHtmlHeaderPage(false, "Ajouter un lien", $iLarg).
      getHtmlLineFrame($iLarg).
      getHtmlHeaderFrame($iLarg, 15);
      getHtmlTableCtrlHeader();

    if( $dsTypeAppli->iCountTotDr>0 && strlen(ALK_S_TYPEAPPLI_INTER_APPLI)-1 > 0 ) {
      $oCtrlSelect = new HtmlSelect(0, "atype_id", "", "S�lectionnez un type d'application");
      $oCtrlSelect->oValTxt = $dsTypeAppli;
      $strHtml .= getHtmlTableCtrlLine($oCtrlSelect->label, $oCtrlSelect->getHtml()).
        getHtmlTableCtrlSpaceLine(10);

      $oBtV = new HtmlLink("javascript:Valider('formTypeAppli');", "Valider",
                           "valid_gen.gif", "valid_gen_rol.gif");
      $oBtA = new HtmlLink("javascript:window.close();", "Annuler", 
                           "annul_gen.gif", "annul_gen_rol.gif");
      $strLabel = "";
      $strCtrl = $oBtV->getHtml()."&nbsp;&nbsp;".$oBtA->getHtml();
    } else {
      $strLabel = "";
      $strCtrl = "Aucune r�f�rence n'est possible.";
    }

    $strHtml .= getHtmlTableCtrlLine($strLabel, $strCtrl).
      getHtmlTableCtrlSpaceLine(10).
      getHtmlTableCtrlFooter().
      getHtmlLineFrame($iLarg).
      getHtmlFooterFrame(10).
      getHtmlFooterPage().
      "</form>";

    return $strHtml;
  }
}
?>