<?php

/**
 * @brief Classe de base pour les classes query et queryAction
 */
class AlkQuery extends AlkObject
{
  /** Classe de connexion � la base */
  var $dbConn;

  /** r�f�rences vers les query n�cessaires � l'indexation */
  var $oQuerySearch;
  var $oQuerySearchAction;

  /**
   * @brief Constructeur par d�faut
   *
   * @param dbConn    objet contenant la connexion ouverte vers la base
   * @param tabLangue tableau des langues utilis�es
   */
  function AlkQuery(&$dbConn)
  {
    parent::AlkObject();

    $this->dbConn =& $dbConn;

    $this->oQuerySearch = null;
    $this->oQuerySearchAction = null;

    if( defined("ALK_B_APPLI_SEARCH") && ALK_B_APPLI_SEARCH==true ) {
      global $querySearch, $querySearchAction;
      if( isset($querySearch) && isset($querySearchAction) ) {
        $this->oQuerySearch =& $querySearch;
        $this->oQuerySearchAction =& $querySearchAction;
      }
    }
  }

  /**
   * @brief Execute puis retourne le r�sultat de la requete strSql dans un dataSet
   *
   * @param strSql  Requete sql � ex�cuter
   * @param iFirst  Indice de d�but pour la pagination
   * @param iLast   Indice de fin pour la pagination
   * @param bErr    true si gestion des erreurs, false pour passer sous silence l'erreur
   * @return Retourne un dataSet
   */
  function GetDs($strSql, $iFirst=0, $iLast=-1, $bErr=true)
  {
    return $this->dbConn->InitDataset($strSql, $iFirst, $iLast, $bErr);
  }

  /**
   * @brief Retourne la partie affectation d'une requete update
   * 
   * @param tabValue Tableau contenant les infos (champ, valeur, type) de l'enregistrement
   * @return Retourne un string
   */
  function _GetPartUpdateSql($tabValue)
  {
    $strSql = "";
    foreach($tabValue as $strField => $tabVal) {
      $idType = $tabVal[0];
      $strValue = $tabVal[1];
      if( $strValue != ALK_FIELD_NOT_VIEW ) {
        if( $idType == 0 ) { // string
          $strSql .= ", ".$strField."='".$this->dbConn->analyseSql($strValue)."'";
        } elseif( $idType == 1 ) { // number
          if( $strValue == "" )
            $strSql .= ", ".$strField."=null";
          else
            $strSql .= ", ".$strField."=".str_replace(",", ".", $strValue);
        } elseif( $idType == 2 ) { // date
          if( $strValue == "" )
            $strSql .= ", ".$strField."=null";
          elseif( $strValue == $this->dbConn->getDateCur() )
            $strSql .= ", ".$strField."=".$strValue;
          else
            $strSql .= ", ".$strField."=".$this->dbConn->GetDateFormat("DD/MM/YYYY", "'".$strValue."'");
        } elseif( $idType == 3 ) { // expression sql
          $strSql .= ", ".$strField."=".$strValue;
        }
      }
    }
    if( $strSql != "" )
      return substr($strSql, 1);
    return "";
  }

  /**
   * @brief Retourne la partie affectation d'une requete insert
   * 
   * @param tabValue   Tableau contenant les infos (champ, valeur, type) de l'enregistrement
   * @return Retourne un string
   */
  function _GetPartInsertSql($tabValue)
  {
    $strListField = "";
    $strListValues = "";
    foreach($tabValue as $strField => $tabVal) {
      $idType = $tabVal[0];
      $strValue = $tabVal[1];
      if( $strValue != ALK_FIELD_NOT_VIEW ) {
        $strListField .= ", ".$strField;
      
        if( $idType == 0 ) { // string
          $strListValues .= ", '".$this->dbConn->analyseSql($strValue)."'";
        } elseif( $idType == 1 ) { // number
          if( $strValue == "" )
            $strListValues .= ", null";
          else
            $strListValues .= ", ".str_replace(",", ".", $strValue);
        } elseif( $idType == 2 ) { // date
          if( $strValue == "" )
            $strListValues .= ", null";
          elseif( $strValue == $this->dbConn->getDateCur() )
            $strListValues .= ", ".$strValue;
          else
            $strListValues .= ", ".$this->dbConn->GetDateFormat("DD/MM/YYYY", "'".$strValue."'");
        } else { // =3 : expression sql
          $strListValues .= ", ".$strValue;
        }
      }
    }
    if( $strListField != "" && $strListValues != "")
      return "(".substr($strListField, 1).") values (".substr($strListValues, 1).")";
    return "";
  }

  /**
   * @brief
   *
   * @param iMode       1=ajout, 2=modif, 3=suppr
   * @param strTable    Nom de la table o� collecter les informations � indexer
   * @param tabFields   Tableau contenant la liste des champs de la table � analyser
   * @param tabFiles    Tableau contenant la liste des fichiers associ�s � l'information
   * @param strFieldPk  Nom du champ cl� primaire
   * @param idFieldPk   Valeur de la cl� primaire ou liste de valeurs s�par�es par une virgule
   * @param atype_id    Type de l'application contenant l'information � indexer
   * @param appli_id    Identifiant de l'application contenant l'information � indexer
   * @param datatype_id Type de l'information � indexer
   */
  function setIndex($iMode, $strTable, $tabFields, $tabFiles, $strFieldPk, $idFieldPk, $atype_id, $appli_id, $datatype_id, $strAuthor="", $dDateMin="", $dDateMax="")
  {    
    if ($appli_id==-1)
      return;
    
    if( !($this->oQuerySearch!=null && $this->oQuerySearchAction!=null) ) {
      return;
    }
    

    // suppression avant insertion
    $this->oQuerySearchAction->delData($atype_id, $appli_id, $datatype_id, $idFieldPk, ($iMode == 3));
    
    if( $iMode == 3 ) {
      // mode suppression, on quitte maintenant
      return;
    }

    include_once("../search/classes/alkwordindexer.class.php");
    $oWordIndexer = new AlkWordIndexer("fr");

    $strDoc = "";
    $strSql = "select * from ".$strTable." where ".$strFieldPk." in (".$idFieldPk.")";    
    $oDsData = $this->GetDs($strSql);
    while( $oDrData = $oDsData->GetRowIter() ) {
      $strDoc = "";
      //r�cup�ration des donn�es
      $data_id = $oDrData->GetValueName($strFieldPk);
            
      if ($strAuthor!="" || $dDateMin!="" || $dDateMax!=""){              
        $oDsDatarow = $this->oQuerySearch->GetDatarow_id($data_id, $atype_id, $appli_id, $datatype_id);
        if( $oDsDatarow->bEof ) {
          $datarow_id = $this->oQuerySearchAction->insertDatarow($data_id, $atype_id, $appli_id, $datatype_id);
        } else { 
          $drDatarow = $oDsDatarow->GetRow();
          $datarow_id = $drDatarow->getValueName("DATAROW_ID");  
        }
        $this->oQuerySearchAction->updateDatarow($datarow_id, $strAuthor, $dDateMin, $dDateMax);
      }
      
      //texte � indexer
      for($j=0; $j < count($tabFields); $j++) {
        $strDoc .= $oDrData->GetValueName($tabFields[$j])." ";
      }

      //traitements des mots
      $strDoc = strtolower($strDoc);
      $strDoc = preg_replace('/ [0-9]* /', " ", $strDoc);
      $tabCount = null;
      $tabDoc = null;
      $tabDoc = $oWordIndexer->indexText($strDoc, "fr");
      $tabDoc = array_map(array(&$oWordIndexer, "replaceSpecialChar"), $tabDoc);
      $tabCount = array_count_values($tabDoc);

      //traitement mots par mots
      foreach ($tabCount as $strWord => $strPoid) {
        $oDsWord = $this->oQuerySearch->GetWord_id($strWord);    
        if( $oDsWord->bEof ) {
          $word_id = $this->oQuerySearchAction->insertWord($strWord);
        } else {
          $drWord = $oDsWord->GetRow();
          $word_id = $drWord->getValueName("WORD_ID");
        }

        //si datarow_existe pour ce document
        $oDsDatarow = $this->oQuerySearch->GetDatarow_id($data_id, $atype_id, $appli_id, $datatype_id);
        if( $oDsDatarow->bEof ) {
          $datarow_id = $this->oQuerySearchAction->insertDatarow($data_id, $atype_id, $appli_id, $datatype_id);
        } else { 
          $drDatarow = $oDsDatarow->GetRow();
          $datarow_id = $drDatarow->getValueName("DATAROW_ID");  
        }

        $DsDatarows = $this->oQuerySearch->GetDatarows_id($datarow_id, $strPoid);
        if( $DsDatarows->bEof ) {
          $this->oQuerySearchAction->insertSearch($word_id,$datarow_id,$strPoid,1);
        } else {
          $words_id = "";
          while ( $DrDatarows = $DsDatarows->GetRowIter() ) {
            $words_id .= $DrDatarows->GetValueName("WORDS_ID");  
          }

          if( str_replace("-".$word_id."-", "", $words_id) ) {
            //calcul de la taille 
            $intLongeur = strlen($words_id);
            $a = floor($intLongeur /4000); 
            $r = ($intLongeur % 4000);
            if($r>0)$a++; 
            for($i=1; $i<=$a; $i++){
              $index_id = $i;
              $words = substr($words_id, 0, 4000);
              $words_id = substr($words_id, 4001, strlen($words_id)-1);

              $this->oQuerySearchAction->updateSearch($word_id, $datarow_id, $strPoid, $index_id);
            }
          }
        }
      }
    }
  }

  /**
   * @brief Retourne la liste des datatypes utilis�s pour une fonctionnalit� donn�e
   * 
   * @param strFoncType  Nom de la fonctionnalite (Ex: APPLI_GEOLOC, APPLI_MDATA, APPLI_SEARCH, APPLI_STAT)
   * @param atype_id     Identifiant ou liste d'identifiants type application, (-1 par d�faut, non pris en compte)
   * @return Retourne un dataSet
   */
  function GetDsListeDataTypeID($strFoncType, $atype_id="-1")
  {
    $strSql = "select d.*, ad.ATYPE_ID".
      " from SIT_ATYPE_DATATYPE ad".
      "   inner join SIT_DATATYPE d on ad.DATATYPE_ID=d.DATATYPE_ID".
      " where ".$strFoncType."=1".
      ($atype_id != "-1" ? " and ATYPE_ID in (".$atype_id.")" : "" ).
      " order by ad.ATYPE_ID, d.DATATYPE_INTITULE";
    return $this->dbConn->initDataSet($strSql);
  }

}

?>