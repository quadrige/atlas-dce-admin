<?php

/**
 * @class AlkXml
 *
 * @brief Classe de transformation XML-XSL
 */
class AlkXml
{
  /** Nom du fichier xsl */
  var $strPathFileNameXsl;

  /** Nom du fichier xml */
  var $strPathFileNameXml;

  /** contenu Xml */
  var $strXml;

  /** contenu Xsl */
  var $strXsl;

  /** contenu html obtenu apr�s transformation */
  var $strHtml;

  /** Derniere erreur rencontr�e */
  var $strError;
    
  /**
   * @brief Constructeur par d�faut.
   *        Si strXml est renseign�, strPathFileNameXml non pris en compte
   *        Si strXsl est renseign�, strPathFileNameXsl non pris en compte
   *
   * @param strXml              Contenu xml utilis� pour la transformation, vide par d�faut
   * @param strXsl              Contenu xsl utilis� pour la transformation, vide par d�faut
   * @param strPathFileNameXml  Chemin physique et nom du fichier xml, vide par d�faut
   * @param strPathFileNameXsl  Chemin physique et nom du fichier xsl, vide par d�faut

   */
  function AlkXml($strXml="", $strXsl="", $strPathFileNameXml="", $strPathFileNameXsl="")
  {
    $this->strXml   = $strXml;
    $this->strXsl   = $strXsl;

    $this->strPathFileNameXsl = "";
    $this->strPathFileNameXml = "";

    if( $this->strXml=="" &&  $strPathFileNameXml!="" )
      $this->setFileNameXml($strPathFileNameXml);

    if( $this->strXsl=="" && $strPathFileNameXsl!="" )
      $this->setFileNameXsl($strPathFileNameXsl);

    $this->strHtml  = "";

    $this->strError = "";
  }

  /**
   * @brief M�morise le nom du fichier xml puis charge le contenu xml
   *
   * @param strPathFileName Chemin physique et nom du fichier xml
   */
  function setFileNameXml($strPathFileName)
  {
    $this->strPathFileNameXml = $strPathFileName;
    $this->strXml = "";
    if( file_exists($this->strPathFileNameXml) && is_file($this->strPathFileNameXml) ) {
      if( function_exists("file_get_contents") ) {
        // existe depuis php 4.3.0
        $this->strXml = file_get_contents($this->strPathFileNameXml);
        if( !is_string($this->strXml) ) {
          $this->strXml = "";
        }
      } else {
        $tabLine = file($this->strPathFileNameXml);
        if( count($tabLine) > 0 ) {
          $this->strXml = implode('', $tabLine);
        }
      }
    }
  }

  /**
   * @brief M�morise le nom du fichier xsl puis charge le contenu xsl
   *
   * @param strPathFileName Chemin physique et nom du fichier xsl
   */
  function setFileNameXsl($strPathFileName)
  {
    $this->strPathFileNameXsl = $strPathFileName;
    $this->strXsl = "";
    if( file_exists($this->strPathFileNameXsl) && is_file($this->strPathFileNameXsl) ) {
      if( function_exists("file_get_contents") ) {
        // existe depuis php 4.3.0
        $this->strXsl = file_get_contents($this->strPathFileNameXsl);
        if( !is_string($this->strXsl) ) {
          $this->strXsl = "";
        }
      } else {
        $tabLine = file($this->strPathFileNameXsl);
        if( count($tabLine) > 0 ) {
          $this->strXsl = implode('', $tabLine);
        }
      }
    }
  }

  /**
   * @brief Charge un fichier xsl et le concat�ne au contenu xsl de cet objet
   *
   * @param strPathFileName Chemin physique et nom du fichier xsl � concat�ner
   */
  function addFileNameXsl($strPathFileNameXsl)
  {       
    if( file_exists($strPathFileNameXsl) && is_file($strPathFileNameXsl) ) {
      if( function_exists("file_get_contents") ) {
        // existe depuis php 4.3.0
        $this->strXsl .= file_get_contents($strPathFileNameXsl);
        if( !is_string($this->strXsl) ) {
          $this->strXsl .= "";
        }
      } else {
        $tabLine = file($strPathFileNameXsl);
        if( count($tabLine) > 0 ) {
          $this->strXsl .= implode('', $tabLine);
        }
      }
    }
  }

  /**
   * @brief M�morise le contenu xml
   *
   * @param strXml  Contenu xml utilis� pour la transformation
   */
  function setXml($strXml)
  {
    $this->strXml = $strXml;
  }

  /**
   * @brief M�morise le contenu xsl
   *
   * @param strXsl  Contenu xsl utilis� pour la transformation
   */
  function setXsl($strXsl)
  {
    $this->strXsl = $strXsl;
  }

  /**
   * @brief Retourne le r�sultat de la derni�re transformation
   *
   * @return Retourne un string
   */
  function getHtml() 
  { 
    return $this->strHtml; 
  }

  /**
   * @brief Retourne le texte de la deni�re erreur rencontr�e
   * 
   * @return Retourne un string
   */
  function getLastError() 
  { 
    return $this->strError; 
  }

  /**
   * @brief Effectue la transformation XML/XSL puis m�morise
   *
   * @return Retourne un bool : true si la transformation ok, false sinon
   */
  function setTransformation() 
  {
    $this->strError = "";
    $this->strHtml = "";
    if( $this->strXml == "" ) {
      $this->strError = "Contenu XML vide.\n";
      return false;
    }
    if( $this->strXsl == "" ) {
      $this->strError = "Contenu XSL vide.\n";
      return false;
    }

    if( defined("ALK_B_XSLT_PROCESS")==false || 
        (defined("ALK_B_XSLT_PROCESS")==true && ALK_B_XSLT_PROCESS==true ) ) {
    
      $xh = xslt_create();
      $bTransOk = true;

      $arguments = array('/_xml' => $this->strXml, '/_xsl' => $this->strXsl);

      /* Traitement du document */
      $result = xslt_process($xh, 'arg:/_xml', 'arg:/_xsl', NULL, $arguments);
    
      if( $result ) {
        $this->strHtml = $result;
      } else {
        $this->strError = "Code erreur : ".xslt_errno($xh)."\n".xslt_error($xh);
        $bTransOk = false;
      }
      xslt_free($xh);

      return $bTransOk;

    } else {

      $bTransOk = true;
      $bTmpXml = false;
      $bTmpXsl = false;

      if( $this->strPathFileNameXml=="" || $this->strPathFileNameXsl == "" ) {
        // fichier temporaire
        $nbAlea = date('Gis')."_".rand(0, 999999999);

        $strTmpName = "tmp_".$nbAlea;

        if( $this->strPathFileNameXml == "" ) {
          $this->strPathFileNameXml = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_EXPORT_STAT.$strTmpName.".xml";
          $hFile = fopen($this->strPathFileNameXml, "w+");
          if( $hFile ) {
            fwrite($hFile, $this->strXml);
            fclose($hFile);
            $bTmpXml = true;
          } else {
            $this->strError = "Impossible de cr�er le fichier temporaire XML.\n";
            return false;
          }
        }

        if( $this->strPathFileNameXsl == "" ) {
          $this->strPathFileNameXsl = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_EXPORT_STAT.$strTmpName.".xsl";
          $hFile = fopen($this->strPathFileNameXsl, "w+");
          if( $hFile ) {
            fwrite($hFile, $this->strXsl);
            fclose($hFile);
            $bTmpXsl = true;
          } else {
            if( $bTmpXml == true && 
                file_exists($this->strPathFileNameXml) && is_file($this->strPathFileNameXml) )
              unlink($this->strPathFileNameXml);
            $this->strError = "Impossible de cr�er le fichier temporaire XSL.\n";
            return false;
          }
        }
      }

      $tabRes = array();
      $oRes = "";
      $l_command = "xsltproc ".$this->strPathFileNameXsl." ".$this->strPathFileNameXml;
      exec( $l_command, $tabRes, $oRes );
      
      if( !$oRes ) {
        $this->strHtml = implode(" ", $tabRes);
      } else {
        $this->strError = "Erreur de transformation XML/XSL.\n";
        $bTransOk = false;
      }

      if( $bTmpXml == true && 
          file_exists($this->strPathFileNameXml) && is_file($this->strPathFileNameXml) )
        unlink($this->strPathFileNameXml);

      if( $bTmpXsl == true && 
          file_exists($this->strPathFileNameXsl) && is_file($this->strPathFileNameXsl) )
        unlink($this->strPathFileNameXsl);

      return $bTransOk;
    }
  }
  
}
?>