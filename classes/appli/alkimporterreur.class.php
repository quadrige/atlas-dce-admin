<?php

/**
 * @class Classe ImportErreur
 *
 * @brief Classe d'erreurs de base pour l' import
 */
class AlkImportErreur extends AlkObject
{
	/** Emplacement  du fichier d'Erreur' */
	var $strFichier_Erreur;
	
	/**
   * @brief Constructeur de la classe ImportErreur : initialisation des attributs de ImportErreur.
   *
   * @param strFichier_Erreur Emplacement  du fichier d'import
   */
	function AlkImportErreur( $strFichier_Erreur )
	{
		$this->strFichier_Erreur = $strFichier_Erreur;
	}
	
	/**
   * @brief fonction de gestion d'erreur.
   *
   * @param 
   * @param 
   */
	function erreur($numErreur, $strMesErreur="" )
	{
		// Ouverture du fichier en mode append
		$ferreur = fopen($this->strFichier_Erreur, "a");
		$iRes = 0;	
		if($ferreur) {
			
			switch ($numErreur) {
      case 1 :
        $strMessage = "Erreur : Le fichier d'import est introuvable.";
        break;
      case 2 :
        $strMessage = "Erreur : Colonne ".$strMesErreur." n'est pas au bon endroit verifier l'indice.";
				break;
      case 3 :
				$strMessage = "Erreur : Colonne ".$strMesErreur." est obligatoire.";
				break;
      case 4 :
				$strMessage = "Erreur : Nom de la colonne ".$strMesErreur." incorrecte.";
				break;
      case 5 :
				$strMessage = "Erreur : Ligne ".$strMesErreur." : Le nombre de colonne ne correspond pas aux colonnes de r�f�rence.";
				break;
      case 6 :
				$strMessage = "Erreur : Fichier d'import non int�gr�.";
				break;
      case 7 :
				$strMessage = $strMesErreur;
				break;
      case 8 :
				$strMessage = $strMesErreur." La valeur de la colonne MODE doit �tre S ou M ou C." ;
				break;
			}

			fwrite($ferreur, $strMessage."\n");
			$iRes = 1;
		}
		
		fclose($ferreur);
		return $iRes;  
	}
}
?>