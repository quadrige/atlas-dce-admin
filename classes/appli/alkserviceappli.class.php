<?php

/**
 *  Classe
 *
 * Classe repr�sentant une abstraction d'un service demand� � une application
 */
class AlkServiceAppli extends AlkObject
{
  /** Indentifiant de l'application */
  var $appli_id;

  /** Indentifiant du type d'application */
  var $atype_id;

  /** instance de la classe alkApplication */
  var $oAppli;

  /** R�f�rence vers l'objet espace de travail qui contient cette application */
  var $oSpace;

  /** Indentifiant de l'agent */
  var $agent_id;
  

  /**
   * @brief Constructeur par d�faut
   *
   * @param atype_id Type d'application
   * @param oSpace   R�f�rence vers l'objet de l'espace en cours
   * @param appli_id Identifiant de l'application
   * @param agent_id Identifiant de l'agent connect�
   */
  function AlkServiceAppli($atype_id, &$oSpace, $appli_id, $agent_id)
  {
    $this->appli_id = $appli_id;
    $this->atype_id = $atype_id;
    $this->oSpace =& $oSpace;
    $this->agent_id = $agent_id;
    $this->oAppli = null;

    switch( $this->atype_id ) {
    case 1:
      if( defined("ALK_B_APPLI_ANNU") && ALK_B_APPLI_ANNU==true ) {
        include_once("../".ALK_DIR_ANNU."classes/alkappliannu.class.php");
        $this->oAppli = new AlkAppliAnnu($oSpace, 0, $agent_id,  
                                         ALK_SIALKE_URL.ALK_PATH_UPLOAD_ANNU,
                                         ALK_SIALKE_PATH.ALK_PATH_UPLOAD_ANNU,
                                         $this);
      }
      break;

    case 2:
    case 4: // Fonds documentaires
      if( defined("ALK_B_APPLI_FDOC") && ALK_B_APPLI_FDOC == true ) {
        include_once("../fdoc_01/classes/alkapplifdoc.class.php");
        $this->oAppli = new AlkAppliFDoc($oSpace, $appli_id, $agent_id,  
                                         ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC_IMG,
                                         ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_IMG,
                                         $this);
      }
      break;

    case 3: // Actualit�s
      if( defined("ALK_B_APPLI_ACTU") && ALK_B_APPLI_ACTU == true ) {
        include_once("../actu_01/classes/alkappliactu.class.php");
        $this->oAppli = new AlkAppliActu01($oSpace, $appli_id, $agent_id,
                                           ALK_SIALKE_URL.ALK_PATH_UPLOAD_ACTU_LOGO, 
                                           ALK_SIALKE_PATH.ALK_PATH_UPLOAD_ACTU_LOGO,
                                           $this);
      }
      break;

    case 5: // Agenda
      if( defined("ALK_B_APPLI_AGENDA") && ALK_B_APPLI_AGENDA == true ) {
        include_once("../agenda_01/classes/alkappliagenda.class.php");
        $this->oAppli = new AlkAppliAgenda($oSpace, $appli_id, $agent_id,
                                           ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC_AGENDA, 
                                           ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_AGENDA,
                                           $this);
      }
      break;

    case 6: // Liste de diffusion
      if( defined("ALK_B_APPLI_LSDIF") && ALK_B_APPLI_LSDIF == true ) {
        include_once("../lsdif_01/classes/alkapplilsdif.class.php");
        $this->oAppli = new AlkAppliLsDif($oSpace, $appli_id, $agent_id,
                                          ALK_SIALKE_URL.ALK_PATH_UPLOAD_LSDIF, 
                                          ALK_SIALKE_PATH.ALK_PATH_UPLOAD_LSDIF,
                                          $this);
      }
      break;

    case 7: // Forum
      if( defined("ALK_B_APPLI_FORUM") && ALK_B_APPLI_FORUM == true ) {
        include_once("../forum_01/classes/alkappliforum.class.php");
        $this->oAppli = new AlkAppliForum($oSpace, $appli_id, $agent_id,
                                          ALK_SIALKE_URL.ALK_PATH_UPLOAD_FORUM_IMG, 
                                          ALK_SIALKE_PATH.ALK_PATH_UPLOAD_FORUM_IMG,
                                          $this);
      }
      break;

    case 8: // SIG
      if( defined("ALK_B_APPLI_SIG") && ALK_B_APPLI_SIG == true ) {
        include_once("../sig_admin/classes/alkapplisig.class.phtml");
        $this->oAppli = new AlkAppliSig($oSpace, $appli_id, $agent_id,
                                        ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC_SIG, 
                                        ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_SIG,
                                        $this);
      }
      break;

    case 9: // Favoris
      if( defined("ALK_B_APPLI_FAVORIS") && ALK_B_APPLI_FAVORIS == true ) {
        include_once("../bookmark_01/classes/alkapplifavoris.class.php");
        $this->oAppli = new AlkAppliFavoris($oSpace, $appli_id, $agent_id, "", "", $this);
      }
      break;
    
    case 10: // Bourse d'�change
      if( defined("ALK_B_APPLI_BEF") && ALK_B_APPLI_BEF == true ) {
        include_once("../bef/classes/alkapplibef.class.php");
        $this->oAppli = new AlkAppliBef($oSpace, $appli_id, $agent_id, "", "", $this);
      }
      break;
        
    case 21: // Actualit�s
      if( defined("ALK_B_APPLI_ACTUSITE") && ALK_B_APPLI_ACTUSITE == true ) {
        include_once("../actu/classes/alkappliactu.class.php");
        $this->oAppli = new AlkAppliActu($oSpace, $appli_id, $agent_id, "", "");
      }
      break;

    case 22: // Liens utiles
      if( defined("ALK_B_APPLI_LIEN") && ALK_B_APPLI_LIEN == true ) {
        include_once("../lien/classes/alkapplilien.class.php");
        $this->oAppli = new AlkAppliLien($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case 23: // Gestion �ditoriale
      if( defined("ALK_B_APPLI_GEDIT") && ALK_B_APPLI_GEDIT == true ) {
        include_once("../gedit/classes/alkappligedit.class.php");
        $this->oAppli = new AlkAppliGEdit($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case 24: // Glossaire
      if( defined("ALK_B_APPLI_GLOS") && ALK_B_APPLI_GLOS == true ) {
        include_once("../glos/classes/alkappliglos.class.php");
        $this->oAppli = new AlkAppliGlos($oSpace, $appli_id, $agent_id, "", "");
      }
      break;  
      
    case 25: // Faqs
      if( defined("ALK_B_APPLI_FAQS") && ALK_B_APPLI_FAQS == true ) {
        include_once("../faqs/classes/alkapplifaq.class.php");
        $this->oAppli = new AlkAppliFaq($oSpace, $appli_id, $agent_id, "", "");
      }
      break;

    case 26: // Syndication Alkanet
      if( defined("ALK_B_APPLI_SYNDICATION") && ALK_B_APPLI_SYNDICATION == true ) {
        include_once("../synd/classes/alkapplisynd.class.php");
        $this->oAppli = new AlkAppliSynd($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    
    case 27: // Abonnement newsletter
      if( defined("ALK_B_GEDIT_LETTRE") && ALK_B_GEDIT_LETTRE == true ) {
        include_once("../newsletter/classes/alkapplinewsletter.class.php");
        $this->oAppli = new AlkAppliNewsletter($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
   case 29: // Traduction
      if( defined("ALK_B_APPLI_TRADUCTION") && ALK_B_APPLI_TRADUCTION == true ) {
        include_once("../trad/classes/alkapplitrad.class.php");
        $this->oAppli = new AlkAppliTraduction($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
   case 30: // sigza
      if( defined("ALK_B_APPLI_SIGZA") && ALK_B_APPLI_SIGZA == true ) {
        include_once("../sigza/classes/alkapplisigza.class.php");
        $this->oAppli = new AlkAppliSigZa($oSpace, $appli_id, $agent_id, "", "");
      }
      break;   
      
   case 31: // Recettes
      if( defined("ALK_B_APPLI_RECETTE") && ALK_B_APPLI_RECETTE == true ) {
        include_once("../recette/classes/alkapplirecette.class.php");
        $this->oAppli = new AlkAppliRecette($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    
     case 32: // Formulaire
      if( defined("ALK_B_APPLI_FORMULAIRE") && ALK_B_APPLI_FORMULAIRE == true ) {
        include_once("../form/classes/alkappliformulaire.class.php");
        $this->oAppli = new AlkAppliFormulaire($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case 33: // Sondage
      if( defined("ALK_B_APPLI_SONDAGE") && ALK_B_APPLI_SONDAGE == true ) {
        include_once("../sondage/classes/alkapplisondage.class.php");
        $this->oAppli = new AlkAppliSondage($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case 41: // explorateur
      if( defined("ALK_B_APPLI_EXPLORER") && ALK_B_APPLI_EXPLORER==true ) {
        include_once("../explorer/classes/alkappliexplorer.class.php");
        $this->oAppli = new AlkAppliExplorer($oSpace, $appli_id, $agent_id, "", "");        
      }

    case 43: // Formations
      if( defined("ALK_B_APPLI_FORMATION") && ALK_B_APPLI_FORMATION == true ) {
        include_once("../formation/classes/alkappliformation.class.php");
        $this->oAppli = new AlkAppliFormation($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    
    case 44: // Forum site' .
        if( defined("ALK_B_APPLI_FORUMSITE") && ALK_B_APPLI_FORUMSITE == true ) {
        include_once("../forum/classes/alkappliforum.class.php");
        $this->oAppli = new AlkAppliForum($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    
    
    case 47: // Edition cooperative' .
        if( defined("ALK_B_APPLI_GCOEDIT") && ALK_B_APPLI_GCOEDIT== true ) {
        include_once("../gcoedit/classes/alkappligcoedit.class.php");
        $this->oAppli = new AlkAppliGCoEdit($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case 48: // P�lerinage .
        if( defined("ALK_B_APPLI_PELERINAGE") && ALK_B_APPLI_PELERINAGE== true ) {
        include_once("../pelerinage/classes/alkapplipelerinage.class.php");
        $this->oAppli = new AlkAppliPelerinage($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
         
    case 49: // Billetterie .
        if( defined("ALK_B_APPLI_BILLETTERIE") && ALK_B_APPLI_BILLETTERIE== true ) {
        include_once("../billetterie/classes/alkapplibilletterie.class.php");
        $this->oAppli = new AlkAppliBilletterie($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case 51: // Fond doc distant
      if( defined("ALK_B_APPLI_FDOC_DIST") && ALK_B_APPLI_FDOC_DIST== true ) {
        include_once("../fdoc_distant/classes/alkapplifdocdist.class.php");
        $this->oAppli = new AlkAppliFDocDist($oSpace, $appli_id, $agent_id, "", "");
      }
      break;          
    
    case 52: // Communiqu� de presse
      if( defined("ALK_B_APPLI_COMMPRESS") && ALK_B_APPLI_COMMPRESS== true ) {
        include_once("../commpress/classes/alkapplicommpress.class.php");
        $this->oAppli = new AlkAppliCommPress($oSpace, $appli_id, $agent_id, "", "");
      }
      break; 
      
    case 53: // Atlas
      if (defined("ALK_B_APPLI_ATLAS") && ALK_B_APPLI_ATLAS==true){
        include_once("../atlas/classes/alkappliatlas.class.php");
        $this->oAppli = new AlkAppliAtlas($oSpace, $appli_id, $agent_id, "", "");      
      }
      break;             

    case 54: // Base de connaissance
      if( defined("ALK_B_APPLI_BDC") && ALK_B_APPLI_BDC== true ) {
        include_once("../bdc/classes/alkapplibdc.class.php");
        $this->oAppli = new AlkAppliBdc($oSpace, $appli_id, $agent_id, "", "");
      }
      break;       
    
    case 55: // G�olocalisation
      if( defined("ALK_B_APPLI_GEOLOC") && ALK_B_APPLI_GEOLOC== true ) {
        include_once("../geoloc/classes/alkappligeoloc.class.php");
        $this->oAppli = new AlkAppliGeoloc($oSpace, $appli_id, $agent_id, "", "");
      }
      break; 

    case 57: // GRR
      if( defined("ALK_B_APPLI_GRR") && ALK_B_APPLI_GRR== true ) {
        include_once("../grr/classes/alkappligrr.class.php");
        $this->oAppli = new AlkAppliGRR($oSpace, $appli_id, $agent_id, 
                                        ALK_SIALKE_URL.ALK_PATH_UPLOAD_GRR,  
                                        ALK_SIALKE_PATH.ALK_PATH_UPLOAD_GRR);
      }
      break; 
      
    case 58: // Gestion de dossiers
      if( defined("ALK_B_APPLI_GID") && ALK_B_APPLI_GID== true ) {
        include_once("../gid/classes/alkappligid.class.php");
        $this->oAppli = new AlkAppliGid($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
               
    case 59: // Circuit de validation
      if( defined("ALK_B_APPLI_CRV") && ALK_B_APPLI_CRV==true ) {
        include_once("../crv_01/classes/alkapplicrv.class.php");
        $this->oAppli = new AlkAppliCrv($oSpace, $appli_id, $agent_id, "", "");
      }
      break;             
    
    
    case 60: // Gestion des droits SIG
      if (defined("ALK_B_APPLI_ASIGS") && ALK_B_APPLI_ASIGS==true){
        include_once("../site/classes/alkappliASIGS.class.php");
        $this->oAppli = new AlkAppliASIGS($oSpace, $appli_id, $agent_id, "", "");      
      }
      break;
             
    case 61: // mdata
      if (defined("ALK_B_APPLI_MDATA") && ALK_B_APPLI_MDATA==true){
        include_once("../mdata/classes/alkapplimdata.class.php");
        $this->oAppli = new AlkAppliMData($oSpace, $appli_id, $agent_id, "", "");      
      }
      break;       
             
    case 63: // search (ALK_ATYPE_ID_SEARCH)
      if (defined("ALK_B_APPLI_SEARCH") && ALK_B_APPLI_SEARCH==true){
        include_once("../search/classes/alkapplisearch.class.php");
        $this->oAppli = new AlkAppliSearch($oSpace, $appli_id, $agent_id, "", "");      
      }
      break;   
    
    case 68: // March�s publics
      if( defined("ALK_B_APPLI_MARCHES") && ALK_B_APPLI_MARCHES == true ) {
        include_once("../marchespublics/classes/alkapplimarches.class.php");
        $this->oAppli = new AlkAppliMarches($oSpace, $appli_id, $agent_id, "", "");
      }
      break;            
    }
    
  }

  /**
   * @brief Appel d'un service offert par une application
   *
   * @param idServ Num�ro du service demand�
   * @param oRes  Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @return Retourne le r�sultat du service demand�, false si aucun service.
   */
  function CallService($idServ, &$oRes)
  {
    if( $this->oAppli != null ) {
      $tabArg = func_get_args();
      $nbArg = count($tabArg);
      $strArgs = "";
      for($i=2; $i<$nbArg; $i++)
        $strArgs .= ($strArgs=="" ? "" : ", ")."\"".$tabArg[$i]."\"";

      if( $strArgs!="" )
        eval("$"."result = "."$"."this->oAppli->CallService".$idServ."("."$"."oRes, ".$strArgs.");");
      else
        eval("$"."result = "."$"."this->oAppli->CallService".$idServ."("."$"."oRes);");

      return $result;
    }
    return false;
  }

  /**
   * @brief Appel d'un service g�n�rique permettant d'obtenir l'url pour l'ajout d'un lien inter appli
   *
   * @param oRes    non utilis�, permet de retourner une info compl�mentaire
   * @param strForm nom du formulaire de destination
   * @param eltForm nom de l'�l�ment (text ou textarea) du formulaire de destination
   */
  function CallServiceUrlAjoutLienInterAppli(&$oRes, $strForm, $strEltForm)
  {
    $strUrl = ALK_SIALKE_URL."scripts/".ALK_DIR_ESPACE."serv_inter_appli.php?form=".$strForm."&elt=".$strEltForm;
    return $strUrl;
  }

  /**
   * @brief permet d'obtenir l'url d�cod� d'un lien inter appli
   *
   * @param oRes    non utilis�, permet de retourner une info compl�mentaire
   * @param strCode lien cod� (ex ##fdoc_id_texte du lien##)
   * @param strCss  Nom du classe css pour le lien g�n�r�
   */
  function GetHtmlDecodeLienInterAppli(&$oRes, $strCode, $strCss)
  {
    global $queryCont;
    $strHtml = $strCode;

    //Supprime les #
    $strCode = substr($strCode,2,strlen($strHtml)-4);
    $tabCode = explode("_",$strCode,3);

    //Recherche du type d'application demand�e
    $dsTypeAppli = $queryCont->GetDs_ficheTypeAppliByAbrev($tabCode[0]);
    if( $drTypeAppli = $dsTypeAppli->getRowIter() ) {
      $atype_id = $drTypeAppli->getValueName("ATYPE_ID");
      $oService = new AlkServiceAppli($atype_id, $this->oSpace, "-1", $this->agent_id);
      $oRet = null;
      $strTemp = $oService->CallService("HtmlDecodeLienInterAppli", $oRet, 
                                        $tabCode[1], $tabCode[2], $strCss);

      if( $strTemp != "" )
        $strHtml = $strTemp;
    }
    return $strHtml;
  }

  /**
   * @brief Appel d'un service g�n�rique permettant de parcourir et remplacer les �ventuels liens inter-appli
   *        cod�s dans le contenu par [diese][diese]fdoc_id_texte du lien[diese][diese]
   *        renvoie le contenu html apr�s avoir effectu� les remplacements n�cessaires
   * 
   * @param oRes       non utilis�, permet de retourner une info complementaire
   * @param strContenu contenu html � parcourir
   * @param strCss     classe Css � appliquer sur les liens transform�s
   */
  function CallServiceHtmlReplaceLienInterAppli(&$oRes, $strContenu, $strCss)
  {
    $strHtml = $strContenu;
    preg_match_all("(##.[^##_]*_.[^##_]*_.[^##]*##)", $strHtml, $tabTemp, PREG_PATTERN_ORDER);
    foreach($tabTemp as $tabCode) {
      foreach ($tabCode as $strCode) {
        $oRes = null;
        $strHtml = str_replace($strCode, 
                               $this->GetHtmlDecodeLienInterAppli($oRes, $strCode, $strCss), 
                               $strHtml);
      }
    }
    return $strHtml;
  }

  /**
   * @brief Appel d'un service g�n�rique permettant d'obtenir le code html de la page de s�lection
   *        d'un type d'appli pour l'insertion d'un lien r�f�rence vers une appli
   *
   * @param strForm nom du formulaire de destination
   * @param eltForm nom de l'�l�ment (text ou textarea) du formulaire de destination
   */
  function CallServiceHtmlAjoutLienInterAppli(&$oRes, &$queryCont, $strForm, $strEltForm, $strAction)
  {
    $strTmp = substr(ALK_S_TYPEAPPLI_INTER_APPLI,0,strlen(ALK_S_TYPEAPPLI_INTER_APPLI)-1);
    $dsTypeAppli = $queryCont->GetDs_listeTypeAppliByListId($strTmp);
    $iLarg = "606";

    $strHtml = "<form name='formTypeAppli' action=\"".$strAction."?form=".
      $strForm."&elt=".$strEltForm."\" method=post>".
      getHtmlHeaderPage(false, "Ajouter un lien", $iLarg).
      getHtmlLineFrame($iLarg).
      getHtmlHeaderFrame($iLarg, 15);
      getHtmlTableCtrlHeader();

    if( $dsTypeAppli->iCountTotDr>0 && strlen(ALK_S_TYPEAPPLI_INTER_APPLI)-1 > 0 ) {
      $oCtrlSelect = new HtmlSelect(0, "atype_id", "", "S�lectionnez un type d'application");
      $oCtrlSelect->oValTxt = $dsTypeAppli;
      $strHtml .= getHtmlTableCtrlLine($oCtrlSelect->label, $oCtrlSelect->getHtml()).
        getHtmlTableCtrlSpaceLine(10);

      $oBtV = new HtmlLink("javascript:Valider('formTypeAppli');", "Valider",
                           "valid_gen.gif", "valid_gen_rol.gif");
      $oBtA = new HtmlLink("javascript:window.close();", "Annuler", 
                           "annul_gen.gif", "annul_gen_rol.gif");
      $strLabel = "";
      $strCtrl = $oBtV->getHtml()."&nbsp;&nbsp;".$oBtA->getHtml();
    } else {
      $strLabel = "";
      $strCtrl = "Aucune r�f�rence n'est possible.";
    }

    $strHtml .= getHtmlTableCtrlLine($strLabel, $strCtrl).
      getHtmlTableCtrlSpaceLine(10).
      getHtmlTableCtrlFooter().
      getHtmlLineFrame($iLarg).
      getHtmlFooterFrame(10).
      getHtmlFooterPage().
      "</form>";

    return $strHtml;
  }
}

?>