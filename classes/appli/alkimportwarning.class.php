<?php

/**
 * @class Classe ImportErreur
 *
 * @brief Classe d'erreurs de base pour l'import
 */
class AlkImportWarning extends AlkObject
{
	/** Emplacement  du fichier d'Erreur' */
	var $strFichier_Warning;
	
	/**
   * @brief Constructeur de la classe ImportErreur : initialisation des attributs de ImportErreur.
   *
   * @param strFichier_Erreur Emplacement  du fichier d'import
   */
	function AlkImportWarning( $strFichier_Warning )
	{
		$this->strFichier_Warning = $strFichier_Warning;
	}
	
	/**
   * @brief fonction de gestion d'erreur.
   *
   * @param 
   */
	function erreur($numErreur, $strMesWarning="" )
	{
		
		// Ouverture du fichier en mode append
		$ferreur = fopen($this->strFichier_Warning, "a");
		$iRes = 0;	
		if($ferreur){ 
			switch ($numErreur){
      case 7 :
				$strMessage = $strMesWarning;
				break;
			}
			
			fwrite($ferreur, $strMessage."\n");
			$iRes = 1;
		}
		
		fclose($ferreur);
		return $iRes;  
	}
}
?>