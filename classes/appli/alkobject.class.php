<?php

/**
 *  Classe abstraite de base des objects Alkanet
 */
class AlkObject 
{ 
  /** tableau des langues prises en compte, = vide par d�faut */
  var $tabLangue;

  /** nombre de langue uilis�e */
  var $nbLangue;

  /** 
   * @brief Constructeur par d�faut
   */
  function AlkObject()
  {
    global $_LG_tab_langue;

    $this->tabLangue =  $_LG_tab_langue;
    $this->nbLangue = count($this->tabLangue);    
  }
}

?>