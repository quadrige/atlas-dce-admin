<?php

/**
 * @class Classe Import
 *
 * @brief Classe de base pour l' import.
 */
class AlkImport extends AlkObject
{
	/** Emplacement  du fichier d'import. */
	var $strFichier_Import;
	
	/** Nom de l'objet  ImportErreur. */
	var $obj_Erreur;
	
	/** Nom de l'objet  ImportErreur. */
	var $obj_Warning;
	
	/** 
   * @brief Tableau contenant les  propri�t�s des colonnes.
   *
   * les elements du tableau sont des tableaux a quatre cellules 
   * cellule 'name' correspand au nom 
   * cellule 'oglig' correspand au type de la col (1:obligatoire , 0:non)
   * cellule 'type ' correspand au type de chamaps
   * cellule 'lg' correspand � la taille max  du champs
   * cellule  'index' correspand � l'indice de la col
   * cellule  'mode' correspand au mode 0:si numeric ou string , 1:si tableau simple ou liste, 2:si tableaux de correspandance
   */
	var $tabAlkImportCol;
	
	/** query de consultation sur l'appli */
	var $oQuery;
	
	/** query action de l'appli */
	var $oQueryAction;
	
	/** reference vers l'objet workspace */
	var $oSpace;
	/**
   * @brief Constructeur de la classe Import : initialisation des attributs de Import.
   *
   * @param strFichier_Import Emplacement  du fichier d'import
   * @param tabImport  Tableau contenant les propriet�s des  colonnes
   * @param tabSup  Tableau contenant les colonnes obligatoires pour la suppression
   * @param tabAjoutt  Tableau contenant les colonnes obligatoires pour l'ajouts
   * @param objErreur  Nom de l'objet  ImportErreur
   */
	function AlkImport( $strFichier_Import,  $tabAlkImportCol, $tabSup, $tabAjout, $tabModif, 
                      &$obj_Erreur,&$objWarning, &$oQuery, &$oQueryAction, &$oSpace )
	{
		$this->strFichier_Import = $strFichier_Import;
		$this->tabAlkImportCol = $tabAlkImportCol;
		$this->obj_Erreur = &$obj_Erreur;
		$this->obj_Warning = &$objWarning;
		$this->tabSup = $tabSup;
		$this->tabAjout = $tabAjout;
		$this->tabModif = $tabModif;
		$this->oQueryAction = &$oQueryAction;
		$this->oQuery = &$oQuery;
		$this->oSpace = &$oSpace;
	}
	
	/**
   * @brief Fonction indiqant si le fichier d'import est correct au niveau de sa structure.
   *    
   * @param intCarSeparateur Notation decimale du caract�re ascii separateur de champ du fichier d'import
   * @return 0 si correcte .
   */
	function parseFile($intCarSeparateur)
	{
		
		$intTest=0;
		$intSeparateur = $intCarSeparateur; // Separateur de champ
		if(file_exists($this->strFichier_Import))	{
			$fImport = fopen($this->strFichier_Import, "r");
		}	else {
			$fImport = false;
			$this->obj_Erreur->erreur(1);
			$intTest++;
		}
		
		$strChaineSyntaxe = "";
		if( $fImport ){
			
			// lecture de la premiere ligne
			$intTailleFichier = filesize($this->strFichier_Import);
			$tabLigne = fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
			//print_r($tabLigne);
			$intNbColonne = count($tabLigne);
		        
			//verifie si les colonnes obligatoires sont pr�sentes ou pas (avec verifie l d'indice )
			for ($j=0; $j<count($this->tabAlkImportCol); $j++) {	
				if (!in_array($this->tabAlkImportCol[$j]["name"], $tabLigne) )  {
					if ($this->tabAlkImportCol[$j]["oblig"] == 1){
						$this->obj_Erreur->erreur(3,$this->tabAlkImportCol[$j]["name"]);
						$intTest++;
					}
				}else{
          if ( ($this->tabAlkImportCol[$j]["index"]!=0) && 
               (array_search('$this->tabAlkImportCol[$j][\"name\"]', $tabLigne) != $this->tabAlkImportCol[$j]["index"]) ){
						$this->obj_Erreur->erreur(2,$this->tabAlkImportCol[$j]["name"]);
						$intTest++;
          }
        }		   	
			}
			
			//verifie si les colonnes du fichiers sont valides
			$strNom=","; 
			for ($j=0; $j<count($this->tabAlkImportCol); $j++) {
				$strNom.=$this->tabAlkImportCol[$j]["name"].",";				
			}
			
			for ($j=0; $j<count($tabLigne); $j++) {	
				if($tabLigne[$j] != NULL){
					if( !strpos($strNom,$tabLigne[$j])){
						$this->obj_Erreur->erreur(4,$tabLigne[$j]);
						$intTest++;
					}
				} else {
					$strMes = "Erreur : Nom de la colonne ".$j." non defini.";
					$this->obj_Erreur->erreur(7,$strMes);
					$intTest++;
				}            
			}
			
			// verifie si les colonnes  correspondent  aux colonnes de r�f�rence !
			$i = 2;
			while( !feof($fImport) ) {
				$tabLigne = fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
				if( $tabLigne == NULL )	{	
					// on ignore les lignes vides
				} else {
					if( $intNbColonne >= count($tabLigne) ){
						// La ligne est ok
					} else {
						//$strChaineMessage = "Ligne ".$i." : Le nombre de colonne ne correspond pas aux colonnes de r�f�rence !";
						$this->obj_Erreur->erreur(5,$i);
						$intTest++;
					}
					$i++;
				}
			}
		}
		
		// fermeture du fichier d'import
		if(file_exists($this->strFichier_Import))
      fclose($fImport);
		if( $intTest > 0 ){
			$this->obj_Erreur->erreur(6);
		}
		
		return $intTest;
	}
  
  /**
   * @brief Fonction indiqant si la ligne a importer dans la base est correcte au niveau de sa structure.
   * 
   * @param intCarSeparateur Notation decimale du caract�re ascii separateur de champ du fichier d'import
   * @return 0 si correcte 
   */
	function parseLines($intCarSeparateur)
	{
		$intTest=0;
		$intSeparateur = $intCarSeparateur; // Separateur de champ
		$tabCopy = array();
		$tabTrace = array();
		if(file_exists($this->strFichier_Import))	{
			$fImport = fopen($this->strFichier_Import, "r");
		}
		else{
			$fImport = false;
			$this->obj_Erreur->erreur(1);
			$intTest++;
		}
		
		if( $fImport ){
			// lecture de la premiere ligne
			$intTailleFichier = filesize($this->strFichier_Import);
			$tabLigne = fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
			for ($j=0; $j<count($tabLigne); $j++) {
				for ($k=0; $k<count($this->tabAlkImportCol); $k++) {
					if ($tabLigne[$j]==$this->tabAlkImportCol[$k]["name"]){
						//array_push($tabCopy,$this->tabAlkImportCol[$k]);
						$tabCopy[$tabLigne[$j]]=$this->tabAlkImportCol[$k];
						array_push($tabTrace, $this->tabAlkImportCol[$k]);
					}	
					
				}           
			}
			$tabKeys = array_keys($tabCopy);
			$intKey="";
			$i = 2;
			
			while( !feof($fImport) ) {
				$tabLigne = fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
				
				if( $tabLigne == NULL )	{	
					// on ignore les lignes vides
				}	else {
					for ($j=0; $j<count($tabLigne); $j++) {
						
						if( $tabKeys[$j]=="MODE" ) {
							
							switch(strtoupper($tabLigne[$j])){
              case "S":
								for ($k=0; $k<count($this->tabSup); $k++) {
									$intKey = array_search(($this->tabSup[$k]), $tabKeys);
									
									if($tabLigne[$intKey] == ""){
										$strMes = "Erreur : Le champ de la ligne ".$i." et de la colonne ".
                      $tabKeys[$intKey]." est obligatoire pour la suppression.";
										$this->obj_Erreur->erreur(7,$strMes);
										$intTest++;	
									}
								}
								break;
              case "C":
								for ($k=0; $k<count($this->tabAjout); $k++) {
									$intKey = array_search(($this->tabAjout[$k]), $tabKeys);
									
									if($tabLigne[$intKey] == ""){
										$strMes = "Erreur : Le champ de la ligne ".$i." et de la colonne ".
                      $tabKeys[$intKey]." est obligatoire pour l'ajout.";
										$this->obj_Erreur->erreur(7, $strMes);
										$intTest++;
									}
								}
								break;
              case "":
								$strMes = "Erreur : Ligne ".$i." ";
								$this->obj_Erreur->erreur(8,$strMes);
								$intTest++;	
								break;
              case "M":
								for ($k=0; $k<count($this->tabModif); $k++) {
									$intKey = array_search(($this->tabModif[$k]), $tabKeys);
									
									if($tabLigne[$intKey] == ""){
										$strMes = "Erreur : Le champ de la ligne ".$i." et de la colonne ".$tabKeys[$intKey].
                      " est obligatoire pour la modification.";
										$this->obj_Erreur->erreur(7,$strMes);
										$intTest++;	
									}
								}
								break;
							}
						}
						
						if((strlen($tabLigne[$j]) > $tabTrace[$j]["lg"]) && (!is_array($tabTrace[$j]["type"]))){
							$strMes = "Erreur : Le champ de la ligne ".$i." et de la colonne ".$tabKeys[$j].
                " est limit� � ".$tabTrace[$j]["lg"]." caract�res.";
							$this->obj_Erreur->erreur(7,$strMes);
						}
						
						if($tabLigne[$j] != "" && $tabTrace[$j]["type"]=="numeric"){
							$tabLigne[$j] = preg_replace('/,/', '.', $tabLigne[$j]);
							if (!(is_numeric(trim($tabLigne[$j])))){
								$strMes = "Erreur : Le champ de la ligne ".$i.
                  " et de la colonne ".$tabKeys[$j]." est de type numerique.";
								$this->obj_Erreur->erreur(7, $strMes);
								$intTest++;
							}
						}
						//print_r( $tabTrace[$j]);
						//echo "<br>";
						if($tabLigne[$j] != "" && $tabTrace[$j]["mode"]==1 ){
							$strSelect = "";
							for ($s=0; $s<count($tabTrace[$j]["type"]); $s++) {
								$strSelect .= " , ".$tabTrace[$j]["type"][$s];
							}

							if (!(in_array(trim($tabLigne[$j]),$tabTrace[$j]["type"]))){
								$strMes = "Erreur : Le champ de la ligne ".$i.
                  " et de la colonne ".$tabKeys[$j]." est de type ".$strSelect.".";
								$this->obj_Erreur->erreur(7,$strMes);
								$intTest++;													
							}
						}
						if($tabLigne[$j] != "" && $tabTrace[$j]["mode"]==2 ){
								
							$strSelect = "";
							foreach ( $tabTrace[$j]["type"] as $champs ){
                $strSelect .= " , ".$champs;
							}
							
              if (!in_array(trim($tabLigne[$j]),$tabTrace[$j]["type"] )) {
                $strMes = "Erreur : Le champ de la ligne ".$i.
                  " et de la colonne ".$tabKeys[$j]." est de type ".$strSelect.".";
								$this->obj_Erreur->erreur(7,$strMes);
								$intTest++;
              }
                            
						}
						
						if ($tabTrace[$j]["type"]=="date"){
							if($tabLigne[$j] != "" ){
								if(strlen(trim($tabLigne[$j]))==$tabTrace[$j]["lg"] &&  substr_count(trim($tabLigne[$j]),"/") ==2 ){
									$dateFormat = preg_split("/\//",trim($tabLigne[$j]));
									if((!is_numeric($dateFormat[0])) || ($dateFormat[0]<1 || $dateFormat[0]>31)){
										$strMes = "le champ jour de la date � la ligne ".$i.
                      " et � la colonne ".$tabKeys[$j]." est incorrecte." ;
										$this->obj_Erreur->erreur(7,$strMes);
										$intTest++;
									}
									if((!is_numeric($dateFormat[1]))|| ($dateFormat[1]<1 || $dateFormat[1]>12)){
										$strMes = "Erreur : Le champ mois de la date � la ligne ".
                      $i." et � la colonne ".$tabKeys[$j]." est incorrecte.";
										$this->obj_Erreur->erreur(7,$strMes);
										$intTest++;												
									}
									if(!is_numeric($dateFormat[2])){
										$strMes = "Erreur : Le champ ann�e de la date � la ligne ".$i.
                      " et � la colonne ".$tabKeys[$j]." est incorrecte.";
										$this->obj_Erreur->erreur(7,$strMes);
										$intTest++;
									}
								}else{
									$strMes = "Erreur : Le champ date de la ligne ".$i.
                    " et de la colonne ".$tabKeys[$j]." doit etre au format JJ/MM/AAAA";
									$this->obj_Erreur->erreur(7,$strMes);
									$intTest++;											
								}
							}
						}
					}
					$i++;
				}
			}
		}
		
		// fermeture du fichier d'import
		if(file_exists($this->strFichier_Import))
      fclose($fImport);

		if( $intTest > 0 ) {
			$this->obj_Erreur->erreur(6);
		}
		
		return $intTest;
	}
	
  /**
   * @brief M�thode virtuelle effectuant une m�j par rapport � une ligne du fichier d'import
   *
   * @param tabLigne
   * @param tabCopy
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @param t : num ligne
   * @return Retourne un entier : 0 si aucune erreur, >0 sinon
   */
  function update($tabLigne, $tabCopy, $tabLigneRef,$t) { }
  
  /**
   * @brief M�thode virtuelle effectuant la suppression par rapport � une ligne du fichier d'import
   *
   * @param tabLigne
   * @param tabCopy
   * @return Retourne un entier : 0 si aucune erreur, >0 sinon
   */
  function delete($tabLigne, $tabCopy, $tabLigneRef,$t) { }
  
  /**
   * @brief M�thode virtuelle effectuant un ajout par rapport � une ligne du fichier d'import
   *
   * @param tabLigne
   * @param tabCopy
   * @return Retourne un entier : 0 si aucune erreur, >0 sinon
   */
  function insert($tabLigne, $tabCopy, $tabLigneRef,$t) { }

  /**
   * @brief Integre le fichier d'import
   *
   * @param intSeparateur  caractere s�parateur du fichier
   * @return Retourne un entier : 0 si aucune erreur, >0 sinon
   */
  function integrate($intSeparateur) 
  {
    $iTotRes = 0;
    $tabCopy = array();
    $fImport = fopen($this->strFichier_Import, "r");
    $i = 2;
	
    if( $fImport ) {
      // lecture de la premiere ligne
      $intTailleFichier = filesize($this->strFichier_Import);
      $tabLigneRef = fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
      for ($j=0; $j<count($tabLigneRef); $j++) {
        $tabCopy[$tabLigneRef[$j]] = $j;
      }

      while( !feof($fImport) ) {
        $oRes = 0;
        $tabLigne = fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
        if( $tabLigne == NULL )	{
          // on ignore les lignes vides
        } else {
          switch ($tabLigne[$tabCopy["MODE"]]){
					case "S":
						$oRes = $this->delete($tabLigne, $tabCopy, $tabLigneRef,$i);			
            break;
          case "M":
						$oRes = $this->update($tabLigne, $tabCopy, $tabLigneRef,$i);	
            break;
					case "C":
            $oRes = $this->insert($tabLigne, $tabCopy, $tabLigneRef,$i);
            break;
          }
        }
        $iTotRes += $oRes;
        $i++;
      }
    }
    return $iTotRes;
  }

}			

?>