<?php
class Arbre {

  var $noeuds;
  var $racine;

  function Arbre () {
    $this->noeuds = array( 0 => array( 'val' => NULL , 'fils' => NULL , 'pere' => NULL, 'niv' => 0) );
    $this->racine = 0;
  }

  /* v�rifie que l'arbre est valide */
  function valide () {
    return array_key_exists($this->racine, $this->noeuds);
  }

  /* remonter d'un noeud */
  function remonter () {
    if (!$this->valide()) return FALSE;
    if ($this->racine != 0) {
      /* on va trouver le pere de la racine actuelle */
      for ($noeud=0; $noeud<sizeof($this->noeuds); $noeud++) {
        /* noeuds[i]['fils'] = fils o� fils est un tableau d'entiers */
        $fils = $this->noeuds[$noeud]['fils'];
        if (is_array($fils) && in_array($this->racine, $fils)) {
          $this->racine = $noeud;
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /* descendre vers le $i_eme fils du noeud actuel */
  /* $i = 1 : premier fils */
  function descendre ($i) {
    if (!$this->valide()) return FALSE;
    $noeuds = $this->noeuds[$this->racine];
    if ($i<=0 || $i>sizeof($noeuds['fils'])) return FALSE;
    $racine = $this->racine; // sauvegarde
    $this->racine = $noeuds['fils'][$i-1];
    if (!$this->valide()) {
      echo "INVALIDE!";
      /* le noeud vis� est invalide ! il n'est pas d�fini */
      $this->racine = $racine;
      return FALSE;
    }
    return TRUE;
  }

  /* cr�e un nouveau fils vierge */
  function ajouter_fils () {
    if (!$this->valide()) return FALSE;
    // on ajoute la r�f�rence
    $ref = sizeof($this->noeuds);
    $this->noeuds[$this->racine]['fils'][] = $ref;
    // on cr�e le noeud effectif
    $this->noeuds[$ref] = array( 'val' => NULL , 'fils' => NULL , 'pere' => $this->racine, 'niv' =>  $this->noeuds[$this->racine]['niv']+1);
    return $this->nb_fils();
  }

  /* renvoie le nombre de fils */
  function nb_fils () {
    if (!$this->valide()) return FALSE;
    return sizeof($this->noeuds[$this->racine]['fils']);
  }

  /** positionne le root sur le noeud correspondant � la page idPage */
  function goToRoot($idPage)
  {
    reset($this->noeuds);
    while( list($idKey, $tabVal) = each($this->noeuds)) {
      $tabVal = $tabVal["val"];
      if( is_array($tabVal) && array_key_exists("id", $tabVal) && $tabVal["id"] == $idPage ) {
        $this->racine = $idKey;
        break;
      }
    }
  }

  /* renvoie la valeur du noeud courant */
  function valeur () {
    if (!$this->valide()) return FALSE;
    return $this->noeuds[$this->racine]['val'];
  }


  /* renvoie le niveau de profondeur dans l'arbre */
  function niveau () {
    if (!$this->valide()) return FALSE;
    return $this->noeuds[$this->racine]['val'];
  }

  /* donne une valeur au noeud courant */
  function modif_valeur ($valeur) {
    if (!$this->valide()) return FALSE;
    $this->noeuds[$this->racine]['val'] = $valeur;
    return TRUE;
  }

  /* cr�e un fils initialis� a la valeur donn�e */
  function creer_fils ($valeur) {
    if( !$this->ajouter_fils() ) return FALSE;
    if( !$this->descendre($this->nb_fils()) ) return FALSE;
    if( !$this->modif_valeur($valeur) ) return FALSE;
    if( !$this->remonter() ) return FALSE;
  }

  /* renvoie la forme textuelle de la valeur du noeud courant */
  function valeur_texte () {
    $noeud = $this->noeuds[$this->racine];
    if (isset($noeud['val'])) switch (gettype($noeud['val'])) {
      case "boolean":
        return $noeud['val'] ? 'TRUE' : 'FALSE';
      case "string":
        return '"' . $noeud['val'] . '"';
      case "integer":
      case "double":
        return (string) $noeud['val'];
      case "array":
        return "Array";
      case "object":
        return "Object";
      case "resource":
        return "Resource";
      case "user function":
        return "Function";
      default:
        return "Undefined";
    }
    else {
      return '';
    }
  }

  /* renvoie la forme textuelle */
  function texte () {
    $noeud = $this->noeuds[$this->racine];
    $str = $this->valeur_texte() . "\n";
    for ($i=0; $i<$this->nb_fils(); $i++) {
      $str .= '+-- ';
      $this->descendre($i+1);
      /* on r�cup�re le texte du fils */
      $substr = explode("\n", $this->texte());
      if (sizeof($substr)>0) $str .= array_shift($substr) . "\n";
      foreach ($substr as $line) if ($line != '') {
        $str .= $i<$this->nb_fils()-1 ? '|' : ' ';
        $str .= '   ' . $line . "\n";
      }
      /* et on remonte */
      $this->remonter();
    }
    return $str;
  }

  /* renvoie la forme textuelle au format html */
  function html () {
    $noeud = $this->noeuds[$this->racine];
    $str = $this->valeur_texte() . '<br/><ul>';
    for ($i=0; $i<$this->nb_fils(); $i++) {
      $this->descendre($i+1);
      $str .= '<li>' . $this->html() . '</li>';
      $this->remonter();
    }
    $str .= '</ul>';
    return $str;
  }


}



/** fonction utilitaire pour faciliter la cr�ation d'arbres **/


function creer_arbre ($val, $fils, $a)
{
  $a->modif_valeur($val);
  if (is_array($fils)) {
    foreach ($fils as $v => $f) {
      $a->ajouter_fils();
      $a->descendre($a->nb_fils());
      creer_arbre($v, $f, &$a);
      $a->remonter();
    }
  }
}


?>