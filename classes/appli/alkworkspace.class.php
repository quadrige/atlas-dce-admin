<?php

/**
 *  @brief Classe d'un espace de travail.
 */
class AlkWorkSpace extends AlkObject
{
  /** R�f�rence vers l'ojet interface source de donn�es espace */
  var $oQuery;

  /** R�f�rence vers l'ojet interface action source de donn�es espace */
  var $oQueryAction;

  /** R�f�rence vers l'ojet interface source de donn�es annuaire */
  var $oQueryAnnu;
  
  /** R�f�rence vers l'ojet interface action des donn�es annuaire */
  var $oQueryAnnuAction;

  /** R�f�rence ver l'objet interface actions courantes sur annuaire et espace */
  var $oQueryContAnnuAction;

  /** Indentifiant de l'espace */
  var $cont_id;

  /** Identifiant de l'application en cours */
  var $appli_id;

  /** DataRow associ� � l'espace en cours */
  var $oDrCont;

  /** r�f�rence vers l'objet template smarty */
  var $oTemplate;
  
  /** Type de la base de donn�es */
  var $typeDB;
  
  /** Connexion � la base de donn�es */
  var $dbConn;
  
  /** Profil de l'utilisateur connect� : 1=admin principal, 2=animateur, 3=utilisateur */
  var $iProfilCont;

  /** Droit de l'utilisateur sur l'application en cours : 0=aucun, 1=utilisation, 2>=util + admin */
  var $iDroitAppli;

  /** Droit de l'utilisateur sur l'annuaire : 
   *  1=utilisation + admin tout l'annuaire, 
   *  2=utilisation + admin service
   *  3=utilisation 
   */
  var $iDroitAnnu;

  /** Droit de g�rer les droits sur les espaces accessibles sur l'annuaire : 
   *  0= n'a pas le droit
   *  1= a le droit
   */
  var $iAdminDroitAnnu;

  /** Identifiant de l'utilisateur connect� */
  var $agent_id;

  /** Identifiant du profil utilisateur connect� */
  var $profil_id;

  /** tableau contenant les informations relatives � l'application en cours */
  var $tabUrl;
  var $tabAppli;
  var $tabMenuAppli;
  var $tabSsMenuAppli;
  var $tabMenuEspace;
  var $tabSsMenuEspace;
  var $tabMenuEspaceFrere;
  var $tabSsMenuEspaceFrere;

  /** Identiifant de l'onglet et sous onglet s�lectionn� */
  var $iSheet;
  /** Tableau contenant les informations d'onglets */
  /** Tableau de type [0..m][0..n-1]["idSheet"] => identifiant de l'onglet
   *                                ["text"]    => texte sur l'onglet 
   *                                ["title"]   => info bulle sur l'onglet  
   *                                ["url"]     => adresse du lien
   *                                ["visible"] => vrai ou faux
   *                                ["target"]  => nom de la fen�tre cible 
   * m : type d'onglet : 0= partie consultation, 1= partie administration, 2= partie personnalisation ou param�trage
   * n : nombre d'onglets par type
   * p : nombre de sous onglets par onglet
   */
  var $tabSheets;


  /**
   * @brief Constructeur par d�faut
   *
   * @param cont_id          Identifiant de l'espace en cours
   * @param appli_id         Identifiant de l'application en cours
   * @param dbConn           Classe de connexion � la source de donn�es
   * @param typeDB           Type de source de donn��s
   * @param queryCont        R�f�rence vers l'interface dataSrc de l'espace
   * @param queryAnnu        R�f�rence vers l'interface dataSrc de l'annuaire
   * @param agent_id         Identifiant de l'utilisateur connect�
   * @param profil_id        Identifiant du profil utilisateur connect�
   * @param agent_admin_annu =1 si l'agent est super admin de service, =0 sinon
   */
  function AlkWorkSpace($cont_id, $appli_id, &$dbConn, $typeDB, &$queryCont, 
                        &$queryAnnu, &$queryContAnnuAction, $agent_id="-1", 
                        $profil_id="-1", 
                        $ALK_B_REMOVE_PRIV_PROFIL1_SPACE=false,
                        $agent_admin_annu=0)
  {  	
    $this->cont_id = $cont_id;
    $this->appli_id = ($appli_id=="" || $appli_id=="0" ? "-1" : $appli_id);
    $appli_id = $this->appli_id;
    $this->dbConn = &$dbConn;
    $this->typeDB = $typeDB;
    $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE = $ALK_B_REMOVE_PRIV_PROFIL1_SPACE;

    $this->agent_id = $agent_id;
    $this->profil_id = $profil_id;
    $this->agent_admin_annu = $agent_admin_annu;

    $this->oQuery =& $queryCont;
    $this->oQueryAction = null;
    $this->oQueryAnnu =& $queryAnnu;
    $this->oQueryContAnnuAction =& $queryContAnnuAction;
    $this->oQueryAnnuAction = null;

    $this->tabUrl = array();
    $this->tabAppli = array();
    $this->tabMenuAppli = array();
    $this->tabSsMenuAppli = array();
    $this->tabMenuEspace = array();
    $this->tabSsMenuEspace = array();
    $this->tabMenuEspaceFrere = array();
    $this->tabSsMenuEspaceFrere = array();

    $this->iProfilCont = 99;
    $this->iDroitAppli = 0;
    $this->iDroitAnnu = 3;
    $this->iAdminDroitAnnu = 0;

    if( $agent_id != "-1" )
      $this->_SetProfil();
    
    if( $appli_id != "-1" )
      $this->SetAppliProperty("", true);
      
    $dsCont = $this->oQuery->GetDs_ficheEspaceById($this->cont_id);
    $this->oDrCont = $dsCont->getRowIter();

    $this->iSheet = 0;
    $this->tabSheets = array();
    $this->initTemplate();
    $this->setTabSheets();
  }

  /**
   * @brief Affecte l'objet Template si l'option est activ�e
   */
  function initTemplate()
  {
    $this->oTemplate = null;
    if( defined("ALK_B_TEMPLATE") && ALK_B_TEMPLATE==true ) {
      include_once("../../classes/template/Smarty.class.php");
      $this->oTemplate = new Smarty();
      
      $this->oTemplate->compile_dir = ALK_SIALKE_PATH.( defined("ALK_PATH_TEMPLATES_C") ? ALK_PATH_TEMPLATES_C : "upload/template_c" );
      // necessaire d'instancier template_dir avant execution du template: ../[appli]/template
      $this->oTemplate->template_dir = "";
    }
  }

  /**
   * @brief Retourne une propri�t� de l'espace
   *
   * @param strPropertyName Nom de la propri�t�
   * @return Retourne une chaine
   */
  function getProperty($strPropertyName)
  {
    if( $this->oDrCont )
      return $this->oDrCont->getValueName($strPropertyName);
  }

  /**
   * @brief Affecte � la propri�t� iProfilCont l'identifiant du profil utilisateur par rapport � l'espace courant
   */
  function _setProfil()
  {
    // profil inexistant par d�faut   
    $this->iProfilCont = 99;
    if( $this->profil_id == "1" ) {
      // administrateur principal
      $this->iProfilCont = 1;
    } else {
      $dsAgent = $this->oQuery->GetDs_accesEspaceAgentById($this->cont_id, $this->agent_id);
      if( $drAgent = $dsAgent->getRowIter() ) {
        $cont_admin = $drAgent->getValueName("CONT_ADMIN");
        // 2=animateur, 3=utilisateur
        $this->iProfilCont = ( $cont_admin == "1" ? "2" : "3" );
      }
    }

    // aucun droit par d�faut
    $this->iDroitAppli = 0;
    if( $this->profil_id == "1" ) {
      $this->iDroitAppli = ((defined("ALK_B_RIGHT_PUB") && ALK_B_RIGHT_PUB==true) ? 4 : 2);
    } else {
      $dsAgent = $this->oQuery->GetDs_accesAppliAgentById($this->appli_id, $this->agent_id);
      if( $drAgent = $dsAgent->getRowIter() )
        $this->iDroitAppli = $drAgent->getValueName("APPLI_DROIT_ID");
    }
    
    $iAgentAdminAnnu = $this->agent_admin_annu+0;

    $this->iDroitAnnu = 3;
    // droit � l'admin sur l'annuaire : profil
    if( $this->iProfilCont==1 || $this->profil_id==1 ) {
      $this->iDroitAnnu = 1;
    } elseif( defined("ALK_B_ADMIN_ANIM_ANNU") && ALK_B_ADMIN_ANIM_ANNU==true && ($iAgentAdminAnnu & 1) == 1 ) {
      $this->iDroitAnnu = 1;
      /*    } elseif( defined("ALK_B_ADD_ANIM_ANNU") && ALK_B_ADD_ANIM_ANNU==true && $this->agent_admin_annu=="2" ) {
      $this->iDroitAnnu = 1;*/
    } elseif( $this->profil_id == 2  || (defined("ALK_I_PROFIL_DROIT_ANNU") && $this->profil_id == ALK_I_PROFIL_DROIT_ANNU)) {
      $this->iDroitAnnu = 2;
    }

    $this->iAdminDroitAnnu = 0;
    if( $this->iProfilCont<=2 || $this->profil_id==1 ) {
      $this->iAdminDroitAnnu = 1;
    } elseif( defined("ALK_B_ADMIN_DROIT_ANNU") && ALK_B_ADMIN_DROIT_ANNU==true && ($iAgentAdminAnnu & 2) == 2 ) {
      $this->iAdminDroitAnnu = 1;
    }
  }

  /**
   * @brief Affecte une interface action associ�e aux espaces
   *
   * @param queryContAction R�f�rence vers l'interface action
   */
  function setQueryAction(&$queryContAction)
  {
    $this->oQueryAction =& $queryContAction;
  }

  /**
   * @brief Affecte une interface action associ�e � l'annuaire
   *
   * @param queryAnnuAction R�f�rence vers l'interface action
   */
  function setQueryAnnuAction(&$queryAnnuAction)
  {
    $this->oQueryAnnuAction =& $queryAnnuAction;
  }

  //
  // M�thodes � l'ajout / modif / suppr d'espace
  //

  /**
   * @brief Cr�ation d'un espace (param�tres li�s � la base)
   *
   * @return Retourne un entier : identifiant de l'espace cr��
   */
  function add_espace($cont_intitule, $cont_intitule_court, $cont_desc, $cont_public,
                      $cont_pere, $cont_rang, $cont_logoBiblio, $cont_visible, 
                      $cont_reserve, $strParamNameLogo, $strPathUpload, $typecrea,
                      $tabAdmin, $bDroitPub, $bAddAdmin, $cont_logoBiblio2="")
  {
    $cont_niveau = $this->getProperty("CONT_NIVEAU")+1;
    $cont_arbre = $this->getProperty("CONT_ARBRE");
    $cont_pere_public = $this->getProperty("CONT_PUBLIC");
    $cont_pere_reserve = $this->getProperty("CONT_RESERVE");
    
    if( $typecrea>0 ) {
      // recopie les propri�t�s communes dans le cas d'un espace clone
      $cont_public = $cont_pere_public;
      $cont_reserve = $cont_pere_reserve;
    }

    $cont_id = $this->oQueryAction->add_espace($this->agent_id, $this->profil_id, $cont_intitule, 
                                               $cont_intitule_court, $cont_desc, $cont_public, 
                                               $cont_pere, $cont_rang, $cont_logoBiblio, $cont_visible, 
                                               $cont_reserve, $cont_niveau, $cont_arbre,
                                               $strParamNameLogo, $strPathUpload, $cont_logoBiblio2);

    // collectionne les id animateurs
    $strListAdminId = "";
    if( $typecrea<2  && ($this->profil_id=="1" || $bAddAdmin==true) ) {
      // ceux s�lectionn�s
      $strListAdminId = $this->agent_id;
      if( is_array($tabAdmin) && count($tabAdmin)>0 ) {
        foreach($tabAdmin as $admin_id) 
          $strListAdminId .= ( $admin_id != "" ? ",".$admin_id : "" );
      }

      // ceux par d�faut
      $strSql = "select AGENT_ID from SIT_AGENT where AGENT_VALIDE>0 and PROFIL_ID=1";
      $dsAgent = $this->dbConn->initDataSet($strSql);
      while( $drAgent = $dsAgent->getRowIter() ) {
        $admin_id = $drAgent->getValueName("AGENT_ID");
        $strListAdminId .= ",".$admin_id;
      }
  
      // applique les droits sur les applis de cet espace pour les animateurs s�lectionn�s
      $this->add_agentToEspace($bDroitPub,  $strListAdminId, "1", $cont_id);
    }

    switch( $typecrea ) {
    case 0: // espace vierge
      // si le conteneur est publique
      if( $cont_public == 1 ) {
        // ajoute les agents appartenant � un service de type �tat
        $this->oQueryAction->add_agentNonAnimateurToEspacePublic($cont_id);

        // applique les droits par d�faut aux applications du conteneur
        // pas n�cessaires puisque pas d'applis � ce moment la
      }
      break;

    case 1: // recopie les applis de l'espace parent + applications des droits pour animateurs
      $this->copy_appliEspace2Espace($cont_id, $bDroitPub);
      break;

    case 2: // recopie les utilisateurs de l'espace parent
      $this->oQueryAction->copy_agentEspace2Espace($cont_pere, $cont_id);
      break;

    case 3: // recopie les applis et les utilisateurs + droits sur les utilisateurs
      $this->oQueryAction->copy_agentEspace2Espace($cont_pere, $cont_id);
      $this->copy_appliEspace2Espace($cont_id, $bDroitPub);
      break;
    }

    return $cont_id;
  }

  /**
   * @brief Modification d'un espace (param�tres li�s � la base)
   *
   */
  function maj_espace($cont_intitule, $cont_intitule_court, $cont_desc, $cont_public,
                      $cont_pere, $cont_rang, $cont_logoBiblio, $cont_visible, 
                      $cont_reserve, $strParamNameLogo, $iLogoSup, $strPathUpload, 
                      $tabAdmin, $cont_reserve_old, $bDroitPub, $bAddAdmin,
                      $cont_logoBiblio2="", $iLogoSup2=0)
  {
    // recupere les infos avant modif
    $cont_rang_old = $this->getProperty("CONT_RANG");
    $cont_logo_old = $this->getProperty("CONT_LOGO");
    $cont_logo2_old = $this->getProperty("CONT_LOGO2");
    $cont_public_old = $this->getProperty("CONT_PUBLIC");
    $cont_alias_old = $this->getProperty("CONT_INTITULE_COURT");
   
    $bRes = $this->oQueryAction->maj_espace($this->agent_id, $this->profil_id, $this->cont_id, $cont_intitule, 
                                            $cont_intitule_court, $cont_desc, $cont_public, $cont_pere, $cont_rang, 
                                            $cont_logoBiblio, $cont_visible, $cont_reserve, $strParamNameLogo, 
                                            $iLogoSup, $strPathUpload, $cont_rang_old, $cont_logo_old, $cont_alias_old,
                                            $cont_logoBiblio2, $iLogoSup2, $cont_logo2_old);
    
    // collectionne les id animateurs
    $strListAdminId = "";
    if( $this->profil_id=="1" || $bAddAdmin==true ) {
      // ceux s�lectionn�s
      $strListAdminId = $this->agent_id;
      $strListAdminToDel = $this->agent_id;
      if( is_array($tabAdmin) && count($tabAdmin)>0 ) {
        foreach($tabAdmin as $admin_id) {
          $strListAdminId .= ( $admin_id != "" ? ",".$admin_id : "" );
          $strListAdminToDel .= ( $admin_id != "" ? ",".$admin_id : "" );
        }
      }

      // ceux par d�faut
      $strSql = "select AGENT_ID from SIT_AGENT where AGENT_VALIDE>0 and PROFIL_ID=1";
      $dsAgent = $this->dbConn->initDataSet($strSql);
      while( $drAgent = $dsAgent->getRowIter() ) {
        $admin_id = $drAgent->getValueName("AGENT_ID");
        $strListAdminId .= ",".$admin_id;
      }

      // ceux � supprimer
      $strSql = "select AGENT_ID from SIT_AGENT_CONT where CONT_ID=".$this->cont_id." and CONT_ADMIN=1";
      $dsAgent = $this->dbConn->initDataSet($strSql);
      while( $drAgent = $dsAgent->getRowIter() ) {
        $admin_id = $drAgent->getValueName("AGENT_ID");
        $strListAdminToDel .= ",".$admin_id;
      }
  
      // applique les droits sur les applis de cet espace pour les animateurs s�lectionn�s
      $this->add_agentToEspace($bDroitPub,  $strListAdminId, "1", "-1", $strListAdminToDel);
    }
    
    // cas ou l'espace devient publique
    if( $cont_public_old!=$cont_public && $cont_public==1 ) {
      // ajoute les agents appartenant � un service de type �tat
      $this->oQueryAction->add_agentNonAnimateurToEspacePublic($this->cont_id);
   
      //applique les droits par d�faut aux applications du conteneur aux agents
      $this->set_droitProfilToAgentByEspace($bDroitPub);
    }
  }

  /**
   * @brief Suppression de l'espace courant
   *        Suppose que l'espace est vide : pas d'appli, pas de fils
   *
   * @return Retourne un entier : identifiant de l'espace parent 
   */
  function del_espace()
  {
    $cont_rang = $this->getProperty("CONT_RANG");
    $cont_logo = $this->getProperty("CONT_LOGO");
    $cont_pere = $this->getProperty("CONT_PERE");
    $strAlias = $this->getProperty("CONT_INTITULE_COURT");

    $this->oQueryAction->del_espace($this->cont_id, $cont_rang, $cont_logo, $cont_pere, 
                                    $strAlias, $this->oQueryAnnuAction);

    return $cont_pere;
  }

  /**
   * @brief Dupplique l'espace cont_id_copy et l'accroche comme fils � cont_id_dest
   *        Copie applications, utilisateurs et droits
   * 
   * @param cont_id_copy  Espace � copier
   * @param cont_id_dest   Espace parent du nouvel espace copi�
   */
  function copy_espace($cont_id_copy, $cont_id_dest, $bDroitPub, $bAddAdmin)
  {
    $oSpaceCopy = new AlkWorkspace($cont_id_copy, "-1", $this->dbConn, $this->typeDB, 
                                   $this->oQuery, $this->oQueryAnnu, $this->oQueryContAnnuAction, 
                                   $this->agent_id, $this->profil_id);
    $oSpaceCopy->setQueryAction($this->oQueryAction);

    $cont_intitule = "Copie de ".$oSpaceCopy->getProperty("CONT_INTITULE");
    $cont_desc = $oSpaceCopy->getProperty("CONT_DESC");
    $cont_public = $oSpaceCopy->getProperty("CONT_PUBLIC");
    $cont_reserve = $oSpaceCopy->getProperty("CONT_RESERVE");
    $cont_pere = $cont_id_copy;
    $cont_rang = "1";

    // recopie appli + util + droits
    $typecrea = 3; 
    $tabAdmin = array();

    // cr�ation de l'espace copi� comme espace fils de cont_id_copy
    $cont_id = $oSpaceCopy->add_espace($cont_intitule, "", $cont_desc, $cont_public,
                                       $cont_pere, $cont_rang, "", "0", 
                                       $cont_reserve, "", "", $typecrea, 
                                       $tabAdmin, $bDroitPub, $bAddAdmin);

    // d�placement de l'espace cr��
    $this->oQueryAction->move_Espace($cont_id, $cont_id_dest);

    $oSpaceCopy = null;
  }

  //
  // M�thodes � l'ajout / modif / suppr / copie d'application
  //

  /**
   * @brief Ajoute une application sur l'espace en cours
   *        anciennement lib_appli.php : creerAppli()
   *
   * @param appli_intitule   Intitul� de l'application
   * @param atype_id         Type de l'application
   * @param appli_defaut     =1 si appplication par d�faut, =0 sinon
   * @param appli_rang       rang d'affichage
   * @param bDroitPub        =vrai si droit de publication sur les applis, faux sinon
   * @param appli_id_src     =-1 par d�faut. Contient d'identifiant de l'application 
   *                         duppliqu�e pour permettre la copie des droits sur profil 
   * @return Retourne un entier : l'identifiant de l'application cr��e
   */
  function add_application($appli_intitule, $atype_id, $appli_defaut, $appli_rang, $bDroitPub, $appli_id_src="-1", $appli_id_dist="-1")
  {
    // cr�ation de l'application
    $appli_id_defaut = $this->getProperty("CONT_APPLI_DEFAUT");
    $appli_id = $this->oQueryAction->add_application($this->cont_id, $appli_intitule, $atype_id, 
                                                     $appli_defaut, $appli_rang, $appli_id_defaut,
                                                     $bDroitPub, $appli_id_src, $appli_id_dist);
    
    // fixe les droits aux agents de l'espace / aux droits sur profil
    $this->oQueryAction->set_droitProfilToAgentByAppli($this->cont_id, $appli_id, $bDroitPub);

    // Initialisation de l'application cr��e
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
    $oRes = $appli_intitule;
    $oService->callService("Create", $oRes, $this->cont_id);
    
    return $appli_id;
  }

  /**
   * @brief Modifie les propri�t�s d'un application sur l'espace en cours
   *
   * @param appli_id         Indentifiant de l'application
   * @param appli_intitule   Intitul� de l'application
   * @param atype_id         Type de l'application
   * @param appli_defaut     =1 si appplication par d�faut, =0 sinon
   * @param appli_rang       rang d'affichage
   * 
   * @return Retourne un entier : l'identifiant de l'application cr��e
   */
  function maj_application($appli_id, $appli_intitule, $atype_id, $appli_defaut, $appli_rang)
  {
    $appli_id_defaut = $this->getProperty("CONT_APPLI_DEFAUT");
    $this->oQueryAction->maj_application($this->cont_id, $appli_id, $appli_intitule, 
                                         $appli_defaut, $appli_rang, $appli_id_defaut);
    
    // applique les modifications � l'application si n�cessaire
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
    $oRes = null;
    $oService->callService("Update", $oRes, $this->cont_id, $appli_intitule);
  }

  /**
   * @brief Retire l'application de l'espace puis la supprime
   * 
   * @param appli_id Identifiant de l'application � supprimer
   * @param atype_id Type de l'application
   */
  function del_application($appli_id, $atype_id)
  {
    // Suppression Initialisation de l'application cr��e
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
    $oRes = null;
    $oService->callService("Delete", $oRes, $this->cont_id);

    // suppression de l'application
    $appli_id_defaut = $this->getProperty("CONT_APPLI_DEFAUT");
    $appli_id = $this->oQueryAction->del_application($this->cont_id, $appli_id, $appli_id_defaut);
  }

  /**
   * @brief Recopie les applications de cet espace vers cont_id_dest
   *
   * @param cont_id_dest        Identifiant du conteneur destination
   * @param bDroitPub           =vrai si droit de publication sur les applis, faux sinon
   */
  function copy_appliEspace2Espace($cont_id_dest, $bDroitPub)
  {
    // cr�ation de l'objet workspace destinataire
    $oSpaceDest = new AlkWorkspace($cont_id_dest, "-1", $this->dbConn, $this->typeDB, 
                                   $this->oQuery, $this->oQueryAnnu, $this->oQueryContAnnuAction, 
                                   $this->agent_id, $this->profil_id);

    $oSpaceDest->setQueryAction($this->oQueryAction);
    
    $appli_id_defaut = $this->getProperty("CONT_APPLI_DEFAUT");

    $dsAppli = $this->oQuery->GetDs_listeAppliByEspace($this->cont_id);
    while( $drAppli = $dsAppli->getRowIter() ) {
      $appli_id_src = $drAppli->getValueName("APPLI_ID");
      $appli_intitule = $drAppli->getValueName("APPLI_INTITULE");
      $atype_id = $drAppli->getValueName("ATYPE_ID");
      $appli_rang = $drAppli->getValueName("APPLI_RANG");
      
      $appli_defaut = ($appli_id_defaut==$appli_id_src ? "1" : "0");

      $appli_id = $oSpaceDest->add_application($appli_intitule, $atype_id, 
                                               $appli_defaut, $appli_rang, $bDroitPub, $appli_id_src);
    }

    $oSpaceDest = null;
  }

  //
  // M�thodes sur la gestion des droits
  //
 
  /**
   * @brief Applique les droits des profils aux agents de l'espace
   *        pour l'application appli_id
   *
   * @param appli_id   Identifiant de l'application � supprimer
   * @param atype_id   Type de l'application
   * @param bDroitPub  =vrai si droit de publication sur les applis, faux sinon
   */
  function set_droitProfilToAgentByAppli($appli_id, $atype_id, $bDroitPub)
  {
    // applique les droits profil -> agent pour l'application
    $this->oQueryAction->set_droitProfilToAgentByAppli($this->cont_id, $appli_id, $bDroitPub);

    // Meme action pour la gestion interne de l'application
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
    $oRes = null;
    $oService->callService("SetDroitProfilToAgent", $oRes, $this->cont_id, $bDroitPub);
  }

  /**
   * @brief Applique les droits du profil aux agents de l'espace
   *        pour ses applications
   *
   * @param profil_id   Identifiant de l'application � supprimer
   * @param bDroitPub  =vrai si droit de publication sur les applis, faux sinon
   */
  function set_droitProfilToAgentEspaceByProfil($profil_id, $bDroitPub)
  {
    // applique les droits du profil -> agents pour les applications de l'espace
    $this->oQueryAction->set_droitProfilToAgentEspaceByProfil($this->cont_id, $profil_id, $bDroitPub);
    
    // Meme action pour la gestion interne de l'application
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    $dsAppli = $this->oQuery->GetDs_listeAppliByEspace($this->cont_id);
    while( $drAppli = $dsAppli->getRowIter() ) {
      $appli_id = $drAppli->getValueName("APPLI_ID");
      $atype_id = $drAppli->getValueName("ATYPE_ID");

      $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
      $oRes = null;
      $oService->callService("SetDroitProfilAppliToAgent", $oRes, $this->cont_id, $profil_id, $bDroitPub);
    }
  }

  /**
   * @brief Applique les droits des profils aux agents sur ttes les appli de l'espace
   *
   * @param bDroitPub  =vrai si droit de publication sur les applis, faux sinon
   * @param agent_id   identifiant de l'agent, =-1 pour ts les agents
   * @param cont_id    identifiant de l'espace, si diff�rent de celui caract�ris� par cet objet
   */
  function set_droitProfilToAgentByEspace($bDroitPub, $agent_id="-1", $cont_id="-1")
  {
    $cont_id = ( $cont_id == "-1" ? $this->cont_id : $cont_id );

    $dsAppli = $this->oQuery->GetDs_listeAppliByEspace($cont_id);
    $strListAppliId = "-1";
    while( $drAppli = $dsAppli->getRowIter() ) {
      $strListAppliId .= ",".$drAppli->getValueName("APPLI_ID");
    }

    // applique les droits profil -> agent pour les applications de l'espace
    $this->oQueryAction->set_droitProfilToAgentByAppli($cont_id, $strListAppliId, $bDroitPub, $agent_id);

    // Meme action pour la gestion interne de l'application
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    $dsAppli->moveFirst();
    while( $drAppli = $dsAppli->getRowIter() ) {
      $appli_id = $drAppli->getValueName("APPLI_ID");
      $atype_id = $drAppli->getValueName("ATYPE_ID");

      $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
      $oRes = null;
      $oService->callService("SetDroitProfilToAgent", $oRes, $cont_id, $bDroitPub, $agent_id);
    }
  }

  /**
   * @brief Applique les droits sur les profils pour l'application appli_id
   *
   * @param appli_id   Identifiant de l'application
   * @param atype_id   Type de l'application
   * @param tabDroit   Tableau contenant les couples (profil_id, droit_id)
   * @param bDroitPub  =vrai si droit de publication sur les applis, faux sinon
   */
  function set_droitToProfilByAppli($appli_id, $atype_id, $tabDroit, $bDroitPub)
  {
    // applique les droits profil pour l'application
    $this->oQueryAction->set_droitToProfilByAppli($appli_id, $tabDroit, $bDroitPub);

    // Meme action pour la gestion interne de l'application
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
    $oRes = null;
    $oService->callService("CopyDroitToProfil", $oRes, $this->cont_id, "-1", $bDroitPub);
  }

  /**
   * @brief Modifie les droits du profil pour ttes les applis de l'espace
   *
   * @param profil_id   Identifiant du profil
   * @param tabDroit   Tableau contenant les couples (appli_id, droit_id)
   * @param bDroitPub  =vrai si droit de publication sur les applis, faux sinon
   */
  function set_droitToProfilByEspace($profil_id, $tabDroit, $bDroitPub)
  {
    // applique les droits profil pour ttes les applis de l'espace
    $this->oQueryAction->set_droitToProfilByEspace($this->cont_id, $profil_id, $tabDroit, $bDroitPub);

    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");

    $dsAppli = $this->oQuery->GetDs_listeAppliByEspace($this->cont_id);
    while( $drAppli = $dsAppli->getRowIter() ) {
      $appli_id = $drAppli->getValueName("APPLI_ID");
      $atype_id = $drAppli->getValueName("ATYPE_ID");

      // Meme action pour la gestion interne de l'application
      $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
      $oRes = null;
      $oService->callService("CopyDroitToProfil", $oRes, $this->cont_id, $profil_id, $bDroitPub);
    }
  }

  /**
   * @brief Invite des utilisateurs � un espace suite � une s�lection
   *        faite par le service annuaire de s�lection multiple.
   *
   * @param bDroitPub        =vrai si droit de publication sur les applis, faux sinon
   * @param strListeId       liste d'identifiant d'agent (ou d'animateur si iForceAnim=1)
   * @param iForceAnim       =1 pour forcer l'ajout d'annimateur, -1 par d�faut
   * @param cont_id          identifiant de l'espace en cours de cr�ation, =-1 par d�faut
   * @param strListeIdToDel  liste d'identifiant d'agent � retirer (ou d'animateur si iForceAnim=1)
   */
  function add_agentToEspace($bDroitPub, $strListeId="", $iForceAnim="-1", $cont_id="-1", $strListeIdToDel="")
  {
    if( $strListeId == "" ) {
      // reception de annu/01_users_form.php

      $tabListeAgentId = RequestCheckbox("listeAgentId", REQ_POST, array());
      $tabListeId = array();
      $iCpt = 0;
      $tabListeId[$iCpt] = "";
      foreach($tabListeAgentId as $agent_id) {
        if( $agent_id != "" && $agent_id != "-1" )
          $tabListeId[$iCpt] .= ( $tabListeId[$iCpt] == "" ? "" : "," ).$agent_id;
        if( strlen($tabListeId[$iCpt])>500 ) {
          $iCpt++;
          $tabListeId[$iCpt] = "";
        }
      }
    } else {
      $tabListeId = array($strListeId);
    }

    $cont_id = ( $cont_id == "-1" ? $this->cont_id : $cont_id );

    $dsAppli = $this->oQuery->GetDs_listeAppliByEspace($cont_id);
    
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    
    for($i=0; $i<count($tabListeId); $i++) {
      $this->oQueryAction->add_agentToEspace($cont_id, $tabListeId[$i], $bDroitPub, $iForceAnim, $strListeIdToDel);
      
      // applique les droits sur les infos de chaque application
      $dsAppli->moveFirst();
      while( $drAppli = $dsAppli->getRowIter() ) {
        $appli_id = $drAppli->getValueName("APPLI_ID");
        $atype_id = $drAppli->getValueName("ATYPE_ID");

        $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
        $oRes = null;
        $oService->callService("AddDroitToAgent", $oRes, $cont_id, 
                               $tabListeId[$i], $bDroitPub, $iForceAnim, $strListeIdToDel);
      }
    }
  }

  /**
   * @brief Retire des utilisateurs d'un espace suite � une s�lection case � cocher
   */
  function del_agentFromEspace()
  {
    $iCptMin = Request("iCptMin", REQ_POST, "0", "is_numeric");
    $iCptMax = Request("iCptMax", REQ_POST, "0", "is_numeric");

    $iCpt = 0;
    $strListeId[$iCpt] = "";
    for($i=$iCptMin; $i<=$iCptMax; $i++) {
      $agent_id = Request("agId_".$i, REQ_POST, "-1", "is_numeric");
      $iSelect = RequestCheckbox("agSelect_".$i, REQ_POST);
      if( $iSelect == "1" && $agent_id != "") {
        $strListeId[$iCpt] .= ( $strListeId[$iCpt] == "" ? "" : "," ).$agent_id;
        if( strlen($strListeId[$iCpt])>500 ) {
          $iCpt++;
          $strListeId[$iCpt] = "";
        }
      }
    }

    $dsAppli = $this->oQuery->GetDs_listeAppliByEspace($this->cont_id);
    
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    
    for($i=0; $i<count($strListeId); $i++) {
      // applique les droits sur les infos de chaque application
      $dsAppli->moveFirst();
      while( $drAppli = $dsAppli->getRowIter() ) {
        $appli_id = $drAppli->getValueName("APPLI_ID");
        $atype_id = $drAppli->getValueName("ATYPE_ID");

        $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
        $oRes = null;
        $oService->callService("DelDroitToAgent", $oRes, $this->cont_id, $strListeId[$i]);
      }

      $this->oQueryAction->del_agentFromEspace($this->cont_id, $strListeId[$i]);
    }
  }

  /**
   * @brief Applique les droits d'un agent sur les applis d'un espace
   *
   * @param agent_id   Identifiant de l'application � supprimer
   * @param tabDroit   Tableau contenant les couples (appli_id, droit_id)
   */
  function set_droitToAgentByEspace($agent_id, $tabDroit)
  {
    $this->oQueryAction->set_droitToAgentByEspace($this->cont_id, $agent_id, $tabDroit);
        
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    
    if( is_array($tabDroit) && count($tabDroit) > 0 ) {
      foreach($tabDroit as $strDroit) {
        $tabTmp = explode("-", $strDroit);
        $atype_id = $tabTmp[0];
        $appli_id = $tabTmp[1];
        $droit_id = $tabTmp[2];

        // applique les droits sur les infos de chaque application
        $oService = new AlkServiceAppli($atype_id, $this, $appli_id, $this->agent_id);
        $oRes = null;
        $oService->callService("SetDroitToAgent", $oRes, $this->cont_id, $agent_id, $droit_id);
      }
    }
  }

  /**
   * @brief Ajout d'un agent suite � une cr�ation : l'invite � tous les espaces publics
   *
   * @param agent_id   Identifiant de l'agent
   * @param profil_id  Identifiant de son profil
   * @param bDroitPub  =vrai si droit de publication sur les applis, faux sinon
   */
  function add_agent($agent_id, $profil_id, $bDroitPub)
  {
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");

    // recherche ts les espaces publics si profil_id!=1
    // recherche ts les espaces profil_id=1
    $dsCont = $this->oQuery->GetDs_listeEspace(($profil_id=="1" ? "-1" : "1"));
    while( $drCont = $dsCont->getRowIter() ) {
      $cont_id = $drCont->getValueName("CONT_ID");
      $this->add_agentToEspace($bDroitPub, $agent_id, "-1", $cont_id);
    }
  }

  /**
   * @brief Transfert la propri�t� d'info d'un utilisateur � un autre
   *        Effectuer avant une mutation ou une suppression
   *
   * @param agentOld_id   Identifiant de l'utilisateur remplac�
   * @param agentNew_id   Identifiant de l'utilisateur rempla�ant
   */
  function ReplaceAgent($agentOld_id, $agentNew_id)
  {
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    
    // pour ts les types d'applications, appeler le service de suppression
    $dsAppliType = $this->oQuery->GetDs_listeTypeAppliByListId("", "", "1");
    
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    
    while( $drAppliType = $dsAppliType->getRowIter() ) {
      $atype_id = $drAppliType->getValueName("ATYPE_ID");

      $oService = new AlkServiceAppli($atype_id, $this, "-1", $this->agent_id);
      $oRes = null;
      $oService->callService("ReplaceAgent", $oRes, $agentOld_id, $agentNew_id);
    }
  }

  /**
   * @brief Suppression d'un agent
   *
   * @param agent_id   Identifiant de l'agent � supprimer
   */  
  function del_agent($agent_id)
  {
    $this->oQueryAction->del_agent($agent_id);

    // pour ts les types d'applications, appeler le service de suppression
    $dsAppliType = $this->oQuery->GetDs_listeTypeAppliByListId("", "", "1");
    
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    
    while( $drAppliType = $dsAppliType->getRowIter() ) {
      $atype_id = $drAppliType->getValueName("ATYPE_ID");

      $oService = new AlkServiceAppli($atype_id, $this, "-1", $this->agent_id);
      $oRes = null;
      $oService->callService("DelAgent", $oRes, $agent_id);
    }
  }

  /**
   * @brief Reapplique les droits du profil � l'agent sur ts les espaces accessibles
   *
   * @param agent_id   Identifiant de l'agent
   * @param profil_id  Identifiant du profil (forcement != 1 )
   * @param bDroitPub  =vrai si droit de publication sur les applis, faux sinon
   */
  function set_droitProfilToAgent($agent_id, $profil_id, $bDroitPub)
  {
    if( $profil_id == "1" ) return;

    // recherche les espaces accessibles
    $dsCont = $this->oQuery->getDs_listeEspaceByAgentForComboMaj($agent_id);
    while( $drCont = $dsCont->getRowIter() ) {
      $cont_id = $drCont->getValueName("CONT_ID");
      $this->set_droitProfilToAgentByEspace($bDroitPub, $agent_id, $cont_id);
    }
  }

  /**
   * @brief Suppression d'un profil
   *
   * @param profil_id   Identifiant du profil � supprimer
   */  
  function del_profil($profil_id)
  {
    $this->oQueryAction->del_profilAppli($profil_id);

    // pour ts les types d'applications, appeler le service de suppression
    $dsAppliType = $this->oQuery->GetDs_listeTypeAppliByListId("", "", "1");
    
    include_once("../../classes/".ALK_DIR_CLASSE_APPLI."alkserviceappli.class.php");
    
    while( $drAppliType = $dsAppliType->getRowIter() ) {
      $atype_id = $drAppliType->getValueName("ATYPE_ID");

      $oService = new AlkServiceAppli($atype_id, $this, "-1", $this->agent_id);
      $oRes = null;
      $oService->callService("DelProfil", $oRes, $profil_id);
    }
  }

  //
  // M�thodes sur la collecte d'information pour construire les menus
  //
  
  /**
   * @brief Retourne un tableau de valeurs n�cessaires � l'affichage du menu espace
   *
   * @param strUrlAccueil  Url de la page accueil si d�j� calcul�e
   * @return Retourne un tableau
   */
  function getTabUrl($ALK_SIALKE_URL, $ALK_DIR_ESPACE,$ALK_DIR_ANNU, 
                     $ALK_SI_MAIL, $ALK_URL_APPLI, $ALK_URL_ON_LOGO, $strUrlAccueil, $strTargetAccueil="")
  {
    if( is_array($this->tabUrl) && count($this->tabUrl)>0 ) return $this->tabUrl;

    // cas de l'acc�s r�serv�
    $strCondProfil = "";
    if( $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE == true ) {
      if( $this->profil_id == "1" )
        $strCondProfil = " and c.CONT_RESERVE=0 ";
    }

    $strUrlAcc = "";
    $strTargetAcc = $strTargetAccueil;
    if( $strUrlAccueil == "" ) {
      // page d'accueil
      // selection du conteneur de niveau de plus faible auquel l'utilisateur � droit de connexion
      $dsCont = $this->oQuery->GetDs_appliDefautByAgent($this->agent_id, "", $strCondProfil);
      if( $drCont = $dsCont->GetRowIter() ) {
        $_iAppliDefaut = $drCont->getValueName("DEFAUT");
        $_strUrlAppli = $drCont->getValueName("ATYPE_URL");
        $_strUrlTargetAppli =  $drCont->getValueName("ATYPE_URL_TARGET");
        $_cont_id = $drCont->getValueName("CONT_ID");
        $_appli_id = $drCont->getValueName("APPLI_ID");
        
        if( $_iAppliDefaut == 1 ) {
          if( $this->_testUrlJs($_strUrlAppli) )
            $strUrlAcc = $this->_getUrlJS($_strUrlAppli, $_cont_id, $_appli_id, "'".$_strUrlTargetAppli."'");
          else {
            $strUrlAcc = ( substr($_strUrlAppli, 0, 4) != "http"
                           ? $ALK_SIALKE_URL."scripts/".$_strUrlAppli."?cont_id=".$_cont_id."&cont_appli_id=".$_appli_id
                           : $_strUrlAppli."?cont_id=".$_cont_id."&cont_appli_id=".$_appli_id );
          }
        } else {
          $strUrlAcc = ( defined("ALK_B_ACCUEIL_SPACE") && ALK_B_ACCUEIL_SPACE==true
                         ? $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."00_accueil.php?cont_id=".$_cont_id
                         : $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."sommaire.php?cont_id=".$_cont_id );
          
          $_strUrlTargetAppli = $strTargetAccueil;
        }
      }
    } else {
      $strUrlAcc = $strUrlAccueil;
    }
  
    // nom de l'agent
    $agent_nom = "";
    $agent_prenom = "";
    $agent_login = "";
    $dsAgent = $this->oQueryAnnu->GetDs_ficheAgent($this->agent_id);
    if( $drAgent = $dsAgent->GetRowIter() ) {
      $agent_nom    = $drAgent->getValueName("AGENT_NOM");
      $agent_prenom = $drAgent->getValueName("AGENT_PRENOM");
      $agent_login  = $drAgent->getValueName("AGENT_LOGIN");
    }

    // autres url utiles
    $strUrlAdmin = "";
    if( $this->iProfilCont <= 2 )
      $strUrlAdmin = $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."04_conteneur_form.php?cont_id=".$this->cont_id;

    $strUrlPlan =  $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."sommaire.php?cont_id=".$this->cont_id;
    $strUrlAnnuaire = $ALK_URL_APPLI."?cont_id=".$this->cont_id;
    $strUrlOnLogo = ( $ALK_URL_ON_LOGO == "" ? $strUrlAcc : $ALK_URL_ON_LOGO );

    $this->tabUrl = array("logo"      => $strUrlOnLogo,
                          "accueil"   => $strUrlAcc,
                          "targetAcc" => $strTargetAcc,
                          "admin"     => $strUrlAdmin,
                          "plan"      => $strUrlPlan,
                          "annuaire"  => $strUrlAnnuaire,
                          "agent"     => array("nom"    => $agent_nom,
                                               "prenom" => $agent_prenom,
                                               "login"  => $agent_login));

    return $this->tabUrl;
  }
  
  /**
   * @brief Retourne une propri�t� de l'application
   *
   * @param atype_id  Type de l'application recherch�e si application transverse aux espaces
   * @return Retourne un tableau
   */
  function setAppliProperty($atype_id="", $bCallByConst=false)
  {
    if( is_array($this->tabAppli) && count($this->tabAppli)>0 ) return $this->tabAppli;
    
    $this->tabAppli = array();

    // cas de l'acc�s r�serv�
    $strCondProfil = "";
    if( $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE == true ) {
      if( $this->profil_id == "1" )
        $strCondProfil = " and c.CONT_RESERVE=0 ";
    }

    if( $atype_id != "" ) {
      // recherche les infos par le type de l'application : cas annuaire
      $dsAppli = $this->oQuery->GetDs_listeTypeAppliByListId($atype_id);
      if( $drAppli = $dsAppli->GetRowIter() )
        $this->tabAppli = array("APPLI_INTITULE"         => $drAppli->getValueName("ATYPE_INTITULE"),
                                "APPLI_DEFAUT"           => "0",
                                "ATYPE_ID"               => $drAppli->getValueName("ATYPE_ID"),
                                "ATYPE_URL"              => $drAppli->getValueName("ATYPE_URL"),
                                "ATYPE_LOGO"             => $drAppli->getValueName("ATYPE_LOGO"),
                                "ATYPE_URL_TARGET"       => $drAppli->getValueName("ATYPE_URL_TARGET"),
                                "ATYPE_URL_ADMIN"        => $drAppli->getValueName("ATYPE_URL_ADMIN"),
                                "ATYPE_ADMIN_LOGO"       => $drAppli->getValueName("ATYPE_ADMIN_LOGO"),
                                "ATYPE_URL_TARGET_ADMIN" => $drAppli->getValueName("ATYPE_URL_TARGET"));
    } else {
      if( $this->appli_id != "-1" ) {
        // recherche les infos sur l'application
        $dsAppli = $this->oQuery->GetDs_ficheEspaceAppliById($this->cont_id, $this->appli_id, $strCondProfil);
        if( $drAppli = $dsAppli->GetRowIter() )
          $this->tabAppli = array("APPLI_INTITULE"         => $drAppli->getValueName("APPLI_INTITULE"),
                                  "APPLI_DEFAUT"           => $drAppli->getValueName("DEFAUT"),
                                  "ATYPE_ID"               => $drAppli->getValueName("ATYPE_ID"),
                                  "ATYPE_URL"              => $drAppli->getValueName("ATYPE_URL"),
                                  "ATYPE_LOGO"             => $drAppli->getValueName("ATYPE_LOGO"),
                                  "ATYPE_URL_TARGET"       => $drAppli->getValueName("ATYPE_URL_TARGET"),
                                  "ATYPE_URL_ADMIN"        => $drAppli->getValueName("ATYPE_URL_ADMIN"),
                                  "ATYPE_ADMIN_LOGO"       => $drAppli->getValueName("ATYPE_ADMIN_LOGO"),
                                  "ATYPE_URL_TARGET_ADMIN" => $drAppli->getValueName("ATYPE_URL_TARGET"));
      }
    }
    
    if( count($this->tabAppli) == 0 && $bCallByConst == false)
      $this->tabAppli = array("APPLI_INTITULE"         => "",
                              "APPLI_DEFAUT"           => "",
                              "ATYPE_ID"               => "",
                              "ATYPE_URL"              => "",
                              "ATYPE_LOGO"             => "",
                              "ATYPE_URL_TARGET"       => "",
                              "ATYPE_URL_ADMIN"        => "",
                              "ATYPE_ADMIN_LOGO"       => "",
                              "ATYPE_URL_TARGET_ADMIN" => "" );
    return $this->tabAppli;
  }

  /**
   * @brief Retourne une propri�t� de l'application
   *
   * @param strPropertyName Nom de la propri�t�
   * @return Retourne une chaine
   */
  function getAppliProperty($strPropertyName)
  {
    if( is_array($this->tabAppli) && array_key_exists($strPropertyName, $this->tabAppli) )
      return $this->tabAppli[$strPropertyName];
    return "";
  }
  
  /**
   * @brief Retourne un tableau contenant les infos n�cessaires au bas de page de l'espace
   *
   * @param iDroitAppli  =-1 par d�faut, pour forcer ou non l'affichage du bouton admin de l'appli
   * @return Retourne un tableau
   */
  function getTabAdminAppli($ALK_SIALKE_URL, $ALK_PATH_UPLOAD_APPLI_LOGO, $iDroitAppli="-1")
  {
    $iDroitAppli = ( $iDroitAppli == "-1" ? $this->iDroitAppli : $iDroitAppli );
    $iDroitAdmin = ( $iDroitAppli >= 2 ? 1 : 0 );

    $strUrlAdmin = $this->getAppliProperty("ATYPE_URL_ADMIN");
    $strUrlAdmin = ( $strUrlAdmin != "" 
                     ? ( substr($strUrlAdmin, 0, 4) != "http" 
                         ? $ALK_SIALKE_URL."scripts/".$strUrlAdmin."?cont_id=".$this->cont_id."&cont_appli_id=".$this->appli_id
                         : $strUrlAdmin."?cont_id=".$this->cont_id."&cont_appli_id=".$this->appli_id )
                     : "#");

    $strTargetAdmin = $this->getAppliProperty("ATYPE_URL_TARGET_ADMIN");

    $strUrlLogo = $this->getAppliProperty("ATYPE_ADMIN_LOGO");
    $strUrlLogo = (  $strUrlLogo != "" ? $strUrlLogo : "admin_base.gif" );
    $strUrlLogo = $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_APPLI_LOGO.$strUrlLogo;

    return array("droit"  => $iDroitAdmin, 
                 "url"    => $strUrlAdmin,
                 "img"    => $strUrlLogo,
                 "target" => $strTargetAdmin);
  }

  /**
   * @brief Retourne le tableau des �lts de menu : liste des applications
   *
   * @param tabAppliMenu Tableau contenant les elts de menu pour l'application en cours
   * @return Retourne un tableau contenant le couple (tableau Appli, tableau sous appli)
   */
  function getTabMenuAppli($tabAppliMenu, $ALK_SIALKE_URL, $ALK_PATH_UPLOAD_APPLI_LOGO)
  {
    if( is_array($this->tabMenuAppli) && count($this->tabMenuAppli)>0 )
      return array($this->tabMenuAppli, $this->tabSsMenuAppli);

    $this->tabMenuAppli = array();
    $this->tabSsMenuAppli = array();

    // cas de l'acc�s r�serv�
    $strCondProfil = "";
    if( $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE == true ) {
      if( $this->profil_id == "1" )
        $strCondProfil = " and c.CONT_RESERVE=0 ";
    }
   
    if( !is_array($tabAppliMenu) ) $tabAppliMenu = array();

    $dsAppli = $this->oQuery->GetDs_listeAppliAgentByEspace($this->agent_id, $this->cont_id, $strCondProfil);
    $cpt = 0;
    while( $drAppli = $dsAppli->GetRowIter() ) {
      $cpt++;
      $_appli_id         = $drAppli->getValueName("APPLI_ID");
      $_atype_id         = $drAppli->getValueName("ATYPE_ID");
      $_strAppliLogo     = $drAppli->getValueName("APPLI_LOGO");
      $_strAppliTypeLogo = $drAppli->getValueName("ATYPE_LOGO");
      $_strUrlAppli      = $drAppli->getValueName("ATYPE_URL");
      $_strTargetAppli   = $drAppli->getValueName("ATYPE_URL_TARGET");
      $_strAppliIntitule = $drAppli->getValueName("APPLI_INTITULE");

      //appli prend la main
      if( count($tabAppliMenu)>0 && $this->appli_id==$_appli_id ) {
        $this->tabMenuAppli[$cpt] = 
          array("url"     => $tabAppliMenu[0]["url"]."&cont_appli_id=".$_appli_id."&cont_id=".$this->cont_id,
                "target"  =>  $_strTargetAppli,
                "titre"   => $this->_tronqueItemMenu($tabAppliMenu[0]["titre"]),
                "logo"    => ( $_strAppliLogo != "" ? $_strAppliLogo : $_strAppliTypeLogo ),
                "id"      => $_appli_id,
                "atypeid" => $_atype_id,
                "nbFils"  => 0);
        $this->tabMenuAppli[$cpt]["logo"] = 
          $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_APPLI_LOGO.$this->tabMenuAppli[$cpt]["logo"];
           
        $cpt2 = 0;
        foreach($tabAppliMenu as $tab) {
          if( $cpt2!=0 ) {
            $this->tabSsMenuAppli[$cpt][$cpt2] = 
              array("url"   => $tab["url"]."&cont_appli_id=".$_appli_id."&cont_id=".$this->cont_id,
                    "titre" => $this->_tronqueItemSsMenu($tab["titre"]),
                    "target" => (array_key_exists("target", $tab) == true ? $tab["target"] : ""));
          }
          $cpt2++;
        }
        $this->tabMenuAppli[$cpt]["nbFils"] = $cpt2-1;
      } else {
        $this->tabMenuAppli[$cpt] = 
          array("url"  => ( $this->_testUrlJs($_strUrlAppli)
                            ? $this->_getUrlJS($_strUrlAppli, $this->cont_id, $_appli_id)
                            : ( substr($_strUrlAppli, 0, 4) != "http"
                                ? $ALK_SIALKE_URL."scripts/".$_strUrlAppli."?cont_appli_id=".$_appli_id."&cont_id=".$this->cont_id
                                : $_strUrlAppli."?cont_appli_id=".$_appli_id."&cont_id=".$this->cont_id) ),
                "target" => $_strTargetAppli,
                "titre"  => $this->_tronqueItemMenu($_strAppliIntitule),
                "logo"   => ( $_strAppliLogo!="" ? $_strAppliLogo : $_strAppliTypeLogo ),
                "id"     => $_appli_id,
                "atypeid" => $_atype_id,
                "nbFils" => 0);
        $this->tabMenuAppli[$cpt]["logo"] = $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_APPLI_LOGO.$this->tabMenuAppli[$cpt]["logo"];
      }
    }

    return array($this->tabMenuAppli, $this->tabSsMenuAppli);
  }

  /**
   * @brief Retourne le tableau des �lts de menu : liste des espaces et ss-espaces
   *
   * @return Retourne un tableau contenant le couple (tableau espace fils, tableau espace petit fils)
   */
  function getTabMenuEspace($ALK_SIALKE_URL, $ALK_DIR_ESPACE, $ALK_PATH_UPLOAD_SPACE_LOGO, $strDefaultTarget="")
  {
    if( is_array($this->tabMenuEspace) && count($this->tabMenuEspace)>0 )
      return array($this->tabMenuEspace, $this->tabSsMenuEspace);
    
    $this->tabMenuEspace = array();
    $this->tabSsMenuEspace = array();

    // cas de l'acc�s r�serv�
    $strCondProfil = "";
    if( $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE == true ) {
      if( $this->profil_id == "1" )
        $strCondProfil = " and c.CONT_RESERVE=0 ";
    }
    
    // selection des espaces fils accessibles avec d�termination de l'appli par defaut
    $cpt=0;
    $dsCont = $this->oQuery->GetDs_listeSousEspaceByAgent($this->agent_id, $this->cont_id, $strCondProfil);
    while( $drCont = $dsCont->GetRowIter() ) {
      $cpt++;
      $_cont_id         = $drCont->getValueName("CONT_ID");
      $_iAppliDefaut    = $drCont->getValueName("DEFAUT");
      $_appli_id        = $drCont->getValueName("APPLI_ID");
      $_strUrlAppli     = $drCont->getValueName("ATYPE_URL");
      $_strTargetAppli  = $drCont->getValueName("ATYPE_URL_TARGET");
      $_strContIntitule = $drCont->getValueName("CONT_INTITULE");
      $_iContPublic     = $drCont->getValueName("CONT_PUBLIC");

      if( $_iAppliDefaut == 1 ) {
        if( $this->_testUrlJs($_strUrlAppli) ) {
          $strUrl = $this->_getUrlJS($_strUrlAppli, $_cont_id, $_appli_id, "'".$_strTargetAppli."'");
          $strUrlTab = $strUrl;
          $strTargetTab = $_strTargetAppli;
        } else {
          $strUrl = $_strUrlAppli."?cont_appli_id=".$_appli_id;
          $strUrlTab = ( substr($strUrl, 0, 4) != "" 
                         ? $ALK_SIALKE_URL."scripts/".$strUrl."&cont_id=".$_cont_id
                         : $strUrl."&cont_id=".$_cont_id );
          $strTargetTab = $_strTargetAppli;
        }
      } else {
        $strUrl = ( defined("ALK_B_ACCUEIL_SPACE") && ALK_B_ACCUEIL_SPACE==true
                    ? $ALK_DIR_ESPACE."00_accueil.php?cont_appli_id=0"
                    : $ALK_DIR_ESPACE."sommaire.php?cont_appli_id=0" );
        $strUrlTab = $ALK_SIALKE_URL."scripts/".$strUrl."&cont_id=".$_cont_id;
        $strTargetTab = $strDefaultTarget;
      }

      $this->tabMenuEspace[$cpt] =
        array("url"    => $strUrlTab,
              "target" => $strTargetTab,
              "titre"  => $this->_tronqueItemMenu($_strContIntitule),
              "public" => $_iContPublic,
              "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO.($_iContPublic==1 ? "cont_public.gif" : "cont_prive.gif"));
    
      // select des conteneurs fils visible de l'utilisateur avec
      // d�termination de l'appli par defaut
      $cptFils = 0;
      $dsContFils = $this->oQuery->GetDs_listeSousEspaceByAgent($this->agent_id, $_cont_id, $strCondProfil);
      while( $drContFils = $dsContFils->GetRowIter() ) {
        $cptFils++;
        $__cont_id         = $drContFils->getValueName("CONT_ID");
        $__iAppliDefaut    = $drContFils->getValueName("DEFAUT");
        $__appli_id        = $drContFils->getValueName("APPLI_ID");
        $__strUrlAppli     = $drContFils->getValueName("ATYPE_URL");
        $__strTargetAppli  = $drContFils->getValueName("ATYPE_URL_TARGET");
        $__strContIntitule = $drContFils->getValueName("CONT_INTITULE");
        $__iContPublic     = $drContFils->getValueName("CONT_PUBLIC");

        $this->tabSsMenuEspace[$cpt][$cptFils] =
          array("url"  => ( $__iAppliDefaut==0 
                            ? ( defined("ALK_B_ACCUEIL_SPACE") && ALK_B_ACCUEIL_SPACE==true
                                ? $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."00_accueil.php?cont_id=".$__cont_id
                                : $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."sommaire.php?cont_id=".$__cont_id )
                            : ( $this->_testUrlJs($__strUrlAppli) 
                                ? $this->_getUrlJS($__strUrlAppli, $__cont_id, $__appli_id)
                                : ( substr($__strUrlAppli, 0, 4) != "http"
                                    ? $ALK_SIALKE_URL."scripts/".$__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id
                                    : $__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id ))),
                "target" => $__strTargetAppli,
                "titre"  => $this->_tronqueItemSsMenu($__strContIntitule),
                "public" => $__iContPublic,
                "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO.($__iContPublic==1 ? "cont_public.gif" : "cont_prive.gif"));
      }
    
      $this->tabMenuEspace[$cpt]["nbFils"] = $cptFils;
    }

    //-------------------------------------------------
    // Recherche les espaces non connect�s directement
    //-------------------------------------------------

    // liste des espaces dont le pere n'est pas accessible
    $dsCont = $this->oQuery->GetDs_listeSousEspaceOrphelinsByAgent($this->agent_id, $this->cont_id, $strCondProfil);
    if( $drCont = $dsCont->GetRowIter()) {
      $cpt++;
      $this->tabMenuEspace[$cpt] =  
        array("url"    => "#",
              "target" => "",
              "titre"  => "Autres espaces",
              "public" => "1",
              "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO."cont_public.gif");
      $cptFils = 0;
      while( $drCont ) {
        $cptFils++;
        $__cont_id         = $drCont->getValueName("CONT_ID");
        $__iAppliDefaut    = $drCont->getValueName("DEFAUT");
        $__appli_id        = $drCont->getValueName("APPLI_ID");
        $__atype_id        = $drCont->getValueName("ATYPE_ID");
        $__strUrlAppli     = $drCont->getValueName("ATYPE_URL");
        $__strTargetAppli  = $drCont->getValueName("ATYPE_URL_TARGET");
        $__strContIntitule = $drCont->getValueName("CONT_INTITULE");
        $__iContPublic     = $drCont->getValueName("CONT_PUBLIC");

        $this->tabSsMenuEspace[$cpt][$cptFils] =
          array("url"  => ( $__iAppliDefaut==0 
                            ? ( defined("ALK_B_ACCUEIL_SPACE") && ALK_B_ACCUEIL_SPACE==true
                                ? $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."00_accueil.php?cont_id=".$__cont_id
                                : $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."sommaire.php?cont_id=".$__cont_id )
                            : (  $this->_testUrlJs($__strUrlAppli) 
                                 ?  $this->_getUrlJS($__strUrlAppli, $__cont_id, $__appli_id)
                                 : ( substr($__strUrlAppli, 0, 4) != "http"
                                     ? $ALK_SIALKE_URL."scripts/".$__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id
                                     : $__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id ))),
                "target" => $__strTargetAppli,
                "titre"  => $this->_tronqueItemSsMenu($__strContIntitule),
                "public" => $__iContPublic,
                "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO.($__iContPublic==1 ? "cont_public.gif" : "cont_prive.gif"));
        $drCont = $dsCont->GetRowIter();
      }
      $this->tabMenuEspace[$cpt]["nbFils"] = $cptFils;
    }
   
    return array($this->tabMenuEspace, $this->tabSsMenuEspace);
  }

  /**
   * @brief Retourne le tableau des �lts de menu : liste des espaces et ss-espaces
   *
   * @return Retourne un tableau contenant le couple (tableau espace fr�res, tableau espace fils)
   */
  function getTabMenuEspaceFrere($ALK_SIALKE_URL, $ALK_DIR_ESPACE, $ALK_PATH_UPLOAD_SPACE_LOGO, $strDefaultTarget="")
  {
    if( is_array($this->tabMenuEspaceFrere) && count($this->tabMenuEspaceFrere)>0 )
      return array($this->tabMenuEspaceFrere, $this->tabSsMenuEspaceFrere);
    
    $this->tabMenuEspaceFrere = array();
    $this->tabSsMenuEspaceFrere = array();

    // cas de l'acc�s r�serv�
    $strCondProfil = "";
    if( $this->ALK_B_REMOVE_PRIV_PROFIL1_SPACE == true ) {
      if( $this->profil_id == "1" )
        $strCondProfil = " and c.CONT_RESERVE=0 ";
    }
    
    $dsCont = $this->oQuery->GetDs_ficheEspaceById($this->cont_id);
    if( $drCont = $dsCont->GetRowIter() ) {
      $idContPere = $drCont->getValueName("CONT_PERE");
    }
    
    // selection des espaces fils accessibles avec d�termination de l'appli par defaut
    $cpt=0;
    $dsCont = $this->oQuery->GetDs_listeSousEspaceByAgent($this->agent_id, $idContPere, $strCondProfil);
    while( $drCont = $dsCont->GetRowIter() ) {
      $cpt++;
      $_cont_id = $drCont->getValueName("CONT_ID");
      $_iAppliDefaut = $drCont->getValueName("DEFAUT");
      $_appli_id = $drCont->getValueName("APPLI_ID");
      $_strUrlAppli = $drCont->getValueName("ATYPE_URL");
      $_strTargetAppli = $drCont->getValueName("ATYPE_URL_TARGET");
      $_strContIntitule = $drCont->getValueName("CONT_INTITULE");
      $_iContPublic = $drCont->getValueName("CONT_PUBLIC");

      if( $_iAppliDefaut == 1 ) {
        if( $this->_testUrlJs($_strUrlAppli) ) {
          $strUrl = $this->_getUrlJS($_strUrlAppli, $_cont_id, $_appli_id, "'".$_strTargetAppli."'");
          $strUrlTab = $strUrl;
          $strTargetTab = $_strTargetAppli;
        } else {
          $strUrl = $_strUrlAppli."?cont_appli_id=".$_appli_id;
          $strUrlTab = ( substr($strUrl, 0, 4) != "http"
                         ? $ALK_SIALKE_URL."scripts/".$strUrl."&cont_id=".$_cont_id
                         : $strUrl."&cont_id=".$_cont_id );
          $strTargetTab = $_strTargetAppli;
        }
      } else {
        $strUrl = ( defined("ALK_B_ACCUEIL_SPACE") && ALK_B_ACCUEIL_SPACE==true
                    ? $ALK_DIR_ESPACE."00_accueil.php?cont_appli_id=0"
                    : $ALK_DIR_ESPACE."sommaire.php?cont_appli_id=0" );
        $strUrlTab = $ALK_SIALKE_URL."scripts/".$strUrl."&cont_id=".$_cont_id;
        $strTargetTab = $strDefaultTarget;
      }

      $this->tabMenuEspaceFrere[$cpt] =
        array("url"    => $strUrlTab,
              "target" => $strTargetTab,
              "titre"  => $this->_tronqueItemMenu($_strContIntitule),
              "public" => $_iContPublic,
              "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO.($_iContPublic==1 ? "cont_public.gif" : "cont_prive.gif"));
    
      // select des conteneurs fils visible de l'utilisateur avec
      // d�termination de l'appli par defaut
      $cptFils = 0;
      $dsContFils = $this->oQuery->GetDs_listeSousEspaceByAgent($this->agent_id, $_cont_id, $strCondProfil);
      while( $drContFils = $dsContFils->GetRowIter() ) {
        $cptFils++;
        $__cont_id = $drContFils->getValueName("CONT_ID");
        $__iAppliDefaut = $drContFils->getValueName("DEFAUT");
        $__appli_id = $drContFils->getValueName("APPLI_ID");
        $__strUrlAppli = $drContFils->getValueName("ATYPE_URL");
        $__strTargetAppli = $drContFils->getValueName("ATYPE_URL_TARGET");
        $__strContIntitule = $drContFils->getValueName("CONT_INTITULE");
        $__iContPublic     = $drCont->getValueName("CONT_PUBLIC");

        $this->tabSsMenuEspaceFrere[$cpt][$cptFils] =
          array("url"  => ( $__iAppliDefaut==0 
                            ? ( defined("ALK_B_ACCUEIL_SPACE") && ALK_B_ACCUEIL_SPACE==true
                                ? $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."00_accueil.php?cont_id=".$__cont_id
                                : $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."sommaire.php?cont_id=".$__cont_id )
                            : ( $this->_testUrlJs($__strUrlAppli) 
                                ? $this->_getUrlJS($__strUrlAppli, $__cont_id, $__appli_id)
                                : ( substr($__strUrlAppli, 0, 4) != "http"
                                    ? $ALK_SIALKE_URL."scripts/".$__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id
                                    : $__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id))),
                "target" => $__strTargetAppli,
                "titre"  => $this->_tronqueItemSsMenu($__strContIntitule),
                "public" => $__iContPublic,
                "logo"   => "");
      }
    
      $this->tabMenuEspaceFrere[$cpt]["nbFils"] = $cptFils;
    }

    //-------------------------------------------------
    // Recherche les espaces non connect�s directement
    //-------------------------------------------------

    // liste des espaces dont le pere n'est pas accessible
    $dsCont = $this->oQuery->GetDs_listeSousEspaceOrphelinsByAgent($this->agent_id, $this->cont_id, $strCondProfil);
    if( $drCont = $dsCont->GetRowIter()) {
      $cpt++;
      $this->tabMenuEspaceFrere[$cpt] =  
        array("url"    => "#",
              "target" => "",
              "titre"  => "Autres espaces",
              "public" => "1",
              "logo"   => $ALK_SIALKE_URL.$ALK_PATH_UPLOAD_SPACE_LOGO."cont_public.gif");
      $cptFils = 0;
      while( $drCont ) {
        $cptFils++;
        $__cont_id = $drCont->getValueName("CONT_ID");
        $__iAppliDefaut = $drCont->getValueName("DEFAUT");
        $__appli_id = $drCont->getValueName("APPLI_ID");
        $__strUrlAppli = $drCont->getValueName("ATYPE_URL");
        $__strTargetAppli = $drCont->getValueName("ATYPE_URL_TARGET");
        $__strContIntitule = $drCont->getValueName("CONT_INTITULE");
        $__iContPublic     = $drCont->getValueName("CONT_PUBLIC");

        $this->tabSsMenuEspaceFrere[$cpt][$cptFils] =
          array("url"  => ( $__iAppliDefaut==0 
                            ? ( defined("ALK_B_ACCUEIL_SPACE") && ALK_B_ACCUEIL_SPACE==true
                                ? $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."00_accueil.php?cont_id=".$__cont_id
                                : $ALK_SIALKE_URL."scripts/".$ALK_DIR_ESPACE."sommaire.php?cont_id=".$__cont_id )
                            : (  $this->_testUrlJs($__strUrlAppli) 
                                 ?  $this->_getUrlJS($__strUrlAppli, $__cont_id, $__appli_id)
                                 : ( substr($__strUrlAppli, 0, 4) != "http"
                                     ? $ALK_SIALKE_URL."scripts/".$__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id
                                     : $__strUrlAppli."?cont_appli_id=".$__appli_id."&cont_id=".$__cont_id))),
                "target" => $__strTargetAppli,
                "titre"  => $this->_tronqueItemSsMenu($__strContIntitule),
                "public" => $__iContPublic,
                "logo"   => "");
        $drCont = $dsCont->GetRowIter();
      }
      $this->tabMenuEspaceFrere[$cpt]["nbFils"] = $cptFils;
    }
   
    return array($this->tabMenuEspaceFrere, $this->tabSsMenuEspaceFrere);
  }

  /**
   * @brief Test si l'url est un appel a une fonction javascript
   *
   * @param strUrl url a tester
   * @return Retourne vrai si l'url est un appel a une fonction javascript, faux sinon
   */
  function _testUrlJs($strUrl)
  {
    if( $strUrl == "" )
      return false;
    if( strtolower(substr($strUrl, 0, 10)) == "javascript" )
      return true;
    return false;
  }


  /**
   * @brief ajoute les param�tres php � la fonction javascript
   *
   * @param strNomJS Nom de la fonction javascript
   * @param N parametres optionnels qui seront ajoutes a la fonction php
   * @return La chaine de caract�res contenant l'appel complet de la fonction javascript
   */
  function _getUrlJs($strNomJS)
  {
    $strParam = "";
    $numargs = func_num_args();
    for($i=1; $i<$numargs; $i++)
      if( $strParam == "" )
        $strParam = func_get_arg($i);
      else
        $strParam .= ",".func_get_arg($i);
    
    $strTmp = str_replace("()", "($strParam)", $strNomJS);
    
    return $strTmp;
  }

  /**
   * @brief Tronque l'intitul� du menu sur 2 lignes de 15 caract�res
   *
   * @param strItemMenu Intitul� du menu
   * @return Retourne l'intitul� correctement format�
   */
  function _tronqueItemMenu($strItemMenu)
  {
    // effectue des cesures tous les 15 caract�res
    $strItemMenu = wordwrap($strItemMenu, 32, "<br>", 1);

    // coupe la chaine avant le second <br>
    $tabStrMenu = explode("<br>", $strItemMenu);
    $strRes = $tabStrMenu[0];
    /*if( count($tabStrMenu) >= 2 )
      $strRes .= "<br>".$tabStrMenu[1];
    */
    return $strRes;
  }

  /**
   * @brief Tronque l'intitul� du sous-menu sur 1 ligne de 32 caract�res
   *
   * @param strItemMenu Intitul� du sous-menu
   * @return Retourne l'intitul� correctement format�
   */
  function _tronqueItemSsMenu($strItemMenu)
  {
    // effectue des cesures tous les 15 caract�res
    $strItemMenu = wordwrap($strItemMenu, 32, "<br>", 1);
    
    // coupe la chaine avant le second <br>
    $tabStrMenu = explode("<br>", $strItemMenu);
    $strRes = $tabStrMenu[0];
    //if( count($tabStrMenu) >= 2 )
    //  $strRes .= "<br>".$tabStrMenu[1];
    return $strRes;
  }

  
  /**
   * @brief Fonction virtuelle appel�e par le constructeur permettant d'initialiser le tableau this->tabSheets
   *
   */
  function setTabSheets() 
  { 
    $j = 0;
    $i = 0;
    $this->tabSheets = array(ALK_CONSULT_TYPESHEET => array(),
                             ALK_ADMIN_TYPESHEET => array(),
                             ALK_PROPRIETE_TYPESHEET => array());

    $this->tabSheets[ALK_CONSULT_TYPESHEET]["text"] = "Consultation";
    
    if( defined("ALK_B_ACCUEIL_SPACE") && ALK_B_ACCUEIL_SPACE==true ) {
      $this->tabSheets[ALK_CONSULT_TYPESHEET][$j++] =
        array("idSheet" => ALK_ACCUEIL_SHEET_SPACE,
              "text"    => "Accueil", 
              "title"   => "Accueil",
              "url"     => "../".ALK_DIR_ESPACE."00_accueil.php?cont_id=".$this->cont_id,
              "target"  => "",
              "visible" => true,
              "subSheet"=> array());
    }
    
    if( defined("ALK_B_SEARCH_SHEET_SPACE") && ALK_B_SEARCH_SHEET_SPACE==true ) {
      $this->tabSheets[ALK_CONSULT_TYPESHEET][$j++] =
        array("idSheet" => ALK_RECHERCHE_SHEET_SPACE,
              "text"    => "Effectuer&nbsp;une&nbsp;recherche", 
              "title"   => "Effectuer une recherche",
              "url"     => "../".ALK_DIR_ESPACE."01_recherche_form.".(defined("ALK_B_APPLI_GEOLOC") && ALK_B_APPLI_GEOLOC==true ? "phtml" : "php")."?cont_id=".$this->cont_id,
              "target"  => "",
              "visible" => true,
              "subSheet"=> array());
    }

    $this->tabSheets[ALK_CONSULT_TYPESHEET][$j] =
      array("idSheet" => ALK_PLAN_SHEET_SPACE,
            "text"    => "Plan&nbsp;de&nbsp;l'espace", 
            "title"   => "Afficher l'arborescence de l'espace",
            "url"     => "../".ALK_DIR_ESPACE."sommaire.php?cont_id=".$this->cont_id,
            "target"  => "",
            "visible" => true,
            "subSheet"=> array());

    $bAdmin = false;
    if( defined("ALK_B_MENTION_SPACE") && ALK_B_MENTION_SPACE==true && $this->iProfilCont <= 2 || 
        !defined("ALK_B_MENTION_SPACE") && $this->iProfilCont <= 2 || 
        $this->agent_id==1 ) {
      $bAdmin = true;
      $this->tabSheets[ALK_ADMIN_TYPESHEET] = 
        array("text"  => "Administration",
              $i => array("idSheet" => ALK_PROPRIETE_SHEET_SPACE,
                          "text"    => "Propri�t�s", 
                          "title"   => "Modifier les propri�t�s de l'espace",
                          "url"     => "../".ALK_DIR_ESPACE."04_conteneur_form.php?cont_id=".$this->cont_id,
                          "target"  => "",
                          "visible" => true,
                          "subSheet"=> array()),
              ++$i => array("idSheet" => ALK_APPLICATION_SHEET_SPACE,
                            "text"    => "Applications", 
                            "title"   => "G�rer les applications de cet espace",
                            "url"     => "../".ALK_DIR_ESPACE."07_appli_list.php?cont_id=".$this->cont_id,
                            "target"  => "",
                            "visible" => true,
                            "subSheet"=> array()),
              ++$i => array("idSheet" => ALK_UTILISATEUR_SHEET_SPACE,
                            "text"    => "Utilisateurs", 
                            "title"   => "G�rer les utilisateurs ayant acc�s � cet espace",
                            "url"     => "../".ALK_DIR_ESPACE."05_agent_list.php?cont_id=".$this->cont_id,
                            "target"  => "",
                            "visible" => true,
                            "subSheet"=> array()));
        
    }
    if( $bAdmin && $this->iProfilCont == 1 ) {
      $this->tabSheets[ALK_ADMIN_TYPESHEET][++$i] = array("idSheet" => ALK_ANIMATEUR_SHEET_SPACE,
                                                          "text"    => "Animateurs", 
                                                          "title"   => "G�rer les animateurs de cet espace",
                                                          "url"     => "../".ALK_DIR_ESPACE."03_agent_admin_list.php?cont_id=".$this->cont_id,
                                                          "target"  => "",
                                                          "visible" => true,
                                                          "subSheet"=> array());
    }
    if( $bAdmin && defined("ALK_B_APPLI_STAT") && ALK_B_APPLI_STAT == true && $this->iProfilCont <= 2 ) {
      $this->tabSheets[ALK_ADMIN_TYPESHEET][++$i] = array("idSheet" => ALK_STAT_SHEET_SPACE,
                                                          "text"    => "Statistiques", 
                                                          "title"   => "Consulter les statistiques de consultation sur cet espace",
                                                          "url"     => "../".ALK_DIR_ESPACE."09_stats_list.php?cont_id=".$this->cont_id,
                                                          "target"  => "",
                                                          "visible" => true,
                                                          "subSheet"=> array());
    }
    if( $bAdmin && defined("ALK_B_LOG_SPACE") && ALK_B_LOG_SPACE == true && $this->iProfilCont <= 2 ) {
      $this->tabSheets[ALK_ADMIN_TYPESHEET][++$i] = array("idSheet" => ALK_LOG_SHEET_SPACE,
                                                          "text"    => "Journalisation", 
                                                          "title"   => "Consulter le journal de actions sur cet espace",
                                                          "url"     => "../".ALK_DIR_ESPACE."10_log_list.php?cont_id=".$this->cont_id,
                                                          "target"  => "",
                                                          "visible" => true,
                                                          "subSheet"=> array());
    }
  }

  /**
   * @brief Retourne le tableau des onglets
   *        Tableau de type [0..n-1]["idSheet"] => identifiant de l'onglet
   *                                ["text"]    => texte sur l'onglet 
   *                                ["title"]   => info bulle sur l'onglet  
   *                                ["url"]     => adresse du lien
   *                                ["target"]  => nom de la fen�tre cible 
   *                                ["visible"] => vrai ou faux
   *                                ["subSheet"]=> tableau de type [0..p-1][["idSheet"], ["text"], ["title"], ["url"], ["target"], ["visible"], ["subSheet"]]
   *         n : nombre d'onglets
   *         p : nombre de sous onglets
   *
   * @param iTypeSheet  Type d'onglets � retourner (-1 par d�faut, dans ce cas, prend la valeur courante $this->iTypeSheet)
   * @return Retourne un array
   */
  function getTabSheets($iTypeSheet="-1")
  {
    if( $iTypeSheet != "-1" )
      $this->iTypeSheet = $iTypeSheet;

    if( $this->iTypeSheet >= 0 && $this->iTypeSheet < count($this->tabSheets) )
      return $this->tabSheets[$this->iTypeSheet];

    return array();
  }
  

}
?>