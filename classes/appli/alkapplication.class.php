<?php

/**
 *  Classe abstraite
 *
 * Classe repr�sentant une abstraction d'une application capable de s'afficher 
 * dans le gestionnaire d'espace alkante.
 */
class AlkApplication extends AlkObject
{
  /** R�f�rence vers l'objet espace de travail qui contient cette application */
  var $oSpace;

  /** R�f�rence vers l'ojet interface source de donn�es de type s�lection */
  var $oQuery;

  /** R�f�rence vers l'ojet interface source de donn�es de type action */
  var $oQueryAction;

  /** Indentifiant de l'application */
  var $appli_id;

  /** Indentifiant du type d'application */
  var $atype_id;

  /** Intitul� de l'application */
  var $appli_intitule;

  /** Identifiant de l'utilisateur connect� */
  var $agent_id;

  /** Droit de l'agent sur l'application */
  var $droit_id;

  /** Url de l'application */
  var $appli_url;

  /** Url de l'upload pour l'application */
  var $strUrlUpload;
    
  /** Chemin complet pour acc�der au r�pertoire d'upload */
  var $strPathUpload;

  /** Clone de la constante de param�trage de l'application */
  var $tabParam;

  /** Contenu html de la page en cours */
  var $strHtml;

  //  var $strXml;
  //var $strUrlXsl;
  
  /** Tableau contenant les */
  var $tabPathAppli;

  /** type d'onglet en cours de s�lection */
  var $iTypeSheet;

  /** Identiifant de l'onglet et sous onglet s�lectionn� */
  var $iSheet;
  var $iSubSheet;

  /** Tableau contenant les informations d'onglets */
  /** Tableau de type [0..m][0..n-1]["idSheet"] => identifiant de l'onglet
   *                                ["text"]    => texte sur l'onglet 
   *                                ["title"]   => info bulle sur l'onglet  
   *                                ["url"]     => adresse du lien
   *                                ["target"]  => nom de la fen�tre cible 
   *                                ["visible"] => vrai ou faux
   *                                ["subSheet"]=> tableau de type [0..p-1][["idSheet"], ["text"], ["title"], ["url"], ["target"], ["visible"]]
   * m : type d'onglet : 0= partie consultation, 1= partie administration, 2= partie personnalisation ou param�trage
   * n : nombre d'onglets par type
   * p : nombre de sous onglets par onglet
   */
  var $tabSheets;

  /** R�f�rence vers l'objet alkArbre contenant les informations des rubriques */
  var $oAlkArbre;

  /**
   * @brief Constructeur par d�faut
   *
   * @param oSpace R�f�rence vers l'objet de l'espace en cours
   * @param 
   */
  function AlkApplication(&$oSpace, $appli_id,  $agent_id, $strUrlUpload, $strPathUpload)
  {
    parent::AlkObject();
    
    $this->oSpace =& $oSpace;
    $this->appli_id = $appli_id;
    $this->agent_id = $agent_id;
    $this->strUrlUpload = $strUrlUpload;
    $this->strPathUpload = $strPathUpload;
    $this->strHtml = "";
    
    $this->oAlkArbre = null;
    $this->iSheet = -1;
    $this->iTypeSheet = -1;
    $this->tabSheet = array();
    // N�cessit� d'instancier les objets $oQueryXXX dans les classes h�rit�es
  }

  /**
   * @brief initialise le tableau de param�tre
   *
   * @param tabParam  tableau de param�tre de l'application
   */
  function SetTabParam()
  {
    $this->tabParam = array();
  }

  /**
   * @brief Indique si oui ou non la fonctionnalit� est activ�e
   *
   * @param strKeyParam Cl� du param�trage de fonctionnalit�
   * @return Retourne un boleen
   */
  function IsActive($strKeyParam)
  {
    if( is_array($this->tabParam) && array_key_exists($strKeyParam, $this->tabParam) ){
      return $this->tabParam[$strKeyParam];
    }
    return false;
  }

  /**
   * @brief Appel d'un service offet par l'application
   *
   * @param idServ Num�ro du service demand�
   * @param oRes   Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   */
  function CallService($idServ, &$oRes) { }

  /**
   * @brief M�thode appel�e par le workspace pour permettre son initialisation suite
   *        � un ajout sur un espace.
   *
   * @param oRes    Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param cont_id Identifiant de l'espace en cours
   */
  function CallServiceCreate(&$oRes, $cont_id) { }

  /**
   * @brief M�thode appel�e par le workspace pour permettre le repport de la modification sur
   *        les donn�es de l'application
   *
   * @param oRes           Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param cont_id        Identifiant de l'espace en cours
   * @param appli_intitule Intitule de l'application
   */
  function CallServiceUpdate(&$oRes, $cont_id, $appli_intitule) { }

  /**
   * @brief M�thode appel�e par le workspace pour d�truire les donn�es de l'application
   *        avant de la supprimer d�finitivement de l'espace
   *
   * @param oRes    Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param cont_id Identifiant de l'espace en cours
   */
  function CallServiceDelete(&$oRes, $cont_id) { }

  /**
   * @brief  M�thode appel�e par le workspace pour appliquer les droits des profils aux agents de l'application
   *         clic sur bouton appliquer de la gestion des applications
   *
   * @param oRes       Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param cont_id    Identifiant de l'espace en cours
   * @param bDroitPub  =vrai si droit de publication sur les applis, faux sinon
   * @param agent_id   identifiant de l'agent, =-1 pour ts les agents
   */
  function CallServiceSetDroitProfilToAgent($oRes, $cont_id, $bDroitPub, $agent_id=-1) { }

  /**
   * @brief  M�thode appel�e par le workspace pour appliquer les droits du profil aux agents des application de l'espace
   *         clic sur bouton appliquer de la gestion des profils
   *
   * @param oRes       Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param cont_id    Identifiant de l'espace en cours
   * @param profil_id  Identifiant du profil
   * @param bDroitPub  =vrai si droit de publication sur les applis, faux sinon
   * @param agent_id   identifiant de l'agent, =-1 pour ts les agents
   */
  function CallServiceSetDroitProfilAppliToAgent($oRes, $cont_id, $profil_id, $bDroitPub, $agent_id=-1) { }

  /**
   * @brief M�thode appel�e par le workspace pour recopier les droits fix�s sur les profils (si profil_id=-1,
   *        ou sur le profil_id sinon) de l'application pour les r�attribuer aux droits sur les profils 
   *        li�s � l'information de l'application
   *        clic sur bouton droit de la gestion des applications (=-1)
   *        clic sur bouton droit de la gestion des profils (!=-1)
   *
   * @param oRes       Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param cont_id    Identifiant de l'espace en cours
   * @param profil_id  Identifiant du profil � recopier, -1 pour tous les profils
   * @param bDroitPub  =vrai si droit de publication sur les applis, faux sinon
   */
  function CallServiceCopyDroitToProfil($oRes, $cont_id, $profil_id, $bDroitPub) { }

  /**
   * @brief M�thode appel�e par le workspace pour fixer les droits aux agents nouvellement invit�s dans l'espace
   *
   * @param oRes             Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param cont_id          Identifiant de l'espace en cours
   * @param strListeAgentId  Liste des agent_id s�lectionn�s
   * @param bDroitPub        =vrai si droit de publication sur les applis, faux sinon
   * @param iForceAnim       non utilis�
   * @param strListeIdToDel  liste d'identifiant d'agent � retirer (ou d'animateur si iForceAnim=1)
   */
  function CallServiceAddDroitToAgent($oRes, $cont_id, $strListeAgentId, $bDroitPub, $iForceAnim="-1", $strListeIdToDel="") { }

  /**
   * @brief M�thode appel�e par le workspace pour retirer les droits aux agents nouvellement supprim�s de l'espace
   *
   * @param oRes             Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param cont_id          Identifiant de l'espace en cours
   * @param strListeAgentId  Liste des agent_id s�lectionn�s
   */
  function CallServiceDelDroitToAgent($oRes, $cont_id, $strListeAgentId) { }

  /**
   * @brief M�thode appel�e par le workspace pour modifier les droits d'un agent sur l'application
   *
   * @param oRes       Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param cont_id    Identifiant de l'espace en cours
   * @param agent_id   Identifiant de l'agent
   * @param droit_id   Droit sur l'application
   */
  function CallServiceSetDroitToAgent($oRes, $cont_id, $agent_id, $droit_id) { }

  /**
   * @brief M�thode appel�e par le workspace pour supprimer un agent
   *        Action � effectuer sur toutes les applications de ce type.
   *
   * @param oRes       Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param agent_id   Identifiant de l'agent
   */
  function CallServiceDelAgent($oRes, $agent_id) { }

  /**
   * @brief M�thode appel�e par le workspace pour supprimer toute association avec un profil
   *        Fonction � impl�menter sur toute application associant des droits � des profils
   *        R�pond � l'�v�nement : suppression d'un profil sur l'admin annuaire
   *        Action � effectuer sur toutes les applications de ce type.
   *
   * @param oRes       Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param profil_id   Identifiant du profil
   */
  function CallServiceDelProfil($oRes, $profil_id) { }

  /**
   * @brief M�thode appel�e par le workspace pour transf�rer les donn�es d'un utilisateur (bient�t 
   *        mut� ou supprim�) par un autre
   *        Fonction � impl�menter sur toute application o� un agent est cr�ateur ou modificateur d'une info
   *        R�pond � l'�v�nement : Transfert de propri�t�s
   *
   * @param oRes         Non utilis�
   * @param agentOld_id  Identifiant de l'agent � remplacer
   * @param agentNew_id  Identifiant du l'agent rempla�ant
   */
  function CallServiceReplaceAgent($oRes, $agentOld_id, $agentNew_id) 
  { 
    if( $this->oQueryAction != null && method_exists($this->oQueryAction, "ReplaceAgent") ) {
      $this->oQueryAction->ReplaceAgent($agentOld_id, $agentNew_id);
    }
  }

  /**
   * @brief Service appel� par l'application de traduction pour mettre � jour les informations traduites
   *
   * @param oRes Pass� par r�f�rence, retourne une information compl�mentaire au r�sultat de la fonction
   * @param strTable     Nom de la table � mettre � jour
   * @param strKeyField  Nom du champ cl� primaire (Noms s�par�s par un pipe)
   * @param strKeyId     Identifiant de la cl� primaire (Identifiants s�par�s par un pipe)
   * @param strField     Nom du champ � mettre � jour
   * @param strTxtTrad   Texte traduit

   */
  function CallServiceValidTraduction($oRes, $strTable, $strKeyField, $strKeyId, $strField, $strTxtTrad) { }

  /**
   * @brief Fonction de routage pour afficher la page identifi�e
   *
   * @param idPage Identifiant de la page demand�e
   */
  function CallPage($idPage) { }
  
  /**
   * @brief Fonction qui retourne le contenu html de la page identifi�e 
   *
   * @param idPage Identifiant de la page demand�e
   */
  function GetStrHtmlPage()
  {
    return $this->strHtml;
  }

  /**
   * @brief Fonction permettant d'obtenir le tableau du parcours de navigation
   *
   */
  function GetTabPathAppli($idPage) { } 

  /**
   * @brief Fonction virtuelle appel�e par le constructeur permettant d'initialiser le tableau this->tabSheets
   *
   */
  function setTabSheets() { }
  

  /**
   * @brief Fonction virtuelle appel�e par le constructeur permettant d'initialiser l'objet $this->oAlkArbre
   *
   */
  function setAlkArbre() { }

  /**
   * @brief Retourne le tableau des onglets
   *        Tableau de type [0..n-1]["idSheet"] => identifiant de l'onglet
   *                                ["text"]    => texte sur l'onglet 
   *                                ["title"]   => info bulle sur l'onglet  
   *                                ["url"]     => adresse du lien
   *                                ["target"]  => nom de la fen�tre cible 
   *                                ["visible"] => vrai ou faux
   *                                ["subSheet"]=> tableau de type [0..p-1][["idSheet"], ["text"], ["title"], ["url"], ["target"], ["visible"], ["subSheet"]]
   *         n : nombre d'onglets
   *         p : nombre de sous onglets
   *
   * @param iTypeSheet  Type d'onglets � retourner (-1 par d�faut, dans ce cas, prend la valeur courante $this->iTypeSheet)
   * @return Retourne un array
   */
  function getTabSheets($iTypeSheet="-1")
  {
    if( $iTypeSheet != "-1" )
      $this->iTypeSheet = $iTypeSheet;

    if( $this->iTypeSheet >= 0 && $this->iTypeSheet < count($this->tabSheets) )
      return $this->tabSheets[$this->iTypeSheet];

    return array();
  }
  
  /**
   * brief fonction d'incr�mentation des logs
   * 
   * @param action_id         Identifiant de l'action r�alis�e
   */
  function maj_logs($action_id){
  	global $queryContAnnuAction;    
    
    $tabLogActionId = ( isset($_SESSION["tabLogAction"]) 
                                 ? $_SESSION["tabLogAction"] 
                                 : array() );
    if( !array_key_exists($action_id, $tabLogActionId) ) {
      $tabLogActionId[$action_id] = true;
      $_SESSION["tabLogAction"] = $tabLogActionId;
      $bLog = true;
    } else {
      $bLog = false;
    }
    if($bLog){
      $queryContAnnuAction->add_log($_SESSION["sit_idUser"], $action_id);	
    }
  }
  
  /**
   * brief fonction d'incr�mentation des statistiques Appli
   * 
   * @param data_id           Identifiant de la donn�e
   * @param stat_action_id    Identifiant de l'action r�alis�e
   * @param datatype_id       Identifiant du type de donn�e
   * @param atype_id          Identifiant du type d'application
   */
  function maj_stats($data_id, $stat_action_id, $datatype_id, $atype_id=0)
  {
    global $queryCont;
    global $queryContAnnuAction;    
    if ($atype_id==0)
      $atype_id = $this->atype_id;
    
    $tabStatAccesDataAppliId = ( isset($_SESSION["tabStatAccesDataAppli"]) 
                                 ? $_SESSION["tabStatAccesDataAppli"] 
                                 : array() );
    
    if( !array_key_exists($this->appli_id."_".$data_id."_".$stat_action_id, $tabStatAccesDataAppliId) ) {
      $tabStatAccesDataAppliId[$this->appli_id."_".$data_id."_".$stat_action_id] = true;
      $_SESSION["tabStatAccesDataAppli"] = $tabStatAccesDataAppliId;
      $bStat = true;
    } else {
      $bStat = false;
    }
    if( $bStat ) {
      $dsTest = $queryCont->GetDs_listeStatsAppliByAgent($_SESSION["sit_idUser"], $data_id, $atype_id, 
                                                         $datatype_id, $this->appli_id, $stat_action_id);
      if( $dsTest->iCountTotDr > 0 ) {
        // maj
        $queryContAnnuAction->maj_statsAppliByAgent($_SESSION["sit_idUser"], $data_id, $atype_id, 
                                                    $datatype_id, $this->appli_id, $stat_action_id);
      } else { 
        // ajout
        $queryContAnnuAction->add_statsAppliByAgent($_SESSION["sit_idUser"], $data_id, $atype_id, 
                                                    $datatype_id, $this->appli_id, $stat_action_id);
      }
    }
  }

}
?>