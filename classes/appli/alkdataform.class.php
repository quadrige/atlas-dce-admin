<?php

/**
 * @class AlkDataFormAttrib
 * @brief Classe repr�sentant un attribut data de la classe AlkDataForm
 */
class AlkDataFormAttrib extends AlkObject
{
  /** nom de l'attribut */
  var $name;

  /** valeur de l'attribut */
  var $value;

  /** valeur par d�faut */
  var $defaultValue;

  /** nom de la classe ctrl utilis�e */
  var $htmlCtrlName;

  /** type de donn�es SQL associ� 0=varchar ou text, 1=int ou number ou float, 2=date */
  var $typeSql;

  /** vrai si le champ est multilingue */
  var $bMultilingue;

  /** tableau contenant les restrictons de validation */
  var $tabValidator;

  /** nom de la fonction appel�e pour v�rifier le format de la donn�e post�e */
  var $functionVerif;

  /**
   * @brief Constructeur par d�faut.
   *
   * @param strName     nom de l'attribut
   * @param strValue    valeur par d�faut
   * @param strCtrl     nom de la classe
   * @param strType      type du controle de saisie : 'memo', 'text', 'int', 'date10', 'radio', 'check', 'select', 'mail', 'checkgroup',...
   * @param bRequire     =true si champ obligatoire, =false sinon
   * @param oMin         valeur minimale requise pour le controle (ou longueur min en fonction de strType)
   * @param oMax         valeur maximale requise pour le controle (ou longueur max en fonction de strType)
   * @param strExcept    valeurs qui font exceptions pour la v�rification (s�par�es par "|" si plusieurs)
   * @param bStrict      vrai si in�galit� strict pour v�rifier min et max, faux sinon (par d�faut)
   * @param bMutilingue  vrai si le champ est multilingue, faux sinon (par d�faut)
   * @param strFuncVerif nom de la fonction appel�e pour v�rifier le format de la donn�e post�e
   */
  function AlkDataFormAttrib($strName, $strValue, $strCtrl, $strType, $bRequire=false, $oMin="", $oMax="",
                             $strMsgErr="", $strExcept="", $bStrict=true, $bMultilingue=false, $strFuncVerif="DefaultTest", $typeSql="0")
  {
    parent::AlkObject();

    $this->name = $strName;
    $this->value = $strValue;
    $this->defaultValue = $strValue;
    $this->typeSql = $typeSql;
    $this->htmlCtrlName = $strCtrl;
    $this->bMultilingue = $bMultilingue;
    if( $this->bMultilingue == true ) {     
      reset($this->tabLangue);
      foreach($this->tabLangue as $key => $tabLg) {
        $this->value[$key] = "";
      }
      $this->defaultValue = $this->value;   
    }
 
    $this->functionVerif = $strFuncVerif;
    $this->tabValidator = array();
    if( $strType != "none" )
      $this->tabValidator = array("form"    => "",
                                  "type"    => $strType,
                                  "require" => $bRequire,
                                  "min"     => $oMin,
                                  "max"     => $oMax,
                                  "msgerr"  => $strMsgErr,
                                  "except"  => $strExcept,
                                  "strict"  => $bStrict);
  }

  /**
   * @brief Charqe les donn�es � partir du dataRow.
   *        Prend en charge le multilingue
   *
   * @param oDrData  R�f�rence vers un dataRow
   */
  function GetData(&$oDrData)
  {
    if( $this->bMultilingue == true ) {
      reset($this->tabLangue);
      foreach($this->tabLangue as $key => $tabLg) {
        $this->value[$key] = $oDrData->GetValueName(strtoupper($this->name).$tabLg["bdd"]);
      }
    } else {
      $this->value = $oDrData->GetValueName(strtoupper($this->name));
    }
  }

  /**
   * @brief Charqe les donn�es � partir du dataRow.
   *        Prend en charge le multilingue
   *
   * @param reqMethod Type de request (REQ_POST, REQ_GET, REQ_POST_GET (par d�faut), REQ_GET_POST)
   */
  function GetRequest($reqMethod=REQ_POST_GET)
  {
    if( $this->bMultilingue == true ) {
      reset($this->tabLangue);
      foreach($this->tabLangue as $key => $tabLg) {
        $this->value[$key] = $this->_GetRequest($reqMethod, $this->name.$tabLg["bdd"], $key);
      }
    } else {
      $this->value = $this->_GetRequest($reqMethod, $this->name);
    }    
  }

  /**
   * @brief Se charge de r�cup�rer la valeur post�e de la bonne mani�re
   *
   * @param reqMethod   Type de request (REQ_POST, REQ_GET, REQ_POST_GET, REQ_GET_POST)
   * @param strVarName
   * @param idLg        Identifiant de la langue courante (-1 par d�faut correspondant bMultilingue=false)
   */
  function _GetRequest($reqMethod, $strVarName, $idLg="-1")
  {
    if( !function_exists($this->functionVerif) )
      $this->functionVerif = "DefaultTest";

    $strRes = "";
    switch( strtolower($this->htmlCtrlName) ) {
    case "htmlcheckbox":
      // pas de multilingue sur les checkbox
      $strRes = RequestCheckbox($strVarName, $reqMethod);
      break;

    case "htmlcheckboxgroup":
      // pas de multilingue sur les checkbox
      // retourne les valeurs dans une chaine. Chaque valeur �tant s�par�e par un pipe
      $nbCheckbox = Request($strVarName, $reqMethod, 0, "is_numeric");
      for($i=0; $i<$nbCheckbox; $i++) {
        $strRes .= "|".RequestCheckbox($strVarName."_".$i, $reqMethod);
      }
      // retire le premier pipe
      $strRes = substr($strRes, 1);
      break;

    default:
      if( $idLg == "-1" )
        $strRes = Request($strVarName, $reqMethod, $this->defaultValue, $this->functionVerif);
      else
        $strRes = Request($strVarName, $reqMethod, $this->defaultValue[$idLg], $this->functionVerif);
      break;
    }
    
    return $strRes;
  }

  /**
   * @brief  Retourne la longueur de chaine max pour un controle de saisie de type texte
   *
   * @return Retourne un int
   */
  function GetMaxLength()
  {
    $iRes = 255;
    if( strtolower($this->htmlCtrlName) == "htmltext" ) {
      if( count($this->tabValidator)>0 ) {
        switch( strtolower($this->tabValidator["type"]) ) {
        case "text":
        case "textalpha":
        case "mail":
          $iRes = ( is_numeric($this->tabValidator["max"]) && $this->tabValidator["max"]!="" ? $this->tabValidator["max"] : 255 );
          break;
        case "memo":
          $iRes = ( is_numeric($this->tabValidator["max"]) && $this->tabValidator["max"]!="" ? $this->tabValidator["max"] : 4000 );
          break;
        case "int":
        case "textnum":
          $iMinLen = ( is_numeric($this->tabValidator["min"]) && $this->tabValidator["min"]!="" ? strlen($this->tabValidator["min"]) : 10 );
          $iMaxLen = ( is_numeric($this->tabValidator["max"]) && $this->tabValidator["max"]!="" ? strlen($this->tabValidator["max"]) : 10 );
          $iRes = max($iMinLen, $iMaxLen);
          break;
        case "date10":
          $iRes = 10;
          break;
        case "heure5":
          $iRes = 5;
          break;
        } 
      }
    }
    return $iRes;
  }
  
}



/**
 * @brief Classe G�n�rique regroupant l'ensemble des champs pr�sents dans un formulaire
 * 
 * @derivation Toute classe d�riv�e devra d�clarer (var $xxx;) les champs du formulaire 
 *             avec pour convention de nommage pr�f�rentielle : nom_champ_form = nom_champ_bd (en minuscule)
 * 
 */
class AlkDataForm extends AlkObject
{ 
  /** AlkApplication instanciant cet objet */
  var $oAppli;
  
  /** HtmlForm pour lequel les donn�es sont r�cup�r�es */
  var $oForm;
  
  /**
   * @brief Constructeur par d�faut.
   *
   * @param oAppli  r�f�rence sur objet AlkApplication instanciant cet objet
   * @param oForm   r�f�rence sur objet Formulaire pour lequel les donn�es sont r�cup�r�es
   */
  function AlkDataForm(&$oAppli, &$oForm) 
  {
    parent::AlkObject();
    
    $this->oAppli =& $oAppli;
    $this->oForm =& $oForm; 
  }
  
  /**
   * @brief AbstractMethod - 
   *        Instancie les attributs � partir d'un DataSet sp�cifique au formulaire
   *        Le dataSet est obtenu � partir de l'objet oAppli->oQuery->getDs_xxxx()
   *        Cette m�thode connait la m�thode � appeler.
   * @param  Prend en param�tre les param�tres n�cessaire au getDs_xxxx()
   */
  function GetData()
  {   
  }
  
  /**
   * @brief AbstractMethod -
   *        Effectue sur chaque attribut de la classe (sauf oAppli) la m�thode Request 
   * 
   * @param reqMethod Type de request (REQ_POST, REQ_GET, REQ_POST_GET (par d�faut), REQ_GET_POST)
   */
  function GetRequest($reqMethod=REQ_POST_GET)
  {
  }

  /**
   * @brief Retourne un tableau associatif conforme aux m�thodes AlkQuery::_GetPartInsertSql et AlkQuery::_GetPartUpdateSql
   *        Le tableau retourn� est de type array("pk" => $tabResPk, "field" => $tabResField);
   *        Les tableaux $tabResPk et $tabResField sont de type associatif fieldName => array(fieldType, fieldValue)
   *        fieldType  : = 0 string, 1 number, 2 date, 3 expression sql
   *        fieldValue : valeur brute php
   *
   * @brief tabFieldPk     Tableau contenant le nom des champs utilis�s comme cl� primaire
   * @brief tabFieldIgnore Tableau contennat le nom des champs � ignorer (= vide par d�faut correspondant � aucun champ)
   * @brief tabFieldSelect Tableau contennat le nom des champs � prendre en compte (= vide par d�faut correspondant � tous les champs)
   * @brief tabFieldAlias  Tableau contenant une liste d'association entre (champs-alias => champ-r�el)
   * @return Retourne un array
   */
  function GetTabQuery($tabFieldPk=array(), $tabFieldIgnore=array(), $tabFieldSelect=array(), $tabFieldAlias=array())
  {
    
    $tabResField = array();
    $tabResPk = array();
    $tabAttrib = get_object_vars($this);
    foreach($tabAttrib as $strAttrib => $oTmp) {
      if( !((count($tabFieldIgnore)>0 && in_array(strtoupper($strAttrib), $tabFieldIgnore))) &&
          (count($tabFieldSelect)==0 ||
           (count($tabFieldSelect)>0 && in_array(strtoupper($strAttrib), $tabFieldSelect))) ) {
        eval("\$oAttrib =& \$this->".$strAttrib.";");
        if( get_class($oAttrib) != "alkdataformattrib" ) continue;
        $strField = strtoupper($strAttrib);
        
        if( array_key_exists($strField, $tabFieldAlias) ) {
          
          $strField = $tabFieldAlias[$strField];
     
        }
        if( in_array($strField, $tabFieldPk ) ) {
          $tabResPk[$strField] = array($oAttrib->typeSql, $oAttrib->value);
        } else {
          if( $oAttrib->bMultilingue == true ) {
            reset($this->tabLangue);
            foreach($this->tabLangue as $strKey => $tabLg) {
              $tabResField[$strField.$tabLg["bdd"]] = array($oAttrib->typeSql, $oAttrib->value[$strKey]);
            }
          } else {
            $tabResField[$strField] = array($oAttrib->typeSql, $oAttrib->value);
          }
        }
      }
    }
      
    return array("pk" => $tabResPk, "field" => $tabResField);
  }

}
?>