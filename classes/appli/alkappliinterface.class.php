<?php
include_once("alkapplication.class.php");

/**
 *  Classe abstraite
 *
 * Classe repr�sentant une abstraction d'une application capable de s'afficher 
 * dans le gestionnaire d'espace alkante.
 */
class AlkAppliInterface extends AlkApplication
{
  

  /**
   * @brief Constructeur par d�faut
   *
   * @param oSpace R�f�rence vers l'objet de l'espace en cours
   * @param 
   */
  function AlkAppliInterface(&$oSpace, $appli_id,  $agent_id, $strUrlUpload, $strPathUpload)
  {
    $this->beforeConstrutor();
    parent::AlkApplication($oSpace, $appli_id,  $agent_id, $strUrlUpload, $strPathUpload);
    $this->afterConstructor();

  }

  function BeforeConstrutor() { }
  function AfterConstructor() { }

  function RequestData() { }
  function InitData() { }
  function InitDBConnection() { }
  function ExecuteSql() { }


  function GetHtmlJsInit() { }
  function GetHtmlJsOnLoad() { }
  function GetHtmlJsFileSource() { }

  function GetHtmlPage() { }
}
?>