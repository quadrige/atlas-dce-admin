<?php
include_once "alkserviceappli.class.php";

/**
 *  Classe
 *
 * Classe repr�sentant une abstraction d'un service demand� � une application
 */
class AlkServiceAppliSite extends AlkServiceAppli
{
  /**
   * @brief Constructeur par d�faut
   *
   * @param atype_id Type d'application
   * @param oSpace   R�f�rence vers l'objet de l'espace en cours
   * @param appli_id Identifiant de l'application
   * @param agent_id Identifiant de l'agent connect�
   */
  function AlkServiceAppliSite($atype_id, &$oSpace, $appli_id, $agent_id)
  {
    $this->appli_id = $appli_id;
    $this->atype_id = $atype_id;
    $this->oSpace =& $oSpace;
    $this->agent_id = $agent_id;
    $this->oAppli = null;

    switch( $this->atype_id ) {
    case 2:
    case 4: // Fonds documentaires
      if( ALK_B_APPLI_FDOC == true ) {
        include_once("../site/classes/alkapplifdocsite.class.php");
        $this->oAppli = new AlkAppliFDocSite($oSpace, $appli_id, $agent_id,
                                         ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC_IMG,
                                         ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_IMG,
                                         $this);
      }
      break;

    case 3: // Actualit�s
      if( ALK_B_APPLI_ACTU == true ) {
        include_once("../actu_01/classes/alkappliactu.class.php");
        $this->oAppli = new AlkAppliActu01($oSpace, $appli_id, $agent_id,
                                           ALK_SIALKE_URL.ALK_PATH_UPLOAD_ACTU_LOGO, 
                                           ALK_SIALKE_PATH.ALK_PATH_UPLOAD_ACTU_LOGO,
                                           $this);
      }
      break;

    case 5: // Agenda
      if( ALK_B_APPLI_AGENDA == true ) {
        include_once("../agenda_01/classes/alkappliagenda.class.php");
        $this->oAppli = new AlkAppliAgenda($oSpace, $appli_id, $agent_id,
                                           ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC_AGENDA, 
                                           ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_AGENDA,
                                           $this);
      }
      break;

    case 6: // Liste de diffusion
      if( ALK_B_APPLI_LSDIF == true ) {
        include_once("../lsdif_01/classes/alkapplilsdif.class.php");
        $this->oAppli = new AlkAppliLsDif($oSpace, $appli_id, $agent_id,
                                          ALK_SIALKE_URL.ALK_PATH_UPLOAD_LS_DIF_PJ, 
                                          ALK_SIALKE_PATH.ALK_PATH_UPLOAD_LS_DIF_PJ,
                                          $this);
      }
      break;

    case 7: // Forum
      if( ALK_B_APPLI_FORUM == true ) {
        include_once("../forum_01/classes/alkappliforum.class.php");
        $this->oAppli = new AlkAppliForum($oSpace, $appli_id, $agent_id,
                                          ALK_SIALKE_URL.ALK_PATH_UPLOAD_FORUM_IMG, 
                                          ALK_SIALKE_PATH.ALK_PATH_UPLOAD_FORUM_IMG,
                                          $this);
      }
      break;

    case 8: // SIG
      if( ALK_B_APPLI_SIG == true ) {
        include_once(ALK_ROOT_PATH.ALK_DIR_SIG_ADMIN."classes/alkapplisig.class.phtml");
        $this->oAppli = new AlkAppliSig($oSpace, $appli_id, $agent_id,
                                        ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC_SIG, 
                                        ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_SIG,
                                        $this);
      }
      break;

    case 9: // Favoris
      if( ALK_B_APPLI_FAVORIS == true ) {
        include_once("../bookmark_01/classes/alkapplifavoris.class.php");
        $this->oAppli = new AlkAppliFavoris($oSpace, $appli_id, $agent_id, "", "", $this);
      }
      break;
      
    case 21: // Actualit�s
      if( ALK_B_APPLI_ACTUSITE == true ) {
        include_once("../actu/classes/alkappliactu.class.php");
        $this->oAppli = new AlkAppliActu($oSpace, $appli_id, $agent_id, "", "");
      }
      break;

    case 22: // Liens utiles
      if( ALK_B_APPLI_LIEN == true ) {
        include_once("../lien/classes/alkapplilien.class.php");
        $this->oAppli = new AlkAppliLien($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case 23: // Gestion �ditoriale
      if( ALK_B_APPLI_GEDIT == true ) {
        include_once("../gedit/classes/alkappligedit.class.php");
        $this->oAppli = new AlkAppliGEdit($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case 24: // Glossaire
      if( ALK_B_APPLI_GLOS == true ) {
        include_once("../glos/classes/alkappliglos.class.php");
        $this->oAppli = new AlkAppliGlos($oSpace, $appli_id, $agent_id, "", "");
      }
      break;  
      
    case 25: // Faqs
      if( ALK_B_APPLI_FAQS == true ) {
        include_once("../faqs/classes/alkapplifaq.class.php");
        $this->oAppli = new AlkAppliFaq($oSpace, $appli_id, $agent_id, "", "");
      }
      break;

    case 26: // Syndication Alkanet
      if( ALK_B_APPLI_SYNDICATION == true ) {
        include_once("../synd/classes/alkapplisynd.class.php");
        $this->oAppli = new AlkAppliSynd($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    
    case 27: // Abonnement newsletter
      if( ALK_B_GEDIT_LETTRE == true ) {
        include_once("../newsletter/classes/alkapplinewsletter.class.php");
        $this->oAppli = new AlkAppliNewsletter($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
   case 31: // Recettes
      if( ALK_B_APPLI_RECETTE == true ) {
        include_once("../recette/classes/alkapplirecette.class.php");
        $this->oAppli = new AlkAppliRecette($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
    
     case 32: // Formulaire
      if( ALK_B_APPLI_FORMULAIRE == true ) {
        include_once("../form/classes/alkappliformulaire.class.php");
        $this->oAppli = new AlkAppliFormulaire($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case 33: // Sondage
      if( ALK_B_APPLI_SONDAGE == true ) {
        include_once("../sondage/classes/alkapplisondage.class.php");
        $this->oAppli = new AlkAppliSondage($oSpace, $appli_id, $agent_id, "", "");
      }
      break;
      
    case 49: // Billetterie .
        if( defined("ALK_B_APPLI_BILLETTERIE") && ALK_B_APPLI_BILLETTERIE== true ) {
        include_once("../billetterie/classes/alkapplibilletterie.class.php");
        $this->oAppli = new AlkAppliBilletterie($oSpace, $appli_id, $agent_id, "", "");
      }
      break; 
    }
  }
 }

?>