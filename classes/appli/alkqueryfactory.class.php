<?php
include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."/alkobject.class.php");

/**
 * @brief Classe de factory sur les query et queryAction 
 *        en fonction des types d'application et du type de base utilis�e.
 */
class AlkQueryFactory extends AlkObject
{
  /** R�f�rence sur l'objet de connexion � la base */
  var $dbConn;

  /** tableau contenant les r�f�rences des objets query instanci�s */
  var $tabQuery;

  /** tableau contenant les r�f�rences des objets queryAction instanci�s */
  var $tabQueryAction;

  /**
   * @brief Constructeur par d�faut
   *
   * @param tabLangue tableau des langues utilis�es
   */
  function AlkQueryFactory()
  {
    parent::AlkObject();
    
    $this->dbConn = null;
    $this->tabQuery = array();
    $this->tabQueryAction = array();    

    $this->_InitConnection();

    $this->tabSGBD = array(SGBD_ORACLE   => array("file" => "oracle", "class" => "Oracle"),
                           SGBD_MYSQL    => array("file" => "mysql",  "class" => "Mysql"),
                           SGBD_POSTGRES => array("file" => "pgsql" , "class" => "Pgsql"));

    $this->tabDir = array("_".ALK_ATYPE_ID_ESPACE     => array("espace",    "cont",     "Cont"),
                          "_".ALK_ATYPE_ID_ANNU       => array("annu",      "annu",     "Annu"),
                          "_".ALK_ATYPE_ID_ESPACEANNU => array("espace",    "contannu", "ContAnnu"),
                          "_".ALK_ATYPE_ID_ACTUSIT    => array("actu_01",   "actu",     "ActuExtra"),
                          "_".ALK_ATYPE_ID_FDOC       => array("fdoc_01",   "fdoc",     "FDoc"),
                          "_".ALK_ATYPE_ID_LSDIF      => array("lsdif_01",  "lsdif",    "LsDif"),
                          "_".ALK_ATYPE_ID_ACTU       => array("actu",      "actu",     "Actu"),
                          "_".ALK_ATYPE_ID_LIEN       => array("lien",      "lien",     "Lien"),
                          "_".ALK_ATYPE_ID_GEDIT      => array("gedit",     "page",     "Page"),
                          "_".ALK_ATYPE_ID_GLOS       => array("glos",      "glos",     "Glos"),
                          "_".ALK_ATYPE_ID_FAQS       => array("faqs",      "faqs",     "Faqs"),
                          "_".ALK_ATYPE_ID_SYND       => array("synd",      "synd",     "Synd"),
                          "_".ALK_ATYPE_ID_LETTRE     => array("", ""),
                          "_".ALK_ATYPE_ID_TRAD       => array("", ""),
                          "_".ALK_ATYPE_ID_SIGZA      => array("", ""),
                          "_".ALK_ATYPE_ID_RECETTE    => array("", ""),
                          "_".ALK_ATYPE_ID_SONDAGE    => array("", ""),
                          "_".ALK_ATYPE_ID_EXPLORER   => array("", ""),
                          "_".ALK_ATYPE_ID_FORMATION  => array("", ""),
                          "_".ALK_ATYPE_ID_FORUMSITE  => array("forum",     "forum",    "Forum"),
                          "_".ALK_ATYPE_ID_GCOEDIT    => array("", ""),
                          "_".ALK_ATYPE_ID_PELERINAGE => array("", ""),
                          "_".ALK_ATYPE_ID_BILLETTERIE=> array("", ""),
                          "_".ALK_ATYPE_ID_FDOC_DIST  => array("", "") );
  }

  /**
   * @brief Ouvre la connexion � la base et pr�-ouvre les queryCont, queryAnnu et queryContAnnuAction
   */
  function _InitConnection()
  {
    include_once("./../../classes/appli/alkquery.class.php");

    switch( ALK_TYPE_BDD ) {
    case SGBD_ORACLE:
      include_once("./../../classes/err/errOracle.class.php");
      include_once("./../../classes/db/droracle.class.php");
      include_once("./../../classes/db/dsoracle.class.php");
      include_once("./../../classes/db/dboracle.class.php");
      include_once("./../".ALK_DIR_ANNU."classes/queryannu_oracle.class.php");
      include_once("./../".ALK_DIR_ESPACE."classes/querycont_oracle.class.php");
      include_once("./../".ALK_DIR_ESPACE."classes/querycontannu_action_oracle.class.php");

      $this->dbConn = new DbOracle(ALK_ORA_LOGIN, ALK_ORA_PWD, ALK_ORA_SID);
      $this->dbConn->Connect();

      $this->tabQuery["_".ALK_ATYPE_ID_ESPACE]           = new QueryContOracle($this->dbConn, $this->tabLangue);
      $this->tabQuery["_".ALK_ATYPE_ID_ANNU]             = new QueryAnnuOracle($this->dbConn, $this->tabLangue);
      $this->tabQueryAction["_".ALK_ATYPE_ID_ESPACEANNU] = new QueryContAnnuActionOracle($this->dbConn, $this->tabLangue);
      break;
    
    case SGBD_MYSQL:
      include_once("./../../classes/err/errMysql.class.php");
      include_once("./../../classes/db/drmysql.class.php");
      include_once("./../../classes/db/dsmysql.class.php");
      include_once("./../../classes/db/dbmysql.class.php");
      include_once("./../".ALK_DIR_ANNU."classes/queryannu_mysql.class.php");;
      include_once("./../".ALK_DIR_ESPACE."classes/querycont_mysql.class.php");
      include_once("./../".ALK_DIR_ESPACE."classes/querycontannu_action_mysql.class.php");
      
      $this->dbConn = new DbMySql(ALK_MYSQL_LOGIN, ALK_MYSQL_HOST, ALK_MYSQL_PWD, ALK_MYSQL_BD, "", ALK_MYSQL_PORT);
      $this->dbConn->Connect();

      $this->tabQuery["_".ALK_ATYPE_ID_ESPACE]           = new QueryContMysql($this->dbConn, $this->tabLangue);
      $this->tabQuery["_".ALK_ATYPE_ID_ANNU]             = new QueryAnnuMysql($this->dbConn, $this->tabLangue);
      $this->tabQueryAction["_".ALK_ATYPE_ID_ESPACEANNU] = new QueryContAnnuActionMysql($this->dbConn, $this->tabLangue);
      break;
    
    case SGBD_POSTGRES:
      include_once("./../../classes/err/errPgsql.class.php");
      include_once("./../../classes/db/drpgsql.class.php");
      include_once("./../../classes/db/dspgsql.class.php");
      include_once("./../../classes/db/dbpgsql.class.php");
      include_once("./../".ALK_DIR_ANNU."classes/queryannu_pgsql.class.php");;
      include_once("./../".ALK_DIR_ESPACE."classes/querycont_pgsql.class.php");
      include_once("./../".ALK_DIR_ESPACE."classes/querycontannu_action_pgsql.class.php");
      
      $this->dbConn = new DbPgSql(ALK_POSTGRES_LOGIN, ALK_POSTGRES_HOST, ALK_POSTGRES_PWD, ALK_POSTGRES_BD, ALK_POSTGRES_PORT);
      $this->dbConn->Connect();

      $this->tabQuery["_".ALK_ATYPE_ID_ESPACE]           = new QueryContPgsql($this->dbConn, $this->tabLangue);
      $this->tabQuery["_".ALK_ATYPE_ID_ANNU]             = new QueryAnnuPgsql($this->dbConn, $this->tabLangue);
      $this->tabQueryAction["_".ALK_ATYPE_ID_ESPACEANNU] = new QueryContAnnuActionPgsql($this->dbConn, $this->tabLangue);
      break;
    }
  }
  
  /**
   * @brief Retourne une r�f�rence vers la connexion ouverte de la base de donn�es
   *
   * @param dbConn  Objet de type Db pass� en r�f�rence
   */
  function GetConnection(&$db)
  {
    $db =& $this->dbConn;
  }

  /**
   * @brief Retourne une r�f�rence sur le query de l'application de type atype_id
   *
   * @param oQuery    Objet de type AlkQuery pass� en r�f�rence
   * @param atype_id  Identifiant du type de l'application
   */
  function GetQuery(&$oQuery, $atype_id)
  {
    $strKey = "_".$atype_id;
    $oQuery = null;
    if( array_key_exists($strKey, $this->tabQuery) ) {
      $oQuery =& $this->tabQuery[$strKey];
      return;
    }

    $strAbrvFile  = $this->tabSGBD[ALK_TYPE_BDD]["file"];
    $strAbrvClass = $this->tabSGBD[ALK_TYPE_BDD]["class"];
    $strDir = $this->tabDir[$strKey][0];
    $strAbrvAppliFile  = $this->tabDir[$strKey][1];
    $strAbrvAppliClass = $this->tabDir[$strKey][2];

    if( $strAbrvAppliFile != "" ) {
      include_once("../../".$strDir."/classes/query".$strAbrvAppliFile."_".$strAbrvFile.".class.php");
      eval("\$this->tabQuery[\$strKey] = new Query".$strAbrvAppliClass.$strAbrvClass."(\$dbConn, \$this->tabLangue);");

      $oQuery =& $this->tabQuery[$strKey];
    }
  }
  
  /**
   * @brief Retourne une r�f�rence sur le queryAction de l'application de type atype_id
   *
   * @param oQueryAction  Objet de type AlkQuery pass� en r�f�rence
   * @param atype_id      Identifiant du type de l'application
   */
  function GetQueryAction(&$oQueryAction, $atype_id)
  {
    $strKey = "_".$atype_id;
    $oQueryAction = null;
    if( array_key_exists($strKey, $this->tabQueryAction) ) {
      $oQueryAction =& $this->tabQueryAction[$strKey];
      return;
    }
    
    $strAbrvFile  = $this->tabSGBD[ALK_TYPE_BDD]["file"];
    $strAbrvClass = $this->tabSGBD[ALK_TYPE_BDD]["class"];
    $strDir = $this->tabDir[$strKey][0];
    $strAbrvAppliFile  = $this->tabDir[$strKey][1];
    $strAbrvAppliClass = $this->tabDir[$strKey][2];

    if( $strAbrvAppliFile != "" ) {
      include_once("../../".$strDir."/classes/query".$strAbrvAppliFile."_action_".$strAbrvFile.".class.php");
      eval("\$this->tabQueryAction[\$strKey] = new Query".$strAbrvAppliClass."Action".$strAbrvClass."(\$dbConn, \$this->tabLangue);");

      $oQueryAction =& $this->tabQueryAction[$strKey];
    }
  }
}

?>