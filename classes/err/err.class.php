<?php

if( !(defined("ALK_B_LOG") == true && ALK_B_LOG==true) ) {
  // cas o� le app_conf.php ne param�tre pas la gestion d'erreur
  if( !(defined("ALK_B_LOG")) ) define("ALK_B_LOG", false);

  if( !(defined("ECRAN_LOG")) ) define('ECRAN_LOG', 1);
  if( !(defined("FILE_LOG")) )  define('FILE_LOG',  2);
  if( !(defined("MAIL_LOG")) )  define('MAIL_LOG',  4);

  // mode de gestion : = ECRAN_LOG+MAIL_LOG en ligne
  if( !(defined("ERROR_GEST")) ) define('ERROR_GEST', ECRAN_LOG); // mode de gestion

  // 0 : pas de trace d'execution de la pile, 1 : trace d'execution de la pile des variables
  if( !(defined("DEBUG")) )     define('DEBUG', 1);

  if( !(defined("MAIL_DEST")) ) define('MAIL_DEST', "p.cliquet@wanadoo.fr");
  if( !(defined("FILE_PATH")) ) define('FILE_PATH', $_SERVER["DOCUMENT_ROOT"]."/");
  if( !(defined("FILE_NAME")) ) define('FILE_NAME', "errlog.txt");
}

if( DEBUG == 1 ) {
	include_once "debuglib.php.inc";
}

/**
 * @class Class err 
 *
 * @brief Classe de base permettant la gestion des erreurs des scripts
 */
class err
{
	/** Mode de gestion des cas d'erreur 1 : msg ecran, 2 : msg fichier, 4 : msg mail, 3 : 1+2 , 5 : 1+4 , 6 : 2+4 */
	var $iMod;
    
	/** chaine des adresses mail des destinataires des msg d'erreur (separateur : point-virgule) */
	var $strDest;
    
	/** Chemin physique du r�pertoire contenant le fichier de msg d'erreur */
	var $strPath;
	
	/** Nom du fichier contenant les msg d'erreur */
	var $strFileName;

  /**
   * @brief constructeur de la classe err : initialisation des attributs de l'objet err
   *
   * @param iMod
   * @param strDest
   * @param strPath
   * @param strFileName
   */
  function err($iMod, $strDest='', $strPath='', $strFileName='')
  {		
		$this->iMod = $iMod;
		$this->strDest = $strDest;
		$this->strPath = $strPath;
		$this->strFileName = $strFileName;
	}

  /**
   * @brief g�re le traitement du cas d'erreur
   *
   * @param msg
   * @param ctx
   */
	function _traiteErreur($msg, $ctx)
  {
    $msgtmp = $msg;
			
    $msgEcran = str_replace("\t","<br>", $msgtmp);
    $msgEcran = str_replace("\n","<p>", $msgtmp);

    if( DEBUG == 1 ) {
      ob_start(); // debut de la bufferisation de sortie
      print_r($ctx);
      $msgtmp .= ob_get_contents();
      ob_clean();
      $msgtmp .= show_vars(TRUE, TRUE);
    }

    $msgLog = $msgtmp;
			
    $msgMail = $msgtmp;
    $headers = "Subject: SIT - Erreur\r\nContent-type: text/html; charset=iso-8859-1\r\n";
    switch($this->iMod) {
    case 1 :
      echo $msgEcran."<br>";
      break;
    case 2 :
      error_log($msgLog, 3, $this->strPath.$this->strFileName);
      break;
    case 3 :
      error_log($msgLog, 3, $this->strPath.$this->strFileName);
      echo $msgEcran;
      break;
    case 4 :
      error_log($msgMail, 1, $this->strDest, $headers);
      break;
    case 5 :
      echo $msgEcran;
      error_log($msgMail, 1, $this->strDest, $headers);
      break;
    case 6 :
      error_log($msgLog,  3, $this->strPath. $this->strFileName);
      error_log($msgMail, 1, $this->strDest, $headers);
      break;
    }
    return true;
  }
	
	function _errError($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [Error]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }

	function _errWarning($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [Warning]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }

	function _errParse($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [Parse]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }

	function _errNotice($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [Notice]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
  }

	function _errCoreError($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [CoreError]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }

	function _errCoreWarning($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [CoreWarning]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg,$ctx);
  }

	function _errCompileError($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [CompileError]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }
	function _errCompileWarning($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [CompileWarning]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
  }

	function _errUserError($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [UserError]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }

	function _errUserWarning($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [UserWarning]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
  }

	function _errUserNotice($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [UserNotice]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
  }
}
?>