<?php
include_once ('err.class.php');

/**
 * Class errPgsql
 *
 * @brief Permet la gestion des erreurs des scripts
 */
class errPgsql extends err
{
}

function ErrorHandlerPgsql($no, $str, $file, $line, $ctx) 
{
  if( defined("ALK_B_LOG") && ALK_B_LOG==true ) {
    $objError = new errPgsql(ERROR_GEST, MAIL_DEST, FILE_PATH, FILE_NAME);
    
    switch( $no ) {
    case E_ERROR :
      $objError->_errError($no, $str, $file, $line, $ctx);
      break;
    case E_WARNING : 
      $objError->_errWarning($no, $str, $file, $line, $ctx);
      break;
    case E_PARSE :
      $objError->_errParse($no, $str, $file, $line, $ctx);
      break;
    case E_NOTICE :
      $objError->_errNotice($no, $str, $file, $line, $ctx);
      break;
    case E_CORE_ERROR :
      $objError->_errCoreError($no, $str, $file, $line, $ctx);
      break;
    case E_CORE_WARNING :
      $objError->_errCoreWarning($no, $str, $file, $line, $ctx);
      break;
    case E_COMPILE_ERROR :
      $objError->_errCompileError($no, $str, $file, $line, $ctx);
      break;
    case E_COMPILE_WARNING :
      $objError->_errCompileWarning($no, $str, $file, $line, $ctx);
      break;
    case E_USER_ERROR :
      $objError->_errUserError($no, $str, $file, $line, $ctx);
      break;
    case E_USER_WARNING :
      $objError->_errUserWarning($no, $str, $file, $line, $ctx);
      break;
    case E_USER_NOTICE :
      $objError->_errUserNotice($no, $str, $file, $line, $ctx);
      break;
    default:
      break;
    }
  }
}  

function startErrorHandlerPgsql()
{
  if( defined("ALK_B_LOG") && ALK_B_LOG==true ) {
    // on capte toutes les erreurs php
    set_error_handler('ErrorHandlerPgsql');
    error_reporting(E_ALL);
  }
}

function endErrorHandlerPgsql()
{
  if( defined("ALK_B_LOG") && ALK_B_LOG==true ) {
    // restore l'ancien gestionnaire
    restore_error_handler();

    // toujours � E_ALL, sp�cifier dans le lib_session
    //error_reporting(E_ALL ^ E_NOTICE);
  }
}
?>