<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

/**
 * @package Alkanet_Class_Pattern
 * @class AlkImportWarning
 * 
 * Classe d'erreurs de base pour l'import
 */
class AlkImportWarning extends AlkObject
{
	/** Emplacement  du fichier d'Erreur' */
	protected $strFichier_Warning;
	
	
	/**
	 * Constructeur de la classe ImportErreur : initialisation des attributs de ImportErreur.
	 *
	 * @param unknown_type $strFichier_Warning
	 */
	public function __construct( $strFichier_Warning )
	{
	  parent::__construct();
	  
	  $this->AlkImportWarning($strFichier_Warning);
	}
	
	/**
   * Constructeur de la classe ImportErreur : initialisation des attributs de ImportErreur.
   *
   * @param strFichier_Erreur Emplacement  du fichier d'import
   */
	public function AlkImportWarning( $strFichier_Warning )
	{
		$this->strFichier_Warning = $strFichier_Warning;
	}
	
  /**
   * Fonction de gestion d'erreur.
   *
   * @param unknown_type $numErreur
   * @param unknown_type $strMesWarning
   * @return int
   */
	public function erreur($numErreur, $strMesWarning="" )
	{
		
		// Ouverture du fichier en mode append
		$ferreur = @fopen($this->strFichier_Warning, "a");
		$iRes = 0;	
		if($ferreur){ 
			switch ($numErreur){
      case 7 :
				$strMessage = $strMesWarning;
				break;
			}
			
			@fwrite($ferreur, mb_convert_encoding($strMessage."\r\n", ALK_EXPORT_ENCODING, ALK_HTML_ENCODING));
			$iRes = 1;
		}
		
		@fclose($ferreur);
		return $iRes;  
	}
}
?>