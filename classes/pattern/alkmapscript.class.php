<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/form/alkhtmlform.class.php");


$globMapType = 0;
define("TYPE_MAP",      $globMapType++);
define("TYPE_KEYMAP",   $globMapType++);
define("TYPE_SCALEBAR", $globMapType++);
define("TYPE_FEATURE",  $globMapType++);
define("TYPE_LABEL",    $globMapType++);

function checkRequest(&$tabRequest)
{
  $tmp = array();
  foreach ($tabRequest as $key=>$value){
    $value = preg_replace("!SNIOM!usi", "-", $value);
    $value = preg_replace("!ECAPSE!usi", " ", $value);
    $tmp[$key] = $value;
  }
  $tabRequest = $tmp;
}


function checkRequest2($tabRequest)
{
  foreach($tabRequest as $key=>$value){
    if ( is_array($value) )
      $tabRequest[$key] = checkRequest2($value);
    else
      $tabRequest[$key] = str_replace("}", ")", $value);
  }
  return $tabRequest;
}

/**
 * @package Alkanet_Class_Pattern
 * @class AlkMapScript
 * 
 * @brief Contient la classe MapScript permettant de générer
 * une carte à l'aide de MapServer.
 */
class AlkMapScript
{
  
  /**
   * Constructeur par défaut
   */
  function __construct() 
  {
 
  }
  
 /**
  * Destructeur par défaut
  **/
  function __destruct()
  {
  }
  
  
  /**
   * Retourne le code javascript initalisant les constantes mapserver
   */
  function getMapserverConstants()
  {
    $tabConstants = array(
      //Boolean values:
        "MS_TRUE", "MS_FALSE", "MS_ON", "MS_OFF", "MS_YES", "MS_NO", 
        
      //Map units:
        "MS_INCHES", "MS_FEET", "MS_MILES", "MS_METERS", "MS_KILOMETERS", "MS_DD", "MS_PIXELS", 
        
      //Layer types:
        "MS_LAYER_POINT", "MS_LAYER_LINE", "MS_LAYER_POLYGON", "MS_LAYER_RASTER", "MS_LAYER_ANNOTATION", "MS_LAYER_QUERY", "MS_LAYER_CIRCLE", 
        "MS_LAYER_TILEINDEX",
        
      //Layer/Legend/Scalebar/Class Status:
        "MS_ON", "MS_OFF", "MS_DEFAULT", "MS_EMBED", "MS_DELETE",
        
      //Layer alpha transparency : allows alpha transparent pixmaps to be used with RGB map images
        "MS_GD_ALPHA",
        
      //Font types:
        "MS_TRUETYPE", "MS_BITMAP",
        
      //Label positions:
        "MS_UL", "MS_LR", "MS_UR", "MS_LL", "MS_CR", "MS_CL", "MS_UC", "MS_LC", "MS_CC", "MS_AUTO", "MS_XY",
        
      //Bitmap font styles:
        "MS_TINY", "MS_SMALL", "MS_MEDIUM", "MS_LARGE", "MS_GIANT",
        
      //Shape types:
        "MS_SHAPE_POINT", "MS_SHAPE_LINE", "MS_SHAPE_POLYGON", "MS_SHAPE_NULL",
        
      //Shapefile types:
        "MS_SHP_POINT", "MS_SHP_ARC", "MS_SHP_POLYGON", "MS_SHP_MULTIPOINT",
        
      //Query/join types:
        "MS_SINGLE", "MS_MULTIPLE",
        
      //Querymap styles:
        "MS_NORMAL", "MS_HILITE", "MS_SELECTED",
      
      //Connection Types:
        "MS_INLINE", "MS_SHAPEFILE", "MS_TILED_SHAPEFILE", "MS_SDE", "MS_OGR", "MS_TILED_OGR", "MS_POSTGIS", "MS_WMS", 
        "MS_ORACLESPATIAL", "MS_WFS", "MS_GRATICULE", "MS_MYGIS",
        
      //Symbol types:
        "MS_SYMBOL_SIMPLE", "MS_SYMBOL_VECTOR", "MS_SYMBOL_ELLIPSE", "MS_SYMBOL_PIXMAP", "MS_SYMBOL_TRUETYPE", "MS_SYMBOL_CARTOLINE",
      
      //Image Mode types (outputFormatObj):
        "MS_IMAGEMODE_PC256", "MS_IMAGEMODE_RGB", "MS_IMAGEMODE_RGBA", 
        "MS_IMAGEMODE_INT16", "MS_IMAGEMODE_FLOAT32", "MS_IMAGEMODE_BYTE", 
        "MS_IMAGEMODE_NULL"
    );
    
    $strJs = " MS_CONSTANTS = {};";
    foreach( $tabConstants as $constant ){
      if ( defined($constant) )
        $strJs .= "MS_CONSTANTS.".$constant." = '".constant($constant)."';";
    }
    return $strJs;
  }

  /*
   * Récupère les métadonnées du mapfile
   * @param mapfile chemin complet du mapfile 
   */
  function getLayersMetadata($mapfile)
  {
    $tabToken = ms_TokenizeMap($mapfile);
    $tabMetadata = array();
    $bLayer = false;
    $bStart = false;
    $iLayer = 0;
    for ($i=0; $i<count($tabToken); $i++){
      $strToken = $tabToken[$i];
      
      if ( mb_strtoupper($strToken)=="LAYER" ){
        $bLayer = true;
        $temp = array();
        continue;
      }
      if ( $bLayer && mb_strtoupper($strToken)=="METADATA" ){
        $bStart = true;
        continue;
      }
      if ( $bStart &&  mb_strtoupper($strToken)=="END" ){
        $iLayer++;
        $bStart = false;
        continue;
      }
      if ( $bLayer && !$bStart && mb_strtoupper($strToken)=="NAME" ){
        $tabMetadata[] = $tabMetadata[str_replace("\"", "", $tabToken[++$i])] = $temp;
        $bLayer = false;
        continue;
      }
      if ( $bStart ){
        $temp[$strToken] = str_replace("\"", "", $tabToken[++$i]); 
        continue;
      }
    }
    return $tabMetadata;
  }
  
  function readObject($oObject, $tabMetadata)
  {
    $tabFollow = array("ms_map_obj"=>array("numlayers"=>"getLayer"), "ms_layer_obj"=>array("numclasses"=>"getClass"), );
    
    $className = strtolower(get_class($oObject));
    $strJs = "{";
        
    foreach ($oObject as $key=>$value){
      if ( is_resource($value) || $key=="_handle_" || ($key=="connection" && $oObject->connectiontype==MS_POSTGIS) )
        continue;
      else if ( is_object($value) )
        $strJs .= $key." : ".AlkMapScript::readObject($value, $tabMetadata).",\n";
      else if ( is_string($value) )
        $strJs .= $key.": '".addslashes($value)."', \n";
     else 
        $strJs .= $key.": ".addslashes($value).", \n";  
    }
    
    if ( $className=="ms_map_obj" ){
      $strJs .= " metadata : {";
      $strJs .= "\"METAFICHE\": '".addslashes($oObject->getMetaData("METAFICHE"))."', \n";
      $strJs .= "\"MAPEXTENT\": '".addslashes($oObject->getMetaData("MAPEXTENT"))."', \n";
      $strJs .= "},";
    }
    
    if ( $className=="ms_layer_obj" && array_key_exists($oObject->index, $tabMetadata) ){
      $keyname = "\"INTERNAL_NAME\"";
      $strJs .= " metadata : {";
      foreach ( $tabMetadata[$oObject->index] as $key=>$value){
        $strJs .= $key." : \"".addslashes($value)."\", ";
      }
      $strJs .= "},";
      if ( array_key_exists($keyname, $tabMetadata[$oObject->index]) )
        $strJs .= " guid: \"".addslashes($tabMetadata[$oObject->index][$keyname])."\",";
      else
        $strJs .= " guid: \"".addslashes($oObject->name)."\",";
      $strJs .= " filter : \"".str_replace("\"", "", $oObject->getFilter())."\",";
    }
    
    if ( in_array($className, array_keys($tabFollow)) ){
      $tabFollowObj = $tabFollow[$className];
      foreach($tabFollowObj as $numFollow=>$funcFollow){
        $strJs .= str_replace("num", "", $numFollow)." : [";
        
        eval("for (\$i=0; \$i<\$oObject->".$numFollow."; \$i++){" .
            "  \$oFollow = \$oObject->".$funcFollow."(\$i);" .
            "  if ( !\$oFollow ) continue;" .
            "  \$strJs .= (\$i>0 ? ', ': '').AlkMapScript::readObject(\$oFollow, \$tabMetadata);" .
            "}");
        
        $strJs .= "], \n"; 
      }
    }
    $strJs .= "}\n";
    $strJs = preg_replace("!,\s*\}!", "}", str_replace("\n", "\n", $strJs));
    return $strJs; 
  }
  
  function readCopyrights($oMap)
  {
    $strJs = "copyright = {";
    $copyright = $oMap->getMetadata("COPYRIGHT");
    if ( $copyright!= "" ){
      $strJs .= "type : 'TYPE_MAP',";
      $strJs .= "values : ['".addslashes($copyright)."']";
    } 
    else {
      $strJs .= "type : 'TYPE_LAYER',";
      $tabCopyrights = array();
      for ($i=0; $i<$oMap->numlayers; $i++){
        $oLayer = $oMap->getLayer($i);
        $copyright = $oLayer->getMetadata("COPYRIGHT");
        if ( $copyright!="" )
          $tabCopyrights[] = addslashes($copyright);
      }
      $tabCopyrights = array_unique($tabCopyrights);
      $strJs .= "values : ['".implode("', '", $tabCopyrights)."']";
    }
    $strJs .= "};";
    return $strJs;
  }
  
  function readNorthArrow($oMap)
  {
    $strJs = "northarrow = '".$oMap->getMetadata("NORTHARROW")."';";
    return $strJs;
  }
  
  function readProjection($oMap)
  {
    $strProjection = $oMap->getProjection();
    $strProjection = preg_replace("!\D+!", "", $strProjection);
    $strJs = "projection = '".$strProjection."';";
    return $strJs;
  }
  
  function readMapfile($mapfile, $layer="")
  {
    $tabMetadata = array();
    $oMap = ms_newMapObj($mapfile);
    if ( $layer!="" ){
      $old_mapfile = "";
      $strNewMapfile = str_replace(".map", "_temp.map", $mapfile); 
      for ($i=0; $i<$oMap->numlayers; $i++){
        $oLayer = $oMap->getLayer($i);
        if ( $oLayer->name!=$layer && $oLayer->getMetadata("INTERNAL_NAME")!=$layer )
          $oLayer->set("status", MS_DELETE);
        else {
          if ( $oLayer->getMetadata("MAPFILE_BACKGROUND")!="" ){
            if ( $oMap2 = @ms_newMapObj(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES.$oLayer->getMetadata("MAPFILE_BACKGROUND")) ){
              $oMap =& $oMap2;
              $oLayerToAdd = $oLayer;
              $old_mapfile = $mapfile;
              $mapfile = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES.$oLayer->getMetadata("MAPFILE_BACKGROUND");
            }
          }
          else {
            $oMap->save($strNewMapfile);
            $oMap = ms_newMapObj($strNewMapfile);
          }
        }
      }
    }
    
    $tabMetadata = array_merge($tabMetadata, AlkMapScript::getLayersMetadata($mapfile));
    
    $strJs =  " var mapProperties = ".AlkMapScript::readObject($oMap, $tabMetadata).";";
    $strJs .= " mapProperties.mapfile = '".str_replace(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES, "", $mapfile)."';";
    $strJs .= " mapProperties.dirmap = '".ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."';";
    $strJs .= " mapProperties.maspervUrl = '".ALK_ALKANET_ROOT_URL."cgi-bin/mapserv';";
    $strJs .= " mapProperties.".AlkMapScript::readCopyrights($oMap);
    $strJs .= " mapProperties.".AlkMapScript::readNorthArrow($oMap);
    $strJs .= " mapProperties.".AlkMapScript::readProjection($oMap);
    $strJs .= "mapProperties.bShowFondPlan = ".(defined("ALK_B_SIG_SHOW_FDP") && ALK_B_SIG_SHOW_FDP ? "true" : "false").";";
    $guid = $oMap->getMetadata("GUID");
    if ( $guid!="" ){
      $strJs .= " mapProperties.guid = '".$guid."';";
    }
    $default_layer_query = $oMap->getMetadata("layerquery");
    if ( $default_layer_query!="" ){
      $strJs .= " mapProperties.layerquery = '".$default_layer_query."';";
    }else if ($oMap->numlayers>0){//première couche
    	$iLayer = $oMap->numlayers-1;
    	$strJs .= " mapProperties.layerquery = '".$oMap->getLayer($iLayer)->getMetadata("INTERNAL_NAME")."';";
    }
    
    if ( $layer!="" ){
      @unlink($strNewMapfile);
    }
    
    if ( $layer!="" && $old_mapfile!="" ){
      $layerJs = AlkMapScript::readLayer($old_mapfile, $layer);
      if ( $layerJs!="" ){
        $strJs .= " var layer = ".$layerJs.";";
        $strJs .= " layer.group = 'Couche à visualiser';";
        $strJs .= " layer.mapfile = '".str_replace(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES, "", $old_mapfile)."';";
        $strJs .= " mapProperties.layers.push(layer);\n";
      }
      
    }
    return $strJs;
  }
  
  function readLayer($mapfile, $layername)
  {
    $oMap = ms_newMapObj($mapfile);
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      if ( $oLayer->name==$layername || $oLayer->getMetadata("INTERNAL_NAME")==$layername ){
        $tabMetadata = AlkMapScript::getLayersMetadata($mapfile);
        return AlkMapScript::readObject($oLayer, $tabMetadata);
      }
    }
    return "";   
  }
  

  function readLayerByIndex($mapfile, $index)
  {
    $oMap = ms_newMapObj($mapfile);
    if ( $index<0 || $index>$oMap->numlayers )
      return false;
    
    $oLayer = $oMap->getLayer($index);
    if ( !$oLayer ) return false;
    $tabMetadata = AlkMapScript::getLayersMetadata($mapfile);
    return AlkMapScript::readObject($oLayer, $tabMetadata);
  }

  /**
   * draw the map with multiple Layers
   */
  function getMapObject($mapfile, $tabLayer, $tabExtent=array(), $tabSize=array(), $tabQuery=array(), $features = array())
  {
    $oMap = ms_newMapObj($mapfile);
    if (!empty($tabExtent) && !empty($tabSize))
      AlkMapScript::setDimension($oMap, $tabExtent, $tabSize);
    
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      $oLayer->set("status", MS_DELETE);
    }
    

    $temp = array();
    $keys = array_keys($tabLayer);
    $values = array_values($tabLayer);
    for ($i=0; $i<count($keys); $i++){
      $temp[$keys[$i]] = $values[count($tabLayer)-$i-1];
    }
    $tabLayer = $temp;

    $nbLayers = count($tabLayer)+count($features);
    for ($iLayer=0; $iLayer<$nbLayers; $iLayer++){
      if ( array_key_exists($iLayer, $tabLayer) ){
        $value = $tabLayer[$iLayer];
        $tabLayersProperties = explode("@@", $value);
        if ( count($tabLayersProperties)==1 )
          $tabLayersProperties = explode(":", $value);
        if ( count($tabLayersProperties)>3 ){
          if ( $tabLayersProperties[0]=="WMS" )
            continue;
          $tabLayersProperties = array_slice($tabLayersProperties, 1);
        }
        //if(isset($tabLayersProperties[0]) && (strrpos($tabLayersProperties[0], "_")== false)){// eviter les layer de type buffer courants (save_buffer_32132) ou les buffer chargé (ex. 1_2.map);
          $mapName = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES.$tabLayersProperties[0];
          $layerName = $tabLayersProperties[1];
          $oMapExt = ms_newMapObj($mapName);
          $oLayerToAdd = AlkMapScript::getLayerObject($mapName, $layerName);
          if (!is_null($oLayerToAdd)) 
            ms_newLayerObj($oMap, $oLayerToAdd); 
        //}  
      }
      else if ( array_key_exists($iLayer, $features) ){
        $points = $features[$iLayer];
        AlkMapScript::setFeature($oMap, "feature".$iLayer, $points);
      }
    }
    $oMap->save(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."getMapObject.map");
    $oMap = ms_newMapObj(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."getMapObject.map");
    //@unlink(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."getMapObject.map");
    return $oMap;
  }
  
  
  function drawLayer($mapfile, $layername, $tabExtent, $tabSize, $tabQuery=array(), $listEntities ="", $processRaster=0, $oDefaultMap=null, $bTransparent=false, $bLabel=true)
  {
    
    
    $bQuery = false;
    if ( is_null($oDefaultMap) )
      $oMap = ms_newMapObj($mapfile);
    else
      $oMap = $oDefaultMap; 
    AlkMapScript::setDimension($oMap, $tabExtent, $tabSize);
  
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      
      if ( $oLayer->name==$layername || $oLayer->getMetadata("INTERNAL_NAME")==$layername ){

        $tabQuery["extent"] = $tabExtent;
        $tabQuery["size"] = $tabSize;
        $bQuery = AlkMapScript::doQuery($oMap, 
        ( isset($tabQuery["layerSelection"]) && $tabQuery["layerSelection"]== $oLayer->getMetadata("INTERNAL_NAME")) ? $tabQuery : array());
        $oLayer->set("status", MS_ON);
        AlkMapScript::LimitData($oMap, $oLayer, $listEntities);
        if ($bTransparent==false){
          $oLayer->set("transparency", 100);
        }

        if ($oLayer->type==MS_LAYER_RASTER && $oLayer->classitem!="[pixel]" ){//cas des GRID : transparence
          $oMap->outputformat->set("name", "jpeg");
          $oMap->outputformat->set("driver","GD/JPEG");
          $oMap->outputformat->set("mimetype","image/jpeg");
          $oMap->outputformat->set("imagemode",MS_IMAGEMODE_RGB);
          $oMap->outputformat->set("extension","jpg");
          $oMap->selectOutputFormat("jpeg");
        }
        else {
          $oMap->outputformat->set("name", "gif");
          $oMap->outputformat->set("driver","GD/GIF");
          $oMap->outputformat->set("mimetype","image/gif");
          $oMap->outputformat->set("imagemode",MS_IMAGEMODE_PC256);
          $oMap->outputformat->set("extension","gif");
          $oMap->selectOutputFormat("gif");
          $oMap->imagecolor->setRGB(-1, -1, -1);
          $oMap->set("transparent", MS_ON);
          $oMap->outputformat->set("transparent", MS_ON);
          if ($bLabel){
            $oLayer->set("postlabelcache", MS_TRUE);
            $oLayer->set("labelcache", MS_ON);
          }
        }
      }
      else {
        $oLayer->set("status", MS_OFF);
      }
      if ($processRaster == 1){
        //raster niveaux de gris
        $oLayer->setProcessing("BANDS=2");
      }
    }
    $oMap->imagecolor->setRGB(-1, -1, -1);
    $oMap->set("transparent", MS_ON);
    $oMap->outputformat->set("transparent", MS_ON);
    $img = null;
    if ($bQuery) {
      $img = @$oMap->drawQuery();
    }
    if ( !$img || is_null($img) ){    
      $img = $oMap->draw();
    }
        
    $rand = rand(0, 1000000);
    
    $imgfile = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_IMGMS.$rand.str_replace("/", "_", substr(basename($mapfile), 0, -4)).".gif";
    $img->saveImage($imgfile);
    
    return $imgfile;
  }
  
  
  function drawLabels($mapfile, $tabLayer, $tabExtent, $tabSize, $tabQuery, $listEntities)
  {
    $oMap = AlkMapScript::getMapObject($mapfile, $tabLayer, $tabExtent, $tabSize);
    $oMap->imagecolor->setRGB(-1, -1, -1);
    $oMap->set("transparent", MS_ON);
    $oMap->outputformat->set("transparent", MS_ON);
    $countLabelledLayers = 0; 
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      if ( $oLayer->labelitem=="" ){
        $oLayer->set("status", MS_OFF);
      }
      else {
        $countLabelledLayers++;
        AlkMapScript::LimitData($oMap, $oLayer, $listEntities);
        $oLayer->set("postlabelcache", MS_TRUE);
        $oLayer->set("labelcache", MS_ON);
      }
    }
    $extension=".png";
    if ($countLabelledLayers==0){//Aucun label dessiné => ne crée pas d'image => levée d'image error => non visible
      return "";
    }
    
    $img = $oMap->prepareImage();
    $oMap->draw();
    $oMap->drawLabelCache($img);
    $imgfile = ALK_PATH_SIG_IMGS."label".rand(0, 1000000).str_replace("/", "_", substr(basename($mapfile), 0, -4)).$extension;
    $img->saveImage($imgfile);
    return $imgfile;
  }
   /**
   * @brief limite l'affichage d'une couche en fonction des entités sélectionnées
   */
  function LimitData($oMap, $oLayer, $listEntities){
    if ($listEntities == "")
      return;
    $strData = $oLayer->data;
    $strFieldName = $oLayer->getMetaData("CLE_PRIMAIRE");
    if ($strFieldName != ""){
      $oLayer->set("data", str_replace(") as foo", " where ".$strFieldName." in ".$listEntities.") as foo", $strData));
    }
  }
  
  function drawScalebar($mapfile, $tabExtent, $tabSize)
  {
    if( func_num_args()>3 )
      $oMap = func_get_arg(3);
    else 
      $oMap = ms_newMapObj($mapfile);
    AlkMapScript::setDimension($oMap, $tabExtent, $tabSize);
    if($oMap->scale < 10000)
      $oMap->scalebar->set("units",MS_METERS);
    else
      $oMap->scalebar->set("units", MS_KILOMETERS);
    $img = $oMap->drawScalebar();
    $imgfile = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_IMGMS.rand(0, 1000000).str_replace("/", "_", substr(basename($mapfile), 0, -4)).".png";
    $img->saveImage($imgfile);
    return $imgfile;
  }
  
  function drawReference($mapfile, $tabExtent, $tabSize)
  {
    if( func_num_args()>3 )
      $oMap = func_get_arg(3);
    else 
      $oMap = ms_newMapObj($mapfile);
    AlkMapScript::setDimension($oMap, $tabExtent, $tabSize);
    $img = $oMap->drawReferenceMap();
    $imgfile = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_IMGMS.rand(0, 1000000).str_replace("/", "_", substr(basename($mapfile), 0, -4)).".png";
    $img->saveImage($imgfile);
    return $imgfile;
  }
  
  function drawMapfile($oMap, $mapfile, $tabExtent, $tabSize)
  {
    if( is_null($oMap) )
      $oMap = ms_newMapObj($mapfile);
    AlkMapScript::setDimension($oMap, $tabExtent, $tabSize);
    $img = $oMap->draw();
    
    $imgfile = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_IMGMS.rand(0, 1000000).str_replace("/", "_", substr(basename($mapfile), 0, -4)).".png";
    $img->saveImage($imgfile);
    return $imgfile;
  }
  
  function drawQueryMapfile($oMap, $mapfile, $tabExtent, $tabSize)
  {
    if( is_null($oMap) )
      $oMap = ms_newMapObj($mapfile);
    AlkMapScript::setDimension($oMap, $tabExtent, $tabSize);
    $img = $oMap->drawQuery();
    
    $imgfile = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_IMGMS.rand(0, 1000000).str_replace("/", "_", substr(basename($mapfile), 0, -4)).".png";
    $img->saveImage($imgfile);
    return $imgfile;
  }
  
  function &setFeature(&$oMap, $layerName, $tabPoints)
  {
    $typeProperties = array(
      "POINT"     => array("layer"=>"POINT",    "shape"=>"POINT",   "startIsEnd"=>false),
      "TEXT"      => array("layer"=>"POINT",    "shape"=>"POINT",   "startIsEnd"=>false),
      "ANNOTATION"=> array("layer"=>"POINT",    "shape"=>"POINT",   "startIsEnd"=>false),
      "CIRCLE"    => array("layer"=>"CIRCLE",   "shape"=>"POLYGON", "startIsEnd"=>false),
      "RECTANGLE" => array("layer"=>"POLYGON",  "shape"=>"POLYGON", "startIsEnd"=>true),
      "POLYLINE"  => array("layer"=>"LINE",     "shape"=>"LINE",    "startIsEnd"=>false),
      "POLYGON"   => array("layer"=>"POLYGON",  "shape"=>"POLYGON", "startIsEnd"=>true),
    );
    
    
    $oFeatureLayer = ms_newLayerObj($oMap);
    $oFeatureLayer->set("transparency", 80);
    $oFeatureLayer->set("name", $layerName);
    $oFeatureLayer->set("status", MS_ON);
    $oFeatureLayer->setMetadata("INTERNAL_NAME", $layerName);
    $oFeatureLayer->setMetaData("INTERROGEABLE", 1);
    foreach ($tabPoints as $points) {
      $type = $points["type"];
      $numpoints = $points["numpoints"];
      $fillcolor = (array_key_exists("fillcolor", $points) ? $points["fillcolor"] : null);
      $bordercolor = (array_key_exists("bordercolor", $points) ? $points["bordercolor"] : null);
      
      $oFeatureLayer->set("type", constant("MS_LAYER_".$typeProperties[$type]["layer"]));
      $newShape = ms_newShapeObj(constant("MS_LAYER_".$typeProperties[$type]["shape"]));
      $newLine = ms_newLineObj();
      
      if ( $type=="RECTANGLE" ){
        $copy = $points;
        $points = array();
        $points["x0"] = $copy["x0"];
        $points["y0"] = $copy["y0"];
        $points["text0"] = $copy["text0"];
        $points["x1"] = $copy["x0"];
        $points["y1"] = $copy["y1"];
        $points["text1"] = $copy["text0"];
        $points["x2"] = $copy["x1"];
        $points["y2"] = $copy["y1"];
        $points["text2"] = $copy["text0"];
        $points["x3"] = $copy["x1"];
        $points["y3"] = $copy["y0"];
        $points["text3"] = $copy["text0"];
        $numpoints = 4;
      }
      
      if ( $type=="CIRCLE" ){
        $copy = $points;
        $x0 = $copy["x0"];
        $y0 = $copy["y0"];
        $x1 = $copy["x1"];
        $y1 = $copy["y1"];
        
        $cx = $x0;
        $cy = $y0;
        
        $rx = abs($x1-$x0);
        $ry = abs($y1-$y0);
        
        $points["x0"] = $cx - max($rx, $ry);
        $points["y0"] = $cy - min($rx, $ry);
        
        $points["x1"] = $cx + max($rx, $ry);
        $points["y1"] = $cy + min($rx, $ry);
      }
      
      for ($i=0; $i<$numpoints; $i++){
        $px   = $points["x".$i];
        $py   = $points["y".$i];
        $txt  = $points["text".$i];
        $newLine->addXY($px, $py);
        
        $txt = str_replace("\r\n", "¤", $txt);        
        $newShape->set("text", stripslashes(urldecode($txt)));        
      }
      if ( $typeProperties[$type]["startIsEnd"] ){
        $px   = $points["x0"];
        $py   = $points["y0"];
        $txt  = $points["text0"];
        $newLine->addXY($px, $py);
        
        $txt = str_replace("\r\n", "¤", $txt);        
        $newShape->set("text", $txt);     
      }
      $newShape->add($newLine);
      $oFeatureLayer->addFeature($newShape);
      
      $pntClass = ms_newClassObj($oFeatureLayer);
      $pntClass->label->set("font", "Verdana");
      $pntClass->label->set("position", MS_UR);
      $pntClass->label->set("type", MS_TRUETYPE);
      
      $pntClass->label->set("size", 10);

      $pntClass->label->set("buffer", 2);
      $pntClass->label->set("wrap", ord('¤'));
      $clStyle = ms_newStyleObj($pntClass);
      
      switch ( $type ){
        case "POINT" : 
          if ( is_null($fillcolor) )
            $fillcolor = array("red"=>255, "green"=>220, "blue"=>200);
          if ( is_null($bordercolor) )
            $bordercolor = array("red"=>255, "green"=>0, "blue"=>0);
           $clStyle->color->setRGB($fillcolor["red"], $fillcolor["green"], $fillcolor["blue"]);
           $clStyle->outlinecolor->setRGB($bordercolor["red"], $bordercolor["green"], $bordercolor["blue"]);
           $clStyle->set("symbolname", "circle");
           $clStyle->set("size", 8);
        break;
        case "TEXT" : 
          // Nothing to do
        break;
        case "ANNOTATION" : 
          // Class properties
          $pntClass->label->color->setRGB(0, 0 , 0);
          $pntClass->label->backgroundcolor->setRGB(255, 255, 210);
          $pntClass->label->backgroundshadowcolor->setRGB(170, 170 , 170);
          $pntClass->label->set("backgroundshadowsizex", 2);
          $pntClass->label->set("backgroundshadowsizey", 2);
          
          if ( is_null($fillcolor) )
            $fillcolor = array("red"=>57, "green"=>128, "blue"=>0);
          if ( is_null($bordercolor) )
            $bordercolor = array("red"=>129, "green"=>144, "blue"=>255);
          $clStyle->color->setRGB($fillcolor["red"], $fillcolor["green"], $fillcolor["blue"]);
          $clStyle->outlinecolor->setRGB($bordercolor["red"], $bordercolor["green"], $bordercolor["blue"]);
          $clStyle->set("symbolname", "circle");
          $symSize = 12;
          $clStyle->set("size", $symSize);
        break;
        case "CIRCLE" : 
        case "RECTANGLE" : 
        case "POLYGON" : 
          // Class properties
          if ( is_null($fillcolor) )
            $fillcolor = array("red"=>255, "green"=>220, "blue"=>200);
          if ( is_null($bordercolor) )
            $bordercolor = array("red"=>255, "green"=>0, "blue"=>0);
          $clStyle->color->setRGB($fillcolor["red"], $fillcolor["green"], $fillcolor["blue"]);
          $clStyle->outlinecolor->setRGB($bordercolor["red"], $bordercolor["green"], $bordercolor["blue"]);
        break;
        case "POLYLINE" : 
          if ( is_null($bordercolor) )
            $bordercolor = array("red"=>255, "green"=>0, "blue"=>0);
          $oFeatureLayer->set("transparency", 80);
          $clStyle->outlinecolor->setRGB($bordercolor["red"], $bordercolor["green"], $bordercolor["blue"]);
        break;
      }
    }
    return $oFeatureLayer;
  }
  
  function drawFeature($mapfile, $layerName, $tabExtent, $tabSize, $tabPoints, $tabQuery=array(), $listEntities ="")
  {
    $oMap = ms_newMapObj($mapfile);
    AlkMapScript::setDimension($oMap, $tabExtent, $tabSize);
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      $oLayer->set("status", MS_DELETE);
    }
    
    AlkMapScript::setFeature($oMap, $layerName, $tabPoints);
    
    $oMap->querymap->color->setRGB(214, 245, 57);
    return AlkMapScript::drawLayer($mapfile, $layerName, $tabExtent, $tabSize, $tabQuery, $listEntities, 0, $oMap, false, false);
  }
  
  function setDimension(&$oMap, $tabExtent, $tabSize)
  {
    if ( count($tabSize)==2 ){
      $oMap->setSize($tabSize["width"], $tabSize["height"]);
      $oMap->querymap->set("width", $tabSize["width"]); 
      $oMap->querymap->set("height", $tabSize["height"]); 
    }
    if ( count($tabExtent)==4 )
      $oMap->setExtent($tabExtent["minx"], $tabExtent["miny"], $tabExtent["maxx"], $tabExtent["maxy"]);
    
  }
  
  function doQuery(&$oMap, $tabQuery)
  {
    if (empty($tabQuery))
      return false;
    
    $tabQuery = checkRequest2($tabQuery);
    if ( !array_key_exists("querycolored", $tabQuery) )
      $tabQuery["queryColored"] = 1;
    $querycolored = ($tabQuery["querycolored"]==1);
    switch ( $tabQuery["type"] ){
      case "QUERY_ATTRIBUTES" :
        AlkMapScript::checkQuery($oMap, $querycolored);
        return AlkMapScript::doQueryByAttributes($oMap, $tabQuery["layerSelection"], $tabQuery["queryRequest"],
                                 $tabQuery["queryItem"]);
      case "QUERY_SPATIAL" :
        AlkMapScript::checkQuery($oMap, $querycolored);
        return AlkMapScript::doQuerySpatial($oMap, $tabQuery["layerSelection"], $tabQuery["queryRequest"],
                                 $tabQuery["queryItem"], $tabQuery["layerReference"], $tabQuery["queryRequestB"]);
                                 
      case "QUERY_RECT" :
        return AlkMapScript::doQueryByRect($oMap, $tabQuery["mapSelection"], $tabQuery["layerSelection"], $tabQuery["extentQuery"],
                                 $tabQuery["extent"], $tabQuery["size"], false, false, true, $querycolored);
      case "QUERY_CIRCLE" :
        return AlkMapScript::doQueryByCircle($oMap, $tabQuery["mapSelection"], $tabQuery["layerSelection"], $tabQuery,
                                $tabQuery["extent"], $tabQuery["size"], false, false, $querycolored);
      case "QUERY_POLYGON" :
        return AlkMapScript::doQueryByShape($oMap, $tabQuery["mapSelection"], $tabQuery["layerSelection"], $tabQuery,
                                $tabQuery["extent"], $tabQuery["size"], false, false, $querycolored);
      case "ALK_TEMP" :
        return  AlkMapScript::doQueryByTempTable($oMap, $tabQuery["layerSelection"]);
    }
    return false; 
  }
  
  function checkQuery(&$oMap, $querycolored=true)
  {
    if ( $querycolored==1 )
      $oMap->querymap->set("style", MS_HILITE);
    else 
      $oMap->querymap->set("style", MS_NORMAL);
  }
  

  /**
   * effectue une selection par requetes
   */
  function doQueryByAttributes(&$oMap, $layername, $queryRequest, $queryItem)
  {
    //pour résoudre les problèmes d'encodage si on a un é dans l'url par exemple

    $bSuccess = false;
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      if ( $oLayer->name==$layername || $oLayer->getMetadata("INTERNAL_NAME")==$layername ){
        $tabParam = explode("_", $layername);
        $doc_id = $tabParam[1];
        $queryString = utf8_decode("(".str_replace("\'", "'", $queryRequest).")"); 
        $queryString = str_replace("þþ", "'", $queryString);  //select par linestring 
        $queryString = str_replace("ø", ".", $queryString);
        $queryString = str_replace("|", "\'", $queryString); 
        $oLayer->set("template", "query.html");

        $bSuccess = (@$oLayer->queryByAttributes($queryItem, $queryString, MS_MULTIPLE)== MS_SUCCESS);
      }
    }
    return $bSuccess;
  }
  /**
   * effectue une selection par requetes
   */
  function doQueryByTempTable(&$oMap, $layername, $returnResults=false, $bProcessTemplate=false)
  {
    $strSchema = ( ALK_SIGBD_SCHEMA != "" ? ALK_SIGBD_SCHEMA."." : "" );
  	$bSuccess = false;
    if ( !isset($_SESSION["SIG_TEMP_TABLE"]) ) return false;
    $mapProjection = $oMap->getProjection();
    $tabProjection = explode(":", $mapProjection);
    $mapProjection =  $tabProjection[1] ;
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      if ( $oLayer->name==$layername || $oLayer->getMetadata("INTERNAL_NAME")==$layername ){
        $tabDesc = explode("_", $layername);
        $queryRequest = "gid=tmpgid";
        $where = "";
        
        if ($oLayer->template=="")
          $oLayer->set("template", "MS_query.html");
        //echo $oLayer->data;
        $tabMatch = array();
        /*preg_match("!^the_geom from \((.+)\) as foo using unique gid using SRID = !", $oLayer->data, $tabMatch);print_r($tabMatch);
        $old_data = $tabMatch[1];
        $tabMatch = array();
        preg_match("!select (.+) from (.+)( where .+)?!", $old_data, $tabMatch); 
        $select = $tabMatch[1];
        $from = $tabMatch[2];
        $where = (count($tabMatch)>3 ? $tabMatch[3] : "");

        $select = preg_replace("!^\.?\*!", "$1lyr_".$tabDesc[1].".*", $select);
*/
        $where = "";
        $data = "the_geom from (select ".$strSchema."lyr_".$tabDesc[1].".*, temp.gid as tmpgid from ".$strSchema."lyr_".$tabDesc[1]." left join ".$strSchema.$_SESSION["SIG_TEMP_TABLE"]." as temp on (temp.gid=".$strSchema."lyr_".$tabDesc[1].".gid) ".$where.") as foo using unique gid using SRID = ".$mapProjection;
        $oLayer->set("data", $data);
        $oMap->freequery($oLayer->index);
        
        $bSuccess = (@$oLayer->queryByAttributes("the_geom", $queryRequest, MS_MULTIPLE)== MS_SUCCESS);
        if ($bSuccess){
          if ($returnResults && $bProcessTemplate && $oLayer->template != "MS_query.html") {
            echo "<script language='JavaScript' type='text/javascript'>top.oMap.execQueryOnLayer('', '".$layername."', '');</script>";
            echo $oMap->processquerytemplate(array(), MS_FALSE);
            
            exit();
          }
          if ($returnResults)
            return AlkMapScript::DumpQueryResults($oLayer); 
          else
            return true;
        }
      }
    }
    return $bSuccess;
  }
 
  
  /**
   * effectue une selection par requetes
   * $lastQueryRequest // prévu pour intégrer la sélection spatiale d'obj d'une coucheA à partir d'objets sélectionnés "=$queryRequestB" dans une couche B'
   **/
  function doQuerySpatial(&$oMap, $layername, $queryRequest, $queryItem, $lastLayerName, $lastQueryRequest)
  {
    $bSuccess = false;
    
    $mapProjection = $oMap->getProjection();
    $tabProjection = explode(":", $mapProjection);
    $mapProjection = $tabProjection[1] ;
    
    $tabSql = AlkMapScript::getQuerySpatial($layername, $queryRequest, $queryItem, $lastLayerName, $lastQueryRequest, $mapProjection);
    
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      if ( $oLayer->name==$layername || $oLayer->getMetadata("INTERNAL_NAME")==$layername ){
        $oLayer->set("data", $tabSql["select"]);
        $oLayer->set("template", "query.html");
        $bSuccess = (@$oLayer->queryByAttributes($queryItem, $tabSql["query"], MS_MULTIPLE)== MS_SUCCESS);
      }
    }
   return $bSuccess;
  }
  
  function getQuerySpatial($layername, $queryRequest, $queryItem, $lastLayerName, $lastQueryRequest, $mapProjection=-1)
  {
    
    $queryRequest = utf8_decode($queryRequest);
    $queryRequest = str_replace("§§", "&&", $queryRequest);    
    $queryRequest = str_replace("µ", "'", $queryRequest);
    
    if($lastQueryRequest!=""){
      $lastQueryRequest = utf8_decode($lastQueryRequest);
      $lastQueryRequest = str_replace("µ", "'", $lastQueryRequest);  //select par linestring 
      $lastQueryRequest = str_replace("§§", "&&", $lastQueryRequest);    
      $lastQueryRequest = str_replace("\'", "'", $lastQueryRequest);
      
      $pos = strpos($lastQueryRequest, "|"); 
      if(strpos($lastQueryRequest, "|")!= false && $pos == 0)    // si | en première position
        $lastQueryRequest[0]= "'";    
      $nbChar = strlen($lastQueryRequest);
      if($lastQueryRequest[$nbChar-1] == "|")// si | en dernière  position
        $lastQueryRequest[$nbChar-1] == "'";
      if(substr_count($lastQueryRequest, "GeometryFromText")==0){// si on n'est pas dans une requete de type geometryfromtext donc dans le cas d'un querybyattribute'
        $lastQueryRequest = str_replace("|", "\'", $lastQueryRequest); // on remplace les | en milieu de la chaine par \'
      }else{// cas d'une selection par geometry except pour selection par circle
        $lastQueryRequest = str_replace("|", "'", $lastQueryRequest);
      }
    }
    $tablayername = explode("_", $layername );
    $tablayernameB = explode("_", $lastLayerName );
    
    $layerId = $tablayername[1];
    $lastLayerId = $tablayernameB[1];
    
    $layerTable = $layerAlias = "lyr_".$layerId;
    $lastLayerTable = $lastLayerAlias = "lyr_".$lastLayerId;
    if ($lastLayerId == $layerId){ // si les deux couches sont les meme 
      $lastLayerAlias = "lyr_".$lastLayerId."2";
    }
    
    $lastQueryRequest = str_replace("[lastAlias]", $lastLayerAlias, str_replace("[alias]", $layerAlias, $lastQueryRequest));
    
    $allquery = "select ".$layerAlias.".gid, ".$layerAlias.".the_geom" .
           " from ".$layerTable." as ".$layerAlias.", ".$lastQueryRequest." as ".$lastLayerTable."" .
            " where ".str_replace("[lastAlias]", $lastLayerAlias, str_replace("[alias]", $layerAlias, $queryRequest))."" .
            "";
    
    $select = "the_geom from (select ".$layerAlias.".the_geom, ".$layerAlias.".gid, ".$lastLayerAlias.".the_geom as the_geom".$lastLayerAlias." from ".$layerTable." as ".$layerAlias.", ".$lastQueryRequest." as ".$lastLayerAlias.") as foo using unique gid using SRID=".$mapProjection;
    $query = str_replace("[lastAlias].the_geom", "the_geom".$lastLayerAlias, str_replace("[alias].the_geom", "the_geom", $queryRequest));
        
    return array("allquery"=>$allquery, "select"=>$select, "query"=>$query);
  }
  
  function doQueryByCircle($oMap, $mapfile, $layername, $tabQuery, $tabExtent, $tabSize, $returnInfos = false, $returnResults = false, $querycolored=true)
  {
    
    if (is_null($oMap)){
      $oMap = ms_newMapObj($mapfile);
    }
    AlkMapScript::setDimension($oMap, $tabExtent, $tabSize);
    AlkMapScript::checkQuery($oMap, $querycolored);
    
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      
      if ( $oLayer->name==$layername || $oLayer->getMetadata("INTERNAL_NAME")==$layername ){
        if ($oLayer->getMetaData("INTERROGEABLE")==1){
          if ($oLayer->template == "")
              $oLayer->set("template", "MS_query.html");
          /*$tabQuery["x0"] = AlkMapScript::Geo2Pix($tabQuery["x0"], 0, $tabSize["width"], $tabExtent["minx"], $tabExtent["maxx"], 0);
          $tabQuery["x1"] = AlkMapScript::Geo2Pix($tabQuery["x1"], 0, $tabSize["width"], $tabExtent["minx"], $tabExtent["maxx"], 0);
          $tabQuery["y0"] = AlkMapScript::Geo2Pix($tabQuery["y0"], 0, $tabSize["width"], $tabExtent["minx"], $tabExtent["maxx"], 1);
          $tabQuery["y1"] = AlkMapScript::Geo2Pix($tabQuery["y1"], 0, $tabSize["width"], $tabExtent["minx"], $tabExtent["maxx"], 1);
          */
          $buffer = sqrt(($tabQuery["x1"] - $tabQuery["x0"])*($tabQuery["x1"] - $tabQuery["x0"]) +
                   ($tabQuery["y1"] - $tabQuery["y0"])*($tabQuery["y1"] - $tabQuery["y0"]));
                   
          $new_pt = ms_newPointObj();
          $new_pt->setXY($tabQuery["x0"],$tabQuery["y0"]); 
          $bSucces = (@$oMap->queryByPoint($new_pt,MS_MULTIPLE, round($buffer))==MS_SUCCESS);
          if ($bSucces){
            if($returnInfos)
              return AlkMapScript::GetQueryInfo($mapfile, $oLayer);
            if($returnResults)  
              return AlkMapScript::DumpQueryResults($oLayer);
            else
              return true;  
          }
        }
      }
    }
    return array();
    
  }
  
  function doQueryByShape($oMap, $mapfile, $layername, $tabQuery, $tabExtent, $tabSize, $returnInfos = false, $returnResults = false, $querycolored=true){
    $tabRetour = array();
    if (is_null($oMap)){
      $oMap = ms_newMapObj($mapfile);
    }
    AlkMapScript::setDimension($oMap, $tabExtent, $tabSize);
    AlkMapScript::checkQuery($oMap, $querycolored);
    
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      
      if ( $oLayer->name==$layername || $oLayer->getMetadata("INTERNAL_NAME")==$layername ){
        if ($oLayer->getMetaData("INTERROGEABLE")==1){
          if ($oLayer->template == "")
              $oLayer->set("template", "MS_query.html");
          $newShape = ms_newShapeObj(MS_SHAPE_POLYGON);
          $numPoints = $tabQuery["numpoints"];
          for ($j=0; $j<$numPoints; $j++){
            //echo "x : ".$tabQuery["x".$j]. " y: ".$tabQuery["y".$j]."<br>";
            $newLine = ms_newLineObj();
            $newLine->addXY($tabQuery["x".$j], $tabQuery["y".$j]);
            $newShape->add($newLine);
          }
          //print_r($newShape);
          $bSucces = ( @$oLayer->queryByShape($newShape)==MS_SUCCESS );
          if ($bSucces){
            if ($returnResults)
              return AlkMapScript::DumpQueryResults($oLayer);
            elseif ($returnInfos)
              return AlkMapScript::GetQueryInfo($mapfile, $oLayer);
            else
              return true;
          }
        }
      }
    }
  }
  
  function doQueryByRect($oMap, $mapfile, $layername, $tabQuery, $tabExtent, $tabSize, 
                         $returnInfos = false, $returnResults = false,
                         $bProcessTemplate = true, $querycolored=true, $bCheckFilter= false 
  ){
    $tabRetour = array();
    if (is_null($oMap)){
      $oMap = ms_newMapObj($mapfile);
    }
    AlkMapScript::setDimension($oMap, $tabExtent, $tabSize);
    AlkMapScript::checkQuery($oMap, $querycolored);
    if ($tabQuery["minx"] == $tabQuery["maxx"]){//sélection ponctuelle               
      if ( defined("ALK_SIG_TOLERANCE_QUERY") ){
        $tolerance_geo = AlkMapScript::Pix2Geo(ALK_SIG_TOLERANCE_QUERY, 0, $tabSize["width"], $tabExtent["minx"], $tabExtent["maxx"], false)-$tabExtent["minx"];
        $tabQuery["minx"] = $tabQuery["minx"]-$tolerance_geo;
        $tabQuery["miny"] = $tabQuery["miny"]-$tolerance_geo;
        $tabQuery["maxx"] = $tabQuery["maxx"]+$tolerance_geo;
        $tabQuery["maxy"] = $tabQuery["maxy"]+$tolerance_geo;
      }
    }
    $oGeoPoint = ms_newPointObj(); 
    $oGeoPoint->setXY($tabQuery["minx"], $tabQuery["miny"]);
    
    $oGeorefRect = ms_newRectObj();
    $oGeorefRect->setExtent($tabQuery["minx"], $tabQuery["miny"], $tabQuery["maxx"], $tabQuery["maxy"]);
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      if ( $oLayer->name==$layername || $oLayer->getMetadata("INTERNAL_NAME")==$layername ){
        
        if ($oLayer->getMetaData("INTERROGEABLE")==1){
          if ($oLayer->template == "")
              $oLayer->set("template", "MS_query.html");
          //set filter
          if($bCheckFilter){
            $filterName = substr($oLayer->getFilter(), 2,  -2);
            $filter = Request($filterName, REQ_GET_POST, "1=1");
            $oLayer->setFilter(urldecode($filter));
          }
              
          if ($oLayer->connectiontype == MS_WMS  ){
            $nXCenter = $tabQuery["minx"];
            $nYCenter = $tabQuery["miny"];
            if ($tabQuery["minx"] != $tabQuery["maxx"]){
              //query effectué sur le centre
              $nXCenter = ($tabQuery["maxx"] -  $tabQuery["minx"])/2 + $tabQuery["minx"];
              $nYCenter = ($tabQuery["maxy"] -  $tabQuery["miny"])/2 + $tabQuery["miny"];
            }
            $nXCenter = AlkMapScript::Geo2Pix($nXCenter, 0, $tabSize["width"], $tabExtent["minx"], $tabExtent["maxx"], 0);
            $nYCenter = AlkMapScript::Geo2Pix($nYCenter, 0, $tabSize["height"], $tabExtent["miny"], $tabExtent["maxy"], 0);
            $wmsUrl = $oLayer->getWMSFeatureInfoURL($nXCenter, $nYCenter, 0, $oLayer->getMetaData("wms_info_format"));
            $tabRetour["mapfile"] = $mapfile;
            $tabRetour["name"]= $oLayer->getMetaData("INTERNAL_NAME");
            $tabRetour["description"] = $oLayer->getMetaData("DESCRIPTION");
            $tabRetour["group"] = $oLayer->group;
            $tabRetour["type"] = $oLayer->type;
            $tabRetour["wmsUrl"] = $wmsUrl;
            $tabRetour["wmsInfoFormat"] = $oLayer->getMetaData("wms_info_format");
            if($returnInfos)
              return $tabRetour;
            else
              return false;  
          }
          else{//Non WMS
            
            $bSucces = ( @$oLayer->queryByRect($oGeorefRect)==MS_SUCCESS )
                    || ( @$oLayer->queryByPoint($oGeoPoint, MS_MULTIPLE)==MS_SUCCESS );
            if ($bSucces){
              if ($returnResults && $bProcessTemplate && $oLayer->template != "MS_query.html") 
                return $oMap->processquerytemplate(array(), MS_FALSE);
              if ($returnResults)
                return AlkMapScript::DumpQueryResults($oLayer);
              elseif ($returnInfos)
                return AlkMapScript::GetQueryInfo($mapfile, $oLayer);
              else
                return true;
            }
          }
        }
      }
    }
    return $tabRetour;
  }
  
  
  function GetQueryInfo($mapfile, $oLayer){
    $tabRetour = array ();
    $numResultsTotal = 0;
    $numResults = $oLayer->getNumResults();
    if ($numResults == 0)
      return  $tabRetour;
    else{
      $tabRetour["mapfile"] = $mapfile;
      $tabRetour["name"]= $oLayer->getMetaData("INTERNAL_NAME");
      $tabRetour["description"] = $oLayer->getMetaData("TITLE");
      $tabRetour["group"] = $oLayer->group;
      $tabRetour["type"] = $oLayer->type;
    }
    return $tabRetour;
  }
  
  function DumpQueryResults($oLayer) {
    $tabRetour = array ();
    $numResultsTotal = 0;
    $tabRetourLayer = array();
    $numResults = $oLayer->getNumResults();
    if ($numResults == 0)
      return $tabRetour; // No results in this layer
      
    $oLayer->open();
    for ($iRes = 0; $iRes < $numResults; $iRes ++) {
      $oRes = $oLayer->getResult($iRes);
      $oShape = @$oLayer->getShape($oRes->tileindex, $oRes->shapeindex);
      if ( is_null($oShape)) continue;
      if ($iRes == 0) {
        if ($oLayer->getMetaData("RESULT_FIELDS")) {
          $selFields = explode("|", $oLayer->getMetaData("RESULT_FIELDS"));
        } else {
          $i = 0;
          while (list ($key, $val) = each($oShape->values)) {
            $selFields[$i ++] = $key;
            if ($i >= 4)
              break;
          }
        }

        $tabRetourLayer[0] = array ();
        $tabRetourLayer[0]["name"] = $oLayer->getMetaData("INTERNAL_NAME");//$oLayer->name;
        $tabRetourLayer[0]["description"] = $oLayer->getMetaData("TITLE");
        $tabRetourLayer[0]["data"] = $oLayer->data;
      
        $tabRetourLayer[1] = array ();
        for ($iField = 0; $iField < count($selFields); $iField ++) {
          $tabRetourLayer[1][$iField] = $selFields[$iField];
        }
      }
      for ($iField = 0; $iField < sizeof($selFields); $iField ++) {
        $tabRetourLayer[2][$iRes][$iField] = $oShape->values[$selFields[$iField]];
      }
      $oShape->free();
      $numResultsTotal ++;
    }
    $tabRetour = $tabRetourLayer;
    $oLayer->close();
    return $tabRetour;
  }
  
  function Geo2Pix($nGeoPos, $dfPixMin, $dfPixMax, $dfGeoMin, $dfGeoMax, $nInverseGeo){
    $dfWidthGeo = max(1, $dfGeoMax - $dfGeoMin);
    $dfWidthPix = max(1, $dfPixMax - $dfPixMin);
    
    $dfGeoToPix = $dfWidthPix / $dfWidthGeo;
    
    if (!$nInverseGeo) {
      $dfDeltaGeo = $nGeoPos - $dfGeoMin;
    } else {
      $dfDeltaGeo = $dfGeoMax - $nGeoPos;
    }
    
    $dfDeltaPix = $dfDeltaGeo * $dfGeoToPix;
    $dfPosPix = $dfPixMin + $dfDeltaPix;
    
    return round($dfPosPix);
  }
  
  function Pix2Geo($nPixPos, $dfPixMin, $dfPixMax, $dfGeoMin, $dfGeoMax, $nInversePix) {
    $dfWidthGeo = $dfGeoMax - $dfGeoMin;
    $dfWidthPix = $dfPixMax - $dfPixMin;

    $dfPixToGeo = $dfWidthGeo / $dfWidthPix;

    if (!$nInversePix) {
      $dfDeltaPix = $nPixPos - $dfPixMin;
    } else {
      $dfDeltaPix = $dfPixMax - $nPixPos;
    }

    $dfDeltaGeo = $dfDeltaPix * $dfPixToGeo;
    $dfPosGeo = $dfGeoMin + $dfDeltaGeo;

    return ($dfPosGeo);
  }
  
  function getLayerObject($mapfile, $layername){
    $oMap = @ms_newMapObj($mapfile);
    if ( is_bool($oMap) )
      return null; 
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      if ( $oLayer->name==$layername || $oLayer->getMetadata("INTERNAL_NAME")==$layername ){
        return $oLayer;
      }
    }
    return null;
  }
  
  /**
   * @brief Crée un layer de numerisation à partir du modèle modele.map
   */
  function createLayerNumerisation($couche_doc_id, $couche_intitule, $couche_type, $mapProjection, $fillcolor, $bordercolor, $mapfile_background, $tablePrefixe="lyr_")
  {
    $tabFillColor = explode(",", $fillcolor);
    $tabBorderColor = explode(",", $bordercolor);
    
    $tabType = array(
      ALK_SIG_TYPE_COUCHE_POINT     => MS_LAYER_POINT,
      ALK_SIG_TYPE_COUCHE_POLYLIGNE => MS_LAYER_LINE,
      ALK_SIG_TYPE_COUCHE_POLYGONE  => MS_LAYER_POLYGON,
    );
    AlkMapScript::setProjLib(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."numerisation.map");
    if  ( !$oMap = @ms_newMapObj(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."numerisation.map") ){
      AlkMapScript::setProjLib(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."modele.map");
      $oMap = @ms_newMapObj(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."modele.map");
      if ( !$oMap ) die("Création de cette couche impossible");
    
    if (file_exists(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES . "etc/fonts.txt"))
      $oMap->setFontSet(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES . "etc/fonts.txt");
    $oMap->setSymbolSet(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES . "etc/symboles/symbols.sym");
    $oMap->set("shapepath", ALK_PATH_SIG_THEMES);
    $oMap->web->set("imagepath", ALK_PATH_SIG_IMGS);
      for ($i=0; $i<$oMap->numlayers; $i++){
        $oLayer = $oMap->getLayer($i);
        $oLayer->set("status", MS_DELETE);
      }            
    }
    
    $oLayer = ms_newLayerObj($oMap);
    $oLayer->set("name", $couche_intitule);
    $oLayer->set("type", $tabType[$couche_type]);
    $oLayer->set("connectiontype", MS_POSTGIS);
    $oLayer->set("data", "the_geom from (select * from ".$tablePrefixe.$couche_doc_id.") as foo using unique gid using SRID=".$mapProjection);
    $oLayer->set("connection", "user=".ALK_POSTGRES_LOGIN." dbname=".ALK_POSTGRES_BD." host=".ALK_POSTGRES_HOST." port=".ALK_POSTGRES_PORT);
    $oLayer->setMetadata("INTERNAL_NAME", "layer_".$couche_doc_id);
    $oLayer->setMetadata("IS_NUMERISATION", "1");
    $oLayer->setMetadata("MAPFILE_BACKGROUND", $mapfile_background);
    $oLayer->setMetadata("legende_name_0", "numerisation");
    $oLayer->set("transparency", 80);
    $oLayer->set("status", MS_ON);
    
    $oClass = ms_newClassObj($oLayer);
    $oStyle = ms_newStyleObj($oClass);
    $oStyle->color->setRGB(-1, -1, -1);
    $oStyle->outlinecolor->setRGB(-1, -1, -1);
    if ( count($tabFillColor)==3 ){
      $oStyle->color->setRGB($tabFillColor[0], $tabFillColor[1], $tabFillColor[2]);
    }
    if ( count($tabBorderColor)==3 ){
      $oStyle->outlinecolor->setRGB($tabBorderColor[0], $tabBorderColor[1], $tabBorderColor[2]);
    }
    
    if ( $couche_type==ALK_SIG_TYPE_COUCHE_POLYLIGNE ){
      $oStyle->color->setRGB(-1, -1, -1);
    }
    if ( $couche_type==ALK_SIG_TYPE_COUCHE_POINT ){
      $oStyle->set("size", 10);
      $oStyle->set("symbolname", "circle");
    }
    
    $oMap->save(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."numerisation.map");
  }
    
  /**
   * @brief Change le nom d'un layer de numerisation
   */
  function setNameLayerNumerisation($couche_doc_id, $couche_intitule, $fillcolor, $bordercolor)
  {
    $tabFillColor = explode(",", $fillcolor);
    $tabBorderColor = explode(",", $bordercolor);
    
    $mapfile = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."numerisation.map";
    $oMap = ms_newMapObj($mapfile);
    
    $layername = "layer_".$couche_doc_id;
    $oLayer = null;
    for ($i=0; $i<$oMap->numlayers; $i++){
      $oLayerTmp = $oMap->getLayer($i);
      if( $oLayerTmp->name==$layername || $oLayerTmp->getMetadata("INTERNAL_NAME")==$layername ){
        $oLayer =& $oLayerTmp;
        break;
      }
    }
    if ( is_null($oLayer) ) return; 
    $oLayer->set("name", $couche_intitule);
    $oLayer->set("transparency", 80);
    
    $oClass = $oLayer->getClass(0);
    $oStyle = $oClass->getStyle(0);
    $oStyle->color->setRGB(-1, -1, -1);
    $oStyle->outlinecolor->setRGB(-1, -1, -1);
    if ( count($tabFillColor)==3 ){
      $oStyle->color->setRGB($tabFillColor[0], $tabFillColor[1], $tabFillColor[2]);
    }
    if ( count($tabBorderColor)==3 ){
      $oStyle->outlinecolor->setRGB($tabBorderColor[0], $tabBorderColor[1], $tabBorderColor[2]);
    }
    
    $oMap->save($mapfile);
  }
  
  function makeMapObject($map)
  {
    $map = checkRequest2($map);
    
    $oMap = ms_newMapObj(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES.$map["mapfile"]);
    AlkMapScript::setDimension($oMap, $map["extent"], $map["size"]);
    for($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      $oLayer->set("status", MS_DELETE);
    }
    $filecopy = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."copy".str_replace("/", "_", $map["mapfile"]);
    $oMap->save($filecopy);
    $oMap = ms_newMapObj($filecopy);
    @unlink($filecopy);
    
    if ( array_key_exists("metadata", $map) ){
      foreach($map["metadata"] as $key=>$value){
        $oMap->setMetadata($key, urldecode($value));
      }
    }
    
    $layers = $map["layers"];
    foreach($layers as $layer){
      $mapfile = "";
      $oLayer = null;
      $bCopy = true;

      // Traitement des couches de dessin
      if ( array_key_exists("features", $layer) && is_array($layer["features"]) ){
        $bCopy = false;
        $oLayer = AlkMapScript::setFeature($oMap, $layer["guid"], $layer["features"]);
        $mapfile = $map["mapfile"];
      }

      // Traitement des couches définies localement ayant un groupe de couche
      else if ( $layer["mapgroup"]!="" ){
        $oLayer = AlkMapScript::getLayerObject(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES."carte".$layer["mapgroup"].".map", $layer["guid"]);
        if ( !is_null($oLayer) ){
          $oLayer->setMetadata("COUCHES_GROUP", $layer["mapgroup"]);
          if ( array_key_exists($layer["mapgroup"], $map["mapgroups"]) )
          $oLayer->setMetadata("COUCHES_GROUP_NOM", urldecode($map["mapgroups"][ $layer["mapgroup"] ]["mapgroup_name"]));
        }
        $mapfile = "carte".$layer["mapgroup"].".map";
      }

      // Traitement des couches définies localement n'ayant pas de groupe de couche
      else if ( $layer["mapfile"]!="" && $layer["guid"]!="" ){
        $oLayer = AlkMapScript::getLayerObject(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES.$layer["mapfile"], $layer["guid"]);
        $mapfile = $map["mapfile"];
      }

      // Traitement des WMS importés
      else if ( array_key_exists("connection", $layer) && $layer["connection"]!="" ){
        $bCopy = false;
        $oLayer = ms_newLayerObj($oMap);
        $oLayer->set("name", $layer["name"]);
        $oLayer->set("connection", $layer["connection"]);
        $oLayer->set("connectiontype", $layer["connectiontype"]);
        $oLayer->set("type", $layer["type"]);
        foreach ($layer["metadata"] as $key=>$value)
          $oLayer->setMetadata($key, urldecode($value));
      }
      
      if ( !is_null($oLayer) ){
        if ( $bCopy )
          $oLayer = ms_newLayerObj($oMap, $oLayer);
        if ( $layer["layergroup"]!="" )
          $oLayer->set("group", $layer["layergroup"]);
        $oLayer->setMetadata("MAPFILE", $mapfile);
        $oLayer->set("transparency", $layer["transparency"]);
        $oLayer->set("status", ($layer["visible"]&&$layer["enable"] ? MS_ON : MS_OFF));
      }
    }
    
    return $oMap;
  }
  
  
  function readContext($mapfile, $context_file)
  {
    if (version_compare(PHP_VERSION, '5', '>='))
      include_once ('../sig_admin/lib/domxml-php4-to-php5.php');
    $tabMap = array();
    $tabLayer = array();
    $tabLayers = array();
    $tabFeatures = array();
    $tabExtent = array();
    $tabSize = array();
    
    $wms_order = "";
    $nonwms_order = "";
    
    $strFileContext = ALK_PATH_CONTEXTE.$context_file;
    $file = domxml_open_file($strFileContext);
    
     //suppression de l'attribut SRS
    $tabXmlBBox = $file->get_elements_by_tagname("BoundingBox");
    if ( !empty($tabXmlBBox) ){
      $oBBox = $tabXmlBBox[0];
      $oBBox->remove_attribute("SRS");
    }
         
    $tabXmlMap = $file->get_elements_by_tagname("ALK_MAPFILE");
    $bAlkContext = false;
    if ( !empty($tabXmlMap) ){
      $bAlkContext = true;
      $oMapfile = $tabXmlMap[0];
      $node_mapfile = $oMapfile->get_elements_by_tagname("ALK_mapfile");
      if ( !empty($node_mapfile) ){
        $node_mapfile = $node_mapfile[0];
        $_mapfile = $node_mapfile->get_content();
        if ( $_mapfile!="" )
          $mapfile = $_mapfile;
      } 
      $node_extent = $oMapfile->get_elements_by_tagname("ALK_extent");
      if ( !empty($node_extent) ){
        $node_extent = $node_extent[0];
        $childs = $node_extent->children();
        foreach ($childs as $child){
          $tabExtent[str_replace("ALK_", "", $child->node_name())] = $child->get_content();
        }
      }
      $node_size = $oMapfile->get_elements_by_tagname("ALK_size");
      if ( !empty($node_size) ){
        $node_size = $node_size[0];
        $childs = $node_size->children();
        foreach ($childs as $child){
          $tabSize[str_replace("ALK_", "", $child->node_name())] = $child->get_content();
        }
      }
    } 
    $file->dump_file($strFileContext, false, true);        
    
    $oMap = AlkMapScript::getMapObject(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES.$mapfile, $tabLayers, $tabExtent, $tabSize);
    $oMap->LoadMapContext($strFileContext);
    
    if ( $bAlkContext ){
      $tabWmsLayer = array();
      for($i=0; $i<$oMap->numlayers; $i++){
        $oLayer = $oMap->getLayer($i);
        $parse = parse_url($oLayer->connection);
        $strIndex = preg_replace("!\W!", "_", $parse["host"].$parse["path"]."_".$oLayer->getMetadata("wms_title"));
        $tabWmsLayer[$strIndex] = $i;
        $oLayer->setMetadata("INTERNAL_NAME", $strIndex);
        $oLayer->set("name", $oLayer->getMetadata("wms_title"));
      }

      //lecture des métadonnées de la carte      
      $map_metadata = $oMapfile->get_elements_by_tagname("ALK_metadata");
      $tabMetadata = AlkMapScript::readXMLNode($map_metadata[0]);
      $tabMetadata = $tabMetadata["metadata"];
      foreach($tabMetadata as $key=>$value){
        $oMap->setMetadata($key, $value);
      }
      // lecture des layers
      $tabLayers = array();
      $layers = $oMapfile->get_elements_by_tagname("ALK_layer");
      //$layers = array_reverse($layers, true);
      foreach($layers as $iLayer=>$oLayer){
        $tabLayers[$iLayer] = AlkMapScript::readXMLNode($oLayer);
      }
      
      $tabDesc = array(//index=>defaultValue
        "connectiontype"  => -1,
        "connection"      => "",
        "name"            => "",
        "guid"            => "",
        "type"            => "0",
        "layergroup"      => "",
        "mapgroup"        => "",
        "mapfile"         => "",
        "transparency"    => "100",
        "metadata"        => array(),
        "features"        => array(),
        "visible"         => "1",
        "enable"          => "1",
      );
      
      $tabOrder = array();
      $tabOrder2 = array();
      foreach($tabLayers as $iLayer=>$tabLayer){
        $connectiontype  = -1;
        $connection      = "";
        $name            = "";
        $guid            = "";
        $type            = "0";
        $layergroup      = "";
        $mapgroup        = "";
        $mapfile         = "";
        $transparency    = "100";
        $metadata        = array();
        $features        = array();
        $visible         = "1";
        $enable          = "1";
        
        $tabLayer = $tabLayer["layer"];
        foreach($tabDesc as $key=>$defaultValue){
          // $$ : définition dynamique par lecture de tabDesc
          if ( array_key_exists($key, $tabLayer) ){
            $$key = $tabLayer[$key];
          }
          else
            $$key = $defaultValue;
        }
        $oLayerObj = null;
        $strIndex = "";
        if ( $connection!="" ){
          $parse = parse_url($connection);
          $strIndex = preg_replace("!\W!", "_", $parse["host"].$parse["path"]."_".$name);
        }
        if ( $connectiontype==MS_WMS && array_key_exists($strIndex, $tabWmsLayer) ){
          $tabOrder[$iLayer] = $tabWmsLayer[$strIndex];
          $oLayerObj = $oMap->getLayer($tabWmsLayer[$strIndex]);
        }
        else {
          $tabOrder[$iLayer] = $oMap->numlayers;
          if ( !empty($features) ){
            $oLayerObj = AlkMapScript::setFeature($oMap, $name, $features);
          }
          else {
            $oLayerToAdd = AlkMapScript::getLayerObject(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES.$mapfile, $guid);
            if ( !is_null($oLayerToAdd) ){
              $oLayerObj = ms_newLayerObj($oMap, $oLayerToAdd);
            } 
          }
        }
        if ( !is_null($oLayerObj) ){
          foreach( $metadata as $key=>$value ){
            $oLayerObj->setMetadata($key, $value);
          }
          $oLayerObj->setMetadata("IS_VISIBLE", $visible);
          $oLayerObj->setMetadata("IS_ENABLE", $enable);
          $oLayerObj->setMetadata("INTERNAL_NAME", $guid);
          $oLayerObj->set("group", $layergroup);
          $oLayerObj->set("transparency", $transparency);
        }
      }
    }
    $oMap->setlayersdrawingorder($tabOrder);
    
    $mapfile = ALK_PATH_CONTEXTE. str_replace(".xml", ".map", $context_file ); 
    $oMap->save($mapfile);
    return $mapfile;
  }
  
  function readXMLNode($oNode)
  {
    $key = str_replace("ALK_", "", $oNode->node_name());
    $tabXML = array();
    if ( $oNode->has_child_nodes() ){
      $tabXML[$key] = array();
      $childs = $oNode->children(); 
      foreach ($childs as $child){
        if ( $child->node_name()=="#text" )
          $tabXML[$key] = utf8_decode($child->get_content());
        else
          $tabXML[$key] = array_merge($tabXML[$key], AlkMapScript::readXMLNode($child));
      }
    }
    else if ( $oNode->node_name()!="#text" ){
      $tabXML[$key] = utf8_decode($oNode->get_content());
    }
    else {
      $tabXML = utf8_decode($oNode->get_content());
    }
    return $tabXML;
  }
  
  function RequestMap($reqMethod)
  {
    function TransformFeature($features)
    {
      if ($features=="") return array();
      $tabDico = array(
          "t"=>"type", 
          "n"=>"numpoints", 
          "f"=>array("fillcolor", array("red", "green", "blue")), 
          "b"=>array("bordercolor", array("red", "green", "blue")),
          "e"=>array("extent", array("minx", "miny", "maxx", "maxy")),
          "s"=>array("size", array("width", "height")), 
          "p"=>array("x", "y", "text"));
      $newFeatures = array(); 
      foreach ($features as $index=>$text){
        $tabParts = explode("||", $text);
        foreach ($tabParts as $part){
          $args = explode(":", $part);
          $indice = array();
          if ( !preg_match("!^p(\d+)!", $args[0], $indice) ){
            $alias = $tabDico[$args[0]];
            if ( !is_array($alias) )
              $newFeatures[$index][$alias] = $args[1];
            else {
              $values = explode("_", $args[1]);
              $newFeatures[$index][$alias[0]] = array();
              foreach($alias[1] as $i=>$key){
                $newFeatures[$index][$alias[0]][$key] = $values[$i];
              }
            }
          }
          else {
            $indice = $indice[1];
            $values = explode("_", $args[1]);
            $alias = $tabDico["p"];
            foreach($alias as $i=>$key){
              $newFeatures[$index][$key.$indice] = $values[$i];
            }
          }
        }
      }
      return $newFeatures;
    }
    
    function TransformQuery($query)
    {
      if ($query=="") return array();
      $tabDico = array(
          "t"=>"type", 
          "pj"=>"mapProjection", 
          "s"=>"selectionType", 
          "m"=>"mapSelection", 
          "l"=>"layerSelection", 
          "lb"=>"layerReference", 
          "i"=>"queryItem", 
          "r"=>"queryRequest", 
          "rad"=>"radius", 
          "rb"=>"queryRequestB", 
          "c"=>"querycolored", 
          "n"=>"numpoints", 
          "e"=>array("extentQuery", array("minx", "miny", "maxx", "maxy")), 
          "p"=>array("x", "y"),);
      
      $newQuery = array(); 
      $tabParts = explode("__", $query);
      foreach ($tabParts as $part){
        $args = explode(":", $part);
        
        $indice = array();
        if ( !preg_match("!^p(\d+)!", $args[0], $indice) ){
          $alias = $tabDico[$args[0]];
          if ( !is_array($alias) ){
            $newQuery[$alias] = $args[1];
          }
          else {
            $values = explode("_", $args[1]);
            $newQuery[$alias[0]] = array();
            foreach($alias[1] as $i=>$key){
               $newQuery[$alias[0]][$key] = $values[$i];
            }
          }
        }
        else {
          $indice = $indice[1];
          $values = explode("_", $args[1]);
          $alias = $tabDico["p"];
          foreach($alias as $i=>$key){
            $newQuery[$key.$indice] = $values[$i];
          }
        }
      }
      return $newQuery;
    }
    
    function TransformToArray($value, $tabDico)
    {
      $tabRes = array();
      $values = explode("_", $value);
      foreach($tabDico as $index=>$key){
        if (array_key_exists($index, $values))
          $tabRes[$key] = $values[$index];
      }
      return $tabRes;
    }
    
    $type          = Request("T", $reqMethod, TYPE_MAP, "is_numeric");
    $map           = Request("M", $reqMethod, "");
    $map_guid      = Request("MG", $reqMethod, "");
    $dirmap        = Request("D", $reqMethod, ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES);
    $layer         = Request("L", $reqMethod, "");
    $layers        = Request("LS", $reqMethod, array());
    $extent        = Request("E", $reqMethod, "");
    $size          = Request("S", $reqMethod, "");
    $processRaster = Request("PR", $reqMethod, "0");
    $features      = Request("F", $reqMethod, array());
    $tabQuery      = Request("Q", $reqMethod, "");
    $mapProjection    = Request("pj", $reqMethod, "");
    $layerquery    = Request("lq", $reqMethod, "");
    $tabQuery=stripslashes($tabQuery);
    $tabQuery=str_replace("|", "\'", $tabQuery);
    $tabQuery=str_replace("ø", ".", $tabQuery);
    $extent   = TransformToArray($extent, array("minx", "miny", "maxx", "maxy"));
    $size     = TransformToArray($size, array("width", "height"));
    $features = TransformFeature($features);
    $tabQuery = TransformQuery($tabQuery);
    
    
    $tabReq = array("type"=>$type, "map"=>$map, "doc_id"=>$map_guid, "dirmap"=>$dirmap, "layer"=>$layer, "layers"=>$layers, 
                    "extent"=>$extent, "features"=>$features, "size"=>$size, "processRaster"=>$processRaster,
                    "tabquery"=>$tabQuery, "mapProjection"=>$mapProjection, "layerquery" => $layerquery);
    
    switch ( $reqMethod ){
      case REQ_POST :
        foreach($tabReq as $key=>$value){
          $_POST[$key] = $value;
        }  
      break;
      case REQ_GET :
      default : 
        foreach($tabReq as $key=>$value){
          $_GET[$key] = $value;
        } 
      break;
    }
  }
  
  function setProjLib($mapfile){
    if ( file_exists($mapfile) ){
      $contents = file_get_contents($mapfile);
      $contents = preg_replace("!CONFIG PROJ_LIB .+$!", "CONFIG PROJ_LIB \"".ALK_SIG_PROJECTION_CONFIG_PATH."\"", $contents);
      $fwrite = fopen($mapfile, "w");
      fwrite($fwrite, $contents);
      fclose($fwrite);
    }
  }
  
  
  
    function getImageFromCluster($mapfile, $layer, $layerName, $tabSize = array("width"=>800,"height"=>800), $strParam="", $bSpirale=false)
  {
     $strSuffixe= time().mt_rand(1000,9999);
     $strTable = "alkarto.temp".$strSuffixe;
     $strSqlCreate = "CREATE TABLE ".$strTable." AS  SELECT * FROM alkarto.".$layer.($strParam != "" ? " where ".$strParam: "");
     AlkFactory::getDbConn()->ExecuteSql( $strSqlCreate );
     $tabTemp = array();
     $tabFinal = array();
     $tabGid = array();
     $tabFinalGid = array();
     $tabUpdate = array();
     
     $strSql = "select gid, astext(the_geom) from  ".$strTable;
     $DsTemp =  AlkFactory::getDbConn()->initDataset( $strSql );
     
     while ( $oDrTemp = $DsTemp->getRowIter() ){
      array_push($tabTemp,array($oDrTemp->getValueName("gid"),$oDrTemp->getValueName("astext"), 0));  
     }
    
    //ajouter condition existance
    
    
    for($i=0;$i<count($tabTemp);$i++){
      
      if($tabTemp[$i][2] != 1){
              $tabTemp[$i][2] = 1;
              array_push($tabGid, $tabTemp[$i][0]); 
              for($j=0;$j<count($tabTemp);$j++){
                      if($tabTemp[$j][2] != 1){
                              $strSqlDistance = "SELECT Distance((select astext(the_geom) from ".$strTable." where gid=".$tabTemp[$i][0]."), (select astext(the_geom) from ".$strTable." where gid=".$tabTemp[$j][0]."));";
                              $DsTempDistance =  AlkFactory::getDbConn()->initDataset( $strSqlDistance );
                              while ( $DrTempDistance = $DsTempDistance->getRowIter() ){
                                $intDistance = $DrTempDistance->getValueName("distance");
                              }
                              
                              if($intDistance < 10000){
                                   
                                  $tabTemp[$j][2] = 1;
                                  array_push($tabGid, $tabTemp[$j][0]);    
                              
                              }
                      }
                      
              }
              
              $tabFinal[$tabTemp[$i][1]] = $tabGid;
              $tabGid = array();
      }
    
    } 
    $tabResult = array();
    
    
    
    foreach ($tabFinal as $geom=>$tabGid) {
          $geom = str_replace(")", "", $geom);
          $geom = str_replace("(", "", $geom);
          $geom = str_replace("POINT", "", $geom);
          $tabTempSplit = split(" ", $geom);
          array_push($tabFinalGid, array($tabTempSplit[0], $tabTempSplit[1], $tabGid));
    } 
    
    
  if($bSpirale){  
    // Le centre des cercles inscrit est 
   
   $d = 8000-500;
   for ($i = 0; $i < count($tabFinalGid); $i++) {
        $j= 0;
        $teta=2*pi()/count($tabFinalGid[$i][2]);
        $x0= $tabFinalGid[$i][0];
        $y0= $tabFinalGid[$i][1];
        while ($j < count($tabFinalGid[$i][2])) {
          $tabUpdate[$tabFinalGid[$i][2][$j]] =  array($x0 + $d*cos(($j+1)*$teta) ,  $y0 + $d*sin(($j+1)*$teta)) ;
          $j++;
        }  
   }
   //print_r($tabUpdate);die();
//avec x0,y0 le centre du cercle externe, et i entier entre 1 et NmbDeCercles 
  }else{    
    for ($i = 0; $i < count($tabFinalGid); $i++) {
              
                            $delta = 5000;
                            $direction = "est";
                            $j = 0;
                            
                            $nbest = 2;
                            $nbsud = 2;
                            $nbwest = 1;
                            $nbnord = 1;
                            $isInBoucle = true;
                            $nbdirection = 0;
                            $isecrire = true;
                            $direction = "est";
                    
                        while ($j < count($tabFinalGid[$i][2])) {
                          if($j !=0){
                                $isInBoucle = true;
                                $isecrire = true;
                                
                                if (($direction =="est") && ($isInBoucle)) {
                                    $tabUpdate[$tabFinalGid[$i][2][$j]] =  array($tabFinalGid[$i][0] + $delta, $tabFinalGid[$i][1]) ;
                                    $tabFinalGid[$i][0] = $tabFinalGid[$i][0] + $delta;
                                    $direction = "sud";
                                    $isInBoucle = false;
                                }
                                if (($direction == "sud") && ($isInBoucle)) {
                                    $tabUpdate[$tabFinalGid[$i][2][$j]] =  array($tabFinalGid[$i][0] , $tabFinalGid[$i][1] - $delta) ;
                                    $tabFinalGid[$i][1] = $tabFinalGid[$i][1] - $delta;
                                    $direction = "west";
                                    $isInBoucle = false;

                                }
                                if (($direction == "west" ) && ($isInBoucle)) {

                                    if ($nbdirection > $nbwest) {
                                        $direction = "nord";
                                        $nbdirection = 0;
                                        $isecrire = false;
                                        $j--;
                                        $nbwest += 2;

                                    } else {
                                        $direction = "west";
                                        $tabUpdate[$tabFinalGid[$i][2][$j]] =  array($tabFinalGid[$i][0] - $delta, $tabFinalGid[$i][1]) ;
                                        $tabFinalGid[$i][0] = $tabFinalGid[$i][0] - $delta;
                                        $nbdirection++;
                                    }
                                    $isInBoucle = false;
                                }
                                if (($direction == "nord") && ($isInBoucle)) {
                                    if ($nbdirection > $nbnord) {
                                        $direction = "est2";
                                        $nbdirection = 0;
                                        $isecrire = false;
                                        $j--;
                                        $nbnord += 2;
                                    } else {
                                        $tabUpdate[$tabFinalGid[$i][2][$j]] =  array($tabFinalGid[$i][0], $tabFinalGid[$i][1] + $delta) ;
                                        $tabFinalGid[$i][1] = $tabFinalGid[$i][1] + $delta;
                                        $direction = "nord";
                                        $nbdirection++;
                                    }
                                    $isInBoucle = false;
                                }
                                if (($direction == "est2") & ($isInBoucle)) {
                                    if ($nbdirection > $nbest) {
                                        $direction =  "sud2";
                                        $nbdirection = 0;
                                        $isecrire = false;
                                        $j--;
                                        $nbest += 2;
                                    } else {
                                        $tabUpdate[$tabFinalGid[$i][2][$j]] =  array($tabFinalGid[$i][0] + $delta, $tabFinalGid[$i][1]) ;
                                        $tabFinalGid[$i][0] = $tabFinalGid[$i][0] + $delta;
                                        $direction = "est2";
                                        $nbdirection++;
                                    }
                                    $isInBoucle = false;

                                }
                                if (($direction == "sud2") && ($isInBoucle)) {
                                    if ($nbdirection > $nbsud) {
                                        $direction = "west";
                                        $nbdirection = 0;
                                        $isecrire = false;
                                        $nbsud += 2;
                                        $j--;
                                    } else {
                                        $tabUpdate[$tabFinalGid[$i][2][$j]] =  array($tabFinalGid[$i][0], $tabFinalGid[$i][1] - $delta) ;
                                        $tabFinalGid[$i][1] = $tabFinalGid[$i][1] - $delta;
                                        $direction = "sud2";
                                        $nbdirection++;
                                    }
                                    $isInBoucle = false;

                                }
                                
                               
                          }else{
                            
                             $tabUpdate[$tabFinalGid[$i][2][$j]] =  array($tabFinalGid[$i][0], $tabFinalGid[$i][1]) ;
                          }                         
               $j++;
       }
    
    
      }
      
   }  
    
   foreach ($tabUpdate as $geomKey=>$tabGidValue) {
     $strUpdate = "update ".$strTable." set the_geom = GeomFromText('POINT(".$tabGidValue[0]." ".$tabGidValue[1].")', 27572) where gid=".$geomKey;
     AlkFactory::getDbConn()->setSchema("public");
     AlkFactory::getDbConn()->ExecuteSql($strUpdate);
   }
   
   $oCarte = new AlkMapScript();
    $oMap = ms_newMapObj(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES.$mapfile);
    $oMap->selectOutputFormat("png");
    $oOutputFormat =& $oMap->outputformat;
    $oOutputFormat->set("name", "png");
    $oOutputFormat->set("mimetype", "image/png");
    $oOutputFormat->set("driver", "png");
    
    
    
    $oMapSVG = ms_newMapObj(ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_MAPFILES.$mapfile);
    $oMapSVG->selectOutputFormat("svg");
    $oOutputFormatSVG =& $oMapSVG->outputformat;
    $oOutputFormatSVG->set("name", "svg");
    $oOutputFormatSVG->set("mimetype", "image/svg+xml");
    $oOutputFormatSVG->set("driver", "svg");
    
    
    
    for($i=0; $i<$oMap->numlayers; $i++){
      $oLayer = $oMap->getLayer($i);
      if($oLayer->name == $layerName){
        $oLayer->set("status", MS_OFF); 
      }
      $oLayer->setFilter("");
    }
    
    for($j=0; $j<$oMapSVG->numlayers; $j++){
      $oLayerSVG = $oMapSVG->getLayer($j);
      if($oLayerSVG->name != $layerName){
        $oLayerSVG->set("status", MS_OFF); 
      }else{
        $oLayerSVG->set("data", "the_geom from ".$strTable." as foo using unique gid using SRID=27572");
        $oLayerSVG->set("status", MS_ON);  
      }
      $oLayerSVG->setFilter("");
    } 
    
    AlkMapScript::setDimension($oMap, null, $tabSize);
    $img = $oMap->draw();
    $strImgFDP = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_IMGMS.rand(0, 1000000).str_replace("/", "_", substr(basename($mapfile), 0, -4)).".png";
    $img->saveImage($strImgFDP);
    
    AlkMapScript::setDimension($oMapSVG, null, $tabSize);
    $imgSVG = $oMapSVG->draw();
    $strImgSVG = ALK_ALKANET_ROOT_PATH.ALK_SIG_DIR_IMGMS.rand(0, 1000000).str_replace("/", "_", substr(basename($mapfile), 0, -4));
    $imgSVG->saveImage($strImgSVG.".svg");
    exec("convert -background none ".$strImgSVG.".svg ".$strImgSVG.".png");
    exec("convert -background none ".$strImgFDP."  ".$strImgSVG.".png  -flatten ".$strImgFDP);
    
    
    
   $strSqlDrop = "drop TABLE ".$strTable;
   AlkFactory::getDbConn()->ExecuteSql( $strSqlDrop );
   return $strImgFDP;  
  }
  
  function getLegend($oMap) {
  /*$oMap->set("transparent", MS_ON);
          print_r($oMap->outputformat);
   $oMap->outputformat->set('imagemode', MS_IMAGEMODE_RGBA);
   $oMap->outputformat->set("name", "png");
   $oMap->outputformat->set('transparent', MS_ON);
   $oMap->outputformat->set("mimetype", "image/png;");
   $oMap->outputformat->set("driver", "AGG/PNG");
   $oMap->outputformat->set("extension", "png");
   print_r($oMap->outputformat);
   echo("selectoutput ".$oMap->selectOutputFormat("png")); 
   $legend_img=$oMap->drawLegend();
   $legend_url=$legend_img->saveWebImage();
   return ($legend_img);*/
   $legend_img=$oMap->drawLegend();
   $legend_url=$legend_img->saveWebImage();
   return ($legend_url);
  }
  
}
?>