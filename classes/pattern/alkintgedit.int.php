<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


/**
 * @package Alkanet_Class_Pattern
 * 
 * @interface AlkIntGEdit
 * @brief Interface liée aux applications fournissant les méthodes à implémenter
 *        pour la gestion courante : ajout / modif / suppr / déplacement / duplication
 * @note La gestion des droits est assurée par l'interface AlkIntRights
 */
interface AlkIntGEdit
{


  /**
   * Méthode appelée à l'ajout d'un bloc gedit
   *        Implémenter les actions effectuant l'ajout de ce bloc sur ce type d'application
   * @param bloc_id     identifiant du bloc
   * @param typeIdBloc  type de bloc 
   * @param atypeIdBloc type de l'application associé au type de bloc
   */
  public function addGEditBloc($bloc_id, $typeIdBloc, $atypeIdBloc);
  
  /**
   * Méthode appelée avant suppression définitive d'une application de type gedit
   *        ou avant suppression d'une page gedit
   *        ou avant suppression d'un bloc gedit
   *        Implémenter les actions effectuant le ménage interne à l'application
   * @param page_id     identifiant de la page
   * @param bloc_id     identifiant du bloc
   * @param typeIdBloc  type de bloc 
   * @param atypeIdBloc type de l'application associé au type de bloc
   */
  public function delGEditBloc($page_id, $bloc_id, $typeIdBloc, $atypeIdBloc);

  /**
   * Recopie les associations d'un bloc à un autre bloc
   * 
   * @param bloc_id_src  identifiant du bloc source
   * @param bloc_id_dest identifiant du bloc destination
   */
  public function dupGEditBloc($bloc_id_src, $bloc_id_dest);

  /**
   * Retourne le contenu raccourci d'un bloc éditorial
   * pour un affichage de gestion ou d'aperçu
   * @apram bloc_id        Identifiant du bloc
   * @param bloc_typeassoc type d'association : 0=aucune, 1=appli, 2=cat, 3=data
   *                       si bit 2 on =4 : filtre publication : news uniquement
   *                       si bit 3 on =8 : filtre publication : infos publiés passés (archivés)
   *                       si bit 4 on =16: filtre publication : infos publiés présents
   *                       si bit 5 on =32: filtre publication : infos non publiés
   *                       bit 6 au bit 12 : anciennes versions de tris prédéfinis 
   *                       si bit 13 on : ordre : champ nouveauté
   *                       si bit 14 on : ordre : champ catégorie
   *                       si bit 15 on : ordre : champ data
   *                       si bit 16 on : ordre : champ date pub décroissant
   *                       si bit 17 on : ordre : champ date pub croissant
   *                       si bit 18 on : ordre : champ date info décroissant
   *                       si bit 19 on : ordre : champ date info croissant
   *                       si bit 20 on : ordre : champ date maj décroissant
   *                       si bit 21 on : ordre : champ date maj croissant
   *                       si bit 22 on : filtre calendaire : date début-fin pub 
   *                       si bit 23 on : filtre calendaire : date début-fin info 
   *                       si bit 24 on : filtre calendaire : date début-fin màj
   *                       si bit 25 on : filtre publication : infos synf 
   *                       si bit 26 on : filtre publication : en cours de validation
   *                       si bit 27 on : filtre publication : 30 derniers jours date pub
   *                       si bit 28 on : filtre publication : 30 derniers jours date info
   *                       si bit 29 on : filtre publication : 30 derniers jours date màj
   *                       si bit 30 on : ordre : champ rang des données décroissant
   *                       si bit 31 on : ordre : champ rang des données croissant
   * @param atypeIdBloc    type de l'application liée, =-1 si non utilisé
   * @param bloc_ordre     liste de nombres séparés par une virgule. 1 nombre correspond à une puissance de deux. 
   *                       cette liste correspond à l'ordre des champs d'après les champs de bit de bloc_typeassoc
   * @param bloc_datedeb   Contient la date de début pour un filtre éventuel en fonction de la valeur de bloc_typeassoc
   * @param bloc_datefin   Contient la date de début pour un filtre éventuel en fonction de la valeur de bloc_typeassoc 
   * @return array
   */
  public function getGEditBlocShortContents($bloc_id, $bloc_typeassoc, $atypeIdBloc=-1, $bloc_ordre="", $bloc_datedeb="", $bloc_datefin="");

  /**
   * Retourne le contenu complet d'un bloc éditorial dans la langue sélectionnée
   * pour un affichage de consultation
   * @param bloc_id   Identifiant du bloc
   * @param lg        clé de la langue sélectionnée
   * @param bloc_typeassoc type d'association : 0=aucune, 1=appli, 2=cat, 3=data
   *                       si bit 2 on =4 : filtre publication : news uniquement
   *                       si bit 3 on =8 : filtre publication : infos publiés passés (archivés)
   *                       si bit 4 on =16: filtre publication : infos publiés présents
   *                       si bit 5 on =32: filtre publication : infos non publiés
   *                       bit 6 au bit 12 : anciennes versions de tris prédéfinis 
   *                       si bit 13 on : ordre : champ nouveauté
   *                       si bit 14 on : ordre : champ catégorie
   *                       si bit 15 on : ordre : champ data
   *                       si bit 16 on : ordre : champ date pub décroissant
   *                       si bit 17 on : ordre : champ date pub croissant
   *                       si bit 18 on : ordre : champ date info décroissant
   *                       si bit 19 on : ordre : champ date info croissant
   *                       si bit 20 on : ordre : champ date maj décroissant
   *                       si bit 21 on : ordre : champ date maj croissant
   *                       si bit 22 on : filtre calendaire : date début-fin pub 
   *                       si bit 23 on : filtre calendaire : date début-fin info 
   *                       si bit 24 on : filtre calendaire : date début-fin màj
   *                       si bit 25 on : filtre publication : infos synf 
   *                       si bit 26 on : filtre publication : en cours de validation
   *                       si bit 27 on : filtre publication : 30 derniers jours date pub
   *                       si bit 28 on : filtre publication : 30 derniers jours date info
   *                       si bit 29 on : filtre publication : 30 derniers jours date màj
   *                       si bit 30 on : ordre : champ rang des données décroissant
   *                       si bit 31 on : ordre : champ rang des données croissant
   * @param atypeIdBloc    type de l'application liée, =-1 si non utilisé
   * @param bloc_ordre     liste de nombres séparés par une virgule. 1 nombre correspond à une puissance de deux. 
   *                       cette liste correspond à l'ordre des champs d'après les champs de bit de bloc_typeassoc
   * @param bloc_datedeb   Contient la date de début pour un filtre éventuel en fonction de la valeur de bloc_typeassoc
   * @param bloc_datefin   Contient la date de début pour un filtre éventuel en fonction de la valeur de bloc_typeassoc 
   * @return array
   */
  public function getGEditBlocContents($bloc_id, $lg, $bloc_typeassoc, $atypeIdBloc=-1, $bloc_ordre="", $bloc_datedeb="", $bloc_datefin="");

  /**
   * Retourne le type de méthode pour mettre à jour les blocs
   *        Retourne un tableau en fonction du type sélectionné.
   * @param typeIdBloc   type de bloc
   * @param atypeIdBloc  type de l'application associée au type de bloc
   * @return array
   */
  public function getGEditBlocTypeUpdate($typeIdBloc, $atypeIdBloc);

  /**
   * Associe une catégorie appartenant à l'application de type atypeIdBloc 
   * et d'identifiant appliIdBloc au bloc
   * @param bloc_id     Identifiant du bloc
   * @param cat_id      Identifiant de la catégorie
   * @param atypeIdBloc type de l'application contenant la catégorie, =-1 par défaut si non utile
   * @param appliIdBloc Identifiant de l'application contenant la catégorie, =-1 par défaut si non utile
   */
  public function assocCatToBloc($bloc_id, $cat_id, $atypeIdBloc=-1, $appliIdBloc=-1);

  /**
   * Associe un ensemble de données appartenant à l'application de type atypeIdBloc 
   * et d'identifiant appliIdBloc au bloc
   * @param bloc_id     Identifiant du bloc
   * @param tabDataId   Tableau contenant les identifiants des données à associer
   * @param atypeIdBloc type de l'application contenant la catégorie, =-1 par défaut si non utile
   * @param appliIdBloc Identifiant de l'application contenant la catégorie, =-1 par défaut si non utile
   */
  public function assocDataToBloc($bloc_id, $tabDataId, $atypeIdBloc=-1, $appliIdBloc=-1);

  /**
   * Supprime une association entre une catégorie appartenant à l'application de type atypeIdBloc 
   * et d'identifiant appliIdBloc et le bloc
   * @param bloc_id     Identifiant du bloc
   * @param cat_id      Identifiant de la catégorie, =-1 pour supprimer toutes les catégories du bloc
   * @param atypeIdBloc type de l'application contenant la catégorie, =-1 par défaut si non utile
   * @param appliIdBloc Identifiant de l'application contenant la catégorie, =-1 par défaut si non utile
   */
  public function removeCatFromBloc($bloc_id, $cat_id, $atypeIdBloc=-1, $appliIdBloc=-1);
  
  /**
   * Supprime une association entre une donnée appartenant à l'application de type atypeIdBloc 
   * et d'identifiant appliIdBloc et le bloc
   * @param bloc_id     Identifiant du bloc
   * @param data_id     Identifiant de la données à associer, =-1 pour supprimer toutes les données du bloc
   * @param atypeIdBloc type de l'application contenant la catégorie, =-1 par défaut si non utile
   * @param appliIdBloc Identifiant de l'application contenant la catégorie, =-1 par défaut si non utile
   */
  public function removeDataFromBloc($bloc_id, $data_id, $atypeIdBloc=-1, $appliIdBloc=-1);
  
  /**
   * Met à jour le rang d'une donnée dans un bloc
   * @param bloc_id     Identifiant du bloc
   * @param data_id     Identifiant de la donnée
   * @param iRank       Rang de la donnée courante
   * @param iDelta      Entier : 1 pour descendre d'un rang, -1 pour monter d'un rang
   * @param atypeIdBloc Type de l'application contenant la catégorie, =-1 par défaut si non utile
   * @param appliIdBloc Identifiant de l'application contenant la catégorie, =-1 par défaut si non utile
   */
  public function updateDataRankInBloc($bloc_id, $data_id, $iRank, $iDelta, $atypeIdBloc=-1, $appliIdBloc=-1);
   
  /**
   * Retourne un tableau contenant l'information sélectionnée par cette méthode (appel régulier de getDataContents())
   * @param typeAff         type d'affichage, permet d'établir une conditionnelle dans cette méthode pour gérer plusieurs type d'affichage
   * @param cont_id         Identifiant de l'espace, pris en compte si appli_id vaut -1
   * @param appli_id        Identifiant de l'application, peut valoir =-1 pour toutes les applis de cont_id
   * @param cat_id          Identifiant de la catégorie, peut valoir =-1 pour toutes les infos de appli_id
   * @param data_id         Identifiant de la données, différent de -1 pour afficher la fiche, =-1 sinon par défaut
   * @param lg              langue utilisé, _FR par défaut
   * @param page_typeassoc  Type d'association des données à la page
   * @param page_ordre      Ordre des données dans la page
   * @param page_datedeb    Contient la date de début pour un filtre éventuel en fonction de la valeur de page_typeassoc
   * @param page_datefin    Contient la date de début pour un filtre éventuel en fonction de la valeur de page_typeassoc 
   * @return array
   */
  public function getGEditDataContents($typeAff, $cont_id, $atype_id, $appli_id, $cat_id, $data_id="-1", $lg="fr", $page_typeassoc=0, $page_ordre="", $page_datedeb="", $page_datefin=""); 
  
}

?>