<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkRequest
 * @brief Classe se chargeant de récupérer les éléments postés en GET ou en POST
 */
final class AlkRequest extends AlkObject
{
  private static $tabToken = array();

  /**
   *  Constructeur non accessible
   */
   public function __construct() { }

  /**
   *  Récupère puis retourne la valeur d'un paramètre passé en GET
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @param strFunctionTestType  Nom de la fonction de test, optionnel
   * @return string
   */
  public static function _GET($strParamName, $strDefaultValue="", $strFunctionTestType="")
  {
    $strParamValue = ( isset($_GET[$strParamName]) 
                       ? $_GET[$strParamName]
                       : $strDefaultValue );
    $bTest = true;
    if( $strFunctionTestType != "" ) {
      eval("\$bTest = $strFunctionTestType(\$strParamValue);");
    }
    return ( $bTest ? $strParamValue : $strDefaultValue );
  }

  /**
   *  Récupère puis retourne la valeur d'un paramètre passé en POST
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @param strFunctionTestType  Nom de la fonction de test, optionnel
   * @return string
   */
  public static function _POST($strParamName, $strDefaultValue="", $strFunctionTestType="")
  {
    $strParamValue = ( isset($_POST[$strParamName])
                       ? $_POST[$strParamName]
                       : $strDefaultValue );
    $bTest = true;
    if( $strFunctionTestType != "" ) {
      eval("\$bTest = $strFunctionTestType(\$strParamValue);");
    }
    return ( $bTest ? $strParamValue : $strDefaultValue );
  }

  /**
   *  Récupère puis retourne la valeur d'un paramètre passé en GET ou en POST
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @param strFunctionTestType  Nom de la fonction de test, optionnel
   * @return string
   */
  public static function _REQUEST($strParamName, $strDefaultValue="", $strFunctionTestType="")
  {
    $strParamValue = ( isset($_REQUEST[$strParamName])
                       ? $_REQUEST[$strParamName]
                       : $strDefaultValue );
    $bTest = true;
    if( $strFunctionTestType != "" ) {
      eval("\$bTest = $strFunctionTestType(\$strParamValue);");
    }
    return ( $bTest ? $strParamValue : $strDefaultValue );
  }

  /**
   *  Récupère puis retourne la valeur numérique d'un paramètre passé en GET
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return int
   */
  public static function _GETint($strParamName, $strDefaultValue=0)
  {
    return ( isset($_GET[$strParamName]) && is_numeric($_GET[$strParamName])
             ? $_GET[$strParamName]
             : $strDefaultValue );
  }

  /**
   *  Récupère puis retourne la valeur numérique d'un paramètre passé en POST
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return int
   */
  public static function _POSTint($strParamName, $strDefaultValue=0)
  {
    return ( isset($_POST[$strParamName]) && is_numeric($_POST[$strParamName])
             ? $_POST[$strParamName]
             : $strDefaultValue );
  }

  /**
   *  Récupère puis retourne la valeur numérique d'un paramètre passé en GET ou en POST
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return int
   */
  public static function  _REQUESTint($strParamName, $strDefaultValue=0)
  {
    return ( isset($_REQUEST[$strParamName]) && is_numeric($_REQUEST[$strParamName])
             ? $_REQUEST[$strParamName]
             : $strDefaultValue );
  }

  /**
   *  Récupère puis retourne la valeur date / time d'un paramètre passé en GET
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _GETdate($strParamName, $strDefaultValue="")
  {
    return ( isset($_GET[$strParamName]) && mb_ereg_match("\d", $_GET[$strParamName])
             ? $_GET[$strParamName]
             : $strDefaultValue );
  }

  /**
   *  Récupère puis retourne la valeur date / time d'un paramètre passé en GET
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _POSTdate($strParamName, $strDefaultValue="")
  {
    return ( isset($_POST[$strParamName]) && mb_ereg_match("\d", $_POST[$strParamName])
             ? $_POST[$strParamName]
             : $strDefaultValue );
  }

  /**
   *  Récupère puis retourne la valeur date / time d'un paramètre passé en GET
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _REQUESTdate($strParamName, $strDefaultValue="")
  {
    return ( isset($_REQUEST[$strParamName]) && mb_ereg_match("\d", $_REQUEST[$strParamName])
             ? $_REQUEST[$strParamName]
             : $strDefaultValue );
  }


  /**
   *  Récupère puis retourne la valeur d'une checkbox passé en GET
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _GETcheck($strParamName, $strDefaultValue="")
  {
    $strParamNameNot = "not_".$strParamName;
    return ( isset($_GET[$strParamName])
             ? $_GET[$strParamName]
             : (  isset($_GET[$strParamNameNot])
                  ? $_GET[$strParamNameNot]
                  : $strDefaultValue )
           );
  }

  /**
   *  Récupère puis retourne la valeur d'une checkbox passé en POST
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _POSTcheck($strParamName, $strDefaultValue="")
  {
    $strParamNameNot = "not_".$strParamName;
    return ( isset($_POST[$strParamName])
             ? $_POST[$strParamName]
             : (  isset($_POST[$strParamNameNot])
                  ? $_POST[$strParamNameNot]
                  : $strDefaultValue )
           );
  }

  /**
   *  Récupère puis retourne la valeur d'une checkbox passé en GET ou POST
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _REQUESTcheck($strParamName, $strDefaultValue="")
  {
    $strParamNameNot = "not_".$strParamName;
    return ( isset($_REQUEST[$strParamName])
             ? $_REQUEST[$strParamName]
             : (  isset($_REQUEST[$strParamNameNot])
                  ? $_REQUEST[$strParamNameNot]
                  : $strDefaultValue )
           );
  }

  /**
   * Récupère puis retourne la valeur d'un entier sachant qu'il est inclus dans tableau
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, 0 par défaut
   * @return int
   */
  public static function _GETarrayint($strParamName, $strIndex, $strDefaultValue=0)
  {
    $res = self::_array($_GET, $strParamName, $strIndex, $strDefaultValue);
    if ( is_numeric($res) )
      return $res;
    return $strDefaultValue;
  }
  /**
   * Récupère puis retourne la valeur d'un entier sachant qu'il est inclus dans tableau
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, 0 par défaut
   * @return int
   */
  public static function _POSTarrayint($strParamName, $strIndex, $strDefaultValue=0)
  {
    $res = self::_array($_POST, $strParamName, $strIndex, $strDefaultValue);
    if ( is_numeric($res) )
      return $res;
    return $strDefaultValue;
  }
  /**
   * Récupère puis retourne la valeur d'un entier sachant qu'il est inclus dans tableau
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, 0 par défaut
   * @return int
   */
  public static function _REQUESTarrayint($strParamName, $strIndex, $strDefaultValue=0)
  {
    $res = self::_array($_REQUEST, $strParamName, $strIndex, $strDefaultValue);
    if ( is_numeric($res) )
      return $res;
    return $strDefaultValue;
  }
  
  /**
   * Récupère puis retourne la valeur à un indice d'un résultat tableau
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _GETarrayvalue($strParamName, $strIndex, $strDefaultValue="")
  {
    return self::_array($_GET, $strParamName, $strIndex, $strDefaultValue);
  }
  /**
   * Récupère puis retourne la valeur à un indice d'un résultat tableau
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _POSTarrayvalue($strParamName, $strIndex, $strDefaultValue="")
  {
    return self::_array($_POST, $strParamName, $strIndex, $strDefaultValue);
  }
  /**
   * Récupère puis retourne la valeur à un indice d'un résultat tableau
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _REQUESTarrayvalue($strParamName, $strIndex, $strDefaultValue="")
  {
    return self::_array($_REQUEST, $strParamName, $strIndex, $strDefaultValue);
  }
  /**
   * Récupère puis retourne la valeur à un indice d'un résultat tableau sans test de type
   * @param tabValues            Tableau dans lequel s'effectue la recherche
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  protected static function _array($tabValues, $strParamName, $strIndex, $strDefaultValue="")
  {
    $tabRes = ( isset($tabValues[$strParamName])
                ? $tabValues[$strParamName]
                : $strDefaultValue
              );
    if ( !is_array($tabRes) )
      return $tabRes;
    if ( array_key_exists($strIndex, $tabRes) )
      return $tabRes[$strIndex];
    return $strDefaultValue;
  }
  

  /**
   * Récupère puis retourne la valeur d'une checkbox sachant qu'il est inclus dans tableau
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _GETarraycheck($strParamName, $strIndex, $strDefaultValue="")
  {
    return self::_arraycheck($_GET, $strParamName, $strIndex, $strDefaultValue);
  }

  /**
   * Récupère puis retourne la valeur d'une checkbox sachant qu'il est inclus dans tableau
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _POSTarraycheck($strParamName, $strIndex, $strDefaultValue="")
  {
    return self::_arraycheck($_POST, $strParamName, $strIndex, $strDefaultValue);
  }

  /**
   * Récupère puis retourne la valeur d'une checkbox sachant qu'il est inclus dans tableau
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  public static function _REQUESTarraycheck($strParamName, $strIndex, $strDefaultValue="")
  {
    return self::_arraycheck($_REQUEST, $strParamName, $strIndex, $strDefaultValue);
  }

  /**
   * Récupère puis retourne la valeur d'une checkbox sachant qu'il est inclus dans tableau
   * @param tabValues            Tableau dans lequel s'effectue la recherche
   * @param strParamName         Nom du paramètre
   * @param strIndex             Index du tableau
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @return string
   */
  protected static function _arraycheck($tabValues, $strParamName, $strIndex, $strDefaultValue="")
  {
    $strParamNameNot = "not_".$strParamName;
    $tabRes = ( isset($tabValues[$strParamName])
                ? $tabValues[$strParamName]
                : $strDefaultValue 
               );
    $tabResNot = ( isset($tabValues[$strParamNameNot])
                   ? $tabValues[$strParamNameNot]
                   : $strDefaultValue 
                 );
    if ( !is_array($tabRes) )
      return $tabRes;
    if ( array_key_exists($strIndex, $tabRes) )
      return $tabRes[$strIndex];
    if ( array_key_exists($strIndex, $tabResNot) )
      return $tabResNot[$strIndex];
    return $strDefaultValue;
  }
  /**
   *  Récupère puis retourne le tableau correspondant à un fichier uploadé
   *        Retourne un tableau vide si non trouvé
   * @param strParamName  Nom du paramètre
   * @return array
   */
  public static function _FILES($strParamName)
  {
    return ( isset($_FILES[$strParamName])
             ? $_FILES[$strParamName]
             : array() );
  }

  /**
   *  Encode puis retourne une chaine de caractères
   *        représentant un paramètre dans une URL http
   * 
   * @param strParam  Chaine de caractères à encoder
   * @return string
   */
  public static function getEncodeParam($strParam, $bCheckSum=true)
  {
    $strEncode = "";
    for($i=0; $i<strlen($strParam); $i++) {
      $strEncode .= dechex(ord(substr($strParam, $i, 1)));
    }
    /*if ( $bCheckSum && $strEncode!="" ){
      $iCheckSum = self::getCheckSumEncodage($strEncode);
      $strEncode .= self::getEncodeParam("&ALK_CHECKSUM=".$iCheckSum, false);
    }*/
    return $strEncode;
  }

  /**
   *  Récupère le paramètre http selon la méthode REQUEST
   *        Décode le param encodée par getEncodeParam() 
   *        Vérifie le format en fonction de $strDefaultValue et $strFunctionTestType
   *        Puis retourne la valeur du paramètre
   * 
   * @param strParamName         Nom du paramètre
   * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
   * @param strFunctionTestType  Nom de la fonction de test, optionnel
   * @return string
   */  
  public static function getDecodeParam($strParamName,  $strDefaultValue="", $strFunctionTestType="")
  {  
    // récupère le paramètre encodé
    $strParamValue = self::_REQUEST($strParamName);

    //décodage
    $strDecode = self::decodeValue($strParamValue);

    // vérifie le format du param décodé
    $bTest = true;
    if( $strFunctionTestType != "" ) {
      eval("\$bTest = $strFunctionTestType(\$strDecode);");
    }
    $strDecode = (($bTest && $strDecode!="") ? $strDecode : $strDefaultValue);

    return $strDecode;
  }

  /**
   * Décode la valeur passée en paramètre puis retourne le résultat
   * @param strValue valeur à décoder
   * @return string 
   */
  public static function decodeValue($strValue)
  {
    //décodage
    $strDecode = "";
    for($i=0; $i<strlen($strValue)-1; $i+=2 ) {
      $strDecode .= chr(hexdec(substr($strValue,  $i, 1).substr($strValue, $i+1, 1)));
    }
    return $strDecode; 
  }

  /**
   *  Lit et décode les paramètres encodés et regroupés par le paramètre token
   */
  public static function readToken()
  {
    $strToken = self::_REQUEST("token", "");
    $strParams = self::getDecodeParam("token");
    $iChecksumParam = 0;
    $tabParams = explode("&", $strParams);
    foreach($tabParams as $strParam) {
      $tabParam = explode("=", $strParam,2);
      if( count($tabParam)==2 ) {
        /*if ( $tabParam[0]=="ALK_CHECKSUM" ){
           $iChecksumParam = $tabParam[1];
           $strToken = str_replace(strtolower(self::getEncodeParam("&".$strParam, false)), "", strtolower($strToken));
        }
        else {*/
          self::$tabToken[$tabParam[0]] = $tabParam[1];
        //}
      }
    }
    //$iChecksum = self::getCheckSumEncodage($strToken);
    return true; //$iChecksum==$iChecksumParam;
  }

  /**
   *  Retourne la valeur d'un paramètre appartenant au token
   * @param strParam         Nom du paramètre
   * @param strDefaultValue  Valeur par défaut si non présent
   * @return string : la valeur du paramètre
   */
  public static function getToken($strParam, $strDefaultValue="")
  {
    return ( array_key_exists($strParam, self::$tabToken)
             ? self::$tabToken[$strParam]
             : $strDefaultValue );
  }

  /**
   * Retourne un checksum pour le token donné
   * @param strToken    Chaine encodée par getEncodeParam
   * @return int : checksum du token
   */
  public static function getCheckSumEncodage($strToken)
  {
    $iChecksum = 0;
    for($i=0; $i<strlen($strToken); $i++ ) {
      $iChecksum += hexdec(substr($strToken,  $i, 1));
    }
    return $iChecksum;
  }
  
  /**
   * Retourne la langue de navigateur
   * @param bReturnIndex =false par défaut pour retourner la langue sous forme texte, =true pour retourner l'indice de la langue
   * @return string si bReturnIndex=false
   *         int    si bReturnIndex=true
   */
  public static function getNavigatorLanguage($bReturnIndex=false)
  {
    $iIndex = 0;
    $strLg = "fr";
    if( !(isset($GLOBALS["tabLg"]) && is_array($GLOBALS["tabLg"]) && !empty($GLOBALS["tabLg"])) ) {
      return ( $bReturnIndex ? $iIndex : $strLg );
    }
    $strLg =  $GLOBALS["tabLg"][0];
    
    if( isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]) ) {
      // format : 'en,en-us;q=0.5,fr-fr;q=0.8' (exemple)
      $strAccept = strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
      // split chaque langue
      $tabLangs = explode(",", $strAccept);
      $i = 0;
      while( $i < count($tabLangs) ) { 
        $strLang = $tabLangs[$i];
        // retire le coefficiant d'ordre
        $tabLang = explode(";", $strLang);
        if( count($tabLang) > 0 ) {
          $strLangAccept = $tabLang[0];
          // extrait la langue du pays (séparé avec un tiret)
          $tabLg = explode("-", $strLangAccept);
          if( count($tabLg) >0 ) {
            $strLgAccept = $tabLg[0];
            if( in_array($strLgAccept, $GLOBALS["tabLg"]) ) {
              $strLg = $strLgAccept;
              $iIndex = array_search($strLgAccept, $GLOBALS["tabLg"]);
              break;
            } 
          }
        }
        $i++;
      }
    }
    
    return ( $bReturnIndex ? $iIndex : $strLg );
  }
}


?>