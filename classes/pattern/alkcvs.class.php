<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

if( !defined("ALK_ROOT_DIR") ) {
  define("ALK_ROOT_DIR", "/"); 
}

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkCVS
 * @brief Classe de gestion cvs Alkanet
 */
class AlkCVS 
{
  protected $strUserLogin;
  protected $strTrigramme;
  
  protected $strProjectName;
  protected $strProjectPatternName;
  protected $strPathProject;
  protected $strPathWorking;
  protected $strPathRepository;
  protected $strPathBaseTemp;
  protected $strPathTemp;

  protected $tabModules;
  protected $bCanDeployWork;
  protected $bCanDeployRelease;
  protected $bCanDeployPatch;
  
  protected $strReleaseNumber;
  protected $strReleaseDate;

  /**
   *  Constructeur par défaut
   *        Nécessite d'instancier this->atype_id avant d'appeler parent::__constructor()
   *
   * @param strPathProject nom du répertoire du projet
   * @param strUserLogin   Identifiant de l'utilisateur
   */
  public function __construct($strUserLogin, $strProjectName="")
  {
    $this->strUserLogin = $strUserLogin;
    $this->strTrigramme = substr($strUserLogin, 0, 2).substr($strUserLogin, -1, 1);
    
    $this->strPathBaseTemp = "/mnt/temp/_deploiementv3/";
    $this->strProjectPatternName = "mpr_modelevierge_v1.tgz"; 
    $this->strPathWorking = "/mnt/devperso/".$strUserLogin."/"; //"d:/intranet/www/";
    $this->strPathTemp    = $this->strPathBaseTemp.$strUserLogin."/";
    $this->strProjectName = $strProjectName;
    $this->strPathProject = ( $strProjectName != "" ? $this->strPathWorking.$strProjectName."/" : "" );
    
    $this->strPathRepository = ( defined("ALK_PATH_REPOSITORY_ALKCVS") ? ALK_PATH_REPOSITORY_ALKCVS : "/home/cvs/" );
    
    $this->tabModules = array();
    $this->bCanDeployWork    = false;
    $this->bCanDeployRelease = false;
    $this->bCanDeployPatch   = false;
    
    $this->strReleaseNumber = "";
    $this->strReleaseDate = "";
    
    if( !(file_exists($this->strPathTemp) && is_dir($this->strPathTemp)) ) {
       @mkdir($this->strPathTemp, 0777);
    }
  }

  /**
   *  Destructeur par défaut
   */
  public function __destruct()
  {
  }

  /**
   * Accesseur pour l'attribut strProjectName en mode modif et lecture
   * Retourne la valeur courante de l'attribut
   * @param  strProjectName  valeur optionnelle, modifie l'attribut si passé en paramètre
   * @return string
   */
  public function setProjectName($strProjectName="no_value")
  {
    if( $strProjectName != "no_value" ) {
      $this->strProjectName = $strProjectName;
      $this->strPathProject = ( $strProjectName != "" ? $this->strPathWorking.$strProjectName."/" : "" );
    }
    return $strProjectName;
  }

  /**
   * Création du projet alkanet mpr_xxx à partir d'un modèle existant au format .tgz
   * La marque de création est déduite de la branche en y suffixant _RELEASE
   * Retourne un tableau contenant 3 clés :
   * - cmds : La liste des cmd bash pour effectuer :
   *   - la création du projet vierge
   *   - ajout du fichier version_init.xml
   *   - création du fichier version.xml par copie de version_init.xml
   *   - import du projet dans cvs
   * - results : tableau contenant le retour de l'exécution
   * - error   : chaine contenant l'éventuelle erreur rencontrée
   * La fonction se charge au préalable de :
   *  - créer le fichier version_init.xml à la racine du répertoire temporaire de déploiement
   * @param cvs_depot              dépôt sélectionné
   * @param cvs_project            intitulé du projet au format mpr_[NOM]_v[n° version]
   * @param cvs_branch             intitulé de la branche [NOM]_[n° majeur]_[n° mineur]_[0]
   * @param tabModules             ensemble des modules composant le projet
   * @param strProjectPatternName  Nom du fichier archvie TGZ contenant le modèle du projet MPR
   * @return array
   */
  public function createAlkanetProject($cvs_depot, $cvs_project, $cvs_branch, $tabModules, $strProjectPatternName="")
  {
    if( $strProjectPatternName == "" ) {
      $strProjectPatternName = $this->strProjectPatternName;
    }
    
    $this->tabModules = array();
    $tabRes = array("cmds" => "", "results" => array(), "error" => "");
    
    // vérifie si le module projet n'existe pas déjà
    if( file_exists($this->strPathRepository.$cvs_depot."/".$cvs_project) && is_dir($this->strPathRepository.$cvs_depot."/".$cvs_project) ) {
      $tabRes["error"] = "Le projet ".$cvs_project." existe déjà dans le dépôt ".$cvs_depot.".";
      return $tabRes; 
    }

    // initialisation de $this->tabModules avec le module projet en premier
    $tabModuleProject = explode("_", $cvs_project);
    array_pop($tabModuleProject); // retire la version
    $strModulePath = str_replace("mpr_", "", implode("_", $tabModuleProject)); 
    $this->tabModules[$cvs_project] = 
      array("module"     => $cvs_project,
            "repository" => $cvs_depot,
            "branch"     => $cvs_branch,
            "lastTag"    => $cvs_branch."_RELEASE",
            "cvsParam"   => ":pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$cvs_depot,
            "modulePath" => $strModulePath,
            "basePath"   => "");
    foreach($tabModules as $strModule) {
      if( $strModule != "" ) {
        $tabModule = explode("|", $strModule);
        $this->tabModules[$tabModule[0]] =
          array("module"     => $tabModule[0],
                "repository" => $cvs_depot,
                "branch"     => $tabModule[1],
                "lastTag"    => $tabModule[2],
                "cvsParam"   => ":pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$cvs_depot,
                "modulePath" => $tabModule[4],
                "basePath"   => $tabModule[5]);
      }
    }

    // création du fichier version_init.xml à la racine de $this->strPathTemp
    $this->initReleaseFile(true);
    
    $strPathPattern = substr($strProjectPatternName, 0, -4);
    $strCmd = // nettoyage avant lancement du processus 
      "if [ -d ".$this->strPathTemp.$cvs_project." ]; then\n".
      "rm -rf ".$this->strPathTemp.$cvs_project."\n".
      "fi\n". 
      "if [ -d ".$this->strPathTemp.$strPathPattern." ]; then\n".
      "rm -rf ".$this->strPathTemp.$strPathPattern."\n".
      "fi\n". 
      // extraction du projet modèle
      "cd ".$this->strPathTemp."\n".
      "tar -xzf ".$this->strPathBaseTemp.$strProjectPatternName."\n".
      // copie du fichier version_init.xml
      "cp ".$this->strPathTemp."version_init.xml ".$this->strPathTemp.$strPathPattern.ALK_ROOT_DIR."libconf/\n".
      // création du fichier version.xml      
      "cp ".$this->strPathTemp."version_init.xml ".$this->strPathTemp.$strPathPattern.ALK_ROOT_DIR."libconf/version.xml\n";
      
    // création des répertoires upload par module map_xxx
    foreach($this->tabModules as $tabModule) {
      if( substr($tabModule["module"], 0, 4) == "map_" ) {
        $strCmd .= "mkdir ".$this->strPathTemp.$strPathPattern.ALK_ROOT_DIR."upload/".$tabModule["modulePath"]."\n";
        $strCmd .= "touch ".$this->strPathTemp.$strPathPattern.ALK_ROOT_DIR."upload/".$tabModule["modulePath"]."/vide\n";
      }
    }
      
    // import du projet cvs
    $strCmd .= "cd ".$this->strPathTemp.$strPathPattern."\n".
      "cvs -d :pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$cvs_depot." login\n".
      "cvs".
      " -d :pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$cvs_depot.
      " import -m \"\"".
      " ".$cvs_project.
      " ".$cvs_branch.
      " ".$cvs_branch."_RELEASE\n".
      "cvs -d :pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$cvs_depot." logout\n";
      
    // exécution des commandes
    $tabRes["cmds"] = $strCmd;
    exec($strCmd, $tabRes["results"]);
      
    return $tabRes;
  }

  /**
   * Effectue une extraction d'un projet pour le developpement
   * Retourne un tableau contenant 3 clés : 
   * cmds    : chaine contenant l'ensemble des commandes exécutées
   * results : tableau contenant le retour des commandes exécutées
   * error   : chaine contenant l'erreur rencontrée éventuelle
   * @param cvs_depot    dépôt sélectionné
   * @param cvs_project  intitulé du projet au format mpr_[NOM]_v[n° version]
   * @param cvs_branch   intitulé de la branche ou marque [NOM]_[n° majeur]_[n° mineur]_[0]
   * @return array
   */
  public function extractWorkingProject($cvs_depot, $cvs_project, $cvs_branch)
  {
    return $this->extractProject($cvs_depot, $cvs_project, $cvs_branch, $this->strPathWorking, true);
  } 

  /**
   * Effectue une extraction d'un projet release
   * Retourne un tableau contenant 3 clés :
   * cmds    : chaine contenant l'ensemble des commandes exécutées
   * results : tableau contenant le retour des commandes exécutées
   * error   : chaine contenant l'erreur rencontrée éventuelle
   * @param cvs_depot    dépôt sélectionné
   * @param cvs_project  intitulé du projet au format mpr_[NOM]_v[n° version]
   * @param cvs_branch   intitulé de la branche ou marque [NOM]_[n° majeur]_[n° mineur]_[0]
   * @return array
   */
  public function extractReleaseProject($cvs_depot, $cvs_project, $cvs_branch)
  {
    return $this->extractProject($cvs_depot, $cvs_project, $cvs_branch, $this->strPathTemp, false);
  }
  
  /**
   * TODO Effectue une extraction du dernier patch, en se basant sur le fichier version.xml
   * Le patch est effectué entre les deux dernières entrées du fichier version.xml
   * Retourne un tableau contenant 3 clés :
   * cmds    : chaine contenant l'ensemble des commandes exécutées
   * results : tableau contenant le retour des commandes exécutées
   * error   : chaine contenant l'erreur rencontrée éventuelle
   * @param cvs_depot    dépôt sélectionné
   * @param cvs_project  intitulé du projet au format mpr_[NOM]_v[n° version]
   * @param cvs_branch   intitulé de la branche ou marque [NOM]_[n° majeur]_[n° mineur]_[0]
   * @return array
   */
  public function extractLastPatch($cvs_depot, $cvs_project, $cvs_branch)
  {
    $tabRes = array("cmds" => "", "results" => array(), "error" => "");

    // selection mode extraction
    $strCvsCmd = "checkout";
    $strTargetPath = $this->strPathTemp;
    
    // ok, lire le fichier version.xml, faire un diff sur chaque module, puis extraire les diffs
    $tabModuleProject = explode("_", $cvs_project);
    array_pop($tabModuleProject); // retire la version
    $strModulePath = str_replace("mpr_", "", implode("_", $tabModuleProject));
    
    $this->setProjectName($strModulePath);
    $tabRes["cmds"] .= "\n# Projet ".$this->strProjectName." : ".$this->strPathProject."\n";

    // 1. On cherche le projet sur devperso
    if( !(file_exists($this->strPathProject) && is_dir($this->strPathProject)) ) {
      $tabRes["cmds"] .= "# Impossible de trouver le projet : ".$this->strPathProject."\n";
      return $tabRes;
    }
    
    // 2. 
    // TODO Faire la diff entre $tabParamsCVS et $tabParamsFile afin de savoir quels sont les modules à marquer
    // TODO Une fois que les modules modifiés depuis la dernière release ont été marqués, ajouter une nvelle 'release' dans version.xml
    
    $this->readModules(true);  // from file (version.xml)
    $tabModulesFile = $this->tabModules;
    $this->readModules(false); // from dir (CVS)
    $tabModulesCVS = $this->tabModules;
    
    $isUpToDate = true;
    $bUpdateRelease = false;
    foreach($tabModulesCVS as $modulePath=>$moduleParams) {
      // conserver les modules qui ne sont pas à jour, afin de les marquer
      if($moduleParams["status"]==false) {
        $isUpToDate = false;
        $tabRes["cmds"] .= "# Le module n'est pas à jour, il est peut-être nécessaire de marquer le module : ".$moduleParams["module"]."\n";
      }
      
      // vérifier s'il faut ajouter une nouvelle release dans version.xml
      $bUpdateRelease = $bUpdateRelease || 
        !(array_key_exists($modulePath, $tabModulesFile) && 
            $tabModulesFile[$modulePath]["lastTag"]==$moduleParams["lastTag"]);
      
    }
    // TODO si le projet n'est pas à jour, il faut d'abord marqués les modules
    if(!$isUpToDate) {
      return $tabRes;
    }
    if($bUpdateRelease) {
      $tabRes["cmds"] .= "# Mise à jour de la nouvelle release dans version.xml\n";
      $this->updateRelaseFile(); // se base sur $this->tabModules, donc ici celui de CVS
      $tabRes["cmds"] .= "# Pensez à commiter le fichier...\n";
      // TODO faut-il commiter le version.xml modifié ???
    }
    
    // 3. Faire un patch (rdiff cvs) entre les deux dernières releases de version.xml
    $this->readProjectByFile($this->strPathProject, 0); // dernière
    $tabParamsFileLast = $this->tabModules;
    $realeaseId = $this->strReleaseNumber."_".str_replace("/","",$this->strReleaseDate);
    
    $this->readProjectByFile($this->strPathProject, 1); // avant dernière
    $tabParamsFilePrevious = $this->tabModules;
    
    // se positioner dans /temp/deploiement
    $strPathPatch = $strTargetPath."patch_".$this->strProjectName."_".$realeaseId."/";
    $strCmd = "# Creation du patch dans ".$strPathPatch."\n\n".
          "cd ".$strTargetPath."\n".
          "if [ -d ".$strPathPatch." ]; then\n".
          "rm -rf ".$strPathPatch."\n".
          "fi\n".
          "mkdir ".$strPathPatch."\n".
          "cd ".$strPathPatch."\n";
    $tabRes["cmds"] .= $strCmd;
    exec($strCmd, $tabRes["results"]);
    
    foreach($tabParamsFileLast as $modulePath=>$moduleParams) {
      if( !array_key_exists($modulePath, $tabParamsFilePrevious) ) {
        // ce module n'existait pas avant, faire un export cvs
        $strCvsCmd = "export";
        $strCmd = "# Extraction du module ".$moduleParams["module"]."\n\n".
          "cd ".$strPathPatch."\n".
          "cvs -d ".$moduleParams["cvsParam"]." login\n".
          "cvs".
          " -d ".$moduleParams["cvsParam"].
          " ".$strCvsCmd.
          " -r ".$moduleParams["branch"].
          " ".$moduleParams["module"]."\n".
          "cvs -d ".$moduleParams["cvsParam"]." logout\n".
          "mv ".$moduleParams["module"]." ".$strPathPatch.$moduleParams["basePath"].$moduleParams["modulePath"];
        $tabRes["cmds"] .= $strCmd;
        exec($strCmd, $tabRes["results"]);
        
      } else {
        $moduleParamsPrevious = $tabParamsFilePrevious[$modulePath];
        // ce module existait avant, faire un rdiff cvs, et exporter les fichiers modifiés
        $isMpr = strpos($moduleParams["module"], "mpr_")===0; // starts with mpr_
        $lastTag = strpos($moduleParams["lastTag"], $moduleParams["modulePath"])===0 ? $moduleParams["lastTag"] :
                    $moduleParams["modulePath"]."_".str_replace(".", "_", $moduleParams["lastTag"]);
        $previousTag = strpos($moduleParamsPrevious["lastTag"], $moduleParamsPrevious["modulePath"])===0 ? $moduleParamsPrevious["lastTag"] : 
                        $moduleParamsPrevious["modulePath"]."_".str_replace(".", "_", $moduleParamsPrevious["lastTag"]);
        
        $strCmd = "\n# Diff sur module ".$moduleParams["module"]."\n".
          "cd ".$strPathPatch."\n".
          ($isMpr ? "" : 
            "if [ ! -d ".$strPathPatch.$moduleParams["basePath"]." ]; then\n".
            "mkdir ".$strPathPatch.$moduleParams["basePath"]."\n".
            "fi\n".
            "cd ".$strPathPatch.$moduleParams["basePath"]."\n"
          ).
          "cvs -d ".$moduleParams["cvsParam"]." login\n".
          "cvs".
          " -d ".$moduleParams["cvsParam"].
          " rdiff -c -s ".
          " -r ".$lastTag.
          " -r ".$previousTag.
          " ".$moduleParams["module"]." > ".$moduleParams["module"].".diff \n".
          "cvs -d ".$moduleParams["cvsParam"]." logout\n";
        $tabRes["cmds"] .= $strCmd;
        exec($strCmd, $tabRes["results"]);
        
        // ok, extraire les fichiers modifiés à partir du diff
        $strCmd = "# Extraction depuis le diff\n".
          "cd ".$strPathPatch.$moduleParams["basePath"]."\n".
          "cvs -d ".$moduleParams["cvsParam"]." login\n";
        $fp = fopen($strPathPatch.$moduleParams["basePath"].$moduleParams["module"].".diff", "r");
        if($fp) {
          while(!feof($fp)) {
            $line = fgets($fp);
            $matches=array();
            if(preg_match("/File\s([^\s]*)\s[changed|is new].*/i", $line, $matches)) {
              $strCvsCmd = "export";
              $strCmd .= 
                "cvs".
                " -d ".$moduleParams["cvsParam"].
                " ".$strCvsCmd.
                " -r ".$moduleParams["branch"].
                " ".$matches[1]."\n";
            }           
          }
          fclose($fp);
        }
        $strCmd .= 
          "cvs -d ".$moduleParams["cvsParam"]." logout\n".
          "if [ -d ".$moduleParams["module"]." ]; then\n".
          ($isMpr
            ? "mv ".$moduleParams["module"]."/* . \n".
              "rm -rf ".$moduleParams["module"]." \n"
            : "mv ".$moduleParams["module"]." ".$moduleParams["modulePath"]."\n"
          ).
          "fi\n";
          //"rm -f ".$moduleParams["module"].".diff \n"; // TODO SUPPR DES FICHIERS DIFF...
          
        $tabRes["cmds"] .= $strCmd;
        exec($strCmd, $tabRes["results"]);
      }
    }
    
    // 4. Finalement, effectuer le patch SGBD correspondant (! sans modif du version.xml cette fois-ci)
    // TODO script à terminer...
    
    // ...
      
    return $tabRes;
  }

  /**
   * Effectue une extraction d'un projet release ou pour le developpement
   * Retourne un tableau contenant 3 clés :
   * cmds    : chaine contenant l'ensemble des commandes exécutées
   * results : tableau contenant le retour des commandes exécutées
   * error   : chaine contenant l'erreur rencontrée éventuelle
   * @param cvs_depot     dépôt sélectionné
   * @param cvs_project   intitulé du projet au format mpr_[NOM]_v[n° version]
   * @param cvs_branch    intitulé de la branche ou marque [NOM]_[n° majeur]_[n° mineur]_[0]
   * @param strTargetPath chemin de base pour l'extraction
   * @param bWorking      true pour une extraction developpement, false pour une extraction release
   * @return array
   */
  private function extractProject($cvs_depot, $cvs_project, $cvs_branch, $strTargetPath, $bWorking=true)
  {
    $tabRes = array("cmds" => "", "results" => array(), "error" => "");

    // selection mode extraction
    $strCvsCmd = $bWorking ? "checkout" : "export";
    
    $strCmd = "# Extraction du projet ".$cvs_project."\n\n".
      "cd ".$strTargetPath."\n".
      "if [ -d ".$strTargetPath.$cvs_project." ]; then\n".
      "rm -rf ".$strTargetPath.$cvs_project."\n".
      "fi\n".
      "cvs -d :pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$cvs_depot." login\n".
      "cvs".
      " -d :pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$cvs_depot.
      " ".$strCvsCmd.
      " -r ".$cvs_branch.
      " ".$cvs_project."\n".
      "cvs -d :pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$cvs_depot." logout\n";

    $tabRes["cmds"] .= $strCmd;
    exec($strCmd, $tabRes["results"]);

    if( $cvs_depot == "PHP5DEV" ) {
      // extraction spécifique alkanet v3

      // migration du version.xml si besoin (nécessaire pour les étapes suivantes)
      $tabRes["cmds"] .= "\n# Migration des fichiers version_init.xml et version.xml (si besoin)\n";
      $bModif = $this->migrateProjectVersionFile($cvs_depot, $cvs_project, $cvs_branch, $strTargetPath.$cvs_project);
      $tabRes["cmds"] .= ($bModif) ? "NOTE: version_init.xml et version.xml ont été modifiés, pensez à les commiter !\n" : "";
      
      // lecture des modules à extraire à partir version.xml
      $this->setProjectName($cvs_project);
      $this->readProjectByFile($strTargetPath.$cvs_project);

      // récupération des commandes à exécuter
      $strCmd = $this->getCheckoutCmd($strTargetPath, $bWorking, $cvs_project, ($bWorking ? "checkout" : "export"));
      $tabRes["cmds"] .= "\n# Extraction des modules internes\n\n".$strCmd;

      // exécution des commandes
      $tabResults = array();
      exec($strCmd, $tabResults);

      $tabRes["results"] = array_merge($tabRes["results"], $tabResults);
    }

    return $tabRes;
  }

  /**
   * Création du fichier ./libconf/version_init.xml à partir des modules installés en développement
   * sur le devperso de l'utilisateur connecté
   * Retourne true si ok, false sinon
   * @return boolean
   */
  public function initReleaseFile($bCreateProject=false)
  {
    if( empty($this->tabModules) ) 
      return false;
    
    $xmlFileVersionInit = ( $bCreateProject
                            ? $this->strPathTemp."version_init.xml"
                            : substr($this->strPathProject, 0, -1).ALK_ROOT_DIR."libconf/version_init.xml" );
    
    $dom = new DomDocument();
    $dom->preserveWhiteSpace = false;
    $dom->loadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><updates></updates>");
    $dom->formatOutput = true;
    
    $update = $dom->createElement("update");
    $update->setAttribute("release", "1");
    $update->setAttribute("date", date('d/m/Y'));
    $update->setAttribute("name", $this->strTrigramme);
    
    foreach($this->tabModules as $tabModule) {
      $module = $dom->createElement("module");
      $module->setAttribute("name", $tabModule["basePath"].$tabModule["modulePath"]);
      $module->setAttribute("cvs_tag", $tabModule["lastTag"]);
      $module->setAttribute("cvs_repository", $tabModule["repository"]);
      $module->setAttribute("cvs_module", $tabModule["module"]);
      $module->setAttribute("cvs_branch", $tabModule["branch"]);
      $update->appendChild($module); 
    }
    
    $root = $dom->documentElement;
    $root->insertBefore($update, $root->getElementsByTagName("update")->item(0));
    $dom->save($xmlFileVersionInit);
    return true;
  }

  /**
   * Ajout d'une version au fichier ./libconf/version.xml.
   * Si celui ci n'existe, il est créé par duplication de version_init.xml
   * Retourne true si ok, false sinon
   * @return boolean
   */
  public function updateRelaseFile()
  {
    if( empty($this->tabModules) ) 
      return false;
    
    $xmlFileVersionInit = substr($this->strPathProject, 0, -1).ALK_ROOT_DIR."libconf/version_init.xml";
    $xmlFileVersion     = substr($this->strPathProject, 0, -1).ALK_ROOT_DIR."libconf/version.xml";
    
    if( file_exists($xmlFileVersion) && is_file($xmlFileVersion) ) {
      // rien à faire
    } elseif( file_exists($xmlFileVersionInit) && is_file($xmlFileVersionInit) ) {
      // recopie version_init.xml -> version.xml
      $bRes = @copy($xmlFileVersionInit, $xmlFileVersion);
      if( !$bRes ) return false;
    } else {
      // création du fichier init
      $bRes = $this->initReleaseFile();
      if( $bRes ) {
        // recopie version_init.xml -> version.xml
        $bRes = @copy($xmlFileVersionInit, $xmlFileVersion);
      }
      // inutile d'ajouter une autre version qui sera identique à la version init
      return $bRes; 
    }

    $dom = new DomDocument();
    $dom->preserveWhiteSpace = false;
    $dom->load($xmlFileVersion);
    $dom->formatOutput = true;

    $release = 1;
    $root = $dom->documentElement;
    $lastUpdate = $root->getElementsByTagName("update")->item(0);
    $release = $lastUpdate->getAttribute('release'); 

    $update = $dom->createElement("update");
    $update->setAttribute("release", $release+1);
    $update->setAttribute("date", date('d/m/Y'));
    $update->setAttribute("name", $this->strTrigramme);

    foreach($this->tabModules as $tabModule) {
      $module = $dom->createElement("module");
      $module->setAttribute("name", $tabModule["basePath"].$tabModule["modulePath"]);
      $module->setAttribute("cvs_tag", $tabModule["lastTag"]);
      $module->setAttribute("cvs_repository", $tabModule["repository"]);
      $module->setAttribute("cvs_module", $tabModule["module"]);
      $module->setAttribute("cvs_branch", $tabModule["branch"]);
      $update->appendChild($module); 
    }
    
    $root->insertBefore($update, $lastUpdate);
    $dom->save($xmlFileVersion);
    return true;
  }

  /**
   * Lecture à partir du fichier version.xml, la version installée lors du dernier patch
   * Retourne true si ok, false sinon
   * @return boolean
   */
  public function readPreviousReleaseFile()
  {
    if( empty($this->tabModules) ) 
      return false;
    
    $xmlFileVersionInit = substr($this->strPathProject, 0, -1).ALK_ROOT_DIR."libconf/version_init.xml";
    $xmlFileVersion     = substr($this->strPathProject, 0, -1).ALK_ROOT_DIR."libconf/version.xml";
        
    if( file_exists($xmlFileVersion) && is_file($xmlFileVersion) ) {
      // rien à faire
    } elseif( file_exists($xmlFileVersionInit) && is_file($xmlFileVersionInit) ) {
      // recopie version_init.xml -> version.xml
      $bRes = @copy($xmlFileVersionInit, $xmlFileVersion);
      if( !$bRes ) return false;
    } else {
      // création du fichier init
      $bRes = $this->initReleaseFile();
      if( $bRes ) {
        // recopie version_init.xml -> version.xml
        $bRes = @copy($xmlFileVersionInit, $xmlFileVersion);
      }
      if( !$bRes ) return false; 
    }

    // création d'une table d'index
    $tabIdx = array();
    foreach($this->tabModules as $strKey => $tabModule) {
      $tabIdx[$tabModule["module"]] = $strKey;
    }

    $dom = new DomDocument();
    $dom->preserveWhiteSpace = false;
    $dom->load($xmlFileVersion);
    $dom->formatOutput = true;

    $root = $dom->documentElement;
    $lastUpdate = $root->getElementsByTagName("update")->item(0);
    
    $dnlModules = $lastUpdate->getElementsByTagName("module");
    for($i=0; $i<$dnlModules->length; $i++) {
      $dnModule = $dnlModules->item($i); 
      $strModule = $dnModule->getAttribute("cvs_module");
      if( isset($this->tabModules[$tabIdx[$strModule]]) ) {
        $this->tabModules[$tabIdx[$strModule]]["prevTag"] = $dnModule->getAttribute("cvs_tag");
      }
    }
    
    return true;
  }

  /**
   * Récupère et mémorise la liste de tous les modules CVS du projet :
   * si bByInitVersionFile=true : en lisant le fichier version_init.xml, utile pour initialiser le projet en mode working
   * sinon                      : en analysant chaque module du projet installé sur le devperso de l'utilisateur connecté
   * Retourne true si ok, false sinon
   * @param bByInitVersionFile chemin de base du projet cvs
   * @return boolean
   */
  public function readModules($bByInitVersionFile=false)
  {
    $this->tabModule = array();
    $this->bCanDeployWork = false;
    $this->bCanDeployRelease = false;
    $this->bCanDeployPath    = false;
    $bRes = false;
    if( $bByInitVersionFile ) {
      // tabModules est prêt pour effectuer un déploiement release ou patch
      $this->bCanDeployWork = $this->readProjectByFile($this->strPathProject);
      $bRes = $this->bCanDeployWork;
    } else {
      // tabModules est prêt pour effectuer un déploiement release ou patch 
      $this->tabModules = $this->readProjectByDir($this->strPathProject, $this->strPathProject);
      ksort($this->tabModules);
      $this->bCanDeployRelease = true;
      $this->bCanDeployPath    = true;
      $bRes = true;
    }
    //print_r($this->tabModules);
    return $bRes;
  }
  
  /**
   * Retourne l'ensemble des commandes cvs pour déployer un projet complet
   */
  public function getReleaseProject()
  {
    $strCmd = $this->getCheckoutCmd($this->strPathTemp, false);
    $strCmd .= $this->getCleanCmd($this->strPathTemp.$this->strProjectName);
  } 

  /**
   * Retourne l'ensemble des commandes cvs pour déployer un patch au projet
   */
  public function getPatchProject($lastUpdateDate)
  {
    
  } 

  /**
   * Retourne la liste des modules d'un dépot
   * @param strRepository   nom du dépôt
   * @param strModulePrefix préfixe utilisé pour filtrer la liste
   * @param bExclude        =false par défaut pour include le préfixe dans le filtre, =true pour exclure le préfixe du filtre
   * @return array
   */
  public function getTabProjectsList($strRepository, $strModulePrefix="", $bExclude=false)
  {
    $tabRes = array();
    $strPath = $this->strPathRepository.$strRepository."/";
    if( file_exists($strPath) && is_dir($strPath) ) {
      $hDir = @opendir($strPath);
      while( $strFile = readdir($hDir) ) {
        if( $strFile == "." || $strFile == ".." || $strFile == "CVSROOT" ) 
          continue;
        if( is_dir($strPath.$strFile) && 
            ($strModulePrefix=="" || 
             $strModulePrefix!="" && ((!$bExclude && substr($strFile, 0, strlen($strModulePrefix)) == $strModulePrefix) || 
                                      ($bExclude && substr($strFile, 0, strlen($strModulePrefix)) != $strModulePrefix))) ) {
          $tabRes[$strFile] = $strFile;
        }
      }
      closedir($hDir);
    }
    
    if( !empty($tabRes) ) {
      ksort($tabRes); 
    }
    
    return $tabRes;
  }

  /**
   * Retourne la liste des dépots
   * @return array
   */
  public function getTabRepositoriesList($bCreate=false)
  {
    if( $bCreate )
      return array("PHP5DEV"  => "PHP5");
    return array("PHP5DEV"  => "PHP5",
                 "PHPDEV"   => "PHP4",
                 "LINUXDEV" => "Linux",
                 "ASPDEV"   => "ASP",
                 "WINDEV"   => "Windows",
                 );
  }

  /**
   * Retourne la liste des dépots
   * @return array
   */
  public function getTabPatternsList()
  {
   $tabRes = array();
    $strPath = $this->strPathBaseTemp;
    if( file_exists($strPath) && is_dir($strPath) ) {
      $hDir = @opendir($strPath);
      while( $strFile = readdir($hDir) ) {
        if( is_file($strPath.$strFile) && substr($strFile, -4) == ".tgz" ) { 
          $tabRes[$strFile] = $strFile;
        }
      }
      closedir($hDir);
    }
    
    if( !empty($tabRes) ) {
      ksort($tabRes); 
    }
    
    return $tabRes;
  }

  /**
   * Lit les infos de chaque module cvs de tabModules du dépot
   * Retourne un tableau au format
   * @param cvs_depot
   * @param tabModules
   * @return array
   */
  public function getTabModulesInfo($cvs_depot, $tabModules)
  {
    $tabModulesInfo = array();
    $tabBasePath = array("mcg" => "classes/", "mlp" => "/", "map" => "scripts/", "msg" => "services/");
    $tabModulePath = array("geditc" => "cedit");
    
    foreach($tabModules as $strModule) {
      $tabModuleName = explode("_", $strModule);
      $strModulePrefix = $tabModuleName[0];
      $strModulePath = ( isset($tabModuleName[count($tabModuleName)-2]) 
                         ? $tabModuleName[count($tabModuleName)-2] 
                         : $strModule );
      $strModulePath = ( array_key_exists($strModulePath, $tabModulePath)
                         ? $tabModulePath[$strModulePath]
                         : $strModulePath );
      $strBasePath = ( array_key_exists($strModulePrefix, $tabBasePath) 
                       ? $tabBasePath[$strModulePrefix] 
                       : "/" );
      
      $tabContents = array();
      $strCmd = "find ".$this->strPathRepository.$cvs_depot."/".$strModule." -name \"*.sql.patch.php,v\"";
      exec($strCmd, $tabContents);
      if( is_array($tabContents) && !empty($tabContents) && count($tabContents)<=2 ) { 
        $strPathFileName = $tabContents[0];
        if( file_exists($strPathFileName) && is_file($strPathFileName) ) {
          // recherche des versions
          $hf = @fopen($strPathFileName, "r");
          if( $hf ) {
            $strFileContents = fread($hf, 4096);
            $tabMatch = array();
            if( preg_match("/symbols(.*)locks/", str_replace("\n", "", $strFileContents), $tabMatch)>0 ) {
              $tabVersions = split("[:\t]", trim($tabMatch[1]));
              $lastTag = $tabVersions[0];
              $tabBranch = explode("_", $lastTag);
              array_pop($tabBranch);
              $branch = implode("_", $tabBranch)."_0"; 
              
              $tabModulesInfo[$strModule] = 
                array("module"     => $strModule,
                      "repository" => $cvs_depot,
                      "branch"     => $branch,
                      "lastTag"    => $tabVersions[0],
                      "cvsParam"   => ":pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$cvs_depot,
                      "modulePath" => $strModulePath,
                      "basePath"   => $strBasePath);
            }
            @fclose($hf); 
          }
        }
      }
    }
    
    return $tabModulesInfo;
  } 

  /**
   * Retourne la liste des commandes cvs pour effectuer le déployement complet (checkout ou export) d'un projet
   * @param strPath         chemin de base pour l'extraction
   * @param bWorkingProject =true par défaut pour caractériser un projet développement (extraction / branche)
   *                        =false pour une extraction / marque
   * @param strProjectName  ="" par défaut, <>"" si le projet maître est déjà extrait
   * @param strCvsCmd       mode d'extraction ="checkout" par défaut ou "export" sinon
   * @return string
   */
  private function getCheckoutCmd($strPath, $bWorkingProject=true, $strProjectName="", $strCvsCmd="checkout")
  {
    if( $strCvsCmd != "checkout" ) {
      $strCvsCmd = "export";
    }
    
    $strCmd = "";
    $cvsParam = "";

    foreach($this->tabModules as $tabModule) {
      if( $tabModule["module"] == "" )
        continue;
      $bMaster = false;
      if( $tabModule["basePath"]=="" && 
          ( $strProjectName=="" && $tabModule["modulePath"]==$this->strProjectName ||
            $strProjectName!="" && $tabModule["module"]==$this->strProjectName && $this->strProjectName==$strProjectName ) ) {
        // vérifie si le projet existe déjà, dans ce cas il est supprimé
        $strCmd .= "if [ -d ".$strPath.$tabModule["modulePath"]." ]; then\n".
          "rm -rf ".$strPath.$tabModule["modulePath"]."\n".
          "fi\n";
        $bMaster = true; 
      }
      
      if( $cvsParam == "" ) {
        $cvsParam = $tabModule["cvsParam"];
      }
      
      // vérifie si le répertoire extrait existe déjà, dans ce cas il est supprimé
      if( $bMaster && $strProjectName!="" ) {
        // cas module maître déjà extrait, dans ce cas :
        //  - effectue un update sur la branche ou marque donnée par le fichier version.xml
        //  - renommage de la racine
        $strCmd .= "cd ".$strPath.$strProjectName."\n".
          "cvs".
          " -d ".$tabModule["cvsParam"].
          " update".
          " -r ".( $bWorkingProject ? $tabModule["branch"] : $tabModule["lastTag"] ).
          " -Pd ".
          "\n";
        $this->setProjectName($tabModule["modulePath"]);
        $strProjectName = "";
        $strCmd .= "cd ".$strPath."\n".
          "mv ".$strPath.$tabModule["module"]." ".$strPath.$tabModule["modulePath"]."\n";
      } else {
        // cas extraction classique : extraction puis déplacement avec renommage     
        $strCmd .= "cd ".$strPath."\n".
          "if [ -d ".$strPath.$tabModule["module"]." ]; then\n".
          "rm -rf ".$strPath.$tabModule["module"]."\n".
          "fi\n".
          "cvs".
          " -d ".$tabModule["cvsParam"].
          " ".$strCvsCmd.
          " -r ".( $bWorkingProject ? $tabModule["branch"] : $tabModule["lastTag"] ).
          " ".$tabModule["module"].
          "\n".
          ( $bMaster
            ? "mv ".$strPath.$tabModule["module"]." ".$strPath.$tabModule["modulePath"]
            : "mv ".$strPath.$tabModule["module"]." ".$strPath.$this->strProjectName."/".$tabModule["basePath"].$tabModule["modulePath"] ).
          "\n";
      }
    }
    
    if( $strCmd != "" && $cvsParam != "" ) {
      $strCmd = "cvs -d ".$cvsParam." login\n".
        $strCmd.
        "cvs -d ".$cvsParam." logout\n"; 
    }
    
    return $strCmd;
  }

  /**
   * Retourne la liste des commandes cvs pour effectuer le déployement complet (export) d'un projet
   * @param strPath         chemin de base pour l'extraction
   * @param strProjectName  ="" par défaut, <>"" si le projet maître est déjà extrait
   * @return string
   */
  private function getExportCmd($strPath, $strProjectName="")
  {
    return $this->getCheckoutCmd($strPath, false, $strProjectName, "export");
  }

  /**
   * Retourne la liste des commandes pour nettoyer le projet avant déploiement
   * @param strPath  chemin du projet
   * @return string
   */
  private function getCleanCmd($strPath)
  {
    $strCmd = "";
    
    return $strCmd;
  }

  /**
   * Lecture du fichier version_init.xml pour initialiser le tabModules et permettre le déploiement du projet en mode work
   * sur le devperso de l'utilisateur connecté
   * Retourne True si ok, false sinon
   * @param strPathProject  chemin du projet sa racine (sans / a la fin)
   * @param releaseIdx      indice de la release à extraire (0=dernière, 1=avant dernière, etc.), vaut 0 par défaut
   * @return boolean
   */
  private function readProjectByFile($strPathProject, $releaseIdx=0)
  {
    $xmlFileVersion = $strPathProject.ALK_ROOT_DIR."libconf/version.xml";

    if( !(file_exists($xmlFileVersion) && is_file($xmlFileVersion)) ) return false;

    $dom = new DomDocument();
    $dom->preserveWhiteSpace = false;
    $dom->load($xmlFileVersion);
    $dom->formatOutput = true;

    $root = $dom->documentElement;
    if($root->getElementsByTagName("update")->length <= $releaseIdx) return false; // la release n'existe pas
    $lastUpdate = $root->getElementsByTagName("update")->item($releaseIdx);
    
    $this->strReleaseNumber = $lastUpdate->getAttribute("release");
    $this->strReleaseDate = $lastUpdate->getAttribute("date");
    
    $dnlModules = $lastUpdate->getElementsByTagName("module");
    for($i=0; $i<$dnlModules->length; $i++) {
      $dnModule = $dnlModules->item($i);
      $repository = $dnModule->getAttribute("cvs_repository");
      $strName    = $dnModule->getAttribute("name");
      $tabTmp = explode("/", $strName);
      if( is_array($tabTmp) && count($tabTmp)>0 ) {
        $strModulePath = array_pop($tabTmp);
        $strBasePath   = ( $i==0 ? "" : implode("/", $tabTmp)."/" );
      } else {
        $this->tabModules = array();
        return false; 
      }
      
      $strKey = ( $i==0 ? $strPathProject : $strPathProject."/".$strName );
      
      $this->tabModules[$strKey] = array("module"     => $dnModule->getAttribute("cvs_module"),
                                         "repository" => $repository,
                                         "branch"     => $dnModule->getAttribute("cvs_branch"),
                                         "lastTag"    => $dnModule->getAttribute("cvs_tag"),
                                         "cvsParam"   => ":pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$repository,
                                         "modulePath" => $strModulePath,
                                         "basePath"   => $strBasePath);
    }    
    return true;
  }

  /**
   * Lecture récursive des répertoires du projet pour retrouver tous les modules cvs qui le compose
   * Pour chaque module trouvé, récupération des paramètres cvs
   * @param $strPathBase  chemin de base (avec slash à la fin)
   * @param $strPath      chemin current / à la récursion (avec slash à la fin)
   */
  private function readProjectByDir($strPathBase, $strPath)
  {  
    if( !file_exists($strPath) )
      return array();

    $tabRes = array();
    if( $strPathBase == $strPath ) {
      // traitement du projet racine
      $tabParam = $this->readModuleParam(substr($strPath, 0, -1));
      if( !empty($tabParam) )
        $tabParam = $this->readCVSBranchTag(substr($strPath, 0, -1).ALK_ROOT_DIR."scripts/ident", $tabParam);
      if( !empty($tabParam) ) {
        $tabParam["modulePath"] = $this->strProjectName;
        $tabParam["basePath"] = ""; 
        $tabRes[$strPath] = $tabParam; 
      }
    }
    
    $hDir = opendir($strPath);
    while( $strFile = readdir($hDir) ) {
      if( $strFile=='.' || $strFile=='..' || 
          $strFile=='libconf' || $strFile=='upload' || $strFile=='styles' || $strFile=='CVS' ) 
        continue;
      if( @is_dir($strPath.$strFile) ) {
        $tabRes = array_merge($tabRes, $this->readProjectByDir($strPathBase, $strPath.$strFile."/"));
        $tabParam = $this->readModuleParam($strPath.$strFile);
        if( !empty($tabParam) )
          $tabParam = $this->readCVSBranchTag($strPath.$strFile, $tabParam);
        if( !empty($tabParam) ) {
          $tabParam["modulePath"] = $strFile;
          $tabParam["basePath"] = str_replace($this->strPathProject, "", $strPath); 
          $tabRes[$strPath.$strFile] = $tabParam; 
        }
      }
    }
    closedir($hDir);
 
    return $tabRes;
  }

  /**
   * Récupère les paramètres du module CVS repéréré par le chemin passé en paramètre.
   * Les paramètres ne sont retournés qu'à la racine du module
   * Retourne un tableau vide si ko
   *    sinon un tableau associatif avec les clés module, repository, branch et userParam
   * @param strPath  chemin du module traité (sans slash à la fin)
   * @return array
   */
  private function readModuleParam($strPath)
  {
    $module     = "";
    $repository = "";
    $branch     = "";
    $cvsParam   = "";
    
    $strPathCVS = $strPath."/CVS";
    if( !(file_exists($strPathCVS) && is_dir($strPathCVS)) ) 
      return array();
    
    $tabTmp = file($strPathCVS."/Repository");
    if( is_array($tabTmp) && count($tabTmp)>0 ) {
      $module = trim($tabTmp[0]);
      $tabTmp = explode("/", $module);
      $module = ( count($tabTmp) == 1 ? $tabTmp[0] : "" );
    } 

    $tabTmp = file($strPathCVS."/Tag");
    if( is_array($tabTmp) && count($tabTmp)>0 ) {
      $branch = trim($tabTmp[0]);
      // retire le premier caractère
      $branch = substr($branch, 1);
    } 

    $tabTmp = file($strPathCVS."/Root");
    if( is_array($tabTmp) && count($tabTmp)>0 ) {
      $cvsParam = trim($tabTmp[0]);
      $tabTmp = explode("/", $cvsParam);
      $repository = $tabTmp[count($tabTmp)-1];
    } 
   
    return ( $module!="" && $repository!="" && $branch!=""  
             ? array("module"     => $module, 
                     "repository" => $repository, 
                     "branch"     => $branch, 
                     "cvsParam"   => ":pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$repository )
             : array() );
  }

  /**
   * Execute une requête cvs pour récupérer la dernière marque du module sur la branche passée en paramètre
   * Retourne le tableau tabParam avec les clés complémentaires : lastTag, lock
   * @param strPath   chemin à la racine du module (sans slash à la fin)
   * @param tabParam  tableau calculé par la méthode readModuleParam
   * @return array
   */
  private function readCVSBranchTag($strPath, $tabParam)
  {
    if( empty($tabParam) ) return array();
    
    $strSqlPatchFileName = "";
    if( file_exists($strPath."/_admin/") && is_dir($strPath."/_admin/") ) {
      $strPath .= "/_admin/";
    } else {
      $strPath .= "/";
    } 
    $hDir = opendir($strPath);
    while( $strFile = readdir($hDir) ) {
      if( substr($strFile, -14) == ".sql.patch.php" ) {
        $strSqlPatchFileName =  $strFile;
        break;
      }
    }
    closedir($hDir);
    
    if( $strSqlPatchFileName != "" ) {
      $tabContents = array();
      
      // recherche le status cvs du fichier *.sql.patch.php
      $strCmd = "cd ".$strPath." && cvs st -v ".$strSqlPatchFileName; 
      exec($strCmd, $tabContents);

      $strStatus = "";
      $strLocalRevision = "";
      $strRepositoryRevision = "";
      $strCurrentBranch = "";

      $i = 0;
      while( $i<count($tabContents) && strpos($tabContents[$i], "Existing Tags:") === false ) {
        $tabMatch = array();
         
        if( preg_match("/Status:[\s]*(.+)/", $tabContents[$i], $tabMatch)>0 ) {
          $strStatus = trim($tabMatch[1]);
        } 
        elseif( preg_match("/Working revision:[\s]*([\d.]+)/", $tabContents[$i], $tabMatch)>0 ) {
          // détection en local de la version interne du dernier commit sur la branche courante
          $strLocalRevision = trim($tabMatch[1]);
        }
        elseif( preg_match("/Repository revision:[\s]*([\d.]+)/", $tabContents[$i], $tabMatch)>0 ) {
          // détection sur le dépôt de la version interne du dernier commit sur la branche courante
          $strRepositoryRevision = trim($tabMatch[1]);
        }
        $i++;
      }
      
      // le module est bien à jour localement
      $tabParam["status"]  = ( $strStatus == "Up-to-date" && $strLocalRevision == $strRepositoryRevision ? true : false ); 
      $tabParam["lastTag"] = "";
      $tabParam["lastTagStatus"] = false;
      
      $tabBranchVersion = explode("_", $tabParam["branch"]);
      
      $bFound = false;
      while( $i<count($tabContents) && !$bFound ) {
        $tabMatch = array();
        
        if( preg_match("/[\s]*([a-zA-Z0-9_-]*)[\s]*[(][revision|branch]+:[\s]*([\d.]+)[)]/", $tabContents[$i], $tabMatch)>0 ) {
          $strTag = trim($tabMatch[1]);
          $strRevision = trim($tabMatch[2]);

          $tabTagVersion = explode("_", $strTag);
          $bTag = true;
          for($b=0; $b<count($tabBranchVersion)-1; $b++) {
            $bTag = $bTag && $tabBranchVersion[$b]==$tabTagVersion[$b];
          }
          if( $bTag ) {
            $tabParam["lastTag"]       = $strTag;
            // le dernier tag correspond bien au dernier commit
            $tabParam["lastTagStatus"] = ( $strRepositoryRevision==$strRevision ? true : false );  
            $bFound = true;
          }
        }
        $i++;
      }
      return $tabParam;
    }
    return array();
  }
  
  
  /**
   * Migration du fichier version_init.xml du projet passé en paramètre
   * Retourne True si fichier modifié, False sinon
   * @param strPathProject  chemin du projet sa racine (sans / a la fin)
   * @return boolean
   */
  private function migrateProjectVersionFile($cvs_depot, $cvs_project, $cvs_branch, $strPathProject) {
    
    $tabXmlFileVersion[] = $strPathProject.ALK_ROOT_DIR."libconf/version_init.xml";
    $tabXmlFileVersion[] = $strPathProject.ALK_ROOT_DIR."libconf/version.xml";

    foreach($tabXmlFileVersion as $xmlFileVersion) {
    
      if( !(file_exists($xmlFileVersion) && is_file($xmlFileVersion)) ) return false;
  
      $dom = new DomDocument();
      $dom->preserveWhiteSpace = false;
      $dom->load($xmlFileVersion);
      $dom->formatOutput = true;
      $root = $dom->documentElement;
      $bModif = false;
      
      // chercher tous les tags 'update' et l'ancien attribut 'livraison'
      $updates = $root->getElementsByTagName("update");
      foreach($updates as $update) {
        // 1. modifier l'ancien attribut 'livraison' par 'release'
        if($update->hasAttribute("livraison")) {
          $value = $update->getAttribute("livraison");
          $update->removeAttribute("livraison");
          $update->setAttribute("release", $value);
          $bModif = true;
        }
        
        // 2. pour chaque module, modifier l'ancien attribut 'version' par 'cvs_tag'
        $modules = $update->getElementsByTagName("module");
        $hasMprModule = false;
        foreach($modules as $module) {
          if($module->hasAttribute("version")) {
            $value = $module->getAttribute("version");
            $module->removeAttribute("version");
            $module->setAttribute("cvs_tag", $value);
            $bModif = true;
          }
          $hasMprModule = strpos($module->getAttribute("cvs_tag"), "_RELEASE")>0;
        }
        if(!$hasMprModule) {
          // 3. ajouter le projet (mpr) dans la liste des updates si non présent
          // le mpr doit être le 1er noeud, et avoir un cvs_tag qui termine par '_RELEASE'
          $tabModuleProject = explode("_", $cvs_project);
          array_pop($tabModuleProject); // retire la version
          $strModulePath = str_replace("mpr_", "", implode("_", $tabModuleProject));
          
          $node = $dom->createElement("module");
          $node->setAttribute("name", $strModulePath);
          $node->setAttribute("cvs_tag", $cvs_branch."_RELEASE");
          $node->setAttribute("cvs_repository", $cvs_depot);
          $node->setAttribute("cvs_module", $cvs_project);
          $node->setAttribute("cvs_branch", $cvs_branch);
          
          $update->insertBefore($node, $update->getElementsByTagName("module")->item(0));
          $bModif = true;
        }
      }
      // enregistre les modifications éventuelles
      if($bModif) $dom->save($xmlFileVersion);
      
      // TODO Faut-il commiter directement les modifications ? 
    
    }
    return $bModif;
  }
  
  
  /**
   * Execute un requête cvs pour retourner la liste des branches d'un module
   * Cette fonction se base sur la commande 'cvs rlog', et l'on cherche dans les logs 
   * les lignes du type 'branchname: 1.1.0.2', où le '.0.' est un marqueur magique cvs 
   * qui sert à identifier les branches. (TODO: il est aussi possible de chercher le tag 'RELEASE' 
   * pour avoir la branche par défaut initialisée à la création)
   * @param cvs_depot              dépôt sélectionné
   * @param cvs_project            intitulé du projet au format mpr_[NOM]_v[n° version]
   * @return tableau des branches du modules sélectionné
   */
  public function getBranchesFromRepository($cvs_depot, $cvs_projet) {
    // exemple: cvs -d :pserver:vlegloahec:cvs@cvs.alkante.al:/home/cvs/PHP5DEV rlog -h mpr_betton_v1 2>/dev/null 
    // | grep ": .*\.0\..*" | awk -F: '{print $1}' | sed -e 's/[[:blank:]]*//' | sort -u
    $cmd = "cvs -d ".
           " :pserver:".$this->strUserLogin.":cvs@cvs.alkante.al:".$this->strPathRepository.$cvs_depot.
           " rlog -h ".$cvs_projet." 2>/dev/null ".
           " | grep \": .*\.0\..*\" ". // ex. chercher "_RELEASE"
           " | awk -F: '{print $1}' ".
           " | sed -e 's/[[:blank:]]*//' ".
           " | sort -u";
    $branches = array();
    exec($cmd, $branches);
    
    return $branches;
  }


}

?>
