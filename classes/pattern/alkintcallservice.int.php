<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


/**
 * @package Alkanet_Class_Pattern
 * 
 * @interface AlkIntCallService
 * @brief Interface de définition des callServices
 * 
 * Convention de structure : créer une interface AlkIntCallService<module> héritant de AlkIntCallService par module appelant. 
 * Placer dans AlkIntCallService les services devant être obligatoirement implémenté par tous les modules implémentant une de ces interfaces.
 * 
 * Convention de nommage des fonctions : <type d'action><module de provenance><indication sur le fichier appelant>
 * Exemple : <type d'action> = {getHtml, doSql, ...}
 *           <module de provenance> = {Annu, Fdoc, ...}
 *           <indication sur le fichier appelant> = UserForm pour le fichier alkhtmlformagentform
 * 
 */
interface AlkIntCallService
{
  
}

/**
 * @package Alkanet_Class_Pattern
 * 
 * @interface AlkIntCallServiceAnnu
 * @extends   AlkIntCallService
 * @brief Interface de définition des callServices du module Annu
 * 
 */
interface AlkIntCallServiceAnnu extends AlkIntCallService
{
  ######################################## Fiche utilisateur ########################################  
  /**
   * Ajout de code html sur la fiche utilisateur - fonctionne avec AlkIntCallServiceAnnu::getHtmlAnnuUserForm
   * Permet aux applications de l'espace de rajouter des onglets spécifiques à la fiche utilisateur
   * @param oForm       AlkHtmlForm   Formulaire d'agent
   * @param oPanelSheet AlkHtmlPanel  Panel sur lequel chaque application peut ajouter un onglet
   * @param agent_id                  Identifiant de l'agent modifié
   */
  public function getHtmlAnnuUserForm(AlkHtmlForm $oForm, AlkHtmlPanel $oPanelSheet, $agent_id);
  
  /**
   * Enregistrement des champs ajoutés à la fiche utilisateur - fonctionne avec AlkIntCallServiceAnnu::doSqlAnnuUserForm
   * Permet aux applications de l'espace d'enregistrer les informations spécifiques aux appli ajoutées à la fiche utilisateur
   * @param oForm       AlkHtmlForm   Formulaire d'agent
   * @param agent_id                  Identifiant de l'agent modifié
   * @param tabFields                 Liste des informations à enregistrer
   */
  public function doSqlAnnuUserForm(AlkHtmlForm $oForm, $agent_id, $tabFields=array());

  ######################################## Fiche service ########################################  
  /**
   * Ajout de code html sur la fiche service - fonctionne avec AlkIntCallServiceAnnu::getHtmlAnnuServiceForm
   * Permet aux applications qui implémentent cette interface de rajouter des onglets spécifiques à la fiche service
   * @param oForm       AlkHtmlForm   Formulaire du service
   * @param oPanelSheet AlkHtmlPanel  Panel sur lequel chaque application peut ajouter un onglet
   * @param service_id                Identifiant du service modifié
   */
  public function getHtmlAnnuServiceForm(AlkHtmlForm $oForm, AlkHtmlPanel $oPanelSheet, $service_id);
  
  /**
   * Enregistrement des champs ajoutés à la fiche service - fonctionne avec AlkIntCallServiceAnnu::doSqlAnnuServiceForm
   * Permet aux applications qui implémentent cette interface d'enregistrer les informations spécifiques aux appli ajoutées à la fiche service
   * @param oForm       AlkHtmlForm   Formulaire du service
   * @param service_id                Identifiant du service modifié
   * @param tabFields                 Liste des informations à enregistrer
   */
  public function doSqlAnnuServiceForm(AlkHtmlForm $oForm, $service_id, $tabFields=array());
  
  ######################################## Fiche profil ########################################  
  /**
   * Ajout de code html sur la fiche profil - fonctionne avec AlkIntCallServiceAnnu::getHtmlAnnuProfilForm
   * Permet aux applications qui implémentent cette interface de rajouter des onglets spécifiques à la fiche profil
   * @param oForm       AlkHtmlForm   Formulaire du profil
   * @param oPanelSheet AlkHtmlPanel  Panel sur lequel chaque application peut ajouter un onglet
   * @param service_id                Identifiant du service modifié
   */
  public function getHtmlAnnuProfilForm(AlkHtmlForm $oForm, AlkHtmlPanel $oPanelSheet, $service_id);
  
  /**
   * Enregistrement des champs ajoutés à la fiche profil - fonctionne avec AlkIntCallServiceAnnu::doSqlAnnuProfilForm
   * Permet aux applications qui implémentent cette interface d'enregistrer les informations spécifiques aux appli ajoutées à la fiche profil
   * @param oForm       AlkHtmlForm   Formulaire du profil
   * @param profil_id                 Identifiant du profil modifié
   * @param tabFields                 Liste des informations à enregistrer
   */
  public function doSqlAnnuProfilForm(AlkHtmlForm $oForm, $profil_id, $tabFields=array());
}

/**
 * @package Alkanet_Class_Pattern
 * 
 * @interface AlkIntCallServiceGisViewer
 * @extends   AlkIntCallService
 * @brief Interface de définition des callServices du module GisViewer
 * 
 */
interface AlkIntCallServiceGisViewer extends AlkIntCallService
{
  ######################################## Edition de carte sur viewer ########################################  
  /**
   * Ajout de couches paramétrées provenant des applications - fonctionne avec AlkIntCallServiceAnnu::doSqlAnnuUserForm
   * Permet aux applications de l'espace de rajouter la liste des couches requetes paramatrées qu'elles mettent à disposition dans
   * le sélecteur de couches de l'interface d'édition de carte au sein du viewer
   * @param oForm       AlkHtmlForm   Formulaire de sélection de couches
   * @param map_projection            Projection de la carte
   * @return array(Titre de partie/provenance => array(Titre de couche => requete de sélection de couche))
   */
  public function getHtmlListeCoucheParametree(AlkHtmlForm $oForm, $map_projection);
  
}

?>