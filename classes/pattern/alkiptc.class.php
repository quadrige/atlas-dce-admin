<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l"INRIA
sur le site http://www.cecill.info.

En contrepartie de l"accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n"est
offert aux utilisateurs qu"une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l"auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l"attention de l"utilisateur est attirée sur les risques
associés au chargement, à l"utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l"utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l"adéquation du
logiciel à leurs besoins dans des conditions permettant d"assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l"utiliser et l"exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


/**
 * @package Alkanet_Class_Pattern
 * @class AlkIptc
 * 
 * Classe de manipulation de métadonnées IPTC, notamment pour les images JPG, JPEG
 * Si un fichier de type autre est fourni, il ne sera tout simplement pas possible de lire ni écrire dans les métadonnées IPTC
 * 
 * Actuellement, la classe ne gère pas la sauvegarde de plusieurs mots-clés sous forme de tableau dans le fichier.
 */
class AlkIptc extends AlkObject
{
  protected $file = "";
  protected $tabTagName = array();
  
  /**
   * Constructeur par défaut
   */
  public function __construct()
  {
    parent::__construct();
    $this->setTabTagName();
  }
  
  /**
   * Vérifie s'il est possible d'écrire des métadonnées IPTC dans le fichier fourni en paramètre
   * précondition : le fichier existe
   * postcondition : ne modifie pas le fichier
   * @param string $filename    emplacement du fichier
   * @return boolean true s'il est possible d'écrire des métadonnées IPTC dans le fichier, false sinon
   */
  protected function isFileEmbedded($filename)
  {
    if (GetTypeMime($filename)=="image/jpeg"){
      $iptcdata = $this->_transform_iptc("2#005", "Title"); // métadonnées fictives
      if ( @iptcembed($iptcdata, $filename) ) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }
  
  /**
   * Vérifie que le fichier existe
   * 
   * @param string $filename    emplacement du fichier
   * @return 1 : OK
   *         0 : $filename n'est pas une chaine
   *        -1 : $filename est vide
   *        -2 : $filename ne correspond pas à un emplacement fichier
   *        -3 : le format du fichier ne permet pas d'écrire des métadonnées IPTC
   */
  protected function checkFile($filename)
  {
    // vérifie que le paramètre est bien une chaine
    if ( @is_string($filename) ) {
      // vérifie que la chaine n'est pas vide
      if ( !empty($filename) ) {
        // vérifie que le fichier existe
        if ( @is_file($filename) ) {
          if ( $this->isFileEmbedded($filename) ) {
            return 1;
          } else {
            return -3;
          }
        } else {
          return -2;
        }
      } else {
        return -1;
      }
    } else {
      return 0;
    }
  }
  
  /**
   * Initialise l'emplacement du fichier
   * 
   * @param string $filename    emplacement du fichier
   * @return 1 : OK
   *         0 : $filename n'est pas une chaine
   *        -1 : $filename est vide
   *        -2 : $filename ne correspond pas à un emplacement fichier
   *        -3 : le format du fichier ne permet pas d'écrire des métadonnées IPTC
   */
  public function setFile($filename)
  {
    if ( $res = $this->checkFile($filename) ) {
      $this->file = $filename;
    }
    return $res;
  }
  
  /**
   * Retourne l'emplacement du fichier
   * 
   * @return string emplacement du fichier
   */
  public function getFile()
  {
    return $this->file;
  }
  
  /**
   * Initialise les tags IPTC
   *    clé : code du tag IPTC
   *    valeur : nom du tag IPTC
   * 
   * @return void
   */
  protected function setTabTagName()
  {
    $this->tabTagName = array(
      "2#003" => "Object type",
      "2#004" => "Object attribute",
      "2#005" => "Title",
      "2#007" => "Edit status",
      "2#008" => "Editorial update",
      "2#010" => "Urgency",
      "2#012" => "Subject reference",
      "2#015" => "Category",
      "2#020" => "Supplemental categories",
      "2#022" => "Fixture identifier",
      "2#025" => "Keywords",
      "2#026" => "Location code",
      "2#027" => "Location name",
      "2#030" => "Release date",
      "2#035" => "Release time",
      "2#037" => "Expiration date",
      "2#038" => "Expiration time",
      "2#040" => "Instructions",
      "2#042" => "Action advised",
      "2#045" => "Reference service",
      "2#047" => "Reference date",
      "2#050" => "Reference number",
      "2#055" => "Date created",
      "2#060" => "Time created",
      "2#062" => "Digital creation date",
      "2#063" => "Digital creation time",
      "2#065" => "Originating program",
      "2#070" => "Program version",
      "2#075" => "Object cycle",
      "2#080" => "Author",
      "2#085" => "AuthorsPosition",
      "2#090" => "City",
      "2#092" => "Sublocation",
      "2#095" => "State/Province",
      "2#100" => "Country code",
      "2#101" => "Country",
      "2#103" => "Transmission reference",
      "2#105" => "Headline",
      "2#110" => "Credit",
      "2#115" => "Source",
      "2#116" => "Copyright notice",
      "2#118" => "Contact Information",
      "2#120" => "Description",
      "2#122" => "Description writer",
      "2#130" => "Image type",
      "2#131" => "Image orientation",
      "2#135" => "Language identifier",
      "2#150" => "AudioType",
      "2#151" => "AUdio sampling rate",
      "2#152" => "Audio sampling resolution",
      "2#153" => "Audio duration",
      "2#154" => "Audio outcue",
      "2#184" => "Job ID",
      "2#185" => "Master document ID",
      "2#186" => "Short document ID",
      "2#187" => "Unique document ID",
      "2#188" => "Owner ID",
      "2#200" => "Object preview file format",
      "2#201" => "Object preview file version",
      "2#202" => "Object preview data",
      "2#221" => "Prefs",
      "2#225" => "Classify state",
      "2#228" => "Similarity index",
      "2#230" => "Document notes",
      "2#231" => "Document history",
      "2#232" => "Exif camera info",
      "2#255" => "Catalog sets"
    );
  }
  
  /**
   * Si une clé est fournie, retourne la valeur du tag, sinon un tableau contenant toutes les valeurs des tags
   * 
   * @param string $key       clé correspondant au code du tag
   * @return string si une valeur est trouvée pour la clé fournie
   *         array tableau de tous les tags si aucune clé fournie
   *         0 : fichier non initialisé
   *        -1 : clé non définie
   *        -2 : aucune métadonnée IPTC dans le fichier
   *        -3 : erreur de récupération des tags IPTC ou aucun tag trouvé
   *        -4 : tag non trouvé
   */
  public function getByCode($key=null)
  {
    return $this->get($key, "code");
  }
  
  /**
   * @deprecated utiliser de préférence getByCode($key), le nom étant susceptible de changer
   * Si une clé est fournie, retourne la valeur du tag, sinon un tableau contenant toutes les valeurs des tags
   * 
   * @param string $key       clé correspondant au nom du tag
   * @return string si une valeur est trouvée pour la clé fournie
   *         array tableau de tous les tags si aucune clé fournie
   *         0 : fichier non initialisé
   *        -1 : clé non définie
   *        -2 : aucune métadonnée IPTC dans le fichier
   *        -3 : erreur de récupération des tags IPTC ou aucun tag trouvé
   *        -4 : tag non trouvé
   */
  public function getByName($key=null)
  {
    return $this->get($key, "name");
  }
  
  /**
   * Si une clé est fournie, retourne la valeur du tag, sinon un tableau contenant toutes les valeurs des tags
   * 
   * @param string $key       clé correspondant au nom ou code du tag
   * @param string $keyType   type utilisé pour $key ("name" ou "code"), "name" par défaut
   * @return string si une valeur est trouvée pour la clé fournie
   *         array tableau de tous les tags si aucune clé fournie
   *         0 : fichier non initialisé
   *        -1 : clé non définie
   *        -2 : aucune métadonnée IPTC dans le fichier
   *        -3 : erreur de récupération des tags IPTC ou aucun tag trouvé
   *        -4 : tag non trouvé
   */
  protected function get($key=null, $keyType="name")
  {
    if ( $this->file ) {
      // récupère la valeur du tag
      if ( !is_null($key) && !empty($key) ) {
        if ( $tag = $this->getTagByKey($key, $keyType) ) {
          $info = array();
          $size = getimagesize($this->file, $info);
          if ( isset($info["APP13"]) ) {
            if ( $iptc = @iptcparse($info["APP13"]) ) {
              if ( isset($iptc[$tag]) ) {
                return $this->convert_data(@implode(", ", $iptc[$tag]), ALK_HTML_ENCODING);
              } else {
                return -4;
              }
            } else {
              return -3;
            }
          } else {
            return -2;
          }
        } else {
          return -1;
        }
      }
      // récupère les valeurs de tous les tags
      else {
        $info = array();
        $size = @getimagesize($this->file, $info);
        if ( isset($info["APP13"]) ) {
          return $this->convert_data(@iptcparse($info["APP13"]), ALK_HTML_ENCODING); 
        } else {
          return -2;
        }
      }
    } else {
      return 0;
    }
  }
  
  /**
   * Ajoute/modifie les valeurs des tags passés en paramètre
   * 
   * @param array $tabValues    tableau de correspondance contenant les valeurs des tags à ajouter/modifier
   *                              clé : clé correspondant au code du tag
   *                              valeur : valeur du tag
   * @param string $filename    emplacement de la copie du fichier original
   * @return 1 : OK (si des clés ne sont pas définies, elles ne seront pas ajoutées)
   *         0 : fichier non initialisé
   *        -1 : $tabValues n'est pas un tableau
   *        -2 : $tabValues est vide
   *       -11 : erreur d'ouverture du fichier
   *       -12 : erreur d'écriture dans le fichier
   */
  public function setByCode($tabValues, $filename="")
  {
    return $this->set($tabValues, "code", $filename);
  }
  
  /**
   * @deprecated utiliser de préférence setByCode($key), le nom étant susceptible de changer
   * Ajoute/modifie les valeurs des tags passés en paramètre
   * 
   * @param array $tabValues    tableau de correspondance contenant les valeurs des tags à ajouter/modifier
   *                              clé : clé correspondant au nom du tag
   *                              valeur : valeur du tag
   * @param string $filename    emplacement de la copie du fichier original
   * @return 1 : OK (si des clés ne sont pas définies, elles ne seront pas ajoutées)
   *         0 : fichier non initialisé
   *        -1 : $tabValues n'est pas un tableau
   *        -2 : $tabValues est vide
   *       -11 : erreur de création du contenu binaire (IPTC + image)
   *       -12 : erreur d'ouverture du fichier
   *       -13 : erreur d'écriture dans le fichier
   */
  public function setByName($tabValues, $filename="")
  {
    return $this->set($tabValues, "name", $filename);
  }
  
  /**
   * Ajoute/modifie les valeurs des tags passés en paramètre
   * 
   * @param array $tabValues    tableau de correspondance contenant les valeurs des tags à ajouter/modifier
   *                              clé : clé correspondant au nom ou code du tag
   *                              valeur : valeur du tag
   * @param string $keyType     type utilisé pour la clé ("name" ou "code"), "name" par défaut
   * @param string $filename    emplacement de la copie du fichier original
   * @return 1 : OK (si des clés ne sont pas définies, elles ne seront pas ajoutées)
   *         0 : fichier non initialisé
   *        -1 : $tabValues n'est pas un tableau
   *        -2 : $tabValues est vide
   *       -11 : erreur de création du contenu binaire (IPTC + image)
   *       -12 : erreur d'ouverture du fichier
   *       -13 : erreur d'écriture dans le fichier
   */
  protected function set($tabValues, $keyType="name", $filename="")
  {
    if ( $this->file ) {
      if ( is_array($tabValues) ) {
        if ( count($tabValues) > 0 ) {
          $tabValuesNew = array();
          // récupère les tags IPTC du fichier
          $tabValuesOld = $this->get();
          if ( is_array($tabValuesOld) ) {
            foreach ($tabValuesOld as $tag => $value ) {
              $tabValuesNew[$tag] = @implode(", ", $value);
            }
          }
          // ajoute/modifie les nouveaux tags IPTC au fichier
          foreach ( $tabValues as $key => $value ) {
            if ( $tag = $this->getTagByKey($key, $keyType) ) {
              $tabValuesNew[$tag] = $this->convert_data($value);
            }
          }
          $iptcdata = null;
          foreach ( $tabValuesNew as $tag => $value ) {
            $tag = substr($tag, 2);
            $iptcdata .= $this->_transform_iptc($tag, $value);
          }
          return $this->save($iptcdata, $filename);
        } else {
          return -2;
        }
      } else {
        return -1;
      }
    } else {
      return 0;
    }
  }
  
  /**
   * Encode une donnée dans l'encodage souhaité
   * 
   * @param mixed  $data      donnée à encoder
   * @param string $encoding  chaine définissant l'encodage de sortie
   * @return mixed donnée encodée dans l'encodage souhaité
   */
  private function convert_data($data, $encoding="ISO-8859-1")
  {
    if ( is_array($data) ) {
      $tabValues = array();
      foreach ( $data as $key => $value ) {
        $tabValues[$key] = $this->convert_data($value);
      }
      return $tabValues;
    } elseif ( @is_string($data) ) {
      return mb_convert_encoding($data, $encoding, mb_detect_encoding($data."a", "UTF-8, ISO-8859-15, ISO-8859-1"));
    } else {
      return $data;
    }
  }
  
  /**
   * Retourne la valeur du tag en fonction de la valeur de la clé et de son type
   * 
   * @param string $key       clé correspondant au code ou au nom du tag
   * @param string $keyType   type de la clé ("name" ou "code")
   * 
   * @return string valeur du tag, chaine vide si clé non trouvée
   */
  private function getTagByKey($key, $keyType)
  {
    $tag = "";
    switch ( $keyType ) {
      case "code" :
        if ( array_key_exists($key, $this->tabTagName) ) {
          $tag = $key;
        }
      break;
      case "name" :
        $tag = array_search($key, $this->tabTagName);
        if ( !$tag ) {
          $tag = "";
        }
      break;
    }
    return $tag;
  }
  
  /**
   * Formate une valeur d'IPTC
   *
   * @param string $data   code du tag de l'IPTC (de type 110 et non 2#110)
   * @param string $value  valeur à insérer dans le tag
   * @return string donnée IPTC formattée
   */
  private function _transform_iptc($data, $value)
  {
    $length = strlen($value);
    $retval = chr(0x1C).chr(2).chr($data);
    
    if ($length < 0x8000) {
      $retval .= chr($length >> 8).chr($length& 0xFF);
    } else {
      $retval .= chr(0x80).chr(0x04).
      chr(($length >> 24)& 0xFF).
      chr(($length >> 16)& 0xFF).
      chr(($length >> 8)& 0xFF).
      chr($length& 0xFF);
    }
    
    return $retval.$value;
  }
  
  /**
   * Sauvegarde les données IPTC passées en paramètre dans une copie du fichier original si le paramètre $filename est fourni, sinon dans le fichier original
   * Les métadonnées IPTC existantes du fichier sont écrasées par les nouvelles
   *
   * @param string $datas       données IPTC formatées
   * @param string $filename    emplacement de la copie du fichier original
   * @return 1 : OK
   *       -10 : fichier non initialisé
   *       -11 : erreur de création du contenu binaire (IPTC + contenu)
   *       -12 : erreur d'ouverture du fichier
   *       -13 : erreur d'écriture dans le fichier
   */
  private function save($datas, $filename="")
  {
    if ( $this->file ) {
      if ( !empty($filename) ) {
        $imageName = $filename;
      } else {
        $imageName = $this->file;
      }
      
      $data_image = @iptcembed($datas, $this->file);
      if ( !$data_image ) {
        return -11;
      }
      
      if ( $fp = @fopen($imageName, "wb") ) {
        if ( @fwrite($fp, $data_image) ) {
          return 1;
        } else {
          return -13;
        }
        @fclose($fp);
      } else {
        return -12;
      }
    } else {
      return -10;
    }
  }
  
}

?>
