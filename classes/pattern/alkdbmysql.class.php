<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkdb.class.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkerrormysql.class.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkDbMysql
 * @brief Classe de connexion à une base Mysql
 */
final class AlkDbMySql extends AlkDb
{
  /** Vrai valeur d'encodage du client MySql */
  protected $strMysqlEncoding;
  
  /**
   *  Constructeur de la classe db : initialisation des attributs de l'objet db
   *
   * @param strLogin Identifiant de l'utilisateur
   * @param strHost  Adresse IP du serveur de base de donnees
   * @param strPwd   Mot de passe
   * @param strDb    Nom de la base de donnees
   * @param strAlias Alias de la base de donnees
   * @param strPort  Numéro de port
   */
  public function __construct($strLogin,  $strHost,  $strPwd,  $strDb,  $strPort)
  {
     parent::__construct($strLogin, $strHost, $strPwd, $strDb, "", $strPort);
  }
  
  /**
   *  Destructeur de la classe db 
   */
  
  public function __destruct() 
  {
    $this->disconnect();	  
  }

  /**
   *  établit la connection avec la base de données
   */
  public function connect($strAlkSGBDEncoding=ALK_SGBD_ENCODING)
  {
    startErrorHandlerMysql();

    $strConn = $this->strHost;
    if($this->strPort !="" )
      $strConn = $this->strHost.":".$this->strPort;

    $this->conn = @mysql_connect($strConn, $this->strLogin, $this->strPwd, true);

    if( !$this->conn )
      trigger_error("Impossible de se connecter au serveur MySQL (".$this->strHost.")", E_USER_ERROR);

    if( !@mysql_select_db($this->strDb,$this->conn) )
      trigger_error("Impossible de selectionner la base MySQL (".$this->strDb.")", E_USER_ERROR);

    $tabEncoding = array("UTF-8" => "utf8", "ISO-8859-1" => "latin1");
    
    $this->strDbEncoding = $strAlkSGBDEncoding;
    $this->strMysqlEncoding = $tabEncoding[$this->strDbEncoding]; //@mysql_client_encoding($this->conn);
    
    //force l'encodage du client
    $strSql = "SET character_set_results = '".$this->strMysqlEncoding."',".
      " character_set_client = '".$this->strMysqlEncoding."',".
      " character_set_connection = '".$this->strMysqlEncoding."',".
      " character_set_database = '".$this->strMysqlEncoding."',".
      " character_set_server = '".$this->strMysqlEncoding."'";
    @mysql_query($strSql);
    
    // pas de possibilité pour modifier le client sauf recompiler libmysql pour l'encodage désiré

    endErrorHandlerMysql();
  }
  
  /**
   *  Déconnection avec la base de donnees
   */
  public function disconnect()
  { 
  }

  /**
   *  Caste des éléments de requête dans l'encodage sélectionné pour la base
   * @param strSql    Elément de requête
   * @return string SQL
   */
  private function castToEncoding($strSql)
  {
    if( $this->strMysqlEncoding == "" || $this->strMysqlEncoding == "utf8" ) 
      return $strSql;
    return "cast(".$strSql." as CHAR CHARACTER set ".$this->strMysqlEncoding.")";
  }
  
  /**
   *  Retourne le dataset correspondant à la requete strSql
   *        Capte l'affichage des eventuelles erreurs
   *
   * @param strSql        Requete SQL
   * @param idFirst       Indice de pagination : premier élément
   * @param idLast        Indice de pagination : dernier élément
   * @param bErr          Type de gestion d'erreur :
   *                        = false pour capter les erreurs
   *                        = true stop l'exécution sur erreur
   * @param iExpire       =0 par défaut, >0 pour mémoriser en cache le résutat de la requête avec un délai d'expiration de iExpire secondes
   * @param strCacheName  =alkanet par défaut, permet de regrouper les éléments cachés à l'aide de ce nom afin de gérer en live, la libération du cache 
   * @return Retourne un dataSet
   */
  public function initDataset($strSQL, $idFirst=0, $idLast=-1, $bErr=true, $iExpire=0, $strCacheName="alkanet")
  { 
    //echo $strSQL."<br>";
    $ds = null;
    if( $iExpire>0 && $strCacheName!="" ) {
      $ds = AlkFactory::memCacheGetData($strCacheName, $strSQL);
    }
    if( !is_object($ds) ) {
      $ds = new AlkDsMySql($this->conn, $strSQL, $idFirst, $idLast, $bErr, $this->strDbEncoding);
      if( $iExpire>0 && $strCacheName!="" ) {
        AlkFactory::memCacheSetData($strCacheName, $strSQL, $ds, $iExpire);
      }
    }
    return $ds;
  }

  /**
   * Execute une requête et retourne la valeur du premier champ du premier enregistrement
   * Si une erreur se produit ou qu'aucune valeur n'est trouvée, retourne defaultValue
   * @param strSql        Requête SQL
   * @param defaultValue  valeur retournée si erreur ou si aucune information trouvée
   * @return Retourne la valeur obtenue, 
   */
  public function getScalarSql($strSql, $defaultValue)
  {
    $oRes = $defaultValue;
    ob_start();
    $oDs = mysql_query($strSql, $this->conn);
    if( $oDs ) {
      $tabValues = mysql_fetch_array($oDs);
      if( is_array($tabValues) && !empty($tabValues) ) {
        $oRes = $tabValues[0]; 
      }
    } 
    ob_end_clean();
    return $oRes;
  }

  /**
   *  Exécute la requête SQL = insert, update, delete
   *
   * @param strSql Requête SQL
   * @param bErr   Type de gestion d'erreur :
   *                 = false pour capter les erreurs
   *                 = true stop l'exécution sur erreur
   * @return Retourne un entier : 0 si KO, 1 si OK
   */
  public function executeSql($strSQL , $bErr=true)
  { //echo $strSQL."<br>";
    if( $bErr == true ) 
      startErrorHandlerMysql();
    else
      ob_start();

    $boolret = mysql_query($strSQL, $this->conn);
    if( !$boolret ) {
      if( $bErr == true )
        trigger_error("Erreur dans la requete SQL (executeSql - ".$strSQL.")",E_USER_WARNING);
      else {
        $this->_SetError(mysql_error());
      }
    }
      
    if( $bErr == true )
      endErrorHandlerMysql();
    else
      ob_end_clean();
    return $boolret;
  }

  /**
   *  Retourne le code sql des instructions "show tables" et "show tables like "
   * 
   * @param strLikeTable    Si non vide permet de faire un show tables like 
   * @return string SQL
   */
  public function getShowTables($strLikeTable="")
  {
    $strSql = "show tables";
    if( $strLikeTable!="" )
      $strSql .= " like '".$strLikeTable."'";
    return $strSql;
  }

  /**
   * Retourne la description des colonnes d'une table
   * @param strTableName    Nom de la table
   * @return dataset
   */
  public function getDsTableColumns($strTableName)
  {
    $strSql = "desc " . $strTableName;
    return $this->initDataset($strSql);
  }
  
  /**
   *  Remplace les caractères spéciaux d'un champ texte d'une requete SQL
   *
   * @param strString Valeur du champ texte d'une requete
   * @param bHtmlVerif true par défaut pour éviter les attaques de type XSS, false pour éviter le filtre.
   * @return Retourne une chaine obtenue après traitement
   */
  public function analyseSql($strString, $bHtmlVerif=true)
  {
    return parent::analyseSql($strString, $bHtmlVerif, "\'");
  }
  
  /**
   *  Retourne une chaine de comparaison dans une requete SQL
   *
   * @param strField   Nom du champ dont la valeur est à tester
   * @param strCompare Opérateur de comparaison
   * @param strValeur  Valeur à comparer
   * @param strCaseOk  Valeur retournée si comparaison vraie
   * @param strCaseNok Valeur retournée si comparaison fausse
   * @return Retourne une chaine : l'expression SQL associée à la comparaison
   */
  public function compareSql($strField, $strCompare, $strValue, $strCaseOK, $strCaseNok)
  {
    if( !(is_string($strField) && is_string($strCompare) && 
          is_string($strValue) && is_string($strCaseOK) && is_string($strCaseNok)) ) {
      //$this->triggerError("Erreur dans la chaine (compareSql)", E_USER_WARNING);
      $varRetour = "";
    } else {
      $varRetour = " IF (".$strField." ".$strCompare." ".$strValue.", ".$strCaseOK.", ".$strCaseNok.")";
    }

    return $varRetour;
  }

  /**
   *  Obtenir le prochain identifiant à inserer dans la table strTable
   *
   * @param strTable    Nom de la table
   * @param strField    Nom du champ id
   * @param strSequence Nom de la sequence associée
   * @return Retourne un entier : le prochain id
   */
  public function getNextId($strTable, $strField, $strSequence="")
  {
    $id = 1;
    if( strtoUpper($strTable) == "SEQUENCE" || $strSequence!="" ) {
      $strSequence = ( $strSequence != ""
                       ? $strSequence
                       : $strField);

      // séquence Mysql 4
      $strSql = "UPDATE SEQUENCE set ".$strSequence."=LAST_INSERT_ID(".$strSequence."+1)";
      $this->executeSql($strSql);
  
      $id = mysql_insert_id($this->conn);
    } else {
      $strSQL = "select max(".$strField.") as idMax from ".$strTable;
      $ds = $this->initDataset($strSQL);
      if( $dr = $ds->getRowIter() )
        $id = $dr->getValueName("IDMAX");
      else
        $id = 0;
      $id++;
    }
    return $id;
  }                                                                          

  /**
   * Retourne l'instrcution SQL permettant d'obtenir le prochain identifiant à inserer dans la table strTable
   *
   * @param strTable    Nom de la table
   * @param strField    Nom du champ id
   * @param strSequence Nom de la sequence associée
   * @return Retourne une requete sql
   */
  public function getStrNextId($strTable, $strField, $strSequence="")        
  {
    $this->triggerError("Fonction non implémentée pour le SGBD MySql", E_USER_ERROR);
    $id = 1;
    $strPostSql = "";
    /*if( $this->strSchemaRead != "" ) {
      $strPostSql = "set search_path to ".$this->strSchemaRead."; ";
    }*/
    if( strToUpper($strTable) == "SEQUENCE" || $strSequence!="" ) {
      $strSequence = ( $strSequence != ""
                       ? $strSequence
                       : $strField);

      return $strSql = $strPostSql."select LAST_INSERT_ID(".$strSequence."+1) as id_Next";
    } else {
      return $strSql = $strPostSql."select max(".$strField.") as idMax from ".$strTable;
    }                         
  }
  

  /**
   *  Formate une date au format SQL da la base de donnees
   *
   * @param strFormat Format de la date passee en parametre
   * @param strDate   Valeur de la date équivalente au format ou dans son expression entière
   * @param bToDate   Identifie l'expression à retourner : 
   *                  = true  : l'expression retournée par la requete est une date (insertion)
   *                  = false : l'expression retournée par la requete est une chaine (extraction)
   *  @param bCastToInt =true  pour caster la transformation en entier si bToDate=false
   *                   =false pour laisser to_char() dans son type par défaut, si bToDate=false
   * @note Format : 
   *       - SS    : secondes
   *       - MI    : Minute
   *       - HH    : Heure du jour
   *       - D     : Numéro du jour dans la semaine
   *       - DAY   : Nom du jour
   *       - DD    : Numéro du jour dans le mois
   *       - DDD   : Numéro du jour dans l'année
   *       - IW    : Numéro de la semaine dans l'année (Norme iso)
   *       - WW    : Numéro de la semaine dans l'année
   *       - MM    : Numéro du mois 
   *       - MONTH : Nom du mois
   *       - YYYY  : année sur 4 chiffres
   *       - YY    : année sur 2 chiffres
   * @param Retourne une chaine : l'expression SQL associée
   */
  public function getDateFormat($strFormat,  $strDate, $bToDate=true, $bCastToInt=false)
  {
    startErrorHandlerMysql();

    $strRes = "NULL";
    if( !(is_string($strFormat) && is_string($strDate)) ) {

    } else
      if( $bToDate==false ) {
        if( $strFormat!="" && $strDate!="" && $strDate!="''" && strtolower($strDate)!="'null'" ) {
          $strTempFormat = $strFormat ;
          $strTempFormat = ereg_replace("SS","%S",$strTempFormat) ;
          $strTempFormat = ereg_replace("MI","%i",$strTempFormat) ;
          $strTempFormat = ereg_replace("HH","%H",$strTempFormat) ;
          $strTempFormat = ereg_replace("DDD","%j",$strTempFormat) ;
          $strTempFormat = ereg_replace("DD","%d",$strTempFormat) ;
          $strTempFormat = ereg_replace("DAY","%W",$strTempFormat) ;
          $strTempFormat = ereg_replace("D","%w",$strTempFormat) ;
          $strTempFormat = ereg_replace("WW","%u",$strTempFormat) ;
          $strTempFormat = ereg_replace("IW","%U",$strTempFormat) ;
          $strTempFormat = ereg_replace("MM","%m",$strTempFormat) ;
          $strTempFormat = ereg_replace("MONTH","%M",$strTempFormat) ;
          $strTempFormat = ereg_replace("YYYY","%Y",$strTempFormat) ;
          $strTempFormat = ereg_replace("YY","%y",$strTempFormat) ;
              
          $strRes = "DATE_FORMAT(".$strDate.",'".$strTempFormat."')" ;
        }
      } else {
        if( ( $strDate!="" && $strDate!="''" && strtolower($strDate)!="'null'") ) {
          if (substr($strDate, 0, 1)=="'")
            $strDate = substr($strDate, 1, -1);
          $tabDate = explode (" ",$strDate);
          $tabDate2 = explode ("/",$tabDate[0]);
          if( count($tabDate2)!=3 ) {
            trigger_error("Erreur format date (getDateFormat)", E_USER_WARNING);
          } else {
            $strRes = "";
            for ($i=2; $i>=0; $i--) {
              if ($i>0)
                $strRes .= $tabDate2[$i]."-";
              else
                $strRes .= $tabDate2[$i];
            }
            $strRes = "'".$strRes.(count($tabDate)>1?" ".$tabDate[1]:"")."'";
          }
        }
      }
    
    endErrorHandlerMysql();
    return $strRes;
  }
  
  /**
   * @brief Retourne le nombre de jour entre deux dates
   *
   * @param strDateFrom   Valeur de la date supérieure
   * @param strDateTo   Valeur de la date inférieure 
   * @param Retourne une chaine : l'expression SQL associée
   */
  public function getNbDaysBetween($strDateFrom, $strDateTo)
  {
    return " to_days(".$strDateFrom.")-to_days(".$strDateTo.")-1 ";
  }   
  
  /**
   *  Retourne l'expression SQL permettant d'additionner des intervalles de temps à une date
   *
   * @param strChamp    Nom du champ ou expression sql à traiter
   * @param iNb         Nombre d'intervalles à ajouter
   * @param strInterval Type d'intervalle : Y=année, M=mois, D=jour
   * @return Retourne une chaine : l'expression SQL associée
   */
  public function getDateAdd($strChamp, $iNb, $strInterval) 
  {
    $strRes = "";
    switch( $strInterval ) {
    case "Y": $strRes = "DATE_ADD(".$strChamp.", INTERVAL ".$iNb." YEAR)"; break;
    case "M": $strRes = "DATE_ADD(".$strChamp.", INTERVAL ".$iNb." MONTH)"; break;
    case "D": $strRes = "DATE_ADD(".$strChamp.", INTERVAL ".$iNb." DAY)"; break;
    case "H": $strRes = "DATE_ADD(".$strChamp.", INTERVAL ".$iNb." HOUR)"; break;
    }
    return $strRes;
  }

  /**
   *  Formate une date au format SQL da la base de donnees à partir d'un timestamp
   *
   * @param strFormat Format de la date à obtenir
   * @param timestamp Timestamp à transformer en date
   * @param bToDate   Identifie l'expression à retourner : 
   *                  = true  : l'expression retournée par la requete est une date (insertion)
   *                  = false : l'expression retournée par la requete est une chaine (extraction)   
   * @note Format : 
   *       - SS    : secondes
   *       - MI    : Minute
   *       - HH    : Heure du jour
   *       - D     : Numéro du jour dans la semaine
   *       - DAY   : Nom du jour
   *       - DD    : Numéro du jour dans le mois
   *       - DDD   : Numéro du jour dans l'année
   *       - IW    : Numéro de la semaine dans l'année (Norme iso)
   *       - WW    : Numéro de la semaine dans l'année
   *       - MM    : Numéro du mois 
   *       - MONTH : Nom du mois
   *       - YYYY  : année sur 4 chiffres
   *       - YY    : année sur 2 chiffres
   * @param Retourne une chaine : l'expression SQL associée
   */
  public function getDateFromTimestamp($strFormat,  $timestamp, $bToDate=true)
  {
    startErrorHandlerMysql();

    $strRes = "NULL";
    if( !(is_string($strFormat)) ) {

    } else
      if( $bToDate==false ) {
        if( $strFormat!="" && $timestamp!="" && $timestamp!="''" && strtolower($timestamp)!="'null'" ) {
          $strTempFormat = $strFormat ;
          $strTempFormat = ereg_replace("SS","%S",$strTempFormat) ;
          $strTempFormat = ereg_replace("MI","%i",$strTempFormat) ;
          $strTempFormat = ereg_replace("HH","%H",$strTempFormat) ;
          $strTempFormat = ereg_replace("DDD","%j",$strTempFormat) ;
          $strTempFormat = ereg_replace("DD","%d",$strTempFormat) ;
          $strTempFormat = ereg_replace("DAY","%W",$strTempFormat) ;
          $strTempFormat = ereg_replace("D","%w",$strTempFormat) ;
          $strTempFormat = ereg_replace("WW","%u",$strTempFormat) ;
          $strTempFormat = ereg_replace("IW","%U",$strTempFormat) ;
          $strTempFormat = ereg_replace("MM","%m",$strTempFormat) ;
          $strTempFormat = ereg_replace("MONTH","%M",$strTempFormat) ;
          $strTempFormat = ereg_replace("YYYY","%Y",$strTempFormat) ;
          $strTempFormat = ereg_replace("YY","%y",$strTempFormat) ;
              
          $strRes = "FROM_UNIXTIME(".$timestamp.",'".$strTempFormat."')" ;
        }
      } else {
        if( ( $timestamp!="" && $timestamp!="''" && strtolower($timestamp)!="'null'" ) ) {
          $strRes = "FROM_UNIXTIME(".$timestamp.")";
          }
        }
      
    
    endErrorHandlerMysql();
    return $strRes;
  }
    
  /**
   *  Retourne l'expression SQL qui fournit la concatenation d'un nombre indéfini de paramètres
   *
   * @return Retourne une chaine : l'expression SQL associée
   */
  public function getConcat()
  {
    $strRes = "";
    $nbParam = func_num_args();
    if( $nbParam>1  ) {
      $strRes = "concat(";
      for($i=0; $i<$nbParam; $i++) {
        $strParam = func_get_arg($i);
        $strRes .= $strParam.",";
      }
      if( mb_substr($strRes, -1) == "," )
        $strRes = mb_substr($strRes, 0, -1);
      $strRes .= ")";
    } else {
      $strRes = "null";
    }
    if( defined("ALK_B_NO_CAST_SQL") && ALK_B_NO_CAST_SQL==true )
      return $strRes;
    return $this->castToEncoding($strRes);
  }

  /**
   *  Retourne l'expression SQL qui fournit la concatenation récursive sur une colonne
   * @param strField      Colonne sur laquelle s'effectue la concaténation groupée
   * @param strSeparator  Chaine SQL donnant le séparateur
   * @param strOrder      Ordre de lecture des données (Mysql)
   * @param bDistinct     Indique si sélection des éléments distincts seulement
   * @param strFrom       nom utilisé pour Mysql
   * @param bNullTest     nom utilisé pour Mysql
   *
   * @return Retourne une chaine : l'expression SQL associée
   */
  public function getGroupConcat($strField, $strSeparator=",", $strOrder="", $bDistinct=false, $strFrom="", $bNullTest=true)
  {
    if ( $strFrom!="" ){
      $strRes = "(select group_concat(".
                    ($bDistinct ? "distinct " : "").
                    $strField.
                    " SEPARATOR ".$strSeparator.
                    ($strOrder!="" ? " ORDER BY ".$strOrder : "") .
                  ")".
                  $strFrom.
                ")";
    }
    else {
      $strRes = "group_concat(".
                  ($bDistinct ? "distinct " : "").
                  $strField.
                  ($strOrder!="" ? " ORDER BY ".$strOrder : "").
                  " SEPARATOR ".$strSeparator.
                ")";
    }
    if( defined("ALK_B_NO_CAST_SQL") && ALK_B_NO_CAST_SQL==true )
      return $strRes;
    return $this->castToEncoding($strRes);
  }
  
  /**
   *  Retourne l'expression SQL qui fournit la concatenation d'un nombre indéfini de paramètres
   *
   * @param strChamp Nom du champ ou expression sql à traiter
   * @param iPos     Position de départ (premier caractère = 0)
   * @param iLength  Longueur de la sous-chaine (facultatif)
   * @return Retourne une chaine : l'expression SQL associée
   */
  public function getSubstring($strChamp, $iPos, $iLength=-1)
  {
    if( $iLength == -1 )
      return $this->castToEncoding("substring(".$strChamp.", ".$iPos."+1)");
    return $this->castToEncoding("substring(".$strChamp.", ".$iPos."+1, ".$iLength.")");
  }
  
  /**
   *  Retourne l'expression SQL qui fournit la date-heure système
   *
   * @return Retourne une chaine : l'expression SQL associée
   */
  public function getDateCur( )
  {
    return "SYSDATE()";
  }

  /**
   *  Retourne l'expression SQL qui transforme en minuscules une expression
   *
   * @param strChamp Nom du champ ou expression sql à traiter
   * @return Retourne une chaine : l'expression SQL associée
   */
  public function getLowerCase($strChamp)
  {
    return $this->castToEncoding("lcase(".$strChamp.")");
  }

  /**
   *  Retourne l'expression SQL qui transforme en majuscules une expression
   *
   * @param strChamp Nom du champ ou expression sql à traiter
   * @return Retourne une chaine : l'expression SQL associée
   */
  public function getUpperCase($strChamp)
  {
    return $this->castToEncoding("ucase(".$strChamp.")");
  }
  
  /**
   *  Retourne l'expression SQL qui correspond à la fonction cast
   *
   * @param strValue Nom du champ ou expression sql à traiter
   * @param strType  Type du champ ou expression sql à traiter
   * @return Retourne une chaine : l'expression SQL associée
   */
  public function getCast($strValue, $strType)
  {
    return $strValue;
  }

  /**
   *  Retourne une chaine contenant la fonctionnalite Oracle de comparaison
   *        de chaine sans tenir compte des caracteres francais (accent, etc...)
   *
   * @param strChamp Nom du champ de la table
   * @param strOp    Operateur de test SQL : like, =
   * @param strVal   Chaine de comparaison qui doit etre traitee par ora_analyseSQL auparavant
   * @return Retourne la chaine après traitement
   */
  public function getStrConvert2ASCII7($strChamp, $strOp, $strVal)
  {
    //$strTmp = strtolower($strVal);
    //sttrTmp = strtr($strTmp, "eéèêëaäàâuüùûiîïôoöcç", "eeeeeaaaauuuuiiiooocc");
    $strRes = $this->castToEncoding("lcase(".$strChamp.")")." ".$strOp." ".$this->castToEncoding("lcase(".$strVal.")");
    return $strRes;
  }

  /**
   * @TODO
   *  Effectue un rollback pour annuler l'ensemble des requêtes exécutées sur la transaction en cours
   *        Ferme la transaction après le commit (bUseCommit = false)
   *
   * @return Retourne un bool : true si ok, false sinon
   */
  public function rollBackTransaction()
  { 
  	return mysql_query("ROLLBACK", $this->conn);
  } 

    /**
   * @TODO
   *  initialise une transaction sur la connexion courante
   *        Ouvre la transaction (bUseCommit = true)
   */
  public function initTransaction()
  {
  	$null = mysql_query("START TRANSACTION", $this->conn);
    return mysql_query("BEGIN", $this->conn);   
  } 
  
  /**
   * @TODO
   *  Effectue un commit sur l'ensemble des requêtes exécutées sur la transaction en cours
   *        Ferme la transaction après le commit (bUseCommit = false)
   *
   * @return Retourne un bool : true si ok, false sinon
   */
  public function commitTransaction()
  {	
    return mysql_query("COMMIT", $this->conn);
  } 
  
  
  
  /**
   * Traduit le type de colonne fourni dans la syntaxe du SGBD et y ajoute la longueur de champ si fournie
   * Retourne la traduction en cas de succès
   * Retourne false en cas d'erreur (longueur non fournie mais nécessaire, type incorrect, ...)
   *
   * @param columnType      Type de donnée de colonne
   * @param columnLength    Longueur max des données de la colonne
   * @param columnLength2   Longueur max des données de la colonne
   * @return string, retourne FALSE en cas d'erreur
   */
  public function getColumnType($columnType, $columnLength="", $columnLength2="")
  {
    $strRes = "";
    $columnType = mb_strtoupper($columnType);
    if ( $columnLength2!="" )
      $columnLength2 = ", ".$columnLength2;
       
    switch ( $columnType ){
      case "INT" :
        $strRes = "int";
        if ( $columnLength!="" ){
          $strRes .= "(".$columnLength.")";
        }
      break;
      
      case "BIGINT" :
        $strRes = "bigint";
      break;
           
      case "TEXT":
        $strRes = "text";
      break;
      
      case "VARCHAR":
        if ( $columnLength!="" ){
          $strRes = "varchar(".$columnLength.")";
        }
      break;
      
      case "FLOAT":
        if ( $columnLength!="" ){
          $strRes = "float(".$columnLength.$columnLength2.")";
        }
      break;
      
      case "DATETIME":
        $strRes = "datetime";
      break;                          
    }
    if ( $strRes!="" )
      return $strRes;
    return false;
  }
  
  /**
   * Retourne un tableau associatif fournissant les types de données des champs
   * Le tableau contient les clés suivantes : int, datetime, varchar, float
   * @return array 
   */
  protected function getSqlType()
  {
    return array("int"      => "int", 
                 "bigint"   => "bigint",
                 "varchar"  => "varchar", 
                 "datetime" => "datetime", 
                 "float"    => "float",
                 "text"     => "text"); 
  }
  
  /**
   * Retourne le code SQL permettant de créer une table (uniquement les champs typés)
   * @param strTableName  nom de la table ou tableau
   * @param tabFields     tableau contenant les informations sur les champs à créer
   * @return string
   */
  public function getSqlCreateTable($strTableName, $tabDataFields)
  {
    if ( empty($tabDataFields) ) return "";
    $tabTypeAssoc = $this->getSqlType();
    $strSql = "create table ".mb_strtoupper($strTableName)." (";
    foreach($tabDataFields as $strFieldName => $tabData) {
      $tabType = explode("(", $tabData["type"]);
      $strType = mb_strtolower($tabType[0]);
      if( array_key_exists($strType, $tabTypeAssoc) ) {
        $strType = mb_strtoupper($tabTypeAssoc[$strType]);
      } else {
        $this->triggerError("Type SGBD non reconnu. ".$strTableName.".".$strFieldName." non ajouté.", E_USER_ERROR);
        continue; 
      }
      if( count($tabType)>1 ) {
        $strType .= "(".$tabType[1]; 
      }
      $strSql .= mb_strtoupper($strFieldName)." ".$strType. " ".$tabData["dn"].","; 
    }
    $strSql = mb_substr($strSql, 0, -1).") type=innoDB";
    return $strSql;
  }
  
  /**
   * Retourne le code Sql permettant de supprimer une clé primaire à une table
   * @param strTableName  nom de la table
   * @param strPkName     nom de la clé primaire
   * @return string
   */
  public function getSqlDropPrimary($strTableName, $strPkName)
  {
    return "alter table ".mb_strtoupper($strTableName).
      " drop primary key ";
  }
  
  /**
   * Retourne le code SQL permettant de créer un index sur un champ d'une table
   * @param strTableName   nom de la table
   * @param strIndexName   nom de l'index
   * @param strFieldName   nom du champ
   * @return string
   */
  public function getSqlCreateIndex($strTableName, $strIndexName, $strFieldName)
  {
    return "create index ".mb_strtoupper($strIndexName).
      " on ".mb_strtoupper($strTableName)." (".mb_strtoupper($strFieldName)." asc)";
  }
  
  /**
   * Retourne le code SQL permettant de supprimer un index sur un champ d'une table
   * @param strTableName   nom de la table
   * @param strIndexName   nom de l'index
   * @return string
   */
  public function getSqlDropIndex($strTableName, $strIndexName)
  {
    return "alter table ".mb_strtoupper($strTableName).
      " drop index ".mb_strtoupper($strIndexName);
  }

  /**
   * Retourne le code SQL permettant d'ajouter une clé étrangère à une table
   * @param strTableName   nom de la table locale
   * @param strFkName      nom de la clé étrangère
   * @param strFieldFk     nom du champ local
   * @param strTablePk     nom de la table cible 
   * @param strFieldPk     nom du champ cible
   * @param strOption      option complémentaire
   * @return string
   */
  public function getSqlAddConstraintForeignKey($strTableName, $strFkName, $strFieldFk, $strTablePk, $strFieldPk, $strOption="")
  {
    return "alter table ".mb_strtoupper($strTableName).
      " add constraint ".mb_strtoupper($strFkName)." foreign key (".mb_strtoupper($strFieldFk).")".
      " references ".mb_strtoupper($strTablePk)." (".mb_strtoupper($strFieldPk).") ".$strOption;
  }
 
  /**
   * Retourne le code SQL permettant de supprimer une clé étrangère à une table
   * @param strTableName   nom de la table locale
   * @param strFkName      nom de la clé étrangère
   * @return string
   */
  public function getSqlDropConstraintForeignKey($strTableName, $strFkName)
  {
    return "alter table ".mb_strtoupper($strTableName).
      " drop foreign key ".mb_strtoupper($strFkName);
  }
  
  /**
   * Retourne le code SQL permettant de supprimer une contrainte d'unicité à une table
   * @param strTableName   nom de la table
   * @param strUqName      nom de la contrainte
   * @param strFieldList   liste des champs caractérisant l'unicité
   * @return string
   */
  public function getSqlDropConstraintUnique($strTableName, $strUqName)
  {
    return $this->getSqlDropIndex($strTableName, $strUqName);
  }
  
  /**
   * Retourne le code SQL permettant de créer une séquence
   * @param strSeqName   nom de la séquence
   * @param iStart       indice de début de la séquence
   */  
  public function getSqlCreateSequence($strSeqName, $iStart)
  {
    return "alter table SEQUENCE add ".mb_strtoupper($strSeqName)." int default ".$iStart;
  }
  
  /**
   * Retourne le code SQL permettant de supprimer une séquence
   * @param strSeqName   nom de la séquence
   * @return string
   */  
  public function getSqlDropSequence($strSeqName)
  {
    return "alter table SEQUENCE drop column ".mb_strtoupper($strSeqName);
  }
  
  /**
   * Effectue un alter table modify column
   * Retourne une chaine vide si erreur
   *
   * @param strTableName        Nom de la table
   * @param strColumnName       Nom actuel de la colonne
   * @param strNewColumnName    Nouveau nom de la colonne, =strColumnName si vide
   * @param strNewColumnType    Nouveau type de la colonne
   * @param strNewColumnLength  Nouvelle longueur de la colonne
   * @param strNewColumnDefault Nouvelle valeur par défaut de la colonne, = "" pour ne rien fait
   * @param iNewColumnNullable  Nouvel état nullable de la colonne (=0 : NOT NULL, =1 : NULL, =-1 no change)
   * @return string
   */
  public function getSqlAlterTableUpdateColumn($strTableName, $strColumnName, $strNewColumnName, $strNewColumnType="", 
                                               $strNewColumnLength="", $strNewColumnDefault="", $iNewColumnNullable=-1)
  {
    if( $strNewColumnName == "" ) {
      $strNewColumnName = $strColumnName;
    }
    if( $strTableName=="" || $strColumnName=="" ) 
      return "";
      
    $strSql = "alter table ".mb_strtoupper($strTableName).
      " change ".mb_strtoupper($strColumnName)." ".mb_strtoupper($strNewColumnName);
    
    if( $strNewColumnType!="" ) {
      $strType = $this->getColumnType($strNewColumnType, $strNewColumnLength);
      if ( $strType!==false ) {
        $strSql .= " ".$strType;
      } else {
        return "";
      }
    }
    $strSql .= 
      ( $strNewColumnDefault!=""
        ? " default ".$strNewColumnDefault
        : "" ).
      ( $iNewColumnNullable!=-1 
        ? ( $iNewColumnNullable==0 
            ? " not null" 
            : " null")
        : "" );
    
    return $strSql;
  }

  /**
   * Construit la requête sql qui recopie la ligne d'une table vers une ligne d'une autre
   * Retourne la requête sql générée
   * @param strTableSrc     Nom de la table source
   * @param strTablsDest    Nom de la table destination
   * @param tabFieldPkSrc   tableau associatif : cle = nom du champ clé primaire de la table source, valeur = valeur de cette cle
   * @param tabFieldPkDest  tableau associatif : cle = nom du champ clé primaire de la table destination, valeur = valeur de cette cle
   * @param tabFieldsName   tableau associatif : cle = nom du champ destination (sans alias de table en début), 
   *                                             valeur = nom du champ source avec alias de table égale à "s." 
   *                                                    ou valeur spécifique
   *                                                    ou vide pour reprendre le même nom de colonne que la source
   * @return string
   */
  public function getSqlCopyRowFromTableToTable($strTableSrc, $strTableDest, $tabFieldPkSrc, $tabFieldPkDest, $tabFieldsName)
  {
    
    $strCopy = "";
    foreach($tabFieldsName as $strFieldNameDest => $strValueSrc ) {
      $strCopy .= ( $strCopy == "" ? "" : ", " ).
        " d.".$strFieldNameDest."=".
        ( $strValueSrc == "" 
          ? "s.".$strFieldNameDest
          : $strValueSrc ); 
    }
    
    $strWhere = "";
    foreach($tabFieldPkSrc as $strFielPkName => $FielPkValue) {
      $strWhere .= ( $strWhere == "" ? "" : " and " )." s.".$strFielPkName."=".$FielPkValue;
    }
    foreach($tabFieldPkDest as $strFielPkName => $FielPkValue) {
      $strWhere .= ( $strWhere == "" ? "" : " and " )." d.".$strFielPkName."=".$FielPkValue;
    }

    $strSql = "update ".$strTableSrc." s, ".$strTableDest." d set ".
      $strCopy.
      ( $strWhere != "" ? " where ".$strWhere : "" );
    
    return $strSql;
  }
}
?>