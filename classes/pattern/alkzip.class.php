<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

/**
 * @brief Construit un fichier zip puis l'enregistre ou le retourne sur la sortie standard
 *
 * @param tabFileToZip  Tableau contenant les noms de fichiers à placer dans le zip (avec le chemin complet)
 * @param strFileZip    Nom du fichier zip à créer (sans le chemin, sans l'extension zip)
 * @param bDownload     Vrai = le zip est présenté en téléchargement (par défaut), faux = le zip est enregistré dans le répertoire $strPathSave
 * @param strPathSave   Chemin complet ou est enregistré le fichier zip dans le cas bDownLoad=false (= vide par défaut)
 * @param tabZipDirectories Tableau contenant les répertoires où placer les fichiers dans le Zip (même taille que tabFileToZip)
 */
function ExportZip(&$tabFileToZip, $strFileZip, $bDownload=true, $strPathSave="", $tabZipDirectories=array())
{
  $strFileZip .= ".zip";

  $zipfile = new AlkZipFile($strPathSave.$strFileZip);
  
  foreach ($tabFileToZip as $strPathFileName ) {
  	$strPosSlash=strrpos($strPathFileName,"/");
    if($strPosSlash==false){
    	$strFileName = $strPathFileName;
    }
    else
    {
    	$strFileName = substr($strPathFileName,$strPosSlash+1);
    }
    $indice=array_keys($tabFileToZip, $strPathFileName);
    if (isset($tabZipDirectories[$indice[0]])){
    	$strFileName = $tabZipDirectories[$indice[0]].$strFileName;
    }
    
    $zipfile->addFile($strPathFileName, $strFileName);
  }

  $zipfile->close();
  
  if( $bDownload == true ) {
    $mime_type = 'application/x-zip';
    header("Content-Type: ".$mime_type);
    header("Expires: ".gmdate("D, d M Y H:i:s")." GMT");
    // lem9 & loic1: IE need specific headers
    if (STRNAVIGATOR == NAV_IE) {
      header("content-type: application/octet-stream");
      header("Content-Disposition: inline; filename=".$strFileZip."");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Pragma: public");
    } else {
      header("content-type: application/octet-stream");
      header("Content-Disposition: attachment; filename=".$strFileZip."");
      header("Pragma: no-cache");
    }
    readfile($strPathSave.$strFileZip);
    exit();    
  } 
	return $strFileZip;  
}


/**
 * @brief Construit un fichier zip puis l'enregistre ou le retourne sur la sortie standard
 *
 * @param tabFileToZip  Tableau contenant les noms de fichiers à placer dans le zip (avec le chemin complet)
 * @param strFileZip    Nom du fichier zip à créer (sans le chemin, sans l'extension zip)
 * @param bDownload     Vrai = le zip est présenté en téléchargement (par défaut), faux = le zip est enregistré dans le répertoire $strPathSave
 * @param strPathSave   Chemin complet ou est enregistré le fichier zip dans le cas bDownLoad=false (= vide par défaut)
 * @param tabZipDirectories Tableau contenant les répertoires où placer les fichiers dans le Zip (même taille que tabFileToZip)
 */
function ExportZipOld(&$tabFileToZip, $strFileZip, $bDownload=true, $strPathSave="", $tabZipDirectories=array())
{
  $strFileZip .= ".zip";

  $zipfile = new AlkZipFile($strPathSave.$strFileZip);
  
  foreach ($tabFileToZip as $strPathFileName ) {
  	$file = fopen($strPathFileName,"r");
    $fileContenu = fread($file,filesize($strPathFileName));
    $strPosSlash=strrpos($strPathFileName,"/");
    if($strPosSlash==false){
    	$strFileName = $strPathFileName;
    }
    else
    {
    	$strFileName = substr($strPathFileName,$strPosSlash+1);
    }
    $indice=array_keys($tabFileToZip, $strPathFileName);
    if (isset($tabZipDirectories[$indice[0]])){
    	$strFileName = $tabZipDirectories[$indice[0]].$strFileName;
    }
    
    $zipfile->addFile($fileContenu, $strFileName);
  }
      
  return;
  $dump_buffer = $zipfile->file();
  $iSize = strlen($dump_buffer);

  if( $bDownload == true ) {
    $mime_type = 'application/x-zip';
    header("Content-Type: ".$mime_type);
    header("Expires: ".gmdate("D, d M Y H:i:s")." GMT");
    // lem9 & loic1: IE need specific headers
    if (STRNAVIGATOR == NAV_IE) {
      header("content-type: application/octet-stream");
      header("Content-Disposition: inline; filename=".$strFileZip."");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Pragma: public");
    } else {
      header("content-type: application/octet-stream");
      header("Content-Disposition: attachment; filename=".$strFileZip."");
      header("Pragma: no-cache");
    }
    echo $dump_buffer;
  }/* else {
    $hFile = fopen($strPathSave.$strFileZip);
    fwrite($hFile, $dump_buffer);
    fclose($hFile);
  }  */  
}

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");
/**
 * @class AlkZipFile
 * @brief Permet de zipper un ou plusieurs fichiers
 *        Ne s'utilise pas directement. Utiliser la fonction ExportZip.
 */
class AlkZipFile extends AlkObject
{
  /** Array to store compressed data  */
  var $datasec;

  /** Central directory */
  var $ctrl_dir;

  /** End of central directory record */
  var $eof_ctrl_dir;

  /** Last offset position */
  var $old_offset;
    
  /** Length of data send to standard output */
  var $output_data_length;
    
  /**
   * @brief constructeur par défaut
   *
   */
  function AlkZipFile($strFileZip)
  {
    $this->strFileZip = $strFileZip;
    $this->zip = new ZipArchive();
    $this->zip->open($this->strFileZip, ZIPARCHIVE::OVERWRITE);
        
    $this->datasec  = array();    
    $this->ctrl_dir = array();
    /*
    $this->eof_ctrl_dir = "\x50\x4b\x05\x06\x00\x00\x00\x00";
    $this->old_offset   = 0;
    $this->output_data_length = 0;*/
  }

  /**
   * @brief Convertit un timestamp unix en date DOS sur 4 octets
   *        Retourne la date au format DOS 4 octets
   *
   * @param  integer  the current Unix timestamp
   * @return Retourne un int
   */
  function unix2DosTime($unixtime = 0) 
  {
    $timearray = ($unixtime == 0) ? getdate() : getdate($unixtime);
    
    if( $timearray['year'] < 1980) {
      $timearray['year']    = 1980;
      $timearray['mon']     = 1;
      $timearray['mday']    = 1;
      $timearray['hours']   = 0;
      $timearray['minutes'] = 0;
      $timearray['seconds'] = 0;
    }

    return (($timearray['year'] - 1980) << 25) | ($timearray['mon'] << 21) | ($timearray['mday'] << 16) |
      ($timearray['hours'] << 11) | ($timearray['minutes'] << 5) | ($timearray['seconds'] >> 1);
  }

  /**
   * @brief Ajoute un fichier au zip
   *
   * @param data            Contenu du fichier à ajouter
   * @param name            Nom du fichier dans le zip (peut contenir un chemin)
   * @param time            Timestamp courant
   * @param bOutputDataNow  =false par défaut, =true pour retourner le contenu zippé sur la sortie standard
   */
  function addFile($strPathFile, $strFileName)
  {
    $this->zip->addFile($strPathFile, $strFileName);
  }

  /**
   * @brief Ajoute un fichier au zip
   *
   * @param data            Contenu du fichier à ajouter
   * @param name            Nom du fichier dans le zip (peut contenir un chemin)
   * @param time            Timestamp courant
   * @param bOutputDataNow  =false par défaut, =true pour retourner le contenu zippé sur la sortie standard
   */
  function close()
  {
    $this->zip->close();
  }

  /**
   * @brief Ajoute un fichier au zip
   *
   * @param data            Contenu du fichier à ajouter
   * @param name            Nom du fichier dans le zip (peut contenir un chemin)
   * @param time            Timestamp courant
   * @param bOutputDataNow  =false par défaut, =true pour retourner le contenu zippé sur la sortie standard
   */
  function _addFile($data, $name, $time = 0, $bOutputDataNow = false)
  {
    
    //ini_set("memory_limit", "64M");
    $bEmpty = ($name=="" && $data=="");
    $name     = str_replace('\\', '/', $name);

    $dtime    = dechex($this->unix2DosTime($time));
    $hexdtime = '\x' . $dtime[6] . $dtime[7]
      . '\x' . $dtime[4] . $dtime[5]
      . '\x' . $dtime[2] . $dtime[3]
      . '\x' . $dtime[0] . $dtime[1];
    eval('$hexdtime = "' . $hexdtime . '";');

    $length_write = 0;
    $fr   = "\x50\x4b\x03\x04";
    $fr   .= "\x14\x00";            // ver needed to extract
    $fr   .= "\x00\x00";            // gen purpose bit flag
    $fr   .= "\x08\x00";            // compression method
    $fr   .= $hexdtime;             // last mod time and date
    if ($bOutputDataNow) {
      $length_write += strlen($fr);
      echo $fr;
      flush();
      $fr = "";
    }
    
    // "local file header" segment
    $unc_len = strlen($data);
    $crc     = crc32($data);
    $zdata   = gzcompress($data);
    $zdata   = substr(substr($zdata, 0, strlen($zdata) - 4), 2); // fix crc bug
    $c_len   = strlen($zdata);
    
    $fr      .= pack('V', $crc);             // crc32
    $fr      .= pack('V', $c_len);           // compressed filesize
    $fr      .= pack('V', $unc_len);         // uncompressed filesize
    $fr      .= pack('v', strlen($name));    // length of filename
    $fr      .= pack('v', 0);                // extra field length
    $fr      .= $name;
    if ($bOutputDataNow) {
      $length_write += strlen($fr);
      echo $fr;
      flush();
      $fr = "";
    }
    
    // "file data" segment
    $fr .= $zdata;
    if ($bOutputDataNow) {
      $length_write += strlen($fr);
      echo $fr;
      flush();
      $fr = "";
    }

    // "data descriptor" segment (optional but necessary if archive is not
    // served as file)
    $fr .= pack('V', $crc);                 // crc32
    $fr .= pack('V', $c_len);               // compressed filesize
    $fr .= pack('V', $unc_len);             // uncompressed filesize
    
    if ($bOutputDataNow) {
      $length_write += strlen($fr);
      echo $fr;
      flush();
      $fr = "";
    }
        
    // add this entry to array
    if (!$bOutputDataNow) $this -> datasec[] = $fr;
    
    // now add to central directory record
    $cdrec = "\x50\x4b\x01\x02";
    $cdrec .= "\x00\x00";                // version made by
    $cdrec .= "\x14\x00";                // version needed to extract
    $cdrec .= "\x00\x00";                // gen purpose bit flag
    $cdrec .= "\x08\x00";                // compression method
    $cdrec .= $hexdtime;                 // last mod time & date
    $cdrec .= pack('V', $crc);           // crc32
    $cdrec .= pack('V', $c_len);         // compressed filesize
    $cdrec .= pack('V', $unc_len);       // uncompressed filesize
    $cdrec .= pack('v', strlen($name) ); // length of filename
    $cdrec .= pack('v', 0 );             // extra field length
    $cdrec .= pack('v', 0 );             // file comment length
    $cdrec .= pack('v', 0 );             // disk number start
    $cdrec .= pack('v', 0 );             // internal file attributes
    $cdrec .= pack('V', 32 );            // external file attributes - 'archive' bit set

    $cdrec .= pack('V', $this -> old_offset ); // relative offset of local header
    if (!$bEmpty) $this -> old_offset += ($bOutputDataNow ? $length_write : strlen($fr));
    $this->output_data_length += $length_write; 
    
    $cdrec .= $name;

    // optional extra field, file comment goes here
    // save to central directory
    if (!$bEmpty) $this -> ctrl_dir[] = $cdrec;
  }
  
  /**
   * @brief Retourne le contenu du fichier zippé
   *
   * @return Retourne un string
   */
  function file()
  {  	
  	$this->close();
  	return file_get_contents($this->strFileZip);
    /* 16/06/09 suppression de l'ancien code
    $data    = implode('', $this -> datasec);
    $ctrldir = implode('', $this -> ctrl_dir);

    return $data .
      $ctrldir .
      $this -> eof_ctrl_dir .
      pack('v', sizeof($this -> ctrl_dir)) .  // total # of entries "on this disk"
      pack('v', sizeof($this -> ctrl_dir)) .  // total # of entries overall
      pack('V', strlen($ctrldir)) .           // size of central dir
      pack('V', $this -> output_data_length + strlen($data)) .  // offset to start of central dir
      "\x00\x00";                             // .zip file comment length
      */
  }

}
?>