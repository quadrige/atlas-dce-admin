<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

/**
 * @package Alkanet_Class_Pattern
 * @class AlkImportErreur
 * 
 * Classe d'erreurs de base pour l'import
 */
class AlkImportError extends AlkObject
{
	/** Emplacement  du fichier d'Erreur' */
	protected $strFichier_Erreur;
	
	
	/**
	 * Constructeur de la classe ImportErreur : initialisation des attributs de ImportErreur.
	 *
	 * @param unknown_type $strFichier_Erreur  Emplacement du fichier d'import
	 */
	public function __construct( $strFichier_Erreur )
	{
	  parent::__construct();
	  
	  $this->AlkImportError($strFichier_Erreur);
	}
	
	/**
   * Constructeur de la classe ImportErreur : initialisation des attributs de ImportErreur.
   *
   * @param strFichier_Erreur Emplacement du fichier d'import
   */
	public function AlkImportError( $strFichier_Erreur )
	{
		$this->strFichier_Erreur = $strFichier_Erreur;
	}
	
	/**
	 * Fonction de gestion d'erreur.
	 *
	 * @param unknown_type $numErreur
	 * @param unknown_type $strMesErreur
	 * @return int
	 */
	public function erreur($numErreur, $strMesErreur="" )
	{
		// Ouverture du fichier en mode append
		$ferreur = @fopen($this->strFichier_Erreur, "a");
		$iRes = 0;	
		if($ferreur) {
			
			switch ($numErreur) {
      case 1 :
        $strMessage = "Erreur : Le fichier d'import est introuvable.";
        break;
      case 2 :
        $strMessage = "Erreur : Colonne ".$strMesErreur." n'est pas au bon endroit verifier l'indice.";
				break;
      case 3 :
				$strMessage = "Erreur : Colonne ".$strMesErreur." est obligatoire.";
				break;
      case 4 :
				$strMessage = "Erreur : Nom de la colonne ".$strMesErreur." incorrecte.";
				break;
      case 5 :
				$strMessage = "Erreur : Ligne ".$strMesErreur." : Le nombre de colonne ne correspond pas aux colonnes de référence.";
				break;
      case 6 :
				$strMessage = "Erreur : Fichier d'import non intégré.";
				break;
      case 7 :
				$strMessage = $strMesErreur;
				break;
      case 8 :
				$strMessage = $strMesErreur." La valeur de la colonne MODE doit être S ou M ou C." ;
				break;
			}

			@fwrite($ferreur, mb_convert_encoding($strMessage."\r\n", ALK_EXPORT_ENCODING, ALK_HTML_ENCODING));
			$iRes = 1;
		}
		
		@fclose($ferreur);
		return $iRes;  
	}
}
?>