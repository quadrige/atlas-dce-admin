<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


/**
 * @package Alkanet_Class_Pattern
 * 
 * @interface AlkIntRights
 * @brief Interface nécessaire pour la gestion des utilisateurs / profils / droits et privilèges
 */
interface AlkIntRights
{
  /**
   *  Transfère de propriété d'un utilisateur à un autre
   *        Méthode à appeler avant suppression définitive d'un utilisateur
   * @param idUserFrom  identifiant de l'utilisateur perdant la propriété des ses données
   * @param idUserTo    identifiant de l'utilisateur récupérant la propriété des données
   */
  public function replaceUser($idUserFrom, $idUserTo);

  /**
   *  Invitation d'un utilisateur à un espace (tous si cont_id=-1, tous les espaces publics si cont_id=-2)
   * @param user_id  identifiant d'un utilisateur
   * @param cont_id  identifiant de l'espace. -1 pour tous les espaces. -2 tous les espaces publics
   * @param priv_id  identifiant du privilège
   */
  public function addUserToSpace($user_id, $cont_id, $priv_id);

  /**
   *  Modification du privilège espace pour l'utilisateur à un espace (tous si cont_id=-1)
   * @param user_id  identifiant d'un utilisateur
   * @param cont_id  identifiant de l'espace. -1 pour tous les espaces.
   * @param priv_id  identifiant du privilège
   */
  public function updateUserPrivToSpace($user_id, $cont_id, $priv_id);

  /**
   *  Suppression de l'accès à un espace (ou tous si cont_id=-1) pour un utilisateur
   * @param user_id  identifiant d'un utilisateur
   * @param cont_id  identifiant de l'espace. -1 pour tous les espaces.
   */
  public function removeUserFromSpace($user_id, $cont_id);

  /**
   *  Personnalisation des droits d'un utilisateur
   * @note Appelé en admin. annuaire avec gestion des droits sur un profil sur l'espace courant
   * @param cont_id       identifiant de l'espace
   * @param user_id       identifiant de l'utilisateur sélectionné
   * @param iRight        non utilisé
   * @param iDefaultRight non utilisé
   */
  public function setUserRightsOnSpace($cont_id, $user_id, $iRight=-1, $iDefaultRight=-1);

  /**
   *  Enregistre les droits des profils sur les applications d'un espace
   * @param cont_id       identifiant d'utilisateur
   * @param tabRight      non utilisé dans le cas espace, 
   *                      sinon tableau de type "_".idProfil => droid_id pour les autres applis
   */
  public function setProfilRightsOnSpace($cont_id, $tabRight=array());

  /**
   *  Application de droits sur les profils pour l'application
   * @note Appelé en config. espace avec gestion des droits sur les profils pour l'appli
   * @param atype_id  type d'application
   * @param appli_id  identifiant de l'application
   */
  public function setRightsOnAppli($atype_id, $appli_id);

  /** 
   *  Ajout d'un profil sur l'extranet.
   *        Si l'application gère des droits, il faut y associer le droit NONE
   * @param profil_id  identifiant du profil à supprimer
   * @param cont_id    identifiant de l'espace associé, =0 pour tous les espaces (par défaut)
   */
  public function addProfil($profil_id, $cont_id=0);

  /** 
   *  Suppression d'un profil sur l'extranet.
   *        Si l'application gère des droits, il faut supprimer les droits associés
   * @param profil_id  identifiant du profil à supprimer
   */
  public function delProfil($profil_id);
  
   /**
   * calcule les droits de l'agent
   */
  public function setProfilRightToUser($agent_id);

}

?>