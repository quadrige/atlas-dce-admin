<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkdsldap.class.php");

if( !defined("ALK_LDAPQUERY_SEARCH") ) {
  define("ALK_LDAPQUERY_SEARCH", 0);
  define("ALK_LDAPQUERY_READ",   1);
  define("ALK_LDAPQUERY_LIST",   2);  
}

/*
define ('ADLDAP_NORMAL_ACCOUNT', 805306368);
define ('ADLDAP_WORKSTATION_TRUST', 805306369);
define ('ADLDAP_INTERDOMAIN_TRUST', 805306370);
define ('ADLDAP_SECURITY_GLOBAL_GROUP', 268435456);
define ('ADLDAP_DISTRIBUTION_GROUP', 268435457);
define ('ADLDAP_SECURITY_LOCAL_GROUP', 536870912);
define ('ADLDAP_DISTRIBUTION_LOCAL_GROUP', 536870913);*/

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkDbLDAP
 * @brief Classe de connexion LDAP
 */
class AlkDbLDAP extends AlkObject
{
  /** Adresse du serveur LDAP */
  protected $strHost;

  /** Numéro de port = 389 par défaut */
  protected $strPort;

  /** Chemin de base LDAP */
  protected $strBaseDN; 

  /** Identifiant de l'utilisateur */
  protected $strUserDN;

  /** Suffixe eventuel d'un DN */
  protected $strAccountSuffix;

  /** Mot de passe utilisateur */
  protected $strUserPwd;

  /** Ressource de la connexion */
  public $conn;

  /** Vrai si connecté et authentifié, faux sinon */
  protected $bBind;

  /** nombre max de résultat d'une requête, 4096 par défaut */
  protected $iSizeLimit;
  
  /** temps max d'une requête en seconde, 30s par défaut */
  protected $iTimeLimit;

  /** Spécifie le nombre d'alias qui doivent être gérés pendant la recherche. Il peut être un parmi les suivants :
   * LDAP_DEREF_NEVER - (défaut) les alias ne sont jamais déréférencés.
   * LDAP_DEREF_SEARCHING - les alias doivent être déréférencés pendant la recherche mais pas lors de la localisation de l'objet de base de la recherche.
   * LDAP_DEREF_FINDING - les alias doivent être déréférencés lors de la localisation de l'objet de base mais pas durant la recherche.
   * LDAP_DEREF_ALWAYS - les alias doivent toujours être déréférencés.
   */
  protected $deref;

  /**
   *  Constructeur par défaut
   *
   * @param strHost    Adresse du serveur LDAP
   * @param strPort    Numéro de port LDAP, =389 par défaut
   * @param strBaseDn  Chemin de base LDAP, chaine vide par défaut   
   */
  public function __construct($strHost, $strPort, $strBaseDn, $strUserDN, $strUserPwd, $strAccountSuffix="")
  {
    $this->strHost    = $strHost;
    $this->strPort    = $strPort;
    $this->strBaseDN  = $strBaseDn;

    $this->strUserDN        = $strUserDN;
    $this->strUserPwd       = $strUserPwd;
    $this->strAccountSuffix = $strAccountSuffix;

    $this->conn = @ldap_connect($strHost, $strPort);
    $this->bBind = false;
  
    $this->iSizeLimit = 4096;
    $this->iTimeLimit = 30;
    $this->deref = LDAP_DEREF_NEVER;

    if( $this->conn ) {
      @ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
      @ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0); //disable plain text passwords
    }
  }

  /**
   *  Desctructeur de la classe : ferme la connexion
   */  
  public function __destruct()
  {
    $this->disconnect(); 
  }

  /**
   *  Ouvre une connexion ldap, retourne vrai si ok, faux sinon
   * @return boolean
   */
  public function connect()
  {
    if( $this->conn ) {
      $strDn = $this->strUserDN.$this->strAccountSuffix;
      $this->bBind = @ldap_bind($this->conn, $strDn, $this->strUserPwd);
    }
    return $this->bBind;
  }

  /**
   *  Déconnexion ldap
   * @return boolean
   */
  public function disconnect()
  {
    if( $this->bBind ) 
      @ldap_close($this->conn); 
    $this->bBind = false;
  }
   
  /**
   * Fixe le nombre de résultat d'une requête ldap
   * @param iSizeLimit  nombre de résultat max d'une requête ldap, =4096 par défaut
   */ 
  public function setSizeLimit($iSizeLimit=4096)
  {
    $this->iSizeLimit = $iSizeLimit;
  }  
  
  /**
   * Fixe le temps limite d'une requete ldap
   * @param iTimeLimit  temps en seconde, =30s par défaut
   */
  public function setTimeLimit($iTimeLimit=30)
  {
    $this->iTimeLimit = $iTimeLimit;
  }
  
  /**
   * @param iTypeQuery  type de requête ldap : ALK_LDAPQUERY_SEARCH ou ALK_LDAPQUERY_READ, ALK_LDAPQUERY_LIST
   * @param strFilter   filtre de la requête ldap
   * @param tabAttrib   tableau contenant les attributs de l'enregistrement à retourner, =vide par défaut pour tous les attributs (à éviter)
   * @param strSortFild nom du champ utilisé pour le tri
   * @param idFirst     indice de début de pagination
   * @param idLast      indice de fin de pagination
   * @param bErr        =true par défaut pour afficher les erreurs, =false pour capter les erreurs
   */ 
  public function initDataSet($iTypeQuery, $strFilter, $strBaseDn="", $tabAttrib=array(), $strSortField="", $idFirst=0, $idLast=-1, $bErr=true) 
  {
    if( $strBaseDn == "" ) {
      $strBaseDn = $this->strBaseDN;
    }
    
    if( !$bErr ) ob_start();
    
    switch( $iTypeQuery ) {
    case ALK_LDAPQUERY_SEARCH:
       $iRes = @ldap_search( $this->conn, $strBaseDn, $strFilter, $tabAttrib, 0, $this->iSizeLimit, $this->iTimeLimit, $this->deref);
      break;
      
    case ALK_LDAPQUERY_READ:
      $iRes = @ldap_read( $this->conn, $strBaseDn, $strFilter, $tabAttrib, 0, $this->iSizeLimit, $this->iTimeLimit, $this->deref);  
      break;

    case ALK_LDAPQUERY_LIST:
      $iRes = @ldap_list( $this->conn, $strBaseDn, $strFilter, $tabAttrib, 0, $this->iSizeLimit, $this->iTimeLimit, $this->deref);  
      break;
    }
    
    if( $strSortField != "" ) {
      @ldap_sort($this->conn, $iRes, $strSortField); 
    }
    if( !$bErr ) ob_end_clean();
    
    $ds = new AlkDsLdap($this->conn, $iRes, $idFirst, $idLast, $bErr, ALK_LDAP_ENCODING);
    
    return $ds;
  }

  /**
   * Vérifie l'authentification ldap. Retourne true si l'utilisateur est authentifié, faux sinon
   * @param strUserName
   * @param strPwd
   * @return boolean
   */
  public function authenticate($strUserName, $strPwd)
  {
    $bRes = false;
    if( $strUserName.""!="" && $strPwd.""!="" ) { 
      $this->strUserDN  = $strUserName.$this->strAccountSuffix."";
      $this->strUserPwd = $strPwd."";
      $bRes = $this->connect();
    }
    return $bRes;    
  }
}

?>