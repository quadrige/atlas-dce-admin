<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l"INRIA
sur le site http://www.cecill.info.

En contrepartie de l"accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n"est
offert aux utilisateurs qu"une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l"auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l"attention de l"utilisateur est attirée sur les risques
associés au chargement, à l"utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l"utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l"adéquation du
logiciel à leurs besoins dans des conditions permettant d"assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l"utiliser et l"exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once("iCalcreator.class.php");

/**
 * @package Alkanet_Class_Pattern
 * @class AlkIcs
 * 
 * Classe de gestion d'un calendrier ICS
 */
class AlkIcs extends AlkObject
{
  
  protected $calendar;
  protected $tabEvents;
  protected $strHtmlContents;
  protected $strTextContents;
  
  /**
   * Constructeur par défaut
   */
  public function __construct()
  {
    $this->calendar = new vcalendar( array( 'unique_id' => ALK_APP_TITLE ));
    $this->tabEvents = array();
    parent::__construct();

    $this->strHtmlContents = "";
    $this->strTextContents = "";
  }
  
  /**
   * Ajoute un événément au calendrier
   * 
   * @param tabParamsEvent  tableau des informations relatives à l'événement indexé de la façon suivante :
   *          "objet"       => string objet de l'événement
   *          "event_desc"  => string description de l'événement
   *          "location"    => string endroit de l'événement
   *          "datedeb"     => string date de début de l'événement (DD/MM/YYYY)
   *          "heuredeb"    => string heure de début de l'événement (HH:MI[:SS])
   *          "datefin"     => string date de fin de l'événement (DD/MM/YYYY)
   *          "heurefin"    => string heure de fin de l'événement (HH:MI[:SS])
   *          "alarm_desc"  => string description de l'alarme
   *          "alarm_time"  => string heure de déclenchement de l'alarme avant début de l'événement (HH:MI[:SS])
   * 
   * @return string
   */
  public function addEvent($tabParamsEvent)
  {
    $event =& $this->calendar->newComponent("vevent");
    
    // permet d'initialiser les paramètres par défaut
    $tabParamsEventDefault = array("objet" => "", "event_desc" => "", "location" => "",
                                   "datedeb" => "", "heuredeb" => "", "datefin" => "", "heurefin" => "",
                                   "alarm_desc" => "", "alarm_time" => "");
    $this->tabEvents[] = $tabParamsEvent = array_merge($tabParamsEventDefault, $tabParamsEvent);
    
    $event->setProperty("summary", $tabParamsEvent["objet"]);
    $event->setProperty("description", $tabParamsEvent["event_desc"]);
    $event->setProperty("location", $tabParamsEvent["location"]);
    
    $tabDatedeb   = explode("/", $tabParamsEvent["datedeb"]);
    $tabHeuredeb  = explode(":", $tabParamsEvent["heuredeb"]);
    if ( count($tabDatedeb) == 3 && count($tabHeuredeb) >= 2 ) {
      $event->setProperty("dtstart", $tabDatedeb[2], $tabDatedeb[1], $tabDatedeb[0], $tabHeuredeb[0], 
                          $tabHeuredeb[1], ( array_key_exists(2,$tabHeuredeb) ?  $tabHeuredeb[2] : 00 ));
    }
    
    $tabDatefin   = explode("/", $tabParamsEvent["datefin"]);
    $tabHeurefin  = explode(":", $tabParamsEvent["heurefin"]);
    if ( count($tabDatefin) == 3 && count($tabHeurefin) >= 2 ) {
      $event->setProperty("dtend", $tabDatefin[2], $tabDatefin[1], $tabDatefin[0], $tabHeurefin[0], $tabHeurefin[1], 
                          ( array_key_exists(2,$tabHeurefin) ?  $tabHeurefin[2] : 00 ));
    }
    
    if ( $tabParamsEvent["alarm_time"] != "" ) {
      $alarm = & $event->newComponent("valarm");
      $alarm->setProperty("action", "DISPLAY");   
      $alarm->setProperty("description", $tabParamsEvent["alarm_desc"]);
      
      $tab_trigger = array();
      $tabAlarmTime = explode(":", $tabParamsEvent["alarm_time"]);
      if ( count($tabAlarmTime) >= 2 ) {
        $nbWeeks = (int)($tabAlarmTime[0]/(24*7));
        if ( $nbWeeks > 0 ) {
          $tab_trigger["week"] = $nbWeeks;
          $tabAlarmTime[0]-= $nbWeeks*(24*7);
        }
        $nbDays = (int)($tabAlarmTime[0]/24);
        if ( $nbDays > 0 ) {
          $tab_trigger["day"] = $nbDays;
          $tabAlarmTime[0]-= $nbDays*24;
        }
        if ( $tabAlarmTime[0] != 0 ) {
          $tab_trigger["hour"] = $tabAlarmTime[0];
        }
        if ( $tabAlarmTime[1] != 0 ) {
          $tab_trigger["min"] = $tabAlarmTime[1];
        }
        if ( array_key_exists(2,$tabAlarmTime) && $tabAlarmTime[2] != 0 ) {
          $tab_trigger["sec"] = $tabAlarmTime[2];
        }
        $alarm->setProperty("trigger", $tab_trigger);
      }
    }
    
    return $event;
  }
  
  /**
   * Retourne le contenu du fichier ICS
   * @return string
   */
  public function getContentFile()
  {
    return $this->calendar->createCalendar();
  }
  
  /**
   * Retourne le contenu HTML correspondant au calendrier
   * @param strTitle   titre du contenu
   * @param bForce     = false par défaut, = true pour forcer le calcul du contenu
   * @return string
   */
  public function getHtmlContent($strTitle="Calendrier", $bForce=false)
  {
    if( $this->strHtmlContents == "" || $bForce ) {
      $this->createContent($strTitle);
    }
    return $this->strHtmlContents;
  }

  /**
   * Retourne le contenu texte correspondant au calendrier
   * @param strTitle   titre du contenu
   * @param bForce     = false par défaut, = true pour forcer le calcul du contenu
   * @return string
   */
  public function getTextContent($strTitle="Calendrier", $bForce=false)
  {
    if( $this->strTextContents == "" || $bForce ) {
      $this->createContent($strTitle);
    }
    return $this->strTextContents;
  }

  /**
   * Création du contenu HTML et texte
   * Met à jour les attributs strHtmlContents et strTextPlainContents
   * @param strTitle   titre du contenu
   */
  private function createContent($strTitle="Calendrier")
  {
    $strHtml = "";   
    $strText = "";
    $strRC = "\r\n";
    
    $nbEvents = count($this->tabEvents);
    if ( $nbEvents > 0 ) {
      $strHtml = '<div id="ics"><h2>'.$strTitle.'</h2>';
      $strText = $strTitle." : ".$strRC;
      
      /*require_once(ALK_ALKANET_ROOT_PATH."classes/form/alkhtmlfactory.class.php");
      $oTabEvents = AlkHtmlFactory::getHtmlList(null, $nbEvents, 1, $nbEvents, false, "", "", true, "tabEvents");
      
      $oTabEvents->setColumns(array("left",   "150"),
                              array("left",   "200"),
                              array("center", "150"),
                              array("center", "150"),
                              array("center", "150"),
                              array("center", "150"),
                              array("center", "150"));
      
      $oTabEvents->addTitleRow(array("Objet"), array("Description"), array("Lieu"), array("Date de début"), array("Date de fin"), array("Rappel"), array("Description du rappel"));
      */
      
      foreach ( $this->tabEvents as $tabParamsEvent ) {
        $date_debut = trim($tabParamsEvent["datedeb"]." ".$tabParamsEvent["heuredeb"]);
        $date_fin   = trim($tabParamsEvent["datefin"]." ".$tabParamsEvent["heurefin"]);
        $str_rappel_time = "";
        if ( $tabParamsEvent["alarm_time"] != "" ) {
          $tab_str_rappel_time = array();
          $tabAlarmTime = explode(":", $tabParamsEvent["alarm_time"]);
          if ( count($tabAlarmTime) >= 2 ) {
            $nbWeeks = (int)($tabAlarmTime[0]/(24*7));
            if ( $nbWeeks > 0 ) {
              $tab_str_rappel_time[] = $nbWeeks." semaine".( $nbWeeks > 1 ? "s" : "" );
              $tabAlarmTime[0]-= $nbWeeks*(24*7);
            }
            $nbDays = (int)($tabAlarmTime[0]/24);
            if ( $nbDays > 0 ) {
              $tab_str_rappel_time[] = $nbDays." jour".( $nbDays > 1 ? "s" : "" );
              $tabAlarmTime[0]-= $nbDays*24;
            }
            if ( $tabAlarmTime[0] != 0 ) {
              $tab_str_rappel_time[] = $tabAlarmTime[0]." heure".( $tabAlarmTime[0] > 1 ? "s" : "" );
            }
            if ( $tabAlarmTime[1] != 0 ) {
              $tab_str_rappel_time[] = $tabAlarmTime[1]." minute".( $tabAlarmTime[1] > 1 ? "s" : "" );
            }
            if ( array_key_exists(2,$tabAlarmTime) && $tabAlarmTime[2] != 0 ) {
              $tab_str_rappel_time[] = $tabAlarmTime[2]." seconde".( $tabAlarmTime[0] > 1 ? "s" : "" );
            }
          }
          $str_rappel_time = implode(", ", $tab_str_rappel_time)." avant";
        }
        //$oTabEvents->addRowByArray(array($tabParamsEvent["objet"], $tabParamsEvent["event_desc"], $tabParamsEvent["location"], $date_debut, $date_fin, $str_rappel_time, $tabParamsEvent["alarm_desc"]));

        $strText .= $strRC.
          $tabParamsEvent["objet"].$strRC.
          "  - "._t("Description :")." ". $tabParamsEvent["event_desc"].$strRC.
          ( $tabParamsEvent["location"] != ""
            ? "  - "._t("Lieu :")." ".$tabParamsEvent["location"].$strRC
            : "" ).
          ( $date_debut != "" && $date_fin == ""
             ? "  - "._f("Date : à partir du %s ", $date_debut).$strRC
             : ( $date_debut == "" && $date_fin != ""
                 ? "  - "._f("Date : jusqu'au %s ", $date_fin).$strRC
                 : ( $date_debut != "" && $date_fin != ""
                     ? "  - "._f("Date : du %s au %s ", $date_debut, $date_fin).$strRC
                     : "" ))).
          ( $str_rappel_time != "" 
            ? "  - "._t("Rappel :")." ".$str_rappel_time.
              ( $tabParamsEvent["alarm_desc"] != ""
                ? $strRC."  - ".$tabParamsEvent["alarm_desc"]
                : "" ).
              $strRC
            : "" );

        $strHtml .= "<br/>".
          "<b>".$tabParamsEvent["objet"]." :</b>".
          "<ul>".
          "<li>"._t("Détail :")." ". $tabParamsEvent["event_desc"]."</li>".
          ( $tabParamsEvent["location"]
            ? "<li>"._t("Lieu :")." ".$tabParamsEvent["location"]."</li>"
            : "" ).
          ( $date_debut != "" && $date_fin == ""
             ? "<li>"._f("Date : à partir du %s ", $date_debut)."</li>"
             : ( $date_debut == "" && $date_fin != ""
                 ? "<li>"._f("Date : jusqu'au %s ", $date_fin)."</li>"
                 : ( $date_debut != "" && $date_fin != ""
                     ? "<li>"._f("Date : du %s au %s ", $date_debut, $date_fin)."</li>"
                     : "" ))).
          ( $str_rappel_time != "" 
            ? "<li>"._t("Rappel :")." ".$str_rappel_time.
              ( $tabParamsEvent["alarm_desc"] != ""
                ? "<br/>".$tabParamsEvent["alarm_desc"]
                : "" ).
              "</li>"
            : "" ).
          "</ul>";
          

      }
      
      $strHtml .= '</div>';
      //$strHtml.= $oTabEvents->getHtml();
    }

    $this->strHtmlContents = "<br/>".$strHtml;
    $this->strTextContents = $strRC.$strText;
  }
}
?>