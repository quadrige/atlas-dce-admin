<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


/**
 * @package Alkanet_Class_Pattern
 * @class AlkXml
 * 
 * Classe de transformation XML-XSL
 */
class AlkXml extends AlkObject 
{
  /** Nom du fichier xsl */
  protected $strPathFileNameXsl;

  /** Nom du fichier xml */
  protected $strPathFileNameXml;

  /** contenu Xml */
  protected $strXml;

  /** contenu Xsl */
  protected $strXsl;

  /** contenu html obtenu après transformation */
  protected $strHtml;

  /** Derniere erreur rencontrée */
  protected $strError;
    
  
  /**
   * Constructeur par défaut.
   *        Si strXml est renseigné, strPathFileNameXml non pris en compte
   *        Si strXsl est renseigné, strPathFileNameXsl non pris en compte
   *
   * @param strXml              Contenu xml utilisé pour la transformation, vide par défaut
   * @param strXsl              Contenu xsl utilisé pour la transformation, vide par défaut
   * @param strPathFileNameXml  Chemin physique et nom du fichier xml, vide par défaut
   * @param strPathFileNameXsl  Chemin physique et nom du fichier xsl, vide par défaut
   */
  public function __construct($strXml="", $strXsl="", $strPathFileNameXml="", $strPathFileNameXsl="") 
  {
    parent::__construct();
    
    $this->AlkXml($strXml, $strXsl, $strPathFileNameXml, $strPathFileNameXsl);
  }
  
  /**
   * Constructeur par défaut.
   *        Si strXml est renseigné, strPathFileNameXml non pris en compte
   *        Si strXsl est renseigné, strPathFileNameXsl non pris en compte
   *
   * @param strXml              Contenu xml utilisé pour la transformation, vide par défaut
   * @param strXsl              Contenu xsl utilisé pour la transformation, vide par défaut
   * @param strPathFileNameXml  Chemin physique et nom du fichier xml, vide par défaut
   * @param strPathFileNameXsl  Chemin physique et nom du fichier xsl, vide par défaut
   */
  public function AlkXml($strXml="", $strXsl="", $strPathFileNameXml="", $strPathFileNameXsl="")
  {
    $this->strXml   = $strXml;
    $this->strXsl   = $strXsl;

    $this->strPathFileNameXsl = "";
    $this->strPathFileNameXml = "";

    if( $this->strXml=="" &&  $strPathFileNameXml!="" )
      $this->setFileNameXml($strPathFileNameXml);

    if( $this->strXsl=="" && $strPathFileNameXsl!="" )
      $this->setFileNameXsl($strPathFileNameXsl);

    $this->strHtml  = "";

    $this->strError = "";
  }

  /**
   * Mémorise le nom du fichier xml puis charge le contenu xml
   *
   * @param strPathFileName Chemin physique et nom du fichier xml
   */
  public function setFileNameXml($strPathFileName)
  {
    $this->strPathFileNameXml = $strPathFileName;
    $this->strXml = "";
    if( @file_exists($this->strPathFileNameXml) && @is_file($this->strPathFileNameXml) ) {
      if( function_exists("file_get_contents") ) {
        // existe depuis php 4.3.0
        $this->strXml = @file_get_contents($this->strPathFileNameXml);
        if( !is_string($this->strXml) ) {
          $this->strXml = "";
        }
      } else {
        $tabLine = @file($this->strPathFileNameXml);
        if( count($tabLine) > 0 ) {
          $this->strXml = implode('', $tabLine);
        }
      }
    }
  }

  /**
   * Mémorise le nom du fichier xsl puis charge le contenu xsl
   *
   * @param strPathFileName Chemin physique et nom du fichier xsl
   */
  public function setFileNameXsl($strPathFileName)
  {
    $this->strPathFileNameXsl = $strPathFileName;
    $this->strXsl = "";
    if( @file_exists($this->strPathFileNameXsl) && @is_file($this->strPathFileNameXsl) ) {
      if( function_exists("file_get_contents") ) {
        // existe depuis php 4.3.0
        $this->strXsl = @file_get_contents($this->strPathFileNameXsl);
        if( !is_string($this->strXsl) ) {
          $this->strXsl = "";
        }
      } else {
        $tabLine = @file($this->strPathFileNameXsl);
        if( count($tabLine) > 0 ) {
          $this->strXsl = implode('', $tabLine);
        }
      }
    }
  }

  /**
   * Charge un fichier xsl et le concatène au contenu xsl de cet objet
   *
   * @param strPathFileName Chemin physique et nom du fichier xsl à concaténer
   */
  public function addFileNameXsl($strPathFileNameXsl)
  {       
    if( @file_exists($strPathFileNameXsl) && @is_file($strPathFileNameXsl) ) {
      if( function_exists("file_get_contents") ) {
        // existe depuis php 4.3.0
        $this->strXsl .= @file_get_contents($strPathFileNameXsl);
        if( !is_string($this->strXsl) ) {
          $this->strXsl .= "";
        }
      } else {
        $tabLine = @file($strPathFileNameXsl);
        if( count($tabLine) > 0 ) {
          $this->strXsl .= implode('', $tabLine);
        }
      }
    }
  }
  
  
  /**
   * Retourne le contenu xml
   *
   * @return string
   */
  public function getXml()
  {
    return $this->strXml;
  }
  
  /**
   * Retourne le contenu xsl
   *
   * @return string
   */
  public function getXsl()
  {
    return $this->strXsl;
  }
  
  /**
   * Mémorise le contenu xml
   *
   * @param strXml  Contenu xml utilisé pour la transformation
   */
  public function setXml($strXml)
  {
    $this->strXml = $strXml;
  }

  /**
   * Mémorise le contenu xsl
   *
   * @param strXsl  Contenu xsl utilisé pour la transformation
   */
  public function setXsl($strXsl)
  {
    $this->strXsl = $strXsl;
  }

  /**
   * Retourne le résultat de la dernière transformation
   *
   * @return Retourne un string
   */
  public function getHtml() 
  { 
    return $this->strHtml; 
  }

  /**
   * Retourne le texte de la denière erreur rencontrée
   * 
   * @return Retourne un string
   */
  public function getLastError() 
  { 
    return $this->strError; 
  }

  /**
   *  Effectue la transformation XML/XSL puis mémorise
   * @param bInputHTML  true si le contenu en entrée est de type HTML, false (default) si il est de type XML 
   * @return Retourne un bool : true si la transformation ok, false sinon
   */
  public function setTransformation($bInputHTML=false) 
  {
    $this->strError = "";
    $this->strHtml = "";
    if( $this->strXml == "" ) {
      $this->strError = "Contenu XML vide.\n";
      return false;
    }
    if( $this->strXsl == "" ) {
      $this->strError = "Contenu XSL vide.\n";
      return false;
    }

    if( defined("ALK_B_XSLT_PROCESS")==false || 
        (defined("ALK_B_XSLT_PROCESS")==true && ALK_B_XSLT_PROCESS==true ) ) {
    
      // initialise le resultat de la transformation
      $oldErrReport = error_reporting();
      $result = false;
      try {
        $bTransOk = true;
        
        // create a DomDocument
        $doc = new DOMDocument();
        $xsl = new XSLTProcessor();
        
        // chargement de la feuille de style
        $doc->loadXML($this->strXsl);
        $xsl->importStyleSheet($doc);
        
        // traitement du document
        error_reporting(0);
        if ( $bInputHTML )
          $doc->loadHTML(mb_convert_encoding($this->strXml, ALK_EXPORT_ENCODING, ALK_HTML_ENCODING));
        else
          $doc->loadXML($this->strXml);
        $result = $xsl->transformToXml($doc);
      } catch(Exception $e) {
        $this->strError = "Code erreur : ".$e->getMessage();
      }
      error_reporting($oldErrReport);
      
      if( $result ) {
        $this->strHtml = $result;
      } else {
        $bTransOk = false;
      }

      return $bTransOk;

    } else {

      $bTransOk = true;
      $bTmpXml = false;
      $bTmpXsl = false;
      
      if( $this->strPathFileNameXml=="" || $this->strPathFileNameXsl == "" ) {
        // fichier temporaire
        $nbAlea = date('Gis')."_".rand(0, 999999999);

        $strTmpName = "tmp_".$nbAlea;

        if( $this->strPathFileNameXml == "" ) {
          $this->strPathFileNameXml = ALK_ALKANET_ROOT_PATH.ALK_ROOT_UPLOAD.ALK_UPLOAD_CACHE.$strTmpName.".xml";          
          $hFile = @fopen($this->strPathFileNameXml, "w+");
          if( $hFile ) {
            fwrite($hFile, $this->strXml);
            fclose($hFile);
            $bTmpXml = true;
          } else {
            $this->strError = "Impossible de créer le fichier temporaire XML.\n";
            return false;
          }
        }

        if( $this->strPathFileNameXsl == "" ) {
          $this->strPathFileNameXsl = ALK_ALKANET_ROOT_PATH.ALK_ROOT_UPLOAD.ALK_UPLOAD_CACHE.$strTmpName.".xsl";          
          $hFile = @fopen($this->strPathFileNameXsl, "w+");
          if( $hFile ) {
            fwrite($hFile, $this->strXsl);
            fclose($hFile);
            $bTmpXsl = true;
          } else {           
            if( $bTmpXml == true && 
                file_exists($this->strPathFileNameXml) && is_file($this->strPathFileNameXml) )
              @unlink($this->strPathFileNameXml);
            $this->strError = "Impossible de créer le fichier temporaire XSL.\n";
            return false;
          }
        }
      }

      $tabRes = array();
      $oRes = "";
      $l_command = "xsltproc ".$this->strPathFileNameXsl." ".$this->strPathFileNameXml;
      @exec( $l_command, $tabRes, $oRes );
      
      if( !$oRes ) {
        $this->strHtml = implode(" ", $tabRes);
      } else {
        $this->strError = "Erreur de transformation XML/XSL.\n";
        $bTransOk = false;
      }

      if( $bTmpXml == true && 
          file_exists($this->strPathFileNameXml) && is_file($this->strPathFileNameXml) )
        @unlink($this->strPathFileNameXml);

      if( $bTmpXsl == true && 
          file_exists($this->strPathFileNameXsl) && is_file($this->strPathFileNameXsl) )
        @unlink($this->strPathFileNameXsl);

      return $bTransOk;
    }
  }
  
}
?>