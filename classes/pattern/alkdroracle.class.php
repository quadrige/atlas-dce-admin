<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkdr.class.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkerrororacle.class.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkDrOracle
 * @brief Classe qui hérite de la class dr (datarow)
 */
final class AlkDrOracle extends AlkDr
{

  /**
   *  Constructeur par défaut
   * @param oRow      Tableau associatif d'un enregistrement
   * @param dsParent  Référence sur le dataSet associé
   */
  public function __construct(&$oRow, &$dsParent)
  {
    parent::__construct($dsParent);

    if( isset($oRow) && is_array($oRow) ) {
      $this->oRow = $oRow;
      
      //Le nombre de champ est le nombre de colonnes du tableau divisé par 2
      //car chaque champ pred 2 colonnes : 1 colonne pour son indice, et une colonne
      //pour sa clé ( on a donc l'information en double).
      $this->iCountField = count($oRow);
    } else {
      $this->oRow = array();
      $this->iCountField = 0;
    }
  }
  
  /**
   * Retourne la valeur du champs passé en paramètre
   * Retourne une chaine vide si le champ n'existe pas
   *
   * @param strField          nom du champs
   * @param strDefaultValue   Valeur par défaut si le champ n'est pas trouvé dans oRow
   * @param bToUpper          vrai=force strField en majuscule (par défaut), faux=ne modifie pas strField
   * @return Retourne une chaine
   */
  public function getValueName($strField, $strDefaultValue="", $bToUpper=true)
  {
    $strField = ($bToUpper ? mb_strtolower($strField) : $strField);
    $strRes = $strDefaultValue;
    $strEncoding = $this->dsParent->getEncoding();
    if( is_array($this->oRow) && array_key_exists($strField, $this->oRow) ) {
      $strRes = $this->oRow[$strField];
      
      if( is_object($strRes) && method_exists($strRes, "load") ) {
        $strRes = $strRes->load();
      }
      if( $strEncoding != ALK_HTML_ENCODING ) {
        $strRes = mb_convert_encoding($strRes, ALK_HTML_ENCODING, $strEncoding);
      }
      
      /*$tabCle = array_keys($this->oRow);
      $val = array_search($strField, $tabCle);
      if( $this->dsParent->getFieldType($iNumField) == "1" ) {
        $strRes = mb_ereg_replace(",", ".", $strRes);
        $strRes = mb_ereg_replace("^\.", "0.", $strRes);
      }*/
    }
    return $strRes;
  }

  /**
   * Retourne la valeur du champs identifiant par son numéro d'ordre dans la requete
   *
   * @param iNumField numéro du champs (indicé à 0 à n-1)
   * @return Retourne une chaine : valeur du champs
   */
  public function getValueNum($iNumField)
  {
    $strRes = "";
    $strEncoding = $this->dsParent->getEncoding();
    if( is_array($this->oRow) ) {
      if( is_int($iNumField) && array_key_exists($iNumField, $this->oRow) ) {
        $strRes = $this->oRow[$iNumField];
        if( is_object($strRes) && method_exists($strRes, "load") ) {
          $strRes = $strRes->load();
        }
        if( $strEncoding != ALK_HTML_ENCODING ) {
          $strRes = mb_convert_encoding($strRes, ALK_HTML_ENCODING, $strEncoding);
        }
        
        /*if( $this->dsParent->getFieldType($iNumField) == "1" ) {
          $strRes = mb_ereg_replace(",", ".", $strRes);
          $strRes = mb_ereg_replace("^\.", "0.", $strRes);
        }*/
      }
    }
    return $strRes;
  }
}
?>