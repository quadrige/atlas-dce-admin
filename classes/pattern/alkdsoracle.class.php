<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkds.class.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkdroracle.class.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkerrororacle.class.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkDsOracle
 * @brief DataSet basé sur oracle, hérite de la classe AlkDs
 */
final class AlkDsOracle extends AlkDs
{
  
  /** indique le mode de récupération des donnés (FetchOnly=sans tableau php) */
  protected $bFetchOnly;
  
  /** Objet contenant les données du dataset */
  protected $oRes;

  /** Rempli dans le cas bFetchOnly car tirage d'une ligne pour savoir si le résultat de la requête est vide */
  protected $tabDrFirstLine;

  /**
   *  Constructeur par défaut de la classe
   *
   * @param conn    handle de la connexion ouverte
   * @param strSQL  requête sql
   * @param idFirst indice de début pour la pagination (=0 par défaut)
   * @param idLast  indice de fin pour la pagination (=-1 par défaut)
   * @param bErr    true si gestion des erreurs, false pour passer sous silence l'erreur
   * @param strDbEncoding encodage du client sgbd
   */
  public function __construct($conn, $strSql, $idFirst=0, $idLast=-1, $bErr=true, $strDbEncoding=ALK_SGBD_ENCODING)
  {
    parent::__construct($strDbEncoding);
    $this->bFetchOnly = false;
    
    // liste des champs reconnu par Oracle
    $tabType = array("VARCHAR2" => 0, "NUMBER" => 1, "DATE" => 2, "FLOAT" => 3);
  
    $this->lastErr = "";
  
    if( $bErr == true )
      startErrorHandlerOracle();
    else
      ob_start();
    
    $this->tabDrFirstLine = null;
    $this->iCountDr = 0;
    $this->bEof = true;
    $this->iCurDr = -1;
    $this->tabDr = array();
    $this->tabType = array();
    
    $strSqlFetch = $strSql;
    // requête de pagination, si demandé
    if($idLast >= $idFirst) {
      // WARNING : Oracle ROWNUM starts at 1
      $strSqlFetch = "select a.* ".
                     " from (select q.*, RowNum as ocirnum ".
                     "       from (".$strSql.") q where RowNum <= ".($idLast+1).") a ".
                     " where ocirnum >= ".($idFirst+1);
    }
    
    //$strSqlFetch = mb_eregi_replace("^select", "select /*+ NO_MONITOR */ ", $strSqlFetch);
    //echo "SQL: ".$strSqlFetch."<br>";
    $this->oRes = oci_parse($conn, $strSqlFetch);
    
    // prefetch - reduce network traffic
    $defaultPrefetch = 20;
    oci_set_prefetch($this->oRes, ($idLast >= $idFirst ? max(1, $idLast-$idFirst) : $defaultPrefetch));
    
    $res = oci_execute($this->oRes);
    
    if( $error = oci_error($this->oRes) ) {
      if( $bErr == true ) {
        trigger_error("AlkdsOracle - Erreur requête (".$strSql.", Oracle : ".
                      $error['code'].", ".$error['message'].")", E_USER_ERROR);
      } else {
        $this->_SetError($error['message']);
      }
    }
  
    $nbFields = oci_num_fields($this->oRes);
    for($i=0; $i<$nbFields; $i++) {
      $strType = oci_field_type($this->oRes, $i+1);
      $this->tabType[$i] = ( array_key_exists($strType, $tabType) == true ? $tabType[$strType] : 0 );
      $this->tabRealFields[$i] = oci_field_name($this->oRes, $i+1);
    }
  
    $this->iCurDr = -1;
    if(!$this->bFetchOnly) {
      // mode avec tableau, récupérer les données
      $this->iCountDr = oci_fetch_all($this->oRes, $this->tabDr, 0, -1, OCI_ASSOC+OCI_FETCHSTATEMENT_BY_ROW);
    } else {
      // mode sans tableau, iCountDr n'est pas disponible !
      $this->iCountDr = 0;
    }
  
    $this->iCountTotDr = $this->iCountDr;      
    if( $idLast >= $idFirst ) {
      // pagination en cours : calcul le nombre total
      $strSql = "select count(*) as NBTOT from (".$strSql.") r";
      $resAct = oci_parse($conn, $strSql);
      
      if( !ociexecute($resAct) ) {
        $error = oci_error($resAct);
        if( $bErr == true )
          trigger_error("AlkdsOracle - Erreur requête (".$strSql.", Oracle : ".
                        $error['code'].", ".$error['message'].")", E_USER_ERROR);
        else
          $this->_SetError($error['message']);
      }
      if( ociFetch($resAct) )
        $this->iCountTotDr = ociResult($resAct, "NBTOT");
    }

    if(!$this->bFetchOnly){
      if( $this->iCountDr > 0 ) {
        $this->iCurDr = 0;
        $this->bEof = false;
      } else {
        $this->tabDr = array();
        $this->iCountDr = 0;
        $this->iCountTotDr = 0;
      }
    } else {
      $this->iCurDr = 0;
      // récupération et sauvegarde de la première ligne pour savoir si il y a au moins une valeur
      // NOTE - IDEE : faire une fonction faisant ce if pour savoir constammment si il y a une prochaine ligne et incrémenter iCount (appelée avec 1 coup d'avance)
      if  ( $this->tabDrFirstLine = oci_fetch_array($this->oRes, OCI_ASSOC) ) {
        $this->bEof = false;
        $this->iCountDr = 1;
        $this->iCountTotDr = 1;
      }      
      $this->tabDr = array();
  
    }
    
  
    if( $bErr == true )
      endErrorHandlerOracle();
    else
      ob_end_clean();
  }
  
  /**
  * destructeur par défaut
  */
  public function __destruct() 
  {
  }
  
  /**
   *  Retourne le datarow (dr) correspondant à l'indice courant ($iCurDr)
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  public function GetRow()
  {
    $drRes = false;
    if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drOracletmp = $this->tabDr[$this->iCurDr];
      $drRes = new AlkDrOracle($drOracletmp, $this);
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }

    return $drRes;
  }

  /**
   *  Retourne le datarow (dr) correspondant à l'indice courant ($iCurDr) puis passe au suivant
   *        Retourne false si EOF est atteint
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  public function GetRowIter()
  {
    $drRes = false;
    if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drOracletmp = $this->tabDr[$this->iCurDr];
      $this->iCurDr++;
      $drRes = new AlkDrOracle($drOracletmp, $this);
    }

    return $drRes;
  }

  /**
   *  Retourne le datarow (dr) positionné à l'indice id
   *        Retourne false si l'indice n'est pas satisfaisant
   *         n'agit ni sur bEOF, ni sur le pointeur 
   *
   * @param id Indice du dataRow
   * @return Retourne un dataRow si ok, false sinon
   */
  public function GetRowAt( $id )
  {
    $drRes = false;      
    if( $this->iCountDr>0 && $id>=0 && $id<$this->iCountDr ){
      $drOracletmp = $this->tabDr[$id];
      $drRes = new AlkDrOracle($drOracletmp, $this);
    }
  
    return $drRes;
  }

  /**
   *  Place le pointeur du dataset sur le premier datarow
   *        Met à jour l'indice courant du dataset
   */
  public function MoveFirst() 
  {
    if( $this->iCountDr > 0 ) {
      $this->iCurDr = 0;
      $this->bEof = false;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Place le pointeur du dataset sur le datarow précédent
   *        Met à jour l'indice courant du dataset
   */
  public function MovePrev() 
  {
    if( $this->iCountDr>0 && $this->iCurDr>0 ) {
      $this->iCurDr--;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Place le pointeur du dataset sur le datarow suivant
   *        Met à jour l'indice courant du dataset
   */
  public function MoveNext() 
  {
    if( $this->iCountDr>0 && $this->iCurDr<$this->iCountDr ) {
      $this->iCurDr++;
      
      if( $this->iCurDr >= $this->iCountDr ) {
        $this->bEof = true;
      }
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Place le pointeur du dataset sur le dernier datarow
   *        Met à jour l'indice courant du dataset
   */
  public function MoveLast() 
  {
    if ( $this->iCountDr > 0 ) {
      $this->iCurDr = $this->iCountDr-1;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Place le pointeur du dataset sur le (iCurDr+iNb) ième datarow
   *        Met à jour l'indice courant du dataset
   *
   * @param iNb Décalage positive ou négatif par rapport au dataRow courant
   */
  public function Move($iNb) 
  {
    if( $this->iCountDr > 0 && $this->iCurDr+$iNb>=0 && $this->iCurDr+$iNb<$this->iCountDr ) {
      $this->iCurDr += $iNb;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Détruit l'objet dataset()
   */
  public function Close() 
  {
    $this->oRes = "";
  }
    
  /**
   *  Tri le dataSet après lecture dans la source donnée
   *        Effectue un tri à plusieurs niveaux
   *        Nécessite de connaite les noms de champ : ID et ID_PERE
   *        le dataset doit être déjà trié par niv puis par rang
   *
   * @param strFieldId  Nom du champ ID dans la requeête
   * @param strFieldIdp Nom du champ ID_PERE dans la requête
   */
  public function setTree($strFieldId, $strFieldIdp)
  {
    $tabNoeud = array();   // contient l'ensemble des noeuds
    $tabIndex = array();   // contient l'index inverse id -> cpt
    $stack = array();
    
    for ($i=0; $i< $this->iCountTotDr; $i++) {
      //$drOracletmp = Oracle_fetch_array($this->oRes);
      $drOracletmp = $this->tabDr[$i];
      
      $idFils = $drOracletmp[strtolower($strFieldId)];
      $idPere = $drOracletmp[strtolower($strFieldIdp)];
      $tabNoeud[$i]["FILS"] = array();
      $tabNoeud[$i]["ID"] = $idFils;
      $tabNoeud[$i]["OK"] = false;

      // memorize the first level id in a stack
      array_push($stack, $i);

      // met a jour la table index secondaire
      $tabIndex[$idFils] = $i;

      // ajoute l'id fils a l'id pere
      if( $idPere > 0 )
        if( array_key_exists($idPere, $tabIndex) == true ) {
          $iPere = $tabIndex[$idPere];
          array_push($tabNoeud[$iPere]["FILS"], $i);
        }
    }

    // parse the sons
    $tabDr2 = array();
    $j = 0;
    while( count($stack) > 0 ) {
      $i = array_shift($stack);
      if( $tabNoeud[$i]["OK"] == false ) {
        $id = $tabNoeud[$i]["ID"];
        $tabNoeud[$i]["OK"] = true;
        $tabDr2[$j] = $this->tabDr[$i];
        $j++;
        
        $nbFils = count($tabNoeud[$i]["FILS"]);
        for ($k=$nbFils-1; $k>=0; $k--) {
          $if = $tabNoeud[$i]["FILS"][$k];
          array_unshift($stack, $if);
        }
      }
      $i++;
    }

    $tabNoeud = null;
    $tabIndex = null;
    $this->tabDr = $tabDr2;
  }
  
}
?>