<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


/**
 * @package Alkante_Class_Pattern
 * 
 * @interface AlkIntAbonne
 * @brief Interface liée aux applications fournissant les méthodes à implémenter
 *        pour la gestion des abonnés (ajout / modif / suppression d'abonné)
 */
interface AlkIntAbonne
{
  
  /**
   * Méthode appelée pour créer une liste d'abonnés associée à la donnée
   */
  public function createListeAbonnes($data_id=-1, $mode=-1, $iAbonnement=0);
  
  /**
   * Méthode appelée pour autoriser l'abonnement
   */
  public function enableAbonnement($data_id=-1, $mode=-1);
  
  /**
   * Méthode appelée pour interdire l'abonnement
   */
  public function disableAbonnement($data_id=-1, $mode=-1);
  
  /**
   * Méthode appelée avant suppression d'un abonnement
   * @param abonne_id  identifiant de l'abonné, peut contenir une liste d'identifiant
   * @param liste_id   identifiant de la liste de laquelle on souhaite désabonné
   */
  public function delAbonnement($abonne_id, $liste_id=-1);

  /**
   * Méthode appelée avant suppression définitive d'un abonne
   * Attention, si un abonné est propriétaire d'une info, il ne faut pas la supprimer
   * Implémenter les actions effectuant le ménage interne à l'application
   * Retourne true si l'abonné reste supprimable, faux si il est propriétaire d'info
   * @param abonne_id  identifiant de l'abonné, peut contenir une liste d'identifiant
   * @return boolean
   */
  public function delAbonne($abonne_id);

  /**
   * Méthode appelée avant suppression définitive d'une liste d'abonnés
   * @param liste_id  identifiant de la liste, peut contenir une liste d'identifiant
   */
  public function delListe($liste_id);

}

?>