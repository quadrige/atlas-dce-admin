<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


/**
 * @package Alkanet_Class_Pattern
 * 
 * @interface AlkIntFDoc
 * @brief Interface liée à l'application fdoc pour gérer les mouvements d'une fiche doc et la suppression d'une application
 */
interface AlkIntFDoc
{

  /**
   * Création d'une fiche documentaire. Retourne l'identifiant de la fiche doc
   * @param tabInfo    tableau associatif contenant les informations de la fiche doc
   * @return int
   */
  //public function addFDocDoc($doc_id);

  /**
   * Mise à jour d'une fiche documentaire
   * @param doc_id     Identifiant ou liste d'identifiants séparés par une virgule
   * @param tabInfo    tableau associatif contenant les informations de la fiche doc
   */
  //public function updateFDocDoc($doc_id, $tabInfo);

  /**
   * Suppression définitive d'un ensemble de fiche documentaire
   * @param doc_id     Identifiant ou liste d'identifiants séparés par une virgule
   */
  public function delFDocDoc($doc_id);
   
  /**
   * Suppression d'une application fdoc fournisseur d'information pour l'application
   * interprétant cette méthode
   * @param appli_id  identifiant de l'application fdoc supprimée
   */
  public function delFDocAppli($appli_id);
   
}

?>