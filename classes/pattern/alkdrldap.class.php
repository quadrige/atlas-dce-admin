<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkdr.class.php");

/**
 * @package  Alkanet_Class_Pattern
 * 
 * @class AlkdrLdap
 * @brief Classe qui hérite de la class AlkDr (datarow)
 */
final class AlkDrLdap extends AlkDr
{

  /**
   *  Constructeur par défaut
   * @param oRow      Tableau associatif d'un enregistrement
   * @param dsParent  Référence sur le dataSet associé
   */
  public function __construct(&$oRow, &$dsParent)
  {
    parent::__construct($dsParent);

    if( isset($oRow) && is_array($oRow) ) {
      // met en majuscule toutes les clés
      foreach($oRow as $strKey => $strVal) {
        $this->oRow[mb_strtoupper($strKey)] = $strVal;
      }
      $this->iCountField = count($this->oRow)/2;
    } else {
      $this->oRow = array();
      $this->iCountField = 0;
    }
  }
  
  /**
   *   Retourne la valeur du champs passé en paramètre
   *         Retourne une chaine vide si le champ n'existe pas
   *
   * @param strField          nom du champs
   * @param strDefaultValue   Valeur par défaut si le champ n'est pas trouvé dans oRow
   * @param bToUpper          vrai=force strField en majuscule (par défaut), faux=ne modifie pas strField
   * @return Retourne une chaine
   */
  public function getValueName($strField, $strDefaultValue="", $bToUpper=true)
  {
    $strRes = $strDefaultValue;
    $strEncoding = $this->dsParent->getEncoding();
    $strField = mb_strtoupper($strField);
    if( is_array($this->oRow) && array_key_exists($strField, $this->oRow) ) {
      $strRes = ( $strEncoding != ALK_HTML_ENCODING && is_string($this->oRow[$strField])
                  ? mb_convert_encoding($this->oRow[$strField], ALK_HTML_ENCODING, $strEncoding)
                  : $this->oRow[$strField] );
    }
    return $strRes;
  }

  /**
   *  Retourne la valeur du champs identifiant par son numéro d'ordre dans la requete
   *
   * @param iNumField numéro du champs (indicé à 0 à n-1)
   * @return Retourne une chaine : valeur du champs
   */
  public function getValueNum($iNumField)
  {
    $strRes = "";
    $strEncoding = $this->dsParent->getEncoding();
    if( is_array($this->oRow) ) {
      if( is_int($iNumField) && array_key_exists($iNumField, $this->oRow) )
        $strRes = ( $strEncoding != ALK_HTML_ENCODING && is_string($this->oRow[$iNumField])
                    ? mb_convert_encoding($this->oRow[$iNumField], ALK_HTML_ENCODING, $strEncoding)
                    : $this->oRow[$iNumField] );
    }
    return $strRes;
  }
	
  /**
   *  Retourne dans un tableau la liste des champs
   *
   * @return Retoune un array
   */
	public function getFields()
  {
    $tabRes = array();
    $indice=0 ;
    foreach ( $this->oRow as $key => $value) {
      if ( !is_int($key) ) {
        $tabRes[$indice] = $key;
        $indice++ ;
      }
    }
    return $tabRes;
  }

  /**
   *  Réinitialise l'objet
   */
  public function close( )
  {
    $this->oRow = array();
    $this->iCountField = 0;
  }

}
?>