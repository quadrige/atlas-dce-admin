<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

if( !function_exists("quoted_printable_encode") ) {
  
  /**
   * Cette fonction n'existequ'à partir de php 5.3
   */
  function quoted_printable_encode($input, $line_max = 75) 
  {
     $hex = array('0','1','2','3','4','5','6','7', '8','9','A','B','C','D','E','F');
     $lines = preg_split("/(?:\r\n|\r|\n)/", $input);
     $linebreak = "=0D=0A=\r\n";
     /* the linebreak also counts as characters in the mime_qp_long_line
      * rule of spam-assassin */
     $line_max = $line_max - strlen($linebreak);
     $escape = "=";
     $output = "";
     $cur_conv_line = "";
     $length = 0;
     $whitespace_pos = 0;
     $addtl_chars = 0;
  
     // iterate lines
     for ($j=0; $j<count($lines); $j++) {
       $line = $lines[$j];
       $linlen = strlen($line);
  
       // iterate chars
       for ($i = 0; $i < $linlen; $i++) {
         $c = substr($line, $i, 1);
         $dec = ord($c);
  
         $length++;
  
         if ($dec == 32) {
            // space occurring at end of line, need to encode
            if (($i == ($linlen - 1))) {
               $c = "=20";
               $length += 2;
            }
  
            $addtl_chars = 0;
            $whitespace_pos = $i;
         } elseif ( ($dec == 61) || ($dec < 32 ) || ($dec > 126) ) {
            $h2 = floor($dec/16); $h1 = floor($dec%16);
            $c = $escape . $hex["$h2"] . $hex["$h1"];
            $length += 2;
            $addtl_chars += 2;
         }
  
         // length for wordwrap exceeded, get a newline into the text
         if ($length >= $line_max) {
           $cur_conv_line .= $c;
  
           // read only up to the whitespace for the current line
           $whitesp_diff = $i - $whitespace_pos + $addtl_chars;
  
          /* the text after the whitespace will have to be read
           * again ( + any additional characters that came into
           * existence as a result of the encoding process after the whitespace)
           *
           * Also, do not start at 0, if there was *no* whitespace in
           * the whole line */
           if (($i + $addtl_chars) > $whitesp_diff) {
              $output .= substr($cur_conv_line, 0, (strlen($cur_conv_line) -
                             $whitesp_diff)) . $linebreak;
              $i =  $i - $whitesp_diff + $addtl_chars;
            } else {
              $output .= $cur_conv_line . $linebreak;
            }
  
          $cur_conv_line = "";
          $length = 0;
          $whitespace_pos = 0;
        } else {
          // length for wordwrap not reached, continue reading
          $cur_conv_line .= $c;
        }
      } // end of for
  
      $length = 0;
      $whitespace_pos = 0;
      $output .= $cur_conv_line;
      $cur_conv_line = "";
  
      if ($j<=count($lines)-1) {
        $output .= $linebreak;
      }
    } // end for
  
    return trim($output);
  } // end quoted_printable_encode 
}


/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkMail
 * @brief Permet d'envoyer un mail.
 */
final class AlkMail extends AlkObject
{
  /** sujet du message */
  protected $subject;
  /** corps du message au format html si ContentType=text/html */
  protected $body;
  /** corps du message au format texte uniquement */
  protected $altBody;
  /** nom de l'expediteur */
  protected $fromName;
  /** email de l'expediteur */
  protected $fromMail;
  /** liste des destinataires */
  protected $tabTo;
  /** liste des destinataires en copie */
  protected $tabCC;
  /** liste des destinataires en copie cachée */
  protected $tabCCi;
  /** liste des adresses pour répondre à, par défaut ajoute l'expéditeur */
  protected $tabReplyTo;
  /** liste des pièces jointes */
  protected $tabFiles;
  /** liste des événements du calendrier ICS */
  protected $tabIcs;
  /** vrai pour envoyer un à un, tous les destinataires, = faux par défaut */
  protected $bSendCCi2To;
  /** Nombre de destinataires max pour un envoi en direct */
  protected $maxSend;
  /** priorité du mail (1=haute à 5=basse, 3=normal) */
  protected $priority = 3;
  /** code caractere pour le message */
  protected $charSet;
  /** Encoding du message "8bit", "7bit", "binary", "base64" */
  protected $encoding = "8bit";
  /** Type du message text/plain ou text/html */
  protected $contentType = "text/plain";
  /** Type de message : alt, plain, attachments, alt_attachments */
  protected $message_type;
  /** clé boundary */
  protected $boundary;
  protected $boundary2;
  /** séparateur de ligne */
  protected $LE = "\n";
  /** Détermine le mode debug, true : le message n'est pas envoyé */
  protected $bDebugMode = ALK_DEBUG_MAIL;

  /** nom du client messagerie qui envoie le mail */
  protected $strUserAgent;
  
  /** paramétre de l'appel à sendmail */
  protected $sendmailArgs;

  /** tableau associatif contenant des infos de publipostage globale au message { CLE => VALEUR, ... } */
  protected $tabBodyAssoc;

  /** tableau associatif contenant des infos de publipostage spécifiques à chaque destinataire : { mail => { CLE => VALEUR, ... }, ...} */
  protected $tabDestAssoc;

  /** chemin temporaire contenant la pj ics */
  protected $strPathTmpICS;
  
  /** true pour forcer l'ajout du calendrier ics à la fin du message, false par défaut pour n'ajouter le contenu que si le body est vide */
  protected $bForceIcsToBody;

  /**
   *  Contructeur par défaut
   *        L'expéditeur est automatiquement placé dans la liste répondre à.
   *
   * @param strSubject  Sujet du message, vide par défaut
   * @param strBody     Corps du message (html ou texte en fonction contentType), vide par défaut
   * @param strFromName Nom de l'expéditeur, vide par défaut
   * @param strFomMail  Adresse de l'expéditeur, vide par défaut
   * @param strToName   Nom du destinataire, vide par défaut
   * @param strToMail   Adresse du destinataire, vide par défaut
   */ 
  public function __construct($strSubject="", $strBody="", $strFromName="", $strFromMail="", $strToName="", $strToMail="")
  {
    $this->charSet = (defined("ALK_MAIL_ENCODING") ? ALK_MAIL_ENCODING : ALK_HTML_ENCODING);
    $this->setSubject($strSubject);
    $this->body = $strBody;
    $this->bForceIcsToBody = false;
    $this->altBody = "";
    $this->fromName = $strFromName;
    $this->fromMail = $strFromMail;
    $this->bSendCCi2To = false;
    $this->sendmailArgs = "-i -t";
    $this->maxSend = ( defined("ALK_MAIL_MAX_SEND") && ALK_MAIL_MAX_SEND>1 ? ALK_MAIL_MAX_SEND : 0 );    

    $this->tabTo = array();
    if( $strToName!="" && $strToMail!="" )
      $this->tabTo = array_merge($this->tabTo, array($strToMail => $strToName));

    $this->tabReplyTo = array();
    if( $strFromName!="" && $strFromMail!="" )
      $this->tabReplyTo = array_merge($this->tabReplyTo, array($strFromMail => $strFromName));

    $this->strUserAgent = "Thunderbird 2.0.0.14 (X11/20080502)";

    $this->tabCC = array();
    $this->tabCCi = array();
    $this->tabFiles = array();
    
    $this->tabBodyAssoc = array();
    $this->tabDestAssoc = array();
    
    $this->strPathTmpICS = "";
  }
  
  /**
   *  Destructeur par défaut
   */
  function __destruct()
  {
  }

  /**
   *  Se charge de réinitialiser l'objet
   *
   * @param strType Type d'initialisation
   *                all : tous les tableaux destinataires (replyto, to, cc, cci)
   *                replyto, to, cc, cci : chaque tableau individuel
   */
  function clear($strType)
  {
    switch( strToLower($strType) ) {
    case "all":
      $this->tabTo = array();
      $this->tabCC = array();
      $this->tabCCi = array();
      $this->tabReplyTo = array();
      break;
    case "to"     : $this->tabTo = array(); break;
    case "cc"     : $this->tabCC = array(); break;
    case "cci"    : $this->tabCCi = array(); break;
    case "replyto": $this->tabReplyTo = array(); break;

    case "files"  : $this->tabFiles = array(); break;
    }
  }

  /**
   * Modifie le nombre de destinataires max pour un envoi en direct.
   * @param iMaxSend  nb de destinataires max, =200 par défaut 
   */
  public function setMaxSend($iMaxSend=200)
  {
    $this->maxSend = $iMaxSend;
  }

  /**
   * Modifie les paramètres de sendmail
   * @param strArgs  nouveaux paramètres de sendmail
   */
  public function setSendMailArgs($strArgs)
  {
    $this->sendmailArgs = $strArgs;
  }

  /**
   * Accesseur de bForceIcsToBody
   * @param bForceIcsToBody  =true par défaut pour forcer l'ajout du contenu ics à la fin du message,
   *                         =false pour n'ajouter le contenu ics au message que si celui est vide
   */
  public function setForceIcsToBody($bForceIcsToBody=true)
  {
    $this->bForceIcsToBody = $bForceIcsToBody;
  }

  /**
   *  Enregistre le sujet du message
   * @param strSubject Sujet du message
   */
  public function setSubject($strSubject)
  {
    $this->subject = ( $strSubject != "" 
                       ? mb_encode_mimeheader($strSubject, $this->charSet, "Q", "\n")
                       : "" );
  }

  /**
   *  Enregistre le corps du message au format déterminé par ContentType
   * @param strBody Corps du message au format déterminé par ContentType
   */
  public function setBody($strBody)
  {
    $this->body = $strBody; 
  }

  /**
   * Met à jour le tableau d'association variable / valeur pour un publipostage global au message
   * Ce tableau contient des valeurs identiques pour tous les destinataires
   * @param tabAssoc tableau associatif de type CLE => VALEUR
   */
  public function setTabBodyAssoc($tabAssoc)
  {
    $this->tabBodyAssoc = $tabAssoc;
  }

  /**
   * Met à jour le tableau d'association variable / valeur pour un publipostage spécifique à un destinataire
   * @param tabAssoc tableau associatif de type CLE => VALEUR
   */
  private function addDestAssoc($strMail, $tabAssoc)
  {
    if( !empty($tabAssoc) ) {
      $this->tabDestAssoc[$strMail] = $tabAssoc;
    }
  }

  /**
   *  Enregistre le corps du message qui doit être au format text.
   *        Ce texte sera envoyé lorsque 
   * @param strAltBody Corps du message au format texte
   */
  public function setAltBody($strAltBody)
  {
    $this->altBody = $strAltBody;
  }

  /**
   *  Transforme le body en html et calcul 
   *        la version texte associée pour altBody
   */
  public function transformBody()
  {
    $strHtml = $this->body;
    $strHtml = mb_ereg_replace("\n", "<br>", $strHtml);
    $strHtml = mb_ereg_replace("\r", "", $strHtml);
    $this->body = $strHtml;

    $strTxt = $strHtml;
    $strTxt = mb_ereg_replace("<br>", "\n", $strTxt);
    $strTxt = mb_ereg_replace("<li>", "\t- ", $strTxt);
    $strTxt = mb_ereg_replace("</li>", "", $strTxt);

    // suppression du <head..>..</head>
    $strTxt = mb_ereg_replace("<head[^<]*>.*</head>","",$strTxt);
    
    // suppression des tag
    $tabTagD = array("html", "body", "h1", "h2", "h3", "h4", "h5", "h6", "p", "div", "span", 
                     "font", "area","embed",
                     "ul", "ol",
                     "b", "big", "em", "i", "small", "strong", "sub", "sup", "ins", "del", "s", "strike", "u",
                     "table", "tr", "td", "th", "caption", "col", "thead", "tbody", "tfoot",
                     "form", "textarea", "label", "select", "option",
                     "hr", "input", "img", "link");

    // suppression des tags
    for($i=0; $i<count($tabTagD); $i++) {
      $strTag = $tabTagD[$i];
      $strTxt = mb_ereg_replace("<".$strTag."[^<]*>|</".$strTag.">", "", $strTxt);
      $strTxt = mb_ereg_replace("<".mb_strtoupper($strTag)."[^<]*>|</".mb_strtoupper($strTag).">", "", $strTxt);
    }
    
    $tabTrans = array("â" => "&#226;",
                      "ä" => "&#228;",
                      "é" => "&#233;",
                      "è" => "&#232;",
                      "ê" => "&#234;",
                      "ë" => "&#235;",
                      "î" => "&#238;",
                      "ï" => "&#239;",
                      "ô" => "&#244;",
                      "ö" => "&#246;",
                      "ù" => "&#249;",
                      "û" => "&#251;",
                      "ü" => "&#252;",
                      "'" => "&#34;",
                      "&" => "&#38;",
                      "<" => "&#60;",
                      ">" => "&#62;",
                      " " => "&#160;",
                      "à" => "&#224;",
                      "ç" => "&#231;");
		$tabTrans = array_flip($tabTrans); 

		foreach( $tabTrans as $strRech => $strValue) {
			$strTxt = str_replace($strRech, $strValue, $strTxt);
		}

    $strTxt = mb_ereg_replace('href=([^"\'> ]*)([ >]{1})', 'href="\\1"\\2', $strTxt);

    $strTxt = mb_ereg_replace("href='([^']*)'", 'href="\\1"', $strTxt);

		//correction expression regulière GO 27/10/2006
    //$strTxt = ereg_replace('<a [^href]*href="([^"]*)"[^<]*>([^</a>]*)</a>', "\\2 : \\1", $strTxt);
    $strTxt = mb_ereg_replace('<a[^>]*href="([^"]*)"[^>]*>([^<]*)</a>', "\\2 : \\1", $strTxt);

    $this->altBody = $strTxt;
  }
  
   /**
   *  Harmonise les retours chariots du message source à envoyer
   *
   * @param str Contenu du message source
   * @return string
   */
  private function fixEOL($str) 
  {
    $str = mb_ereg_replace("\r\n", "\n", $str);
    $str = mb_ereg_replace("\r", "\n", $str);
    $str = mb_ereg_replace("\n", $this->LE, $str);
    return $str;
  }
  
  /**
   *  Formate le message source afin de ne pas avoir de ligne exédant $length caractères
   *
   * @param message  source du message à analyser
   * @param length   longeur max à ne pas dépasser
   * @param qp_mode  =false par défaut, =vrai pour ajouter = en fin de ligne
   * @return string
   */
  private function wrapText($message, $length, $qp_mode = false) 
  {
    $soft_break = ($qp_mode) ? sprintf(" =%s", $this->LE) : $this->LE;
  
    $message = $this->FixEOL($message);
    if (mb_substr($message, -1) == $this->LE)
      $message = mb_substr($message, 0, -1);
  
    $line = explode($this->LE, $message);
    $message = "";
    for ($i=0 ;$i < count($line); $i++) {
      $line_part = explode(" ", $line[$i]);
      $buf = "";
      for ($e = 0; $e<count($line_part); $e++) {
        $word = $line_part[$e];
        if ($qp_mode and (mb_strlen($word) > $length)) {
          $space_left = $length - mb_strlen($buf) - 1;
          if ($e != 0) {
            if ($space_left > 20) {
              $len = $space_left;
              if (mb_substr($word, $len - 1, 1) == "=")
                $len--;
              elseif (mb_substr($word, $len - 2, 1) == "=")
                $len -= 2;
              $part = mb_substr($word, 0, $len);
              $word = mb_substr($word, $len);
              $buf .= " " . $part;
              $message .= $buf . sprintf("=%s", $this->LE);
            } else {
              $message .= $buf . $soft_break;
            }
            $buf = "";
          }
          while (mb_strlen($word) > 0) {
            $len = $length;
            if (mb_substr($word, $len - 1, 1) == "=")
              $len--;
            elseif (mb_substr($word, $len - 2, 1) == "=")
              $len -= 2;
            $part = mb_substr($word, 0, $len);
            $word = mb_substr($word, $len);
            
            if (mb_strlen($word) > 0)
              $message .= $part . sprintf("=%s", $this->LE);
            else
              $buf = $part;
          }
        } else {
          $buf_o = $buf;
          $buf .= ($e == 0) ? $word : (" " . $word); 
          
          if (mb_strlen($buf) > $length and $buf_o != "") {
            $message .= $buf_o . $soft_break;
            $buf = $word;
          }
        }
      }
      $message .= $buf . $this->LE;
    }
    
    return $message;
  }

  /**
   *  Positionne le ContentType à text/html (true) ou à text/plain (false)
   *
   * @param bContentType booléen 
   */
  function setHtml($bContentType)
  {
    if( $bContentType == true )
      $this->contentType = "text/html";
    else
      $this->contentType = "text/plain";
  }

  /**
   *  Positionne le ContentType à text/html (true) ou à text/plain (false)
   *
   * @param bContentType booléen 
   */
  function setContentType($strContentType)
  {
    $this->contentType = $strContentType;
  }

  /**
   *  Positionne le ContentType à text/html (true) ou à text/plain (false)
   *
   * @param iPriority Priorité du message de 5 (faible) à 1 (haute) =3 par défaut (normal)
   */
  function setPriority($iPriority)
  {
    if( !is_numeric($iPriority) ) return;
    if( $iPriority!=1 && $iPriority!=2 && $iPriority!=3 && $iPriority!=4 && $iPriority!=5 ) return;
    $this->priority = $iPriority;
  }

  /**
   *  Enregistre l'expéditeur du message
   *        Gère la conformité de l'adresse mail.
   *
   * @param strName  Nom d'une personne
   * @param strMail  Adresse mail de cette personne 
   * @param bReplyTo Ajoute (=true par défaut) ou (=false) non cette personne dans la liste répondre à.
   * @return retourne un entier :
   *          1 : ajout
   *          0 : adresse mail non valide
   */
  public function setFrom($strName, $strMail, $bReplyTo=true)
  {
    if( $strMail=="" )
      return 0;

    $tabMail = explode(",", $strMail);
    if( count($tabMail) == 1 ) {
      $tabMail = explode(";", $strMail);
    }
    // on ne garde que la première adresse pour le from
    $strMail = trim($tabMail[0]);

    if( $strMail=="" || $this->_VerifyMail($strMail)==false )
      return 0;
    
    $this->fromName = $strName;
    $this->fromMail = $strMail;
    if( $bReplyTo == true && !array_key_exists($strMail, $this->tabReplyTo) ) 
      $this->AddReplyTo($strName, $strMail);
    return 1;
  }

  /**
   *  Ajoute un destinataire à la liste
   *        Gère les éventuels doublons et la conformité de l'adresse mail.
   *
   * @param strName  Nom d'une personne
   * @param strMail Adresse mail de cette personne 
   * @param tabAssoc informations de publipostage dédiés au destinataire
   * @return retourne un entier :
   *          1 : ajout
   *          0 : adresse mail non valide
   *         -1 : adresse existe déjà
   */
  public function addTo($strName, $strMail, $tabAssoc=array())
  {
    if( $strMail=="" )
      return 0;

    $tabMail = explode(",", $strMail);
    if( count($tabMail) == 1 ) {
      $tabMail = explode(";", $strMail);
    }
    $nbMailAdd = 0;
    foreach($tabMail as $strM ) {
      $strM = trim($strM);
      if( $strM=="" || $this->_VerifyMail($strM)==false )
        continue;

      if( !array_key_exists($strM, $this->tabTo) ) {
        $this->tabTo = array_merge($this->tabTo, array($strM => $strName));
        $nbMailAdd++;
        $this->addDestAssoc($strM, $tabAssoc);
      }
    }
    return ( $nbMailAdd>0 ? 1 : 0 );
  }

  /**
   *  Ajoute un destinataire en copie à la liste.
   *        Gère les éventuels doublons et la conformité de l'adresse mail.
   *
   * @param strName  Nom d'une personne
   * @param strMail Adresse mail de cette personne 
   * @param tabAssoc informations de publipostage dédiés au destinataire
   * @return retourne un entier :
   *          1 : ajout avec succès
   *          0 : adresse mail non valide
   *         -1 : adresse existe déjà
   */
  public function addCC($strName, $strMail, $tabAssoc=array())
  {
    if( $strMail=="" )
      return 0;

    $tabMail = explode(",", $strMail);
    if( count($tabMail) == 1 ) {
      $tabMail = explode(";", $strMail);
    }
    $nbMailAdd = 0;
    foreach($tabMail as $strM ) {
      $strM = trim($strM);
      if( $strM=="" || $this->_VerifyMail($strM)==false )
        continue;

      if( !array_key_exists($strM, $this->tabCC) ) {
        $this->tabCC = array_merge($this->tabCC, array($strM => $strName));
        $nbMailAdd++;
        $this->addDestAssoc($strM, $tabAssoc);
      }
    }
    return ( $nbMailAdd>0 ? 1 : 0 );
  }

  /**
   *  Ajoute un destinataire en copie cachée à la liste
   *        Gère les éventuels doublons et la conformité de l'adresse mail.
   *
   * @param strName  Nom d'une personne
   * @param strMail Adresse mail de cette personne 
   * @param tabAssoc informations de publipostage dédiés au destinataire
   * @return retourne un entier :
   *          1 : ajout avec succès
   *          0 : adresse mail non valide
   *         -1 : adresse existe déjà
   */
  public function addCCi($strName, $strMail, $tabAssoc=array())
  {
    if( $strMail=="" )
      return 0;

    $tabMail = explode(",", $strMail);
    if( count($tabMail) == 1 ) {
      $tabMail = explode(";", $strMail);
    }
    $nbMailAdd = 0;
    foreach($tabMail as $strM ) {
      $strM = trim($strM);
      if( $strM=="" || $this->_VerifyMail($strM)==false )
        continue;

      if( !array_key_exists($strM, $this->tabCCi) ) {
        $this->tabCCi = array_merge($this->tabCCi, array($strM => $strName));
        $nbMailAdd++;
        $this->addDestAssoc($strM, $tabAssoc);
      }
    }
    return ( $nbMailAdd>0 ? 1 : 0 );
  }

  /**
   *  Ajoute un destinataire pour répondre à
   *        Gère les éventuels doublons et la conformité de l'adresse mail.
   *
   * @param strName  Nom d'une personne
   * @param strMail Adresse mail de cette personne 
   * @return retourne un entier :
   *          1 : ajout avec succès
   *          0 : adresse mail non valide
   *         -1 : adresse existe déjà
   */
  public function addReplyTo($strName, $strMail)
  {
    if( $strMail=="" )
      return 0;

    $tabMail = explode(",", $strMail);
    if( count($tabMail) == 1 ) {
      $tabMail = explode(";", $strMail);
    }

    $nbMailAdd = 0;
    foreach($tabMail as $strM ) {
      $strM = trim($strM);
      if( $strM=="" || $this->_VerifyMail($strM)==false )
        continue;

      if( !array_key_exists($strM, $this->tabReplyTo) ) {
        $this->tabReplyTo = array_merge($this->tabReplyTo, array($strM => $strName));
        $nbMailAdd++;
      }
    }
    return ( $nbMailAdd>0 ? 1 : 0 );
  }

  /**
   *  Ajoute une piece jointe à la liste
   *
   * @param strPathFileName Chemin complet du fichier
   * @param strFileName     Nom de base du fichier
   */
  public function addFile($strPathFileName, $strFileName)
  {
    if( file_exists($strPathFileName) && is_file($strPathFileName) ) {
      $cur = count($this->tabFiles);
      $this->tabFiles[$cur][0] = $strPathFileName;
      if( mb_strlen($strFileName)>30 ) {
        // tronque la longueur du nom de fichier à 30 caractères
        $iPosExt = mb_strrpos($strFileName, ".");
        $iNum = $cur+1;
        $iNumlen = mb_strlen("".$iNum."");
        if( $iPosExt == false ) {
          $strFileName = mb_substr($strFileName, 0, 30-$iNumlen).$iNum;
        } else {
          $strFileName = mb_substr($strFileName, 0, 30-$iNumlen-(mb_strlen($strFileName)-$iPosExt)).
            $iNum.mb_substr($strFileName, $iPosExt);
        }
      }
      $this->tabFiles[$cur][1] = $strFileName;
    }
  }
  
  /**
   * Ajoute un événement au calendrier ICS du mail
   * 
   * @param tabParamsEvent  tableau des informations relatives à l'événement indexé de la façon suivante :
   *          "objet"       => string objet de l'événement         
   *          "event_desc"  => string description de l'événement
   *          "location"    => string endroit de l'événement
   *          "datedeb"     => string date de début de l'événement (DD/MM/YYYY)
   *          "heuredeb"    => string heure de début de l'événement (HH:MI:SS)
   *          "datefin"     => string date de fin de l'événement (DD/MM/YYYY)
   *          "heurefin"    => string heure de fin de l'événement (HH:MI:SS)
   *          "alarm_desc"  => string description de l'alarme
   *          "alarm_time"  => heure de déclenchement de l'alarme avant début de l'événement
   */
  public function addEventToIcs($tabParamsEvent=array())
  {
    $this->tabIcs[] = $tabParamsEvent;
  }
  
  /**
   * Envoi le message.
   * Si le nombre de destinataire est supérieur à ALK_MAIL_MAX_SEND et ALK_MAIL_MAX_SEND>1,
   * alors send() appelle sendMailToQueue() pour l'envoyer en différé
   *
   * @param msg_id  identifiant du message, =-1 par défaut.
   * @param bWithNotifyToReply  false par défaut, true pour retourner à l'adresse reply, les éventuels erreurs
   *                             le replyeur est celui du message, sinon celui de l'extranet (ALK_MAIL_REPLY_ERR), sinon pas de retour d'erreur 
   * @return Retourne un entier :
   *         >0 : message envoyé avec succès, le nombre correspond au nombre de destinataires
   *         0  : message non envoyé
   *        -1  : Aucun destinataire
   *        -2  : Problème dans la construction du body (fichier attaché, etc..)
   *        -3  : impossible de placer l'envoi en file d'attente
   *        <-10: message placé en attente d'envoi avec succès
   */
  public function send($msg_id=-1, $bWithNotifyToReply=false)
  {
    if( !defined("ALK_CMD_SENDMAIL") ) define("ALK_CMD_SENDMAIL", "/usr/sbin/sendmail");
    
    $nbDest = count($this->tabTo) + count($this->tabCC) + count($this->tabCCi); 
    if( $nbDest < 1) {
      return -1;
    }
    if( $this->maxSend>0 && $nbDest > $this->maxSend ) {
      $iRes = $this->sendMailToQueue($msg_id, $bWithNotifyToReply);
      if( $iRes > 0 ) {
        $iRes = -$iRes-10;
      }
      return $iRes;
    }
    
    if( count($this->tabTo) == 0 ) {
    	$this->AddTo($this->fromName, $this->fromMail);
    }	  

    if( $this->altBody != "" )
      $this->ContentType = "multipart/alternative";

    $strMailTo = "";
    if( $this->bSendCCi2To == false ) {
	    foreach($this->tabTo as $strMail => $strName)
	      $strMailTo .= ",".$strMail;
	    if( $strMailTo != "" )
  	    $strMailTo = mb_substr($strMailTo, 1);
    }      

    $this->_createICS();

    $strHeader = "";
    $strHeader .= sprintf("Date: %s%s", $this->_GetRfcDate(), $this->LE);
    $strHeader .= $this->_createHeader();
    $strBody = $this->_createBody();
    if( is_bool($strBody) ) {
      $this->_cleanICS();
      return -2;
    }

    if( !$this->bDebugMode ) {
      //echo "$strMailTo<br>$this->subject<br>body=<br>".$this->WrapText($strBody, 80)."<br>header=<br>".$strHeader;
      $bRes = mail($strMailTo, $this->subject, $this->wrapText($strBody, 80), $strHeader);
      $this->_cleanICS();
      return ( $bRes == true ? $nbDest : 0);
    }
    $this->_cleanICS();
    return 0;
  }

  /**
   * Construit la demande d'envoi de mail et la place en file d'attente
   * @param msg_id       identifiant du message
   * @param bWithNotify  false par défaut, true pour retourner à l'adresse reply, les éventuels erreurs
   *                     le replyeur est celui du message, sinon celui de l'extranet (ALK_MAIL_REPLY_ERR), sinon pas de retour d'erreur 
   * @return int
   *   >0  : message envoyé avec succès, le nombre correspond au nombre de destinataires
   *   = 0 : message non envoyé
   *   =-1 : Aucun destinataire
   *   =-2 : Problème dans la construction du body (fichier attaché, etc..)
   *   =-3 : impossible de placer l'envoi en file d'attente
   */
  public function sendMailToQueue($msg_id, $bWithNotifyToReply=false)
  {
    if( !defined("ALK_CMD_SENDMAIL") ) define("ALK_CMD_SENDMAIL", "/usr/sbin/sendmail");
    if( !defined("ALK_CMD_PHP") ) define("ALK_CMD_PHP", "/usr/bin/php");
    
    if( $this->bDebugMode ) {
      return 0;
    }
    
    $tabRes = array();
    $nbDest = count($this->tabTo) + count($this->tabCC) + count($this->tabCCi); 
    if( $nbDest < 1) {
      return -1;
    }
    if( $this->altBody != "" )
      $this->ContentType = "multipart/alternative";
    
    // concaténation de tous les destinataires dans tabTo
    foreach($this->tabCC as $strMail => $strName) {
      $this->tabTo[$strMail] = $strName;
    }
    $this->tabCC = array();
    foreach($this->tabCCi as $strMail => $strName) {
      $this->tabTo[$strMail] = $strName;
    }
    $this->tabCCi = array();

    $strFrom = sprintf('"%s" <%s>', mb_encode_mimeheader(addslashes($this->fromName)), $this->fromMail);
    
    $strReply = "";
    $strReplyMail = "";
    if( !empty($this->tabReplyTo) ) {
      list($mailReply, $nameReply) = each($this->tabReplyTo);
      $strReply = sprintf('%s <%s>', mb_encode_mimeheader(addslashes($nameReply)), $mailReply);
      $strReplyMail = $mailReply; 
    }
    if( defined("ALK_MAIL_REPLY_ERR") && ALK_MAIL_REPLY_ERR!="" &&
        ($strReplyMail == "" || $strReplyMail==ALK_MAIL_NOREPLY_MAIL) ) {
      $strReply = ALK_MAIL_REPLY_ERR;
      $strReplyMail = ALK_MAIL_REPLY_ERR;
    }
    
    $strPathCmd = ALK_ALKANET_ROOT_PATH.ALK_ROOT_UPLOAD."queue/";
    $strBaseFileName = date("YmdHis")."_mail_".( $msg_id == -1 ? rand(0, 1000) : $msg_id );
    $strQueueFileName     = $strPathCmd.$strBaseFileName.".queue"; // liste des cmd sendmail
    $strMailFileName      = $strPathCmd.$strBaseFileName.".queue.mail"; // message à envoyer
    $strMailEndFileName   = $strPathCmd.$strBaseFileName.".queue.mail.end";   // message accusé fin d'envoi
    
    $hMailQueue        = fopen($strQueueFileName, "w");
    $fMailContent      = fopen($strMailFileName, "w");
    $fMailContentEnd   = fopen($strMailEndFileName, "w");
    
    if( !($hMailQueue && $fMailContent && $fMailContentEnd) ) {
      return -3;
    } 

    // enregistrement du message à envoyer dans un fichier
    $strHtmlBody = $this->getMailSourceCode();
    if( is_bool($strHtmlBody) ) {
      return -2;
    }
    fwrite($fMailContent, $strHtmlBody);
    fclose($fMailContent);

    // enregistrement du message de confirmation de fin de l'envoi dans un fichier
    $strMsgHtml = '<p>Bonjour,<br/><br/>'.
      'Le message suivant vient d\'être envoyé :</br>'.
      '<ul>' .
      '<li>Titre : '.$this->subject.'</li>'.
      '<li>Nb de destinataires : '.count($this->tabTo).'</li>'.
      '<li>Numéro d\'identification : '.$strBaseFileName.'</li>'.
      '</ul>'.
      ( $bWithNotifyToReply && $strReply!=""
        ? 'Les éventuelles erreurs seront automatiquement renvoyées vers l\'adresse suivante : '.$strReplyMail.'<br/>'
        : '' ).
      '<br/>' .
      'Cordialement<br/>'.
      '<br/>' .
      ALK_APP_TITLE.'<br/><br/>'.
      '<i>Ce message est généré puis envoyé automatiquement par '.ALK_APP_TITLE.'</i></p>';

    $oAlkMailEnd = new AlkMail();
    $oAlkMailEnd->setFrom($this->fromName, $this->fromMail);
    $oAlkMailEnd->setSubject(ALK_APP_TITLE." : confirmation de l'envoi du message id=".$strBaseFileName);
    $oAlkMailEnd->setHtml(true);
    $oAlkMailEnd->setBody($strMsgHtml);
    $oAlkMailEnd->transformBody();
    $strHtmlBodyEnd = $oAlkMailEnd->getMailSourceCode();
    if( is_string($strHtmlBodyEnd) ) { 
      fwrite($fMailContentEnd, "To: ".$strFrom.$this->LE.$strHtmlBodyEnd);
    }
    fclose($fMailContentEnd);
    $oAlkMailEnd = null;
    
    // création du fichier de cmd d'envoi
    // pose un lock
    fwrite($hMailQueue, "touch ".$strQueueFileName.".lock\n");
    fwrite($hMailQueue, "dateStart=`date '+_%d_%m_%y__%H_%M_%S'`\n");
    
    foreach($this->tabTo as $strMail => $strName) {
      $strMail = trim($strMail);
      $strName = trim($strName);
      $strCmd = ALK_CMD_SENDMAIL.
        ( $this->sendmailArgs != ""
          ? ' '.$this->sendmailArgs 
          : '').
        ( defined("ALK_MAIL_SENDER") && ALK_MAIL_SENDER!="" 
          ? ' -L '.ALK_MAIL_SENDER.'${dateStart}'
          : '' ).
        ( $bWithNotifyToReply && $strReply!=""
          ? ' -f'.escapeshellarg($strReply)
          : '' );    
      
      $tabAssoc = ( isset($this->tabDestAssoc[$strMail]) ? $this->tabDestAssoc[$strMail] : array() );
      fwrite($hMailQueue, 'sed "1 i To: \\"'.$strName.'\\" <'.$strMail.'>'.
                          $this->getConvertSED($this->tabBodyAssoc).
                          $this->getConvertSED($tabAssoc).'" '.$strMailFileName.
                          ' | '.$strCmd."\n");

      $iTimeSleep = 1;
      /*$iTimeSleep = floor(strlen($strHtmlBody)/(???*1024));
      $iTimeSleep = ( $iTimeSleep <= 0 ? 1 : $iTimeSleep );*/ 
      fwrite($hMailQueue, "sleep ".$iTimeSleep."\n");
    }

    $strCmdSEMail = ALK_CMD_SENDMAIL.
        ( $this->sendmailArgs != ""
          ? ' '.$this->sendmailArgs 
          : '').
        ( defined("ALK_MAIL_SENDER") && ALK_MAIL_SENDER!="" 
          ? ' -L '.ALK_MAIL_SENDER.'${dateStart}'
          : '' );
              
    // envoi du message de confirmation d'envoi    
    fwrite($hMailQueue, $strCmdSEMail.' < '.$strMailEndFileName."\n");
    // suppression des fichiers nécessaires à l'envoi
    //fwrite($hMailQueue, 'rm -f '.$strMailFileName.' '.$strQueueFileName." ".$strQueueFileName.".lock ".$strMailEndFileName."\n");
    fwrite($hMailQueue, "mv ".$strQueueFileName." ".$strQueueFileName.".done\n");
    fwrite($hMailQueue, "rm -f ".$strQueueFileName.".lock\n");
    fwrite($hMailQueue, "cd ".ALK_ALKANET_ROOT_PATH."scripts/alkanet/task/\n");
    fwrite($hMailQueue, ALK_CMD_PHP." saveMsgSent.php ".$msg_id."\n");
    fclose($hMailQueue);
    
    // envoi du message de confirmation de mise en file d'attente
    $strMsgHtml = '<p>Bonjour,<br/><br/>'.
      'Vous venez de valider l\'envoi du message suivant le '.date("d/m/Y H:i:s").' :</br>'.
      '<ul>' .
      '<li>Titre : '.$this->subject.'</li>'.
      '<li>Nb de destinataires : '.count($this->tabTo).'</li>'.
      '<li>Numéro d\'identification : '.$strBaseFileName.'</li>'.
      '</ul>'.
      'Ce message a été déposé dans une file d\'attente et sera traité très prochainement.<br/>'.
      'Vous serez informé par messagerie lorsque l\'envoi sera achevé.</br>'.
      '<br/>' .
      'Cordialement<br/>'.
      '<br/>' .
      ALK_APP_TITLE.'<br/><br/>'.
      '<i>Ce message est généré puis envoyé automatiquement par '.ALK_APP_TITLE.'</i></p>';
      
    $oAlkMailStart = new AlkMail();
    $oAlkMailStart->setFrom($this->fromName, $this->fromMail);
    $oAlkMailStart->setSubject(ALK_APP_TITLE." : confirmation de la mise en file d'attente du message id=".$strBaseFileName);
    $oAlkMailStart->setHtml(true);
    $oAlkMailStart->setBody($strMsgHtml);
    $oAlkMailStart->transformBody();
    $strHtmlBodyStart = $oAlkMailStart->getMailSourceCode();
    
    if( is_string($strHtmlBodyStart) ) {
      $strCmdSEMail = str_replace('${dateStart}', date("_d_m_Y__H_i_s"), $strCmdSEMail);
      $hStartMail = @popen($strCmdSEMail, "w");
      if( $hStartMail ) {
        fwrite($hStartMail, "To: ".$strFrom.$this->LE.$strHtmlBodyStart);
        pclose($hStartMail); 
      }
    }
    $oAlkMailStart = null;
    
    return count($this->tabTo);
  }
  
  /**
   * Retourne le code source du message à envoyer sans les destinataires
   * @return string si ok,
   */
  public function getMailSourceCode()
  {
    // prépa de l'entête 
    $this->_createICS();        
        
    $strHeader = "";
    $strHeader .= sprintf("Date: %s%s", $this->_GetRfcDate(), $this->LE);
    $strHeader .= $this->_createHeader();    

    // prépa du corps du message
    $strBody = $this->_createBody();
    $this->_cleanICS();
    if( is_bool($strBody) ) {
      return false;
    }
    
    return 'Subject: '.$this->subject.$this->LE.
      $strHeader.$this->LE.$this->LE.
      $this->wrapText($strBody, 80);
  }

  /**
   * Retourne la partie paramétre de la commande SED pour convertir un tag publipostable par sa valeur
   * @param
   * @return string
   */
  private function getConvertSED($tabAssoc)
  {
    $strRes = "";
    foreach($tabAssoc as $strPattern => $strValue) {
      
      $strPattern = str_replace("[", "\\[", $strPattern);
      $strPattern = str_replace("]", "\\]", $strPattern);
      
      $strRes .= "\n".
        ' s/'.$strPattern.'/'.str_replace('"','', addslashes($strValue)).'/g';
    }
    return $strRes;
  }
      
  /**
   *  Retourne la liste des adresses mails à partir 
   *        de l'un des tableaux 
   * 
   * @param strType Type de destinataires contenu dans le tableau passé en second paramètre
   *                Valeur possible : Cc, Bcc, Reply-to
   * @param tabAddr Tableau association (nom => mail)
   * @param iFirst  Indice du premier destinataire
   * @param iLast   Indice du dernier destinataire
   * @return Retourne une chaine de caractère correspondant à la liste des destinataires 
   *         contenue dans le tableau tabAddr
   */
  private function _addrAppend($strType, $tabAddr, $iFirst=-1, $iLast=-2)
  {
    $strRes = $strType.": ";
    $nb = 0;

    if( $iFirst==-1 &&  $iLast==-2 ) {
      foreach($tabAddr as $strMail => $strName) {
        if( $nb>0 ) $strRes .= ",";
        $strRes .= sprintf('"%s" <%s>', mb_encode_mimeheader(addslashes($strName)), $strMail);
        $nb++;
      }
    } else {
      $tabKeys = array_keys($tabAddr);
      for($i=$iFirst; $i<=$iLast; $i++) {
        if( $nb>0 ) $strRes .= ",";
        $strMail = $tabKeys[$i];
        $strName = $tabAddr[$strMail];
        $strRes .= sprintf('"%s" <%s>', mb_encode_mimeheader(addslashes($strName)), $strMail);
        $nb++;
      }
    }

    $strRes .= $this->LE;
    return $strRes;
  }

  /**
   * génère et ajoute le fichier ICS en pièce jointe
   * Ajoute le contenu au message si celui-ci est vide ou si on le force
   */
  private function _createICS()
  {
    if( empty($this->tabIcs) ) return;
      
    $strICSFileName = "calendar.ics";
    $this->strPathTmpICS = sys_get_temp_dir()."/".uniqid("ALK_ICS");
    if( !(file_exists($this->strPathTmpICS) && is_dir($this->strPathTmpICS)) ) {
      $bRes = mkdir($this->strPathTmpICS, 0755);
      if( $bRes ) {
        $strPathFileICS = $this->strPathTmpICS."/".$strICSFileName;
        if( $hFileIcs = @fopen($strPathFileICS, "w") ) {

          $oIcs = AlkFactory::getIcs();
          foreach ( $this->tabIcs as $tabParamsEvent ) {
            $oIcs->addEvent($tabParamsEvent);
          }

          @fwrite($hFileIcs, $oIcs->getContentFile());
          @fclose($hFileIcs);
          $this->addFile($strPathFileICS, $strICSFileName);

          if( $this->bForceIcsToBody || ($this->altBody == "" && $this->body == "") ) {
            $this->altBody .= $oIcs->getTextContent();
            $this->body .= $oIcs->getHtmlContent();
          }
        }
      }
    }
  }

  /**
   * Supprime les fichiers temporaires ICS éventuellement créés
   * @param bRmBash  =false par défaut pour effectuer la suppression en directe via php
   *                 =true pour retourner la cmd bash permettant d'effectuer la suppression en différé
   */
  private function _cleanICS()
  {
    if( $this->strPathTmpICS == "" ) return;
    
    $strICSFileName = "calendar.ics";
    $strPathFileICS = $this->strPathTmpICS."/".$strICSFileName;
    if( file_exists($strPathFileICS) && is_file($strPathFileICS) ) {
      @unlink($strPathFileICS);
    }
    if( file_exists($this->strPathTmpICS) && is_dir($this->strPathTmpICS) ) {
      @rmdir($this->strPathTmpICS);
    }
  }

  /**
   *  Création de l'entete du message
   *
   * @param iFirst  Indice du premier destinataire CCi
   * @param iLast   Indice du dernier destinataire CCi
   */
  private function _createHeader($iFirst=-1, $iLast=-2)
  {
    $header = array();
    $uniq_id = md5(uniqid(time()));
    $this->boundary  = "b1_".$uniq_id;
    $this->boundary2 = "b2_".$uniq_id;

    $header[] = sprintf("From: \"%s\" <%s>%s", 
                        mb_encode_mimeheader(addslashes($this->fromName)), trim($this->fromMail), $this->LE);

    if( count($this->tabCC) > 0 && $this->bSendCCi2To == false )
      $header[] = $this->_AddrAppend("Cc", $this->tabCC);

    if( count($this->tabCCi) > 0  && $this->bSendCCi2To == false )
      $header[] = $this->_AddrAppend("Bcc", $this->tabCCi, $iFirst, $iLast);

    $header[] = $this->_AddrAppend("Reply-to", $this->tabReplyTo);

    $header[] = sprintf("X-Priority: %d%s", $this->priority, $this->LE);
    $header[] = sprintf("Return-Path: %s%s", trim($this->fromMail), $this->LE);

    // spécifique microsoft
    if( $this->priority == 1 )
      $MSPriority = "High";
    elseif( $this->priority == 5 )
      $MSPriority = "Low";
    else
      $MSPriority = "Medium";
    //$header[] = sprintf("X-MSMail-Priority: %s%s", $MSPriority, $this->LE);
    $header[] = sprintf("Importance: %s%s", $MSPriority, $this->LE);
    
    $header[] = sprintf("User-Agent: %s%s", $this->strUserAgent, $this->LE);
    //$header[] = sprintf("X-Mailer: %s%s", $this->strUserAgent, $this->LE);
    
    $header[] = sprintf("MIME-Version: 1.0%s", $this->LE);
    
    // determine le type de message
    if( count($this->tabFiles) < 1 && mb_strlen($this->altBody) < 1) {
      $this->message_type = "plain";
    } else {
      if(count($this->tabFiles) > 0)
        $this->message_type = "attachments";
      if(mb_strlen($this->altBody) > 0 && count($this->tabFiles) < 1)
        $this->message_type = "alt";
      if(mb_strlen($this->altBody) > 0 && count($this->tabFiles) > 0)
        $this->message_type = "alt_attachments";
    }

    switch( $this->message_type ) {
    case "plain":
      $header[] = sprintf("Content-Transfer-Encoding: %s%s", $this->encoding, $this->LE);
      $header[] = sprintf("Content-Type: %s; charset = \"%s\"", $this->contentType, $this->charSet);
      break;
    case "attachments":
    case "alt_attachments":
      $header[] = sprintf("Content-Type: %s;%s", "multipart/mixed", $this->LE);
      $header[] = sprintf("\tboundary=\"%s\"%s", $this->boundary, $this->LE);
      break;
    case "alt":
      $header[] = sprintf("Content-Type: %s;%s", "multipart/alternative", $this->LE);
      $header[] = sprintf("\tboundary=\"%s\"%s", $this->boundary, $this->LE);
      break;
    }

    return join("", $header);
  }

  /**
   * Créé puis retourne le corps du message
   * @return string si ok, false sinon
   */
  private function _createBody()
  {
    $body = array();

    /* nécessaire pour éviter la qualification spam */
    if( stripos($this->body, "<body") === false ) {
      $bCssExists = ( file_exists(ALK_ALKANET_ROOT_PATH."styles/mailing.css") && is_file(ALK_ALKANET_ROOT_PATH."styles/mailing.css") );
      $this->body = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">".
        "<HTML><HEAD>".
        "<META http-equiv=\"content-type\" content=\"text/html; charset=".$this->charSet."\"/>".
        ( $bCssExists
          ? "<link rel=\"stylesheet\" type=\"text/css\" href=\"".ALK_ALKANET_ROOT_URL."styles/mailing.css\" />"
          : "" ).
        "<STYLE></STYLE>".
        "</HEAD>".
        "<BODY>".$this->body."</BODY></HTML>";
    }
    
    switch( $this->message_type ) {
    case "alt":
      // Retourne texte du body
      $body[] = $this->_GetMimeSource($this->boundary, true, "text/plain");
      $body[] = sprintf("%s%s", $this->altBody, $this->LE.$this->LE);
      $body[] = $this->_GetMimeSource($this->boundary, true, "text/html");
      $body[] = sprintf("%s%s", $this->body, $this->LE.$this->LE);
      // fin boundary
      $body[] = sprintf("%s--%s--%s", $this->LE, $this->boundary, $this->LE.$this->LE);
      break;

    case "plain":
      $body[] = $this->body;
      break;

    case "attachments":
      $body[] = sprintf("%s%s%s%s", $this->_GetMimeSource($this->boundary, false), $this->LE, $this->body, $this->LE);
     
      if( !$body[] = $this->_attachAll() )
        return false;
      break;

    case "alt_attachments":
      $body[] = sprintf("--%s%s", $this->boundary, $this->LE);
      $body[] = sprintf("Content-Type: %s;%s"."\tboundary=\"%s\"%s",
                        "multipart/alternative", $this->LE, 
                        $this->boundary2, $this->LE.$this->LE);
    
      // texte du body
      $body[] = $this->_GetMimeSource($this->boundary2, true, "text/plain").$this->LE;
      $body[] = sprintf("%s%s", $this->altBody, $this->LE.$this->LE);
    
      // HTML du body
      $body[] = $this->_GetMimeSource($this->boundary2, true, "text/html").$this->LE;
      $body[] = sprintf("%s%s", $this->body, $this->LE.$this->LE);
      $body[] = sprintf("%s--%s--%s", $this->LE, $this->boundary2, $this->LE.$this->LE);
      
      if( !$body[] = $this->_attachAll() )
        return false;
      break;
    }

    // Add the encode string code here
    $sBody = join("", $body);
    $sBody = $this->_getEncodeString($sBody, $this->encoding);
    return $sBody;
  }

  /**
   *  Retourne le source du boundary.
   *
   * @param boudary     clé boundary en cours
   * @param bLineEnding true par défaut. Ajoute un saut de ligne à la fin si =true.
   * @param contentType contentType en cours
   * @param disposition 
   * @param fileName
   * @return Retourne le source du message correspondant
   */
  private function _getMimeSource($boundary, $bLineEnding = true, $contentType="", $disposition="", $fileName="") 
  {
    $ret = array();
    if( $contentType == "" ) $contentType = $this->contentType;

    $mime[] = sprintf("--%s%s", $boundary, $this->LE);
    $mime[] = sprintf("Content-Type: %s; charset=\"%s\"%s", $contentType, $this->charSet, $this->LE);
    $mime[] = sprintf("Content-Transfer-Encoding: %s%s", $this->encoding, $this->LE);
        
    if( mb_strlen($disposition) > 0 ) {
      $mime[] = sprintf("Content-Disposition: %s;", $disposition);
      if( mb_strlen($fileName) > 0 )
        $mime[] = sprinf("filename=\"%s\"", $fileName);
    }
        
    if( $bLineEnding ==true )
      $mime[] = $this->LE;
    
    return join("", $mime);
  }

  /**
   *  Attache tous les fichiers joints au message. Les fichiers sont codés en base64 par défaut.
   *
   * @return Retourne Le source du message contenant toutes les pièces jointes si succès.
   *         Retourne false sinon
   */
  private function _attachAll()
  {
    $mime = array();
    
    $type = "application/octet-stream";
    $disposition = "attachment";
    $bOk = false;

    // Ajoute ts les fichiers joints
    for($i = 0; $i < count($this->tabFiles); $i++) {
      $strPathFilename = $this->tabFiles[$i][0];
      $strFilename = $this->tabFiles[$i][1];

      $type = GetTypeMime($strPathFilename);

      $mime[] = sprintf("--%s%s", $this->boundary, $this->LE);
      if( $type == "text/calendar" ) {
        $encoding = "quoted-printable";
        $mime[] = sprintf("Content-Type: %s; charset=%s; method=REQUEST; name=\"%s\"%s", $type, $this->charSet, $strFilename, $this->LE);
        $mime[] = sprintf("Content-Transfer-Encoding: %s%s", $encoding, $this->LE.$this->LE);
      } 
      else {
        $encoding = "base64";
        $mime[] = sprintf("Content-Type: %s; name=\"%s\"%s", $type, $strFilename, $this->LE);
        $mime[] = sprintf("Content-Transfer-Encoding: %s%s", $encoding, $this->LE);
        $mime[] = sprintf("Content-Disposition: %s; filename=\"%s\"%s", $disposition, $strFilename, $this->LE.$this->LE);
      }

      if( !$mime[] = sprintf("%s%s", $this->_getEncodeFile($strPathFilename, $encoding), $this->LE.$this->LE) )
        return false;
      $bOk = true;  
    }

    if( $bOk ) { 
      $mime[] = sprintf("--%s--%s", $this->boundary, $this->LE);
    }
    
    return join("", $mime);
  }
  
  /**
   *  Encode le fichier attaché dans le format passé en paramètre.
   *
   * @param strPathFilename Chemin complet local vers le fichier à joindre
   * @param encoding        type d'encodage (base64, 7bit, 8bit, binary) = base64 par défaut
   * @return Retourne la chaine encodée si succès, false sinon
   */
  private function _getEncodeFile($strPathFilename, $encoding = "base64")
  {
    if( !@$hFile = fopen($strPathFilename, "rb") )
      return false;

    $strFileAttachment = fread($hFile, filesize($strPathFilename));
    $strEncoded = $this->_getEncodeString($strFileAttachment, $encoding);
    fclose($hFile);

    return $strEncoded;
  }

  /**
   *  Encode la chaine de caractères dans le format passé en paramètre.
   *
   * @param str      Chaine de caractère à encoder
   * @param encoding type d'encodage (base64, 7bit, 8bit, binary) = base64 par défaut
   * @return Retourne la chaine codée
   */
  function _getEncodeString($str, $encoding = "base64") 
  {
    switch( strtolower($encoding) ) {
    case "base64":
      // chunk_split is found in PHP >= 3.0.6
      $encoded = chunk_split(base64_encode($str));
      break;

    case "7bit":
    case "8bit":
      $encoded = $this->FixEOL($str);
      if (mb_substr($encoded, -2) != $this->LE)
        $encoded .= $this->LE;
      break;

    case "binary":
      $encoded = $str;
      break;
      
    case "quoted-printable":
      $encoded = quoted_printable_encode($str);
      break;
      
    default:
      return false;
    }
    return $encoded;
  }

  /**
   *  Retourne la date au format RFC 822
   *
   * @return Retourne une chaine de caractère.
   */
  private function _getRfcDate() 
  {
    $tz = date("Z", AlkFactory::getLocalDate());
    $tzs = ($tz < 0) ? "-" : "+";
    $tz = abs($tz);
    $tz = ($tz/3600)*100 + ($tz%3600)/60;
    $date = sprintf("%s %s%04d", date("D, j M Y H:i:s", AlkFactory::getLocalDate()), $tzs, $tz);
    return $date;
  }

  /**
   *  Test l'adresse mail. L'adresse peut être corrigée en cas
   *        de présence d'espace en début ou de fin.
   *      
   * @param strMail Adresse mail à vérifier passée en référence.
   * @return Retourne un booléen. True si succès, false sinon.
   */
  private function _verifyMail(&$strMail)
  {
    $strRegexp = "^([-!#\$%&'*+./0-9=?A-Z^_`a-z{|}~^?])".
      "+@([-!#\$%&'*+/0-9=?A-Z^_`a-z{|}~^?]+\\.)+[a-zA-Z]{2,6}\$";

    $strMail = trim($strMail);
    return ( mb_eregi($strRegexp, $strMail) != 0 ? true : false );
  }
}

?>