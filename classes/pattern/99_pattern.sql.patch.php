<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

// version

/**
 * la version 3.3.0_RELEASE correspond à la marquée 3.2.31
 */
$strNum = "3.3.0";
$tabIns[$strNum][] = "";

/** 
 * PCT : Remplacement de ob_clean() par ob_end_clean()
 *       alkintabonne.class.php : modif proto de delAbonnement()
 *       alkmail :ajout des méthodes setSendMailArgs(), sendMailToQueue(), setContentType() et getMailSourceCode()
 *                la méthode sendMailToQueue() permet d'envoyer un mail en différé. 
 *                il faut l'utiliser dans le cas avec beaucoup de destinataires. 
 *                la méthode send() appelle sendMailToQueue() lorsque le nbDest > ALK_MAIL_MAX_SEND > 1
 *       alkfactory.class.php : ajout méthode getNavigator() pour gérer les familles de navigateur par marque et par regroupement css
 * MTO : alkiptc.class.php : création de la classe AlkIptc permettant de manipuler les métadonnées IPTC de fichiers images
 *       alkimport.class.php : modifications permettant de prendre en compte les fichiers XLS
 *       alkquery.class.php : modification des requêtes liées aux pièces jointes, ajout fonction getSitDataTypeId()
 * EMA : alquery.class.php : modif DelPj
 *     : alkfactory.class.php : modif function getMasterATypeId pour ajouter le nouveau type ALK_ATYPE_ID_TACHE
 * MTO : alkquery.class.php : modif AddPj() et getDsListePJ(), lecture et enregistrement des métadonnées MPEG
 *       alkappli.class.php : modif delDataAssoc(), appel delDataAssoc() sur l'espace
 */
$strNum = "3.3.1";
$tabIns[$strNum][] = "";

/** 
 * DMI : AlkMail : correction constructeur (répercussion correction de la branche 3.2.0)
 * MTO : alkfactory.class.php : ajout méthodes statics getIptc() et getIcs()
 *       alkquery.class.php : utilisation getIptc()
 *       ajout classes alkics.class.php, iCalCreator.class.php et iCalUtilityFunctions.class.php
 *       alkMail.class.php : gestion fichier ICS, ajout méthode addEventToIcs()
 * EMA : alkobject.class.php : modif méthode construct()
 *     : alkfactory.class.php : modif getDBCurrentLanguageField() + ajout initLangue()
 *     : alkimport.class.php   : modif parseLines() : remplacement du array_push par $tabTrace[$j]= $this->tabAlkImportCol[$k]; 
            
 */
$strNum = "3.3.2";
$tabIns[$strNum][] = "";

/**
 * EMA : alkfactory : modif initLangue()
 */
 
$strNum = "3.3.3";
$tabIns[$strNum][] = "";

/**
 * EMA : alkintrights.int.php => ajout function setProfilRightToUser($agent_id);
 * MTO : alkics.class.php : modif fonction getContentHtmlBody()
 *     : alkintabonne.class.php : définition des méthodes enableAbonnement(), disableAbonnement() et createListeAbonnes()
 *     : alkquery.class.php : modification getDsListePJ() ajout champ DATA_ID
 */
 
$strNum = "3.3.4";
$tabIns[$strNum][] = "";

/**
 * MTO : alkquery.class.php : division DelPj() => ajout doDelPj() + suppression des images retaillées
 *     : alkquery.class.php : ajout test isFileMultimedia() pour extraire les métadonnées Mpeg uniquement sur les fichiers multimédia
 *     : alkquery.class.php : correction updatePJDetails()
 *     : alkquery.class.php : suppression fonction DelPj() et renommage doDelPj() en DelPj() + modification fonction DelPjByDataId() pour utiliser la fonction DelPj()
 *     : alkquery.class.php : modif getDsListePJ() => ordonne les pièces jointes de la plus ancienne à la plus récente par défaut
 * MTO : alkquery.class.php : modif AddPj() => gestion valeur par défaut pour métadonnées IPTC
 *     : alkquery.class.php : modif getSqlWhereByGEditBlocTypeAssoc() => ajout cas TASSOC_TOUTETATPUB récupère toutes les informations
 * PCT : AlkFactory : évolution de initLangue()
 *       AlkQuery   : correction getSqlWhereByGEditBlocTypeAssoc() avec les constantes TASSOC_ENVALIDATION et TASSOC_VALIDATION
 * ASN : AlkFactory : Correction getNavigator()
 */
 
$strNum = "3.3.5";
$tabIns[$strNum][] = "";

/**
 * PCT : Evolution de AlkFactory::memCacheGetKey() pour prendre en compte la constante ALK_ROOT_URL_MEMCACHE si elle existe
 *       Sans cette évolution, problème de mise à jour de site web lorsque le site admin est sur un autre domaine
 *       Correction sur l'indexation et l'appel à solr
 */
$strNum = "3.3.6";
$tabIns[$strNum][] = "";

/**
 * DMI : alkfactory.class.php : getNavigator correction strNavCss par defaut = ALK_NAVCSS_FF et non ALK_NAVCSS_ALK inexistant
 * PCT : Ajout encodage des noms de l'expéditeur et des destinataires
 */
$strNum = "3.3.7";
$tabIns[$strNum][] = "";

/**
 * DMI : SendMailType maj url_site si ALK_ROOT_URL_FRONTOFFICE est définie
 * PCT : alkMail : Correction pour supprimer tout caractère invisible dans une adresse mail (trim)
 */
$strNum = "3.3.8";
$tabIns[$strNum][] = "";

/**
 * BFE : mise à jour des fichiers de langue suite aux corrections syntaxiques
 * PCT : modif alkFactory pour prendre en compte AGENT_LG
 *       Modif de initLangue() pour charger en premier, si il existe, le fichier locale_js.json se trouvant dans le répertoire locales
 *       pour charger les traductions de toutes les langues sur la page. Nécessaire si l'on souhaite afficher une langue par défaut 
 *       différente de fr_FR si la traduction n'existe pas.
 */
$strNum = "3.3.9";
$tabIns[$strNum][] = "";

/**
 * BFE : traductions
 */
$strNum = "3.3.10";
$tabIns[$strNum][] = "";

/**
 * PCT : AlkDbXXX : ajout de getScalarSql()
 *      AlkQuery : Modif de getSqlWhereByGEditBlocTypeAssoc() pour que le filtre calendaire prenne la date du jour pour lorsqu'aucune date n'est proposée
 *      Création de la branche 3.4.0
 */
$strNum = "3.3.11";
$tabIns[$strNum][] = "";

/**
 * MTO : alkmailing.class.php : correction utilisation ALK_ROOT_DIR à la place de ALK_VIRTUAL_DIR non définit
 *     : alkquery.class.php : modif initSearcherSolr() => prise en compte la classe générique AlkSearcherSolr + modif UpdatePjDetails() => réindexe les pièces-jointes
 *                            fonction initSearcherSolr() passé en public
 *     : ajout classe générique alksearcher_solr.class.php
 */
$strNum = "3.3.12";
$tabIns[$strNum][] = "";

/**
 * MTO : alksearcher_solr.class.php : modification mergeDataToPjs() pour vérifier les extensions avant d'effectuer la recopie des informations
 */
$strNum = "3.3.13";
$tabIns[$strNum][] = "";

/**
 * MTO : alksearcher_solr.class.php : modif fonction mergeDataToPj() => suppression doublon
 *     : alkquery.class.php : correction AddPj() et UpdatePjDetails() => ajout test sur existance $tabFieldInfo["value"]  
 */
$strNum = "3.3.14";
$tabIns[$strNum][] = "";
?>