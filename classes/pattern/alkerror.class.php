<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

if( ALK_ERROR_DEBUG == 1 ) {
	include_once(ALK_ALKANET_ROOT_PATH."classes/pattern/debuglib.inc.php");
}

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkError
 * @brief Classe de base permettant la gestion des erreurs des scripts
 */
class AlkError extends AlkObject
{
	/** Mode de gestion des cas d'erreur 1 : msg ecran, 2 : msg fichier, 4 : msg mail, 3 : 1+2 , 5 : 1+4 , 6 : 2+4 */
	var $iMod;
    
	/** chaine des adresses mail des destinataires des msg d'erreur (separateur : point-virgule) */
	var $strDest;
    
	/** Chemin physique du répertoire contenant le fichier de msg d'erreur */
	var $strPath;
	
	/** Nom du fichier contenant les msg d'erreur */
	var $strFileName;

  /**
   *  constructeur de la classe err : initialisation des attributs de l'objet err
   *
   * @param iMod
   * @param strDest
   * @param strPath
   * @param strFileName
   */
  public function __construct($iMod, $strDest="", $strPath="", $strFileName="")
  {		
		$this->iMod = $iMod;
		$this->strDest = $strDest;
		$this->strPath = $strPath;
		$this->strFileName = $strFileName;
	}

  /**
   *  gère le traitement du cas d'erreur
   *
   * @param msg
   * @param ctx
   */
	public function _traiteErreur($msg, $ctx)
  {
    $msgtmp = $msg;
			
    $msgEcran = str_replace("\t","<br>", $msgtmp);
    $msgEcran = str_replace("\n","<p>", $msgtmp);

    if( ALK_ERROR_DEBUG == 1 ) {
      ob_start(); // debut de la bufferisation de sortie
      print_r($ctx);
      $msgtmp .= ob_get_contents();
      ob_end_clean();
      $msgtmp .= show_vars(TRUE, TRUE);
    }

    $msgLog = $msgtmp;
			
    $msgMail = $msgtmp;
    $headers = "Subject: Alkanet - Erreur\r\nContent-type: text/html; charset=iso-8859-1\r\n";
    switch($this->iMod) {
    case 1 :
      echo $msgEcran."<br>";
      break;
    case 2 :
      error_log($msgLog, 3, $this->strPath.$this->strFileName);
      break;
    case 3 :
      error_log($msgLog, 3, $this->strPath.$this->strFileName);
      echo $msgEcran;
      break;
    case 4 :
      error_log($msgMail, 1, $this->strDest, $headers);
      break;
    case 5 :
      echo $msgEcran;
      error_log($msgMail, 1, $this->strDest, $headers);
      break;
    case 6 :
      error_log($msgLog,  3, $this->strPath. $this->strFileName);
      error_log($msgMail, 1, $this->strDest, $headers);
      break;
    }
    return true;
  }
	
	public function _errError($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [Error]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }

	public function _errWarning($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [Warning]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }

	public function _errParse($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [Parse]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }

	public function _errNotice($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [Notice]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
  }

	public function _errCoreError($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [CoreError]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }

	public function _errCoreWarning($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [CoreWarning]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg,$ctx);
  }

	public function _errCompileError($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [CompileError]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }
	public function _errCompileWarning($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [CompileWarning]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
  }

	public function _errUserError($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [UserError]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
    die();
  }

	public function _errUserWarning($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [UserWarning]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
  }

	public function _errUserNotice($no, $str, $file, $line, $ctx)
  {
    $msg = "Erreur : [UserNotice]"."\t";
    $msg .= "Message : ".$str."\t";
    $msg .= "Ligne : ".$line."\t";
    $msg .= "Fichier : ".$file."\n";
    $this->_traiteErreur($msg, $ctx);
  }
}
?>