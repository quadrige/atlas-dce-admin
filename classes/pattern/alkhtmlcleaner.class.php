<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkHtmlCleaner
 * Classe permettant de nettoyer le code html générer par l'éditeur
 */ 
class AlkHtmlCleaner extends AlkObject
{
  /** message contenant la dernière erreur rencontrée */
  protected $lastError;

  /**
   * Constructeur par défaut 
   */
  function __construct()
  {
    parent::__construct();
    $this->lastError = "";
  }
    
  /**
   * Retourne la dernière erreur rencontrée
   * @return string
   */
  public function GetLastError() 
  {
    return $this->lastError;
  }
    
  /**
   * Supprime un certain nombre de tag html
   * @param htmlCnt  contenu html à nettoyer passé par référence
   * @param bDiv     false par défaut, true pour supprimer les tag <div> et </div>
   */
  public function getCleanHtml(&$htmlCnt, $bDiv = false)
  {
    $htmlCnt = mb_ereg_replace("<text>","", $htmlCnt);
    $htmlCnt = mb_ereg_replace("</text>", "", $htmlCnt);
    $htmlCnt = mb_ereg_replace("</img>", "", $htmlCnt);
    $htmlCnt = mb_ereg_replace("</param>", "", $htmlCnt);
    $htmlCnt = mb_ereg_replace("</area>", "", $htmlCnt);
    $htmlCnt = mb_ereg_replace("</input>", "", $htmlCnt);
    $htmlCnt = mb_ereg_replace("<div></div>","", $htmlCnt);
    
    if ( $bDiv==true ) {
      $htmlCnt = mb_ereg_replace("<div>","", $htmlCnt);
      $htmlCnt = mb_ereg_replace("</div>","", $htmlCnt);
    }
  }
  
  /**
  * Corrige le code HTML à l'aide de TIDY et retourne de l'html ou du xml
  * @param htmlCnt      code html à corriger passé en référence
  * @param bXmlOutput   détermine le format de sortie (html ou xml) du code html corrigé (html par défaut)
  * @param bAddTagText  false par défaut, true pour ajouter le tag spécifique <text>...</text>
  * @param bRepair      true par défaut, false pour ne pas réparer le code entrant
  * @return boolean False if problem, true otherwise
  */
  function cleanWithTidy(&$htmlCnt, $bXmlOutput=false, $bAddTagText=false, $bRepair=true)
  {
    if( $bRepair ) {
      try {          
        $oTidy = new Tidy();
      } catch( Exception $e ) {
        $this->lastError = "Impossible d'instancier l'objet Tidy.";
        return false; 
      }
      
      $config = array('indent'                      => FALSE,
                      'bare'                        => FALSE, //si TRUE alors on ne voit plus les cellules vides d'un tableau
                      'word-2000'                   => TRUE,
                      'input-xml'                   => TRUE,
                      'output-xhtml'                => !$bXmlOutput,
                      'output-xml'                  => $bXmlOutput,
                      'drop-proprietary-attributes' => TRUE,
                      'wrap'                        => 12000,
                      'uppercase-tags'              => FALSE,
                      'drop-empty-paras'            => FALSE,
                      'newline'                     => 1);
      
      $oTidy->parseString($htmlCnt, $config, ALK_TIDY_ENCODING);
      $oTidy->cleanRepair();
      $htmlCnt = $oTidy;
    }
    
    $htmlCnt = mb_ereg_replace(".*<body>", "", $htmlCnt);
    $htmlCnt = mb_ereg_replace("</body>.*", "", $htmlCnt);
    
    $htmlCnt = mb_ereg_replace("<\?xml[^>]*\?>", "", $htmlCnt );
    
    if ( $bAddTagText ) {
      $this->addTagText($htmlCnt);
    }
    return true;
  }
  
  /**
   * Ajoute le tag text spécifique au contenu éditorial
   * @param htmlCnt  Contenu Html passé en référence
   */
  private function addTagText(&$htmlCnt)
  {
    $strTextHtml = "";

    $strTextHtml = $htmlCnt;
    
    if ( substr($strTextHtml, 0, 4)!="<div" ) {
      $strTextHtml = "<div>".$strTextHtml."</div>";
    }
  
    $strTextHtml = preg_replace("!<p>!", "<p align=\"justify\">", $strTextHtml); 
    $strTextHtml = preg_replace("!<text>!", "", $strTextHtml);
    $strTextHtml = preg_replace("!</text>!", "", $strTextHtml);
   
    $htmlCnt = preg_replace("!<([^>]+)>([^<\n\r]+)!", "<$1><text>$2</text>", $strTextHtml);
  } 
}  
?>