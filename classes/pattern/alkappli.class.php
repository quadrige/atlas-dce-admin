<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkfactory.class.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkintappli.int.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkintrights.int.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkintabonne.int.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkintcrv.int.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkintgedit.int.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkintfdoc.int.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkintcallservice.int.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkinthtdig.int.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkintworkflow.int.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkAppli
 * @brief Classe abstraite racine des applications Alkanet
 *        Les classes qui héritent de AlkAppli peuvent implémenter 
 *        les interfaces : (AlkIntAppli ou AlkIntSpace) et AlkIntRights
 */
abstract class AlkAppli extends AlkObject
{
  /** référence sur l'objet oSpace */
  protected $oSpace;
  
  /** abreviation de l'application */
  protected $appli_abrev;

  /** type de l'application */
  public $atype_id;

  /** Identifiant de l'application */
  public $appli_id;

  /** Identifiant de l'espace de travail */
  public $cont_id;

  /** tableau contenant les propriétés de l'application */
  protected $tabProperty;

  /** tableau contenant les paramétrage de l'application */
  protected $tabParameter;

  /** Chemin physique de base pour les données de l'appli en upload */
  protected $strPathUpload;

  /** Url de base pour les données de l'appli en download */
  protected $strUrlUpload;

  /** référence vers l'objet de type query lié à l'application */
  public $oQuery;

  /** référence vers l'objet de type queryAction lié à l'application */
  public $oQueryAction;

  /** tableau des onglets d'application */
  protected $tabSheets;
  
  /** Type d'onglet initial */
  protected $iTypeSheet;
  
  /** Indice d'onglet initial */
  protected $iSheet;
  
  /** Indice de sous-onglet initial */
  protected $iSSheet;

  /**
   *  Constructeur par défaut
   *        Nécessite d'instancier this->atype_id avant d'appeler parent::__constructor()
   *
   * @param atype_id  Identifiant du type d'application
   * @param appli_id  Identifiant de l'application, -1 par défaut.
   */
  public function __construct($atype_id, $appli_id=-1)
  {
    parent::__construct();
  
    if( get_class($this) != "AlkAppliEspace" ) {
      $this->oSpace = AlkFactory::getSpace(AlkRequest::getToken("cont_id", -1));
      $this->cont_id = ( !is_null($this->oSpace) ? $this->oSpace->cont_id : -1 );
    } else {
      $this->oSpace = $this;
    }

    $this->atype_id      = $atype_id;
    $this->appli_id      = $appli_id;

    $this->appli_abrev   = ( defined("ALK_ATYPE_ABREV_".$atype_id) ? constant("ALK_ATYPE_ABREV_".$atype_id) : "" );
    if( $this->appli_abrev == "" ) {
      $this->triggerError("Constructeur AlkAppli : la constante ALK_ATYPE_ABREV_".$atype_id." n'existe pas", E_USER_ERROR);
    }
    $strDirUpload = strtolower($this->appli_abrev)."/";

    $this->strPathUpload = ALK_ALKANET_ROOT_PATH.ALK_ROOT_UPLOAD.$strDirUpload;
    $this->strUrlUpload  = ALK_ALKANET_ROOT_URL.ALK_ROOT_UPLOAD.$strDirUpload;
    $this->tabProperty   = array();
    $this->tabParameter  = null;
    $this->oQuery        =& AlkFactory::getQuery($this->atype_id);
    $this->oQueryAction  =& AlkFactory::getQueryAction($this->atype_id);
    
    $this->iTypeSheet = -1;
    $this->iSheet     = -1;
    $this->iSSheet    = -1;
    
    $this->tabSheets     = array("_".ALK_TYPESHEET_CONSULT   => array(), 
                                 "_".ALK_TYPESHEET_ADMIN     => array(), 
                                 "_".ALK_TYPESHEET_PROPRIETE => array(),
                                 "_".ALK_TYPESHEET_POPUP     => array());
  }

  /**
   *  Destructeur par défaut
   */
  public function __destruct()
  {
  }

  /**
   * Affecte un espace lié à l'application
   */
  public function setSpace($cont_id)
  {
    $this->oSpace = AlkFactory::getSpace($cont_id);
    $this->cont_id = ( !is_null($this->oSpace) ? $cont_id : -1 );
  }

  /**
   *  Ajoute un onglet ou un sous onglet
   * @param iTypeSheet     identifiant du type onglet
   * @param iSheet         identifiant de l'onglet
   * @param iSSheet        identifiant de l'onglet
   * @param strText        intitulé de l'onglet
   * @param strUrl         url fichier uniquement ou appel javascript
   * @param strParam       param complémentaire encodé dans le token
   * @param strTitle       infobulle sur l'onglet
   * @param iRightAppli    droit nécessaire pour voir l'onglet, -1 pour ne pas vérifier
   * @param iPrivSpace     privilège nécessaire pour voir l'onglet, -1 pour ne pas vérifier
   * @param bVisible       vrai si visible, faux sinon
   * @param strTarget      possibilité d'ouvrir un lien vers une autre fenêtre en cliquant l'onglet
   * @param bDefaultSheet  =false par défaut, =true si l'onglet donné est sélectionné par défaut sur le typeSheet donné
   * @param strDecodeParam param complémentaire non encodé dans le token
   * @param bCheckUrl      =false par défaut, =true si on vérifie que l'url générée est déjà présente dans le tableau d'onglets
   * @return boolean : onglet ajouté
   */
  protected function addSheet($iTypeSheet, $iSheet, $iSSheet, $strText, $strUrl, $strParam="", $strTitle="", 
                              $iRightAppli=-1, $iPrivSpace=-1, $bVisible=true, $strTarget="", $bDefaultSheet=false, 
                              $strDecodeParam="", $bCheckUrl=false, $guid="", $bSelected=false)
  {
    $user_right = AlkFactory::getSProperty("user_right", ALK_APPLI_RIGHT_NONE);
    $user_priv  = AlkFactory::getSProperty("user_priv", ALK_PRIV_SPACE_NONE);

    if ( mb_strpos($strUrl, "javascript:")===false ){
      $strToken = $this->getToken($iTypeSheet, $iSheet, $iSSheet, $strParam);
      $strUrl .= "?token=".$strToken.$strDecodeParam;
    }
    if ( $strTitle=="" )
      $strTitle = $strText;
    
    $tabTypeTexte = array("_".ALK_TYPESHEET_CONSULT   => "Consultation", 
                          "_".ALK_TYPESHEET_ADMIN     => "Administration", 
                          "_".ALK_TYPESHEET_PROPRIETE => "Propriétés",
                          "_".ALK_TYPESHEET_POPUP     => "Popup");
    
    if( !array_key_exists("_".$iTypeSheet, $this->tabSheets) ) {
      $this->triggerError("Type d'onglet inconnu (addSheet).", E_USER_ERROR);
    }

    $iUserRightAppli = ( $iRightAppli != -1 ? $user_right : ALK_APPLI_RIGHT_PUBLI );
    $iUserPrivSpace  = ( $iPrivSpace  != -1 ? $user_priv  : ALK_PRIV_SPACE_USER  );
    
    if( !array_key_exists("text", $this->tabSheets["_".$iTypeSheet]) ) {
      if( $iTypeSheet==ALK_TYPESHEET_CONSULT   && $iUserRightAppli>=ALK_APPLI_RIGHT_READ ||
          $iTypeSheet==ALK_TYPESHEET_ADMIN     && $iUserRightAppli>=ALK_APPLI_RIGHT_ADMIN ||
          $iTypeSheet==ALK_TYPESHEET_PROPRIETE && $iUserRightAppli>=ALK_APPLI_RIGHT_PUBLI ||
          $iTypeSheet==ALK_TYPESHEET_POPUP ) {
        $this->tabSheets["_".$iTypeSheet] = array("text" => $tabTypeTexte["_".$iTypeSheet], "url" => "");
      } else {
        // aucun droit, on n'ajoute pas l'onglet
        return false;
      }
    }
    
    $iIndex = -1;
    $bAddSheet = false;
    // recherche si l'onglet existe
    foreach($this->tabSheets["_".$iTypeSheet] as $strKey=>$tabData) {
      if( is_numeric($strKey) && is_array($tabData) && $tabData["idSheet"] == $iSheet && $tabData["idSSheet"] == $iSSheet && (!$bCheckUrl || $tabData["url"] == $strUrl) ) {
        $iIndex = $strKey;
        break;
      }
    }
    if( $iIndex == -1 ) {
      $bAddSheet = true;
      $iIndex = count($this->tabSheets["_".$iTypeSheet])-1;
    }

    $bVisible = $bVisible && 
      ($iRightAppli==-1 || $iRightAppli!=-1 && $iUserRightAppli>=$iRightAppli) && 
      ($iPrivSpace==-1  || $iPrivSpace!=-1  && ($iUserPrivSpace & $iPrivSpace)>0 );

    if( $bAddSheet ) {
      if( $bVisible && ($this->tabSheets["_".$iTypeSheet]["url"] == "" || $bDefaultSheet) ) {
        // l'onglet de type prend l'url du premier onglet de sa classe
        $this->tabSheets["_".$iTypeSheet]["url"] = $strUrl;
      }
      $this->tabSheets["_".$iTypeSheet][$iIndex] = array("idSheet"         => $iSheet,
                                                     "idSSheet"        => ($iSSheet!="-1" ? $iSSheet : ""),
                                                     "text"            => $strText, 
                                                     "title"           => $strTitle,
                                                     "url"             => $strUrl,
                                                     "target"          => $strTarget,
                                                     "visible"         => $bVisible,
                                                     "_userRightAppli" => $iUserRightAppli,
                                                     "_rightAppli"     => $iRightAppli,
                                                     "_userPrivSpace"  => $iUserPrivSpace,
                                                     "_privSpace"      => $iPrivSpace,
                                                     "subSheet"        => array(),
                                                     "selected"        => $bSelected);
      if ( $guid!="" ){
        $this->tabSheets["_".$iTypeSheet][$iIndex]["guid"] = $guid; 
      }                                               
    }
    return true;
  }
  
  /**
   *  Détermine l'ordre de priorité des types d'onglets pour l'initialisation des coordonnées
   * @return array : une permutation de ALK_TYPESHEET_CONSULT, ALK_TYPESHEET_ADMIN, ALK_TYPESHEET_PROPRIETE
   */
  protected function getTypeSheetOrder()
  {
    return array(ALK_TYPESHEET_CONSULT, ALK_TYPESHEET_ADMIN, ALK_TYPESHEET_PROPRIETE, ALK_TYPESHEET_POPUP);
  }
  /**
   *  Détermine les valeurs initiales des attributs iTypeSheet, iSheet, iSSheet 
   *        en fonction du tabSheets avec priorité de types : CONSULT > ADMIN > PROPRIETE 
   */
  protected function setInitialCoordinates()
  {
    if ( $this->iTypeSheet==-1 ){
      $tabTypeSheet = $this->getTypeSheetOrder();
      $bFound = false;
      foreach ( $tabTypeSheet as $iTypeSheet ){
        if ( $bFound ) break;

        if ( !$bFound && array_key_exists("_".$iTypeSheet, $this->tabSheets) ){           
          $tabSheets = $this->tabSheets["_".$iTypeSheet];
          if ( empty ($tabSheets) ) continue;
          
          $iIndexSheet = $this->getFirstIndexVisible($tabSheets);
          if ( $iIndexSheet!=-1) {
            $this->iTypeSheet = $iTypeSheet;
            $bFound = true;
            $this->iSheet = $tabSheets[$iIndexSheet]["idSheet"];
            if ( $tabSheets[$iIndexSheet]["idSSheet"]!="" )
              $this->iSSheet = $tabSheets[$iIndexSheet]["idSSheet"];
            else
              $this->iSSheet = ALK_SHEET_NONE;
          }
        }
      }
      if ( !$bFound ){
        $this->iTypeSheet = -1;
        $this->iSheet     = -1;
        $this->iSSheet    = -1;
      }
    }
  }
  
  /**
   *  recherche de l'indice du premier d'onglet visible dans le tableau d'onglets donné
   * @param tabSheets   Tableau de description d'onglets
   * @return int : indice du premier d'onglet visible ou -1 si n'existe pas
   */
  private function getFirstIndexVisible($tabSheets)
  {
    foreach ( $tabSheets as $iIndex=>$tabSheet ){
      if ( is_array($tabSheet) && array_key_exists("visible", $tabSheet) && $tabSheet["visible"]==true ) 
        return $iIndex;
    }
    return -1;
  }
  
  /**
   *  Retourne les valeurs courantes de iTypeSheet, iSheet et iSSheet
   * @param iTypeSheet  type d'onglet
   * @param iSheet      identifiant de l'onglet
   * @param iSSheet     identifiant du sous onglet
   */
  public function getCurrentSheet(&$iTypeSheet, &$iSheet, &$iSSheet)
  {
    $this->setInitialCoordinates();
    $this->iTypeSheet = $iTypeSheet = AlkRequest::getToken("iTypeSheet", $this->iTypeSheet);
    $this->iSheet     = $iSheet     = AlkRequest::getToken("iSheet", $this->iSheet);
    $this->iSSheet    = $iSSheet    = AlkRequest::getToken("iSSheet", $this->iSSheet);
  }
  
  /**
   *  Retourne le parcours dans l'application en fonction des coordonnées données
   * @param iTypeSheet    Type d'onglet
   * @param iSheet        Indice d'onglet
   * @param iSSheet       Indice de sous-onglet
   * @return string : le parcours dans l'application
   */
  public function getHtmlRoute()
  {    
    $strParcours = ""; 
    if ( array_key_exists("_".$this->iTypeSheet, $this->tabSheets) ){
      foreach ( $this->tabSheets["_".$this->iTypeSheet] as $iIndex=>$tabSheets ){        
        if (!is_array($tabSheets))
          continue;          
        if ( $this->iSheet == $tabSheets["idSheet"] && ($this->iSSheet == $tabSheets["idSSheet"] || $tabSheets["idSSheet"]=="")) { 
          $strParcours .= $tabSheets["text"];
        } 
      }
    }
    return $strParcours;
  }

  /**
   *  modifie une propriété d'un onglet
   * @param iTypeSheet     identifiant du type onglet
   * @param iSheet         identifiant de l'onglet
   * @param iSSheet        identifiant de l'onglet
   * @param strProperty    nom de la propriété
   * @param strValue       valeur de la propriété
   *
  public function setPropertySheet($iTypeSheet, $iSheet, $iSSheet, $strProperty, $strValue)
  {
    if( substr($strProperty, 0, 1) == "_" ) {
      $this->triggerError("Interdiction de modifier le paramètre onglet (setPropertySheet) : ".$strProperty.".", E_USER_ERROR);
    }
    if( !array_key_exists($iTypeSheet, $this->tabSheets) ) {
      $this->triggerError("Type d'onglet inconnu (setPropertySheet).", E_USER_ERROR);
    }

    $iIndex = -1;
    $bAddSheet = false;
    // recherche l'onglet de premier niveau
    foreach($this->tabSheets[$iTypeSheet] as $strKey=>$tabData) {
      if( is_numeric($strKey) && is_array($tabData) && $tabData["idSheet"] == $iSheet ) {
        $iIndex = $strKey;
        break;
      }
    }

    if( $iIndex!= -1 && $iSSheet==-1 ) {
      $tabSheet = $this->tabSheets[$iTypeSheet][$iIndex];
      if( array_key_exists($strProperty, $tabSheet) ) {
        if( $strProperty == "visible" ) {
          if( is_bool($strValue) ) {
            $strValue = $strValue && 
              ( $tabSheet["_rightAppli"]==-1 ||
                $tabSheet["_rightAppli"]!=-1 && $tabSheet["_userRightAppli"]>=$tabSheet["_rightAppli"]) && 
              ( $tabSheet["_privSpace"]==-1 ||
                $tabSheet["_privSpace"]!=-1 && ($tabSheet["_userPrivSpace"] & $tabSheet["_privSpace"])==$tabSheet["_privSpace"] );
          } else {
            $this->triggerError("La propriété visible doit être de type booléen (setPropertySheet).", E_USER_ERROR);
          }
        }

        $this->tabSheets[$iTypeSheet][$iIndex][$strProperty] = $strValue;
        return;
      } else {
        $this->triggerError("Propriété d'onglet inconnu (setPropertySheet) : ".$strProperty.".", E_USER_ERROR);
      }
    }
    
    if( $iIndex!= -1 && $iSSheet!=-1 ) {
      // recherche l'onglet de second niveau
      $iIndex2 = -1;
      foreach($this->tabSheets[$iTypeSheet][$iIndex]["subSheet"] as $strKey=>$tabData) {
        if( is_numeric($strKey) && is_array($tabData) && $tabData["idSheet"] == $iSSheet ) {
          $iIndex2 = $strKey;
          break;
        }
      }
      if( $iIndex2 != -1 ) {
        $tabSSheet = $this->tabSheets[$iTypeSheet][$iIndex]["subSheet"][$iIndex2];
        if( array_key_exists($strProperty, $tabSSheet) ) {
          if( $strProperty == "visible" ) {
            if( is_bool($strValue) ) {
              $strValue = $strValue && 
                ( $tabSSheet["_rightAppli"]==-1 ||
                  $tabSSheet["_rightAppli"]!=-1 && $tabSSheet["_userRightAppli"]>=$tabSSheet["_rightAppli"]) && 
                ( $tabSSheet["_privSpace"]==-1 ||
                  $tabSSheet["_privSpace"]!=-1 && ($tabSSheet["_userPrivSpace"] & $tabSSheet["_privSpace"])==$tabSSheet["_privSpace"] );

            } else {
              $this->triggerError("La propriété visible doit être de type booléen (setPropertySheet).", E_USER_ERROR);
            }
          }
          $this->tabSheets[$iTypeSheet][$iIndex]["subSheet"][$iIndex2][$strProperty] = $strValue;
          return;
        } else {
          $this->triggerError("Propriété de sous onglet inconnu (setPropertySheet) : ".$strProperty.".", E_USER_ERROR);
        }
      }
    }
  }*/

  /**
   *  Retourne le chemin de base vers le répertoire upload
   * @return string
   */
  public function getPathUpload() 
  { 
    return $this->strPathUpload;
  }

  /**
   *  Retourne l'url de base vers le répertoire upload
   * @return string
   */
  public function getUrlUpload()  
  { 
    return $this->strUrlUpload;  
  }

  /**
   * Supprime les informations annexes associées à cette application
   * Si cat_id =-1 et data_id=-1, supprime toutes les informations annexes associées à cette application
   * Si cat_id!=-1 et data_id=-1, supprime toutes les informations annexes associées à cette classification cat_id
   * Si data_id!=-1, supprime toutes les informations annexes associées à cet donnée data_id 
   * 
   * @param cont_id  identifiant de l'espace contenant cette application
   * @param cat_id   identifiant de la classification, =-1 par défaut
   * @param data_id  identifiant de l'information, =-1 par défaut
   */
  public function delDataAssoc($cont_id, $cat_id=-1, $data_id=-1)
  {
    $oSpace = AlkFactory::getSpace($cont_id);
    $oSpace->delDataAssoc($this->appli_id, $cat_id, $data_id);
  }

  /**
   * Méthode virtuelle à implémenter si l'application est de type instanciée par la méthode delDataAssoc()
   * Les paramètres correspondent aux identifiants de l'application appelante
   * @param appli_id  identifiant de l'application contenant l'information principale
   * @param cat_id    identifiant de la classification, =-1 par défaut
   * @param data_id   identifiant de l'information, =-1 par défaut
   */
  protected function delAppliCatData($appli_id, $cat_id=-1, $data_id=-1)
  {
    if( !is_null($this->oQueryAction) && 
        is_object($this->oQueryAction) && method_exists($this->oQueryAction, "delAppliCatData") ) {
      $this->oQueryAction->delAppliCatData($appli_id, $cat_id, $data_id);
    }
  }

  /**
   *  fonction abstraite qui instancie la bonne interface en fonction de iSheet et iSSheet
   *        puis retourne le code html de l'objet panel généré par celle-ci
   * 
   * @param oForm         AlkHtmlForm   Objet formulaire dans lequel le contenu généré va s'intégrer (définition des propriétés)
   * @param oContainer    AlkHtmlPanel  Panel qui contiendra les blocks et controles à ajouter au formulaire
   * 
   * @return AlkHtmlPanel
   */
  abstract public function getPanel(AlkAppli $oAppliFrom=null);

  /**
   *  Fonction qui appelle la fonction de traitement en fonction de iSheet et iSSheet
   *        Retourne l'url de redirection pour une redirection par header
   *        Retourne vide en cas de redirection par javascript (effectue par le code html produit après traitement)
   * @return string
   */
  public function doSql(AlkHtmlForm $oForm=null)
  {
    $iTmp = -1;
    $this->getCurrentSheet($iTmp, $iTmp, $iTmp);
    $oPanel =& $this->GetPanel($oForm);
    if( is_object($oPanel) && !is_null($oPanel) && method_exists($oPanel, "doSql") ) {
      return $oPanel->doSql(); 
    }
    return "";
  }

  /**
   *  Fonction virtuelle appelée par le constructeur permettant d'initialiser le tableau this->tabSheets
   */
  abstract public function setTabSheets();
  
  /**
   *  Retourne la liste des paramétrage de l'application
   * @param bDsElseArray     Retourne un dataset ou sinon un tableau des paramètres de l'application
   * @param param_field      Valeur totale ou partielle du paramètre recherché
   * @param bExactField      Indique s'il s'agit de la valeur totale (true) ou partielle (false)
   * @param bReturnNull      Permet l'initialisation du tableau général de paramètres de l'appli sans évaluation des autres critères => return null
   * @return mixed {dataset ; array} Liste des paramétrages de l'application
   */
  public function getAppliParam($bDsElseArray=true, $param_field="", $bExactField=true, $bReturnNull=false)
  {
    if ( is_null($this->tabParameter) )
      $this->tabParameter = $this->oQuery->getTabAppliParam($this->appli_id);
    if ( $bReturnNull )
      return AlkObject::$oNull;
    if ( $bDsElseArray ){
      return $this->oQuery->getDsAppliParam($this->appli_id, $param_field, $bExactField);
    }
    else {
      return $this->oQuery->getTabAppliParam($this->appli_id, $param_field, $bExactField);
    }
  }
  /**
   *  Retourne la valeur d'une propriété SQL d'un paramètre de l'application
   * @param param_field      Nom du paramètre recherché
   * @param dsField          Champ demandé parmi ("PARAM_ID", "PARAM_FIELD", "PARAM_TYPE", "PARAM_VALUE")
   * @param defaultValue     Valeur par défaut retourné si pas de correspondance
   * @return mixed
   */
  public function readAppliParam($param_field, $dsField, $defaultValue)
  {
    if ( is_null($this->tabParameter) )
      $this->getAppliParam(true, "", true, true);
    
    if ( array_key_exists($param_field, $this->tabParameter) ){
      if ( array_key_exists($dsField, $this->tabParameter[$param_field]) ){
        return $this->tabParameter[$param_field][$dsField];
      }
    }
    return $defaultValue;
  }
  
  /**
   *  Retourne une propriété de l'application
   * @param strParam         nom de la propriété
   * @param strDefaultValue  valeur par défaut si non trouvée
   * @return string : valeur de la propriété
   */
  public function getAppliProperty($strParam, $strDefaultValue="")
  {
    if( empty($this->tabProperty) || 
        array_key_exists("appli_id", $this->tabProperty) && $this->tabProperty["appli_id"]!=$this->appli_id ) {
      $this->setAppliProperty();
    }
    $oRes = ( array_key_exists($strParam, $this->tabProperty)
             ? $this->tabProperty[$strParam]
             : $strDefaultValue );
    //echo $strParam."=".$oRes."<br>";
    return $oRes;
  }

  /**
   *  Méthode qui lit les propriétés de l'application dans la base 
   *        et les collecte dans le tableau tabProperty
   */
  protected function setAppliProperty()
  {
    $user_id = AlkFactory::getSProperty("user_id", "-1");
    $oQuery = AlkFactory::getQuery(ALK_ATYPE_ID_ESPACE);
    $dsAppli = $oQuery->getDsFicheProperty($this->appli_id, $user_id);
    if( $drAppli = $dsAppli->GetRowIter() ) {
      $this->tabProperty = $drAppli->getDataRow();
    }
  }
  
  /**
   *  Construit le token associé à cette application dans les modes d'affichages demandé
   * 
   * @param iTypeSheet   Type d'onglet
   * @param iSheet       Identifiant d'onglet
   * @param iSSheet      Identifiant de sous-onglet
   * @param strParam     Paramètres supplémentaires
   * 
   * @return string : token encodé
   */
  public function getToken($iTypeSheet, $iSheet, $iSSheet, $strParam="")
  {
    $strParamGen = "cont_id=".$this->cont_id."&appli_id=".$this->appli_id.
      "&iTypeSheet=".$iTypeSheet.
      "&iSheet=".$iSheet.
      "&iSSheet=".$iSSheet;
    $strParamGen .= $strParam;
    $strToken = AlkRequest::getEncodeParam($strParamGen);
    return $strToken;
  }

  /**
   *  Vérifie la sécurité. Retourne vrai si l'utilisateur peut afficher cette page, faux sinon.
   * @return boolean
   */
  public function verifSecu()
  {
    // ajoute le privilège ALK_PRIV_SPACE_ANIM si utilisateur animateur de l'espace en cours
    // ajoute le privilège ALK_PRIV_SPACE_USER si utilisateur valide
    $this->oSpace->verifIfUserIsAnim();
    $user_priv = AlkFactory::getSProperty("user_priv", ALK_PRIV_SPACE_NONE);
    
    $iTypeSheet = AlkRequest::getToken("iTypeSheet", ALK_TYPESHEET_CONSULT);

    // récupération du droit utilisateur sur l'application en cours
    $iRight = $this->getUserAppliRight();
    AlkFactory::setSProperty("user_right", $iRight);

    $bRes = 
      ($user_priv & ALK_PRIV_SPACE_USER) == ALK_PRIV_SPACE_USER 
      &&  ( 
          ($user_priv & ALK_PRIV_SPACE_ANIM) == ALK_PRIV_SPACE_ANIM 
      ||  ( $iTypeSheet == ALK_TYPESHEET_CONSULT 
            &&  ( $iRight>=ALK_APPLI_RIGHT_READ 
                  || ($user_priv & ALK_PRIV_SPACE_VIEWER) == ALK_PRIV_SPACE_VIEWER ) 
          ) 
      ||  ( $iTypeSheet == ALK_TYPESHEET_ADMIN      && $iRight>=ALK_APPLI_RIGHT_ADMIN ) 
      ||  ( $iTypeSheet == ALK_TYPESHEET_PROPRIETE  && $iRight==ALK_APPLI_RIGHT_PUBLI ) 
      ||  ( $iTypeSheet == ALK_TYPESHEET_POPUP ) 
      );

    // droit spécifique à l'application
    $bRes = $bRes && $this->verifSecuAppli();

    return $bRes;
  }

  /**
   *  Retourne le droit de l'utilisateur connecté sur l'appli en cours
   * @return int
   */
  public function getUserAppliRight()
  {
    $user_id = AlkFactory::getSProperty("user_id", "-1");
    $oQuery = AlkFactory::getQuery(ALK_ATYPE_ID_ESPACE);
    return $oQuery->getUserAppliRight($this->appli_id, $user_id);
  }

  /**
   *  Vérifie les droits spécifiques à l'application
   *        Retourne vrai si l'utilisateur connecté possède les droits, faux sinon
   * @return booleen
   */
  public function verifSecuAppli()
  {
    return true;
  }

  /**
   * Retourne dans un tableau contenant une sélection d'informations de l'application
   * 
   * @param object_id   identifiant de la donnée : 
   *                      cont_id si typeassoc3&=0, 
   *                      appli_id si typeassoc&1=1, 
   *                      cat_id si typeassoc&2=2, 
   *                      data_id si typeassoc&3=3
   * @param typeAssoc   type d'association : 0=cont, 1=appli, 2=cat, 3=data
   *                    si bit 2 on : présente les news uniquement
   *                    si bit 3 on : présente les docs publiés passés
   *                    si bit 4 on : présente les docs publiés présents
   *                    si bit 5 on : présente les docs à valider
   *                    si bit 6 on : ordre les éléments d'abord par intitulé de catégorie
   *                    si bit 7 on : présente les docs syndiqués
   *                    si <=3, présente tous les docs publiés : passés, présent, à venir
   * @param user_id     identifiant de l'utilisateur pour vérifier les droits, =-1 pour non vérification
   * @param lg          langue utilisé, _FR par défaut
   * @param atypeIdBloc type de l'application liée, =-1 si non utilisé
   * @return array
   */
  public function getDataContents($object_id, $typeAssoc, $user_id, $lg="_FR", $atypeIdBloc=-1)
  {
    return array();
  }

  /**
   * Ecrit sur la sortie standard, le flux rss correspondant au token passé
   * Lire le commentaire des méthodes virtuelles writeRss() pour connaitre le contenu du token 
   */
  public function writeRss()
  {
    $oFSyndXml = AlkFactory::getFSyndXmlWriter(ALK_RSS2);
    $oFSyndXml->setTitle("Aucune information disponible");
    $oFSyndXml->setLink(ALK_ROOT_URL);
    $oFSyndXml->setDescription("Flux RSS 2.0 provenant de ".ALK_APP_TITLE);
    
    $oFSyndXml->genarate(); 
  }

  /**
   * Ecrit sur la sortie standard, le flux xml correspondant au token passé
   * Lire le commentaire des méthodes virtuelles writeXml() pour connaitre le contenu du token
   */
  public function writeXml() { }
 
  /**
   * vérifie les liens gérés par l'application et retourne le résultat sous forme de tableau
   * format du tableau retourné : []["data_id", "token", "url", "desc"]
   * @return array
   * 
   */
  public function verifLinks()
  {
    return array(); 
  }
  
  /**
   * formate et retourne une chaîne pour les logs de tâches planifiées
   */
  public function formatCronLog($strLog, $rank=0)
  {
    $strSpace = "";
    for ( $i=0; $i<$rank; $i++ ) {
      $strSpace.= "  ";
    }
    return "[".date("d/m/Y H:i:s")."] ".$strSpace.$strLog."\n";
  }
  
  /**
   * Execute l'action passée en paramètre dans l'espace et l'application passés en paramètre
   * Méthode à spécialiser pour chaque application
   * @param time        timestamp heure de lancement du démon
   * @param cont_id     identifiant de l'espace (defaut 0 = Tous)
   * @param appli_id    identifiant de l'application (defaut -1 = Toutes)
   * @param action_id   identifiant de l'action d'une tâche planifiée (obligatoire)
   */
  public function cron($time=null, $cont_id=0, $appli_id=-1, $action_id) { }
}

?>