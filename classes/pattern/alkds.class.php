<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

/**
 * @package Alkanet_Class_Pattern

 * @class AlkDs
 * @brief Classe de base pour la connexion à une base de données (Dataset)
 */
abstract class AlkDs extends AlkObject
{
	/** Indice du datarow(dr) courant */
  protected $iCurDr;

	/** Nombre de datarow (dr) dans le dataset */
  protected $iCountDr;

	/** Nombre total de datarow(dr) dans le dataset paginé */
  protected $iCountTotDr;

	/** Booléen indiquant si l'on se trouve à la fin du dataset */
  protected $bEof;

	/** Objet contenant les données du dataset */
  protected $tabDr;

	/** Message de la dernière erreur rencontrée */
	protected $lastErr;

  /** tableau des types de champ */
  protected $tabType;

  /** encodage utilisé */
  protected $strDbEncoding;

  /**
   *  Constructeur par défaut de la classe
   *
   * @param conn           handle de la connexion en cours
   * @param strSQL         requête sql
   * @param idFirst        indice de début pour la pagination (=0 par défaut)
   * @param idLast         indice de fin pour la pagination (=-1 par défaut)
   * @param bErr           true si gestion des erreurs, false pour passer sous silence l'erreur
   * @param strDbEncoding  encodage du client sgbd
   */
  public function __construct($strDbEncoding=ALK_SGBD_ENCODING)
  {		
    parent::__construct();

    $this->iCurDr = -1;
    $this->iCountDr = 0;
    $this->iCountTotDr = 0;
    $this->bEof = true;
    $this->tabDr = array();
    $this->lastErr = "";
    $this->tabType = array();
    $this->strDbEncoding = $strDbEncoding;
  }

  public function __destruct ()
  {
  }

  /**
   * Retourne l'encodage client de la connexion SGBD
   * L'encodage est au format normé : UTF-8 ou ISO-8859-1
   * @return string
   */
  public function getEncoding()
  {
    return $this->strDbEncoding;
  }

	/**
   *  Fixe la nouvelle erreur rencontrée
   *
   * @param err false ou tableau associatif contenant l'erreur
   */
	protected function _setError($err)
  {
    $this->lastErr = $err;
  }

  /**
   * @  Retourne la dernière erreur rencontrée
   *
   * @return Retourne une chaine, vide si pas d'erreur, texte sinon
   */
	public function getLastError()
  {
    if( !is_string($this->lastErr) ) 
      $this->lastErr = "";
    return $this->lastErr;
  }

  /**
   *  Retourne le datarow (dr) correspondant à l'indice courant ($iCurDr)
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  abstract public function getRow();

  /**
   *  Retourne le datarow (dr) correspondant à l'indice courant ($iCurDr) puis passe au suivant
   *        Retourne false si EOF est atteint
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  abstract public function getRowIter();

  /**
   *  Retourne le datarow (dr) positionné à l'indice id
   *        Retourne false si l'indice n'est pas satisfaisant
   *         n'agit ni sur bEOF, ni sur le pointeur 
   *
   * @param id Indice du dataRow
   * @return Retourne un dataRow si ok, false sinon
   */
  abstract public function getRowAt($id);

  /**
   *  Place le pointeur du dataset sur le premier datarow
   *        Met à jour l'indice courant du dataset
   */
  abstract public function moveFirst();

  /**
   *  Place le pointeur du dataset sur le datarow précédent
   *        Met à jour l'indice courant du dataset
   */
	abstract public function movePrev();

  /**
   *  Place le pointeur du dataset sur le datarow suivant
   *        Met à jour l'indice courant du dataset
   */
  abstract public function moveNext();

  /**
   *  Place le pointeur du dataset sur le dernier datarow
   *        Met à jour l'indice courant du dataset
   */
  abstract public function moveLast( );

  /**
   *  Place le pointeur du dataset sur le (iCurDr+iNb) ième datarow
   *        Met à jour l'indice courant du dataset
   *
   * @param iNb Décalage positive ou négatif par rapport au dataRow courant
   */
  abstract public function move($iNb);

  /**
   *  Détruit l'objet dataset()
   */
  abstract public function close();

  /**
   *  Tri le dataSet après lecture dans la source donnée
   *        Effectue un tri à plusieurs niveaux
   *        Nécessite de connaite les noms de champ : ID et ID_PERE
   *        le dataset doit être déjà trié par niv puis par rang
   *
   * @param strFieldId  Nom du champ ID dans la requeête
   * @param strFieldIdp Nom du champ ID_PERE dans la requête
   */
  abstract public function setTree($strFieldId, $strFieldIdp);

  /**
   *  Retourne le type du numéro du champ
   *        Retourne 0 si chaine, 1 si nombre, 2 si date
   *
   * @return Retourne un nombre
   */
  public function getFieldType($iFieldPosition)
  {
    if( $iFieldPosition >= 0 && $iFieldPosition < count($this->tabType) )
      return $this->tabType[$iFieldPosition];
    return "";
  }
  
  /**
   *  Retourne le nombre d'élément dans le datarow
   * @return int
   */
  public function getCountDr()
  {
	  return $this->iCountDr;
  }

  /**
   *  Retourne le nombre d'élément total du datarow lorsqu'il est paginé
   * @return int
   */
  public function getCountTotDr()
  {
    return $this->iCountTotDr;
  }
  

  /**
   *  Retourne l'indice courant de lecture dans le dataset (NB: =iCurDr, =indice du dr suivant(faire -1))
   * @return int
   */
  public function getCurrentIndex()
  {
    return $this->iCurDr;
  }

  /**
   *  Retourne le booléen indiquant que la lecture des résultats est terminée
   * @return boolean
   */
  public function isEndOfFile()
  {
    return $this->bEof || ($this->iCurDr==$this->iCountDr);
  }
  
    /**
   *  Retourne le DS résultant en JSON
   * 
   * @param strTableName Le nom de la table dans la base de données
   * @return string
   */
  public function getJsonFromDs($strTableName="alk")
  {
   return "{".$strTableName.":".json_encode( $this->tabDr)."}";
  }
  
  
}

?>