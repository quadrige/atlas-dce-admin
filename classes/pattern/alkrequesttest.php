<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

// Call AlkRequestTest::main() if this source file is executed directly.
if (!defined("PHPUnit2_MAIN_METHOD")) {
  define("PHPUnit2_MAIN_METHOD", "AlkRequestTest::main");
}

require_once "PHPUnit2/Framework/TestCase.php";
require_once "PHPUnit2/Framework/TestSuite.php";

// You may remove the following line when all tests have been implemented.
require_once "PHPUnit2/Framework/IncompleteTestError.php";

require_once "../../libconf/app_conf.php";
require_once ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php";
require_once ALK_ALKANET_ROOT_PATH."classes/pattern/alkrequest.class.php";

/**
 * @package Alkanet_Class_Pattern_Test
 * Test class for AlkRequest.
 * Generated by PHPUnit2_Util_Skeleton on 2007-09-28 at 12:14:41.
 */
class AlkRequestTest extends PHPUnit2_Framework_TestCase {

  public $strTxt;

  /**
   * Runs the test methods of this class.
   *
   * @access public
   * @static
   */
  public static function main() {
    require_once "PHPUnit2/TextUI/TestRunner.php";

    $suite  = new PHPUnit2_Framework_TestSuite("AlkRequestTest");
    $result = PHPUnit2_TextUI_TestRunner::run($suite);
  }

  /**
   * Sets up the fixture, for example, open a network connection.
   * This method is called before a test is executed.
   *
   * @access protected
   */
  protected function setUp() 
  {
    $this->strTxt01 = -1;
    $this->strTxt02 = "Non envoyé";
    $this->strTxt03 = "";

    $this->strTxt1 = 125.2;
    $this->strTxt2 = "Une chaine c'est bien, mais avec des \"é'_è--è&&éàç_à_çà*ù!:;, c'est mieux.";
    $this->strTxt3 = "' or 1=1 or AGENT_NOM like '";

    $this->tabFile = array("name"     => "test.txt",
                           "type"     => "text/plain",
                           "size"     => "2",
                           "tmp_name" => "tmp1223311",
                           "error"    => "");

    //$_GET["pg0"]; non existant
    $_GET["pg1"] = $this->strTxt1;
    $_GET["pg2"] = $this->strTxt2;
    $_GET["pg3"] = $this->strTxt3;
    
    //$_POST["pp0"]; non existant
    $_POST["pp1"] = $_GET["pg1"];
    $_POST["pp2"] = $_GET["pg2"];
    $_POST["pp3"] = $_GET["pg3"];

    //$_REQUEST["pq0"]; non existant
    $_REQUEST["pq1"] = $_GET["pg1"];
    $_REQUEST["pq2"] = $_GET["pg2"];
    $_REQUEST["pq3"] = $_GET["pg3"];

    $_FILES["pf"] = $this->tabFile;

  }

  /**
   * Tears down the fixture, for example, close a network connection.
   * This method is called after a test is executed.
   *
   * @access protected
   */
  protected function tearDown() {
  }

  /**
   * @todo Implement test_GET().
   */
  public function test_GET() 
  {
    $this->assertTrue(AlkRequest::_GET("pg0")                                == "",              "Cas 1.1 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg0", $this->strTxt01)               == $this->strTxt01, "Cas 1.2 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg0", $this->strTxt02)               == $this->strTxt02, "Cas 1.3 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg0", $this->strTxt01, "is_numeric") == $this->strTxt01, "Cas 1.4 : Erreur _GET.");

    $this->assertTrue(AlkRequest::_GET("pg1")                                == $this->strTxt1,  "Cas 2.1 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg1", $this->strTxt01)               == $this->strTxt1,  "Cas 2.2 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg1", $this->strTxt01, "is_numeric") == $this->strTxt1,  "Cas 2.3 : Erreur _GET.");

    $this->assertTrue(AlkRequest::_GET("pg2")                                == $this->strTxt2,  "Cas 3.1 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg2", $this->strTxt02)               == $this->strTxt2,  "Cas 3.2 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg2", $this->strTxt02, "is_string")  == $this->strTxt2,  "Cas 3.4 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg2", $this->strTxt01, "is_numeric") == $this->strTxt01, "Cas 3.5 : Erreur _GET.");

    $this->assertTrue(AlkRequest::_GET("pg3")                                == $this->strTxt3,  "Cas 4.1 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg3", $this->strTxt03)               == $this->strTxt3,  "Cas 4.3 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg3", $this->strTxt03, "is_string")  == $this->strTxt3,  "Cas 4.4 : Erreur _GET.");
    $this->assertTrue(AlkRequest::_GET("pg3", $this->strTxt01, "is_numeric") == $this->strTxt01, "Cas 4.5 : Erreur _GET.");
  }

  /**
   * @todo Implement test_POST().
   */
  public function test_POST() 
  {
    $this->assertTrue(AlkRequest::_POST("pp0")                                == "",              "Cas 1.1 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp0", $this->strTxt01)               == $this->strTxt01, "Cas 1.2 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp0", $this->strTxt02)               == $this->strTxt02, "Cas 1.3 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp0", $this->strTxt01, "is_numeric") == $this->strTxt01, "Cas 1.4 : Erreur _POST.");

    $this->assertTrue(AlkRequest::_POST("pp1")                                == $this->strTxt1,  "Cas 2.1 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp1", $this->strTxt01)               == $this->strTxt1,  "Cas 2.2 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp1", $this->strTxt01, "is_numeric") == $this->strTxt1,  "Cas 2.3 : Erreur _POST.");

    $this->assertTrue(AlkRequest::_POST("pp2")                                == $this->strTxt2,  "Cas 3.1 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp2", $this->strTxt02)               == $this->strTxt2,  "Cas 3.2 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp2", $this->strTxt02, "is_string")  == $this->strTxt2,  "Cas 3.4 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp2", $this->strTxt01, "is_numeric") == $this->strTxt01, "Cas 3.5 : Erreur _POST.");

    $this->assertTrue(AlkRequest::_POST("pp3")                                == $this->strTxt3,  "Cas 4.1 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp3", $this->strTxt03)               == $this->strTxt3,  "Cas 4.3 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp3", $this->strTxt03, "is_string")  == $this->strTxt3,  "Cas 4.4 : Erreur _POST.");
    $this->assertTrue(AlkRequest::_POST("pp3", $this->strTxt01, "is_numeric") == $this->strTxt01, "Cas 4.5 : Erreur _POST.");
  }

  /**
   * @todo Implement test_REQUEST().
   */
  public function test_REQUEST() 
  {
    $this->assertTrue(AlkRequest::_REQUEST("pq0")                                == "",              "Cas 1.1 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq0", $this->strTxt01)               == $this->strTxt01, "Cas 1.2 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq0", $this->strTxt02)               == $this->strTxt02, "Cas 1.3 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq0", $this->strTxt01, "is_numeric") == $this->strTxt01, "Cas 1.4 : Erreur _REQUEST.");

    $this->assertTrue(AlkRequest::_REQUEST("pq1")                                == $this->strTxt1,  "Cas 2.1 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq1", $this->strTxt01)               == $this->strTxt1,  "Cas 2.2 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq1", $this->strTxt01, "is_numeric") == $this->strTxt1,  "Cas 2.3 : Erreur _REQUEST.");

    $this->assertTrue(AlkRequest::_REQUEST("pq2")                                == $this->strTxt2,  "Cas 3.1 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq2", $this->strTxt02)               == $this->strTxt2,  "Cas 3.2 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq2", $this->strTxt02, "is_string")  == $this->strTxt2,  "Cas 3.4 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq2", $this->strTxt01, "is_numeric") == $this->strTxt01, "Cas 3.5 : Erreur _REQUEST.");

    $this->assertTrue(AlkRequest::_REQUEST("pq3")                                == $this->strTxt3,  "Cas 4.1 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq3", $this->strTxt03)               == $this->strTxt3,  "Cas 4.3 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq3", $this->strTxt03, "is_string")  == $this->strTxt3,  "Cas 4.4 : Erreur _REQUEST.");
    $this->assertTrue(AlkRequest::_REQUEST("pq3", $this->strTxt01, "is_numeric") == $this->strTxt01, "Cas 4.5 : Erreur _REQUEST.");

  }

  /**
   * @todo Implement test_GETint().
   */
  public function test_GETint() 
  {
    $this->assertTrue(AlkRequest::_GETint("pf0")                                == array(),               "Cas 1.1 : Erreur _GETint.");
    $this->assertTrue(AlkRequest::_GETint("pg0", $this->strTxt01)               == $this->strTxt01, "Cas 1.2 : Erreur _GETint.");
    $this->assertTrue(AlkRequest::_GETint("pg0", $this->strTxt02)               == $this->strTxt02, "Cas 1.3 : Erreur _GETint.");

    $this->assertTrue(AlkRequest::_GETint("pg1")                                == $this->strTxt1,  "Cas 2.1 : Erreur _GETint.");
    $this->assertTrue(AlkRequest::_GETint("pg1", $this->strTxt01)               == $this->strTxt1,  "Cas 2.2 : Erreur _GETint.");
    $this->assertTrue(AlkRequest::_GETint("pg1", $this->strTxt01, "is_numeric") == $this->strTxt1,  "Cas 2.3 : Erreur _GETint.");

    $this->assertTrue(AlkRequest::_GETint("pg2")                                == 0,               "Cas 3.1 : Erreur _GETint.");
    $this->assertTrue(AlkRequest::_GETint("pg2", $this->strTxt01)               == $this->strTxt01, "Cas 3.2 : Erreur _GETint.");
  }

  /**
   * @todo Implement test_POSTint().
   */
  public function test_POSTint() 
  {
    $this->assertTrue(AlkRequest::_POSTint("pp0")                                == 0,               "Cas 1.1 : Erreur _POSTint.");
    $this->assertTrue(AlkRequest::_POSTint("pp0", $this->strTxt01)               == $this->strTxt01, "Cas 1.2 : Erreur _POSTint.");
    $this->assertTrue(AlkRequest::_POSTint("pp0", $this->strTxt02)               == $this->strTxt02, "Cas 1.3 : Erreur _POSTint.");

    $this->assertTrue(AlkRequest::_POSTint("pp1")                                == $this->strTxt1,  "Cas 2.1 : Erreur _POSTint.");
    $this->assertTrue(AlkRequest::_POSTint("pp1", $this->strTxt01)               == $this->strTxt1,  "Cas 2.2 : Erreur _POSTint.");
    $this->assertTrue(AlkRequest::_POSTint("pp1", $this->strTxt01, "is_numeric") == $this->strTxt1,  "Cas 2.3 : Erreur _POSTint.");

    $this->assertTrue(AlkRequest::_POSTint("pp2")                                == 0,               "Cas 3.1 : Erreur _POSTint.");
    $this->assertTrue(AlkRequest::_POSTint("pp2", $this->strTxt01)               == $this->strTxt01, "Cas 3.2 : Erreur _POSTint.");

  }

  /**
   * @todo Implement test_REQUESTint().
   */
  public function test_REQUESTint() 
  {
    $this->assertTrue(AlkRequest::_REQUESTint("pq0")                                == 0,               "Cas 1.1 : Erreur _REQUESTint.");
    $this->assertTrue(AlkRequest::_REQUESTint("pq0", $this->strTxt01)               == $this->strTxt01, "Cas 1.2 : Erreur _REQUESTint.");
    $this->assertTrue(AlkRequest::_REQUESTint("pq0", $this->strTxt02)               == $this->strTxt02, "Cas 1.3 : Erreur _REQUESTint.");

    $this->assertTrue(AlkRequest::_REQUESTint("pq1")                                == $this->strTxt1,  "Cas 2.1 : Erreur _REQUESTint.");
    $this->assertTrue(AlkRequest::_REQUESTint("pq1", $this->strTxt01)               == $this->strTxt1,  "Cas 2.2 : Erreur _REQUESTint.");
    $this->assertTrue(AlkRequest::_REQUESTint("pq1", $this->strTxt01, "is_numeric") == $this->strTxt1,  "Cas 2.3 : Erreur _REQUESTint.");

    $this->assertTrue(AlkRequest::_REQUESTint("pq2")                                == 0,               "Cas 3.1 : Erreur _REQUESTint.");
    $this->assertTrue(AlkRequest::_REQUESTint("pq2", $this->strTxt01)               == $this->strTxt01, "Cas 3.2 : Erreur _REQUESTint.");

  }

  /**
   * @todo Implement test_FILES().
   */
  public function test_FILES() 
  {
    $a = AlkRequest::_FILES("pf0");
    $this->assertTrue(empty($a)==true    , "Cas 1.1 : Erreur _FILES.");
    $this->assertEquals(AlkRequest::_FILES("pf", $this->tabFile), "Cas 1.2 : Erreur _FILES.");
  }

  /**
   * @todo Implement testGetEncodeParam().
   */
  public function testGetEncodeParam() 
  {

  }

  /**
   * @todo Implement testGetDecodeParam().
   */
  public function testGetDecodeParam() 
  {

  }
}

// Call AlkRequestTest::main() if this source file is executed directly.
if (PHPUnit2_MAIN_METHOD == "AlkRequestTest::main") {
  AlkRequestTest::main();
}
?>
