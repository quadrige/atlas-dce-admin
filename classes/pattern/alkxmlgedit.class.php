<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkXmlGEedit
 * @brief Classe gérant la création d'une page xml éditoriale
 */
class AlkXmlGEdit extends AlkObject
{
  /** identifiant de la page à générer */
  protected $page_id;

  /** title de la page à générer */
  protected $page_title;
  
  /** langue utilisée */
  protected $lg;
  
  /** contenu chaine du xml généré */
  protected $strXml;
  
  /** encodage du xml généré */
  protected $strEncoding;
  protected $bChangeEncoding;
  
  /** tableau contenant les informations de tous les blocs de la page */
  protected $tabBlocs;
  
  /**
   * Constructeur par défaut
   * @param page_id    identifiant de la page générée
   * @param page_title titre de la page dans la langue sélectionnée
   * @param lg         chaine contenu le suffixe de la langue utiliée (_FR, _UK, etc...), =_FR par défaut 
   */
  public function __construct($page_id, $page_title, $lg="_FR")
  {
    parent::__construct();
    $this->page_id = $page_id;
    $this->page_title = $page_title;
    $this->lg = $lg; 
    $this->strEncoding = ALK_HTML_ENCODING;
    $this->bChangeEncoding = false;
    $this->strXml = "";
    $this->tabBlocs = array();
  }
  
  /**
   * Retourne le code xml de la page
   * @return string
   */
  public function getXml()
  {
    $this->createXml();
    return $this->strXml; 
  }
  
  /**
   * Fixe l'encodage du xml
   * @param strEncoding  chaine contenu l'intitulé de l'encodage souhaitée pour l'export xml 
   */
  public function setEncoding($strEncoding)
  {
     $this->strEncoding = $strEncoding;
  }
  
  /**
   * Ajoute un bloc à la page
   * @param bloc_id        identifiant du bloc
   * @param tabTypeUpdate  tableau contenant la méta-info sur le bloc
   * @param tabContents    tableau contenant les infos du bloc
   * @param iColonne       entier = 1 si info en colonne, =0 sinon, =0 par défaut
   * @param iblocUne       entier = 1 si info à la une, =0 sinon, =0 par défaut
   * @param bloc_typeassoc type d'association : 0=aucune, 1=appli, 2=cat, 3=data
   *                       si bit 2 on : présente les news uniquement
   *                       si bit 3 on : présente les docs publiés passés
   *                       si bit 4 on : présente les docs publiés présents
   *                       si <=3, présente tous les docs publiés : passés, présent, à venir
   *                       =0 par défaut
   */
  public function addBloc($bloc_id, $tabTypeUpdate, $tabContents, $iColonne="0", $iblocUne="0", $bloc_typeassoc="0")
  {
    $typeAssoc = ( $bloc_typeassoc>16 ? $bloc_typeassoc-16 : $bloc_typeassoc ); 
    $typeAssoc = ( $typeAssoc>8 ? $typeAssoc-8 : $typeAssoc ); 
  
    $this->tabBlocs[] = array("bloc_id" => $bloc_id,
                              "tabTypeUpdate" => $tabTypeUpdate, 
                              "tabContents" => $tabContents,
                              "iColonne" => $iColonne, 
                              "iblocUne" => $iblocUne,
                              "typeAssoc" => $typeAssoc);
  }
  
  /**
   * Retourne un tableau contenant toutes les informations sur les blocs
   * @return array
   */
  public function getArray()
  {
    return  $this->tabBlocs;
  }
  
  /**
   * Retourne une url correctement formatée
   * @param strUrl  adresse url à vérifier
   * @return string
   */
  private function getUrl($strUrl)
  {
    if( mb_substr($strUrl, 0, 7) != "http://" &&
        mb_substr($strUrl, 0, 6) != "ftp://" &&
        mb_substr($strUrl, 0, 7) != "mailto:" &&
        mb_substr($strUrl, 0, 11) != "javascript:" ) {
      $strUrl = "http://".$strUrl;
    }
    return $strUrl;
  }
  
  /**
   * Création puis retourne de la chaine xml correspondant à la structure de la page
   */
  private function createXml()
  {
    $strRC = "\n";
    $oCleaner = AlkFactory::getHtmlCleaner();
    
    $strXml = "<?xml version='1.0' encoding='".$this->strEncoding."'?>".$strRC.
      "<contenu lg='".$this->lg."'>".$strRC.
      "<page id='".$this->page_id."'>".$strRC.
      "<titre>".$this->page_title."</titre>".$strRC.
      "<blocs nbBloc='".count($this->tabBlocs)."'>".$strRC;
    
    $iCpt = 0;
    foreach($this->tabBlocs as $i => $tabBloc) {
      $strXml .= "<bloc colonne='".$tabBloc["iColonne"]."' id='".$tabBloc["bloc_id"]."' typeAssoc='".$tabBloc["typeAssoc"]."'>".$strRC;
        
      switch( $tabBloc["tabTypeUpdate"]["type"] ) {
      case ALK_GEDIT_TYPEUPDATE_EDITEUR:
        $oCleaner->cleanWithTidy($tabBloc["tabContents"][0]["data_desc"], true, true);

        $strXml .= "<".$tabBloc["tabTypeUpdate"]["xmltag"].">".$strRC.
          $tabBloc["tabContents"][0]["data_desc"].$strRC.
          "</".$tabBloc["tabTypeUpdate"]["xmltag"].">".$strRC;
        break;
        
      case ALK_GEDIT_TYPEUPDATE_SELECTEDIT:
        $strXml .= "<contenu_bloc nbbloc='".count($tabBloc["tabContents"])."'>".$strRC;
        
        foreach($tabBloc["tabContents"] as $tabContent) {
          if( array_key_exists("data_text", $tabContent) && $tabContent["data_text"]!="" )
            $oCleaner->cleanWithTidy($tabContent["data_text"], true, true);
          
          $strXml .= "<".$tabBloc["tabTypeUpdate"]["xmltag"].
            " id='".$tabContent["data_id"]."'". 
            /*" cont_id='".$tabContent["cont_id"]."'".
            " appli_id='".$tabContent["appli_id"]."'".*/
            ( array_key_exists("data_token", $tabContent) && $tabContent["data_token"]!=""
              ? " token='".$tabContent["data_token"]."'"
              : "" ).
            ">".$strRC;
          
          foreach ( $tabContent as $key=>$value )
            $strXml .= $this->getXmlTagForContent($iCpt, $tabBloc, $tabContent, $key, $strRC);
            
          $strXml .= "</".$tabBloc["tabTypeUpdate"]["xmltag"].">".$strRC;
          $iCpt++;  
        }
        
        $strXml .= "</contenu_bloc>".$strRC;
        break;
      }
      $strXml .= "</bloc>".$strRC;
    }
    
    $strXml .= "</blocs>".$strRC."</page>".$strRC."</contenu>".$strRC;
      
    if( $this->bChangeEncoding ) {
      $strXml = mb_convert_encoding($strXml, $this->strEncoding, ALK_HTML_ENCODING);
    }

    $this->strXml = $strXml;
  }
  
  function getXmlTagForContent($iCpt, $tabBloc, $tabContent, $keyContent, $strRC)
  {
    if ( $tabContent[$keyContent] == "" ) return "";
    switch ($keyContent)
    {
      case "cont_id" :      case "appli_id" :
      case "data_type" : case "data_token" :
      case "data_rank" :    case "data_id" :
        return "";
        
      case "cat_name" :
        return "<categorie>".$tabContent["cat_name"]."</categorie>".$strRC;
      case "data_title" :
        return "<titre>".$tabContent["data_title"]."</titre>".$strRC;
      case "data_desc" :   
        return "<desc>".$tabContent["data_desc"]."</desc>".$strRC;
      case "data_text" :   
        return "<texte>".$tabContent["data_text"]."</texte>".$strRC ;
      case "data_descl" :   
        return "<descl>".$tabContent["data_descl"]."</descl>".$strRC;
      case "data_comment" :   
        return "<comment>".$tabContent["data_comment"]."</comment>".$strRC ;
      case "data_ref" :   
        return "<reference>".$tabContent["data_ref"]."</reference>".$strRC;
      case "data_src" :   
        return "<source>".$tabContent["data_src"]."</source>".$strRC ;
      case "data_datepdeb" :   
        return "<datepdeb>".$tabContent["data_datepdeb"]."</datepdeb>".$strRC;
      case "data_datepfin" :   
        return "<datepfin>".$tabContent["data_datepfin"]."</datepfin>".$strRC;
      case "data_date" :   
        return "<date>".$tabContent["data_date"]."</date>".$strRC;
      case "data_datedeb" :   
        return "<datedeb>".$tabContent["data_datedeb"]."</datedeb>".$strRC;
      case "data_datefin" :   
        return "<datefin>".$tabContent["data_datefin"]."</datefin>".$strRC;
      case "data_auteur" :   
        return "<auteur>".$tabContent["data_auteur"]."</auteur>".$strRC;
      case "data_mail" :   
        return "<mail>".$tabContent["data_mail"]."</mail>".$strRC;
      case "data_visuel" :   
        return "<visuel width='".$tabContent["data_visuel_width"]."'".
                " height='".$tabContent["data_visuel_height"]."'>".
                $tabContent["data_visuel"]."</visuel>".$strRC ;
      case "data_url" :   
        return "<url>".$this->getUrl($tabContent["data_url"])."</url>".$strRC;
      case "data_desc" :   
        return "<pj id='".$tabContent["data_id"]."'" .
                " nbpj='".$tabContent["data_nbpj"]."'".
                " module='".$tabBloc["tabTypeUpdate"]["xmltag"].$iCpt."'".
                " align='".( $tabBloc["tabTypeUpdate"]["xmltag"]=="actualite" || $tabBloc["iColonne"]=="1" ? "right" : "left" )."'".
                " token='".AlkRequest::getEncodeParam("cont_id=".$tabContent["cont_id"]."&appli_id=".$tabContent["appli_id"].
                                                      "&iTypeSheet=".ALK_TYPESHEET_CONSULT."&iSheet=".ALK_SHEET_NONE.
                                                      "&data_id=".$tabContent["data_id"].
                                                      "&tablepj=".$tabBloc["tabTypeUpdate"]["tablepj"].
                                                      "&fieldpj=".$tabBloc["tabTypeUpdate"]["fieldpj"].
                                                      "&uploadDir=".$tabBloc["tabTypeUpdate"]["uploadDir"].
                                                      "&lg=".$this->lg).
                                                      "'/>".$strRC;
      default :
        return "<".$keyContent.">".$tabContent[$keyContent]."</".$keyContent.">";
    }
  }
  
  /**
   * Retourne dans une chaine, le contenu xsl correspondant au xml généré par createXml()
   * charge le xsl à partir de la feuille page.xsl concaténé avec le fichier contenu-page.xsl
   * Le chemin vers les feuilles de styles est donné par ALK_GEDIT_PATH_XSL si elle existe et non vide,
   * sinon par ALK_ALKANET_ROOT_PATH."scripts/site/xsl/
   * Essaie d'abord d'associer [$cont_id]_page.xsl si elle existe 
   * @return string
   */
  public static function getXsl()
  {
    $strPathXsl = ( defined("ALK_GEDIT_PATH_XSL") && ALK_GEDIT_PATH_XSL != ""
                    ? ALK_GEDIT_PATH_XSL
                    : ALK_ALKANET_ROOT_PATH."scripts/site/xsl/" );
      
    // fichier contenant le xsl correspondant au html éditorial
    $cont_id = AlkRequest::getToken("cont_id", "-1");
    $strPathFileXsl = $strPathXsl.$cont_id."_page.xsl";
    if( !(file_exists($strPathFileXsl) && is_file($strPathFileXsl)) ) {
      $strPathFileXsl = $strPathXsl."page.xsl";
    }

    // fichier contenant le xsl correspondant au xml gedit
    $strPathFileXslGEdit = $strPathXsl."contenu-page.xsl";
    
    // fusion des 2 xsl
    $strXsl = "";
    $hFile = @fopen($strPathFileXsl, "r");
    if( $hFile ) {
      $strXsl .= trim(fread($hFile, filesize($strPathFileXsl)));
      fclose($hFile);
    }
    
    $hFile = @fopen($strPathFileXslGEdit, "r");
    if( $hFile ) {
      $strXsl .= trim(fread($hFile, filesize($strPathFileXslGEdit)));
      fclose($hFile);
    }
    
    $strXsl .= "</xsl:stylesheet>";

    $strXsl = mb_ereg_replace("ISO-8859-1", ALK_HTML_ENCODING, $strXsl); 

    /*for($i = 0; $i < strlen($this->strXSL ); $i++) {
      $intChr = ord($this->strXSL[$i]);
      if ($intChr < 32 || $intChr > 128) {
        echo $this->strXSL[$i]." test_ ".$i."_".$intChr."<br>";
      }
    }*/
    
    //$this->strXSL = str_replace(chr(239),"",$this->strXSL);
    //$this->strXSL = str_replace(chr(187),"",$this->strXSL);
    //$this->strXSL = str_replace(chr(191),"",$this->strXSL);

    return $strXsl;
  }
  
}
?>