<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


/**
 * @package Alkanet_Class_Pattern
 * 
 * @interface AlkIntCRV
 * @brief Interface nécessaire pour la gestion des circuits de validation
 */
interface AlkIntCRV
{
  /**
   *  Retourne l'url pour acceder directement à l'objet mis en cause par la validation
   * @param objet_id   identifiant de l'information associée au circuit
   * @return string
   */
  public function getCrvUrlAcces($objet_id);

  /**
   * Retourne l'intitule et l'auteur de l'objet mis en cause par la validation
   * Retourne un tableau associatif [[Titre] => valeur, [AUTEUR] => valeur]
   * @param objet_id  identifiant de l'information associée au circuit
   * @return array
   */
  public function getCrvObjetInfo($objet_id);

  /**
   * Ajoute une piece jointe à l'information associée au circuit
   * le fichier se trouve déjà dans le répertoire d'upload de l'application associée
   * il suffit de le duppliquer et le nommer à la norme de cette application
   * @param objet_id       Identifiant de l'information
   * @param strFileName    nom du fichier joint
   * @param strFileNameAff nom du fichier sans les préfixes
   * @param auteur_id      identifiant de l'utilisateur ayant publié le doc
   */  
  public function addCrvPj($doc_id, $strFileName, $strFileNameAff, $auteur_id);
  
  /**
   *   Publie le document au sein de l'application associée
   * @param objet_id   identifiant de l'information associée au circuit
   */
  public function crvPublier($objet_id);

  /**
   *   Retire de la publication le document
   * @param objet_id   identifiant de l'information associée au circuit
   */
  public function crvRetirerPublication($objet_id);


}

?>