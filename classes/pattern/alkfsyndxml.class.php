<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

// RSS 0.90  officiellement obsolete depuis 1.0
// RSS 0.91, 0.92, 0.93 and 0.94  officiellement obsolete depuis 2.0

define("ALK_DATE_RSS", "D, d M Y G:i:s T"); // RFC-822 : Wed, 04 Feb 2008 08:00:00 EST
define("ALK_DATE_ATOM", "c"); // ISO 860, ex.: 2003-12-13T18:30:02.25+01:00

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkFSyndXmlItem
 * Elément utilisé pour la classe AlkFSyndXmlWriter
 * FSyndXml pour Format de Syndication xml
 */
class AlkFSyndXmlItem
{
  /** ensemble des noeuds du flux */
  private $elements = array();

  /** type de flux */
  private $version;
  
  /**
  * Constructor
  *
  * @param    contant     (ALK_RSS1/ALK_RSS2/ALK_ATOM) ALK_RSS2 is default.
  */
  function __construct($version = ALK_RSS2)
  {    
    $this->version = $version;
  }
  
  /**
   * Ajoute un élément à la collection
   *
   * @param strName     Nom du tag
   * @param strContent  Contenu du tag
   * @param attributes  tableau contenant les attributs au format 'attrName' => 'attrValue' si non null par défaut 
   */
  public function addElement($strName, $strContent, $attributes = null)
  {
    $this->elements[$strName]['name']       = $strName;
    $this->elements[$strName]['content']    = $strContent;
    $this->elements[$strName]['attributes'] = $attributes;
  }
  
  /**
   * Ajout des éléments par tableau
   * Chaque élément du tableau eltArray est ajouté à la collection en appelant addElement()
   *
   * @param eltArray  tableau au format 'tagName' => 'tagContent'
   */
  public function addElementArray($eltArray)
  {
    if(! is_array($eltArray)) return;
    foreach ($eltArray as $strName => $strContent) {
      $this->addElement($strName, $strContent);
    }
  }
  
  /**
   * Retourne la collection des éléments
   * @return array
   */
  public function getElements()
  {
    return $this->elements;
  }
  
  /**
   * Mémorise l'élément 'description'
   *
   * @param strDesc  Valeur du tag description
   */
  public function setDescription($strDesc)
  {
    $tag = ( $this->version == ALK_ATOM ? 'summary' : 'description' );
    $this->addElement($tag, $strDesc);
  }
  
  /**
   * Mémorire l'élément 'title'
   * 
   * @param strTitle  valeur du titre
   */
  public function setTitle($strTitle)
  {
    $this->addElement('title', $strTitle);      
  }
  
  /**
   * Mémorise l'élément 'date'
   *
   * @param dDate     valeur de la date (timestamp ou chaine)
   * @param strFormat format de la date passée en paramètre (pris en compte si dDate n'est pas numérique)
   *                  = chaine vide par défaut pour détection auto par strtotime() (fonctionne avec format anglais uniquement)
   *                  le format utilisé est celui de la fonction strftime()
   */
  public function setDate($dDate, $strFormat="")
  {
    $iDate = $dDate;
    if( !is_numeric($dDate) ) { 
      if( $strFormat == "" ) {
        $iDate = strtotime($dDate);
      } else {
        $tabDate = strptime($dDate, $strFormat);
        if( is_array($tabDate) ) {
          $iDate = mktime($tabDate["tm_hour"], $tabDate["tm_min"],$tabDate["tm_sec"],
                          1+$tabDate["tm_mon"], $tabDate["tm_mday"], 1900+$tabDate["tm_year"]);
        } else {
          $iDate = 0;          
        }
      }
    }
    
    if( $this->version == ALK_ATOM ) {
      $tag    = 'updated';
      $value  = date(ALK_DATE_ATOM, $iDate);
    }        
    elseif($this->version == ALK_RSS2) {
      $tag    = 'pubDate';
      $value  = date(ALK_DATE_RSS, $iDate);
    }
    else {
      $tag    = 'dc:date';
      $value  = date("Y-m-d", $iDate);
    }
    
    $this->addElement($tag, $value);    
  }
  
  /**
   * Mémorise l'élémént 'link'
   *
   * @param strLink  valeur du tag
   */
  public function setLink($strLink)
  {
    $strLink = mb_ereg_replace("&", "&amp;", $strLink);
    if($this->version == ALK_RSS2 || $this->version == ALK_RSS1) {
      $this->addElement('link', $strLink);
    }
    else {
      $this->addElement('link','',array('href'=>$strLink));
      $this->addElement('id', ALKFSyndXmlWriter::uuid($strLink,'urn:uuid:'));
    }
  }
  
  /**
   * Mémorise l'élément 'encloser' uniquement pour RSS 2.0
   *
   * @param url     attribut url du tag encloser
   * @param length  attribut longueur du tag encloser
   * @param type    attribut type du tag encloser
   * @param name    attribut optionnel, permettant de donner un intitulé à l'url
   */
  public function setEncloser($url, $length, $type, $name="")
  {
    $url = mb_ereg_replace("&", "&amp;", $url);
    $attributes = array('url'=>$url, 'length'=>$length, 'type'=>str_replace("\"", "'", $type));
    if( $name != "" ) {
      $attributes['name'] = $name;
    }
    $this->addElement('enclosure','',$attributes);
  }  
  
  /**
  * Mémorise l'élément 'guid'
  * 
  * @param    strGuid   valeur du tag
  */
  public function setGuid($strGuid)
  {
    $strGuid = mb_ereg_replace("&", "&amp;", $strGuid);
    $this->addElement('guid', $strGuid);
  }
}


/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkFSyndXmlWriter
 * Générateur de flux RSS 1.0, RSS2.0 et ATOM
 * FSyndXml pour Format de Syndication xml
 */
class AlkFSyndXmlWriter
{
  /** ensemble des éléments channel */  
  protected $channels = array();
   
  /** ensemble d'objet de type AlkFSyndXmlItem */
  protected $items = array();
  
  /** mémorise les informations du flux */
  protected $data = array(); 
  
  /** */
  protected $CDATAEncoding = array();  // The tag names which have to encoded as CDATA
  
  /** type de flux à générer */ 
  private $version = null; 
  
  /**
   * Constructeur
   * 
   * @param  strVersion  version de la file : ALK_RSS1 / ALK_RSS2 / ALK_ATOM       
   */ 
  function __construct($strVersion = ALK_RSS2)
  { 
    $this->version = $strVersion;
      
    $this->channels['title'] = $strVersion;
    $this->channels['link']  = 'http://www.alkante.com';
        
    // nom des tags à encoder dans CDATA
    $this->CDATAEncoding = array('description', 'content:encoded', 'summary');
  }

  /**
   * Mémorise l'élément channel
   *
   * @param strName     nom du tag channel
   * @param strContent  contenu du tag channel
   */
  public function setChannelElement($strName, $strContent)
  {
    $this->channels[$strName] = $strContent ;
  }
  
  /**
   * Set multiple channel elements from an array. Array elements 
   * should be 'channelName' => 'channelContent' format.
   * 
   * @param    array   array of channels
   * @return   void
   */
  public function setChannelElementsFromArray($elementArray)
  {
    if(! is_array($elementArray)) return;
    foreach ($elementArray as $elementName => $content) 
    {
      $this->setChannelElement($elementName, $content);
    }
  }
  
  /**
   * Ecrit sur la sortie standard la file actuelle RSS/ATOM
   */ 
  public function genarate()
  {
    header("Content-type: text/xml");
    
    $this->printHead();
    $this->printChannels();
    $this->printItems();
    $this->printTale();
  }
  
  /**
   * Creation puis retourne une instance d'objet de type AlkFSyndXmlItem.
   * 
   * @return object de type AlkFSyndXmlItem
   */
  public function createNewItem()
  {
    $Item = new AlkFSyndXmlItem($this->version);
    return $Item;
  }
  
  /**
  * Ajoute un élément de type AlkFSyndXmlItem à la classe
  * 
  * @param  oItem  instance d'un objet de type AlkFSyndXmlItem 
  */
  public function addItem($oItem)
  {
    $this->items[] = $oItem;    
  }
  
  /**
  * Mémorise l'élément 'title'
  * 
  * @param strTitle  value du tag
  */
  public function setTitle($strTitle)
  {
    $this->setChannelElement('title', $strTitle);
  }
  
  /**
   * Mémorise l'élément 'description'
   * 
   * @param  strDesc  valeur du tag
   */
  public function setDescription($strDesc)
  {
    $this->setChannelElement('description', $strDesc);
  }
  
  /**
  * Mémorise l'élément 'link' 
  * 
  * @param  strLink  valeur du tag
  */
  public function setLink($strLink)
  {
    $strLink = mb_ereg_replace("&", "&amp;", $strLink);
    $this->setChannelElement('link', $strLink);
  }
  
  /**
  * Mémorise l'élément 'image'
  * 
  * @param    strTitle  titre de l'image
  * @param    strLink   lien url de l'image
  * @param    strUrl    chemin url de l'image
  */
  public function setImage($strTitle, $strLink, $strUrl)
  {
    $strLink = mb_ereg_replace("&", "&amp;", $strLink);
    $this->setChannelElement('image', array('title'=>$strTitle, 'link'=>$strLink, 'url'=>$strUrl));
  }
  
  /**
   * Mémorise l'élément 'about' pour RSS 1.0 uniquement
   * 
   * @param strUrl   valeur du tag about
   */
  public function setChannelAbout($strUrl)
  {
    $this->data['ChannelAbout'] = $strUrl;    
  }
  
  /**
   * Génère puis retourne un UUID
   * 
   * @param      key     clé optionel pour calculer l'uuid
   * @param      prefix  préfixe optionel de l'uuid
   * @return     string
   */
  public function uuid($key = null, $prefix = '') 
  {
    $key = ($key == null)? uniqid(rand()) : $key;
    $chars = md5($key);
    $uuid  = substr($chars,0,8) . '-';
    $uuid .= substr($chars,8,4) . '-';
    $uuid .= substr($chars,12,4) . '-';
    $uuid .= substr($chars,16,4) . '-';
    $uuid .= substr($chars,20,12);
  
    return $prefix . $uuid;
  }
  
  /**
   * Ecrit l'entete xml du flux
   */
  private function printHead()
  {
    $out  = '<?xml version="1.0" encoding="utf-8"?>'.PHP_EOL;
    
    if( $this->version == ALK_RSS2 ) {
      $out .= '<rss version="2.0"'.
        ' xmlns:content="http://purl.org/rss/1.0/modules/content/"'.
        ' xmlns:wfw="http://wellformedweb.org/CommentAPI/"'.
        '>'.PHP_EOL;
    }    
    elseif( $this->version == ALK_RSS1 ) {
      $out .= '<rdf:RDF'. 
        ' xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"'.
        ' xmlns="http://purl.org/rss/1.0/"'.
        ' xmlns:dc="http://purl.org/dc/elements/1.1/"'.
        '>'.PHP_EOL;
    }
    else if( $this->version == ALK_ATOM ) {
      $out .= '<feed xmlns="http://www.w3.org/2005/Atom">'.PHP_EOL;
    }
    echo $out;
  }
  
  /**
   * Ecrit et ferme le tag channel
   */
  private function printTale()
  {
    if( $this->version == ALK_RSS2 ) {
      echo '</channel>'.PHP_EOL.'</rss>'; 
    }    
    elseif( $this->version == ALK_RSS1 ) {
      echo '</rdf:RDF>';
    }
    else if( $this->version == ALK_ATOM ) {
      echo '</feed>';
    }
  }

  /**
   * Création d'un noeud xml
   * Retourne une chaine contenant le tag xml du noeud
   * 
   * @param    tagName     nom du tag
   * @param    tagContent  valeur du tag ou tableau de type 'tagName' => 'tagValue' 
   * @param    attributes   Attributs au format 'attrName' => 'attrValue' format, null par défaut
   * @return   string
   */
  private function makeNode($tagName, $tagContent, $attributes = null)
  {        
    $nodeText = '';
    $attrText = '';

    if( is_array($attributes) ) {
      foreach ($attributes as $key => $value) {
        $attrText .= " $key=\"$value\" ";
      }
    }
    
    if(is_array($tagContent) && $this->version == ALK_RSS1) {
      $attrText = ' rdf:parseType="Resource"';
    }
    
    
    $attrText .= (in_array($tagName, $this->CDATAEncoding) && $this->version == ALK_ATOM)? ' type="html" ' : '';
    $nodeText .= (in_array($tagName, $this->CDATAEncoding))? "<{$tagName}{$attrText}><![CDATA[" : "<{$tagName}{$attrText}>";
     
    if( is_array($tagContent) ) { 
      foreach ($tagContent as $key => $value) {
        $nodeText .= $this->makeNode($key, $value);
      }
    }
    else {
      $nodeText .= (in_array($tagName, $this->CDATAEncoding))? $tagContent : $tagContent; //htmlentities($tagContent);
    }           
      
    $nodeText .= (in_array($tagName, $this->CDATAEncoding))? "]]></$tagName>" : "</$tagName>";

    return $nodeText.PHP_EOL;
  }
  
  /**
   * Ecrit le contenu de la chaine
   */
  private function printChannels()
  {
    // début tag channel
    switch ($this->version) {
    case ALK_RSS2: 
      echo '<channel>' . PHP_EOL;        
      break;
    case ALK_RSS1: 
      echo ( isset($this->data['ChannelAbout']))
             ? "<channel rdf:about=\"{$this->data['ChannelAbout']}\">" 
             : "<channel rdf:about=\"{$this->channels['link']}\">";
      break;
    }
    
    // éléments du channel
    foreach ($this->channels as $key => $value) {
      if( $this->version == ALK_ATOM && $key == 'link' ) {
        // link ATOM
        echo $this->makeNode($key,'',array('href'=>$value));
        // id ATOM
        echo $this->makeNode('id',$this->uuid($value,'urn:uuid:'));
      }
      else {
        echo $this->makeNode($key, $value);
      }    
    }
    
    //RSS 1.0 : tag special <rdf:Seq>  
    if($this->version == ALK_RSS1) {
      echo "<items>".PHP_EOL."<rdf:Seq>".PHP_EOL;
      foreach ($this->items as $item) {
        $thisItems = $item->getElements();
        echo "<rdf:li resource=\"{".$thisItems['link']['content']."}\"/>".PHP_EOL;
      }
      echo "</rdf:Seq>".PHP_EOL."</items>".PHP_EOL."</channel>".PHP_EOL;
    }
  }
  
  /**
   * Ecrit le contenu complet d'un tag item
   */
  private function printItems()
  {    
    foreach ($this->items as $item) {
      $thisItems = $item->getElements();
      
      //the argument is printed as rdf:about attribute of item in rss 1.0 
      echo $this->startItem($thisItems['link']['content']);
      
      foreach ($thisItems as $feedItem ) {
        echo $this->makeNode($feedItem['name'], $feedItem['content'], $feedItem['attributes']); 
      }
      echo $this->endItem();
    }
  }
  
  /**
   * Ecrit et ouvre un tag item
   * 
   * @param about  valeur du param about, pour RSS 1.0 seulement
   */
  private function startItem($about = false)
  {
    if( $this->version == ALK_RSS2 ) {
      echo '<item>'.PHP_EOL; 
    }    
    elseif( $this->version == ALK_RSS1 ) {
      if( $about ) {
        echo "<item rdf:about=\"$about\">".PHP_EOL;
      }
      else {
        die("L'élémént about est requis en RSS 1.0 pour le tag item.");
      }
    }
    else if( $this->version == ALK_ATOM ) {
      echo "<entry>".PHP_EOL;
    }    
  }
  
  /**
   * écrit et ferme le tag item
   */
  private function endItem()
  {
    if( $this->version == ALK_RSS2 || $this->version == ALK_RSS1 ) {
      echo '</item>' . PHP_EOL; 
    }    
    else if( $this->version == ALK_ATOM ) {
      echo "</entry>" . PHP_EOL;
    }
  }
  
}

//require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/magpierss.class.php");


/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkFSyndXml
 * lecteur de flux RSS 1.0, RSS2.0 et ATOM
 * FSyndXml pour Format de Syndication xml
 */
class AlkFSyndXml
{
  
  /**
   * Constructeur
   * 
   * @param  strVersion  version de la file : ALK_RSS1 / ALK_RSS2 / ALK_ATOM       
   */ 
  function __construct()
  { 
  }
  
  /**
   * Parse un flux RSS
   *
   * @param string $url url du flux à lire
   * @return array tableau contenant les items du flux
   *
  function parseUrl($url)
  {
    // utilise l'api de magpierss
    return fetch_rss($url);
  }
  
  function getMagpieRSSFromFlow($flow)
  {
  	return new MagpieRSS($flow);
  }
  
  function getMagpieRSSFromUrlFlow($UrlFlow)
  {
  	return fetch_rss($UrlFlow);
  }*/
  
}

/** Exemples :

Exemple RSS Reader : 

  $fsynd = new AlkFSyndXml();
  $rss = $fsynd->parseUrl("http://www.lemonde.fr/rss/sequence/0,2-3208,1-0,0.xml");
  foreach($rss->items as $item) {
    echo "<div style='border:2px solid grey; background:lightgrey; padding:10px; margin: 10px;'>";
    foreach($item as $meta=>$value) {
      echo "[$meta] ".$value."<br>";
    }
  echo "</div>";
  }


Exemple 1 : RSS 1.0

    $TestFeed = new AlkFSyndXmlWriter(RSS1);
  
  //Setting the channel elements
  //Use wrapper functions for common elements
  //For other optional channel elements, use setChannelElement() function
  $TestFeed->setTitle('Testing the RSS writer class');
  $TestFeed->setLink('http://www.ajaxray.com/rss2/channel/about');
  $TestFeed->setDescription('This is test of creating a RSS 1.0 feed by Universal Feed Writer');
   
  //It's important for RSS 1.0
  $TestFeed->setChannelAbout('http://www.ajaxray.com/rss2/channel/about');
  
  //Adding a feed. Genarally this protion will be in a loop and add all feeds.
  
  //Create an empty FeedItem
  $newItem = $TestFeed->createNewItem();
  
  //Add elements to the feed item
  //Use wrapper functions to add common feed elements
  $newItem->setTitle('The first feed');
  $newItem->setLink('http://www.yahoo.com');
  //The parameter is a timestamp for setDate() function
  $newItem->setDate(time());
  $newItem->setDescription('This is test of adding CDATA Encoded description by the php <b>Universal Feed Writer</b> class');
  //Use core addElement() function for other supported optional elements
  $newItem->addElement('dc:subject', 'Nothing but test');
  
  //Now add the feed item
  $TestFeed->addItem($newItem);
  
  //Adding multiple elements from array
  //Elements which have an attribute cannot be added by this way
  $newItem = $TestFeed->createNewItem();
  $newItem->addElementArray(array('title'=>'The 2nd feed', 'link'=>'http://www.google.com', 'description'=>'This is test of feedwriter class'));
  $TestFeed->addItem($newItem);
  
  //OK. Everything is done. Now genarate the feed.
  $TestFeed->genarate(); 
  
Exemple 2 : RSS 2.0
    
  //Creating an instance of FeedWriter class.
  $TestFeed = new AlkFSyndXmlWriter(RSS2);
  
  //Setting the channel elements
  //Use wrapper functions for common channel elements
  $TestFeed->setTitle('Testing & Checking the RSS writer class');
  $TestFeed->setLink('http://www.ajaxray.com/projects/rss');
  $TestFeed->setDescription('This is test of creating a RSS 2.0 feed Universal Feed Writer');
  
  //Image title and link must match with the 'title' and 'link' channel elements for valid RSS 2.0
  $TestFeed->setImage('Testing the RSS writer class','http://www.ajaxray.com/projects/rss','http://www.rightbrainsolution.com/images/logo.gif');
  
    //Retriving informations from database addin feeds
    $db->query($query);
    $result = $db->result;

    while($row = mysql_fetch_array($result, MYSQL_ASSOC))
    {
        //Create an empty FeedItem
        $newItem = $TestFeed->createNewItem();
        
        //Add elements to the feed item    
        $newItem->setTitle($row['title']);
        $newItem->setLink($row['link']);
        $newItem->setDate($row['create_date']);
        $newItem->setDescription($row['description']);
        
        //Now add the feed item
        $TestFeed->addItem($newItem);
    }
  
  //OK. Everything is done. Now genarate the feed.
  $TestFeed->genarate(); 

Exemple 3 : ATOM

    $TestFeed = new AlkFSyndXmlWriter(ATOM);

    //Setting the channel elements
    //Use wrapper functions for common elements
    $TestFeed->setTitle('Testing the RSS writer class');
    $TestFeed->setLink('http://www.ajaxray.com/rss2/channel/about');
    
    //For other channel elements, use setChannelElement() function
    $TestFeed->setChannelElement('updated', date(DATE_ATOM , time()));
    $TestFeed->setChannelElement('author', array('name'=>'Anis uddin Ahmad'));

    //Adding a feed. Genarally this protion will be in a loop and add all feeds.

    //Create an empty FeedItem
    $newItem = $TestFeed->createNewItem();
    
    //Add elements to the feed item
    //Use wrapper functions to add common feed elements
    $newItem->setTitle('The first feed');
    $newItem->setLink('http://www.yahoo.com');
    $newItem->setDate(time());
    //Internally changed to "summary" tag for ATOM feed
    $newItem->setDescription('This is test of adding CDATA Encoded description by the php <b>Universal Feed Writer</b> class');

    //Now add the feed item    
    $TestFeed->addItem($newItem);
    
    //OK. Everything is done. Now genarate the feed.
    $TestFeed->genarate();  
 
 */ 
 
?>