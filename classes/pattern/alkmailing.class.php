<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

/**
 * @package Alkanet_Class_Pattern
 * @class AlkMailing
 * 
 * Classe de mailing & publipostage
 * 
 * Exemple d'utilisation : 
 * 
 *   $oMail = new AlkMail();
 *   $oMail->SetFrom(ALK_APP_TITLE, ALK_MAIL_ADMIN_MAIL);
 *   $oMail->AddTo("Prenom Nom", "email@dot.com");
 *   
 *   // TEST_MAILING est une valeur de SIT_MAILING.MAIL_CLE correspondant 
 *   // au type de mail à envoyer
 *   AlkMailing::SendMailType($oAppli->oQuery->getProperty("dbConn"), $oMail, "TEST_MAILING", 
 *       array("date"      => date("d/m/Y") . " à " . date("H:i"),
 *             "dest"      => trim("M Prenom Nom"),
 *             "url_site"  => ALK_ROOT_URL, 
 *             "agent_login" => "prenom", "agent_pwd" => "bidon"));
 * 
 *   // ou pour un envoi à plusieurs destinataires
 *   AlkMailing::SendMailTypeByQuery($this->oQuery->getProperty("dbConn"), "select * from SIT_AGENT", "TEST_MAILING", 
 *       array("agent_login" => "vlegloahec", "agent_pwd" => "bidon"));
 * 
 */
class AlkMailing extends AlkObject 
{

  /**
   * Constructeur vide
   */
  public function __construct() { }

  /**
   * Envoi un mail à un agent
   * Retourne true si succès, false sinon
   * Reconstruit l'objet $oAlkMail avec le message de type $strType en y 
   * ajoutant les éléments publipostés de $tabAssoc
   * Le tableau associatif $tabAssocMsg sera automatiquement fusionné avec un tableau par défaut, 
   * comprenant les clés date, dest, url_site et apptitle
   * 
   * @static 
   * @param oAlkMail     objet d'envoi de mail paramétré sauf objet et message
   * @param strType      type de message à envoyer
   * @param tabAssoc     tableau associatif des éléments publipostés
   * @return boolean
   */
  public static function SendMailType($oAlkMail, $strType, $tabAssoc=Array())
  {
    $dbConn = AlkFactory::getDbConn();

    $oAlkMail->setProperty("charSet", strtolower(ALK_HTML_ENCODING));

    $strNomFrom = $oAlkMail->getProperty("fromName");

    $strFirstMailTo = "";
    $strFirstNomTo = "";
    foreach($oAlkMail->getProperty("tabTo") as $key=>$value) {
      $strFirstMailTo = $key;
      $strFirstNomTo = $value;
      break;
    }
    
    $strUrlSite = ( defined("ALK_ROOT_URL_FRONTOFFICE") 
                              ? ALK_ROOT_URL_FRONTOFFICE.ALK_ROOT_DIR
                              : ALK_ALKANET_ROOT_URL );
    $tabAssoc["apptitle"] = ALK_APP_TITLE;
    $tabAssoc["dest"]     = $strFirstNomTo;
    $tabAssoc["exp"]      = $strNomFrom;
    $tabAssoc["date"]     = date("d/m/Y", AlkFactory::getLocalDate()) . " à " . date("H:i", AlkFactory::getLocalDate());
    $tabAssoc["url_site"] = $strUrlSite;
    
    if( !isset($tabAssoc["msgUrlSite"]) ) {
      $tabAssoc["msgUrlSite"] = _f("%s est accessible à l'adresse :", ALK_APP_TITLE)." <a href=\"".$strUrlSite."\">".$strUrlSite."</a><br/>";
    }
    $tabAssoc["msgSign"] = "<br/>"._f("Cordialement%s%s", "<br/>", $strNomFrom)."<br/>";
    $tabAssoc["msgPS"] = "<i>"._f("ps: Ce message est généré puis envoyé automatiquement par %s.", ALK_APP_TITLE)."</i>"; 

    // utilisation de memcache pour optimiser les boucles sur le même type
    $strSql = "select * from SIT_MAILING where MAIL_CLE='".$strType."'";
    $dsMail = $dbConn->initDataSet($strSql, 0, -1, true, 1, "MAILING");
    if($drMail = $dsMail->getRowIter()) {
      $strCle       = $drMail->getValueName("MAIL_CLE");
      $strSujet     = $drMail->getValueName("MAIL_SUJET");
      $strMsgHtml   = $drMail->getValueName("MAIL_MSG");
      $strFileName  = $drMail->getValueName("MAIL_PJ");
      $strMsgTxt = ""; // initialise le message texte
    
      foreach($tabAssoc as $strKey => $strValue) {
        $strSujet   = mb_ereg_replace("\{\\$".$strKey."\}", $strValue, $strSujet);
        $strMsgHtml = mb_ereg_replace("\{\\$".$strKey."\}", $strValue, $strMsgHtml);
      }
      
      $oCleaner = AlkFactory::getHtmlCleaner();
      $oCleaner->cleanWithTidy($strMsgHtml);
      
      // generation de strMsgTxt via XSL avec style/mailing.xsl
      $oXml = AlkFactory::getXml();
      $oXml->setXml($strMsgHtml);
      $oXml->setFileNameXsl(ALK_ALKANET_ROOT_PATH."styles/mailing.xsl");
      $bRes = $oXml->setTransformation(true);
      
      if( $bRes ) {
        $strMsgTxt = $oXml->getHtml();
      } else { 
        trigger_error(__CLASS__.", l.".__LINE__." : ".$oXml->getLastError());
      }
      // reconstruction de l'object AlkMail
      $oAlkMail->SetSubject($strSujet);
      $oAlkMail->SetHtml(true);
      
      $oAlkMail->SetBody($strMsgHtml);
      $oAlkMail->SetAltBody($strMsgTxt);
      
      if( $strFileName != "" ) {
        $strPathFileName = ALK_ALKANET_ROOT_PATH.ALK_ROOT_UPLOAD."mailing/".$strFileName;
        if( file_exists($strPathFileName) && is_file($strPathFileName) ) {
          $oAlkMail->AddFile($strPathFileName, $strFileName);
        }
      }
      
      //$user_id = AlkFactory::getSProperty("user_id", "-1"); if( $user_id==1 ) { echo "MAIL : "; print_r($oAlkMail); }
      $iRes = $oAlkMail->Send();
      return ($iRes==1); 
    }
    
    return false;
  }
  
  /**
   * @todo : à supprimer car non utilisé
   * Retourne dans un tableau nommé, le contenu du message envoyé
   * Le tableau retourné est de type ["subject" => "", "msgTxt" => , "msgHtml" => "", "file" => ""]
   * @param strType      clé du message envoyé
   * @param tabAssoc     tableau associatif des éléments publipostés
   * @return array
   *
  public static function getTabMailContent($strType, $tabAssoc=array())
  {
    $dbConn = AlkFactory::getDbConn();
    $tabRes = array();
    
    $tabAssoc["apptitle"] = ALK_APP_TITLE;
    $tabAssoc["dest"]     = "Destinataire";
    $tabAssoc["exp"]      = "Expéditeur";
    $tabAssoc["date"]     = date("d/m/Y", AlkFactory::getLocalDate()) . " à " . date("H:i", AlkFactory::getLocalDate());
    $tabAssoc["url_site"] = ALK_ROOT_URL;
   
    $strSql = "select * from SIT_MAILING where MAIL_CLE='".$strType."'";
    $dsMail = $dbConn->initDataSet($strSql);
    if($drMail = $dsMail->getRowIter()) {
      $strSujet     = $drMail->getValueName("MAIL_SUJET");
      $strMsgHtml   = $drMail->getValueName("MAIL_MSG");
      $strFileName  = $drMail->getValueName("MAIL_PJ");

      foreach($tabAssoc as $strKey => $strValue) {
        $strSujet   = mb_ereg_replace("\{\\$".$strKey."\}", $strValue, $strSujet);
        $strMsgHtml = mb_ereg_replace("\{\\$".$strKey."\}", $strValue, $strMsgHtml);
      }

      $oXml = AlkFactory::getXml();
      $oXml->setXml($strMsgHtml);
      $oXml->setFileNameXsl(ALK_ALKANET_ROOT_PATH."styles/mailing.xsl");
      $bRes = $oXml->setTransformation(true);
      
      $strMsgTxt = "";
      if( $bRes ) {
        $strMsgTxt = $oXml->getHtml();
      }
      $tabRes = array("subject"  => $strSujet,
                      "msgTxt"  => $strMsgTxt,
                      "msgHtml" => $strMsgHtml,
                      "file"    => $strFileName);
    }
    return $tabRes; 
  } */
  
  /**
   * @todo : à supprimer car non utilisé
   * Mailing à un groupe d'agents donné par requete sql ou dataSet
   * Le nom et mail de l'expéditeur est à renseigner dans tabAssoc avec les clés expMail et expName
   * Sinon l'expéditeur est donné par les constantes ALK_MAIL_ADMIN_NAME et ALK_MAIL_ADMIN_MAIL
   * Le tableau associatif tabAssoc sera automatiquement fusionné avec un tableau par défaut, 
   * comprenant les clés date, dest, url_site et apptitle 
   * 
   * @static 
   * @param oDs          Référence sur dataSet (ou requete sql de type texte) donnant la liste des agents
   * @param strType      type de message à envoyer (correspond à un SIT_MAILING.MAIL_CLE)
   * @param tabAssoc     tableau associatif des éléments publipostés
   *
  public static function SendMailTypeByQuery($oDsDest, $strType, $tabAssoc=array())
  {
    $dbConn = AlkFactory::getDbConn();

    if( is_string($oDsDest) ) {
      $strSql = $oDsDest;
      $oDsDest = $dbConn->initDataSet($strSql);
    }
    while( $drDest = $oDsDest->GetRowIter() ) {
  		$agent_civilite   = trim($drDest->GetValueName("AGENT_CIVILITE"));
  		$agent_prenom     = trim($drDest->GetValueName("AGENT_NOM"));
  		$agent_nom        = trim($drDest->GetValueName("AGENT_PRENOM"));
  		$agent_email      = trim($drDest->GetValueName("AGENT_MAIL"));
  		
  		$oMail = AlkFactory::getMail();
      $strFromName = ALK_MAIL_ADMIN_MAIL;
      $strFromMail = ALK_MAIL_ADMIN_NAME;
  		if( array_key_exists("expName", $tabAssoc) && array_key_exists("expMail", $tabAssoc) ) {
        $strFromName = $tabAssoc["expName"];
        $strFromMail = $tabAssoc["expMail"];
      }
      $oMail->setFrom($strFromName, $strFromMail);
  		$oMail->addTo($agent_prenom." ".$agent_nom, $agent_email);
  		
  		$tabAssoc["date"]     = date("d/m/Y", AlkFactory::getLocalDate()) . " à " . date("H:i", AlkFactory::getLocalDate());
			$tabAssoc["dest"]     = trim($agent_civilite." ".$agent_prenom." ".$agent_nom);
      $tabAssoc["exp"]      = "";
			$tabAssoc["url_site"] = ALK_ROOT_URL;
      $tabAssoc["apptitle"] = ALK_APP_TITLE;

  		// envoi des mails
  		AlkMailing::SendMailType($oMail, $strType, $tabAssoc);
  	}
  }*/

}

?>