<?php


/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Module::Alkanet
Module Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

/**
 * Moteur de recherche et d'indexation SOLR
 */
class AlkSearcherSolr {

  // Classe Query appelante
  protected $oQuery;

  // Connection à la base
  protected $dbConn;
  
  // Tableau des extensions reconnues par Solr
  protected $tabExtAuth;
  
  /**
   * Constructeur
   * @param oQuery    Classe Query appelante
   * @param dbConn    Connection à la base
   */
  public function __construct($oQuery, $dbConn) 
  {
    $this->oQuery = $oQuery;
    $this->dbConn = $dbConn;
    
    $this->tabExtAuth = array("pdf", "doc", "ppt", "xls");
  }
  
  /**
   * Indexe les pièces jointes ajoutées
   * Après insertion en base de données
   * 
   * @param tableName       Nom de la table où la pièce jointe doit être enregistrée
   * @param data_id         Identifiant de la donnée traitée
   * @param champName       Nom du champ de formaulaire contenant la valeur à uploader
   * @param agent_id        Agent effectuant l'ajout
   * @param strPath         Chemin racine des répertoires
   * @param strDirUpload    Chemin d'accès au répertoire upload
   * @param strFileName     le chemin et nom du fichier à indexer
   * @param strOldFileName  le chemin et nom de la version précédente du fichier (ou lui-meme si premier ajout) à indexer
   * @param lg_id           Langue utilisée
   * @param bUploadCtrl     Indique que l'on vient d'un controle de type upload et non d'un controle de type file
   * @param bVersionning    (default false) True si on gère le versionning des fichiers dans upload
   * @return bool
   */
  public function AddPj($pj_id, $tableName, $data_id, $champName, $agent_id, $strPath, 
                        $strDirUpload, $strFileName, $strOldFileName, $lg_id, $bUploadCtrl = true, $bVersionning = false) 
  {
    $strExt = getFileExtension($strFileName);
    if( in_array($strExt, $this->tabExtAuth) ) {
      $strSql = "select * from IEDIT_PJ " .
        " where PJ_ID=".$pj_id;
      $oDsDataSql = $this->dbConn->initDataset($strSql, 0, -1, false);
      if( $oDrDataSql = $oDsDataSql->GetRowIter() ) {
        $pj_name      = $oDrDataSql->GetValueName("PJ_NAME");
        $pj_title     = $oDrDataSql->GetValueName("PJ_TITLE");
        $pj_desc      = $oDrDataSql->GetValueName("PJ_DESC");
        $pj_keywords  = $oDrDataSql->GetValueName("PJ_KEYWORDS");
        $pj_auteur    = $oDrDataSql->GetValueName("PJ_AUTEUR");
        $pj_copyright = $oDrDataSql->GetValueName("PJ_COPYRIGHT");

        /**
         * ATTENTION : la liste des champs que l'on ajoute doit être déclarée 
         * dans le fichier de conf du core solr ./conf/schema.xml (cf pct ou sqn)
         */ 
        $strUrlSolr = "http://".ALK_SOLR_USER.":".ALK_SOLR_PASSWORD."@".ALK_SOLR_SERVER.":".ALK_SOLR_PORT.
          ALK_SOLR_PATH."/update/rich?stream.type=".$strExt.
          "&stream.file=".$strPath.$strDirUpload.$strFileName.
          "&stream.fieldname=PJ_CONTENTS" .
          "&commit=true" .
          "&id=-".$pj_id.
          "&fieldnames=PJ_CONTENTS,PJ_ID,PJ_NAME,PJ_TITLE,PJ_DESC,PJ_KEYWORDS,PJ_AUTEUR,PJ_COPYRIGHT,DATA_DATEMAJ" .
          "&PJ_ID=".$pj_id.
          "&PJ_NAME=".$pj_name.
          "&PJ_TITLE=".$pj_title.
          "&PJ_DESC=".$pj_desc.
          "&PJ_KEYWORDS=".$pj_keywords.
          "&PJ_AUTEUR=".$pj_auteur.
          "&PJ_COPYRIGHT=".$pj_copyright.
          "&DATA_DATEMAJ=NOW";
        
        $bRes = @file_get_contents($strUrlSolr);
        
        if ( !(is_bool($bRes) && $bRes==false) ) {
          $this->mergeDataToPj($data_id, $pj_id);
        }
        
        return ( is_bool($bRes) && $bRes==false ? false : true );
      }
    }
    return false;
  }
  
  /**
   * Réindexe une pièce-jointe
   * Après insertion en base de données
   * 
   * @param pj_id           Identifiant de la piece jointe à réindexer
   * @param data_id         Identifiant de la donnée traitée
   * 
   */
  public function updatePj($pj_id, $data_id)
  {
    $oSolr = AlkFactory::getSolrClient();
    $query = new SolrQuery();
    
    $query->setQuery("PJ_ID:".$pj_id);
    $qResponse = $oSolr->query($query);
    $qResponse->setParseMode(SolrQueryResponse::PARSE_SOLR_DOC);
    $response = $qResponse->getResponse();
    $doc_pj = $response["response"]["docs"][0];
    
    if ( !is_null($doc_pj) ) {
      $doc_pj = $doc_pj->getInputDocument();
      $strSql = "select * from IEDIT_PJ " .
        " where PJ_ID=".$pj_id;
      $oDsDataSql = $this->dbConn->initDataset($strSql, 0, -1, false);
      if( $oDrDataSql = $oDsDataSql->GetRowIter() ) {
        $doc_pj->deleteField("PJ_NAME");$doc_pj->addField("PJ_NAME", $oDrDataSql->GetValueName("PJ_NAME"));
        $doc_pj->deleteField("PJ_TITLE");$doc_pj->addField("PJ_TITLE", $oDrDataSql->GetValueName("PJ_TITLE"));
        $doc_pj->deleteField("PJ_DESC");$doc_pj->addField("PJ_DESC", $oDrDataSql->GetValueName("PJ_DESC"));
        $doc_pj->deleteField("PJ_KEYWORDS");$doc_pj->addField("PJ_KEYWORDS", $oDrDataSql->GetValueName("PJ_KEYWORDS"));
        $doc_pj->deleteField("PJ_AUTEUR");$doc_pj->addField("PJ_AUTEUR", $oDrDataSql->GetValueName("PJ_AUTEUR"));
        $doc_pj->deleteField("PJ_COPYRIGHT");$doc_pj->addField("PJ_COPYRIGHT", $oDrDataSql->GetValueName("PJ_COPYRIGHT"));
        
        // met à jour le doucment Solr associé à la pièce jointe
        $oSolr->addDocument($doc_pj);
        $oSolr->commit();
      }
      $this->mergeDataToPj($data_id, $pj_id);
    }
  }
  
  /**
   * Supprime une ou des pièces jointes à une information donnée
   * 
   * @param pj_id         Identifiant de la piece jointe à supprimer
   * @param strFileName   Nom du fichier à supprimer
   * @param tableName     Nom de la table où la pièce jointe doit être enregistrée
   * @param strPath       Chemin racine des répertoires
   * @param strDirUpload  Chemin d'accès au répertoire upload
   * @param bVersionning  (default false) True si on gère le versionning des fichiers dans upload
   */
  public function DelPj($pj_id, $strFileName, $tableName, $strPath, $strDirUpload, $bVersionning = false) 
  {
    //suppression de l'index
    $oSolr = AlkFactory :: getSolrClient();
    if (!is_null($oSolr)) {
      $oSolr->deleteById("-".$pj_id);
      $oSolr->commit();
    }
  }

  /**
   * Supprime une ou des pièces jointes à une information donnée
   * 
   * @param pj_id         Identifiant de la piece jointe à supprimer
   * @param strFileName   Nom du fichier à supprimer
   * @param tableName     Nom de la table où la pièce jointe doit être enregistrée
   * @param strPath       Chemin racine des répertoires
   * @param strDirUpload  Chemin d'accès au répertoire upload
   * @param bVersionning  (default false) True si on gère le versionning des fichiers dans upload
   */
  public function DelPjByDataId($pj_id, $strFileName, $tableName, $data_id, $strPath, $strDirUpload, $strField = "") 
  {
    //suppression de l'index
    $oSolr = AlkFactory :: getSolrClient();
    if (!is_null($oSolr)) {
      $oSolr->deleteById("-".$pj_id);
      $oSolr->commit();
    }
  }

  /**
     * Indexe ou Désindexe la ou les données identifiées répondant au(x) type(s) de données
     * retourne true si l'information a été indexée, retourne false sinon 
     * @param dsDatatype      Types de données à indexer
     * @param one_data_id     Identifiant d'une donnée à indexer
     * @param bDelete         Désindexation de la donnée si true
     * @param bEcho           Affichage des logs si true
     * @param language        Langue utilisée (fr, en)
     * @return boolean
     */
  public function indexDataByDatatype($dsDatatype, $one_data_id = "-1", $bDelete = false, $bEcho = false, $language = "fr") 
  {
    $bIndex = false;
    $oSolr = AlkFactory::getSolrClient();

    $querySearch = AlkFactory :: getQuery(ALK_ATYPE_ID_SEARCH);
    $querySearchAction = AlkFactory :: getQueryAction(ALK_ATYPE_ID_SEARCH);
    while( $drDatatype = $dsDatatype->getRowIter() ) {
      $datatype_intitule = $drDatatype->getValueName("DATATYPE_INTITULE");
      $table_name        = $drDatatype->getValueName("TABLE_REF");
      $table_alias       = $drDatatype->getValueName("TABLE_ALIAS");
      $primary_key       = $drDatatype->getValueName("FIELD_PK");
      $strWhere          = $drDatatype->getValueName("SELECTING_WHERE");
      
      $tabFieldsText     = explode("|", $drDatatype->getValueName("FIELDS_TEXT"));
      $tabAppliId = explode("::", str_replace("__ALIAS__", $table_alias, str_replace("__TABLE__", $table_name, $drDatatype->getValueName("SELECT_APPLI_ID"))));
      $tabAtypeId = explode("::", str_replace("__ALIAS__", $table_alias, str_replace("__TABLE__", $table_name, $drDatatype->getValueName("SELECT_ATYPE_ID"))));

      $fieldAppliId = "APPLI_ID";
      $tableAppliId = "";
      if( empty ($tabAppliId) )
        $fieldAppliId = "-1";
      if( count($tabAppliId) >= 1 && $tabAppliId[0] != "" )
        $fieldAppliId = $tabAppliId[0];
      if( count($tabAppliId) >= 2 )
        $tableAppliId = $tabAppliId[1];

      $fieldAtypeId = "ATYPE_ID";
      $tableAtypeId = "";
      if( empty ($tabAtypeId) )
        $fieldAtypeId = "-1";
      if( count($tabAtypeId) >= 1 && $tabAtypeId[0] != "" )
        $fieldAtypeId = $tabAtypeId[0];
      if( count($tabAtypeId) >= 2 )
        $tableAtypeId = $tabAtypeId[1];
      $strFieldsText = "";

      if( $one_data_id != "-1" )
        $strWhere .= ($strWhere != "" ? " and " : "") . $primary_key . "=" . $one_data_id;
        
      $strSql = "select ".$primary_key." as DATA_ID, DATATYPE_ID, ".
        $fieldAppliId." as APPLI_ID, ".$fieldAtypeId." as ATYPE_ID, ".
        "DATA_DATEMAJ, DATA_DATEPARUTION,".
        implode(",", $tabFieldsText).
        " from ".$table_name." ".$table_alias.
        " ".$tableAppliId.
        " ".$tableAtypeId .
       ( $strWhere != "" 
         ? " where " . $strWhere 
         : "");

      $dsData = $this->dbConn->initDataset($strSql, 0, -1, false);
      if( !$dsData->isEndOfFile() ) { 
        while( $DrData = $dsData->GetRowIter() ) {
          $data_id     = $DrData->GetValueName("DATA_ID");
          $appli_id    = $DrData->GetValueName("APPLI_ID");
          $datatype_id = $DrData->GetValueName("DATATYPE_ID");
          $data_auteur = $DrData->GetValueName("DATA_AUTEUR");
          
          $oSolr->deleteById($data_id);
          $oSolr->commit();
          
          if ( !$bDelete ) {
            /**
             * ATTENTION : la liste des champs que l'on ajoute doit être déclarée 
             * dans le fichier de conf du core solr ./conf/schema.xml (cf pct ou sqn)
             */
            $doc = new SolrInputDocument(); 
            $doc->addField("id", $data_id);
            $doc->addField("DATA_ID", $data_id);
            $doc->addField("APPLI_ID", $appli_id);
            $doc->addField("DATATYPE_ID", $datatype_id);
                    
            $strSqlClassif = "select CLASSIF_ID" .
              " from IEDIT_DATA_CLASSIFICATION" .
              " where DATA_ID=".$data_id;
            $dsClassifSql = $this->dbConn->initDataset($strSqlClassif, 0, -1, false);
            while( $DrClassifSql = $dsClassifSql->GetRowIter() ) {
              $classif_id = $DrClassifSql->GetValueName("CLASSIF_ID");
              $doc->addField("CLASSIF_ID", $classif_id);
            }       
            
            $doc->addField("DATA_AUTEUR", $data_auteur);
            
            $strFieldSearch = "";            
            foreach($tabFieldsText as $strField) {
              if( $DrData->GetValueName("$strField") != "" ) {
                $strValue = $DrData->GetValueName($strField);
                //$doc->addField($strField, $strValue); // pas nécessaire dans le cas de MAIA car pas de recherche avancée sur ces champs
                $strFieldSearch.= " ".$strValue;
              }
            }
            $doc->addField("name", $strFieldSearch);
            
            $tabDate = explode(" ", $DrData->GetValueName("DATA_DATEMAJ"));
            $strDate = $tabDate[0]."T".$tabDate[1]."Z";
            $doc->addField("DATA_DATEMAJ", $strDate);
            
            $data_dateparution = $DrData->GetValueName("DATA_DATEPARUTION");
            if ( $data_dateparution != "" ) {
              $tabDate = explode(" ", $data_dateparution);
              $strDate = $tabDate[0]."T".$tabDate[1]."Z";
              $doc->addField("DATA_DATEPARUTION", $strDate);
            }
            
            $oSolr->addDocument($doc);
            $oSolr->commit();
            
            $this->mergeDataToPjs($data_id);
          }
        }

        $bIndex = true;
      }     
    }
    return $bIndex;
  }
  
  /**
   * recopie les informations du document Solr lié à la donnée vers les documents Solr liés aux pièces jointes
   * vérifie que les pièces jointes sont reconnues par Solr avant de recopier les informations
   * @param data_id   integer indentifiant de la donnée
   */
  protected function mergeDataToPjs($data_id)
  {
    $strSqlPjData = "select PJ_ID, PJ_NAME from IEDIT_PJ where DATA_ID=".$data_id;
    $oDsPjData = $this->dbConn->initDataset($strSqlPjData, 0, -1, false);
    while ( $oDrPjData = $oDsPjData->GetRowIter() ) {
      $pj_id = $oDrPjData->GetValueName("PJ_ID");
      $pj_name = $oDrPjData->GetValueName("PJ_NAME");
      $strExt = getFileExtension($pj_name);
      if( in_array($strExt, $this->tabExtAuth) ) {
        $this->mergeDataToPj($data_id, $pj_id);
      }
    }
    unset($oDrPjData);
    unset($oDsPjData);
  }
  
  /**
   * recopie les informations du document Solr lié à la donnée vers le document Solr lié à la pièce jointe
   * @param data_id   integer identifiant de la donnée
   * @param pj_id     integer identifiant de la pièce jointe
   */
  protected function mergeDataToPj($data_id, $pj_id)
  {
    $oSolr = AlkFactory::getSolrClient();
    $query = new SolrQuery();
    
    // récupère le document Solr associé à la donnée
    $query->setQuery("id:".$data_id);
    $qResponse = $oSolr->query($query);
    $qResponse->setParseMode(SolrQueryResponse::PARSE_SOLR_DOC);
    $response = $qResponse->getResponse();
    $doc_data = $response["response"]["docs"][0];
    if ( !is_null($doc_data) ) {
      $doc_data = $doc_data->getInputDocument();
    }
    
    // récupère le document Solr associé à la pièce-jointe
    $query->setQuery("PJ_ID:".$pj_id);
    $qResponse = $oSolr->query($query);
    $qResponse->setParseMode(SolrQueryResponse::PARSE_SOLR_DOC);
    $response = $qResponse->getResponse();
    $doc_pj = $response["response"]["docs"][0];
    if ( !is_null($doc_pj) ) {
      $doc_pj = $doc_pj->getInputDocument();
    }
    
    // met à jour les informations du document Solr associé à la donnée vers le document Solr associé à la pièce-jointe
    if ( !is_null($doc_pj) && !is_null($doc_data) ) {
      $doc_pj->deleteField("CLASSIF_ID");
      $doc_pj->deleteField("DATA_AUTEUR");
      $doc_pj->deleteField("DATA_DATEPARUTION");
      $doc_pj->merge($doc_data, false);
      
      // met à jour le champ de recherche "name"
      $strFieldSearch = $doc_data->getField("name")->values[0].
        ( $doc_pj->getField("PJ_NAME") && isset($doc_pj->getField("PJ_NAME")->values[0]) ? " ".$doc_pj->getField("PJ_NAME")->values[0] : "" ).
        ( $doc_pj->getField("PJ_TITLE") && isset($doc_pj->getField("PJ_TITLE")->values[0]) ? " ".$doc_pj->getField("PJ_TITLE")->values[0] : "" ).
        ( $doc_pj->getField("PJ_DESC") && isset($doc_pj->getField("PJ_DESC")->values[0]) ? " ".$doc_pj->getField("PJ_DESC")->values[0] : "" ).
        ( $doc_pj->getField("PJ_KEYWORDS") && isset($doc_pj->getField("PJ_KEYWORDS")->values[0]) ? " ".$doc_pj->getField("PJ_KEYWORDS")->values[0] : "" ).
        ( $doc_pj->getField("PJ_AUTEUR") && isset($doc_pj->getField("PJ_AUTEUR")->values[0]) ? " ".$doc_pj->getField("PJ_AUTEUR")->values[0] : "" ).
        ( $doc_pj->getField("PJ_COPYRIGHT") && isset($doc_pj->getField("PJ_COPYRIGHT")->values[0]) ? " ".$doc_pj->getField("PJ_COPYRIGHT")->values[0] : "" ).
        ( $doc_pj->getField("PJ_CONTENTS") && isset($doc_pj->getField("PJ_CONTENTS")->values[0]) ? " ".$doc_pj->getField("PJ_CONTENTS")->values[0] : "" );
      
      $doc_pj->deleteField("name");
      $doc_pj->addField("name", $strFieldSearch);
      
      $oSolr->addDocument($doc_pj);
      $oSolr->commit();
      
      unset($strFieldSearch);
    }
    
    unset($doc_data);
    unset($doc_pj);
    unset($query);
    unset($response);
    unset($qResponse);
  }
}
?>