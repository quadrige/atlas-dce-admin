<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

/**
 * @package Alkanet_Class_Pattern
 * @class AlkImport
 * 
 * Classe de base pour l' import.
 */
class AlkImport extends AlkObject
{
  /**
   * Notation décimale du caractère ascii séparateur de champ du fichier d'import
   * utilisé uniquement si le fichier d'import CSV est généré à partir du fichier fourni
   */
  protected $intSeparateur;
  
  /** Emplacement  du fichier d'import. */
	protected $strFichier_Import;
	
	/** Nom de l'objet  ImportErreur. */
	protected $obj_Erreur;
	
	/** Nom de l'objet  ImportErreur. */
	protected $obj_Warning;
	
	/** 
   *  Tableau contenant les  propriétés des colonnes.
   *
   * les elements du tableau sont des tableaux a quatre cellules 
   * cellule 'name' correspand au nom 
   * cellule 'oglig' correspand au type de la col (1:obligatoire , 0:non)
   * cellule 'type ' correspand au type de chamaps
   * cellule 'lg' correspand à la taille max  du champs
   * cellule  'index' correspand à l'indice de la col
   * cellule  'mode' correspand au mode 0:si numeric ou string , 1:si tableau simple ou liste, 2:si tableaux de correspandance
   */
	protected $tabAlkImportCol;
	
	/** query de consultation sur l'appli */
	protected $oQuery;
	
	/** query action de l'appli */
	protected $oQueryAction;
	
	/** reference vers l'objet workspace */
	protected $oSpace;
	
	
	/**
	 * Constructeur de la classe Import : initialisation des attributs de Import.
	 *
	 * @param unknown_type $strFichier_Import  Emplacement  du fichier d'import
	 * @param unknown_type $tabAlkImportCol    Tableau contenant les proprietés des  colonnes
	 * @param unknown_type $tabSup             Tableau contenant les colonnes obligatoires pour la suppression
	 * @param unknown_type $tabAjout           Tableau contenant les colonnes obligatoires pour l'ajouts
	 * @param unknown_type $tabModif
	 * @param unknown_type $obj_Erreur
	 * @param unknown_type $objWarning
	 * @param unknown_type $oQuery
	 * @param unknown_type $oQueryAction
	 * @param unknown_type $oSpace
	 */
	public function __construct($strFichier_Import, $tabAlkImportCol, $tabSup, $tabAjout, $tabModif, 
                      &$obj_Erreur, &$objWarning, &$oQuery, &$oQueryAction, &$oSpace)
	{
	  parent::__construct();
	  
	  $this->AlkImport($strFichier_Import, $tabAlkImportCol, $tabSup, $tabAjout, $tabModif, 
                     $obj_Erreur, $objWarning, $oQuery, $oQueryAction, $oSpace);
	}
	
	/**
   * Constructeur de la classe Import : initialisation des attributs de Import.
   *
   * @param strFichier_Import Emplacement  du fichier d'import
   * @param tabImport  Tableau contenant les proprietés des  colonnes
   * @param tabSup  Tableau contenant les colonnes obligatoires pour la suppression
   * @param tabAjoutt  Tableau contenant les colonnes obligatoires pour l'ajouts
   * @param objErreur  Nom de l'objet  ImportErreur
   */
	public function AlkImport( $strFichier_Import,  $tabAlkImportCol, $tabSup, $tabAjout, $tabModif, 
                      &$obj_Erreur,&$objWarning, &$oQuery, &$oQueryAction, &$oSpace )
	{
    $this->intSeparateur = null;
    $this->strFichier_Import = $this->transform2csv($strFichier_Import);
		$this->tabAlkImportCol = $tabAlkImportCol;
		$this->obj_Erreur = &$obj_Erreur;
		$this->obj_Warning = &$objWarning;
		$this->tabSup = $tabSup;
		$this->tabAjout = $tabAjout;
		$this->tabModif = $tabModif;
		$this->oQueryAction = &$oQueryAction;
		$this->oQuery = &$oQuery;
		$this->oSpace = &$oSpace;
	}
	
  /**
   * Fonction qui essaye de créer un fichier CSV à partir du fichier d'entrée
   * Si la création est un succès, initialise la variable globale intSeparateur
   * qui est obligatoirement utilisée dans les fonctions parseFile/parseLines/integrate à la place du séparateur passé en paramètre.
   * Le fichier CSV généré se trouve au même endroit que le fichier d'entrée et possède l'extension .csv (exemple.xls => exemple.xls.csv)
   * 
   * séparateur de champ : {Tab}
   * séparateur de texte : Aucun
   * 
   * @param strFichier_Import   Emplacement du fichier d'import
   * @return Emplacement du fichier d'import au format CSV si opération réussi
   *         Emplacement du fichier d'import d'entrée si opération échouée
   */
  protected function transform2csv($strFichier_Import)
  {
    $intSeparateur = 9;
    $strFichier_Import_new = $strFichier_Import;
    $strFichier_Import_csv = $strFichier_Import.".csv";
    
    supprFichier($strFichier_Import_csv);
    
    $strFichier_typeMime = GetTypeMime($strFichier_Import);
    switch ( $strFichier_typeMime ) {
      case "application/vnd.ms-excel" :   // xls
        // la commande "sed" permet de supprimer le dernier caractère en trop ajouté par xls2csv
        exec(ALK_CMD_XLS2CSV." -c \\".chr($intSeparateur)." -q 0 $strFichier_Import > $strFichier_Import_csv | sed -i '\$ s/.\$//' $strFichier_Import_csv");
        if ( file_exists($strFichier_Import_csv) && is_file($strFichier_Import_csv) && filesize($strFichier_Import_csv) > 0 ) {
          $strFichier_Import_new = $strFichier_Import_csv;
        } else {
          supprFichier($strFichier_Import_csv);
        }
      break;
    }
    
    // initialise le caractère séparteur de champ en cas de succès
    if ( strcmp($strFichier_Import, $strFichier_Import_new) != 0 ) {
      $this->intSeparateur = $intSeparateur;
    }
    
    return $strFichier_Import_new;
  }
  
	/**
   * Fonction indiqant si le fichier d'import est correct au niveau de sa structure.
   *    
   * @param intCarSeparateur Notation decimale du caractére ascii separateur de champ du fichier d'import
   * @return 0 si correcte .
   */
	public function parseFile($intCarSeparateur)
	{
		
		$intTest=0;
    if ( is_null($this->intSeparateur) ) {
		  $intSeparateur = $intCarSeparateur; // Separateur de champ pour fichier CSV fourni
    } else {
      $intSeparateur = $this->intSeparateur;  // Separateur de champ pour fichier CSV généré
    }
		if(@file_exists($this->strFichier_Import))	{
			$fImport = @fopen($this->strFichier_Import, "r");
		}	else {
			$fImport = false;
			$this->obj_Erreur->erreur(1);
			$intTest++;
		}
		
		$strChaineSyntaxe = "";
		if( $fImport ){
			
			// lecture de la premiere ligne
			$intTailleFichier = @filesize($this->strFichier_Import);
			$tabLigne1 = @fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
      $tabLigne = @array_unique($tabLigne1);
      
			//print_r($tabLigne);
			$intNbColonne1 = count($tabLigne1);
      $intNbColonne = count($tabLigne);
      if( $intNbColonne1 != $intNbColonne ) {
         $this->obj_Erreur->erreur(7, "Erreur : Une des colonnes est en double.");
         $intTest++;
         return $intTest;
      } 
		        
			//verifie si les colonnes obligatoires sont présentes ou pas (avec verifie l d'indice )
			for ($j=0; $j<count($this->tabAlkImportCol); $j++) {	
				if (!in_array($this->tabAlkImportCol[$j]["name"], $tabLigne) )  {
					if ($this->tabAlkImportCol[$j]["oblig"] == 1){
						$this->obj_Erreur->erreur(3,$this->tabAlkImportCol[$j]["name"]);
						$intTest++;
					}
				}else{
          if ( ($this->tabAlkImportCol[$j]["index"]!=0) && 
               (array_search('$this->tabAlkImportCol[$j][\"name\"]', $tabLigne) != $this->tabAlkImportCol[$j]["index"]) ){
						$this->obj_Erreur->erreur(2,$this->tabAlkImportCol[$j]["name"]);
						$intTest++;
          }
        }		   	
			}
			
			//verifie si les colonnes du fichiers sont valides
			$strNom=","; 
			for ($j=0; $j<count($this->tabAlkImportCol); $j++) {
				$strNom.=$this->tabAlkImportCol[$j]["name"].",";				
			}
			
			for ($j=0; $j<count($tabLigne); $j++) {	
				if($tabLigne[$j] != NULL){
					if( !strpos($strNom,$tabLigne[$j])){
						$this->obj_Erreur->erreur(4,$tabLigne[$j]);
						$intTest++;
					}
				} else {
					$strMes = "Erreur : Nom de la colonne ".$j." non defini.";
					$this->obj_Erreur->erreur(7,$strMes);
					$intTest++;
				}            
			}
			
			// verifie si les colonnes  correspondent  aux colonnes de référence !
			$i = 2;
			while( !@feof($fImport) ) {
				$tabLigne = @fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
				if( $tabLigne == NULL )	{	
					// on ignore les lignes vides
				} else {
					if( $intNbColonne >= count($tabLigne) ){
						// La ligne est ok
					} else {
						//$strChaineMessage = "Ligne ".$i." : Le nombre de colonne ne correspond pas aux colonnes de référence !";
						$this->obj_Erreur->erreur(5,$i);
						$intTest++;
					}
					$i++;
				}
			}
		}
		
		// fermeture du fichier d'import
		if(@file_exists($this->strFichier_Import))
      @fclose($fImport);
		if( $intTest > 0 ){
			$this->obj_Erreur->erreur(6);
		}
		
		return $intTest;
	}
  
  /**
   * Fonction indiqant si la ligne a importer dans la base est correcte au niveau de sa structure.
   * 
   * @param intCarSeparateur Notation decimale du caractére ascii separateur de champ du fichier d'import
   * @return 0 si correcte 
   */
	public function parseLines($intCarSeparateur)
	{
		$intTest=0;
		if ( is_null($this->intSeparateur) ) {
      $intSeparateur = $intCarSeparateur; // Separateur de champ pour fichier CSV fourni
    } else {
      $intSeparateur = $this->intSeparateur;  // Separateur de champ pour fichier CSV généré
    }
		$tabCopy = array();
		$tabTrace = array();
		if(@file_exists($this->strFichier_Import))	{
			$fImport = @fopen($this->strFichier_Import, "r");
		}
		else{
			$fImport = false;
			$this->obj_Erreur->erreur(1);
			$intTest++;
		}
		
		if( $fImport ){
			// lecture de la premiere ligne
			$intTailleFichier = @filesize($this->strFichier_Import);
			$tabLigne = @fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
			for ($j=0; $j<count($tabLigne); $j++) {
				for ($k=0; $k<count($this->tabAlkImportCol); $k++) {
					if ($tabLigne[$j]==$this->tabAlkImportCol[$k]["name"]){
						//array_push($tabCopy,$this->tabAlkImportCol[$k]);
						$tabCopy[$tabLigne[$j]]=$this->tabAlkImportCol[$k];
						//array_push($tabTrace, $this->tabAlkImportCol[$k]);
            $tabTrace[$j]= $this->tabAlkImportCol[$k];
					}	
					
				}           
			}
      
			$tabKeys = array_keys($tabCopy);
			$intKey="";
			$i = 2;
			
			while( !@feof($fImport) ) {
				$tabLigne = @fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
				
				if( $tabLigne == NULL )	{	
					// on ignore les lignes vides
				}	else {
					
				  // 
				  $tabLigne = $this->convert_values($tabLigne);
				  
				  for ($j=0; $j<count($tabLigne); $j++) {
						
						if( $tabKeys[$j]=="MODE" ) {
							
							switch(strtoupper($tabLigne[$j])){
              case "S":
								for ($k=0; $k<count($this->tabSup); $k++) {
									$intKey = array_search(($this->tabSup[$k]), $tabKeys);
									
									if($tabLigne[$intKey] == ""){
										$strMes = "Erreur : Le champ de la ligne ".$i." et de la colonne ".
                      $tabKeys[$intKey]." est obligatoire pour la suppression.";
										$this->obj_Erreur->erreur(7,$strMes);
										$intTest++;	
									}
								}
								break;
              case "C":
								for ($k=0; $k<count($this->tabAjout); $k++) {
									$intKey = array_search(($this->tabAjout[$k]), $tabKeys);
									
									if($tabLigne[$intKey] == ""){
										$strMes = "Erreur : Le champ de la ligne ".$i." et de la colonne ".
                      $tabKeys[$intKey]." est obligatoire pour l'ajout.";
										$this->obj_Erreur->erreur(7, $strMes);
										$intTest++;
									}
								}
								break;
              case "":
								$strMes = "Erreur : Ligne ".$i." ";
								$this->obj_Erreur->erreur(8,$strMes);
								$intTest++;	
								break;
              case "M":
								for ($k=0; $k<count($this->tabModif); $k++) {
									$intKey = array_search(($this->tabModif[$k]), $tabKeys);
									
									if($tabLigne[$intKey] == ""){
										$strMes = "Erreur : Le champ de la ligne ".$i." et de la colonne ".$tabKeys[$intKey].
                      " est obligatoire pour la modification.";
										$this->obj_Erreur->erreur(7,$strMes);
										$intTest++;	
									}
								}
								break;
							}
						}
						
						if((strlen($tabLigne[$j]) > $tabTrace[$j]["lg"]) && (!is_array($tabTrace[$j]["type"]))){
							$strMes = "Erreur : Le champ de la ligne ".$i." et de la colonne ".$tabKeys[$j].
                " est limité à ".$tabTrace[$j]["lg"]." caractères.";
							$this->obj_Erreur->erreur(7,$strMes);
						}
						
						if($tabLigne[$j] != "" && $tabTrace[$j]["type"]=="numeric"){
							$tabLigne[$j] = ereg_replace(',', '.', $tabLigne[$j]);
							if (!(is_numeric(trim($tabLigne[$j])))){
								$strMes = "Erreur : Le champ de la ligne ".$i.
                  " et de la colonne ".$tabKeys[$j]." est de type numerique.";
								$this->obj_Erreur->erreur(7, $strMes);
								$intTest++;
							}
						}
						//print_r( $tabTrace[$j]);
						//echo "<br>";
						if($tabLigne[$j] != "" && $tabTrace[$j]["mode"]==1 ){
							$strSelect = "";
							for ($s=0; $s<count($tabTrace[$j]["type"]); $s++) {
								$strSelect .= " , ".$tabTrace[$j]["type"][$s];
							}

							if (!(in_array(trim($tabLigne[$j]),$tabTrace[$j]["type"]))){
								$strMes = "Erreur : Le champ de la ligne ".$i.
                  " et de la colonne ".$tabKeys[$j]." est de type ".$strSelect.".";
								$this->obj_Erreur->erreur(7,$strMes);
								$intTest++;													
							}
						}
						if($tabLigne[$j] != "" && $tabTrace[$j]["mode"]==2 ){
								
							$strSelect = "";
							foreach ( $tabTrace[$j]["type"] as $champs ){
                $strSelect .= " , ".$champs;
							}
							
              if (!in_array(($tabLigne[$j]),$tabTrace[$j]["type"] )) {
                $strMes = "Erreur : Le champ de la ligne ".$i.
                  " et de la colonne ".$tabKeys[$j]." est de type ".$strSelect.".";
								$this->obj_Erreur->erreur(7,$strMes);
								$intTest++;
              }
                            
						}
						
						if ($tabTrace[$j]["type"]=="date"){
							if($tabLigne[$j] != "" ){
								if(strlen(trim($tabLigne[$j]))==$tabTrace[$j]["lg"] &&  substr_count(trim($tabLigne[$j]),"/") ==2 ){
									$dateFormat = split("/",trim($tabLigne[$j]));
									if((!is_numeric($dateFormat[0])) || ($dateFormat[0]<1 || $dateFormat[0]>31)){
										$strMes = "le champ jour de la date à la ligne ".$i.
                      " et à la colonne ".$tabKeys[$j]." est incorrecte." ;
										$this->obj_Erreur->erreur(7,$strMes);
										$intTest++;
									}
									if((!is_numeric($dateFormat[1]))|| ($dateFormat[1]<1 || $dateFormat[1]>12)){
										$strMes = "Erreur : Le champ mois de la date à la ligne ".
                      $i." et à la colonne ".$tabKeys[$j]." est incorrecte.";
										$this->obj_Erreur->erreur(7,$strMes);
										$intTest++;												
									}
									if(!is_numeric($dateFormat[2])){
										$strMes = "Erreur : Le champ année de la date à la ligne ".$i.
                      " et à la colonne ".$tabKeys[$j]." est incorrecte.";
										$this->obj_Erreur->erreur(7,$strMes);
										$intTest++;
									}
								}else{
									$strMes = "Erreur : Le champ date de la ligne ".$i.
                    " et de la colonne ".$tabKeys[$j]." doit etre au format JJ/MM/AAAA";
									$this->obj_Erreur->erreur(7,$strMes);
									$intTest++;											
								}
							}
						}
					}
					$i++;
				}
			}
		}
		
		// fermeture du fichier d'import
		if(@file_exists($this->strFichier_Import))
      @fclose($fImport);

		if( $intTest > 0 ) {
			$this->obj_Erreur->erreur(6);
		}
		
		return $intTest;
	}
	
  /**
   * Méthode virtuelle effectuant une màj par rapport à une ligne du fichier d'import
   *
   * @param tabLigne
   * @param tabCopy
   * @return Retourne un entier : 0 si aucune erreur, >0 sinon
   */
  public function update($tabLigne, $tabCopy) { }
  
  /**
   * Méthode virtuelle effectuant la suppression par rapport à une ligne du fichier d'import
   *
   * @param tabLigne
   * @param tabCopy
   * @return Retourne un entier : 0 si aucune erreur, >0 sinon
   */
  public function delete($tabLigne, $tabCopy) { }
  
  /**
   * Méthode virtuelle effectuant un ajout par rapport à une ligne du fichier d'import
   *
   * @param tabLigne
   * @param tabCopy
   * @return Retourne un entier : 0 si aucune erreur, >0 sinon
   */
  public function insert($tabLigne, $tabCopy) { }

  /**
   * Integre le fichier d'import
   *
   * @param intCarSeparateur  caractere séparateur du fichier
   * @return Retourne un entier : 0 si aucune erreur, >0 sinon
   */
  public function integrate($intCarSeparateur) 
  {
    $iTotRes = 0;
    if ( is_null($this->intSeparateur) ) {
      $intSeparateur = $intCarSeparateur; // Separateur de champ pour fichier CSV fourni
    } else {
      $intSeparateur = $this->intSeparateur;  // Separateur de champ pour fichier CSV généré
    }
    $tabCopy = array();
    $fImport = @fopen($this->strFichier_Import, "r");
    $i = 2;
	
    if( $fImport ) {
      // lecture de la premiere ligne
      $intTailleFichier = @filesize($this->strFichier_Import);
      $tabLigneRef = @fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
      for ($j=0; $j<count($tabLigneRef); $j++) {
        $tabCopy[$tabLigneRef[$j]] = $j;
      }

      while( !@feof($fImport) ) {
        $oRes = 0;
        $tabLigne = @fgetcsv($fImport, $intTailleFichier, chr($intSeparateur));
        if( $tabLigne == NULL )	{
          // on ignore les lignes vides
        } else {
          
          // 
          $tabLigne = $this->convert_values($tabLigne);
          
          switch ($tabLigne[$tabCopy["MODE"]]){
					case "S":
						$oRes = $this->delete($tabLigne, $tabCopy, $tabLigneRef,$i);			
            break;
          case "M":
						$oRes = $this->update($tabLigne, $tabCopy, $tabLigneRef,$i);	
            break;
					case "C":
            $oRes = $this->insert($tabLigne, $tabCopy, $tabLigneRef,$i);
            break;
          }
        }
        $iTotRes += $oRes;
        $i++;
      }
    }
    return $iTotRes;
  }
  
  /**
   * Convertit les valeurs du tableau dans l'encodage systeme
   *
   * @param unknown_type $tabData
   * @return unknown
   */
  protected function convert_values($tabData)
  {
    // etant donné que l'encodage du systeme et celui du fichier peuvent être différents
    // on essaye de convertir les chaines parsées dans l'encodage système.
    // il faut fournir à mb_convert l'encodage source, ce qui est fair par mb_detect.
    foreach($tabData as $key=>$value) {
      $tabData[$key] = mb_convert_encoding($value, ALK_HTML_ENCODING, 
        mb_detect_encoding($value, "UTF-8, ISO-8859-15, ISO-8859-1"));
    }
    return $tabData;
  }

}			

?>