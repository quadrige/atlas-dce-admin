<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkobject.class.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkDb
 * @brief Classe de base pour la connexion à une base de données
 */
abstract class AlkDb extends AlkObject
{
  /** connection */
  protected $conn;

  /** Adresse IP du serveur de base de donnees */
  protected $strHost;
    
  /** Identifiant de l'utilisateur */
  protected $strLogin;

  /** Nom de la base de donnees */
  protected $strDb;

  /** Mot de passe */
  protected $strPwd;

  /** Alias */
  protected $strAlias;

  /** numéro de port */
  protected $strPort;
    
  /** Message de la dernière erreur rencontrée */
  protected $lastErr;

  /** true si utilisation d'une transaction, false sinon (par défaut) */
  protected $bUseCommit;

  /** true tant que les exécutions réussissent avec succès, false dès qu'une erreur est détectée */
  protected $bResExecuteInTransaction;

  /** encodage utilisé */
  protected $strDbEncoding;

  /** différence en heure entre heure locale du navigateur et GMT, <0 à l'est, >0 à l'ouest */
  protected $iDeltaGMT;

  /** différence en heure entre heure locale du serveur et GMT, <0 à l'est, >0 à l'ouest */
  protected $iDeltaGMTServ;


  /** 
   * Schema de la base de données, utile lorsque le préfixe de table peut être fixé au préalable, cas postgre 
   * la valeur par défaut est fixée dans le constructeur, utiliser l'accesseur setSchema() sinon
   */
  protected $strSchema;

  /**
   *  Constructeur de la classe db : initialisation des attributs de l'objet db
   *
   * @param strLogin Identifiant de l'utilisateur
   * @param strHost  Adresse IP du serveur de base de donnees
   * @param strPwd   Mot de passe
   * @param strDb    Nom de la base de donnees
   * @param strAlias Alias de la base de donnees
   * @param strPort  Numéro de port
   */

  public function __construct($strLogin,  $strHost,  $strPwd,  $strDb,  $strAlias,  $strPort)
  {
    parent::__construct();

    $this->strLogin = $strLogin;
    $this->strHost = $strHost;
    $this->strPwd = $strPwd;
    $this->strDb = $strDb;
    $this->strSchema = "";
    $this->strAlias = $strAlias;
    $this->strPort = $strPort;
    $this->lastErr = false;
    $this->bUseCommit = false;
    $this->strDbEncoding = ALK_SGBD_ENCODING;
    $this->bResExecuteInTransaction = true;
    $this->iDeltaGMT = -1;
    $this->iDeltaGMTServ = -1;
  }

  /**
   *  destructeur par défaut
   */
  public function destruct()
  {	  
  	$this->Disconnect();
  }

  /**
   *  Fixe la nouvelle erreur rencontrée
   *
   * @param err false ou tableau associatif contenant l'erreur
   */
  protected function _setError($err)
  {
    $this->lastErr = $err;
  }

  /**
   *  Retourne la dernière erreur rencontrée
   *
   * @return Retourne une chaine, vide si pas d'erreur, texte sinon
   */
  public function getLastError()
  {
    if( !is_string($this->lastErr) ) $this->lastErr = "";
    return $this->lastErr;
  }

  /**
   * Retourne l'encodage client de la connexion SGBD
   * Retourne l'encodage au format normé: UTF-8, ISO-8859-1
   * @return string
   */
  public function getEncoding()
  {
    return $this->strDbEncoding;
  }

  /**
   * Force l'encodage client de la connexion SGBD
   * Nécessite d'ouvrir la connexion ensuite
   * @param strEncoding  nom de l'encodage au format normé: UTF-8, ISO-8859-1
   */
  public function setEncoding($strEncoding)
  {
    $this->strDbEncoding = $strEncoding;
  }

  /**
   * Modifie le nom du schema utilisé pour effectuer les requetes
   * @param strSchema Nom du schema, correspond au préfixe placé dans le nom de la table
   */
  public function setSchema($strSchema) 
  {
    $this->strSchema = $strSchema; 
  }

  /**
   * Mémorise la différence en heure entre l'heure locale du navigateur et GMT
   * @param iDeltaGMT      entier, décalage en heure du client / GMT, <0 à l'est, >0 à l'ouest
   * @param iDeltaGMTServ  entier, décalage en heure du serveur / GMT, <0 à l'est, >0 à l'ouest
   */
  public function setDeltaGMT($iDeltaGMT, $iDeltaGMTServ=-1)
  {
    $this->iDeltaGMT = $iDeltaGMT;
    $this->iDeltaGMTServ = $iDeltaGMTServ;
  }

  /**
   *  établit la connection avec la base de données
   */
  abstract public function connect($strAlkSGBDEncoding=ALK_SGBD_ENCODING);

  /**
   *  Déconnection avec la base de donnees
   */
  abstract public function disconnect( );

  /**
   *  Retourne le dataset correspondant à la requete strSql
   *
   * @param strSql        Requete SQL
   * @param idFirst       Indice de pagination : premier élément
   * @param idLast        Indice de pagination : dernier élément
   * @param bErr          Type de gestion d'erreur :
   *                        = false pour capter les erreurs
   *                        = true stop l'exécution sur erreur
   * @param iExpire       =0 par défaut, >0 pour mémoriser en cache le résutat de la requête avec un délai d'expiration de iExpire secondes
   * @param strCacheName  =alkanet par défaut, permet de regrouper les éléments cachés à l'aide de ce nom afin de gérer en live, la libération du cache 
   * @return Retourne un dataSet
   */
  abstract public function initDataset($strSql, $idFirst=0, $idLast=-1, $bErr=true, $iExpire=0, $strCacheName="alkanet"); 

  /**
   * Execute une requête et retourne la valeur du premier champ du premier enregistrement
   * Si une erreur se produit ou qu'aucune valeur n'est trouvée, retourne defaultValue
   * @param strSql        Requête SQL
   * @param defaultValue  valeur retournée si erreur ou si aucune information trouvée
   * @return Retourne la valeur obtenue, 
   */
  abstract public function getScalarSql($strSql, $defaultValue);

  /**
   *  Exécute la requête SQL = insert, update, delete
   *
   * @param strSql Requête SQL
   * @return Retourne un entier : 0 si KO, 1 si OK
   */
  abstract public function executeSql($strSql); 

  /**
   *  initialise une transaction sur la connexion courante
   *        Ouvre la transaction (bUseCommit = true)
   */
  abstract public function initTransaction(); 

  /**
   *  Valide ou annule une transaction sur la connexion courante
   *        en fonction de bResExecuteInTransaction
   *
   * @return Retourne un bool : true si commit, false si rollback
   */
   
  public function doneTransaction()
  {
    if( $this->bResExecuteInTransaction == true ) {
      $this->CommitTransaction();
      return true;
    }

    $this->RollBackTransaction();
    return false;
  }

  /**
   * Retourne un tableau associatif fournissant les types de données des champs
   * Le tableau contient les clés suivantes : int, datetime, varchar, float, text
   * @return array 
   */
  abstract protected function getSqlType();

  /**
   * Construit la requête sql qui recopie la ligne d'une table vers une ligne d'une autre
   * Retourne la requête sql générée
   * @param strTableSrc     Nom de la table source
   * @param strTablsDest    Nom de la table destination
   * @param tabFieldPkSrc   tableau associatif : cle = nom du champ clé primaire de la table source, valeur = valeur de cette cle
   * @param tabFieldPkDest  tableau associatif : cle = nom du champ clé primaire de la table destination, valeur = valeur de cette cle
   * @param tabFieldsName   tableau associatif : cle = nom du champ destination (sans alias de table en début), 
   *                                             valeur = nom du champ source avec alias de table égale à "s." 
   *                                                    ou valeur spécifique
   *                                                    ou vide pour reprendre le même nom de colonne que la source
   * @return string
   */
  abstract public function getSqlCopyRowFromTableToTable($strTableSrc, $strTableDest, $tabFieldPkSrc, $tabFieldPkDest, $tabFieldsName);
  
  /**
   * Retourne le code SQL permettant de créer une table (uniquement les champs typés)
   * @param strTableName  nom de la table ou tableau
   * @param tabFields     tableau contenant les informations sur les champs à créer
   * @return string
   */
  abstract public function getSqlCreateTable($strTableName, $tabDataFields);
  
  /**
   * Retourne un tableau contenant les requetes SQL permettant de créer un ensemble de tables
   * @param tabData  tableau contenant les informations sur les tables à créer
   * @return array
   */
  public function getTabSqlCreateTable($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName => $tabDataFields) {
      $tabSql[] = $this->getSqlCreateTable($strTableName, $tabDataFields);
    }
    return $tabSql;
  }

  /**
   * Retourne le code SQL permettant de supprimer une table
   * @param strTableName  nom de la table ou tableau
   * @return string
   */
  public function getSqlDropTable($strTablename)
  {
    $strSql = "drop table ".mb_strtoupper($strTablename);
    return $strSql;
  }
  
  /**
   * Retourne un tableau contenant les requetes SQL permettant de supprimer un ensemble de tables
   * @param tabData  tableau contenant les informations sur les tables à supprimer
   * @return array
   */
  public function getTabSqlDropTable($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName) {
      $tabSql[] = $this->getSqlDropTable($strTableName);
    }
    return $tabSql;
  }
  
  /**
   * Retourne le code Sql permettant d'ajouter une clé primaire à une table
   * @param strTableName  nom de la table
   * @param strPkName     nom de la clé primaire
   * @param strFieldList  liste des champs caractérisant la clé primaire
   * @return string 
   */
  public function getSqlAddPrimary($strTableName, $strPkName, $strFieldList)
  {
    return "alter table ".mb_strtoupper($strTableName).
      " add constraint ".mb_strtoupper($strPkName)." primary key (".mb_strtoupper($strFieldList).")"; 
  }
  
  /**
   * Retourne un tableau contenant les requetes SQL permettant d'ajouter des clés primaires à un ensemble de tables
   * @param tabData  tableau contenant les informations sur les clés primaires à créer
   * @return array
   */
  public function getTabSqlAddPrimary($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName => $tabDataPk) {
      foreach($tabDataPk as $strPkName => $strFieldList) {
        $tabSql[] = $this->getSqlAddPrimary($strTableName, $strPkName, $strFieldList);
      }
    }
    return $tabSql;
  }
  
  /**
   * Retourne le code Sql permettant de supprimer une clé primaire à une table
   * @param strTableName  nom de la table
   * @param strPkName     nom de la clé primaire
   * @return string
   */
  abstract public function getSqlDropPrimary($strTableName, $strPkName);
  
  /**
   * Retourne un tableau contenant les requetes SQL permettant de supprimer des clés primaires à un ensemble de tables
   * @param tabData  tableau contenant les informations sur les clés primaires à supprimer
   * @return array
   */
  public function getTabSqlDropPrimary($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName => $strPkName) {
      $tabSql[] = $this->getSqlDropPrimary($strTableName, $strPkName);
    }
    return $tabSql;
  }
  
  /**
   * Retourne le code SQL permettant de créer un index sur un champ d'une table
   * @param strTableName   nom de la table
   * @param strIndexName   nom de l'index
   * @param strFieldName   nom du champ
   * @return string
   */
  abstract public function getSqlCreateIndex($strTableName, $strIndexName, $strFieldName);

  /**
   * Retourne un tableau contenant les requetes SQL permettant d'ajouter des index à un ensemble de tables
   * @param tabData  tableau contenant les informations sur les index à créer
   * @return array
   */
  public function getTabSqlCreateIndex($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName => $tabDataIdx) {
      foreach($tabDataIdx as $strIndexName => $strFieldName) {
        $tabSql[] = $this->getSqlCreateIndex($strTableName, $strIndexName, $strFieldName);
      }
    }
    return $tabSql;
  }
  
 /**
   * Retourne le code SQL permettant de supprimer un index sur un champ d'une table
   * @param strTableName   nom de la table
   * @param strIndexName   nom de l'index
   * @return string
   */
  abstract public function getSqlDropIndex($strTableName, $strIndexName);

  /**
   * Retourne un tableau contenant les requetes SQL permettant de supprimer des index à un ensemble de tables
   * @param tabData  tableau contenant les informations sur les index à créer
   * @return array
   */
  public function getTabSqlDropIndex($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName => $tabDataIdx) {
      foreach($tabDataIdx as $strIndexName ) {
        $tabSql[] = $this->getSqlDropIndex($strTableName, $strIndexName);
      }
    }
    return $tabSql;
  }

  /**
   * Retourne le code SQL permettant d'ajouter une clé étrangère à une table
   * @param strTableName   nom de la table locale
   * @param strFkName      nom de la clé étrangère
   * @param strFieldFk     nom du champ local
   * @param strTablePk     nom de la table cible 
   * @param strFieldPk     nom du champ cible
   * @param strOption      option complémentaire
   * @return string
   */
  abstract public function getSqlAddConstraintForeignKey($strTableName, $strFkName, $strFieldFk, $strTablePk, $strFieldPk, $strOption="");

  /**
   * Retourne un tableau contenant les requetes SQL permettant d'ajouter des clés étrangères à un ensemble de tables
   * @param tabData  tableau contenant les informations sur les clés étrangères à créer
   * @return array
   */
  public function getTabSqlAddConstraintForeignKey($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName => $tabDataFks) {
      foreach($tabDataFks as $strFkName => $tabDataFk) {
        $tabSql[] = $this->getSqlAddConstraintForeignKey($strTableName, $strFkName, $tabDataFk["ffk"], $tabDataFk["tpk"], $tabDataFk["fpk"], 
                                                         ( array_key_exists("op", $tabDataFk) ? $tabDataFk["op"] : "") );
      }
    }
    return $tabSql;
  }
  
  /**
   * Retourne le code SQL permettant de supprimer une clé étrangère à une table
   * @param strTableName   nom de la table locale
   * @param strFkName      nom de la clé étrangère
   * @return string
   */
  abstract public function getSqlDropConstraintForeignKey($strTableName, $strFkName);

  /**
   * Retourne un tableau contenant les requetes SQL permettant de supprimer des clés étrangères à un ensemble de tables
   * @param tabData  tableau contenant les informations sur les clés étrangères à créer
   * @return array
   */
  public function getTabSqlDropConstraintForeignKey($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName => $tabDataFks) {
      foreach($tabDataFks as $strFkName ) {
        $tabSql[] = $this->getSqlDropConstraintForeignKey($strTableName, $strFkName);
      }
    }
    return $tabSql;
  }

  /**
   * Retourne le code SQL permettant d'ajouter une contrainte d'unicité à une table
   * @param strTableName   nom de la table
   * @param strUqName      nom de la contrainte
   * @param strFieldList   liste des champs caractérisant l'unicité
   * @return string
   */
  public function getSqlAddConstraintUnique($strTableName, $strUqName, $strFieldList)
  {
    return "alter table ".mb_strtoupper($strTableName).
      " add constraint ".mb_strtoupper($strUqName)." unique (".mb_strtoupper($strFieldList).")";
  }

  /**
   * Retourne un tableau contenant les requetes SQL permettant d'ajouter des contraintes d'unicité à un ensemble de tables
   * @param tabData  tableau contenant les informations sur les contraintes d'unicité à créer
   * @return array
   */
  public function getTabSqlAddConstraintUnique($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName => $tabDataUqs) {
      foreach($tabDataUqs as $strUqName => $strFieldList) {
        $tabSql[] = $this->getSqlAddConstraintUnique($strTableName, $strUqName, $strFieldList);
      }
    }
    return $tabSql;    
  }
  
  /**
   * Retourne le code SQL permettant de supprimer une contrainte d'unicité à une table
   * @param strTableName   nom de la table
   * @param strUqName      nom de la contrainte
   * @return string
   */
  abstract public function getSqlDropConstraintUnique($strTableName, $strUqName);

  /**
   * Retourne un tableau contenant les requetes SQL permettant de supprimer des contraintes d'unicité à un ensemble de tables
   * @param tabData  tableau contenant les informations sur les contraintes d'unicité à créer
   * @return array
   */
  public function getTabSqlDropConstraintUnique($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName => $tabDataUqs) {
      foreach($tabDataUqs as $strUqName) {
        $tabSql[] = $this->getSqlDropConstraintUnique($strTableName, $strUqName);
      }
    }
    return $tabSql;    
  }

  /**
   * Retourne le code SQL permettant de créer une vue
   * @param strViewName   nom de la vue
   * @param strSql        code sql de la vue
   */
  public function getSqlCreateView($strViewName, $strSql)
  {
    return "create view ".mb_strtoupper($strViewName)." ".$strSql;
  }

  /**
   * Retourne un tableau contenant les requetes SQL permettant de créer des vues
   * @param tabData  tableau contenant les informations sur les clés étrangères à créer
   * @return array
   */
  public function getTabSqlCreateView($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strViewName => $strSql) {
      $tabSql[] = $this->getSqlCreateView($strViewName, $strSql);
    }
    return $tabSql;
  }
  
  /**
   * Retourne le code SQL permettant de supprimer une vue
   * @param strViewName   nom de la vue
   * @return string
   */
  public function getSqlDropView($strViewName)
  {
    return "drop view ".mb_strtoupper($strViewName);
  }

  /**
   * Retourne un tableau contenant les requetes SQL permettant de supprimer une vue
   * @param tabData  tableau contenant les informations sur les clés étrangères à créer
   * @return array
   */
  public function getTabSqlDropView($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strViewName ) {
      $tabSql[] = $this->getSqlDropView($strViewName);
    }
    return $tabSql;
  }

  /**
   * Retourne le code SQL permettant de créer une séquence
   * @param strSeqName   nom de la séquence
   * @param iStart       indice de début de la séquence
   */  
  abstract public function getSqlCreateSequence($strSeqName, $iStart);

  /**
   * Retourne un tableau contenant les requetes SQL permettant d'ajouter un ensemble de séquences
   * @param tabData  tableau contenant les informations sur les séquences à créer
   * @return array
   */
  public function getTabSqlCreateSequence($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strSeqName => $iStart) {
      $tabSql[] = $this->getSqlCreateSequence($strSeqName, $iStart);
    }
    return $tabSql;    
  }
  
  /**
   * Retourne le code SQL permettant de supprimer une séquence
   * @param strSeqName   nom de la séquence
   * @return string
   */  
  abstract public function getSqlDropSequence($strSeqName);

  /**
   * Retourne un tableau contenant les requetes SQL permettant de supprimer un ensemble de séquences
   * @param tabData  tableau contenant les informations sur les séquences à supprimer
   * @return array
   */  
  public function getTabSqlDropSequence($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strSeqName) {
      $tabSql[] = $this->getSqlDropSequence($strSeqName);
    }
    return $tabSql;    
  }
  
  /**
   * Retourne un tableau contenant les requetes SQL permettant de modifier la structure de tables
   * @param tabData  tableau contenant les informations sur les tables à modifier
   * @return array
   */  
  public function getTabSqlAlterTable($tabData)
  {
    $tabSql = array();
    foreach($tabData as $strTableName => $tabDatas) {
      foreach($tabDatas as $tabData) {
        if( isset($tabData["action"]) ) {
          switch( $tabData["action"] ) {
          case "add":
            if( isset($tabData["column"]) && isset($tabData["type"]) && 
                isset($tabData["length"]) && isset($tabData["default"]) && isset($tabData["nullable"]) ) {
              $tabSql[] = $this->getSqlAlterTableAddColumn($strTableName, $tabData["column"], $tabData["type"],
                                                           $tabData["length"],$tabData["default"], $tabData["nullable"]);
            }
            break;  
          case "update":
            if( isset($tabData["column"]) && isset($tabData["newcolumn"]) && isset($tabData["type"]) && 
                isset($tabData["length"]) && isset($tabData["default"]) && isset($tabData["nullable"]) ) {
              $tabSql[] = $this->getSqlAlterTableUpdateColumn($strTableName, $tabData["column"], $tabData["newcolumn"], 
                                                              $tabData["type"],$tabData["length"],$tabData["default"], $tabData["nullable"]);
            }
            break;
          case "drop": 
            if( isset($tabData["column"]) )
              $tabSql[] = $this->getSqlAlterTableDropColumn($strTableName, $tabData["column"]);
            break; 
          }
        }
      }
    }
    return $tabSql;    
  }

  /**
   * Retourne la requete sql correspondant à un alter table modify column
   * Retourn vrai si ok, faux sinon
   *
   * @param tableName           Nom de la table
   * @param columnName          Nom actuel de la colonne
   * @param new_columnName      Nouveau nom de la colonne
   * @param new_columnType      Nouveau type de la colonne
   * @param new_columnLength    Nouvelle longueur de la colonne
   * @param new_columnDefault   Nouvelle valeur par défaut de la colonne
   * @param new_columnNullable  Nouvel état nullable de la colonne (=0 : NOT NULL, =1 : NULL)
   * @return boolean
   */
  abstract public function getSqlAlterTableUpdateColumn($strTableName, $strColumnName, $strNewColumnName, $strNewColumnType="", 
                                                        $strNewColumnLength="", $strNewColumnDefault="", $iNewColumnNullable=-1);
  
  /**
   * Effectue un alter table modify column
   * Retourn vrai si ok, faux sinon
   *
   * @param strTableName        Nom de la table
   * @param strColumnName       Nom actuel de la colonne
   * @param strNewColumnName    Nouveau nom de la colonne, =strColumnName si vide
   * @param strNewColumnType    Nouveau type de la colonne
   * @param strNewColumnLength  Nouvelle longueur de la colonne
   * @param strNewColumnDefault Nouvelle valeur par défaut de la colonne, = "" pour ne rien fait
   * @param iNewColumnNullable  Nouvel état nullable de la colonne (=0 : NOT NULL, =1 : NULL, =-1 no change)
   * @return boolean
   */
  public function doAlterTableUpdateColumn($strTableName, $strColumnName, $strNewColumnName, $strNewColumnType="", 
                                           $strNewColumnLength="", $strNewColumnDefault="", $iNewColumnNullable=-1)
  {
    $strSql = $this->getSqlAlterTableUpdateColumn($strTableName, $strColumnName, $strNewColumnName, $strNewColumnType, 
                                                  $strNewColumnLength, $strNewColumnDefault, $iNewColumnNullable);
    return $this->executeSql($strSql); 
  }
    
  /**
   * Retourne la requête sql effectuant un alter table add column
   * Retourne une chaine vide si erreur
   *
   * @param strTableName     Nom de la table
   * @param strColumnName    Nom de la colonne
   * @param strColumnType    Nouveau type de la colonne
   * @param strColumnLength  Nouvelle longueur de la colonne
   * @param strColumnDefault Nouvelle valeur par défaut de la colonne
   * @param iColumnNullable  Nouvel état nullable de la colonne (=0 : NOT NULL, =1 : NULL, =-1 no change)
   * @return string
   */
  public function getSqlAlterTableAddColumn($strTableName, $strColumnName, $strColumnType="", 
                                        $strColumnLength="", $strColumnDefault="", $iColumnNullable=-1)
  {
    if ( $strTableName=="" || $strColumnName=="" ) 
      return "";
      
    $strType = $this->getColumnType($strColumnType, $strColumnLength);
    if ( $strType===false )
      return "";
      
    $strSql = "alter table ".mb_strtoupper($strTableName).
      " add column ".mb_strtoupper($strColumnName)." ".$strType.
      ( $strColumnDefault!="" 
        ? " default ".$strColumnDefault
        : "" ).
      ( $iColumnNullable!=-1 
        ? ( $iColumnNullable==0 
            ? " null" 
            : " not null" )
        : "" );
    
    return $strSql;
  }
  
  /**
   * Effectue un alter table add column
   * Retourn vrai si ok, faux sinon
   *
   * @param strTableName     Nom de la table
   * @param strColumnName    Nom de la colonne
   * @param strColumnType    Nouveau type de la colonne
   * @param strColumnLength  Nouvelle longueur de la colonne
   * @param strColumnDefault Nouvelle valeur par défaut de la colonne
   * @param iColumnNullable  Nouvel état nullable de la colonne (=0 : NOT NULL, =1 : NULL, =-1 no change)
   * @return boolean
   */
  public function doAlterTableAddColumn($strTableName, $strColumnName, $strColumnType="", 
                                        $strColumnLength="", $strColumnDefault="", $iColumnNullable=-1)
  {
    $strSql = $this->getSqlAlterTableAddColumn($strTableName, $strColumnName, $strColumnType, 
                                               $strColumnLength, $strColumnDefault, $iColumnNullable);
    return $this->executeSql($strSql); 
  }

  /**
   * Retourne la requête sql effectuant un alter table drop column
   * Retourne une chaine vide si erreur
   *
   * @param strTableName       Nom de la table
   * @param strColumnName      Nom de la colonne
   * @return string
   */
  public function getSqlAlterTableDropColumn($strTableName, $strColumnName)
  {
    if ( $strTableName=="" || $strColumnName=="" ) 
      return "";
    return "alter table ".mb_strtoupper($strTableName).
      " drop column ".mb_strtoupper($strColumnName);
  }

  /**
   * Effectue un alter table drop column
   * Retourn vrai si ok, faux sinon
   *
   * @param strTableName       Nom de la table
   * @param strColumnName      Nom de la colonne
   * @return boolean
   */
  public function doAlterTableDropColumn($strTableName, $strColumnName)
  {
    $strSql = $this->getSqlAlterTableDropColumn($strTableName, $strColumnName);
    return $this->executeSql($strSql); 
  }

  /**
   *  Effectue un commit sur l'ensemble des requêtes exécutées sur la transaction en cours
   *        Ferme la transaction après le commit (bUseCommit = false)
   *
   * @return Retourne un bool : true si ok, false sinon
   */
  abstract public function commitTransaction() ;

  /**
   *  Effectue un rollback pour annuler l'ensemble des requêtes exécutées sur la transaction en cours
   *        Ferme la transaction après le commit (bUseCommit = false)
   *
   * @return Retourne un bool : true si ok, false sinon
   */
  abstract public function rollBackTransaction() ;

  /**
   *  Retourne le code sql des instructions "show tables" et "show tables like "
   * 
   * @param strLikeTable    Si non vide permet de faire un show tables like 
   * @return string SQL
   */
  abstract public function getShowTables($strLikeTable="") ;

  /**
   * Retourne la description des colonnes d'une table
   * @param strTableName    Nom de la table
   * @return dataset
   */
  abstract public function getDsTableColumns($strTableName);

  /**
   *  Remplace les caractères spéciaux d'un champ texte d'une requete SQL
   *
   * @param strString  Valeur du champ texte d'une requete
   * @param bHtmlVerif true par défaut pour éviter les attaques de type XSS, false pour éviter le filtre.
   * @return Retourne une chaine obtenue après traitement
   */
  public function analyseSql($strString, $bHtmlVerif=true, $strQuoteEscape="''")
  {
    $strTmp = str_replace("\\", "\\\\", $strString);
    $strTmp = $this->convertCharactersCp1252($strTmp);
    $strTmp = mb_convert_encoding($strTmp, $this->strDbEncoding, ALK_HTML_ENCODING);
    if( $bHtmlVerif ) {
      $strTmp = htmlspecialchars($strTmp, ENT_COMPAT, ($this->strDbEncoding == 'UTF8' ? 'UTF-8' : $this->strDbEncoding));
    }
    $lastEnc = mb_regex_encoding();
    mb_regex_encoding($this->strDbEncoding);
    $strTmp = mb_ereg_replace("'", $strQuoteEscape, $strTmp);
    mb_regex_encoding($lastEnc);

    return $strTmp; 
  }

  /**
   *  Retourne le code sql équivalent pour un clob : champ de type TEXT (equivalent varchar)
   * 
   * @param strField   nom du champ clob
   * @param strValue   valeur du champ clob
   * @param bHtmlVerif true par défaut pour éviter les attaques de type XSS, false pour éviter le filtre.
   * @return Retourne un string
   */
  public function getCLob($strField, $strValue, $bHtmlVerif=true) 
  { 
    return "'".$this->analyseSql($strValue, $bHtmlVerif)."'";
  }
  
  /**
   *  Retourne une chaine de comparaison dans une requete SQL
   *
   * @param strField   Nom du champ dont la valeur est à tester
   * @param strCompare Opérateur de comparaison
   * @param strValeur  Valeur à comparer
   * @param strCaseOk  Valeur retournée si comparaison vraie
   * @param strCaseNok Valeur retournée si comparaison fausse
   * @return Retourne une chaine : l'expression SQL associée à la comparaison
   */
  abstract public function compareSql($strField, $strCompare, $strValue, $strCaseOK, $strCaseNok) ;

  /**
   *  Obtenir le prochain identifiant à inserer dans la table strTable
   *
   * @param strTable    Nom de la table
   * @param strField    Nom du champ id
   * @param strSequence Nom de la sequence associée
   * @return Retourne un entier : le prochain id
   */
  abstract public function getNextId($strTable, $strField, $strSequence="");
  
  /**
   * Retourne l'instrcution SQL permettant d'obtenir le prochain identifiant à inserer dans la table strTable
   *
   * @param strTable    Nom de la table
   * @param strField    Nom du champ id
   * @param strSequence Nom de la sequence associée
   * @return Retourne une requete sql
   */
  abstract public function getStrNextId($strTable, $strField, $strSequence="");

  /**
   *  Formate une date au format SQL da la base de donnees
   *
   * @param strFormat Format de la date passee en parametre
   * @param strDate   Valeur de la date équivalente au format ou dans son expression entière
   * @param bToDate   Identifie l'expression à retourner : 
   *                  = true  : l'expression retournée par la requete est une date (insertion)
   *                  = false : l'expression retournée par la requete est une chaine (extraction)
   * @param bCastToInt =true  pour caster la transformation en entier si bToDate=false
   *                   =false pour laisser to_char() dans son type par défaut, si bToDate=false
   * @param Retourne une chaine : l'expression SQL associée
   */
  abstract public function getDateFormat($strFormat, $strDate, $bToDate=true, $bCastToInt=false) ;

 /**
   *  Formate une date au format SQL da la base de donnees à partir d'un timestamp
   *
   * @param strFormat Format de la date à obtenir
   * @param timestamp Timestamp à transformer en date
   * @param bToDate   Identifie l'expression à retourner : 
   *                  = true  : l'expression retournée par la requete est une date (insertion)
   *                  = false : l'expression retournée par la requete est une chaine (extraction)   
   * @note Format : 
   *       - SS    : secondes
   *       - MI    : Minute
   *       - HH    : Heure du jour
   *       - D     : Numéro du jour dans la semaine
   *       - DAY   : Nom du jour
   *       - DD    : Numéro du jour dans le mois
   *       - DDD   : Numéro du jour dans l'année
   *       - IW    : Numéro de la semaine dans l'année (Norme iso)
   *       - WW    : Numéro de la semaine dans l'année
   *       - MM    : Numéro du mois 
   *       - MONTH : Nom du mois
   *       - YYYY  : année sur 4 chiffres
   *       - YY    : année sur 2 chiffres
   * @param Retourne une chaine : l'expression SQL associée
   */
  abstract public function getDateFromTimestamp($strFormat,  $timestamp, $bToDate=true);

  /**
   *  Retourne l'expression SQL permettant d'additionner des intervalles de temps à une date
   *
   * @param strChamp    Nom du champ ou expression sql à traiter
   * @param iNb         Nombre d'intervalles à ajouter
   * @param strInterval Type d'intervalle : Y=année, M=mois, D=jour
   * @return Retourne une chaine : l'expression SQL associée
   */
  
  abstract public function getNbDaysBetween($strDateFrom, $strDateTo) ;

  /**
   * @brief Retourne l'expression SQL permettant d'additionner des intervalles de temps à une date
   *
   * @param strChamp    Nom du champ ou expression sql à traiter
   * @param iNb         Nombre d'intervalles à ajouter
   * @param strInterval Type d'intervalle : Y=année, M=mois, D=jour
   * @return Retourne une chaine : l'expression SQL associée
   */
  
  abstract public function getDateAdd($strChamp, $iNb, $strInterval) ;

  /**
   *  Retourne l'expression SQL qui fournit la concatenation d'un nombre indéfinit de paramètres
   *
   * @return Retourne une chaine : l'expression SQL associée
   */
  abstract public function getConcat() ;

  /**
   *  Retourne l'expression SQL qui fournit la concatenation récursive sur une colonne
   * @param strField      Colonne sur laquelle s'effectue la concaténation groupée
   * @param strSeparator  Chaine SQL donnant le séparateur
   * @param strOrder      Ordre de lecture des données (Mysql)
   * @param bDistinct     Indique si sélection des éléments distincts seulement
   * @param strFrom       paramètre spécifique en fonction du sgbd
   * @param bNullTest     paramètre spécifique en fonction du sgbd
   *
   * @return Retourne une chaine : l'expression SQL associée
   */
  abstract public function getGroupConcat($strField, $strSeparator="','", $strOrder="", $bDistinct=false, $strFrom="", $bNullTest=true);
  
  /**
   *  Retourne l'expression SQL qui fournit la concatenation d'un nombre indéfinit de paramètres
   *
   * @param strChamp Nom du champ ou expression sql à traiter
   * @param iPos     Position de départ (premier caractère = 0)
   * @param iLength  Longueur de la sous-chaine (facultatif)
   * @return Retourne une chaine : l'expression SQL associée
   */
  abstract public function getSubstring($strChamp, $iPos, $iLength=-1) ;

  /**
   *  Retourne l'expression SQL qui fournit la date-heure système
   *
   * @return Retourne une chaine : l'expression SQL associée
   */
  abstract public function getDateCur();

  /**
   *  Retourne l'expression SQL qui transforme en minuscules une expression
   *
   * @param strChamp Nom du champ ou expression sql à traiter
   * @return Retourne une chaine : l'expression SQL associée
   */
  abstract public function getLowerCase($strChamp);

  /**
   *  Retourne l'expression SQL qui transforme en majuscules une expression
   *
   * @param strChamp Nom du champ ou expression sql à traiter
   * @return Retourne une chaine : l'expression SQL associée
   */
  abstract public function getUpperCase($strChamp);
  
  
  /**
   *  Retourne l'expression SQL qui correspond à la fonction cast
   *
   * @param strValue Nom du champ ou expression sql à traiter
   * @param strType  Type du champ ou expression sql à traiter
   * @return Retourne une chaine : l'expression SQL associée
   */
  abstract public function getCast($strValue, $strType);

  /**
   *  Met a jour le rang des informations d'une table avant une suppression
   *
   * @param strTableName    Nom de la table
   * @param strFieldRank    Nom du champ rang
   * @param strWhereUpdate  Filtre SQL pour modifier le rang
   * @param strWhereSelect  Filtre SQL pour retrouver le rang de l'information à supprimer
   * @return Retourne un booleen : toujours true
   */
  public function updateRankBeforeDel($strTableName, $strFieldRank, $strWhereUpdate, $strWhereSelect)
  {
    $iRank = pow(2, 31);
    $strSql = "select ".$strFieldRank." from ".$strTableName." where ".$strWhereSelect;
    $dsRank = $this->initDataSet($strSql);
    if( $drRank = $dsRank->getRowIter() ) {
      $iRank = $drRank->getValueName($strFieldRank);  
    } 
    
    $strSql = "update ".$strTableName." set ".$strFieldRank."=".$strFieldRank."-1 where ".$strWhereUpdate." and ".$strFieldRank.">".$iRank;
    $this->executeSql($strSql);
  }

  /**
   * Met a jour le champ rang d'une table en fonction des parametres
   * 
   * @param strTableName   Nom de la table
   * @param strFieldRank   Nom du champ rang
   * @param iNewRank       Indice du nouveau rang
   * @param bAdd           =true si ajout, false si suppression
   * @param strWhere   Condition supplementaire pour la selection du rang
   */
  public function updateRank($strTableName, $strFieldRank, $iNewRank, $bAdd, $strWhere)
  {
    $bRes = true;
    $bExist = true;
    $strSign = "+";
    $strComp = ">=";
    if( $bAdd == false ) { $strSign = "-"; $strComp=">"; }
    if( $strWhere == "" ) $strWhere = "1=1";
    if( $bAdd == true ) {
      $strSql = "select ".$strFieldRank." from ".$strTableName." where ".$strWhere." and ".$strFieldRank."=".$iNewRank;
      $dsRg = $this->initDataset($strSql);
      if( $dsRg->isEndOfFile() )
        $bExist = false;
    }
    if( $bExist == true ) {
      $strSql = "update ".$strTableName." set ".$strFieldRank."=".$strFieldRank.$strSign."1 where ".
        $strWhere." and ".$strFieldRank.$strComp.$iNewRank;
      $this->executeSql($strSql);
    }
  }
  
  /**
   * Retourne l'indice de rang suivant
   *
   * @param strTable   Nom de la table
   * @param strChamp   Nom du champ
   * @param strWhere   Condition supplementaire pour la selection du rang
   * @return Retourne un entier
   */
  public function getNextRank($strTableName, $strFieldRank, $strWhere)
  {
    $iRank = 1;
    $strSql = "select ".$this->compareSql("max(".$strFieldRank.")", "is", "NULL", "1", "max(".$strFieldRank.")+1")." as MAX_RG".
      " from ".$strTableName.
      ( $strWhere != "" ? " where ".$strWhere : "" );
      
    $dsRk = $this->initDataset($strSql);
    if( $drRk = $dsRk->getRowIter() ) {
      $iRank = $drRk->getValueName("MAX_RG");
    }

    return $iRank;
  }

  /**  
   * Permute les rangs de 2 occurences
   * @param strTableName   Nom de la table
   * @param strFieldRank   Nom du champ rang
   * @param iRank          Indice du rang actuel de la donnée
   * @param iDelta         entier : +1 pour descendre, -1 pour monter
   * @param strWhereGroup  Condition pour filtrer les données d'un même ensemble pour lequel le rang est utilisé (pas de and avant et après l'expression)
   * @param strWhereData   Condition complémentaire à strWhereGroup pour retrouver le rang de l'information à modifier (pas de and avant et après l'expression)
   *   */
  public function switchRank($strTableName, $strFieldRank, $iRank, $iDelta, $strWhereGroup, $strWhereData)
  {
    $iNewRank = $iRank + $iDelta;
    if( $iNewRank < 1 ) return;
 
    // met à jour le rang de la donnée permutée   
    $strSql = "update ".$strTableName.
      " set ".$strFieldRank."=".$iRank.
      " where ".$strWhereGroup." and ".$strFieldRank."=".$iNewRank;
    $this->executeSql($strSql);
    
    // met à jour le rang de la donnée sélectionnée
    $strSql = "update ".$strTableName.
      " set ".$strFieldRank."=".$iNewRank.
      " where ".$strWhereGroup." and ".$strWhereData;
    $this->executeSql($strSql);
  }

  /**
   *  Retourne une chaine contenant la fonctionnalite Oracle de comparaison
   *        de chaine sans tenir compte des caracteres francais (accent, etc...)
   *
   * @param strChamp Nom du champ de la table
   * @param strOp    Operateur de test SQL : like, =
   * @param strVal   Chaine de comparaison qui doit etre traitee par ora_analyseSQL auparavant
   * @return Retourne la chaine après traitement
   */
  abstract public function getStrConvert2ASCII7($strChamp, $strOp, $strVal);


  /**
   *  Enleve les doublons d'une liste séparée par une virgule
   *
   * @param strListId  liste d'identifiants séparés par une virgule
   * @return Retourne un string : chaine sans les doublons
   */
  public function delDoublon($strListId)
  {
    $tabId = explode(",", $strListId);
    $tabNewId = array_unique($tabId);
    $strListId = implode(",", $tabNewId);
    return $strListId;
  }
  
  /**
   * Traduit le type de colonne fourni dans la syntaxe du SGBD et y ajoute la longueur de champ si fournie
   * Retourne la traduction en cas de succès
   * Retourne false en cas d'erreur (longueur non fournie mais nécessaire, type incorrect, ...)
   *
   * @param columnType      Type de donnée de colonne
   * @param columnLength    Longueur max des données de la colonne
   * @return string, retourne FALSE en cas d'erreur
   */
  abstract public function getColumnType($columnType, $columnLength="");
  
  
  /**
   * Convertit les caractères CP-1252 vers des caractères interprétables en UTF-8 ou ISO-8859-1
   * @param string  Chaine en entrée
   * @return string Chaine décodée sur les caractères CP-1252
   */
  public function convertCharactersCp1252($string) 
  {
    $res = strtr($string, array(
      mb_convert_encoding("\x80", ALK_HTML_ENCODING, "cp1252") => "€",    
      mb_convert_encoding("\x81", ALK_HTML_ENCODING, "cp1252") => " ",    
      mb_convert_encoding("\x82", ALK_HTML_ENCODING, "cp1252") => "'", 
      mb_convert_encoding("\x83", ALK_HTML_ENCODING, "cp1252") => 'f',
      mb_convert_encoding("\x84", ALK_HTML_ENCODING, "cp1252") => '"',  
      mb_convert_encoding("\x85", ALK_HTML_ENCODING, "cp1252") => "...",  
      mb_convert_encoding("\x86", ALK_HTML_ENCODING, "cp1252") => "+", 
      mb_convert_encoding("\x87", ALK_HTML_ENCODING, "cp1252") => "#",
      mb_convert_encoding("\x88", ALK_HTML_ENCODING, "cp1252") => "^",  
      mb_convert_encoding("\x89", ALK_HTML_ENCODING, "cp1252") => "0/00", 
      mb_convert_encoding("\x8A", ALK_HTML_ENCODING, "cp1252") => "S", 
      mb_convert_encoding("\x8B", ALK_HTML_ENCODING, "cp1252") => "<",
      mb_convert_encoding("\x8C", ALK_HTML_ENCODING, "cp1252") => "OE", 
      mb_convert_encoding("\x8D", ALK_HTML_ENCODING, "cp1252") => " ",    
      mb_convert_encoding("\x8E", ALK_HTML_ENCODING, "cp1252") => "Z", 
      mb_convert_encoding("\x8F", ALK_HTML_ENCODING, "cp1252") => " ",
      mb_convert_encoding("\x90", ALK_HTML_ENCODING, "cp1252") => " ",  
      mb_convert_encoding("\x91", ALK_HTML_ENCODING, "cp1252") => "`",    
      mb_convert_encoding("\x92", ALK_HTML_ENCODING, "cp1252") => "'", 
      mb_convert_encoding("\x93", ALK_HTML_ENCODING, "cp1252") => '"',
      mb_convert_encoding("\x94", ALK_HTML_ENCODING, "cp1252") => '"',  
      mb_convert_encoding("\x95", ALK_HTML_ENCODING, "cp1252") => "*",    
      mb_convert_encoding("\x96", ALK_HTML_ENCODING, "cp1252") => "-", 
      mb_convert_encoding("\x97", ALK_HTML_ENCODING, "cp1252") => "--",
      mb_convert_encoding("\x98", ALK_HTML_ENCODING, "cp1252") => "~",  
      mb_convert_encoding("\x99", ALK_HTML_ENCODING, "cp1252") => "(TM)", 
      mb_convert_encoding("\x9A", ALK_HTML_ENCODING, "cp1252") => "s", 
      mb_convert_encoding("\x9B", ALK_HTML_ENCODING, "cp1252") => ">",
      mb_convert_encoding("\x9C", ALK_HTML_ENCODING, "cp1252") => "oe", 
      mb_convert_encoding("\x9D", ALK_HTML_ENCODING, "cp1252") => " ",    
      mb_convert_encoding("\x9E", ALK_HTML_ENCODING, "cp1252") => "z", 
      mb_convert_encoding("\x9F", ALK_HTML_ENCODING, "cp1252") => "Y"));
    return $res; 
  }
}
?>