<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkerror.class.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkErrorPgsql
 * @brief Permet la gestion des erreurs des scripts
 */
class AlkErrorPgsql extends AlkError { }

/**
 *  Définit le gestionnaire d'erreur spécifique alkanet
 *
 * @param no   Numéro de l'erreur
 * @param str  Type d'erreur
 * @param file Nom du fichier
 * @param line Numéro de la ligne
 * @param ctx  Contexte de l'erreur
 */
function ErrorHandlerPgsql($no, $str, $file, $line, $ctx) 
{  
  if( defined("ALK_B_LOG") && ALK_B_LOG==true ) {
    $objError = new AlkErrorPgsql(ERROR_GEST, MAIL_DEST, FILE_PATH, FILE_NAME);
  
    switch( $no ) {
    case E_ERROR :
      $objError->_errError($no, $str, $file, $line, $ctx);
      break;
    case E_WARNING : 
      $objError->_errWarning($no, $str, $file, $line, $ctx);
      break;
    case E_PARSE :
      $objError->_errParse($no, $str, $file, $line, $ctx);
      break;
    case E_NOTICE :
      $objError->_errNotice($no, $str, $file, $line, $ctx);
      break;
    case E_CORE_ERROR :
      $objError->_errCoreError($no, $str, $file, $line, $ctx);
      break;
    case E_CORE_WARNING :
      $objError->_errCoreWarning($no, $str, $file, $line, $ctx);
      break;
    case E_COMPILE_ERROR :
      $objError->_errCompileError($no, $str, $file, $line, $ctx);
      break;
    case E_COMPILE_WARNING :
      $objError->_errCompileWarning($no, $str, $file, $line, $ctx);
      break;
    case E_USER_ERROR :
      $objError->_errUserError($no, $str, $file, $line, $ctx);
      break;
    case E_USER_WARNING :
      $objError->_errUserWarning($no, $str, $file, $line, $ctx);
      break;
    case E_USER_NOTICE :
      $objError->_errUserNotice($no, $str, $file, $line, $ctx);
      break;
    default:
      break;
    }
  }
}  

/**
 *  Capte toutes les erreurs php
 */
function startErrorHandlerPgsql()
{
  if( defined("ALK_B_LOG") && ALK_B_LOG==true ) {
    set_error_handler('ErrorHandlerPgsql');
  }
}

/**
 *  Restaure l'ancien gestionnaire
 */
function endErrorHandlerPgsql()
{
  if( defined("ALK_B_LOG") && ALK_B_LOG==true ) {
    restore_error_handler();
  }
}
?>