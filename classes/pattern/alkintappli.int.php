<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


/**
 * @package Alkante_Class_Pattern
 * 
 * @interface AlkIntAppli
 * @brief Interface liée aux applications fournissant les méthodes à implémenter
 *        pour la gestion courante : ajout / modif / suppr / déplacement / duplication
 * @note La gestion des droits est assurée par l'interface AlkIntRights
 */
interface AlkIntAppli
{

  /**
   *  Méthode appelée après création puis ajout de l'application à un espace.
   *        Implémenter les actions à effectuer pour initialiser l'application
   * @param tabFields tableau associatif des champs nécessaires à la création d'une appli
   * @return int
   */
  public function addAppli($tabFields);

  /**
   *  Méthode appelée après modification des propriétés d'une application à un espace.
   * @param tabFields tableau associatif des champs nécessaires à la création d'une appli
   */
  public function updateAppli($tabFields);

  /**
   *  Méthode appelée avant suppression définitive d'une application d'un espace
   *        Implémenter les actions effectuant le ménage interne à l'application
   */
  public function delAppli();

  /**
   *  Déplacement d'une application d'un espace vers un autre
   *        Cette méthode se charge d'appelé la recopie des droits
   * @param appli_id  identifiant de l'appli
   * @param idContTo  identifiant de l'espace recevant l'appli
   */
  public function moveAppli($idContTo);

  /**
   * Duplication de la structure et du paramètrage de l'application src à la courante
   * @param appli_id_src  identifiant de l'application source
   * @param iTypeDup      type de duplication : 0 = application vierge (ne fait rien) 
   *                                            1 = structure et paramétrage
   *                                            2 = structure, paramétrage et données  
   */
  public function dupAppli($appli_id_src, $iTypeDup);

}

?>