<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkds.class.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkdrldap.class.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkDsLdap
 * @brief DataSet basé sur Ldap, hérite de la classe AlkDs
 */
final class AlkDsLdap extends AlkDs
{
  /** Objet contenant les données du dataset */
  protected $oRes;
  
  /** tableau d'association entre les champs ldap et alkanet */
  protected $tabLdapAssoc;

  /** mémorise l'état de connexion */
  protected $bConnected;

  /**
   *  Constructeur par défaut de la classe
   *
   * @param conn          handle de la connexion en cours
   * @param iRes          handle de la ressource ldap (read, search ou list)
   * @param idFirst       indice de début pour la pagination (=0 par défaut)
   * @param idLast        indice de fin pour la pagination (=-1 par défaut)
   * @param bErr          true si gestion des erreurs, false pour passer sous silence l'erreur
   * @param strDbEncoding encodage du client sgbd
   */
  public function __construct($conn, $iRes=null, $idFirst=0, $idLast=-1, $bErr=true, $strDbEncoding=ALK_LDAP_ENCODING)
  {
    parent::__construct($strDbEncoding);
    
    if( is_null($conn) && is_null($iRes) ) {
      $this->bConnected = false;
      return;
    }

    $this->bConnected = true;
    
    if( $bErr == false )
      ob_start();

    if (defined("ALK_LDAP_ASSOC_ANNU"))
    	$this->tabLdapAssoc = unserialize(ALK_LDAP_ASSOC_ANNU);
   	else
   		$this->tabLdapAssoc = array();

    $this->iCountDr = 0;
    $this->bEof = true;
    $this->iCurDr = -1;
    $this->tabDr = array();
    $this->tabType = array();

    if( !$iRes ) {
      $this->tabDr = array();
      $this->iCountDr = 0;
      $this->iCountTotDr = 0;
    } else {
      // récupération des infos ldap
      $tabRes = ldap_get_entries($conn, $iRes);
  
      if( array_key_exists("count", $tabRes) ) {
        for($i=0; $i<$tabRes["count"]; $i++) {
          $tabLdapField = $tabRes[$i];       
          if( array_key_exists("count", $tabLdapField) ) {
            for($j=0; $j<$tabLdapField["count"]; $j++) {
              $strLdapField = $tabLdapField[$j];
              $strFieldAnnu = array_key_exists($strLdapField, $this->tabLdapAssoc) ? $this->tabLdapAssoc[$strLdapField]["field"] : $strLdapField;
              if (count($tabLdapField[$strLdapField]) > 2) {
                $this->tabDr[$i][$strFieldAnnu] = $tabLdapField[$strLdapField];
              } else {
                $this->tabDr[$i][$strFieldAnnu] = $tabLdapField[$strLdapField][0];
              } 
            }
          }
          if( array_key_exists("dn", $tabLdapField) ) {
            $strLdapField = "dn";
            $strFieldAnnu = array_key_exists($strLdapField, $this->tabLdapAssoc) ? $this->tabLdapAssoc[$strLdapField]["field"] : $strLdapField;
            $this->tabDr[$i][$strFieldAnnu] = $tabLdapField[$strLdapField];
          }
        }
  
        $this->iCountDr = $tabRes["count"];
        $this->iCountTotDr = $tabRes["count"];
      }
  
      if( $this->iCountDr > 0 ) {
        $this->iCurDr = 0;
        $this->bEof = false;
      } else {
        $this->tabDr = array();
        $this->iCountDr = 0;
        $this->iCountTotDr = 0;
      }
    }
    if( $bErr == false )
      ob_end_clean();
  }
  
  /**
  * destructeur par défaut
  */
  public function __destruct() 
  {
  }
  
  /**
   * Retourne vrai si la connexion a été ouverte avec succès avant l'exécution de la requête
   * @return boolean
   */
  public function isConnected()
  {
    return $this->bConnected; 
  }
    
  /**
   *  Retourne le datarow (dr) correspondant à l'indice courant ($iCurDr)
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  public function getRow()
  {
    $drRes = false;
      
    if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drLdapTmp = $this->tabDr[$this->iCurDr];
      $drRes = new AlkDrLdap($drLdapTmp, $this);
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
    
    return $drRes;
  }

  /**
   *  Retourne le datarow (dr) correspondant à l'indice courant ($iCurDr) puis passe au suivant
   *        Retourne false si EOF est atteint
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  public function getRowIter()
  {
    $drRes = false;
    if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drLdapTmp = $this->tabDr[$this->iCurDr];
      $this->iCurDr++;
      $drRes = new AlkDrLdap($drLdapTmp, $this);
    }

    return $drRes;
  }
    
  /**
   *  Retourne le datarow (dr) positionné à l'indice id
   *        Retourne false si l'indice n'est pas satisfaisant
   *         n'agit ni sur bEOF, ni sur le pointeur 
   *
   * @param id Indice du dataRow
   * @return Retourne un dataRow si ok, false sinon
   */
  public function getRowAt( $id )
  {
    $drRes = false;      
    if( $this->iCountDr>0 && $id>=0 && $id<$this->iCountDr ){
      $drLdapTmp = $this->tabDr[$id];
      $drRes = new AlkDrLdap($drLdapTmp, $this);
    }
  
    return $drRes;
  }

  /**
   *  Place le pointeur du dataset sur le premier datarow
   *        Met à jour l'indice courant du dataset
   */
  public function moveFirst()
  {
    if( $this->iCountDr > 0 ) {
      $this->iCurDr = 0;
      $this->bEof = false;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Place le pointeur du dataset sur le datarow précédent
   *        Met à jour l'indice courant du dataset
   */
  public function movePrev()
  {
    if( $this->iCountDr>0 && $this->iCurDr>0 ) {
      $this->iCurDr--;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Place le pointeur du dataset sur le datarow suivant
   *        Met à jour l'indice courant du dataset
   */
  public function moveNext()
  {
    if( $this->iCountDr>0 && $this->iCurDr<$this->iCountDr ) {
      $this->iCurDr++;
      
      if( $this->iCurDr >= $this->iCountDr ) {
        $this->bEof = true;
      }
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }


  /**
   *  Place le pointeur du dataset sur le dernier datarow
   *        Met à jour l'indice courant du dataset
   */
  public function moveLast( )
  {
    if ( $this->iCountDr > 0 ) {
      $this->iCurDr = $this->iCountDr-1;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Place le pointeur du dataset sur le (iCurDr+iNb) ième datarow
   *        Met à jour l'indice courant du dataset
   *
   * @param iNb Décalage positive ou négatif par rapport au dataRow courant
   */
  public function move($iNb)
  {
    if( $this->iCountDr > 0 && $this->iCurDr+$iNb>=0 && $this->iCurDr+$iNb<$this->iCountDr ) {
      $this->iCurDr += $iNb;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Détruit l'objet dataset()
   */
  public function close()
  {
    $this->oRes = "";
  }
    
  /**
   *  Tri le dataSet après lecture dans la source donnée
   *        Effectue un tri à plusieurs niveaux
   *        Nécessite de connaite les noms de champ : ID et ID_PERE
   *        le dataset doit être déjà trié par niv puis par rang
   *
   * @param strFieldId  Nom du champ ID dans la requeête
   * @param strFieldIdp Nom du champ ID_PERE dans la requête
   */
  public function setTree($strFieldId, $strFieldIdp)
  {
    $tabNoeud = array();   // contient l'ensemble des noeuds
    $tabIndex = array();   // contient l'index inverse id -> cpt
    $stack = array();
    
    for ($i=0; $i< $this->iCountTotDr; $i++) {
      //$drMysqltmp = mysql_fetch_array($this->oRes);
      $drLdapTmp = $this->tabDr[$i];
      
      $idFils = $drLdapTmp[mb_strtoupper($strFieldId)];
      $idPere = $drLdapTmp[mb_strtoupper($strFieldIdp)];
      $tabNoeud[$i]["FILS"] = array();
      $tabNoeud[$i]["ID"] = $idFils;
      $tabNoeud[$i]["OK"] = false;

      // memorize the first level id in a stack
      array_push($stack, $i);

      // met a jour la table index secondaire
      $tabIndex[$idFils] = $i;

      // ajoute l'id fils a l'id pere
      if( $idPere > 0 )
        if( array_key_exists($idPere, $tabIndex) == true ) {
          $iPere = $tabIndex[$idPere];
          array_push($tabNoeud[$iPere]["FILS"], $i);
        }
    }

    // parse the sons
    $tabDr2 = array();
    $j = 0;
    while( count($stack) > 0 ) {
      $i = array_shift($stack);
      if( $tabNoeud[$i]["OK"] == false ) {
        $id = $tabNoeud[$i]["ID"];
        $tabNoeud[$i]["OK"] = true;
        $tabDr2[$j] = $this->tabDr[$i];
        $j++;
        
        $nbFils = count($tabNoeud[$i]["FILS"]);
        for ($k=$nbFils-1; $k>=0; $k--) {
          $if = $tabNoeud[$i]["FILS"][$k];
          array_unshift($stack, $if);
        }
      }
      $i++;
    }

    $tabNoeud = null;
    $tabIndex = null;
    $this->tabDr = $tabDr2;
  }
}
?>