<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Class::Pattern
Module fournissant les classes de base Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkds.class.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkdrmysql.class.php");
require_once(ALK_ALKANET_ROOT_PATH."classes/pattern/alkerrormysql.class.php");

/**
 * @package Alkanet_Class_Pattern
 * 
 * @class AlkDsMysql
 * @brief DataSet basé sur Mysql, hérite de la classe AlkDs
 */
final class AlkDsMySql extends AlkDs
{
  /** Objet contenant les données du dataset */
  protected $oRes;

  /**
   *  Constructeur par défaut de la classe
   *
   * @param conn          handle de la connexion en cours
   * @param strSQL        requête sql
   * @param idFirst       indice de début pour la pagination (=0 par défaut)
   * @param idLast        indice de fin pour la pagination (=-1 par défaut)
   * @param bErr          true si gestion des erreurs, false pour passer sous silence l'erreur
   * @param strDbEncoding encodage du client sgbd
   */
  public function __construct($conn, $strSQL, $idFirst=0, $idLast=-1, $bErr=true, $strDbEncoding=ALK_SGBD_ENCODING)
  {
    parent::__construct($strDbEncoding);

    if( $bErr == true )
      startErrorHandlerMysql();
    else
      ob_start();

    $this->iCountDr = 0;
    $this->bEof = true;
    $this->iCurDr = -1;
    $this->tabDr = array();
    $this->tabType = array();

    // ajout du code spécifique mysql pour la pagination
    $strSql = $strSQL;
    if( $idLast >= $idFirst ) {
      $strLimit = " LIMIT ".$idFirst.", ".($idLast-$idFirst+1);
      $strSql = $strSQL.$strLimit;
    
    
      // Sur une requete de type select : 
      // utilisation du mot-clé SQL_CALC_FOUND_ROWS mémorisant
      // le nombre réel de résultats qui seraient retournés sans utilisation de limites
      $bFoundRows = false;
      $bSelect = preg_match("!^(\s*SELECT)!si", $strSql);
      if ( $bSelect ){
        $strSql = preg_replace( "!^(\s*SELECT)!si", "$1 SQL_CALC_FOUND_ROWS", $strSql);
        $bFoundRows = !( strpos($strSql, "SQL_CALC_FOUND_ROWS") === false );
      }
    
      if ( $bSelect && !$bFoundRows ) {
        $strErr = "dsMysql[".__LINE__."] - Erreur requête (".$strSql."," .
          " DsMysql : Impossible de calculer le nombre total de résultat sur votre requête" .
          " [Structure requête incorrecte ; Pas d'ajout du mot clé SQL_CALC_FOUND_ROWS])";
        if( $bErr == true )
          trigger_error($strErr, E_USER_ERROR);
        else
          $this->_SetError($strErr);
      }
    }
    $this->oRes = mysql_query($strSql, $conn);
      
    if( mysql_errno() == 0 ) {
      $nbFields = mysql_num_fields($this->oRes);
      for($i=0; $i<$nbFields; $i++) {
        $this->tabType[$i] = mysql_field_type($this->oRes, $i);
      }
      $this->iCountDr = mysql_num_rows($this->oRes);
      if( mysql_errno() != 0 ) {
        if( $bErr == true )
          trigger_error("dsMysql - Erreur requête (".$strSQL.", MySql : ".
                        mysql_errno().", ".mysql_error().")",E_USER_ERROR);
        else
          $this->_SetError(mysql_error());
      }
      $this->iCountTotDr = $this->iCountDr;
      
      if( $this->iCountDr > 0 ) {
        $this->iCurDr = 0;
        $this->bEof = false;
        for ($i=0; $i<$this->iCountDr; $i++){
          $this->tabDr[$i] = mysql_fetch_array($this->oRes);
          if( mysql_errno() != 0 ) {
            if( $bErr == true ) {
              trigger_error("DsMySql[".__LINE__."] - mysql_fetch_array error (Indice :".
                            $this->oRes." , MySql : ".mysql_errno().", ".
                            mysql_error().")", E_USER_ERROR);
            } else {
              $this->_SetError(mysql_error());
            }
          }
        }

        if( $idLast >= $idFirst ) {
          $strSqlTot = "SELECT FOUND_ROWS()";
          $oRes = mysql_query($strSqlTot);
          if( mysql_errno() != 0 ){
            if( $bErr == true ) {
              trigger_error("dsMysql[".__LINE__."] - Erreur requête (".$strSqlTot.", MySql : ".
                            mysql_errno().", ".mysql_error().")",E_USER_ERROR);
            } else {
              $this->_SetError(mysql_error());
            }
          } else {
            $tabTotDr = mysql_fetch_row($oRes);        
            if( mysql_errno() != 0 ){
              if( $bErr == true ) {
                trigger_error("DsMySql[".__LINE__."] - mysql_fetch_row error (Indice :".
                              $oRes." , MySql : ".mysql_errno().", ".
                              mysql_error().")",E_USER_ERROR);
              } else {
                $this->_SetError(mysql_error());
              }
            } else {
              $this->iCountTotDr = $tabTotDr[0];
            }
          }
        }
      }
    } else {
      if( $bErr == true ) {
        trigger_error("dsMysql - Erreur requête (".$strSQL.", MySql : ".
                      mysql_errno().", ".mysql_error().")",E_USER_ERROR);
      } else {
        $this->_SetError(mysql_error());
      }
    }

    $tabTmp = array();
    foreach($this->tabDr as $i=>$tabDr) {
      foreach($tabDr as $key=>$value) {
        $tabTmp[$i][mb_strtoupper($key)] = $value;
      }
    }
    $this->tabDr = $tabTmp;

    if( $this->iCountDr > 0 ) {
      $this->iCurDr = 0;
      $this->bEof = false;
    } else {
      $this->tabDr = array();
      $this->iCountDr = 0;
      $this->iCountTotDr = 0;
    }
    
    if( $bErr == true )
      endErrorHandlerMysql();
    else
      ob_end_clean();
  }
  
  /**
  * destructeur par défaut
  */
  public function __destruct() 
  {
  }
  
  
   /**
   *  Retourne le datarow (dr) correspondant à l'indice courant ($iCurDr)
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  public function getRow()
  {
    $drRes = false;
      
    if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drMysqltmp = $this->tabDr[$this->iCurDr];
      $drRes = new AlkDrMySql($drMysqltmp, $this);
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
    
    return $drRes;
  }

  /**
   *  Retourne le datarow (dr) correspondant à l'indice courant ($iCurDr) puis passe au suivant
   *        Retourne false si EOF est atteint
   *
   * @return Retourne un dataRow si ok, false sinon
   */
  public function getRowIter()
  {
    $drRes = false;
    if( $this->iCurDr>=0 && $this->iCurDr<$this->iCountDr ) {
      $drMysqltmp = $this->tabDr[$this->iCurDr];
      $this->iCurDr++;
      $drRes = new AlkDrMySql($drMysqltmp, $this);
    }

    return $drRes;
  }
    
  /**
   *  Retourne le datarow (dr) positionné à l'indice id
   *        Retourne false si l'indice n'est pas satisfaisant
   *         n'agit ni sur bEOF, ni sur le pointeur 
   *
   * @param id Indice du dataRow
   * @return Retourne un dataRow si ok, false sinon
   */
  public function getRowAt( $id )
  {
    $drRes = false;      
    if( $this->iCountDr>0 && $id>=0 && $id<$this->iCountDr ){
      $drMysqltmp = $this->tabDr[$id];
      $drRes = new AlkDrMySql($drMysqltmp, $this);
    }
  
    return $drRes;
  }

  /**
   *  Place le pointeur du dataset sur le premier datarow
   *        Met à jour l'indice courant du dataset
   */
  public function moveFirst()
  {
    if( $this->iCountDr > 0 ) {
      $this->iCurDr = 0;
      $this->bEof = false;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Place le pointeur du dataset sur le datarow précédent
   *        Met à jour l'indice courant du dataset
   */
  public function movePrev()
  {
    if( $this->iCountDr>0 && $this->iCurDr>0 ) {
      $this->iCurDr--;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Place le pointeur du dataset sur le datarow suivant
   *        Met à jour l'indice courant du dataset
   */
  public function moveNext()
  {
    if( $this->iCountDr>0 && $this->iCurDr<$this->iCountDr ) {
      $this->iCurDr++;
      
      if( $this->iCurDr >= $this->iCountDr ) {
        $this->bEof = true;
      }
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }


  /**
   *  Place le pointeur du dataset sur le dernier datarow
   *        Met à jour l'indice courant du dataset
   */
  public function moveLast( )
  {
    if ( $this->iCountDr > 0 ) {
      $this->iCurDr = $this->iCountDr-1;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Place le pointeur du dataset sur le (iCurDr+iNb) ième datarow
   *        Met à jour l'indice courant du dataset
   *
   * @param iNb Décalage positive ou négatif par rapport au dataRow courant
   */
  public function move($iNb)
  {
    if( $this->iCountDr > 0 && $this->iCurDr+$iNb>=0 && $this->iCurDr+$iNb<$this->iCountDr ) {
      $this->iCurDr += $iNb;
    } else {
      $this->iCurDr = -1;
      $this->bEof = true;
    }
  }

  /**
   *  Détruit l'objet dataset()
   */
  public function close()
  {
    $this->oRes = "";
  }
    
  /**
   *  Tri le dataSet après lecture dans la source donnée
   *        Effectue un tri à plusieurs niveaux
   *        Nécessite de connaite les noms de champ : ID et ID_PERE
   *        le dataset doit être déjà trié par niv puis par rang
   *
   * @param strFieldId  Nom du champ ID dans la requeête
   * @param strFieldIdp Nom du champ ID_PERE dans la requête
   */
  public function setTree($strFieldId, $strFieldIdp)
  {
    $tabNoeud = array();   // contient l'ensemble des noeuds
    $tabIndex = array();   // contient l'index inverse id -> cpt
    $stack = array();
    
    for ($i=0; $i< $this->iCountTotDr; $i++) {
      //$drMysqltmp = mysql_fetch_array($this->oRes);
      $drMysqltmp = $this->tabDr[$i];
      
      $idFils = $drMysqltmp[mb_strtoupper($strFieldId)];
      $idPere = $drMysqltmp[mb_strtoupper($strFieldIdp)];
      $tabNoeud[$i]["FILS"] = array();
      $tabNoeud[$i]["ID"] = $idFils;
      $tabNoeud[$i]["OK"] = false;

      // memorize the first level id in a stack
      array_push($stack, $i);

      // met a jour la table index secondaire
      $tabIndex[$idFils] = $i;

      // ajoute l'id fils a l'id pere
      if( $idPere > 0 )
        if( array_key_exists($idPere, $tabIndex) == true ) {
          $iPere = $tabIndex[$idPere];
          array_push($tabNoeud[$iPere]["FILS"], $i);
        }
    }

    // parse the sons
    $tabDr2 = array();
    $j = 0;
    while( count($stack) > 0 ) {
      $i = array_shift($stack);
      if( $tabNoeud[$i]["OK"] == false ) {
        $id = $tabNoeud[$i]["ID"];
        $tabNoeud[$i]["OK"] = true;
        $tabDr2[$j] = $this->tabDr[$i];
        $j++;
        
        $nbFils = count($tabNoeud[$i]["FILS"]);
        for ($k=$nbFils-1; $k>=0; $k--) {
          $if = $tabNoeud[$i]["FILS"][$k];
          array_unshift($stack, $if);
        }
      }
      $i++;
    }

    $tabNoeud = null;
    $tabIndex = null;
    $this->tabDr = $tabDr2;
  }
}
?>