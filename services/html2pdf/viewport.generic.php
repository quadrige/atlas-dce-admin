<?php
// $Header: /cvsroot/html2ps/viewport.fpdf.php,v 1.3 2005/10/17 16:04:25 Konstantin Exp $

class ViewportGeneric {
  function draw_page_border() {
    $this->setlinewidth(1);
    $this->setrgbcolor(0,0,0);

    $this->moveto($this->left, $this->bottom + $this->offset);
    $this->lineto($this->left, $this->bottom + $this->height + $this->offset);
    $this->lineto($this->left + $this->width, $this->bottom + $this->height + $this->offset);
    $this->lineto($this->left + $this->width, $this->bottom + $this->offset);
    $this->closepath();
    $this->stroke();
  }

  function rect($x, $y, $w, $h) { 
    $this->moveto($x, $y);
    $this->lineto($x + $w, $y);
    $this->lineto($x + $w, $y + $h);
    $this->lineto($x, $y + $h);
    $this->closepath();
  }

  function setup_clip() {
    $this->moveto($this->left, $this->bottom + $this->offset);
    $this->lineto($this->left, $this->bottom + $this->height + $this->offset);
    $this->lineto($this->left + $this->width, $this->bottom + $this->height + $this->offset);
    $this->lineto($this->left + $this->width, $this->bottom + $this->offset);
    $this->clip();
  }
}
?>