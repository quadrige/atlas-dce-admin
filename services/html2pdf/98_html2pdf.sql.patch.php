<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

$strNum = "3.2.1";
$tabIns[$strNum][] = "";

/**
 * PCT : modif de alk_html2ps.php : suppression du chargement du lib_session.php
 */
$strNum = "3.2.2";
$tabIns[$strNum][] = "";

/**
 * MLA : ./pipeline.class.php : ajout des fonctions pushCSS et popCSS qui sont utilisées dans box.[i]frame.php (et non push/pop_css)
 */
$strNum = "3.2.3";
$tabIns[$strNum][] = "";

/**
 * MLA : css.border.color.inc.php : correction sur retour de valeur mal typé
 */
$strNum = "3.2.4";
$tabIns[$strNum][] = "";

/**
 * MLA : fetcher.url.curl.class.php : instruction curl pour ignorer la vérification PEER lors des lectures de pages en HTTPS
 *** TAG *** 
 */
$strNum = "3.2.5";
$tabIns[$strNum][] = "";

/**
 * PCT : config.inc.php : Modif pour ne pas intégrer les fichiers de polices dans le pdf
 *       Modif pour permettre la génération en local avec une source html contenant des url en file://
 *     : Ajout du fichier alkcli_html2pdf.php à appeler pour la génération en mode cli
 */
$strNum = "3.2.6";
$tabIns[$strNum][] = "";

/**
 * LGD : modif du fichier alkcli_html2pdf.php pour éviter pb alloc mémoire
 *     : ini_set("pcre.backtrack_limit",1000000); 
 *     : modif pour pouvoir faire passer des balises html de type img.. comme footerhtml et headerhtml pour créer des entêtes dans html2pdf 
 *     : alkhtml2pdf.class : réalisation d'un encode param pour faire passe le strHtml ex. "alktoken".AlkRequest::getEncodeParam($strParamValue); 
 *     : dans alkcli_html2ps.php et alk_html2ps réalisation d'un decodeparam pour restituer le strHtml. ex. $data = AlkRequest::decodeValue(substr($data, 8));
 */
$strNum = "3.2.7";
$tabIns[$strNum][] = "";

/**
 * MTO : ajout police arialuni.ttf
 */
$strNum = "3.2.8";
$tabIns[$strNum][] = "";


?> 
