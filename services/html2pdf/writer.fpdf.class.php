<?php 
// $Header: /cvsroot/html2ps/writer.fpdf.class.php,v 1.2 2005/10/15 10:47:15 Konstantin Exp $

define('FPDF_PATH','./fpdf/');

include('fpdf/fpdf.php');
include('fpdf/font/makefont/makefont.php');

class FPDFWriter extends PSWriter {
  var $pdf;

  function FPDFWriter($file_name, $version) {
    if (!class_exists('FPDF')) {
      readfile('templates/missing_fpdf.html');
      error_log("No FPDF class found");
      die();
    };

    // Call base-class constructor;
    $this->PSWriter($file_name);
    $this->file_name = $file_name;    
  }

  function create($compressmode, $version) {
    $filename = PSWriter::mk_filename();
    $method = new FPDFWriter($filename, $version);
    return $method;
  }

  function get_viewport($g_media) {
    $this->pdf = new FPDF("P","pt",array(mm2pt($g_media->width()), mm2pt($g_media->height())));

    global $g_font_resolver_pdf;
    foreach ($g_font_resolver_pdf->ttf_mappings as $typeface => $file) {
      // Remove the '.ttf' suffix
      $file = substr($file, 0, strlen($file) - 4);

      // Generate (if required) PHP font description files
      if (!file_exists($this->pdf->_getfontpath().$file.'.php')) {
        // As MakeFont squeaks a lot, we'll need to capture and discard its output
        ob_start();
        MakeFont(PDFLIB_TTF_FONTS_REPOSITORY.$file.'.ttf',
                 PDFLIB_TTF_FONTS_REPOSITORY.$file.'.afm',
                 $this->pdf->_getfontpath());
        ob_end_clean();
      };
      
      $this->pdf->AddFont($typeface, "", $file.'.php'); 
    };

    return new ViewportFPDF($this->pdf, $g_media);
  }

  function write($data) {  }

  function close() { 
    $this->pdf->Output($this->file_name);
  }

  // The wrting is completed. Dump data to the browser and release file.
  function release() {
    $this->close();

    $this->output->execute(array('pdf','application/pdf'), $this->file_name);
    unlink($this->file_name);
  }
};
?>