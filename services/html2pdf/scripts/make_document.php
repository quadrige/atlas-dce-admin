<?php


/*
* File : 01_page_apercu.php
* Create : 17/09/2004
* Author : AS
* Description : Administration aper�u page
* Parameters :
*/

include_once ("lib_session.php");
include_once ("../../classes/html2pdf/html2fpdf.php");
include_once ("../../classes/html2pdf/alkhtml2pdf.class.php");
include_once ("../../classes/html2pdf/fpdi/fpdi.php");

/** Configuration du service */
print_r($_REQUEST);
$output_directory = Request("output_directory", REQ_POST_GET, urlencode(OUTPUT_DIRECTORY));
$output_method = Request("output_method", REQ_POST_GET, "ps2pdf");
$output_mode = Request("output_mode", REQ_POST_GET, "0", "is_numeric");
$page_number = Request("page_number", REQ_POST_GET, "0", "is_numeric");
$page_sommaire = Request("page_sommaire", REQ_POST_GET, "-1", "is_numeric");
$genere_index = Request("genere_index", REQ_POST_GET, "0", "is_numeric");
$index_regexp = str_replace("\\\\", "\\", Request("index_regexp", REQ_POST_GET, "!!"));
$topmargin = Request("topmargin", REQ_POST_GET, "0", "is_numeric");
$bottommargin = Request("bottommargin", REQ_POST_GET, "0", "is_numeric");
$leftmargin = Request("leftmargin", REQ_POST_GET, "0", "is_numeric");
$rightmargin = Request("rightmargin", REQ_POST_GET, "0", "is_numeric");

$tabConfig = array('URL'           => urlencode($this->strUrlSource),
                   'cssmedia'      => "print",
                   'convert'       => "",
                   'media'         => "A4",
                   'scalepoints'   => "",
                   'renderimages'  => "1",
                   'renderlinks'   => "1",
                   'pixels'        => "800",
                   'landscape'     => "",
                   'method'        => $output_method,
                   'ps2pdf'        => "",
                   'leftmargin'    => $leftmargin,
                   'rightmargin'   => $rightmargin,
                   'topmargin'     => $topmargin,
                   'bottommargin'  => $bottommargin,
                   'encoding'      => "iso-8859-1",
                   'compress'      => "2",
                   'output'        => $output_mode,
                   'pdfversion'    => "1.4",
                   'transparency_workaround' => "",
                   'imagequality_workaround' => "",
                   'pageborder' => "",
                   'debugbox'      => "",
                   'html2xhtml'    => "",
                   'bHote'    => "ALK",
                   'output_file_directory'    => $output_directory
                   );
                             
/** Donnees d'identification du document */
$agent_id = Request("agent_id", REQ_POST_GET, "-1", "is_numeric");
$doc_id = Request("doc_id", REQ_POST_GET, "-1", "is_numeric");
$url_source = Request("url_source", REQ_POST_GET, "");
$doc_titre = Request("doc_titre", REQ_POST_GET, "");

$fileName = preg_replace("!\W!", "_", $doc_titre).".pdf";
$outputFile = $fileName;

/** Fichiers utilis�s */
$nbSection = Request("nbSection", REQ_POST_GET, "0", "is_numeric");
$tabPages = Request("tabPages", REQ_POST_GET, array());
$tabPDF = Request("tabPDF", REQ_POST_GET, array());

$bGenere = false;
$oHide = new HtmlHidden("nbSection", $nbSection);

$strScript = "";

$strHtml =  "" .
    "<html><head>" .
    "</head><body onload='onLoadExec()'>" .
    "<form name='make_index' method='post' action='' target=''>";

/** G�n�ration PDF de chaque page html */
foreach ($tabPages as $index => $url_page) {
  if (array_key_exists($index, $tabPDF)) continue;
  $tabPDF[$index] = GenererSection($strHtml, $url_page, $index, $outputFile, $output_directory);
  $bGenere = true;
}
/** G�n�ration PDF du document */
$oFpdi = new fpdi();

$tabSend = array("nbSection", "tabPages", "tabPDF");
foreach ($_REQUEST as $key=>$value){
  if (!in_array($key, $tabSend)){
    if (!is_array($value))
      $oHide->AddHidden($key, $value);
    else {
      foreach ($value as $k=>$v){
        $oHide->AddHidden($key."[".$k."]", $v);
      }
    }
  }
}

if ($bGenere){// g�n�ration des pages manquantes
  foreach ($tabPDF as $index=>$page){
    $oHide->AddHidden("tabPDF[".$index."]", $page);
  }  
  foreach ($tabPages as $index=>$page){
    if (!array_key_exists($index, $tabPDF))
      $oHide->AddHidden("tabPages[".$index."]", $page);
  }  
}

elseif ($genere_index){// generation de l'index

  $indexes = array ();
  $noPage = $page_sommaire+1;
  $firstPage = $page_sommaire+1;
  foreach ($tabPDF as $in => $file) {
    $pagecount = $oFpdi->setSourceFile($file);
    for ($i = 1; $i <= $pagecount; $i ++) {
      $oFpdi->AddPage();
      $tplidx = $oFpdi->ImportPage($i);
      $oFpdi->useTemplate($tplidx);
      $contents = $oFpdi->current_parser->getPageContent($oFpdi->current_parser->pages[$oFpdi->current_parser->pageno][1][1]['/Contents']);
      $str = "";
      foreach ($contents as $tmp_content) {
        $str .= $oFpdi->current_parser->rebuildContentStream($tmp_content);
      }
      $matches = array ();
      $tabCorresp = array ();
      preg_match_all($index_regexp, $str, $matches);
      if (count($matches[0]) > 0) {
        if ($firstPage == 1)
          $firstPage = $noPage;
        foreach ($matches[0] as $match) {
          $tabCorresp[$match] = $noPage;
        }
        $indexes = array_merge($indexes, $tabCorresp);
      }
      $noPage ++;
    }
  }

  
  $oHide->AddHidden("genere_index", 1);
  $oHide->AddHidden("page_sommaire", $page_sommaire);

  foreach ($indexes as $match=>$noPage){
    $oHide->AddHidden("indexes[".$match."]", $noPage);
  }
  foreach ($tabPDF as $index=>$file){
    if ($index==$page_sommaire) continue;
    $oHide->AddHidden("tabPDF[".$index."]", $file);
  }
  unlink($tabPDF[$page_sommaire]);
  
  $strScript .= "document.forms[0].action = 'http://".$url_source."';";
}//fin generation d'index

else{ // concatenation des pages
  ksort($tabPDF);
  $pagecount = 0;
  foreach ($tabPDF as $pdfFile){
    $nbPages = $oFpdi->setSourceFile($pdfFile);
    $pagecount += $nbPages;
    for ($i=1; $i<=$nbPages; $i++){
      $oFpdi->AddPage();
      $tplidx = $oFpdi->ImportPage($i);
      $oFpdi->useTemplate($tplidx);
    }    
 //   unlink($pdfFile);
  }  
  
  if ($page_number){ // generation numerotation des pages
    $oFpdi->Output($output_directory."tmp_".$fileName, "F");
    
    $footerFile = $output_directory.$agent_id."_".$doc_id."_footer.pdf";
    $oFooter = new HTML2FPDF();
    $oFpdi = new fpdi();
    $pagecount = $oFpdi->setSourceFile($output_directory."tmp_".$fileName);
    for ($i = 1; $i <= $pagecount; $i ++) {
      $oFooter->AddPage();
      $oFooter->WriteHTML("", true);
    }
    $oFooter->Output($footerFile, "F");
    
    $oFpdi = new fpdi();
    for ($i = 1; $i <= $pagecount; $i ++) {
      $oFpdi->setSourceFile($output_directory."tmp_".$fileName);
      $oFpdi->AddPage();
      $tplidx = $oFpdi->ImportPage($i);   
      $oFpdi->useTemplate($tplidx);
      $oFpdi->setSourceFile($footerFile);
      $tplidx = $oFpdi->ImportPage($i);   
      $oFpdi->useTemplate($tplidx);
    }
  }
  unlink($output_directory."tmp_".$fileName);
  unlink($output_directory.$agent_id."_".$doc_id."_footer.pdf");
  if (file_exists($output_directory."lock_pid_".$_SESSION["sit_idUser"].".txt"))
    unlink($output_directory."lock_pid_".$_SESSION["sit_idUser"].".txt");
  $oFpdi->Output($output_directory.$fileName, "I");
  exit();
}

$strScript .= "document.forms[0].submit();";
$strHtml .= 
      $oHide->GetHtml().
      "</form>" .
      "<script language='javascript'>" .
      "function onLoadExec(){" .
      $strScript .
      "}" .
      "</script>" .
      "</body>" .
      "</html>";
echo $strHtml;

function GenererSection(&$strHtml, $urlPage, $indexPage, $outputFile, $output_directory) {
  global $FICHIER_MODE_APPEND;
  global $tabConfig;
  
  $tabConfig["URL"] = urlencode($urlPage);
  $strUrl = "";
  foreach($tabConfig as $strParam=>$valParam){
    $value = $valParam;
    if ($strParam=="output") $value = 2; 
    $strUrl .= ($value!=""?
                    ($strUrl==""?"":"&").$strParam."=".$value
                    :"");
  }
  $strUrl = "../alk_html2ps.php?".$strUrl;

  $strHtml .= "<iframe name='genPDF_".$indexPage."' src='".$strUrl."' width='10' height='10' frameborder='1'></iframe><br>";echo $strHtml;exit();
  return $output_directory.preg_replace("!\W!", "_", $urlPage).".pdf";
}

?>

