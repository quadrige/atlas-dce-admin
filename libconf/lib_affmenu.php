<?php

/**
 * @brief Affiche l'entete de la page
 */
function getHtmlMainHeader()
{
	global $PHP_SELF;

  $strHtml = "<html><head>".
    "<title>".ALK_APP_NAME."</title>".
    "<meta name='title' content=''>".
    "<meta name='description' content=''>".
    "<meta NAME='language' content='FR'>";
  
  if( ALK_B_PRINT==true && isset($_GET["print"]) && $_GET["print"]=="1" ) {
    $strHtml .= "<link rel='stylesheet' href='".ALK_SIALKE_URL."styles/si_print.css' type='text/css'>";
  } elseif( STRNAVIGATOR == NAV_NETSCAPE4 ) { 
    //$strHtml .= "<link rel='stylesheet' href='".ALK_SIALKE_URL."styles/si-gen.css' type='text/css'>";
  } else {
    //$strHtml .= "<link rel='stylesheet' href='".ALK_SIALKE_URL."styles/si-gen-ie.css' type='text/css'>";
  } 
  $strHtml .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>";
  
  return $strHtml;
}

function aff_header(){
	echo getHtmlMainHeader();
}

/**
 * @brief Retourne le code javascript sp�cifique au projet pour les fen�tres popup
 *        D�claration de la fonction js onLoadMainInit() appel�e syst�matiquement par le body onload
 *
 * @return Retourne un string
 */
function getHtmlMainPopupJs()
{
  return "<script language='javascript'>".
    " function onLoadMainInit() {".
    " } ".
    "</script>";
}

/**
 * @brief Retourne le pied de page html pour la fen�tre popup
 *
 * @return Retourne un string
 */
function getHtmlMainPopupFooter()
{
  return "<iframe name='footerExecPopup' width='0' height='0' frameborder='0'></iframe></body></html>";  
}

/**
 * @brief Affiche le pied de page
 */
function aff_footer()
{
  echo "</body></html>";
}

/**
 * @brief Affiche le logo SIT :
 *        Largeur : 141 pixels, � encadrer entre <TD></TD>
 *        Hauteur : 97 pixels
 *        Images utilis�es : menu/logo_sit.gif
 */
function affLogoSit()
{
}

/**
 * @brief Affiche la barre de titre
 *        Largeur : 141+639 pixels, � encadrer entre <TR></TR> avec 2 colonnes <TD></TD>
 *        Hauteur : 63 pixels
 * Images utilis�es :
 *    menu/titre_sommaire.gif, menu/visuels.gif, 
 *    menu/aide.gif, menu/fermer_session.gif
 */
function affBarreTitre($cont_id, $strTitrePhoto, $strTitrePage, $strTitreAliasPage, $strLogin)
{
}

/**
 * @brief Affiche la barre de boutons
 *        Largeur : 639 pixels
 *        Hauteur : 34 pixels
 *        Images utilis�es :
 *          menu/fond_haut2.gif, menu/fond_haut2D.gif
 *          menu/menu01accueil.gif,   menu/menu02plan.gif, 
 *          menu/menu03utilisateurs.gif, 
 *          menu/menu04contacts.gif, menu/menu05admin.gif
 */
function affBarreBoutons($cont_id, $strEmailContact, $iLogoAdminGen, $urlAdminGen)
{

}

/**
 * @brief Affiche la barre de navigation avec le texte "Votre parcours"
 *        Largeur : 780 pixels
 *        Hauteur : 23 pixels (peut grandir dynamiquement en fonction du contenu)
 *        Images utilis�es : menu/titre_parcours.gif
 */
function affBarreNavigation($strChemin)
{
}

/**
 * @brief Affiche le menu de gauche et les popup
 */
function affMenuGauche($cont_id, $cont_appli_id, $strEmailContact, $iLogoAdminGen, $urlAdminGen, 
                       $_sit_tabMenuEspace, $_sit_tabMenuEspaceFils, $_sit_tabMenu, $_sit_tabMenuFils)
{
}

/**
 * @brief Affiche les calques des menus popups
 *        Images utilis�es : connect_SIT_Dep.gif, connect_SIT_Dep_1.gif, connect_SIT_Dep_2.gif
 */
function affConnectSIT($strHtmlImg)
{
}

//-------------------------------------------------
// fonctions d'habillage sp�cifique pour les popup
//-------------------------------------------------
/**
 * @brief Retourne le code Html de l'ent�te haute d'une fen�tre popup de l'application
 *        equiv : aff_page_haut (lib_menu_popup)
 *        function d'ouverture : fermeture associ�e : getHtmlFooterPage()
 *
 * @param bUpdate  =true : mode modif, =false : mode information
 * @param strTitle Titre de la fen�tre
 * @param iWidth   Largeur du tableau conteneur d'information (=606 par d�faut)
 * @return Retourne une chaine de caract�res
 */
function getHtmlHeaderPage($bUpdate, $strTitle, $iWidth="606")
{
  $iWidth -= 20;
  return "<table width=".$iWidth." border=0 cellspacing=0 cellpadding=0>".
    "<tr>".
    "<td height=5>".
    "<img width=1 height=1></td>".
    "</tr>".
    "</table>".

    "<table width=".$iWidth." border=0 cellspacing=0 cellpadding=0>".
    "<tr>".
    "<td width=".$iWidth." height=22 bgcolor=".ALK_COL_STR_BG_COLOR_2." class=ContenuBlancBD>&nbsp;".$strTitle."</td>".
    "</tr>".
    "</table>".

    "<table width=".$iWidth." border=0 cellspacing=0 cellpadding=0>".
    "<tr>".
    "<td width=1 bgcolor=".ALK_COL_STR_COLOR_LINE.">".
    "<img width=1 height=1></td>".
    "<td width=".($iWidth-2).">";
}

/**
 * @brief Retourne le code Html du pied de page d'une fen�tre popup de l'application
 *        equiv : aff_page_bas (lib_menu_popup)
 *        function de fermeture : ouverture associ�e : getHtmlHeaderPage()
 *
 * @return Retourne une chaine de caract�res
 */
function getHtmlFooterPage()
{
  return "</td>".
    "<td width=1 bgcolor=".ALK_COL_STR_COLOR_LINE.">".
    "<img width=1 height=1></td>".
    "</tr>".
    "</table>";
}

/**
 * @brief Retourne le code Html d'une ligne s�paratrice
 *        equiv : aff_page_cadre_separateur (lib_menu_popup)
 *        A utiliser : apres xxxHeaderPage, xxxHeaderFrame
 *                     avant xxxFooterPage, xxxFooterFrame
 *
 * @param iWidth Largeur du tableau conteneur d'information (=606 par d�faut)
 * @return Retourne une chaine de caract�res
 */
function getHtmlLineFrame($iWidth="606")
{
  $iWidth -= 20;
  return "<table width=".($iWidth-2)." border=0 cellspacing=0 cellpadding=0>".
    "<tr>".
    "<td height=1 bgcolor=".ALK_COL_STR_COLOR_LINE.">".
    "<img width=1 height=1></td>".
    "</tr>".
    "</table>";
}

/**
 * @brief Retourne le code Html de l'entete d'un cadre d'affichage
 *        equiv : aff_page_cadre_haut (lib_menu_popup)
 *        function d'ouverture : fermeture associ�e : getHtmlFooterFrame()
 *
 * @param iWidth  Largeur du tableau conteneur d'information (=606 par d�faut)
 * @param iHeight Hauteur de base (=5 par d�faut)
 * @return Retourne une chaine de caract�res
 */
function getHtmlHeaderFrame($iWidth="606", $iHeight="5")
{
  $iWidth -= 20;
  return "<table width=".($iWidth-2)." border=0 cellspacing=0 cellpadding=0 bgcolor=#ffffff>".
    ( $iHeight > 0 
      ? "<tr><td height=".$iHeight."><img width=1 height=1></td></tr>"
      : "").
    "<tr><td align=left valign=top>";
}

/**
 * @brief Retourne le code Html du pied d'un cadre d'affichage
 *        equiv : aff_page_cadre_bas (lib_menu_popup)
 *        function de fermeture : ouverture associ�e : getHtmlHeaderFrame()
 *
 * @param iHeight Hauteur de base (=5 par d�faut)
 * @return Retourne une chaine de caract�res
 */
function getHtmlFooterFrame($iHeight="5")
{
  return "</td></tr>".
    ( $iHeight>0 ? "<tr><td height=".$iHeight."></td></tr>" : "").
    "</table>";
}

?>