<?
session_start();

include_once("app_conf.php");
include_once("app_conf_alkanet.php");
include_once("../../lib/lib_request.php");
/** 
 * @brief Constantes spécifiants les types de récupération :
 *        Get, Post, Get puis Post, Post puis Get 
 */

/**
 * @brief Controle de sécurité
 */

if (PHP_VERSION>='5')
   ini_set('zend.ze1_compatibility_mode', '1');       
   
$strPageCur = $_SERVER["PHP_SELF"];
$strPageCur = substr($strPageCur, 1); // enleve le premier caractère = /

// evite d'afficher la popup d'identification pour les pages suivantes
if( strpos($strPageCur, "identification.php")===false && 
    strpos($strPageCur, "login_form.php")===false && 
    strpos($strPageCur, "login_verif.php")===false &&
    strpos($strPageCur, "site/")===false &&
    strpos($strPageCur, "login_sql.php")===false
    )
{
  if( !(isset($_SESSION["sit_idUser"]) && is_numeric($_SESSION["sit_idUser"])) )
    {?>
 <html><head>
    <script language="javascript">
    function OpenWindowLogin() {
      var adr = "<?php echo ALK_SIALKE_URL; ?>scripts/ident/login_form.php";
      var oWind = window.open(adr, 'loginWindow', 
            'toolbar=no,scrollbars=yes,menubar=no,status=no,resizable=no,width=550,height=200,allwaysRaised=yes,screenX=0,screenY=0');
      oWind.focus();
    }
 </script>
     </head>
     <body onload="OpenWindowLogin()"></body>
     </html><?
     exit();
    }
}
?>