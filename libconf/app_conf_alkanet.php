<?php

/**
 * @file   app_conf_sit29.php
 * @author P.C
 * 
 * @brief  liste de constantes sp�cifiques au type d'application SIT29
 * 
 * Chaque constante d�finie ici est un �quivalent d'une variable de session de
 * l'ancienne version de l'appli.
 */

/**
 * Param�tres g�n�riques :
 *   - nom de l'application, version, titre
 */

define("ALK_APP_NAME",    "ALKANET");
define("ALK_APP_VERSION", "1.01");
define("ALK_APP_TITLE",   "IFREMER Atlas DCE");
define("ALK_URL_ON_LOGO", "");
define("ALK_TYPE_INTERTACE", "SIT");

/**
 * Indique si l'API � base de classes est utilis�e.
 */
define("ALK_B_API_FORM_CLASS", true);
define("ALK_FIELD_NOT_VIEW", "_NOT_PUB_");

/** type d'onglet */
define("ALK_CONSULT_TYPESHEET",   0);
define("ALK_ADMIN_TYPESHEET",     1);
define("ALK_PROPRIETE_TYPESHEET", 2);

/** onglet pour la consultation annuaire */

define("ALK_SHEET_ATLAS",   0);
define("ALK_SHEET_ANNU",   	1);
define("ALK_SHEET_QUALITE",	2);
define("ALK_SHEET_MASSE_EAU",	3);

define("ALK_SSHEET_DATA",    0);
define("ALK_SSHEET_IMPORT",  1);
define("ALK_SSHEET_ME",    	 2);
define("ALK_SSHEET_OPERATEUR", 3);
define("ALK_SSHEET_RESEAU",    4);
define("ALK_SSHEET_PARAMETRE", 5);
define("ALK_SSHEET_POINT",     6);
define("ALK_SSHEET_CARTE",     7);
define("ALK_SSHEET_ELEMENT_QUALITE", 8);
define("ALK_SSHEET_ARCHIVAGE", 9);
define("ALK_SSHEET_ARCHIVE", 10);
define("ALK_SSHEET_BASSIN", 11);
define("ALK_SSHEET_CONTROLE", 12);

define("ALK_SSHEET_ANNU_LISTE", 7);
define("ALK_SSHEET_ANNU_FICHE",	8);
define("ALK_SSHEET_ANNU_NEW",	9);
    
/** Mode d'acc�s au subsheet */
define("ALK_MODE_SSHEET_LIST", 0);
define("ALK_MODE_SSHEET_FORM", 1);
define("ALK_MODE_SSHEET_SQL", 2);
define("ALK_MODE_SSHEET_HIDDEN", 3);
define("ALK_MODE_SSHEET_POP", 4);
define("ALK_MODE_SSHEET_POP_SQL", 5);


/** @brief R�pertoire upload */
define("ALK_PATH_UPLOAD_CARTE", "upload/carte/");
define("ALK_PATH_UPLOAD_FICHE", "upload/fiche/");
define("ALK_PATH_UPLOAD_IMPORT", "upload/import/");
define("ALK_PATH_UPLOAD_DOC", "upload/doc/");

define ("ALK_B_PRINT", false);
/**
 * Couleurs dominantes du site
 * strBgColor1 = couleur la plus claire
 * strBgColor3 = couleur la plus fonc�e
 */

/** ent�te */
define("ALK_COL_STR_BG_COLOR_1", "#9DCBE3");

/** fond d'entete sur titre fenetre popup */
define("ALK_COL_STR_BG_COLOR_2", "#3B8BB4");

/** couleur pair */
define("ALK_COL_STR_BG_COLOR_3", "#CEEEFF");

/** couleur impair */
define("ALK_COL_STR_BG_COLOR_4", "#7ED1FC");

/** couleur tripair */
define("ALK_COL_STR_BG_COLOR_5", "#B4E0FF");

/** couleur utilisee pour le texte et les bordures */
define("ALK_COL_STR_TXT_COLOR_1", "#0084E1");

/** couleur du trait, ancien nom : strColorFilet */
define("ALK_COL_STR_COLOR_LINE", "#808080");

/** couleur de fond du titre theme d'une rubrique fdoc */
define("ALK_COL_BG_COLOR_FDOC_THEME_TITRE", "#bbe4f9");

/** couleur de la barre de navigation */
define("ALK_COL_STR_NAV_MENU_BG_COLOR", "#E1E1E1");

/** couleur de la barre de statut */
define("ALK_COL_BG_COLOR_STATUS_BAR", "#E1E1E1");

/** couleur de fond de l'actu Acces direct et Pr�sentation du conteneur */
define("ALK_COL_STR_TITRE_ACTU_BG_COLOR1", "#a8d3d7");

/** couleur de fond de l'actu info zoom et actu en continu */
define("ALK_COL_STR_TITRE_ACTU_BG_COLOR2", "#a8d3d7");

/** affichage d'un visuel sur la barre de titre */
define("ALK_COL_B_AFF_VISUAL_TITLE_BAR", false);

/** Affichage des pictos dans le menu */
define("ALK_COL_B_AFF_PICTO_MENU", true);

/** Couleur du menu (fond, roll, fond popup) */
define("ALK_COL_BG_COLOR_MENU", "#6CB0D5");
define("ALK_COL_BG_COLOR_MENU_SELECT", "#6CB0D5");
define("ALK_COL_BG_COLOR_MENU_ROLL", "#1E6188");
define("ALK_COL_BG_COLOR_MENU_POPUP", "#1E6188");

/**
 * Url et Path
 */

/** URL � la racine de l'appli sialke */
define("ALK_SIALKE_URL", ALK_ROOT_URL.ALK_SIALKE_DIR);

/** Chemin � la racine de l'appli sialke */
define("ALK_SIALKE_PATH", ALK_ROOT_PATH.ALK_SIALKE_DIR);

/** url d'identification */
define("ALK_URL_IDENTIFICATION", ALK_ROOT_URL."/admin/index.php");

/** r�pertoire de base des m�dias */
define("ALK_URL_SI_MEDIA", ALK_SIALKE_URL."media/admin/");

/** r�pertoire des images n�cessaires pour les boutons */
define("ALK_URL_SI_IMAGES", ALK_URL_SI_MEDIA."images/");

/** caract�re s�parateur r�pertoire */
define("ALK_PATH_SPACE_LINK", "/");

/** r�pertoire des classes application et espace */
define("ALK_DIR_CLASSE_APPLI", "appli/");

/** r�pertoire du fichier d'identification */
define("ALK_DIR_IDENTIFICATION", "ident/");

define("ALK_DIR_ESPACE", "");
define("ALK_DIR_ANNU", "");
/** 
 * Navigateur client
 */

/** Type de navigateur */
define("NAV_IE",        "IE");
define("NAV_OPERA",     "Opera");
define("NAV_NETSCAPE4", "Netscape4");
define("NAV_NETSCAPE6", "Netscape6");
define("NAV_OTHER",     "Navigateur inconnu");

/** D�tection du navigateur client */
$strUserAgent = $_SERVER["HTTP_USER_AGENT"];
if( preg_match('/msie/i', $strUserAgent) && !preg_match('/opera/i', $strUserAgent) ) {
  define("STRNAVIGATOR", NAV_IE);
} elseif( preg_match('/opera/i', $strUserAgent) ) {
  define("STRNAVIGATOR", NAV_OPERA);
} elseif( preg_match('/Mozilla\/4./i', $strUserAgent) ) {
  define("STRNAVIGATOR", NAV_NETSCAPE4);
} elseif( preg_match('/Mozilla\/5.0/i', $strUserAgent) && !preg_match('/Konqueror/i', $strUserAgent) ) {
  define("STRNAVIGATOR", NAV_NETSCAPE6);
} else {
  define("STRNAVIGATOR", NAV_OTHER);
}

/**
 * D�tection du type d'API utilis� (V1 ou V2)
 */

$PHP_SELF = $_SERVER["PHP_SELF"];

error_reporting(E_ALL);

define("ALK_B_API_CONN_V2", true);

?>