<?php

/**
 * @file   app_conf.php
 * @author P.C.
 *
 * @brief  liste de constantes sp�cifiques � l'implentation de l'application
 *
 * Chaque constante d�finie ici est un �quivalent d'une variable de session de
 * l'ancienne version de l'appli.
 */

// type SGBD
define("SGBD_ORACLE",   "oracle");
define("SGBD_MYSQL",    "mysql");
define("SGBD_POSTGRES", "postgres");

/** D�finit le type de la base de donn�es employ�e pour cette appli. */
define("ALK_TYPE_BDD", SGBD_MYSQL);

/**
 * parametres de connexion oracle
 */
define("ALK_ORA_LOGIN", "");
define("ALK_ORA_PWD",   "");
define("ALK_ORA_SID",   "DEV1");

/**
 * parametres de connexion MySQL
 */
define("ALK_MYSQL_LOGIN", "");
define("ALK_MYSQL_HOST",  "");
define("ALK_MYSQL_PWD",   "");
define("ALK_MYSQL_BD",    "");
define("ALK_MYSQL_PORT",  "");

/** Adresse email du Cr�ateur (admin) */
define("ALK_SI_MAIL", "");

/** wkhtmltopdf */
define("ALK_CMD_PHP", "/usr/bin/php");

define("ALK_CMD_WKHTMLTOPDF", "/usr/bin/xvfb-run /usr/bin/wkhtmltopdf");
/**/

/**
 * Option g�n�rale
 */

/**
 * vrai si l'appli tourne en mode debug, false sinon
 * En mode debug, les mails ne sont pas envoy�s.
*/
define("ALK_MODE_DEBUG", false);

/** nb de destinataires max par envoi de mail */
define("ALK_MAX_SENDMAIL", 10);

/**
 * Url et Path
 */

/** URL de base du SI */
$strUrlAcces = (empty($_SERVER['HTTP_X_FORWARDED_HOST'])? $_SERVER["HTTP_HOST"]:$_SERVER['HTTP_X_FORWARDED_HOST']);
define("ALK_ROOT_URL", "http://".$strUrlAcces."/");
define("ALK_ROOT_PDF_URL", "http://".$strUrlAcces."/");

/** Chemin de base du SI */
define("ALK_ROOT_PATH", "");



/** Sous-r�pertoire contenant l'appli sialke */
define("ALK_SIALKE_DIR", "/");

/** Gestion des langues */
//$tabLg = array(1=>"fr", 2=>"uk", 3=>"de", 4=>"es",  5=>"it");
$tabLg = array(1=>"fr");
while( list($iKey, $strLg) = each($tabLg) ) {
  define("ALK_LG_".strtoupper($strLg), $iKey);
  $tabLgTmp[$iKey] = array("bdd"=>"_".strtoupper($strLg), "rep"=>$strLg);
}
$_LG_tab_langue = $tabLgTmp;

/** langue en cours */
define("ALK_LG", ALK_LG_FR);

?>
