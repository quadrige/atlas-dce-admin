
 function MaplinkEntity(map, CenterLon, CenterLat, factorLon, factorLat, SizeX,SizeY,IconCampingPath,CampingName,CampingBulleContent, PopUpCampingContent,campingId)  
{  
  // Variable  
  this.CenterLon = CenterLon;
  this.CenterLat = CenterLat;
  this.factorLon = factorLon;
  this.factorLat = factorLat;
  this.campingId = campingId;
  this.markerDisplay = null;
  this.popup = null;
  this.popup1 = null;
  this.bVisible = true;
  
  this.map = map;
    
  this.IconPath= IconCampingPath;
  this.CampingNom = CampingName;
  this.CampingBulle = CampingBulleContent;
  this.PopUpHtmlContent = PopUpCampingContent;  
  this.Autosize = !(this.factorLon==0 && this.factorLat==0);
  
  this.deltaPix = 60;
  
  var oImg = new Image();
  oImg.object = this;
  oImg.onload = function(){
    this.object.sizeIconx = this.width;
    this.object.sizeIcony= this.height;
    this.object.init();  
  }
  oImg.src = IconCampingPath;
     
};  

// Méthodes publiques de maClasse  
MaplinkEntity.prototype.init = function() 
{  
    var size = new OpenLayers.Size(this.sizeIconx,this.sizeIcony);
    var TexteBulle = this.CampingBulle;
    var CampingId = this.campingId;
    var CampingNom = this.CampingNom;
    var TextHtmlPopup = this.PopUpHtmlContent;
    var LonLatObject = new OpenLayers.LonLat(this.CenterLon,this.CenterLat);
    var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
    var icon_tra = new OpenLayers.Icon(this.IconPath,size,offset);
    this.markerDisplay = new OpenLayers.Marker(LonLatObject,icon_tra.clone());
    this.markerDisplay.display(this.bVisible); 
    for(var prop in this){
      if ( typeof this[prop] != "undefined" &&  typeof this[prop] != "function" ){
          this.markerDisplay[prop] = this[prop];
      }
    }
    var CampingAutosize = this.Autosize;
    
    markerI.addMarker(this.markerDisplay);
  
    
    this.markerDisplay.events.register(
      'mouseover', this, 
      function(evt) {
       if (this.popup != null){
          this.map.removePopup(this.popup);
        }
        this.popup = new OpenLayers.Popup.Anchored("Camping",this.markerDisplay.lonlat,new OpenLayers.Size(180,30), TexteBulle, icon_tra, false);
        this.popup.autoSize = true;
        //this.popup.backgroundColor = "transparent";
        //popup.setBorder("1px solid #00888E");
        this.map.addPopup(this.popup);
        Event.stop(evt);
      }
    );
/**    
    this.markerDisplay.events.register(
      'mouseover', this, 
      function(evt) {
        this.markerDisplay.setUrl('../../media/images/imagesCarte/camion-roll.png');
        this.markerDisplay.display(true);
        Event.stop(evt);
      }
    );
*/
    this.markerDisplay.events.register(
      'mouseout', this, 
      function(evt)  {
        if (this.popup != null){
          this.map.removePopup(this.popup);
        }
        Event.stop(evt);
      }
    );

 /**
    this.markerDisplay.events.register(
      'mouseout', this, 
      function(evt)  {
        
        this.markerDisplay.setUrl(this.IconPath);
        this.markerDisplay.display(true);
        Event.stop(evt);
      }
    ); 
*/    
    this.markerDisplay.events.register(
      'click', this, 
      function(evt){
    	  if (this.popup1 != null){
	        this.map.removePopup(this.popup1);
	      }
	      this.popup1 = new OpenLayers.Popup.FramedCloud("Wirma"+CampingId,this.markerDisplay.lonlat,new OpenLayers.Size(300,250),
	      '', icon_tra, true);
	      this.popup1.autoSize = false;
	      //popup1.backgroundColor = "#FFFFFF";
	      //popup1.backgroundColor = "transparent";
	      this.map.addPopup(this.popup1);
	      oAjax = new AlkAjax('nameNlValide', 'fullDiv', ALK_FORM_METHOD_GET, TextHtmlPopup, null);
	      window.fullDiv = function(xhrResponse){
	      fnLoadDiv(xhrResponse, "Wirma"+CampingId+"_contentDiv");
	      }
	      Event.stop(evt);
      }
    );
    
  }
  
 

MaplinkEntity.drawMarker = function(marker)  
{  
  var px = this.map.getLayerPxFromLonLat(new OpenLayers.LonLat(marker.CenterLon,marker.CenterLat));
  var factor = Math.max(0, Math.ceil(this.map.getZoom()/4));
  px = new OpenLayers.Pixel(px.x+marker.deltaPix*marker.factorLon*factor, px.y+marker.deltaPix*marker.factorLat*factor);
  marker.lonlat = this.map.getLonLatFromLayerPx(px);
  if (px == null) {
      marker.display(false);
  } else {
      var markerImg = marker.draw(px);
      if (!marker.drawn) {
          this.div.appendChild(markerImg);
          marker.drawn = true;
      }
  }
};


MaplinkEntity.destroy = function(marker)  
{  
       marker.destroy();
};





MaplinkEntity.prototype.setVisible = function(bVisible)
{
  this.bVisible = bVisible;
  if(this.markerDisplay != null){
    this.markerDisplay.display(this.bVisible); 
  }
};

MaplinkEntity.prototype.destroy = function()
{
    this.markerDisplay.destroy(); 
};


MaplinkEntity.prototype.showOnMouseOver = function(bOver)
{
  if(this.markerDisplay != null){
    if(bOver) 
    	this.markerDisplay.setUrl('../../media/sigsite/camion-roll.png');
    else
    	this.markerDisplay.setUrl('../../media/sigsite/camion.png');
  }
};

MaplinkEntity.prototype.erase = function()
{
  if(this.markerDisplay != null){
    	this.markerDisplay.erase();
  }
};

MaplinkEntity.prototype.getMarker = function()
{
return this.markerDisplay;
};

MaplinkEntity.prototype.moveToPonit = function(px)
{
  if(this.markerDisplay != null){
    	this.markerDisplay.moveTo(px);
  }
};

MaplinkEntity.prototype.movePopupTo = function(px)
{
  if( this.popup1 != null){
      this.popup1.moveTo(px);
      oAjax = new AlkAjax('nameNlValide', 'fullDiv', ALK_FORM_METHOD_GET, this.PopUpHtmlContent, null);
	      window.fullDiv = function(xhrResponse){
	      fnLoadDiv(xhrResponse, "Wirma"+this.campingId+"_contentDiv");
	    }
    	
  }
};


MaplinkEntity.prototype.setPicto = function(picto)
{
   if(this.markerDisplay != null){
    	this.markerDisplay.setUrl('../../media/images/imagesCarte/'+picto+'-camion.png');
  }
};

MaplinkEntity.prototype.setCible = function()
{
   if(this.markerDisplay != null){
    	this.markerDisplay.setUrl('../../media/images/imagesCarte/cercleRouge.gif');
  }
};


MaplinkEntity.prototype.getlonlat = function()
{
   if(this.markerDisplay != null){
    	return this.markerDisplay.lonlat;
  }
};



function fnLoadDiv(strHtmlContent, id)
{
	  var oDiv = document.getElementById(id);
	  if ( !oDiv ) return;
	  var oStyle = ( oDiv.style ? oDiv.style : oDiv );
	  oStyle.display = 'block';
	  oDiv.innerHTML = strHtmlContent;
}
	
	






