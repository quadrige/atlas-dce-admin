
 function GeolocLieux(map, CenterLon, CenterLat, IconCampingPath, LieuName, LieuBulleContent, LieuId, bModif, strPicto)  
{  
  // Variable  
  this.CenterLon = CenterLon;
  this.CenterLat = CenterLat;
  this.LieuId = LieuId;
  this.markerDisplay = null;
  this.popup = null;
  this.popup1 = null;
  this.bVisible = true;
  this.bModif = bModif;
  this.bVisible
  
  this.map = map;
    
  this.IconPath= IconCampingPath;
  this.LieuNom = LieuName;
  this.state = "N";
  this.LieuBulle = LieuBulleContent;
  this.strPicto = strPicto;
  this.deltaPix = 60;
  
  var oImg = new Image();
  oImg.object = this;
  oImg.onload = function(){
    this.object.sizeIconx = this.width;
    this.object.sizeIcony= this.height;
    this.object.init();  
  }
  oImg.src = IconCampingPath;
     
};  

// Méthodes publiques de maClasse  
GeolocLieux.prototype.init = function() 
{  
    var size = new OpenLayers.Size(this.sizeIconx,this.sizeIcony);
    //var TexteBulle = this.LieuBulle;
    var LieuId = this.LieuId;
    var LieuNom = this.LieuNom;
    var LonLatObject = new OpenLayers.LonLat(this.CenterLon,this.CenterLat);
    var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
    var icon_tra = new OpenLayers.Icon(this.IconPath,size,offset);
    this.markerDisplay = new OpenLayers.Marker(LonLatObject,icon_tra.clone());
    this.markerDisplay.display(this.bVisible); 
    for(var prop in this){
      if ( typeof this[prop] != "undefined" &&  typeof this[prop] != "function" ){
          this.markerDisplay[prop] = this[prop];
      }
    }
    
    markerI.addMarker(this.markerDisplay);
  
    
    this.markerDisplay.events.register(
      'mouseover', this, 
      function(evt) {
       if (this.popup != null){
          this.map.removePopup(this.popup);
        }
        this.popup = new OpenLayers.Popup.Anchored("Camping",this.markerDisplay.lonlat,new OpenLayers.Size(180,30), this.LieuBulle, icon_tra, false);
        this.popup.autoSize = true;
        //this.popup.backgroundColor = "transparent";
        //popup.setBorder("1px solid #00888E");
        this.map.addPopup(this.popup);
        Event.stop(evt);
      }
    );

    this.markerDisplay.events.register(
      'mouseout', this, 
      function(evt)  {
        if (this.popup != null){
          this.map.removePopup(this.popup);
        }
        Event.stop(evt);
      }
    );

    this.markerDisplay.events.register(
      'click', this, 
      function(evt){
      if(this.bModif){   
              for(var i=0; i< tabLieux.length; i++ ){
                  if(tabLieux[i].state == 'U'){
                          tabLieux[i].setPicto('/media/site/geoloc/marker-gold.png'); 
                          tabLieux[i].state = 'N'
                  }
              }
              
              
              this.markerDisplay.setUrl('/media/site/geoloc/marker-green.png');
              visible('bModifier');
              invisible('bAjouter');
              visible('bSupprimer');
              this.state = 'U';
              var px = this.map.getLayerPxFromLonLat(new OpenLayers.LonLat(0,0));   
              m.moveTo(px);
              var objLonLat = this.markerDisplay.lonlat
              document.formGeoLocForm.latitude.value=objLonLat.lat;
              document.formGeoLocForm.longitude.value=objLonLat.lon;
              document.formGeoLocForm.latitudeGpx.value='';
              document.formGeoLocForm.longitudeGpx.value='';
              document.formGeoLocForm.strLieu.value= this.LieuBulle;
              document.formGeoLocForm.pictoPoi.value= this.strPicto; 
        }
        Event.stop(evt);
      }
    );
    
  }
  
 

GeolocLieux.drawMarker = function(marker)  
{  
  var px = this.map.getLayerPxFromLonLat(new OpenLayers.LonLat(marker.CenterLon,marker.CenterLat));
  var factor = Math.max(0, Math.ceil(this.map.getZoom()/4));
  px = new OpenLayers.Pixel(px.x+marker.deltaPix*marker.factorLon*factor, px.y+marker.deltaPix*marker.factorLat*factor);
  marker.lonlat = this.map.getLonLatFromLayerPx(px);
  if (px == null) {
      marker.display(false);
  } else {
      var markerImg = marker.draw(px);
      if (!marker.drawn) {
          this.div.appendChild(markerImg);
          marker.drawn = true;
      }
  }
};


GeolocLieux.destroy = function(marker)  
{  
       marker.destroy();
};





GeolocLieux.prototype.setVisible = function(bVisible)
{
  this.bVisible = bVisible;
  if(this.markerDisplay != null){
    this.markerDisplay.display(this.bVisible); 
  }
};

GeolocLieux.prototype.destroy = function()
{
    this.markerDisplay.destroy(); 
};


GeolocLieux.prototype.showOnMouseOver = function(bOver)
{
  if(this.markerDisplay != null){
    if(bOver) 
    	this.markerDisplay.setUrl('../../media/sigsite/camion-roll.png');
    else
    	this.markerDisplay.setUrl('../../media/sigsite/camion.png');
  }
};

GeolocLieux.prototype.erase = function()
{
  if(this.markerDisplay != null){
    	this.markerDisplay.erase();
  }
};

GeolocLieux.prototype.getMarker = function()
{
return this.markerDisplay;
};

GeolocLieux.prototype.moveToPonit = function(px)
{
  if(this.markerDisplay != null){
    	this.markerDisplay.moveTo(px);
  }
};

GeolocLieux.prototype.movePopupTo = function(px)
{
  if( this.popup1 != null){
      this.popup1.moveTo(px);
      oAjax = new AlkAjax('nameNlValide', 'fullDiv', ALK_FORM_METHOD_GET, this.PopUpHtmlContent, null);
	      window.fullDiv = function(xhrResponse){
	      fnLoadDiv(xhrResponse, "Wirma"+this.campingId+"_contentDiv");
	    }
    	
  }
};


GeolocLieux.prototype.setPicto = function(picto)
{
   if(this.markerDisplay != null){
    	this.markerDisplay.setUrl(picto);
  }
};


GeolocLieux.prototype.getlonlat = function()
{
   if(this.markerDisplay != null){
    	return this.markerDisplay.lonlat;
  }
};



function fnLoadDiv(strHtmlContent, id)
{
	  var oDiv = document.getElementById(id);
	  if ( !oDiv ) return;
	  var oStyle = ( oDiv.style ? oDiv.style : oDiv );
	  oStyle.display = 'block';
	  oDiv.innerHTML = strHtmlContent;
}
	
	






