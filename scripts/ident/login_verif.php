<?php
include_once("../../libconf/lib_session.php");
include_once("api/gen_con.php");

$typeIdent = Request("typeIdent", REQ_POST_GET, "");
$strLogin = Request("strLogin", REQ_POST_GET, "");
$strMdp = encrypt(Request("strMdp", REQ_POST, ""));
// v�rifie la connexion
$dsAgent = $queryAnnu->GetDs_ficheAgentByLoginPwd($strLogin, $strMdp);

if( $drAgent = $dsAgent->getRowIter() ) {  
	// connexion ok
	$_SESSION["sit_usernom"] = $drAgent->getValueName("USER_NOM");
	$_SESSION["sit_userprenom"] = $drAgent->getValueName("USER_PRENOM");	
	$_SESSION["sit_idUser"] = $drAgent->getValueName("USER_ID");
  //$_SESSION["bAdminUser"] = $drAgent->getValueName("USER_ADMIN");  
  //$_SESSION["idBassin"] = $drAgent->getValueName("BASSIN_ID");
  // modif janvier 2013
  $_SESSION["idBassin"] = -1;
  $_SESSION["bAdminUserBassinCourant"] = $drAgent->getValueName("USER_ADMIN_ANNU");
  $_SESSION["bAdminUserAllBassin"] = $drAgent->getValueName("USER_ADMIN_ALL_BASSIN");
	if( $typeIdent == "" ) {
	  // identification classique
	  $strUrl = "atlas/01_page_form.php";
	  $_sit_urlHomePage = ALK_SIALKE_URL."scripts/".$strUrl;
	  // enregistrement de la date de derniere connexion
	  $queryAnnuAction->maj_agentDateConn($_SESSION["sit_idUser"]);
	  
	  // redirection en javascript pour forcer l'enregistrement des variables de session    
	  echo "<html><head>".
	    "<script language='javascript'>".
	    " function Redirection() {".
	    " document.location.href = '".$_sit_urlHomePage."'; }".
	    "</script></head>".
	    "<body onload='Redirection()'><!--<a href='javascript:Redirection()'>go</a>--></body></html>";
	} else {
	  // identification par popup
	  echo "<html><head>".
	    "<script language='javascript'>".
	    " function ClosePage() { ".
	    " window.opener.document.location.reload();".
	    " this.close(); }".
	    " </script></head>".
	    "<body onload='ClosePage()'><!--<a href='javascript:ClosePage()'>go</a>--></body></html>";
	}
	exit();
}

if( $typeIdent == "" ) {
	//echo "<a href=identification.php?err=1>erreur</a>";
	header("location: ../".ALK_DIR_IDENTIFICATION."identification.php?err=1");
} else {
	//echo "<a href=scripts/sit_menu/login_form.php?err=1>erreur</a>";
	header("location: login_form.php?err=1");
}
exit();

 /**
 * Encrypte une chaine de caract�re selon la m�thode param�tr�e (md5 par d�faut, sha1 sinon)
 * @param str  Cha�ne � encrypyter
 * @return string
 */
function encrypt($str)
{
  if( !defined("ALK_ANNU_PWD_ENCRYPTION") ) {
    define("ALK_ANNU_PWD_ENCRYPTION", "md5"); 
  }
  return call_user_func(ALK_ANNU_PWD_ENCRYPTION, $str);
}



?>