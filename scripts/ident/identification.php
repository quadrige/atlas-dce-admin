<?php 
include_once("../../libconf/lib_session.php");

$err = isset($_GET["err"]) ? $_GET["err"] : "";
$nc = isset($_GET["nc"]) ? $_GET["nc"] : "";
?>

<html>
<head>
<title><?=ALK_APP_TITLE?></title>
<meta name="title" content="">
<meta name="description" content="">
<meta name="keywords" lang="fr" content="">
<meta name="category" content="SIT">
<meta name="robots" content="index, follow">
<meta NAME="language" content="FR">
<? if( STRNAVIGATOR == "Netscape4") { ?>
<link rel="stylesheet" href="styles/sitpdl-gen.css" type="text/css">
<? } else { ?>
<link rel="stylesheet" href="styles/sitpdl-gen-ie.css" type="text/css">
<? } ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript">

</script>

</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="formLogin" method="post" action="login_verif.php">
<input type=hidden name="nc" value="<?=$nc?>">

<table width="780" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
    <td colspan=2 width="780" valign="top">
      <img src="../../media/admin/accueil/bandeau_ident.jpg" border=0>
    </td>
  </tr>
  <tr> 
    <td width="17"><img src="../../media/imgs/gen/transp.gif" width="10" height="10"></td>
    <td width="780" valign="top" align=center>
      <table width="358" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="10"><img src="../../media/imgs/gen/transp.gif" width="10" height="10"></td>
        </tr>
        
        <tr>
            <td valign="center" height=30 align=center>
              <div class="divContenuMsgErr">
              <?
              switch( $err )
              {
                case 1:
                  echo "Authentification refuse. Veuillez recommencer.";
                  break;

                case 2:
                  echo "Accs interdit. Veuillez vous r-authentifier.";
                  break;
              }
              ?>
              </div></td>
        </tr>
        <tr>
          <td height="10"><img src="../../media/imgs/gen/transp.gif" width="10" height="10"></td>
        </tr>
        <tr>
          <td>
            <table width="358" border="0" cellspacing="0" cellpadding="0">
              <tr valign="top"> 
                <td colspan="3"><img src="../../media/admin/accueil/tab_haut.gif" width="358" height="11"></td>
              </tr>
              <tr valign="middle" align="center" background="../../media/admin/accueil/tab_fond2.gif">
                <td colspan="3" background="../../media/admin/accueil/tab_fond2.gif"><img src="../../media/admin/accueil/Tab_identifier.gif" width="251" height="18"></td>
              </tr>
              <tr valign="top"> 
                <td colspan="3"><img src="../../media/admin/accueil/tab_separe.gif" width="358" height="7"></td>
              </tr>
              <tr> 
                <td background="../../media/admin/accueil/tab_G.gif" width="29">
                  <img src="../../media/admin/accueil/tab_G.gif" width="29" height="87"></td>
                <td valign="top" width="300" background="../../media/admin/accueil/tab_fond.gif">
                  <table background="" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td colspan="3" height="10"><img src="../../media/imgs/gen/transp.gif" width="10" height="10"></td>
                    </tr>
                    <tr>
                      <td align="right" width="120" valign="middle">
                        <img src="../../media/admin/accueil/Tab_identifiant.gif" width="87" height="16"></td>
                      <td width="10"><img src="../../media/imgs/gen/transp.gif" width="10" height="10"></td>
                      <td width="170" valign="middle">
                          <input type="text" name="strLogin" size="20" maxlength="20">
                        </td>
                    </tr>
                    <tr> 
                      <td align="right" valign="middle">
                        <img src="../../media/admin/accueil/Tab_PW.gif" width="96" height="17"></td>
                      <td>&nbsp;</td>
                      <td valign="middle">
                          <input type="password" name="strMdp" size="20" maxlength=10>
                        </td>
                    </tr>
                 
                    <tr> 
                      <td colspan="3" height="10"><img src="../../media/imgs/gen/transp.gif" width="10" height="10"></td>
                    </tr>
                    <tr valign="top"> 
                      <td colspan="3" align="right">
                        <input type=image src="../../media/admin/accueil/entrer.gif" width="82" height="18" border=0></td>
                    </tr>
                  </table>
                </td>
                <td background="../../media/admin/accueil/tab_D.gif" valign="top" width="29">
                   <img src="../../media/admin/accueil/tab_D.gif" width="29" height="87"></td>
              </tr>
              <tr valign="top"> 
                <td colspan="3"><img src="../../media/admin/accueil/tab_bas.gif" width="358" height="12"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td height="10"><img src="../../media/imgs/gen/transp.gif" width="10" height="10"></td>
        </tr>
      </table>
    </td>
    <td width="15" valign="top"><img src="../../media/imgs/gen/transp.gif" width="10" height="10"></td>
  </tr>
</table>

</form>
</body>
</html>
