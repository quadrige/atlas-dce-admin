<?php
include_once("../../lib/lib_session.php");
include_once("../../lib/lib_menu_popup.php");
include_once("../../lib/lib_aff.php");

$strParam = $_SERVER["QUERY_STRING"];
$err = isset($_GET["err"]) ? $_GET["err"] : "";

if( defined("ALK_B_SSO_CAS") && (ALK_B_SSO_CAS == true) ) {
  // charge la fonction d'authentification SSO CAS
  include_once("../../lib/lib_sso_cas.php");

  $strService =  ALK_SIALKE_URL."scripts/".ALK_DIR_ESPACE."login_form.php";

  // teste si jeton existe
  $strLogin = authenticateCAS($strService);

  if( $strLogin == FALSE ) {
    // erreur sur appel sso, redirige vers page authentification du SI
    // affiche le formulaire ci-dessous
  } else {
    // appel de la page permettant de collecter les infos du SI :
    // user_id, profil_id, service_id, cont_id et appli_id de demarrage
    $strUrl = "login_verif.php?typeIdent=1&strLogin=".$strLogin."&sso=1";
    header("location: ".$strUrl);
    exit();
  }
}

$oCtrlH = new HtmlHidden("SelectDep", "");
$oCtrlH->addHidden("typeIdent", "1");

$iLarg = 500;
$strTitle = "";
switch( $err ) {
 case 1: $strTitle .= "Authentification refuse. "; break;
 case 2: $strTitle .= "Accs interdit. "; break;
}
$strTitle .= "Veuillez vous identifier :";

$oCtrlTxt = new HtmlText(0, "strLogin", "", "Identifiant : ", 1, 20, 20);
$oCtrlPwd = new HtmlText(0, "strMdp",   "", "mot de passe : ", 1, 20, 20);
$oCtrlPwd->bPassword = true;

$oBtAnnuler = new HtmlLink("javascript:window.close()", "Annuler l'authentification", 
                           "annul_gen.gif", "annul_gen.gif");

$tabEventBody["onLoad"] = "onLoadMainInit(); onLoadInit(); onLoadWindAuth();";

$strHtml = getHtmlHeader($tabEventBody).
  "<script language='javascript'>".
  " function onLoadWindAuth() {". 
  "  var f = document.formLog;".
  "  f.strLogin.focus();".
  " } </script>".
  "<form NAME='formLog' action='login_verif.php' method='post'>".$oCtrlH->getHtml().

  getHtmlHeaderPage(false, $strTitle, $iLarg).
  getHtmlHeaderFrame($iLarg, 10).

  "<div align='center'>".
  GetHtmlTableCtrlHeader(100, 200).
  GetHtmlTableCtrlLine($oCtrlTxt->label, $oCtrlTxt->getHtml()).
  GetHtmlTableCtrlLine($oCtrlPwd->label, $oCtrlPwd->getHtml()).
  GetHtmlTableCtrlFooter().
  "</div>".

  getHtmlFooterFrame(10).
  getHtmlHeaderFrame($iLarg).

  "<table cellpadding='4' cellspacing='0' align='center'><tr><td>".
  "<input type=image src=\"".ALK_URL_SI_IMAGES."valid_gen.gif\" title=\"Valider l'authentification\">".
  "</td><td>".$oBtAnnuler->getHtml()."</td></tr>".
  "</table>".

  getHtmlFooterFrame().

  getHtmlFooterPage().

  getHtmlFooter();

echo $strHtml;
?>
