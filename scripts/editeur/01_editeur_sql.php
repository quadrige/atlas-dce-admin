<?
include_once ("lib/lib_session.php");
include_once ("api/gen_con.php");
include_once ("lib/lib_aff.php");
include_once ("lib/lib_menu_popup.php");
include_once ("classes/htmlCleaner.class.php");

$cont_id = Request("cont_id", REQ_GET, "-1", "is_numeric");
$cont_appli_id = Request("cont_appli_id", REQ_GET, "-1", "is_numeric");
$menu_appli_id = Request("menu_appli_id", REQ_GET, "1", "is_numeric");
$page = Request("page", REQ_GET, "1", "is_numeric");
$idEnt = Request("idEnt", REQ_GET, "0", "is_numeric"); // identifiant de l'entite (separateur pipe si plusieurs cles)
$strTable = Request("table", REQ_GET, ""); // nom de la table concern�e
$strChampHtml = Request("champsHtml", REQ_GET, ""); // nom du champs de la table qui va contenir le code HTML g�n�r� par l'�diteur
$strChampId = Request("champsId", REQ_GET, ""); // nom du champs correspondant � l'identifiant de l'entit� (separateur pipe si plusieurs cles)
$strRepUpload = Request("repUpload", REQ_GET, ""); // nom du champs correspondant au r�pertoire d'upload des pi�ces jointe
$bReload = Request("reload", REQ_GET, 0, "is_numeric"); // � 1 si la page m�re doit �tre rafraichie

/* Ajout� par MLA le 29/12/05 */
$post_appli_id = Request("post_appli_id", REQ_GET, "-1", "is_numeric");//identifiant de l'appli charg�e de faire les actions Pre et Post update bloc
$bloc_id = Request("bloc_id", REQ_GET, "-1", "is_numeric");//identifiant du bloc sur lequel se font les Pre et Post update bloc

/* Modifi� par MLA le 29/12/05 */
$strParam = "cont_appli_id=".$cont_appli_id."&cont_id=".$cont_id."&menu_appli_id=".$menu_appli_id.
	"&idEnt=$idEnt&table=$strTable&champsHtml=$strChampHtml&champsId=$strChampId" .
	"&repUpload=$strRepUpload"."&post_appli_id=".$post_appli_id."&bloc_id=".$bloc_id;

$iProfilCont = $oSpace->iProfilCont;
$iProfilAppli = $oSpace->iDroitAppli;

if( $iProfilAppli==0 ) {
  header("location:".ALK_URL_IDENTIFICATION."?err=2");
  exit();
}

// ne pas utiliser Request
if( defined("ALK_ENCRYPT_EDITEUR") && ALK_ENCRYPT_EDITEUR==true ) {
  $strTextHtml = RequestDecode("TextHtml", REQ_POST, "");
} else {
  $strTextHtml = ( isset($_POST["TextHtml"]) ? $_POST["TextHtml"] : "" );
  $strTextHtml = stripslashes($strTextHtml);
}

$HtmlCleaner = new HtmlCleaner();
$HtmlCleaner->cleanWithTidy($strTextHtml);


//$bNettoyage = Request("nettoyage", REQ_POST, 0, "is_numeric");
//if ( $bNettoyage == 1)
//	$HtmlCleaner->cleanHtml( $strTextHtml );
	
//contr�le du code html (fermeture des balise...)
//$HtmlCleaner->ConvertHtml( $strTextHtml );

//die("<textarea rows=50 cols=100>".$strTextHtml."</textarea>");
$res = $queryEditeurAction->updateContenu($idEnt, $strTable, $strChampHtml, $strTextHtml, $strChampId); 

/*
 * Ajout� par MLA le 29/12/05
 * @callservice  post_appli_id.CallServicePostUpdateBloc(idEnt)
 */
if ($post_appli_id!=-1){
  include_once("../../classes/appli/alkserviceappli.class.php");
	$oService = new AlkServiceAppli($post_appli_id, $oSpace, $cont_appli_id, $_SESSION["sit_idUser"]);
	$oRes = null;
	$oService->CallService("PostUpdateBloc", $oRes, $bloc_id, true); 
}

?>
<html>
<head>
<script language=javascript>
function onLoadWind() {
	<?if ($bReload == 1){?>
	window.opener.location.reload();
	<?}?>	
  window.close();
}
</script>
</head>
<body onload='onLoadWind()'></body>
</html>
