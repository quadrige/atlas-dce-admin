<?
/**
 * @class queryEditeurAction
 *
 * @brief Ensemble des requetes actions li�es � l'application actualit�s
 *        Les requetes sp�cifiques se trouvent dans une classe h�rit�e queryactu_action_xxx.class.php
 */
class QueryEditeurAction extends AlkQuery
{

  /**
   * Constructeur
   */ 
  function QueryEditeurAction(&$oDb)
  {
    parent::AlkQuery($oDb);
  }
    
  /**
   * @brief  Modifie une actu
   *
   * @param idActu Identifiant de l'actualite
   * 
   * @return boolean False if problem, true otherwise
   */
  function updateContenu($idEnt, $strTable, $strChampsHtml, $strTextHtml, $strChampId) 
  {
    $tabChampId = explode("|", $strChampId);
    $tabId = explode("|", $idEnt);
    
    $strSql = "update ".$strTable." set ".
      $strChampsHtml." = ".$this->dbConn->getCLob($strChampsHtml, $strTextHtml).
      " where 1=1";
      
    // gestion des cles multiples
    $i = 0;
    foreach ($tabChampId as $strChamp) {
      $strSql .= " and ".$strChamp."='".$tabId[$i]."'";
      $i++;
    }
    
    $tabCLob = array($strChampsHtml => $strTextHtml);

    $res = $this->dbConn->executeSql($strSql, true, $tabCLob) ;
    return $res;
  }
  
}

?>