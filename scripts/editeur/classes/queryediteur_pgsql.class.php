<?php
/* 
* File : queryediteurpgsql.php
* Create : 24/04/2004
* Author : AS

* Description : Contains the SQL request for the table ALK_USER
* Parameters :
*/
// Include local
include_once('queryediteur.class.php');

/*************** class queryEditeurMySql *************************
        Class which herits to the queryEditeur Class
******************************************************************/

class queryEditeurPgsql extends queryEditeur
{

  var $dbConn; // Connection to the database

  /**
  * @author      :  AS
  * @create      :  24/04/2004
  * @update      :
  * @description :  Constructor of the class
  *
  * @param dbConn Connection object to the database
  *
  * @return boolean False if problem, true otherwise
  */
  function queryEditeurPgsql($dbConn) {
    return parent::queryEditeur($dbConn);
  }

}
?>