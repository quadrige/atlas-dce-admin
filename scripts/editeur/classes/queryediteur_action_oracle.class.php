<?
include_once "queryediteur_action.class.php";

/**
 * @Class queryEditeurActionOracle
 *
 * @brief Ensemble des requetes actions li�es � l'application Actualites sp�cialis�es Oracle
 *         Les requetes standards se trouvent dans la classe m�re
 */
class QueryEditeurActionOracle extends QueryEditeurAction
{
  /**
   * @brief Constructeur
   */ 
  function QueryEditeurActionOracle(&$oDb)
  {
    parent::QueryEditeurAction($oDb);
  }
}
?>