<?
/**
 * @brief Classe permettant de nettoyer le code html g�n�rer par l'�diteur
 */ 
class HtmlCleaner extends AlkObject
{
  var $lastError = "";

  function HtmlCleaner() 
  {
  }
    
  function GetLastError() 
  {
    return $this->lastError;
  }
  
  function CleanHtml( 
    &$htmlCnt,                          // chaine a nettoyer
    $supprimer_tout_style = 1,          // supprimer tous les styles. supprime la mise en forme CSS donc...
    $supprimer_if = 1,                  // supprimer tout ce qui entre "if" microsoft
    $supprimer_espaces = 1,              // supprimer les dbl espaces (n�cessaires pour les 3 options suivantes)
    $supprimer_def_styles_inutiles = 1,  // virer les def de style CSS non utilis�es dans class=...
    $recherche_balises_approfondie = 0,  // recherche pr�cise de toutes les balises <h1>, <h2>, ...
    $supprimer_classe = 0,               // supprime le parametre class dans les balises
    $binder_nom_classes = 0,            // remplacer les noms des classes par d'autres plus courts
    $supprimer_les_style_none = 1,      // supprie tous les styles "border:none", etc... qui sont pris comme tels par d�faut
    $supprimer_trucs_word = 1,          // supprime diverses balises WORD
    $supprimer_balises_span = 1,        // supprime les balises <span> (pas leur contenu)
    $supprimer_toutes_balises = 0        // supprime toutes les balises
    )
  {
    
    //---------------
    // Do this before processing classes
    if ($supprimer_espaces)
    {
      $htmlCnt = str_replace( "\n", " ", $htmlCnt );
      $htmlCnt = str_replace( "\r", " ", $htmlCnt );
      $htmlCnt = str_replace( "\t", " ", $htmlCnt );
      $this->virerEspace( $htmlCnt );
    }
    
    // Remove IFs
    if ($supprimer_if)
    {
      $this->extractIf( $htmlCnt, 0 );
    }
    
    if ($supprimer_toutes_balises)
    {
      $htmlCnt = preg_replace( "/<style>[^<]*<\/style>/", "", $htmlCnt );  // ici on vire aussi le contenu
      $htmlCnt = preg_replace( "/<[^>]*>/", "", $htmlCnt );  
    }  
    
    if ($supprimer_def_styles_inutiles)
    {
      //---- Find used tags
      $lesClasses = array();
      // -1 within the styles class=...
      $balises = array();
      preg_match_all("(class=[^>]*)", $htmlCnt, $balises, PREG_SET_ORDER );
      
      for ($i=0;$i<sizeof($balises);$i++)
      {
        $good = explode(" ", $balises[$i][0]);
        if (strlen(substr($good[0],6))>0)
          $lesClasses[substr($good[0],6)] = 1;
      }
      
      if ($recherche_balises_approfondie)
      {
        // SLOOOOOOOOOOOOOW
        // -2 Directly tag names  <tagName>
        $balises = array();
        preg_match_all("(<[^>]*)", $htmlCnt, $balises, PREG_SET_ORDER );
        for ($i=0;$i<sizeof($balises);$i++)
        {
          $good = explode(" ", $balises[$i][0]);
          if (substr($good[0],1,1)=="/")
            continue;
          if (substr($good[0],1,1)=="!")
            continue;
          if (strlen(substr($good[0],1))>0)
            $lesClasses[substr($good[0],1)] = 1;
        }
      }
      else
      {
        $lesClasses["h1"] = 2;
        $lesClasses["h2"] = 2;
        $lesClasses["h3"] = 2;
        $lesClasses["h4"] = 2;
        $lesClasses["h5"] = 2;
        $lesClasses["h6"] = 2;
      }
      
      // end of research
      $balisesOk = "";
      foreach( $lesClasses as $k => $type )
      {
        if ($balisesOk)
          $balisesOk .= "|";
        $balisesOk .= $k;
      }
      $regExpression = "((".$balisesOk.") *\\{[^\\}]*\\})";
      // Find used styles
      $stylesDef = array();
      preg_match_all($regExpression, $htmlCnt, $stylesDef , PREG_SET_ORDER );
      $stylesDefString = "";
      for ($i=0;$i<sizeof($stylesDef);$i++)
      {
        $stylesDefString .= "\n".$stylesDef[$i][0]."\n";
      }
      
      if ($binder_nom_classes)
      {
        $i=0;
        foreach( $lesClasses as $k => $type )
        {
          if ($type==1)    // style
          {
            $htmlCnt = str_replace($k, "c".$i, $htmlCnt);
            $stylesDefString = str_replace($k, "c".$i, $stylesDefString);
            $i++;
          }
        }
      }
      
      // Remove all <style>  ... </style> tags
      $pLastStylePos = 0;
      $pStyleBegin = $this->strpoz( $htmlCnt, "<style>", $pLastStylePos );
      $pFirstStyleBegin = $pStyleBegin;
      if ($pStyleBegin != -1)
        $pStyleEnd = $this->strpoz( $htmlCnt, "</style>", $pStyleBegin );
      while ($pStyleBegin != -1)
      {
        $pLastStylePos = $pStyleEnd;
        $htmlCnt = substr( $htmlCnt, 0, $pStyleBegin ).substr( $htmlCnt,$pStyleEnd+8 );
        $pStyleBegin = $this->strpoz( $htmlCnt, "<style>", $pLastStylePos );
        if ($pStyleBegin != -1)
          $pStyleEnd = $this->strpoz( $htmlCnt, "</style>", $pStyleBegin );
      }
      
    }
    
    if ($supprimer_tout_style)
    {
      $htmlCnt = preg_replace( "/style=\"[^\"]*\"/", "", $htmlCnt );
      //$htmlCnt = ereg_replace( "style=\\\"[^\\\"]*\\\"", "", $htmlCnt );
    }
      
    if ($supprimer_les_style_none)
    {
      // C bon c par d�fault !
      $htmlCnt = str_replace( "text-decoration:none", "", $htmlCnt );
      $htmlCnt = str_replace( "text-underline:none", "", $htmlCnt );
      $htmlCnt = str_replace( "border-left:none", "", $htmlCnt );
      $htmlCnt = str_replace( "border-top:none", "", $htmlCnt );
      $htmlCnt = str_replace( "border-bottom:none", "", $htmlCnt );
      $htmlCnt = str_replace( "border-right:none", "", $htmlCnt );
    }
    
    if ($supprimer_trucs_word)
    {
      $htmlCnt = preg_replace( "/<v:[^>]*>/", "", $htmlCnt );
      $htmlCnt = preg_replace( "/</v[^>]*>/", "", $htmlCnt );
      $htmlCnt = preg_replace( "/style='tab-stops:[^']*'/", "", $htmlCnt );
      $htmlCnt = preg_replace( "/<o[^>]*><\/o:[^>]*>/", "", $htmlCnt );
            $htmlCnt = preg_replace( "/<o[^>]*>/", "", $htmlCnt );
            $htmlCnt = preg_replace( "/<\/o:[^>]*>/", "", $htmlCnt );
      $htmlCnt = preg_replace( "/<p[^>]*><\/p>/", "", $htmlCnt );
       $htmlCnt = preg_replace( "/mso-(^[';])*/", "", $htmlCnt );
       $htmlCnt = preg_replace( "/field-code-(^[';])*/", "", $htmlCnt );
      $htmlCnt = preg_replace( "/<?xml[^>]*\/>/", "", $htmlCnt );
      $htmlCnt = preg_replace( "/<?XML[^>]*\/>/", "", $htmlCnt );
      $htmlCnt = str_replace( "<?", "", $htmlCnt );
    }
    
    if ($supprimer_balises_span)
    {
      $htmlCnt = preg_replace( "/<span[^>]*>/", "", $htmlCnt );  
      $htmlCnt = preg_replace( "/<SPAN[^>]*>/", "", $htmlCnt );  
      $htmlCnt = str_replace( "</span>", "", $htmlCnt );
      $htmlCnt = str_replace( "</SPAN>", "", $htmlCnt );
    }
    
    if ($supprimer_classe)
    {
      $htmlCnt = preg_replace( "/class=[^ |^>]*/", "", $htmlCnt );  
    }
  
    // Last optim
    if ($supprimer_espaces)
    {
      $this->virerEspace( $htmlCnt );
    }  
  }  
  
  
  
  //--------------- PRIVATE FUNCTIONS -----------------
  
  function virerEspace( &$htmlCnt )
  {
    // much much faster than $htmlCnt = ereg_replace( " +", " ", $htmlCnt );
    // and works if there is less than 256 spaces at the same time
    $htmlCnt = str_replace( "                                ", " ", $htmlCnt);
    $htmlCnt = str_replace( "                ", " ", $htmlCnt);
    $htmlCnt = str_replace( "        ", " ", $htmlCnt);
    $htmlCnt = str_replace( "    ", " ", $htmlCnt);
    $htmlCnt = str_replace( "  ", " ", $htmlCnt);
  }
  
  function extractIf( &$str, $pos )
  {
    $pIf1 = $this->strpoz($str, "<![if", $pos);
    $pIf2 = $this->strpoz($str, "<!--[if", $pos);
    $pIf = $this->zmin( $pIf1 ,$pIf2 );
    if ($pIf>=0)
    {
      $pIfEnd = $this->strpoz($str, ">", $pIf);
      $pNextIf1 = $this->strpoz($str, "<![if", $pIfEnd);
      $pNextIf2 = $this->strpoz($str, "<!--[if", $pIfEnd);
      $pNextIf = $this->zmin( $pNextIf1, $pNextIf2 );
      if ($pNextIf>=0)
      {
        $this->extractIf( $str, $pNextIf );
      }
      $pNextEndIf1 = $this->strpoz($str, "<![endif]", $pIfEnd);
      $pNextEndIf2 = $this->strpoz($str, "<![endif]", $pIfEnd);
      $pNextEndIf = $this->zmin( $pNextEndIf1, $pNextEndIf2 );
      
      $pNextEndIfEnd1 = $this->strpoz($str, ">", $pNextEndIf);
      $pNextEndIfEnd2 = $this->strpoz($str, ">", $pNextEndIf);
      $pNextEndIfEnd = $this->zmin( $pNextEndIfEnd1, $pNextEndIfEnd2 );
  
      $pCond = $this->strpoz($str, "[", $pIf);
      $ifCondition = substr( $str, $pCond+1+2+1, $pIfEnd-$pCond-2-2-1 );
      
      $oki = false;
      if ($ifCondition=="!vml")
      {
        $oki = true;
      }
      $insideIf = "";
            
      //  $pos    $pIf  $pIfEnd        $pNextEndIf  $pNextEndIfEnd
      //    ....  <![if...  >    ...    <![end if]>            ....
      if ($oki)
      {
        $insideIf = substr( $str, $pIfEnd+1,$pNextEndIf-($pIfEnd+1) );
      }

      $str =     substr( $str, 0, $pIf ).
            $insideIf.
            substr( $str, $pNextEndIfEnd+1 );
    }
    else
    {
      return substr($str, $pos);
    }
  }
  
  function zmin( $p1, $p2 )
  {
    return (($p1>=0)&&(($p1<$p2)||($p2==-1)))?$p1:$p2;
  }
  
  function strpoz( $mystring, $findme, $start )
  {
    $res = @strpos( $mystring, $findme, $start );
    if ($res===false)
      return -1;
    return $res;
  }
  
  function recursiveDirDelete($thePath)
  {  
    if(false === @is_dir($thePath))
    {
      @unlink($thePath); 
      clearstatcache();
      if (@file_exists($thePath))
      {
        if(substr_count($thePath,":"))
        {
          @system("del ".preg_replace("/\//i","\\",$thePath));
        }
        else
        {
          @system("rm $thePath");  
        }
      }
      
      clearstatcache();
      
      if (@file_exists($thePath))
      {  
        return false;  
      }
      else
      {  
        return true;
      }
    }
    else
    {
      $dh = @opendir($thePath);
      while(($file = @readdir($dh)) !==false )
      {
        if($file != "." && $file != "..")
        {
          $fullpath = $thePath.$file; 
          if(@is_dir($fullpath)) $fullpath.="/";
          if (!$this->recursiveDirDelete($fullpath))
          {
            closedir($dh);
            return false;
          }
        }
      }
      @closedir($dh);
      @rmdir($thePath); 
      clearstatcache();
      if (@file_exists($thePath)) 
      { 
        if(substr_count($thePath,":"))
        {
          @system("del ".preg_replace("/\//i","\\",$thePath));
        }
        else
        {
          @system("rmdir $thePath");  
        }
      }
      
      clearstatcache();
      if (@file_exists($thePath))
      {
        return false;  
      }
      else
      {
        return true;
      }
    }
  } 
  
  function ConvertHtml(&$htmlCnt)
  {
    $strTmp="";
    $strTextHtml="";
    $tabHtml=array();
    $tagDebInsert = "<TEXT>";
    $tagFinInsert = "</TEXT>";
    
    $bBalise = false;
    $bTextOuv = false;
    $bParam = false;
    
    $strTextHtml = $htmlCnt;
    
    if ( substr($strTextHtml, 0, 4)!="<DIV" )
      $strTextHtml = "<DIV>".$strTextHtml."</DIV>";
    
    $strTextHtml = preg_replace("/<P>/", "<P align=\"justify\">", $strTextHtml); 
    $strTextHtml = preg_replace("/<BR[^>]*>/","<BR/>", $strTextHtml);
    $strTextHtml = preg_replace("/<HR[^>]*>/","<HR/>", $strTextHtml);
    $strTextHtml = preg_replace("/<PR>/", "<PR/>", $strTextHtml);
    $strTextHtml = preg_replace("/<TEXT>/","", $strTextHtml);
    $strTextHtml = preg_replace("/<\/TEXT>/","", $strTextHtml);
    $strTextHtml = preg_replace("/<\/IMG>/", "", $strTextHtml);
    $strTextHtml = preg_replace("/<\/AREA>/"," ", $strTextHtml);
    $strTextHtml = preg_replace("/<DIV><\/DIV>/","", $strTextHtml);
    $strTextHtml = preg_replace("/selected>/","selected=\"1\">", $strTextHtml);
    $strTextHtml = preg_replace("/CHECKED/","CHECKED=\"1\" ", $strTextHtml);
    $strTextHtml = preg_replace("/noWrap /","noWrap=\"1\" ", $strTextHtml);
                
    $this->convertIsoToUnicode($strTextHtml);
    
    $tabHtml = preg_split('//', $strTextHtml, -1, PREG_SPLIT_NO_EMPTY);
    
    for ($i=0; $i<count($tabHtml);$i++)
    {
      $strTmp .= $tabHtml[$i];
      
      if ( $tabHtml[$i]=='<' )
        $bBalise=true;
      else
      {
        if ( $tabHtml[$i]=='>' && ($i+1)<count($tabHtml) )
        {
          if ($tabHtml[$i+1]!='<' && $bBalise==true)
          {
            $strTmp .= $tagDebInsert;
            $bTextOuv = true;
            $bBalise = false;
          }
        }
        else
        {
          if (($i+1)<count($tabHtml))
          {
            if ($tabHtml[$i+1]=='<' && $bBalise==false)
            {
              $strTmp .= $tagFinInsert;
              $bTextOuv = false;
            }
          }
        }
      }

      //param�tres sans guillemets
      if ( $bBalise==true )
      {
        if ( $bParam==false )
        {
          if ( $tabHtml[$i]=='=' && ($i+1)<count($tabHtml) )
          {
            if ( $tabHtml[$i+1]!='"' )
              $strTmp .= '"';
              
            $bParam = true;
          }
        }
        else
        {
          if ( ($i+1)<count($tabHtml) && ($tabHtml[$i+1]==' ' || $tabHtml[$i+1]=='>') )
          {
            if ( $tabHtml[$i]!='"' )
              $strTmp .= '"';
            $bParam = false;
          }
        }
      }
    }
    
    if ( $bTextOuv==true )
      $strTmp .= $tagFinInsert;

    $this->ConvertImg($strTmp);
    $this->ConvertParam($strTmp);
    $this->ConvertArea($strTmp);
    $this->ConvertInput($strTmp);
    
    $htmlCnt = $strTmp;
    
    return $strTmp;
  }


  function ConvertImg(&$htmlCnt)
  {
    $strTmp="";
    $tabHtml=array();

    $bImg = false;

    if ( strstr( $htmlCnt, "<IMG")!= false )
    {
      // Il y a une image
      $tabHtml = preg_split('//', $htmlCnt, -1, PREG_SPLIT_NO_EMPTY);

      for ($i=0; $i<count($tabHtml);$i++)
      {
        $strTmp .= $tabHtml[$i];

        if ( $bImg==true && $tabHtml[$i]=='>' )
        {
          $strTmp .= "</IMG>";
          $bImg = false;
        }
        else
        {
          if ( $tabHtml[$i]=='<' && ($i+3)<count($tabHtml) )
          {
            // Balise < et inf�rieur � la longueur
            if ( $tabHtml[$i+1]=='I' && $tabHtml[$i+2]=='M' && $tabHtml[$i+3]=='G' )
            {
              // Nous sommes sur l'image
              $strTmp .= "IMG ";
              $bImg = true;
              $i += 4;
            }
          }
        }
      }
    }
    else
    {
      $strTmp = $htmlCnt;
    }

    $htmlCnt = $strTmp;
  }

  function ConvertParam(&$htmlCnt)
  {
    $strTmp = "";
    $tabHtml = array();

    $bImg = false;

    if ( $htmlCnt.("<PARAM")!=0 )
    {
      // Il y a une image
      $tabHtml = preg_split('//', $htmlCnt, -1, PREG_SPLIT_NO_EMPTY);

      for ($i=0; $i<count($tabHtml);$i++)
      {
        $strTmp .= $tabHtml[$i];

        if ( $bImg==true && $tabHtml[$i]=='>' )
        {
          $strTmp .= "</PARAM>";
          $bImg = false;
        }
        else
        {
          if ( $tabHtml[$i]=='<' && ($i+5)<count($tabHtml) )
          {
            // Balise < et inf�rieur � la longueur
            if ( $tabHtml[$i+1]=='P' && $tabHtml[$i+2]=='A' && $tabHtml[$i+3]=='R' && $tabHtml[$i+4]=='A' && $tabHtml[$i+5]=='M' )
            {
              // Nous sommes sur l'image
              $strTmp .= "PARAM ";
              $bImg = true;
              $i += 6;
            }
          }
        }
      }
    }
    else
    {
      $strTmp = $htmlCnt;
    }

    $htmlCnt = $strTmp;
  }

  function ConvertArea(&$htmlCnt)
  {
    $strTmp="";
    $tabHtml=array();

    $bImg = false;

    if ( strstr( $htmlCnt, "<AREA")!= false )
    {
      // Il y a une image
      $tabHtml = preg_split('//', $htmlCnt, -1, PREG_SPLIT_NO_EMPTY);

      for ($i=0; $i<count($tabHtml);$i++)
      {
        $strTmp .= $tabHtml[$i];

        if ( $bImg==true && $tabHtml[$i]=='>' )
        {
          $strTmp .= "</AREA>";
          $bImg = false;
        }
        else
        {
          if ( $tabHtml[$i]=='<' && ($i+3)<count($tabHtml) )
          {
            // Balise < et inf�rieur � la longueur
            if ( $tabHtml[$i+1]=='A' && $tabHtml[$i+2]=='R' && $tabHtml[$i+3]=='E' && $tabHtml[$i+4]=='A' )
            {
              // Nous sommes sur l'image
              $strTmp .= "AREA ";
              $bImg = true;
              $i += 4;
            }
          }
        }
      }
    }
    else
    {
      $strTmp = $htmlCnt;
    }

    $htmlCnt = $strTmp;
  }
    
  function ConvertInput(&$htmlCnt)
  {
    $strTmp="";
    $tabHtml=array();

    $bInput = false;

    if ( strstr( $htmlCnt, "<INPUT")!= false )
    {
      // Il y a une balise input
      $tabHtml = preg_split('//', $htmlCnt, -1, PREG_SPLIT_NO_EMPTY);

      for ($i=0; $i<count($tabHtml);$i++)
      {
        $strTmp .= $tabHtml[$i];

        if ( $bInput==true && $tabHtml[$i]=='>' )
        {
          $strTmp .= "</INPUT>";
          $bInput = false;
        }
        else
        {
          if ( $tabHtml[$i]=='<' && ($i+5)<count($tabHtml) )
          {
            // Balise < et inf�rieur � la longueur
            if ( $tabHtml[$i+1]=='I' && $tabHtml[$i+2]=='N' && $tabHtml[$i+3]=='P' && $tabHtml[$i+4]=='U' && $tabHtml[$i+5]=='T' )
            {
              // Nous sommes sur l'image
              $strTmp .= "INPUT ";
              $bInput = true;
              $i += 5;
            }
          }
        }
      }
    }
    else
    {
      $strTmp = $htmlCnt;
    }

    $htmlCnt = $strTmp;
  }
    
    
  function getCleanHtml(&$htmlCnt, $bDiv = false)
  {
    $htmlCnt = preg_replace ( "/<TEXT>/","", $htmlCnt);
    $htmlCnt = preg_replace ( "/<\/TEXT>/", "", $htmlCnt);
    $htmlCnt = preg_replace("/<\/IMG>/", "", $htmlCnt);
    $htmlCnt = preg_replace("/<\/PARAM>/", "", $htmlCnt);
    $htmlCnt = preg_replace("/<\/AREA>/", "", $htmlCnt);
    $htmlCnt = preg_replace("/<\/INPUT>/", "", $htmlCnt);
    $htmlCnt = preg_replace("/<DIV><\/DIV>/","", $htmlCnt);
    
    if ( $bDiv==true )
    {
      $htmlCnt = preg_replace("/<DIV>/","", $htmlCnt);
      $htmlCnt = preg_replace("/<\/DIV>/","", $htmlCnt);
    }
  }
    
  function convertIsoToUnicode(&$htmlCnt)
  {
    $tabTrans = array("�" => "&#226;",
                      "�" => "&#228;",
                      "�" => "&#233;",
                      "�" => "&#232;",
                      "�" => "&#234;",
                      "�" => "&#235;",
                      "�" => "&#238;",
                      "�" => "&#239;",
                      "�" => "&#244;",
                      "�" => "&#246;",
                      "�" => "&#249;",
                      "�" => "&#251;",
                      "�" => "&#252;",
                      "&quot;"   => "&#34;",
                      "&amp;"    => "&#38;",
                      "&lt;"     => "&#60;",
                      "&gt;"     => "&#62;",
                      "&nbsp;"   => "&#160;",
                      "&acute;"  => "&#180;",
                      "&Icirc;"  => "&#206;",
                      "&Iuml;"   => "&#207;",
                      "&Ograve;" => "&#210;",
                      "&Oacute;" => "&#211;",
                      "&Ocirc;"  => "&#212;",
                      "&Ouml;"   => "&214#;",
                      "&Ugrave;" => "&#217;",
                      "&Uacute;" => "&#218;",
                      "&Ucirc;"  => "&#219;",
                      "&Uuml;"   => "&#220;",
                      "&agrave;" => "&#224;",
                      "&aacute;" => "&#225;",
                      "&acirc;"  => "&#226;",
                      "&auml;"   => "&#228;",
                      "&aelig;"  => "&#230;",
                      "&ccedil;" => "&#231;",
                      "&egrave;" => "&#232;",
                      "&eacute;" => "&#233;",
                      "&ecirc;"  => "&#234;",
                      "&euml;"   => "&#235;",
                      "&igrave;" => "&#236;",
                      "&iacute;" => "&#237;",
                      "&icirc;"  => "&#238;",
                      "&iuml;"   => "&#239;",
                      "&Agrave;" => "&#192;",
                      "&Aacute;" => "&#193;",
                      "&Acirc;"  => "&#194;",
                      "&Auml;"   => "&#196;",
                      "&Egrave;" => "&#200;",
                      "&Eacute;" => "&#201;",
                      "&Ecirc;"  => "&#202;",
                      "&Euml;"   => "&#203;",
                      "&Igrave;" => "&#204;",
                      "&Iacute;" => "&#205;",
                      "&ograve;" => "&#242;",
                      "&oacute;" => "&#243;",
                      "&ocirc;"  => "&#244;",
                      "&ouml;"   => "&#246;",
                      "&ugrave;" => "&#249;",
                      "&uacute;" => "&#250;",
                      "&ucirc;"  => "&#251;",
                      "&uuml;"   => "&#252;",
                      "&laquo;"  => "&#171;",
                      "&raquo;"  => "&#187;",
                      "&rsquo;"  => "'",
                      "&shy;"    => "&#173;",
                      "&ndash;"  => "-",
                      "&quot;"   => "&#34;",
                      "&oelig;"  => "&#156;",
                      "&deg;"    => "&#176;");

    $htmlCnt = strtr($htmlCnt, $tabTrans);
  }
  
  /**
  * @author      :  AS
  * @create      :  09/11/2004
  * @update      :
  * @description :  Corrige le code HTML � l'aide de TIDY et retourne de l'html ou du xml
  *
  * @param $htmlCnt code html � corriger
  * @param $bXmlOutput d�termine le format de sortie (html ou xml) du code html corrig� (html par d�faut)
  * 
  * @return boolean False if problem, true otherwise
  */
  function cleanWithTidy(&$htmlCnt, $bXmlOutput=false)
  {
    if( defined("ALK_B_TIDY_EDITEUR")==false || 
        (defined("ALK_B_TIDY_EDITEUR")==true && ALK_B_TIDY_EDITEUR==true ) ) {
      $php_version = substr(phpversion(), 0, 1);
      if ($php_version=="5"){
        $config = array('indent'                      => FALSE,
                      'bare'                        => FALSE, //si TRUE alors on ne voit plus les cellules vides d'un tableau
                      'word-2000'                   => TRUE,
                      'input-xml'                   => TRUE,
                      'output-xhtml'                => !$bXmlOutput,
                      'output-xml'                  => $bXmlOutput,
                      'drop-proprietary-attributes' => TRUE,
                      'wrap'                        => 12000,
                      'uppercase-tags'              => FALSE,
                      'drop-empty-paras'            => FALSE,
                      'newline'                     => 1);
        
        tidy_clean_repair(tidy_parse_string($htmlCnt, $config));
        $htmlCnt = tidy_get_output(tidy_parse_string($htmlCnt, $config));
      } else {
        tidy_parse_string($htmlCnt);
            
        tidy_setopt('indent', FALSE);
        tidy_setopt('bare', FALSE); //si TRUE alors on ne voit plus les cellules vides d'un tableau
        tidy_setopt('word-2000', TRUE);
        tidy_setopt('input-xml', TRUE);
        tidy_setopt('drop-proprietary-attributes', TRUE);
        tidy_setopt('wrap', 12000);
        tidy_setopt('uppercase-tags', TRUE);
        tidy_setopt('drop-empty-paras', FALSE);
        tidy_setopt('newline', 1);
      
        if ( $bXmlOutput==true )
          tidy_setopt('output-xml', TRUE);
  
        tidy_clean_repair();
        
        $htmlCnt = tidy_get_output();
      }
      
    } else {
      // remplace les caract�res de type &...; en &#...;
      $this->convertIsoToUnicode($htmlCnt);
    }
    
    $htmlCnt = preg_replace("/.*<[Bb][Oo][Dd][Yy]>/","", $htmlCnt);
    $htmlCnt = preg_replace("/<\/[Bb][Oo][Dd][Yy]>.*/","", $htmlCnt);
    
    $htmlCnt = preg_replace( "/<\?[Xx][Mm][Ll][^>]*\/>/", "", $htmlCnt );
    $htmlCnt = str_replace( "<\?", "", $htmlCnt );
    //$htmlCnt = str_replace( chr(13).chr(10), "", $htmlCnt );
    
    if ( $bXmlOutput==true )
      $this->addTagText($htmlCnt);
  }
  
  function addTagText(&$htmlCnt)
  {
    $strTmp="";
    $strTextHtml="";
    $tabHtml=array();
    $tagDebInsert = "<TEXT>";
    $tagFinInsert = "</TEXT>";
    
    $bBalise = false;
    $bTextOuv = false;
    $bParam = false;
    
    $strTextHtml = trim($htmlCnt);
    
    //if ( substr($strTextHtml, 0, 4)!="<DIV" )
    //  $strTextHtml = "<DIV>".$strTextHtml."</DIV>";
    
    $strTextHtml = preg_replace("/<P>/", "<P align=\"justify\">", $strTextHtml); 
    $strTextHtml = preg_replace("/<TEXT>/","", $strTextHtml);
    $strTextHtml = preg_replace("/<\/TEXT>/","", $strTextHtml);
    
    $tabHtml = preg_split('//', $strTextHtml, -1, PREG_SPLIT_NO_EMPTY);
    
    for ($i=0; $i<count($tabHtml);$i++)
    {
      if ( $i==0 && $tabHtml[$i]!='<' && ($i+1)<count($tabHtml) ){
        $strTmp .= $tagDebInsert.$tabHtml[$i];
        $bTextOuv = true;
      }
      else
        $strTmp .= $tabHtml[$i];
      
      if ( $tabHtml[$i]=='<' )
        $bBalise=true;
      else
      {
        if ( $tabHtml[$i]=='>' && ($i+1)<count($tabHtml) )
        {
          if ($tabHtml[$i+1]!='<' && $bBalise==true)
          {
            $strTmp .= $tagDebInsert;
            $bTextOuv = true;
            $bBalise = false;
          }
        }
        else
        {
          if (($i+1)<count($tabHtml))
          {
            if ($tabHtml[$i+1]=='<' && $bBalise==false)
            {
              $strTmp .= $tagFinInsert;
              $bTextOuv = false;
            }
          }
        }
      }
    }
    
    if ( $bTextOuv==true )
      $strTmp .= $tagFinInsert;

    $htmlCnt = $strTmp;
  } 
}  
?>