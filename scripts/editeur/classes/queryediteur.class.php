<?php

/**
 * @class queryEditeur
 * @brief Contains the SQL request for the editor
 */
class queryEditeur extends AlkQuery {

  
  /**
   * @description :  Constructor of the class
   *
   * @param dbConn Connection object to the database
   */
  function queryEditeur(&$dbConn) 
  {
    parent::AlkQuery($dbConn);
  }
  
  
  /**
   * @brief  obtention d'un contenu
   *
   * @param int idEnt identifiant du contenu (s�par� par des | si cl�s multiples)
   * @param string strTable nom de la table
   * @param string strChampHtml nom du champ de la table contenant le contenu
   * @param string strChampId identifiant du contenu (s�par� par des | si cl�s multiples)
   *
   * @return string contenu
   */
  function getContenu($idEnt, $strTable, $strChampHtml, $strChampId) 
  {
    $strContenu = "";     
    $tabChampId = explode("|", $strChampId);
    $tabId = explode("|", $idEnt);
      
    $strSql = "select ".$strChampHtml." from ".$strTable." where 1=1";
      
    // gestion des cles multiples
    $i = 0;
    foreach ($tabChampId as $strChamp) {
      $strSql .= " and ".$strChamp."='".$tabId[$i]."'";
      $i += 1;
    }
      	
    $dsContenu = $this->dbConn->initDataset( $strSql );
    
    if ($drContenu = $dsContenu->GetRowIter()) {
      //if ($drContenu->GetValueName($strChampHtml) != "")
      //  $strContenu = ereg_replace("\"", "'", $drContenu->GetValueName($strChampHtml));
      $strContenu = $drContenu->GetValueName($strChampHtml);  
    }
      
    return $strContenu;
  }
}
?>