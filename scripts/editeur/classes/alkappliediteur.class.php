<?php
include_once "../../classes/appli/alkapplication.class.php";

/**
 *  Classe de l'application Editeur
 *
 * Classe regroupant des fonctionnalit�s de l'�diteur disponibles
 * pour d'autres applications
 */
class AlkAppliEditeur extends AlkApplication
{
  /**
   * @brief Constructeur par d�faut
   *
   * @param oSpace R�f�rence vers l'objet de l'espace en cours
   */
  function AlkAppliEditeur(&$oSpace, $appli_id, $agent_id, $strUrlUpload, $strPathUpload)
  {
    parent::AlkApplication($oSpace, $appli_id, $agent_id, $strUrlUpload, $strPathUpload);

    $this->appli_id = $appli_id;
    $this->atype_id = 21;

    $this->oQuery = "";
    $this->oQueryAction = "";

    global $queryActu, $queryActuAction, $ALK_B_FDOC_PARAM;

    if( isset($queryActu) && isset($queryActuAction) ) {
      $this->oQuery =& $queryActu;
      $this->oQueryAction =& $queryActuAction;
    } else {
      include_once "../actu/classes/queryactu_".$oSpace->typeDB.".class.php";
      //include_once "../actu/classes/queryactu_action_".$oSpace->typeDB.".class.php";
      switch( $oSpace->typeDB ) {
      case SGBD_ORACLE :
        $this->oQuery = new QueryActuOracle($oSpace->dbConn);
        //$this->oQueryAction = new QueryActuActionOracle($oSpace->dbConn);
        break;

      case SGBD_MYSQL :
        $this->oQuery = new QueryActuMysql($oSpace->dbConn);
        //$this->oQueryAction = new QueryActuActionMysql($oSpace->dbConn);
        break;
        
      case SGBD_MYSQL :
        $this->oQuery = new QueryActuPgsql($oSpace->dbConn);
        //$this->oQueryAction = new QueryActuActionPgsql($oSpace->dbConn);
        break;
              
      default :
        echo "Impossible d'instancier les classes actu.";
        exit();
      }
    }    
    
    $this->setTabParam();
    $this->setTabSheets();
    $this->setAlkArbre();
  }
}
?>