<?php
include_once('queryediteur.class.php');

/**
 * @class queryEditeurMySql
 * @brief  Class which herits to the queryEditeur Class
 */
class queryEditeurMySql extends queryEditeur
{

  /**
   * @brief Constructor of the class
   *
   * @param dbConn Connection object to the database
   */
  function queryEditeurMySql(&$dbConn) 
  {
    parent::queryEditeur($dbConn);
  }

}
?>