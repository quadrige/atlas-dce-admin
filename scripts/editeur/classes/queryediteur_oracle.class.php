<?php
include_once('queryediteur.class.php');

/**
 * @class queryEditeurOracle
 * @brief Class which herits to the queryEditeur Class
 */
class queryEditeurOracle extends queryEditeur
{
  /**
   * @brief Constructor of the class
   *
   * @param dbConn Connection object to the database
   */
  function queryEditeurOracle(&$dbConn) 
  {
    parent::queryEditeur($dbConn);
  }

}
?>