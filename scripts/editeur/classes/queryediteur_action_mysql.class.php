<?
include_once "queryediteur_action.class.php";

/**
 * @Class queryEditeurActionMysql
 *
 * @brief  Ensemble des requetes actions li�es � l'application Actualites sp�cialis�es Oracle
 *         Les requetes standards se trouvent dans la classe m�re
 */
class QueryEditeurActionMysql extends QueryEditeurAction
{
  /**
   * @brief Constructeur
   */ 
  function QueryEditeurActionMysql(&$oDb)
  {
    parent::QueryEditeurAction($oDb);
  }
}
?>