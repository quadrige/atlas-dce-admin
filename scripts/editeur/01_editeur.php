<?
include_once ("lib/lib_session.php");
include_once ("api/gen_con.php");
include_once ("lib/lib_aff.php");
include_once ("lib/lib_menu_popup.php");
include_once ("classes/htmlCleaner.class.php");
include_once ("classes/fckeditor.php");

$cont_id = Request("cont_id", REQ_GET, "-1", "is_numeric");
$cont_appli_id = Request("cont_appli_id", REQ_GET, "-1", "is_numeric");
$menu_appli_id = Request("menu_appli_id", REQ_GET, "1", "is_numeric");
$page = Request("page", REQ_GET, "1", "is_numeric");
$idEnt = Request("idEnt", REQ_GET, "0", "is_numeric"); // identifiant de l'entite (separateur pipe si plusieurs cles)
$strTable = Request("table", REQ_GET, ""); // nom de la table concern�e
$strChampHtml = Request("champsHtml", REQ_GET, ""); // nom du champ de la table qui va contenir le code HTML g�n�r� l'�diteur
$strChampId = Request("champsId", REQ_GET, ""); // nom du champ correspondant � l'identifiant de l'entit� (separateur pipe si plusieurs cles)
$strRepUpload = Request("repUpload", REQ_GET, ""); // nom du champ correspondant au r�pertoire d'upload des pi�ces jointe
$bReload = Request("reload", REQ_GET, 0, "is_numeric"); // � 1 si la page m�re doit �tre rafraichie
$bForm = Request("bform", REQ_GET, 0, "is_numeric"); // � 1 si les �l�ments de formulaire doivent �tre affich�s

/* Ajout� par MLA le 29/12/05 */
$post_appli_id = Request("post_appli_id", REQ_GET, "-1", "is_numeric");//identifiant de l'appli charg�e de faire les actions Pre et Post update bloc
$bloc_id = Request("bloc_id", REQ_GET, "-1", "is_numeric");//identifiant du bloc sur lequel se font les Pre et Post update bloc

/* Modifi� par MLA le 29/12/05 */
$strParam = "cont_appli_id=".$cont_appli_id."&cont_id=".$cont_id."&menu_appli_id=".$menu_appli_id.
	"&idEnt=".$idEnt."&table=".$strTable."&champsHtml=".$strChampHtml."&champsId=".$strChampId.
	"&repUpload=".$strRepUpload."&reload=".$bReload."&post_appli_id=".$post_appli_id."&bloc_id=".$bloc_id;
	
$iProfilCont = $oSpace->iProfilCont;
$iProfilAppli = $oSpace->iDroitAppli;

if( $iProfilAppli==0 ) {
  header("location:".ALK_URL_IDENTIFICATION."?err=2");
  exit();
}

$strFolder = ALK_ROOT_PATH.ALK_SIALKE_DIR.$strRepUpload;
if ( !is_dir($strFolder) )
  mkdir ($strFolder);

if ( $bForm==1 )
{
  $iHeightIcones = 46;
  $iHeightIframe = 484;
}
else
{
  $iHeightIcones = 28;
  $iHeightIframe = 500;
}

/*
 * Ajout� par MLA le 29/12/05
 * @callservice  post_appli_id.CallServicePreUpdateBloc(idEnt)
 */
$strScript = "";
if ($post_appli_id!=-1){
  include_once("../../classes/appli/alkserviceappli.class.php");
	$oService = new AlkServiceAppli($post_appli_id, $oSpace, $cont_appli_id, $_SESSION["sit_idUser"]);
	$oRes = null;
	$strScript = $oService->CallService("PreUpdateBloc", $oRes, $bloc_id, $_SESSION["sit_idUser"], true); 
}

$HtmlCleaner = new HtmlCleaner();
 
$strContenu = $queryEditeur->getContenu($idEnt, $strTable, $strChampHtml, $strChampId); 
$HtmlCleaner->getCleanHtml($strContenu, false);

$tabEventBody = array ("onload" => "gestionEvt();window.focus();"); 
aff_menu_haut($tabEventBody);

//echo $strScript;
?>

<script language="JavaScript">
<!--
    
//--------------------------------------------------
// Validate
// But : Valider le contenu
//--------------------------------------------------
function Validate()
{
  var f= document.formTexte;
<? if( defined("ALK_ENCRYPT_EDITEUR") && ALK_ENCRYPT_EDITEUR ) { ?>
  var oFck = FCKeditorAPI.__Instances["TextHtml"];
  oFck.EditorDocument.body.innerHTML = TextEncode(oFck.GetXHTML());
<? } ?>
  f.submit();
}
function CloseEditor()
{
	<? if ($post_appli_id!=-1){ ?>
		var url = '../espace/serv_inter_appli.php?<?=$strParam?>&atype_id=<?=$post_appli_id?>&iServ=3&bCloseEditor=1';
  	document.location = url;
	<? } else { ?>
		window.close();
	<? } ?>		
}
-->
</script>

	<FORM name="formTexte" action="01_editeur_sql.php?<?=$strParam?>"  method="post">
  	<table cellSpacing="0" cellPadding="0" width="100%" border="0">
  		<tr>
    		<td width="1" bgColor="#000000"><IMG height="1" src="<?=ALK_URL_SI_IMAGES?>dot.gif" width="1"></td>
      	<td bgColor="#000000"><IMG height="1" src="<?=ALK_URL_SI_IMAGES?>dot.gif" width="1"></td>
      	<td width="1" bgColor="#000000"><IMG height="1" src="<?=ALK_URL_SI_IMAGES?>dot.gif" width="1"></td>
    	</tr>
     	<tr>
        <td width="1" bgColor="#000000"><IMG height="1" src="<?=ALK_URL_SI_IMAGES?>dot.gif" width="1"></td>
        <td height="<?=$iHeightIframe?>" class="txt" bgColor="#FFFFFF">
      	<?
          $sBasePath = ALK_SIALKE_URL."scripts/editeur/"; 

          $oFCKeditor = new FCKeditor('TextHtml') ;
          $oFCKeditor->BasePath = $sBasePath ;
          
          $oFCKeditor->Config['EditorAreaCSS'] = ( ALK_STR_EDITEUR_CSS!= "" 
                                             ? ALK_STR_EDITEUR_CSS 
                                             : ALK_SIALKE_URL."styles/site_gen.css") ;
          
         
          $strPathXml = ALK_SIALKE_PATH."styles/".$cont_id."_fckstyles.xml";
          //echo $strPathXml."<br />";
          if( file_exists($strPathXml) && is_file($strPathXml) ) {
            $oFCKeditor->Config['StylesXmlPath'] = ALK_SIALKE_URL."styles/".$cont_id."_fckstyles.xml";
          } else {
            $oFCKeditor->Config['StylesXmlPath'] = ALK_SIALKE_URL."styles/fckstyles.xml";
          }
          $strPathTemplatesXml = ALK_SIALKE_URL."styles/".$cont_id."_fcktemplates.xml";
          if( file_exists($strPathTemplatesXml) && is_file($strPathTemplatesXml) ) {
            $oFCKeditor->Config['TemplatesXmlPath'] = ALK_SIALKE_URL."styles/".$cont_id."_fcktemplates.xml";
          } else {
            $oFCKeditor->Config['TemplatesXmlPath'] = ALK_SIALKE_URL."styles/fcktemplates.xml";
          }
          
          /*$oFCKeditor->Config['BodyId'] = ( defined("ALK_EDITEUR_BODYID") ? ALK_EDITEUR_BODYID : "" );
 		  $oFCKeditor->Config['BodyClass'] = ( defined("ALK_EDITEUR_BODYCSS") ? ALK_EDITEUR_BODYCSS : "" );
          echo "ALK_EDITEUR_BODYID : " . ALK_EDITEUR_BODYID . "<br /> ALK_EDITEUR_BODYID:". ALK_EDITEUR_BODYID ."<br />";*/
          //echo $strRepUpload."<br />";
          
          $oFCKeditor->Height   = "530"; 
          $oFCKeditor->Config['AutoDetectLanguage']  = false;
          $oFCKeditor->Config['DefaultLanguage']   = "fr";
          $oFCKeditor->Config['ImageBrowserURL'] = $sBasePath."editor/filemanager/browser/default/browser.html".
			"?Type=&Connector=../../connectors/php/connector.php&ServerPath=".ALK_SIALKE_DIR.$strRepUpload."&UrlRoot=".ALK_ROOT_URL;
		  $oFCKeditor->Config['LinkBrowserURL'] = $sBasePath."editor/filemanager/browser/default/browser.html".
			"?Type=&Connector=../../connectors/php/connector.php&ServerPath=".ALK_SIALKE_DIR.$strRepUpload."&UrlRoot=".ALK_ROOT_URL;
		  $oFCKeditor->Config['FlashBrowserURL'] = $sBasePath."editor/filemanager/browser/default/browser.html".
			"?Type=&Connector=../../connectors/php/connector.php&ServerPath=".ALK_SIALKE_DIR.$strRepUpload."&UrlRoot=".ALK_ROOT_URL;
		  $oFCKeditor->Config['ImageUploadURL'] = $sBasePath."editor/filemanager/upload/php/upload.php".
			"?Type=&ServerPath=".ALK_SIALKE_DIR.$strRepUpload."&UrlRoot=".ALK_ROOT_URL;
		  $oFCKeditor->Config['LinkUploadURL'] = $sBasePath."editor/filemanager/upload/php/upload.php".
			"?Type=&ServerPath=".ALK_SIALKE_DIR.$strRepUpload."&UrlRoot=".ALK_ROOT_URL;
		  $oFCKeditor->Config['FlashUploadURL'] = $sBasePath."editor/filemanager/upload/php/upload.php".
			"?Type=&ServerPath=".ALK_SIALKE_DIR.$strRepUpload."&UrlRoot=".ALK_ROOT_URL;
          
         /* Si le fichier de config est personnalis� pour le projet et qu'il se trouve dans le r�pertoire styles
	     * on l'utilise comme fichier de config, sinon c'est celui par d�faut qui est utilis� (editeur/fckconfig.js)
	     */
	    $strPathConfig = ALK_SIALKE_URL."styles/fckconfig.js";
	    if ( file_exists($strPathConfig) && is_file($strPathConfig) )
	    	$oFCKeditor->Config['CustomConfigurationsPath'] = ALK_ALKANET_ROOT_URL."styles/fckconfig.js" ;
          
          
          if ($bForm==1)
            $oFCKeditor->ToolbarSet = "Formulaire";

          $oFCKeditor->Value = $strContenu ;
          
		    /* capture du html g�n�r� dans $strHtmlFCKEdit */
		    //ob_start();
		    $oFCKeditor->Create();
		    //$strHtmlFCKEdit = ob_get_contents();
		    //ob_end_clean();
		    
		/*echo "<pre>";
		print_r($oFCKeditor->Config);
		echo "</pre>";*/

          ?>
        	<script language="javascript">
        	//Ajustement automatique de la taille de l'iframe � celle de la fen�tre
			   function gestionEvt()
			   {  
			     element = document.getElementById("TextHtml___Frame");
			     element.height = document.body.clientHeight-55;
			   }
			   window.onresize=gestionEvt;
			   </script>
    	</td>
    	<td width="1" bgColor="#000000"><IMG src="<?=ALK_URL_SI_IMAGES?>dot.gif" width="1"></td>
    </tr>
    <tr>
    	<td bgColor="#000000"><IMG src="<?=ALK_URL_SI_IMAGES?>dot.gif" width="1"></td>
    	<td>
        <!-- barre boutons action -->
        <table cellSpacing="0" cellPadding="0" width="100%" bgColor="#EAEAD9" border="0">
        	<tr>
          	<td><IMG height="2" src="<?=ALK_URL_SI_IMAGES?>dot.gif" width="2"></td>
        	</tr>
        	<tr>
          	<td align="right" height="30">
          	<?
          	$oBtValider = new HtmlLink("javascript:Validate();", "Valider", "valid_gen.gif", "valid_gen_rol.gif");
          	$oBtAnnuler = new HtmlLink("javascript:CloseEditor();", "Fermer", "fermer.gif", "fermer_rol.gif");
          	                  		
          	echo $oBtValider->getHtml()."&nbsp;".$oBtAnnuler->getHtml()."&nbsp;&nbsp;";                        
          	?>
          	</td>
        	</tr>
        	<tr>
          	<td><IMG height="2" src="<?=ALK_URL_SI_IMAGES?>dot.gif" width="2"></td>
        	</tr>
        </table><!-- fin barre boutons action -->
      </td>
        <td bgColor="#000000"><IMG src="<?=ALK_URL_SI_IMAGES?>dot.gif" width="1"></td>
      </tr>
      <tr>
    		<td bgColor="#000000" colSpan="3"><IMG height="1" src="<?=ALK_URL_SI_IMAGES?>dot.gif" width="1"></td>
			</tr></FORM>
		</table>
	
<?
aff_menu_bas();
?>
