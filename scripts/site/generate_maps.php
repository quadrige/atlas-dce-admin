<?php

include_once("lib/lib_session.php");
include_once("api/gen_con.php");

$map = (isset($_GET["map"]) ? $_GET["map"] : "LB");
switch($map){
	case "LB":
		$strCode = "EU_CD";
  break;	
	case "SN":
		$strCode = "CODEME2";
  break;
	case "RMC":
	case "RMCC":
    $strCode = "MECODE";
  break;
  case "AG":
  case "AP":
  default:  
    $strCode = "CODE_ME";
  break;  
}

$mapfile = "/home/devperso/bfontaine/ifremerdce/upload/carte/".$map."/mapfiles/carte".$map.".map";
define("ALK_PATH_SIG_IMGS",  "/home/devperso/bfontaine/ifremerdce/upload/carte/".$map."/imgms/");
echo $mapfile;
//recherche de l'extent de chaque objet masse d'eau
$oMap = ms_newMapObj($mapfile);

$oLayer = $oMap->getLayerByName("ifremer");
$oLayer->set("template", "query.html");

$status = $oLayer->open();
$status = $oLayer->whichShapes($oMap->extent);

while ($shape = $oLayer->nextShape())
{
   $fileName = $shape->values[$strCode];
   $oMap2 = ms_newMapObj($mapfile);
   $oMap2 = CNCarteSelection_CreateCouche_addlayerSelection($oMap2);
   $l_msShapeTopo = $oLayer->getShape($shape->tileindex, $shape->index);
   if($l_msShapeTopo)
     CNCarteSelection_addShape($oMap2, $l_msShapeTopo);
   //$oMap2->queryByIndex($oLayer->index, $shape->tileindex, $shape->index, MS_TRUE);
   drawReference($oMap, $shape->bounds, array("width"=>168, "height"=>168),  $fileName);
   drawMapfile($oMap2, $shape->bounds, array("width"=>480, "height"=>450), $fileName, $map);
   $oMap2->freequery($oLayer->index);
}
$oLayer->close();



/**
 * G�n�ration de la vignette
 * @param $mapfile
 * @param $tabExtent
 * @param $tabSize
 * @param $name
 * @return unknown_type
 */
function drawReference($oMap, $oExtent, $tabSize, $name)
{
  setDimension($oMap, $oExtent, $tabSize);
  $img = $oMap->drawReferenceMap();
  $imgfile = ALK_PATH_SIG_IMGS."vignette/".$name.".png";
  $img->saveImage($imgfile);
  return $imgfile;
}

/**
 * G�n�ration de la carte
 * @param $mapfile
 * @param $tabExtent
 * @param $tabSize
 * @param $name
 * @return unknown_type
 */

function drawMapfile($oMap, $oExtent, $tabSize, $name, $strCode)
{
	global $queryAtlas;
  $oExtent->setExtent($oExtent->minx-5000, $oExtent->miny-5000, $oExtent->maxx+5000, $oExtent->maxy+5000);
  setDimension($oMap, $oExtent, $tabSize);
  //ajout des layers GML 
  $dsReseau = $queryAtlas->getDs_listeReseauByBassinCode($strCode);
  while($drReseau = $dsReseau->getRowIter()){
  	$nom = $drReseau->getValueName("RESEAU_CODE");
  	
  	$symbole =  $drReseau->getValueName("RESEAU_SYMBOLE");
  	$couleur = $drReseau->getValueName("RESEAU_COULEUR");
  	$size = $drReseau->getValueName("RESEAU_SYMBOLE_TAILLE");
  	$img = $drReseau->getValueName("RESEAU_IMG_SYMB");
  	if ($img != ""){  	
  	  $nId = ms_newSymbolObj($oMap, $nom);
      $oSymbol = $oMap->getSymbolObjectById($nId);    	    	  
  	  $oSymbol->setImagePath("/home/devperso/bfontaine/ifremerdce/upload/carte/".$strCode."/".$img);
  	  $symbole = $nom;
  	  $size = "25";
  	}
  //	if($nom!="macrophytes" && $nom!="macrophytesOP" && $nom!="macroalgues"){
	  	$newlayerObj=ms_newLayerObj($oMap);
	  	$newlayerObj->setConnectionType(MS_OGR);
  	
	  	$newlayerObj->set("connection", "../".$strCode."_reseau".$nom.".gml");
	  	$newlayerObj->setProjection("init=epsg:4326");
	    $newlayerObj->set('name', $nom);
	    $newlayerObj->set('type', MS_LAYER_POINT);
	    $newlayerObj->set('status', 1);
	    $newclassObj=ms_newClassObj($newlayerObj);
	    $newclassObj->set('name', $nom);
	    $newstyleObj=ms_newStyleObj($newclassObj);
	    $newstyleObj->set('symbolname', $symbole);
	    $color = hex2RGB($couleur);
	    $newstyleObj->color->setRGB($color["red"],$color["green"],$color["blue"]);
	    //d�calage de la repr�sentation vis � vis d'Openlayers
	    $newstyleObj->set('size', $size+5);
  	}
  //}
  //$oMap->save("/home/devperso/bfontaine/ifremerdce/upload/carte/".$strCode."/mapfiles/test.map");
  $img = $oMap->drawQuery();
  echo $name."<br>";
  $imgfile = ALK_PATH_SIG_IMGS."carte/".$name.".png";
  $img->saveImage($imgfile);
  return $imgfile;
}
  
function setDimension(&$oMap, $oExtent, $tabSize)
{
  if ( count($tabSize)==2 ){
    $oMap->setSize($tabSize["width"], $tabSize["height"]);
    $oMap->querymap->set("width", $tabSize["width"]); 
    $oMap->querymap->set("height", $tabSize["height"]); 
  }
  $oMap->setExtent($oExtent->minx, $oExtent->miny, $oExtent->maxx, $oExtent->maxy);
  
}

/**
 * from Prodige
 * @param $p_oMap
 * @param $p_oShape
 * @return unknown_type
 */
function CNCarteSelection_addShape($p_oMap, $p_oShape){
  global $m_CNCarteSelection_idxLayerSel_PT;
  global $m_CNCarteSelection_idxLayerSel_LN;
  global $m_CNCarteSelection_idxLayerSel_SU;
  $l_msLayerSelection = null;
  if ($p_oShape->type == MS_SHAPE_POINT){
    //$l_msLayerSelection = $p_oMap->getLayerByName(LAYER_SEL_PT);
    $l_msLayerSelection = $p_oMap->getLayer($m_CNCarteSelection_idxLayerSel_PT);
  }
  else if ($p_oShape->type == MS_SHAPE_LINE){
    //$l_msLayerSelection = $p_oMap->getLayerByName(LAYER_SEL_LN);
    $l_msLayerSelection = $p_oMap->getLayer($m_CNCarteSelection_idxLayerSel_LN);
  }
  else if ($p_oShape->type == MS_SHAPE_POLYGON){
    //$l_msLayerSelection = $p_oMap->getLayerByName(LAYER_SEL_SU);
    global $m_CNCarteSelection_idxLayerSel_SU;
    $l_msLayerSelection = $p_oMap->getLayer($m_CNCarteSelection_idxLayerSel_SU);
  }
  if ($l_msLayerSelection!=null){
    $l_msLayerSelection->addFeature($p_oShape);
  }
}
/**
 * from Prodige
 * @param $p_oMap
 * @return unknown_type
 */
function CNCarteSelection_CreateCouche_addlayerSelection($p_oMap){
  global $m_CNCarteSelection_idxLayerSel_PT;
  global $m_CNCarteSelection_idxLayerSel_LN;
  global $m_CNCarteSelection_idxLayerSel_SU;

  if (isset($p_oMap) && !empty($p_oMap)){
      //couche de selection des POLYGON 
    //index == layercount - 2
    $newlayerObj=ms_newLayerObj($p_oMap);
    //$newlayerObj->set('name', LAYER_SEL_SU);
    $newlayerObj->set('type', MS_LAYER_POLYGON);
    $newlayerObj->set('status', 1);
    $newclassObj=ms_newClassObj($newlayerObj);
    $newclassObj->set('name', "");
    $newclassObj=ms_newStyleObj($newclassObj);
    //$newclassObj->set('symbolname', '0');
    $newclassObj->outlinecolor->setRGB(0, 10, 88);
    $newclassObj->set('size', 1);
    $newclassObj->set('width', 1);
    //$p_oMap->moveLayerDown($newlayerObj->index);
    $m_CNCarteSelection_idxLayerSel_SU = $newlayerObj->index;
    
    //couche de selection des LIGNE
    //index == layercount - 1
   /* $newlayerObj=ms_newLayerObj($p_oMap);
    //$newlayerObj->set('name', LAYER_SEL_LN);
    $newlayerObj->set('type', MS_LAYER_LINE);
    $newlayerObj->set('status', 1);
    $newclassObj=ms_newClassObj($newlayerObj);
    $newclassObj->set('name', "");
    $newclassObj=ms_newStyleObj($newclassObj);
    $newclassObj->set('symbolname', 'Carre');
    $newclassObj->outlinecolor->setRGB(255, 0, 0);
    $newclassObj->set('size', 3);
    //$p_oMap->moveLayerDown($newlayerObj->index);
    $m_CNCarteSelection_idxLayerSel_LN = $newlayerObj->index;
    
    //couche de selection des POINT
    //index == layercount
    $newlayerObj=ms_newLayerObj($p_oMap);
    //$newlayerObj->set('name', LAYER_SEL_PT);
    $newlayerObj->set('type', MS_LAYER_POINT);
    $newlayerObj->set('status', 1);
    $newclassObj=ms_newClassObj($newlayerObj);
    $newclassObj->set('name', "");
    
    $newStylesObj=ms_newStyleObj($newclassObj);
    $newStylesObj->set('symbolname', 'Carre');
    $newStylesObj->outlinecolor->setRGB(255, 0, 0);
    $newStylesObj->backgroundcolor->setRGB(255, 0, 0);
    $newStylesObj->set('size', 25);
    //$p_oMap->moveLayerDown($newlayerObj->index);
    $m_CNCarteSelection_idxLayerSel_PT = $newlayerObj->index;*/
    
  }
  return  $p_oMap;
}
/**
 * Convert a hexa decimal color code to its RGB equivalent
 *
 * @param string $hexStr (hexadecimal color value)
 * @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
 * @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
 * @return array or string (depending on second parameter. Returns False if invalid hex color value)
 */                                                                                                
function hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {
    $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
    $rgbArray = array();
    if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
        $colorVal = hexdec($hexStr);
        $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
        $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
        $rgbArray['blue'] = 0xFF & $colorVal;
    } elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
        $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
        $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
        $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
    } else {
        return false; //Invalid hex color code
    }
    return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
} 
  
?>
