<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_formTestJS.php");

$cont_id = Request("cont_id", REQ_GET, "-1", "is_numeric");
$cont_appli_id = Request("cont_appli_id", REQ_GET, "-1", "is_numeric");
$menu_id = Request("menu_id", REQ_GET, "-1", "is_numeric");
$actu_id = Request("actu_id", REQ_GET, "0", "is_numeric");
$thematique_id = Request("thema_id", REQ_GET, "0", "is_numeric");
$commune_id = Request("commune_id", REQ_GET, "0", "is_numeric");
if (Request("commune_id", REQ_POST, "0", "is_numeric") != 0)
  $commune_id = Request("commune_id", REQ_POST, "0", "is_numeric");
  
//$date_deb = Request("date_deb", REQ_POST, date("d/m/Y"));
//$date_fin = Request("date_fin", REQ_POST, date("d/m/Y", mktime(0, 0, 0, date("m")+1, date("d"), date("Y"))));
$date_deb = Request("date_deb", REQ_POST_GET, "");
$date_fin = Request("date_fin", REQ_POST_GET, "");

$strParam = "cont_appli_id=".$cont_appli_id."&cont_id=".$cont_id;

/*----------------- CAS PARTICULIER PLANNING DES REUNIONS--------------------------------*/
if( $cont_appli_id == 10 ) {
  header("location:10_planning.php?".$strParam);
  exit();
}


$iProfilCont = $oSpace->iProfilCont;
$iProfilAppli = $oSpace->iDroitAppli;
if( $iProfilAppli==0 ) {
  header("location:".ALK_URL_IDENTIFICATION."?cont_id=1&err=2");
  exit();
}


$bAppliAgenda = false;
if (ALK_STR_ACTU_AGENDA != "" && preg_match("/-".$cont_appli_id."-/",ALK_STR_ACTU_AGENDA))
  $bAppliAgenda = true;
  
if ($bAppliAgenda){
  $dsThematique = $queryActu->getThematiques($cont_appli_id);
  $dsCommune = $queryActu->getCommunesForCombo();
}

$actu_id_first = "";

$dsActuListe = $queryActu->getActusAPublier($cont_appli_id, 1, $thematique_id, $commune_id, 0, -1, $date_deb, $date_fin);
if (!$dsActuListe->bEof) {
   $drActuFirst = $dsActuListe->GetRow();
   $actu_id_first = $drActuFirst->getValueName("ACTU_ID");
}
$dsActuListe->MoveFirst();
   
if ($actu_id!=0)
{
    $dsActu =& $queryActu->getActuById($actu_id);
    if (!$dsActu->bEof) {
      $drActu = $dsActu->GetRow();
      $actu_titre = $drActu->getValueName("TITRE".$strLgBdd);
      $actu_datedeb = $drActu->getValueName("DATEDEB");
      $actu_datefin = $drActu->getValueName("DATEFIN");
      $actu_descc = $drActu->getValueName("DESCC".$strLgBdd);
      $actu_descl = $drActu->getValueName("DESCL".$strLgBdd);
      $actu_visuel = $drActu->getValueName("VISUEL");
      $actu_pj = $drActu->getValueName("PJ");
      $actu_url = $drActu->getValueName("URL");
      
      $_SESSION["strTitre"]=$actu_titre;
    }
    else
    {
      //Redirection vers la premiere actu
      header("location: 02_actualite.php?".$strParam."&thema_id=".$thematique_id."&commune_id=".$commune_id."&actu_id=".$actu_id_first."&menu_id=".$actu_id_first);
    }
}
else
{
   //Redirection vers la premiere actu
   //if ($actu_id_first!="")
   //   header("location: 02_actualite.php?".$strParam."&thema_id=".$thematique_id."&commune_id=".$commune_id."&actu_id=".$actu_id_first."&menu_id=".$actu_id_first);
}

$arbreMenu = new Arbre();
$arbreMenu->modif_valeur("0");
while ($drActuListe = $dsActuListe->getRowIter()) {
  $actu_menu_id = $drActuListe->getValueName("ACTU_ID");
  $actu_menu_titre = $drActuListe->getValueName("TITRE".$strLgBdd);
  
  $i=1;
  $arbreMenu->creer_fils(array("id"=>$actu_menu_id, "nom"=>$actu_menu_titre,"lien"=>"site/02_actualite.php?".$strParam."&thema_id=".$thematique_id."&commune_id=".$commune_id."&actu_id=".$actu_menu_id."&date_deb=".$date_deb."&date_fin=".$date_fin));
  $i++;
}

function writeSubMenuThematique()
{
  global $dsThematique;
  global $thematique_id;
  global $commune_id;
  global $date_deb;
  global $date_fin;
  global $strParam;
  global $strLgBdd;

    if (! $dsThematique->bEof)
    {
?>
  <table border="0" cellspacing="0" cellpadding="0">
  <tr>
      <td valign="middle" class="TXTbrun">
      <?
        while ($drThematique = $dsThematique->getRowIter()) {
          $thema_id = $drThematique->getValueName("THEMATIQUE_ID");
          $thema_titre = $drThematique->getValueName("TITRE".$strLgBdd);

          $url = "02_actualite.php?".$strParam."&commune_id=".$commune_id."&thema_id=".$thema_id."&date_deb=".$date_deb."&date_fin=".$date_fin;

          $strClasse="nav1";
          if ($thema_id==$thematique_id)
            $strClasse="nav1selec";
      ?>
         <!--<img src="<?=ALK_URL_SI_IMAGES?>gen/pictos/fleche_blanche.gif" width="6" height="6" border="0">/-->
         <img src="<?=ALK_URL_SI_MEDIA?>transp.gif" width="5" height="10" border="0">
              <a href="<?=$url?>"><span class="<?=$strClasse?>">&gt;<?=$thema_titre?></span></a>
         <img src="<?=ALK_URL_SI_MEDIA?>transp.gif" width="10" height="10" border="0">
      <?
        }
      ?>
       </td>
    </tr>
  </table>
  <table width=100% border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td height="10" background="<?=ALK_URL_SI_IMAGES?>gen/pointille.gif"><img width=1 height=1></td>
    </tr>
  </table>
<?
   }
}

aff_menu_haut();
aff_menu_gauche($arbreMenu, $cont_id, $cont_appli_id, $menu_id);

if ($actu_id==0) 
{
?>
<!-- Carte de voeux 2006 -->
<SCRIPT LANGUAGE="JavaScript">
function voeuxoff()
{
  if (document.getElementById)
  {
    document.getElementById("ejs_logo").style.visibility = "hidden";
    document.getElementById("ejs_logo2").style.visibility = "hidden";
  }
}

logo_url = "<?=ALK_SIALKE_URL?>media/imgs/fr/voeux2006.gif";
logo_width = 210;
logo_height = 345;

if (document.getElementById)
{
  /*var staticlogo=new Image;
  staticlogo.src=logo_url;
  document.write('<div id="ejs_logo" style="z-index:200;position:absolute;width:'+logo_width+';height:'+logo_height+';background:#FFFFFF;" align="center">&nbsp;<br/><img src="'+staticlogo.src+'" border=0 /><br/><a href="javascript:voeuxoff();" class="txt">Fermer</a></div>')
  document.write('<div id="ejs_logo2" style="z-index:100;position:absolute;width:'+logo_width+';height:'+logo_height+';background:#000000;filter:alpha(opacity=40); -moz-opacity: .4;"></a></div>')
  ejs_scx = (document.body.clientWidth/2)+115;
  ejs_scy = 583;
  document.getElementById("ejs_logo").style.top=(ejs_scy-logo_height-30);
  document.getElementById("ejs_logo").style.left=(ejs_scx-logo_width-30);
  document.getElementById("ejs_logo2").style.top=(ejs_scy-logo_height-25);
  document.getElementById("ejs_logo2").style.left=(ejs_scx-logo_width-25);*/
}
function logoit()
{
  if (document.getElementById)
  {
    w2=document.body.scrollTop;
    document.getElementById("ejs_logo").style.top=(w2+ejs_scy-logo_height-30);
    document.getElementById("ejs_logo2").style.top=(w2+ejs_scy-logo_height-25);
    setTimeout("logoit()",1);
  }
}

//window.onload=logoit
</SCRIPT>
<!-- fin carte de voeux 2006 -->
<?
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td height="20" valign="bottom">
      <!--<span class="titre"><strong><?=$oSpace->GetAppliProperty("APPLI_INTITULE");?></strong></span>-->
    </td>
    <td width="10"><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="1" height="1" border="0"></td>
  </tr>
  <tr> 
    <td valign="top">
        <?
        if ($bAppliAgenda)
          writeSubMenuThematique();

        if ($bAppliAgenda){
          $oCtrlSelect = new HtmlSelect(0, "commune_id", $commune_id, "Commune");
          $oCtrlSelect->oValTxt = $dsCommune;
          $oCtrlSelect->tabValTxtDefault = array(0,"Agglomération");
          //$oCtrlSelect->tabValTxtLast = array(99,"Autre");
          $oCtrlSelect->cssCtrl="txt";
          //$oCtrlSelect->AddEvent("onchange", "Valider();");
          
          $oCtrlTextDateDeb = new HtmlText(0, "date_deb", $date_deb, "Date de début", 1, 8, 10);
          $oCtrlTextDateDeb->cssCtrl="txt";
          $oCtrlTextDateFin = new HtmlText(0, "date_fin", $date_fin, "Date de fin", 1, 8, 10);
          $oCtrlTextDateFin->cssCtrl="txt";
          
          $oBtVal = new HtmlLink("javascript:Valider();", "Valider les modifications",
                           "gen/bouton_ok.gif", "");
        
        form_Test_JS();
        ?>
        <script language="javascript">
        var nbClickVal = 0;
        
        function Valider()
        {  
          var f = document.actu;
          
          if( ! ControleDate(f.date_deb, 10) ) return;
          if( ! ControleDate(f.date_fin, 10) ) return;
          if( nbClickVal == 0 ) {
            nbClickVal++; 
            f.submit();
          }
        }
        
        <?
        AffFonctionCtrlDate();
        ?>
        </script>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <form name="actu" id="actu" action="02_actualite.php?<?=$strParam?>&thema_id=<?=$thematique_id?>" method="post">
          <tr>
            <td valign="top" class="txt">
              <?
              echo "<span class=txt>".$oCtrlSelect->label."</span> ".$oCtrlSelect->getHtml()."&nbsp;&nbsp;&nbsp;&nbsp;";
              echo "<span class=txt>".$oCtrlTextDateDeb->label."</span>&nbsp;".$oCtrlTextDateDeb->getHtml()."&nbsp;&nbsp;";
              echo "<span class=txt>".$oCtrlTextDateFin->label."</span>&nbsp;".$oCtrlTextDateFin->getHtml()." (jj/mm/aaaa)&nbsp;&nbsp;&nbsp;&nbsp;";
              echo $oBtVal->getHtml();
              ?>
            </td>
          </tr></form>
        </table>
        <table width=100% border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td height="10" background="<?=ALK_URL_SI_IMAGES?>gen/pointille.gif"><img width=1 height=1></td>
          </tr>
        </table><br>
        <?
        }
        
        if ($actu_id<>0){
        ?>
        <span class="titre2"><strong><?=$actu_titre?></strong></span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                   <td><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="2" height="6" border="0"></td>
                 </tr>
               </table>
              <?if ($actu_datedeb != "") { ?>
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
               <td valign="top" class="txt"><b><?=$actu_datedeb.($actu_datefin != ""?" - ".$actu_datefin:"")?></b></td>
               </tr>
              </table>
              <?}?>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="2" height="15" border="0"></td>
              </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top"  class="txt">
                      <?if ($actu_visuel != "") {
                      ?>
                      <img src="<?=ALK_SIALKE_URL.ALK_PATH_UPLOAD_ACTUSITE_LOGO.$cont_id."/".$actu_visuel?>" border=0 vspace="2" hspace="5" align=left>
                      <?}?>
                      
                      <div align=justify>
                      <b><?=$actu_descc?></b>
                      <br>
                      <?=$actu_descl?>
                      </div>
                      
                      <?if ($actu_url != "" || $actu_pj != "") {
                      ?>
                      <div align=right>
                        <br>
                        <span class="titre3">
                          <?
                          $strPlus = "".
                                    (($actu_url != "")?"<a href=\"".$actu_url."\" class=\"nav1\" target=\"_blank\"><img align=absmiddle src=\"".ALK_URL_SI_IMAGES."gen/Plus.gif\" border=0></a>":"").
                                    (($actu_pj != "")?"<a href=\"".ALK_SIALKE_URL.ALK_PATH_UPLOAD_ACTUSITE_LOGO.$cont_id."/".$actu_pj."\" target=\"_blank\"><img align=absmiddle src=\"".ALK_URL_SI_IMAGES."gen/pj.gif\" border=0></a>":"");
                                    
                          if (ALK_LG==1)
                            echo ($actu_url != "")?"<a href=\"".$actu_url."\" class=\"titre3\" target=\"_blank\">En savoir plus</a>&nbsp;&nbsp;".$strPlus:"En savoir plus&nbsp;&nbsp;".$strPlus;
                          else
                            echo ($actu_url != "")?"<a href=\"".$actu_url."\" class=\"titre3\" target=\"_blank\">To find out more</a>&nbsp;&nbsp;".$strPlus:"To find out more&nbsp;&nbsp;".$strPlus;
                          ?>
                      </div>
                      <?}?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <?
        }
        else
        {
          $dsActuListe->MoveFirst();
          while ($drActuListe = $dsActuListe->getRowIter())
          {
            $actu_titre = $drActuListe->getValueName("TITRE".$strLgBdd);
            $actu_datedeb = $drActuListe->getValueName("DATEDEB");
            $actu_datefin = $drActuListe->getValueName("DATEFIN");
            $actu_descc = $drActuListe->getValueName("DESCC".$strLgBdd);
            $actu_descl = $drActuListe->getValueName("DESCL".$strLgBdd);
            $actu_visuel = $drActuListe->getValueName("VISUEL");
            $actu_url = $drActuListe->getValueName("URL");
            ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="100%" valign="top">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                       <td><a href="02_actualite.php?<?=$strParam?>&actu_id=<?=$drActuListe->getValueName("ACTU_ID");?>&menu_id=<?=$drActuListe->getValueName("ACTU_ID") ?>&date_deb=<?=$date_deb?>&date_fin=<?=$date_fin?>" class="titre2"><strong><?=$actu_titre?></strong></a>
                     </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
                   <td><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="2" height="6" border="0"></td>
                   </tr>
                 </table>
                  <?if ($actu_datedeb != "") { ?>
                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
                   <td valign="top" class="txt"><b><?=$actu_datedeb.($actu_datefin != ""?" - ".$actu_datefin:"")?></b></td>
                   </tr>
                  </table>
                  <?}?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="2" height="10" border="0"></td>
                  </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td valign="top"  class="txt">
                          <div align=justify>
                          <?if ($actu_visuel != ""){ ?>
                          <a href="02_actualite.php?<?=$strParam?>&actu_id=<?=$drActuListe->getValueName("ACTU_ID") ?>&menu_id=<?=$drActuListe->getValueName("ACTU_ID") ?>&date_deb=<?=$date_deb?>&date_fin=<?=$date_fin?>" class="txt">
                          <img src="<?=ALK_SIALKE_URL.ALK_PATH_UPLOAD_ACTUSITE_LOGO.$cont_id."/".$actu_visuel?>" border="0" vspace="2" hspace="5" align="left" border="0"></a>
                          <?} ?>
                          <a href="02_actualite.php?<?=$strParam?>&actu_id=<?=$drActuListe->getValueName("ACTU_ID"); ?>&menu_id=<?=$drActuListe->getValueName("ACTU_ID"); ?>&date_deb=<?=$date_deb?>&date_fin=<?=$date_fin?>" class="txt" style="text-decoration:none"><?=$actu_descc?></a>
                          </div>
                        </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" height="16"  border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="1" height="1" border="0"></td>
                </tr>
              </table>
             <table width="100%" height="1"  border="0" cellpadding="0" cellspacing="0" bgcolor="#FF954E">
                <tr>
                  <td><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="1" height="1" border="0"></td>
                </tr>
              </table>
             <table width="100%" height="16"  border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="1" height="1" border="0"></td>
                </tr>
              </table>
            <?
          }
        }
        ?>
     </td>
    <td width="10"><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="1" height="1" border="0"></td>
  </tr>
</table>
<?
$dsAccesDirect = $queryActuSite->getActusAPublier(14);
aff_menu_droite($dsAccesDirect);
aff_fin_menu_gauche();
aff_menu_bas();
?>