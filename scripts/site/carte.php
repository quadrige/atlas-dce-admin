<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("classes/alkmasseeau.class.php");

$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$map = Request("map", REQ_GET, "LB", "is_string");
$session_id = Request("idSession", REQ_GET, "-1", "is_numeric");

// carte � afficher
$strExtent = "";
$dsBassin = $queryAtlas->getDs_bassin($map);
if ($drBassin = $dsBassin->getRowIter()){
  $bassin_id = $drBassin->getValueName("BASSIN_ID");
  $bassin_nom = $drBassin->getValueName("BASSIN_NOM");
	$strExtent = $drBassin->getValueName("BASSIN_XMIN").", ".$drBassin->getValueName("BASSIN_YMIN").", ".$drBassin->getValueName("BASSIN_XMAX").", ".$drBassin->getValueName("BASSIN_YMAX");
  $strAlertCarte = $drBassin->getValueName("BASSIN_ALERT_CARTE");
  $strAlert = $drBassin->getValueName("BASSIN_ALERT");
  $strUrlWMS = $drBassin->getValueName("BASSIN_URL_WMS");
  $strLayersWMS = $drBassin->getValueName("BASSIN_LAYER_WMS");
  $strProjection = $drBassin->getValueName("BASSIN_PROJECTION");
  $bassin_code = $drBassin->getValueName("BASSIN_CODE"); 
}

//Prise en compte de la session uniquement si l'on provient de l'admin donc session user_id non vide
if (!(isset($_SESSION["sit_idUser"]) && $_SESSION["sit_idUser"] != "" &&
    isset($_SESSION["idBassin"]) && $_SESSION["idBassin"] != "" && $_SESSION["idBassin"] == $bassin_id)) {
  $session_id = -1;      
}
$strTitreSession = "";
$strDirArchive = "";
if ($session_id != -1){
  $dsSession = $queryAtlas->GetDs("select SESSION_LIBELLE from SESSION_QUALITE where SESSION_ID=".$session_id);
  if ($drSession = $dsSession->getRowIter()){   
    $strTitreSession = $drSession->getValueName("SESSION_LIBELLE");
    $strDirArchive = "/archive_".$session_id;
  }
  else 
    $session_id = -1;
}

// date maj la plus recente reseau ou element de qualite

$dateMaj = "";
$dsDateRecente = $queryAtlas->getDs_DateMAJByBassinId($bassin_id, $session_id);
if($drDateRecente = $dsDateRecente->getRowIter()){
  $dateMaj = $drDateRecente->getValueName("DATE_MAJ_RECENTE");
}


$oMasseEau = new AlkMasseEau(-1, $queryAtlas, "", $session_id);
//ALTER TABLE `BASSIN_HYDROGRAPHIQUE` ADD `BASSIN_XMIN` FLOAT NOT NULL DEFAULT '0';
//ALTER TABLE `BASSIN_HYDROGRAPHIQUE` ADD `BASSIN_YMIN` FLOAT NOT NULL DEFAULT '0';
//ALTER TABLE `BASSIN_HYDROGRAPHIQUE` ADD `BASSIN_XMAX` FLOAT NOT NULL DEFAULT '0';
//ALTER TABLE `BASSIN_HYDROGRAPHIQUE` ADD `BASSIN_YMAX` FLOAT NOT NULL DEFAULT '0';
//UPDATE `alk_ifremer_dce_v2`.`BASSIN_HYDROGRAPHIQUE` SET `BASSIN_XMIN` = '135370 ',`BASSIN_YMIN` = '1806723.88372093',`BASSIN_XMAX` = '536839',`BASSIN_YMAX` = '2155286.11627907' WHERE `BASSIN_HYDROGRAPHIQUE`.`BASSIN_ID` =2 LIMIT 1 ;
//UPDATE `alk_ifremer_dce_v2`.`BASSIN_HYDROGRAPHIQUE` SET `BASSIN_XMIN` = '630.116071428571',`BASSIN_YMIN` = '2115900',`BASSIN_XMAX` = '386550.883928571',`BASSIN_YMAX` = '2451100' WHERE `BASSIN_HYDROGRAPHIQUE`.`BASSIN_ID` =1 LIMIT 1 ;

$dsReseau = $queryAtlas->getReseauForCombo(true, 0, -1, $bassin_id, $session_id);
$oCtrlSelectReseau = new HtmlSelect(0, "reseau_id", "-1", "R�seau de contr�le", 1, 240);
$oCtrlSelectReseau->oValTxt = $dsReseau;
$oCtrlSelectReseau->addEvent("onchange", "changeLayer(this.value)");
$oCtrlSelectReseau->tabValTxtDefault = array(0,"Aucun");
$oCtrlSelectReseau->cssCtrl="txt";

//$dsQualite = $queryAtlas->getDs_listeElementqualite(1, 0, -1, "ELEMENT_QUALITE_RANG", $bassin_id, -1, $session_id);
$tabVal = array();
// construcion d'un tableau permettnat d'obtenir les classment de qulit�
// 1er niv de classement
$dsQualite = $queryAtlas->getDs_listeElementqualiteCarte(1, 0, -1, "tqual.TYPE_CLASSEMENT_ID, tqual.TYPE_ELEMENT_QUALITE_ID", $bassin_id, -1, $session_id);
while($drQualite = $dsQualite->getRowIter()){
  if (!array_key_exists($drQualite->getValueName("TYPE_CLASSEMENT_LIBELLE"), $tabVal)) {
    $tabVal[$drQualite->getValueName("TYPE_CLASSEMENT_LIBELLE")]=array();
  }
  if (!array_key_exists($drQualite->getValueName("TYPE_ELEMENT_QUALITE_NOM"), $tabVal[$drQualite->getValueName("TYPE_CLASSEMENT_LIBELLE")])) {
    $tabVal[$drQualite->getValueName("TYPE_CLASSEMENT_LIBELLE")][$drQualite->getValueName("TYPE_ELEMENT_QUALITE_NOM")]=array();
  }
  $tabVal[$drQualite->getValueName("TYPE_CLASSEMENT_LIBELLE")][$drQualite->getValueName("TYPE_ELEMENT_QUALITE_NOM")][$drQualite->getValueName("ID")]=$drQualite->getValueName("LIB");             
}

$oCtrlSelectQualite = new HtmlSelect(0, "qualite_id", "-1", "Element de qualit�", 1, 240);
$oCtrlSelectQualite->tabValTxt = $tabVal;
$oCtrlSelectQualite->addEvent("onchange", "displayClassement(this.value);reloadLayer();displaySubQualite(this.value, ".$bassin_id.", ".$session_id.")");
$oCtrlSelectQualite->tabValTxtDefault = array(0,"Aucun");
$oCtrlSelectQualite->cssCtrl="txt";
$oCtrlSelectQualite->bWriteId=true;

$oCtrlSelectSubQualite = new HtmlSelect(0, "subqualite_id", "-1", "", 1, 240);
$oCtrlSelectSubQualite->addEvent("onchange", "displayClassement(this.value);reloadLayer();");
$oCtrlSelectSubQualite->cssCtrl="txt";
$oCtrlSelectSubQualite->bWriteId=true;

$oCtrlComboType = new HtmlRadioGroup(0, "etat_masse_eau", "ETAT_ECOLOGIQUE", "Type de classement");
$oCtrlComboType->AddRadio("ETAT_ECOLOGIQUE", "�cologique");
$oCtrlComboType->AddRadio("ETAT_CHIMIQUE", "chimique");
$oCtrlComboType->AddRadio("ETAT_GENERAL", "global");
$oCtrlComboType->AddEventToRadio("ETAT_GENERAL", "onclick", "reloadLayer();");
$oCtrlComboType->AddEventToRadio("ETAT_CHIMIQUE", "onclick", "reloadLayer();");
$oCtrlComboType->AddEventToRadio("ETAT_ECOLOGIQUE", "onclick", "reloadLayer();");
$oCtrlComboType->cssCtrl="txt";

$oCtrlResauCheckBox =  new HtmlCheckBoxGroup(0, "reseau_check_id", "", "R�seaux");
$oCtrlResauCheckBox->iNbItem=1;
$oCtrlResauCheckBox->cssCtrl="txt";
$oCtrlResauCheckBox->cssTdCtrl="txt";
$oCtrlResauCheckBox->bWriteId=true;
$reseau_groupe = "";
if ($drReseau = $dsReseau->getRowIter()){
  $reseau_groupe = $strGroupe1 = $drReseau->getValueName("GROUPE_NOM");
}
$dsReseau->MoveFirst();

while($drReseau = $dsReseau->getRowIter()){
  $strGroupe = "";
  if ($dsReseau->iCurDr < $dsReseau->iCountDr){    
    $drReseauSuiv = $dsReseau->GetRow();
    if ($reseau_groupe != $drReseauSuiv->getValueName("GROUPE_NOM")){
      $strGroupe = $drReseauSuiv->getValueName("GROUPE_NOM");
      $reseau_groupe = $strGroupe;
    }       
  }
  
  $oCtrlResauCheckBox->AddCheckBox($drReseau->getValueName("RESEAU_CODE"),                                                                     
                                                                    $drReseau->getValueName("RESEAU_NOM")."</span>".                                                                    
                                                                    "</td><td class='txt'>".                                                                                                                                        
																																		
                                                                    ($drReseau->getValueName("RESEAU_IMG_SYMB")!="" ? 
                                                                          "<img class='classImgCarte classImgCarteSymb' src=\"".ALK_SIALKE_URL."upload/carte/".$bassin_code."/".$drReseau->getValueName("RESEAU_IMG_SYMB")."\"/>"
                                                                       :  "<div class='divLegReseau' style='background-color:".$drReseau->getValueName("RESEAU_COULEUR")."'><img class='classImgCarte' src=\"".ALK_SIALKE_URL."media/imgs/gen/pictos/legende/".$drReseau->getValueName("RESEAU_SYMBOLE").".png\"/>"
                                                                    ).
                                                                    "</div>".
                                                                    ($strGroupe != "" ? "</tr><tr><td colspan=2><h3>".$strGroupe."</h3></td></tr>" : "")
                                                                    );
  $oCtrlResauCheckBox->AddEventToCheckBox($drReseau->getValueName("RESEAU_CODE"), "onclick", "addLayer(this.checked, this.value, true)");
  
}
$dsReseau->MoveFirst();

$oCtrlMasseEauCheckBox =  new HtmlCheckBoxGroup(0, "masse_eau_check_id", array(), "Masses d'eau");
$oCtrlMasseEauCheckBox->iNbItem=1;
$oCtrlMasseEauCheckBox->cssCtrl="txt";
$oCtrlMasseEauCheckBox->cssTdCtrl="txt";


$tabTypoMEC = $tabTypoMET = $tabTypoMER = array();
$dsTypeMe = $queryAtlas->getDs_listeTypologieMasseEau("C", $map, $session_id);
while ($drTypeMe = $dsTypeMe->getRowIter()){
	$tabTypoMEC[$drTypeMe->getValueName("MASSE_DESC_CODE")] = $drTypeMe->getValueName("MASSE_DESC_COLOR");
}
$dsTypeMe = $queryAtlas->getDs_listeTypologieMasseEau("T", $map, $session_id);
while ($drTypeMe = $dsTypeMe->getRowIter()){
	$tabTypoMET[$drTypeMe->getValueName("MASSE_DESC_CODE")] = $drTypeMe->getValueName("MASSE_DESC_COLOR");
}
$dsTypeMe = $queryAtlas->getDs_listeTypologieMasseEau("R", $map, $session_id);
while ($drTypeMe = $dsTypeMe->getRowIter()){
	$tabTypoMER[$drTypeMe->getValueName("MASSE_DESC_CODE")] = $drTypeMe->getValueName("MASSE_DESC_COLOR");
}
//g�n�ration des autres couches masses d'eau
$tabTypeMe = array( "NONE" => array( "LIB" => "Limites des masses d'eau", "COLOR" => "#e0f5ff", "COND"=>""),  
										//"RISQUE" => array( "LIB" => "Classement RNAOE Risque", "COLOR" => "#cce666", "COND"=>"MASSE_B_RISQUE=1"), 
										//"RESPECT" => array( "LIB" => "Classement RNAOE Respect", "COLOR" => "#50a48b", "COND"=>"MASSE_B_RISQUE=0"), 
										//"DOUTE" => array( "LIB" => "Classement RNAOE Doute", "COLOR" => "#EBF1DE", "COND"=>"MASSE_B_RISQUE=2"),      
										"SURV" => array( "LIB" => "Retenue au titre du programme de surveillance", "COLOR" => "#bed2ff", "COND"=>"MASSE_B_SUIVI=1"));


// sept2014
$tabObjEnvAtteint = array();
$dsTypeObj = $queryAtlas->getDs_listeTypologieAtteinteObjectifEnv($map, -1);
while ($drTypeObj = $dsTypeObj->getRowIter()){
  $tabObjEnvAtteint[$drTypeObj->getValueName("ATTEINTE_OBJECTIF_CODE")] = $drTypeObj->getValueName("ATTEINTE_OBJECTIF_COLOR");
}

$tabTypeMe["TYPOATTOBJ"] = array( "LIB" => "Atteinte des objectifs environnementaux", "COLOR" => $tabObjEnvAtteint, "COND"=>"", "CHAMP"=>"MASSE_DESC_CODE", "DIVSUITE"=>$oMasseEau->getLegendeTypoAtteinteObj($map));

if ($map == "RUN"){
  $tabTypeMe["TYPOMEC"] = array( "LIB" => "Typologie des masses d'eau c�ti�res", "COLOR" => $tabTypoMEC, "COND"=>"MASSE_TYPE='MEC'", "CHAMP"=>"MASSE_DESC_CODE", "DIVSUITE"=>$oMasseEau->getLegendeTypo("C", $map));
  $tabTypeMe["TYPOMER"] = array( "LIB" => "Typologie des masses d'eau r�cifales", "COLOR" => $tabTypoMER, "COND"=>"MASSE_TYPE='MER'", "CHAMP"=>"MASSE_DESC_CODE", "DIVSUITE"=>$oMasseEau->getLegendeTypo("R", $map));
}
else {
  if ($map == "MAY"){  
    $tabTypeMe["TYPOMEC"] = array( "LIB" => "Typologie des masses d'eau c�ti�res", "COLOR" => $tabTypoMEC, "COND"=>"MASSE_TYPE='MEC'", "CHAMP"=>"MASSE_DESC_CODE", "DIVSUITE"=>$oMasseEau->getLegendeTypo("C", $map));   
  } else {
    $tabTypeMe["TYPOMEC"] = array( "LIB" => "Typologie des masses d'eau c�ti�res", "COLOR" => $tabTypoMEC, "COND"=>"MASSE_TYPE='MEC'", "CHAMP"=>"MASSE_DESC_CODE", "DIVSUITE"=>$oMasseEau->getLegendeTypo("C", $map));
    $tabTypeMe["TYPOMET"] = array( "LIB" => "Typologie des masses d'eau de transition", "COLOR" => $tabTypoMET, "COND"=>"MASSE_TYPE='MET'", "CHAMP"=>"MASSE_DESC_CODE", "DIVSUITE"=>$oMasseEau->getLegendeTypo("T", $map));
  }
}

foreach ($tabTypeMe as $code=>$tabParam){
  $oCtrlMasseEauCheckBox->AddCheckBox($code, $tabParam["LIB"]."</span></td>" .
                                              ((array_key_exists("COLOR", $tabParam) && !is_array($tabParam["COLOR"]) ) ?  "<td class='txt'><div class='divLeg' style='background-color:".$tabParam["COLOR"]."'></div>" : "").
																							(array_key_exists("DIVSUITE", $tabParam) ? "</tr><tr><td class='txt' colspan=2><div id='leg".$code."' class='legHide'>".$tabParam["DIVSUITE"]."</div>" : ""));
  $oCtrlMasseEauCheckBox->AddEventToCheckBox($code, "onclick", "addMasseEauLayer(this.checked, this.value, 'leg".$code."')");
  $oCtrlMasseEauCheckBox->bWriteId=true;
}
 
$strHtml ="<link rel=\"stylesheet\" type=\"text/css\" href=\"".ALK_SIALKE_URL."styles/carte.css\" />".
           "<link rel=\"stylesheet\" type=\"text/css\" href=\"".ALK_SIALKE_URL."styles/carte_impression.css\" />".
           "<link rel=\"stylesheet\" type=\"text/css\" href=\"".ALK_SIALKE_URL."scripts/openlayers/lib/theme/default/style.css\" />
           <script src=\"".ALK_SIALKE_URL."scripts/openlayers/lib/OpenLayers.js\" type=\"text/javascript\"></script>
           <script src=\"".ALK_SIALKE_URL."scripts/openlayers/lib/GeolocEntity.js\" type=\"text/javascript\"></script>
           <script src=\"".ALK_SIALKE_URL."scripts/proj4js/lib/proj4js-compressed.js\"></script>
           <script src=\"".ALK_SIALKE_URL."lib/lib_ajax.js\" type=\"text/javascript\"></script>
           <script src=\"".ALK_SIALKE_URL."scripts/site/lib/lib_carte.js\" type=\"text/javascript\"></script>
           <script type=\"text/javascript\">
           	 var ALK_SIALKE_URL = '".ALK_SIALKE_URL."';
           </script>
          <div id=\"menu_carte_hidden\" style=\"display:none;\"><img src=\"".ALK_SIALKE_URL."media/imgs/gen/loader.gif\"/></div>";

$strHtml.=         
            "<div id=\"menu_carte\">
            <div id=\"titre_carte\" class='classTitreCarte' >
            	<div class='classDivCarte' id=\"divCarte\"><h1>Atlas DCE<br/>".$bassin_nom."</h1>".
            	($session_id != -1 ? "<h2>Session archiv�e \"".$strTitreSession."\"</h2>" : "").
            	"</div>      	
            </div>  
            <div id='btImp' style='text-align:center; float: right; margin-right: 15px; margin-top: 35px;'><a id='btImpression' value=0 href=\"javascript:majScreenImpression('".$map."','".$session_id."', 1);\"><img src='".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_imprim.gif' alt='Imprimer' title='Imprimer'></a> 
            </div>
            <form name=\"carteForm\" id=\"carteForm\" action=\"\" method=\"post\">
            	<div class='classDivCarteVisibility'>".$oCtrlSelectReseau->getHtml()."</div>
              <div class='classDivMiseEnForm'>              	

                  <div id=\"divCarte1\" class='txt classDivCarte1'><h2 class='txtLabelCarte'>Qualit� des masses d'eau</h2></div>
                  <div id=\"divCarte2\" class='txt classDivCarte2'><p class='classDivCarte3 txtLabelCarte'>".$strAlert."</p></div>
                  ".
                  "
                <div id='trClassement' class='txt classDivCarte5'>
                   <h3 class=txtLabel>".$oCtrlComboType->label."</h3>".$oCtrlComboType->getHtml().
                "</div> 
                <div class='txt classDivCarte5' id='divQualite'>".
                   "<h3 class=txtLabel>".$oCtrlSelectQualite->label."</h3>".$oCtrlSelectQualite->getHtml().
                "</div>
                <div id='divSubqualite' style='display:none' class='txt classDivCarte5'>".
                   "<span class=txtLabel>".$oCtrlSelectSubQualite->label."</span>".$oCtrlSelectSubQualite->getHtml().
                "</div>    
                <div valign=top class='txt classDivCarte6 clearfix'>".
										$oMasseEau->getLegende("", "ulTabLegCarte").	
								"</div>
								<div class='txt classDivCarte6 clearfix'>".								
								$oMasseEau->getLegendeIndConfiance().
                "</div> 
                <div  id='divReseau1' class='txt classDivCarte4'><h2 class=txtLabel>R�seaux</h2>
                </div> 
                <div id='divReseau2' class=\"txt\"><h3>".$strGroupe1."</h3>".$oCtrlResauCheckBox->GetHtml()."</div>
                <div id='divDecoupME1' class='txt classDivCarte4' ><h2 class=txtLabel>Statut des masses d'eau</h2></div>
                <div id='divDecoupME2' class=\"txt\">".$oCtrlMasseEauCheckBox->GetHtml()."</div>


            </div>         
            </form>
            <div id='logos'>".
            	($map == 'MAY' ?
             	    "<img id='logoIfremer2' class='classlogoONEMA' src='".ALK_SIALKE_URL."media/imgs/gen/".$map."_ONEMA.jpg'/>"
             	    : "").            
            	"<img id='logoIfremer' class='classlogoIfremer' src='".ALK_SIALKE_URL."media/imgs/gen/Ifremer_logo.gif'/>
							<img id='logoAE' class='classlogoAE_55' src='".ALK_SIALKE_URL."media/imgs/gen/".$map."_logo_agence_de_leau.jpg' />
	        </div>
          </div>
          <div id=\"slider_carte\">
            <a href=\"javascript:slideMenu('on');\" id=\"slider_cursor_on\" class=\"lien_slider lien_spider_on\" style=\"display:none;\">&nbsp;</a>
            <a href=\"javascript:slideMenu('off');\" id=\"slider_cursor_off\" class=\"lien_slider lien_spider_off\">&nbsp;</a>
          </div>
          <div class='classDimensionCarte' id=\"carte\"></div>  
          <div id=\"popupContentHidden\"></div>";

      
  $tabEventBody["onLoad"] = "initMap('".$map."',".$strExtent.",'".$strUrlWMS."','".$strLayersWMS."','".$strProjection."', '".addslashes($strAlertCarte)."', '".$strDirArchive."', ".rand().", ".$session_id.");";
	aff_menu_haut($tabEventBody);
	echo $strHtml;
	aff_menu_bas();

?>
