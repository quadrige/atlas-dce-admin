<?
  include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."alkobject.class.php");
	include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."alkquery.class.php");
	include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."alkdataform.class.php");

  $dbConn = null;

  switch( ALK_TYPE_BDD ) {
  case SGBD_ORACLE:
    include_once("./../../classes/err/errOracle.class.php");
    include_once("./../../classes/db/droracle.class.php");
    include_once("./../../classes/db/dsoracle.class.php");
    include_once("./../../classes/db/dboracle.class.php");

    $dbConn = new DbOracle(ALK_ORA_LOGIN, ALK_ORA_PWD, ALK_ORA_SID);
    $dbConn->connect();

    break;

  case SGBD_MYSQL:
    include_once("./../../classes/err/errMysql.class.php");
    include_once("./../../classes/db/drmysql.class.php");
    include_once("./../../classes/db/dsmysql.class.php");
    include_once("./../../classes/db/dbmysql.class.php");
   
    $dbConn = new DbMySql(ALK_MYSQL_LOGIN,  ALK_MYSQL_HOST,  ALK_MYSQL_PWD,  ALK_MYSQL_BD,  "",  ALK_MYSQL_PORT);
    $dbConn->connect();

    break;
  }
  
include_once("./classes/queryatlas_".ALK_TYPE_BDD.".class.php");

 $queryAtlas = null;

 switch( ALK_TYPE_BDD ) {
   case SGBD_ORACLE :
     $queryAtlas = new QueryAtlasOracle($dbConn, $_LG_tab_langue);
     break;
     
   case SGBD_MYSQL :
     $queryAtlas = new QueryAtlasMysql($dbConn, $_LG_tab_langue);
     break;
     
   default:
     echo "Impossible d'instancier les classes atlas.";
     exit();
  }
?>