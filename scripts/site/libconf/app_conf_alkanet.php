<?php

/**
 * @file   app_conf_alkanet.php
 * @author P.C
 * 
 * @brief  liste de constantes sp�cifiques au type d'application SIALKE
 * 
 * Chaque constante d�finie ici est un �quivalent d'une variable de session de
 * l'ancienne version de l'appli.
 */

/**
 * Param�tres g�n�riques :
 *   - nom de l'application, version, titre
 */

define("ALK_APP_NAME",    "ALKANET");
define("ALK_APP_VERSION", "1.01");
define("ALK_APP_TITLE",   "IFREMER Atlas DCE");
define("ALK_URL_ON_LOGO", "");

define ("ALK_I_AGENT_INTERNET",2);
define ("ALK_I_PROFIL_INTERNET",4);

/**
 * Indique si l'API � base de classes est utilis�e.
 */
define("ALK_B_API_FORM_CLASS", true);
define("ALK_FIELD_NOT_VIEW", "_NOT_PUB_");

/** type d'onglet */
define("ALK_CONSULT_TYPESHEET",   0);
define("ALK_ADMIN_TYPESHEET",     1);
define("ALK_PROPRIETE_TYPESHEET", 2);

/** onglet pour la consultation annuaire */
define("ALK_RECHERCHE_SHEET_ANNU",   0);
define("ALK_RESULTAT_SHEET_ANNU",    1);
define("ALK_MAFICHE_SHEET_ANNU",     2);
define("ALK_ADDFICHE_SHEET_ANNU",    3);

/** @brief R�pertoire upload */
define("ALK_PATH_UPLOAD_CARTE", "upload/carte/");
define("ALK_PATH_UPLOAD_FICHE", "upload/fiche/");
define("ALK_PATH_UPLOAD_DOC", "upload/doc/");
/**
 * Url et Path
 */

/** URL � la racine de l'appli sialke */
define("ALK_SIALKE_URL", ALK_ROOT_URL.ALK_SIALKE_DIR);
define("ALK_ALKANET_ROOT_URL", ALK_SIALKE_URL);

/** Chemin � la racine de l'appli sialke */
define("ALK_SIALKE_PATH", ALK_ROOT_PATH.ALK_SIALKE_DIR);
define("ALK_ALKANET_ROOT_PATH", ALK_SIALKE_PATH);

/** url d'identification */
define("ALK_URL_IDENTIFICATION", ALK_ROOT_URL."/admin/index.php");

/** r�pertoire de base des m�dias */
define("ALK_URL_SI_MEDIA", ALK_SIALKE_URL."media/");

/** r�pertoire des images n�cessaires pour les boutons */
define("ALK_URL_SI_IMAGES", ALK_URL_SI_MEDIA."imgs/gen/");

/** caract�re s�parateur r�pertoire */
define("ALK_PATH_SPACE_LINK", "/");

/** r�pertoire des classes application et espace */
define("ALK_DIR_CLASSE_APPLI", "appli/");

/** r�pertoire du fichier d'identification */
define("ALK_DIR_IDENTIFICATION", "ident/");

define ("ALK_B_PRINT", false);

/** 
 * Navigateur client
 */

/** Type de navigateur */
define("NAV_IE",        "IE");
define("NAV_OPERA",     "Opera");
define("NAV_NETSCAPE4", "Netscape4");
define("NAV_NETSCAPE6", "Netscape6");
define("NAV_OTHER",     "Navigateur inconnu");

/** D�tection du navigateur client */
$strUserAgent = $_SERVER["HTTP_USER_AGENT"];


if( preg_match('/msie/i', $strUserAgent) && !preg_match('/opera/i', $strUserAgent) ) {
	define("STRNAVIGATOR", NAV_IE);
}elseif( preg_match('/opera/i', $strUserAgent) ) {
  define("STRNAVIGATOR", NAV_OPERA);
} elseif( preg_match('/Mozilla\/4./i', $strUserAgent) ) {
  define("STRNAVIGATOR", NAV_NETSCAPE4);
} elseif( preg_match('/Mozilla\/5.0/i', $strUserAgent) && !preg_match('/Konqueror/i', $strUserAgent) ) {
  define("STRNAVIGATOR", NAV_NETSCAPE6);
} else {
  define("STRNAVIGATOR", NAV_OTHER);
}





/**
 * D�tection du type d'API utilis� (V1 ou V2)
 */

$PHP_SELF = $_SERVER["PHP_SELF"];

error_reporting(E_ALL);

define("ALK_B_API_CONN_V2", true);
?>