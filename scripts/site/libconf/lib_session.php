<?
session_start();

include_once("app_conf.php");
include_once("app_conf_alkanet.php");
include_once("../../lib/lib_request.php");

if (PHP_VERSION>='5')
   ini_set('zend.ze1_compatibility_mode', '1');       

/**
 * Gestion des variables de session
 */

if( !isset($_SESSION["sit_idUser"]) ||
    (isset($_SESSION["sit_idUser"]) && !is_numeric($_SESSION["sit_idUser"])) ) {
    
  $_SESSION["sit_idUser"] = ALK_I_AGENT_INTERNET;
  $_SESSION["sit_idUserProfil"] = ALK_I_PROFIL_INTERNET;
  $_SESSION["sit_idUserService"] = "-1";
}

/**
 * @brief Controle de s�curit�
 */

$strPageCur = $_SERVER["PHP_SELF"];
$strPageCur = substr($strPageCur, 1); // enleve le premier caract�re = /

// evite d'afficher la popup d'identification pour les pages suivantes
if( strpos($strPageCur, "identification.php")===false && 
    strpos($strPageCur, "login_extranet_form.php")===false &&
    strpos($strPageCur, "login_extranet_verif.php")===false &&
    strpos($strPageCur, "login_sql.php")===false
    )
{
  if( !(isset($_SESSION["sit_idUser"]) && is_numeric($_SESSION["sit_idUser"])) )
    {?>
 <html><head>
    <script language="javascript">
    function OpenWindowLogin() {
      var adr = "<?php echo ALK_SIALKE_URL; ?>site/login_form.php";
      var oWind = window.open(adr, 'loginWindow', 
            'toolbar=no,scrollbars=yes,menubar=no,status=no,resizable=no,width=550,height=200,allwaysRaised=yes,screenX=0,screenY=0');
      oWind.focus();
    }
 </script>
     </head>
     <body onload="OpenWindowLogin()"></body>
     </html><?
     exit();
    }
}
?>