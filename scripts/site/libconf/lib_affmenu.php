<?
/// <comment>
/// <summary>
/// Affiche l'entete de la page
/// </summary>
/// </comment>
function aff_header($bAccueil=false)
{
  global $PHP_SELF;
  global $page_titre1;
  global $strLgBdd;
  $bCar = !(strpos($PHP_SELF, "car_3_DTI")===false);

	echo getHtmlHeader($bAccueil);
}

function getHtmlHeader($bAccueil=false)
{
  global $PHP_SELF;
  global $page_titre1;
  global $strLgBdd;
  $bCar = !(strpos($PHP_SELF, "car_3_DTI")===false);

 
$strHtml = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\">". 
		"<html>" .
		"<head>" .
		"<title>IFREMER Atlas DCE</title>" .
		"<meta name =\"title\" content=\"IFREMER Atlas DCE\">" .
		"<meta name=\"description\" content=\"ALKANTE\">" .
		"<meta NAME=\"language\" content=\"".substr($strLgBdd,1)."\">" .
		"<meta name =\"Content-Type\" content=\"text/html; charset=iso-8859-1\">".
		"<meta name=\"Microsoft Border\" content=\"tlb, default\">" .				
		( isset($_GET["bPdf"]) && $_GET["bPdf"]=="1" ? 
			"<link rel=\"stylesheet\" href=\"".ALK_SIALKE_URL."styles/site_pdf.css\" type=\"text/css\">" : 
			"<link rel=\"stylesheet\" href=\"".ALK_SIALKE_URL."styles/site_gen.css\" type=\"text/css\">");
		return $strHtml;
}

function getHtmlFooter()
{
$strHtml = "</body>" .
		"</html>";
		
	return $strHtml;
}
/// <comment>
/// <summary>
/// Affiche le pied de page
/// </summary>
/// </comment>
function aff_footer()
{
	echo getHtmlFooter();
}

?>