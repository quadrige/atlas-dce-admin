<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("lib/lib_pagination.php");
include_once("classes/alkmasseeau.class.php");
include_once("lib/lib_pdf.php");


$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$page =  Request("page", REQ_GET, "1", "is_numeric");
$bPdf = Request("bPdf", REQ_GET, "0", "is_numeric");
$codeBassin = Request("bassin_id", REQ_GET, ""); // en fait c'ets le bassin code'
$menu_id=0;
$iAcces = 0;
$strParam = "bassin_id=".$codeBassin;

$bassin_id = "";

$dsBassin = $queryAtlas->getDs_bassin($codeBassin);
if ($drBassin = $dsBassin->getRowIter()){
  $bassin_id = $drBassin->getValueName("BASSIN_ID");
  $bassin_nom = $drBassin->getValueName("BASSIN_NOM");
}

$tabPageReseau = array();
$tabAlign = array("","left");

$dsReseau = $queryAtlas->getDs_listeReseau(0, -1, $bassin_id);

while ($drReseau = $dsReseau->getRowIter()) {
		
  $reseauNOM = $drReseau->getValueName("LIB"); 
  $reseauCode = $drReseau->getValueName("CODE");
  
  $reseauNOM = "<a href=\"javascript:void(OpenPopupWindow('".ALK_SIALKE_URL."scripts/site/liste_etat_reseau_point.php?reseau_code=".$reseauCode."&bassin_id=".$codeBassin."', '900', '900', 'ficheetatreseaupoint'));\">".$reseauNOM."</a>";
  $tabPageReseau[] = array($reseauNOM);  								
} 

$strHtml = "<script language=javascript>".
            "function OpenFichePdf()".
            "{".
            "OpenPopupWindow('".ALK_SIALKE_URL."scripts/site/liste_etat_reseau.php?".$strParam."&bPdf=1','600','800','fiche_me');".
            //"OpenFooterExec('liste_etat_reseau_point.php?".$strParam."&bPdf=1');".             
            "}</script>".
            "<div class='popupTitle'>Liste des r�seaux de surveillance".           
            "<div class=\"popupBtClose\"><div onclick=\"javascript:window.close()\" class=\"btClose\"/></div></div></div>";						
                 
$strHtml .= "<div id='conteneur'>";

$strHtml .= "<div id='titreME' class='titreMEdetaille'>".	
              "<h1>Liste des r�seaux de surveillance du bassin ".$bassin_nom.          
              "</h1>
            </div>
            <div id='contenuME' class='txt colon classMEdetaille ".($bPdf != 1 ? "clearfix" : "")."'>		
            <table class='table1'  cellpadding='0' cellspacing='0'>
            <thead>
            <tr class='trEntete1' >
            <td class='tdEntete1_300' rowspan=1 >R�seau</td>
            </tr>
            <tr>";

$strHtml .= "</tr></thead>".getHtmlListePagine($tabPageReseau, count($tabPageReseau), count($tabPageReseau), $page, 
                               $_SERVER["PHP_SELF"]."?".$strParam, 
                               $tabAlign)."</table>";
$strHtml .= "</div>";

 $strHtml .= "<div id='logos'>".
  				"<img id='logoIfremer' src='".ALK_SIALKE_URL."media/imgs/gen/Ifremer_logo.gif' width='110' />".
				"<img id='logoAE' src='".ALK_SIALKE_URL."media/imgs/gen/".$codeBassin."_logo_agence_de_leau.jpg' />".
            "</div>";

  $strHtml .= "</div>";
  $strHtml .= "<div id='pied_page'></div>";
  
if ($bPdf == 1) {
	$strHtml = str_replace(ALK_ROOT_URL, ALK_ROOT_PDF_URL, getHtmlHeader()).
			"</head>" .
      "<body class='bodyPdf'>".$strHtml.getHtmlFooter();

	//$strFile = getPdf2($strHtml, "", "", $codeBassin."_synthese");
	affPdf($strHtml, $codeBassin."_synthese");	
} else {
  aff_menu_haut();
  echo $strHtml;
  aff_menu_bas();
}	



?>