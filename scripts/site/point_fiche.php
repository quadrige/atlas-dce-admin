<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("lib/lib_pdf.php");


$tabEventBody["onLoad"] =" top.HideLoadMsg();";

$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$codePoint = Request("id", REQ_GET, "-1");
$reseau = Request("reseau", REQ_GET, "-1");
$bPdf = Request("bPdf", REQ_GET, "0", "is_numeric");
$session_id = Request("idSession", REQ_GET, "-1", "is_numeric");

$idReseau = -1;

$bassin_id = -1;
$dsPoint = $queryAtlas->getPointByCode($codePoint, "", "", $session_id);
if ($drPoint = $dsPoint->getRowIter()) {
 $bassin_id = $drPoint->getValueName("BASSIN_ID");
}

//Les r�seaux peuvent �tre plusieurs pour le m�me point, si tel est le cas, proposer les liens des fiches vers chacun des r�seaux
//recup GET strListReseau
$listeReseau = Request("strListReseau", REQ_GET, "");
if ($listeReseau != ""){
	$strHtml = "";
	$tabReseau = explode(";", $listeReseau);
	array_pop($tabReseau);
	$tabReseauPoint = array();
	if (count($tabReseau) > 1){
		//on verifie si le point appartient � plusieurs r�seaux et 
	  //si parmi ces r�seaux figurent ceux pass�s en param�tre (=ceux coch�s et affich�s sur la carte de provenance)
	  //pour les r�seaux communs (reseaux coch�s et reseaux auxquels le point appartient, on propose � l'utilisateur la liste des r�seaux)
		$dsReseau = $queryAtlas->getDs_listeReseauByPoint($codePoint, $session_id);
		while ($drReseau = $dsReseau->getRowIter()){
			if (in_array($drReseau->getValueName("RESEAU_CODE"), $tabReseau))
				array_push($tabReseauPoint, $drReseau->getValueName("RESEAU_CODE"));				
		}
				
		if (count($tabReseauPoint) > 1){
		  $strHtml = '<ul>';
			foreach($tabReseauPoint as $codeReseau)
			{
				$dsReseau = $queryAtlas->getReseauByCode($codeReseau, $bassin_id, $session_id);
				if ($drReseau = $dsReseau->getRowIter()){
					$idReseau = $drReseau->getValueName("RESEAU_ID");
					$strReseau = $drReseau->getValueName("RESEAU_NOM");
					$strHtml .= "<li><a href='point_fiche.php?id=".$codePoint."&reseau=".$codeReseau."&idSession=".$session_id."'>Fiche du point ".$codePoint." pour le r�seau ".$strReseau."</a></li>";
				}	
			}
			$strHtml .= '</ul>';
		
			aff_menu_haut($tabEventBody);
			
			$strHtml = "<div class=\"popupTitle\">Fiche point r�seau".
			           "<div class=\"popupBtClose\"><div onclick=\"javascript:top.closeWindow('')\" class=\"btClose\"/></div></div></div>".		             
								 "<div id='conteneur'>
								 	<div class='colon txt_colon' >".
										"<div class='txt'>Ce point appartient � plusieurs r�seaux, vous trouverez ci-dessous les liens vers les fiches pour ses diff�rents r�seaux<br/><br/>".$strHtml."</div>".										
									"</div></div>";
		  
			echo $strHtml;
			aff_menu_bas();
			
			exit();
		}
	}	
}

$menu_id=0;
$iAcces = 0;

$strUrlFileName = ALK_SIALKE_URL.ALK_PATH_UPLOAD_CARTE.$reseau.".pmf";
$strParam = "id=".$codePoint."&reseau=".$reseau."&idSession=".$session_id;  

$strReseau = "";
$dsReseau = $queryAtlas->getReseauByCode($reseau, $bassin_id, $session_id);
if ($drReseau = $dsReseau->getRowIter()){
	$idReseau = $drReseau->getValueName("RESEAU_ID");
	$strReseau = $drReseau->getValueName("RESEAU_NOM");
	$strGroupeReseau = $drReseau->getValueName("GROUPE_NOM");
}
$dateMajReseau = "";
$dsPoint = $queryAtlas->getPointByCode($codePoint, $idReseau, "", $session_id);
if ($drPoint = $dsPoint->getRowIter()) {
	$idPoint = $drPoint->getValueName("POINT_ID");
	$strLongitude = $drPoint->getValueName("POINT_LONGITUDE");
	$strLatitude = $drPoint->getValueName("POINT_LATITUDE");
	$strNomPoint = $drPoint->getValueName("POINT_CODE")." - ".$drPoint->getValueName("POINT_NOM");
	$strNomMe = $drPoint->getValueName("MASSE_CODE")." - ".$drPoint->getValueName("MASSE_NOM");
	$strTypeMe = $drPoint->getValueName("MASSE_TYPE");
  if($idReseau!=""){
  	$dateMajReseau = $drPoint->getValueName("RESEAU_DATE_MAJ");
  }
	$dsParam = $queryAtlas->getParamByPointReseau($idReseau, $idPoint, $session_id);
		
} else {
	
	echo "Aucun point trouv�";
	exit();
}

$strHtml = "<script language=javascript>".
					 "function OpenFichePdf()".
					 "{".
					 "OpenPopupWindow('".ALK_SIALKE_URL."scripts/site/point_fiche.php?".$strParam."&bPdf=1','600','700','point_fiche2');".
           //"OpenFooterExec('point_fiche.php?".$strParam."&bPdf=1');".  
					"}".
					"</script>".							
					"<div class=\"popupTitle\">Fiche point r�seau".
					($bPdf == 1 ? "" : 
	          "<a class=\"imprim\" title=\"Imprimer\" href=\"javascript:OpenFichePdf();\">
            <img id=\"imprimer\" height='19' width='19' onmouseout='MM_swapImgRestore()' onmouseover=\"MM_swapImage('imprimer','','".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif' ,1)\" alt='Exporter en pdf' title='Exporter en pdf' src='".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif'/>
            </a>").
						"<div class=\"popupBtClose\"><div onclick=\"javascript:top.closeWindow('')\" class=\"btClose\"/></div></div></div>";
	
	$strHtml .= "<div id='conteneur'>";
	
	$strHtml .= "<div id='titreME' class='titreMEdetaille'>".
							"<h1>R�seau : ".$strReseau."<br/>".$strNomPoint."</h1>".																		
								"</div>".
							"<div id='contenuME' class='txt colon classMEdetaille ".($bPdf != 1 ? "" : "clearfix")."'>".					              									 	              
                "<div  class='txt largType1'>".
									"Masse d'eau : <b>".$strNomMe."</b>".
								"</div>".
								"<div class='txt largType1'>".
									"Type masse d'eau : <b>".$strTypeMe."</b>".
								"</div>".
	              ($strGroupeReseau != "" ? 
								"<div class='txt largType1'>".
									"Type de r�seau : <b>".$strGroupeReseau."</b>".
								"</div>" : "").
								"<div class='txt largType1' >".
									"Point : <b>".$strNomPoint."</b>".
								"</div>".
                "<div class='txt largType1' >".
									"Longitude (WGS84) : <b>".$strLongitude."</b>".
								"</div>".
                "<div class='txt largType1' >".
									"Latitude (WGS84) : <b>".$strLatitude."</b>".
								"</div>".
					"</div>".
					"<table class='colon txt_colon'>".
							"<thead>".	
							"<tr class=trEntete1>	".		
								//"<td class=tdEntete1 width=100 align=center>Support</td>".
								"<td class=tdEntete1 width=180 align=center>Param�tres</td>".
								"<td class=tdEntete1 width=100 align=center>Op�rateur terrain</td>".
								"<td class=tdEntete1 width=100 align=center>Op�rateur labo</td>".
								"<td class=tdEntete1 width=50 align=center>Derni�re ann�e de pr�l�vement</td>".
								"<td class=tdEntete1 width=50 align=center>Fr�quence annuelle</td>".
								"<td class=tdEntete1 width=50 align=center>P�riode</td>".
								"<td class=tdEntete1 width=100 align=center>Fr�quence dans plan de gestion</td>".
							"</tr>".
							"</thead><tbody>";
									
							$i=0;
								while ($drParam = $dsParam->getRowIter()) {				
									$strStyleLigne = (fmod($i, 2) == 0 ? "Pair" : "Impair");
													
									$strHtml .= "<tr class=tr".$strStyleLigne."1>" .
											//"<td class=td".$strStyleLigne."1>".$drParam->getValueName("SUPPORT_NOM")."</td>".
											"<td class=td".$strStyleLigne."1>".$drParam->getValueName("PARAMETRE_NOM")."</td>".
											"<td class=td".$strStyleLigne."1>".($drParam->getValueName("TERRAIN_NOM") != "" ? $drParam->getValueName("TERRAIN_NOM") : "&nbsp;")."</td>".
											"<td class=td".$strStyleLigne."1>".($drParam->getValueName("LABO_NOM") != "" ? $drParam->getValueName("LABO_NOM") : "&nbsp;")."</td>".
											"<td class=td".$strStyleLigne."1 align=center>".($drParam->getValueName("ANNEE_PRELEVEMENT") != "" ? $drParam->getValueName("ANNEE_PRELEVEMENT") : "&nbsp;")."</td>".
											"<td class=td".$strStyleLigne."1 align=center>".($drParam->getValueName("FREQUENCE") != "" ? $drParam->getValueName("FREQUENCE") : "&nbsp;")."</td>".
											"<td class=td".$strStyleLigne."1 align=center>".($drParam->getValueName("PERIODE") != "" ? $drParam->getValueName("PERIODE") : "&nbsp;")."</td>".
											"<td class=td".$strStyleLigne."1 align=center>".($drParam->getValueName("FREQUENCE_PLAN") != "" ? $drParam->getValueName("FREQUENCE_PLAN") : "&nbsp;")."</td>".
											"</tr>";
									$i++;			
							}
							
					$strHtml .= "</tbody></table>";

  if($dateMajReseau !=""){
    $strHtml .= "<div>Derni�re mise � jour :".$dateMajReseau."</div>";
  }


	$strHtml .= "<div id='logos'>".
  							"<img id='logoIfremer' src='".ALK_SIALKE_URL."media/imgs/gen/Ifremer_logo.gif' width='110' />".
					  		"<img id='logoAE' src='".ALK_SIALKE_URL."media/imgs/gen/".$drPoint->getValueName("BASSIN_CODE")."_logo_agence_de_leau.jpg' />".
	            "</div>";
								   
  $strHtml .= "</div>";
  $strHtml .= "<div id='pied_page'></div>";
  
if ($bPdf == 1) {
	//$strFile = getPdf($strHtml, $idReseau, $idPoint);
	$strHtml = str_replace(ALK_ROOT_URL, ALK_ROOT_PDF_URL, getHtmlHeader()).
			"</head>" .
			"<body>".$strHtml.getHtmlFooter();
			
	//$strFile = getPdf2($strHtml, $idReseau, $idPoint);
	affPdf($strHtml, "fiche_point".$idPoint);	
	
} else {
	aff_menu_haut($tabEventBody);
	echo $strHtml;
	aff_menu_bas();
}
?>