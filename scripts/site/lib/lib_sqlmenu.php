<?php
$g_strLogin = "";

/**
 * @brief Affiche le haut de la page (Logo + bandeau haut + menu gauche)
 *
 * @param tabNav       Tableau associatif contenant les liens de navigation
 * @param iDroitAdmin  Droit sur l'appli en cours =2 admin, =1 lecture, =0 aucun
 * @param tabAppliMenu Tableau associatif contenant les infos statiques sur des �l�ments de menu � inclure
 */
function aff_menu_haut($bAccueil=false)
{
  global $strLgBdd;

  // R�cup�res les param�tres
  $numargs = func_num_args();

  // affiche l'entete html
  aff_header($bAccueil);

?>
<script language="JavaScript">
<!--
var navigateur = null;
var x = null;
var y = null;
function MM_reloadPage(init) { //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
//Hauteur d'une ligne pour un layer avec un item : 40 pixels. Hauteur d'un bloc : 40 pixels
function position(e) {
  var nH2, h2, v;
  x = (navigator.appName.substring(0,3) == "Net") ? e.pageX : event.x; //+document.body.scrollLeft;
  y = (navigator.appName.substring(0,3) == "Net") ? e.pageY : event.y; //+document.body.scrollTop;
  
  // variables globales
  // x et y : position de la souris
  if( document.all && document.body.scrollLeft ) x += document.body.scrollLeft;
  if( document.all && document.body.scrollTop ) y += document.body.scrollTop;
}

if(navigator.appName.substring(0,3) == "Net") document.captureEvents(Event.MOUSEMOVE);
document.onmousemove = position;
verStr = navigator.appVersion;
app = navigator.appName;
version = parseFloat(verStr);
if( (app.indexOf('Netscape') != -1)&&(version <= 4.75) ) {
  // alert('netscape <= 4,75');
  var navigateur = "ecrit";
}
var navigateur = navigator.appName;
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
  if  ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function OpenWindow(strPage, hauteur, largeur, strName, strStatus, strMenu)
{
  if( !strName ) strName = "_blank";
  if( !strStatus ) strStatus = "no";
  if( !strMenu ) strMenu = "no";

  var hauteur_popup=400;
  var largeur_popup=520;
  if (typeof (hauteur) != 'undefined'){hauteur_popup=hauteur} 
  if (typeof (largeur) != 'undefined'){largeur_popup=largeur} 
  var H = (screen.height - hauteur_popup) / 2;
  var L = (screen.width - largeur_popup) / 2;
  popupWindow = window.open(strPage,strName,"status="+strStatus+",menubar="+strMenu+",scrollbars=yes,resizable=yes,height="+hauteur_popup+",width="+largeur_popup+",top="+H+",left="+L);
  return popupWindow;
}

function OpenPrintPreview()
{
  var strUrl = location.href;
  if( strUrl.substr(-4)!=".php" )
    strUrl = strUrl +"&print=1";
  else
    strUrl = strUrl +"?print=1";
  var strName = "PrintPreview";
  var hauteur_popup = 400;
  var largeur_popup = 630;
  var H = (screen.height - hauteur_popup) / 2;
  var L = (screen.width - largeur_popup) / 2;
  popupWindow = window.open(strUrl, strName, "menubar=yes,status=yes,scrollbars=yes,resizable=yes,height="+hauteur_popup+",width="+largeur_popup+",top="+H+",left="+L);
}

// -->

</script>

</head>

<body class=page text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td>
<!-- HEADER -->

<div id="header" width="985">
  <h1>
  <img border="0" src="http://www.ifremer.fr/dce/accueil/pictogramme.png" align="left" width="40" height="25" hspace="3" alt> La directive cadre sur l'eau</h1>
  <div id="bandeau">
    <a href="http://www.ifremer.fr/">
    <img src="http://www.ifremer.fr/dce/css/logo_bandeau.png" width="985" height="45" border="0" alt="Ifremer"></a>
  </div>
	<!-- titre de page -->

  <h2>
  La Directive cadre sur l'eau</h2>
	<!-- Fin titre de page -->
</div>

<!--<div id="top">
<!-- Barre de menus  -->
<!--   <div id="top-ecran">
      		  <div id="top-maison">
            <a href="http://www.ifremer.fr/" title="Site institutionnel Ifremer">Ifremer</a>
            </div>  
            <div id="top-menu">
            <a href="http://www.ifremer.fr/dce/index.htm" title="Page d'accueil du site">Accueil DCE</a>
            <a title="Page d'accueil Environnement littoral" target="_blank" href="http://www.ifremer.fr/envlit/index.htm">Environnement Littoral</a>
            </div>
            <div id="top-langue">
            <!--            <a href="http://www.ifremer.fr/anglais" title="Home page" class="fr">English</a>             <a href="http://www.ifremer.fr/francais" title="Page d'accueil" class="en">Fran�ais</a>            -->
            </div>
   </div>
   -->
<!-- Banniere  de site  -->   
<!--   <div id="top-bandeau">
       <div id="top-titre-site"> La directive cadre sur l'eau</div>        
   		<div id="top-logo">
    		<a href="http://www.ifremer.fr/">
       	<img src="<?=ALK_URL_SI_IMAGES?>gen/ifremer.gif" border="0" height="45" width="150"></a>
   		</div>  
   		<div id="top-guide">
    		<a href="http://www.ifremer.fr/annuweb/index.php" title="Annuaire des sites web Ifremer">Guide Web</a>
   		</div>
   </div>     
</div>
-->
<!-- Boutons navigation parents + Banniere de  page  -->	    
<!--	<div id="navig-parents">
    	<nobr><a href="http://www.ifremer.fr/dce/">.</a></nobr><br><nobr><a href="http://www.ifremer.fr/dce/1_intranet/index.htm">Intranet&nbsp;Ifremer&nbsp;DCE</a></nobr><br><nobr>Partenaires&nbsp;DCE</nobr>
    </div>
    <div id="top-titre-page">
    Atlas DCE
    </div>
-->
<div id="bordures">
  <!-- COLONNE MENU - Bordure gauche -->
  <div id="colMenu">
    <ul id="menuNiveau1">
      <li><a href="http://www.ifremer.fr">Accueil Ifremer</a></li>
      <li><a target="_blank" href="http://www.ifremer.fr/envlit/index.htm">
      Environnement littoral</a></li>
      <li><a href="index.htm">Accueil site</a></li>

      
    </ul>
    <div id="remonter">
	<nobr><a href="./">Remonter</a></nobr><br><nobr>La&nbsp;Directive&nbsp;cadre&nbsp;sur&nbsp;l'eau</nobr><br><nobr><a href="http://www.ifremer.fr/dce/1_intranet/index.htm">Intranet&nbsp;Ifremer&nbsp;DCE</a></nobr><br><nobr><a href="http://www.ifremer.fr/dce/2_extranet/index.htm">Partenaires&nbsp;DCE</a></nobr>

    </div>
    <div id="menuNiveau2">
      <div id="niveau2">
	<nobr><a href="http://www.ifremer.fr/dce/presentation.htm">La&nbsp;Directive&nbsp;cadre&nbsp;sur&nbsp;l'eau</a></nobr><br><nobr>Intranet&nbsp;Ifremer&nbsp;DCE</nobr><br><nobr><a href="http://www.ifremer.fr/dce/2_extranet/index.htm">Partenaires&nbsp;DCE</a></nobr>

      </div>
    </div>
    <div id="menuNiveau3">
      <div id="niveau3">
		
      </div>
    </div>
  </div>
  <!-- FIN COLONNE MENU  -->
  <!-- #################  -->

  </div>
</td></tr><!--msnavigation--></tbody></table><!--msnavigation-->

    <!--fin menu haut - debut du contenu -->
  <?
}


/**
 * @brief Affiche le bas de la page
 *
 */
function aff_menu_bas()
{
  ?>
  <!--  fin contenu - debut menu bas-->
    
  <!--<table border="0" cellpadding="0" cellspacing="0" width="100%">
  	<tbody>
  		<tr>
  			<td>
					<div id="pied">
					<div id="pied-pictos">
			       &nbsp;&nbsp;
			       <a href="mailto:jean.yves.quintin@ifremer.fr">
			        <img src="<?=ALK_URL_SI_IMAGES?>gen/email.gif" border="0" height="32" width="32"></a>
			 		<br><br>
			 		<img src="<?=ALK_URL_SI_IMAGES?>gen/trait-bottom.gif" border="0" height="5" width="162"><br>
			 		</div>
			 		<div id="pied-date">
			 		Mis � jour le 
			        11/12/06<br>
			 		<a href="http://www.ifremer.fr/francais/copyrigh.htm" title="Droits sur les informations pr�sent�es">Ifremer �2005</a> 
					<br>&nbsp;
					</div>
					</div>
					<p>&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>-->
<?
  aff_footer();
}

/**
 * @brief Fonction d'affichage du menu propre a l'application
 */
function aff_menu_gauche($idCourant)
{
  global $strLgBdd;
?>  
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
  	<tr>
  		<td valign="top" width="1%">
				<div id="left">
					<p>
						<img src="<?=ALK_URL_SI_IMAGES?>pix.gif" height="200" width="190"><br>
					</p>
					
				</div>
			</td>
			<td valign="top" width="24"></td>
			
			<!--msnavigation-->
			<td valign="top">
				<br>
<?
}


function aff_fils($arbreMenu, $cont_id, $appli_id, $idCourant)
{
  global $iNiveauMenuAppli;
  global $page_id_racine;

  $iNiveauMenuAppli++;
  
  $nbFils = $arbreMenu->nb_fils();
  for ($i=1; $i<=$nbFils; $i++){
    $arbreMenu->descendre($i);
    $tabMenu = $arbreMenu->valeur();
    $id = $tabMenu["id"];
    $libelle = $tabMenu["nom"];
    if (strpos($tabMenu["lien"],"?")>0)
      $lien = ALK_SIALKE_URL."scripts/".$tabMenu["lien"]."&menu_id=".$id;
    else
      $lien = ALK_SIALKE_URL."scripts/".$tabMenu["lien"]."?menu_id=".$id;
    
    $iLgIndent = ($iNiveauMenuAppli) * 10;
    $strImgPuce = ($id == $idCourant)?
                  "_roll":
                  "";
    if ($id == $idCourant)
      $strClasse = " class=SrubsSelect";
    else
      $strClasse = ($iNiveauMenuAppli >1 && $id != $idCourant )?
                    " class=srubs":
                    " class=Srubs";
    ?>
        <table width="100%" height="9"  border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="1" height="1" border="0"></td>
          </tr>
        </table>
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <? if ($iNiveauMenuAppli==1) {?>
            <td width="15" valign="top"><img src="<?=ALK_URL_SI_IMAGES?>gen/Puce01<?=$strImgPuce?>.gif" width="15" height="3" vspace="5" border="0"></td>
            <?} else {?>
            <td width="<?=$iLgIndent?>" valign="top"><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="<?=$iLgIndent?>" height="3" vspace="5" border="0"></td>
            <?}?>
            <td width="6"><img src="<?=ALK_URL_SI_IMAGES?>gen/transp.gif" width="1" height="1" border="0"></td>
            <td valign="top" class="Srubs"><?=(($iNiveauMenuAppli==1)?"":">&nbsp;") ?><a href="<?=$lien ?>" <?=$strClasse?>><?=$tabMenu["nom"] ?></a></td>
          </tr>
        </table>    
    <?
    aff_fils($arbreMenu, $cont_id, $appli_id, $idCourant);
    $arbreMenu->remonter();
    $iNiveauMenuAppli--;
  }
}

/**
 * @brief Fonction d'affichage du menu propre a l'application
 */
function aff_menu_droite()
{


}

function aff_fin_menu_gauche()
{
?>
			</td></tr><!--msnavigation--></tbody></table><!--msnavigation-->
<?
}
?>