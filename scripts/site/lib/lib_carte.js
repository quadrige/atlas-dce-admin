var iCptPopup = 0;
        
function slideMenu(slide)
{
  var targetElement;
  targetMenuCarte = document.getElementById('menu_carte');

  if (slide=='on'){
    // on affiche le menu de gauche
    if (targetMenuCarte.style.display == 'none'){
      targetMenuCarte.style.display = '';
      panZoom.moveTo(new OpenLayers.Pixel(296, 4));
    }
      
    // on affiche le titre
    targetElement = document.getElementById('titre_carte');
    if (targetElement.style.display == 'none'){
      targetElement.style.display = '';
    }
    
    targetElement = document.getElementById('slider_carte');
    targetElement.style.left='280px';

    targetElement = document.getElementById('slider_cursor_on');
    targetElement.style.display = 'none';

    targetElement = document.getElementById('slider_cursor_off');
    targetElement.style.display = '';
  } else {
      panZoom.moveTo(new OpenLayers.Pixel(30, 4));
    // on affiche le menu de gauche
    targetMenuCarte.style.display = 'none';

    targetElement = document.getElementById('titre_carte');
    targetElement.style.display = 'none';
    
    targetElement = document.getElementById('slider_carte');
    targetElement.style.left='0';
    targetElement.style.top='0';
    
    targetElement = document.getElementById('slider_cursor_on');
    if (targetElement.style.display == 'none'){
      targetElement.style.display = '';
    }

    targetElement = document.getElementById('slider_cursor_off');
    targetElement.style.display = 'none';
  }
}

function montreCacheThema(id) {
  var d = document.getElementById(id);
  if (d) {
    if (d.style.display == 'none'){
      d.style.display='block';
    } else {
      d.style.display='none';
    }
  }
}
        
function montreCachePopup(idDiv, idLien ){
  var d = document.getElementById(idDiv);
  var i = document.getElementById(idLien);

  var oStyle = ( d.style ? d.style : d );
  if (d) {
    if (oStyle.display == 'none'){
      oStyle.display='block';
      i.innerHTML = 'Fermer';
    } else {
      oStyle.display='none';
      i.innerHTML = 'Lire';
    }
  }
}

/**
 * @brief montre/cache un element
 * @return
 */
function showHide(ElementId){
  if (document.getElementById(ElementId).style.display=="none")
    document.getElementById(ElementId).style.display = "block";
  else
    document.getElementById(ElementId).style.display = "none";  
}

function displayClassement(valueCombo){

  if(valueCombo == 0){
    if(document.getElementById("trClassement").style.display=="none")
      document.getElementById("trClassement").style.display = "block"; 
  }else{
    document.getElementById("trClassement").style.display ="none"
  }
}

function displaySubQualite(qualitevalue, bassin_id, session_id){
  var oAjax = new AlkAjax('qualiteAjax', 'fnLoadSubqualite', "GET", ALK_SIALKE_URL+'scripts/site/sub_qualite.php?qualite_id='+qualitevalue+'&bassin_id='+bassin_id+'&idSession='+session_id, null);
}

function fnLoadSubqualite(strHTML)
{
  // retourne le code html de la liste des blocs de la page
  if(strHTML!=""){
	document.carteForm.subqualite_id.innerHTML = strHTML;
	document.getElementById("divSubqualite").style.display='block';
  }else{
	document.getElementById("divSubqualite").style.display='none';
  }
}
//partie carto openlayers
var map, selectControl, selectedFeature, selectedPoint, highlightCtrl;
var iSession;

function onFeatureSelect(feature) {
  selectedFeature = feature;
  
  var iLarg = 850;
  var iHauteur = 900;
  //cas masse d'eau
  if (typeof(feature.attributes.code_me)!="undefined"){ 
    if (document.carteForm.qualite_id.value=="0")
      var strUrl = "fiche_etatme.php?";
    else
      if (document.carteForm.subqualite_id.value=="0")
        var strUrl = "fiche_etatmequal.php?qualite_id="+document.carteForm.qualite_id.value;
      else {          
        //var strUrl = "fiche_etatmequal.php?qualite_id="+document.carteForm.qualite_id.value+"&subqualite_id="+document.carteForm.subqualite_id.value;
        var strUrl = "fiche_etatmequal.php?qualite_id="+document.carteForm.qualite_id.value;
        //iHauteur = 300;
      }
    
    OpenIframe(ALK_SIALKE_URL+"scripts/site/"+strUrl+"&code="+feature.attributes.code_me+"&idSession="+iSession, iHauteur, iLarg, "MasseEauPopup", "");
  }else{//cas r�seau
    //Parcours des reseaux affich�s
    var nbCheckBox = document.carteForm.reseau_check_id.value;
    var strListReseau = "";
    for (var i=0;i<nbCheckBox;i++) {
      var checkBoxReseau = document.carteForm.elements["reseau_check_id_"+i];
      if(checkBoxReseau.checked){
        strListReseau+=checkBoxReseau.value+";";
      }
    }
   
    OpenIframe(ALK_SIALKE_URL+"scripts/site/point_fiche.php?reseau="+feature.attributes.reseau_id+"&id="+feature.attributes.point_id+
      "&strListReseau="+strListReseau+"&idSession="+iSession, 700, 850, "FichePopup", "");
  }
  
}

function showPoup(evt){
  document.body.style.cursor='hand';
  if(document.body.style.cursor!='hand')
    document.body.style.cursor='pointer';
  selectedFeature = evt.feature;
  if(typeof(selectedFeature.attributes.code_me)!="undefined"){
    strIB = "<font color='#ffffff'><b>"+selectedFeature.attributes.code_me+"</b><br>"+
            "<b>"+selectedFeature.attributes.nom_masse_eau+"</b><br></font>";
    var popup = new OpenLayers.Popup.AnchoredBubble("IB", 
        new OpenLayers.LonLat(selectedFeature.geometry.getBounds().left, selectedFeature.geometry.getBounds().bottom),// selectedFeature.geometry.getBounds().getCenterLonLat(),
        new OpenLayers.Size(160, 45),
        strIB,
        null, false, null);
    popup.setBackgroundColor("#000000");
    popup.setOpacity("0.8");
    
    //popup.updateSize();
  //popup.maxSize = new OpenLayers.Size(750, 660),
    selectedFeature.popup = popup;
    map.addPopup(popup);
  } else {
    strIB = "<font color='#ffffff'><b>"+selectedFeature.attributes.point_id+"</b><br>"+
            "<b>"+selectedFeature.attributes.point_nom+"</b><br></font>";
    var popup = new OpenLayers.Popup.AnchoredBubble("IB2", 
    selectedFeature.geometry.getBounds().getCenterLonLat(),
    new OpenLayers.Size(160, 45),
    strIB,
    null, false, null);
    popup.setBackgroundColor("#000000");
    popup.setOpacity("0.8");
    
    //popup.updateSize();
    //popup.maxSize = new OpenLayers.Size(750, 660),
    selectedFeature.popup = popup;
    map.addPopup(popup);
  }
    console.log(selectedFeature);
}

function onPopupClose(evt) {
  if(selectedFeature.popup!=null){
    map.removePopup(selectedFeature.popup);
    selectedFeature.popup.destroy();
    selectedFeature.popup = null;
    document.body.style.cursor='auto';
    //highlightCtrl.unselect(selectedFeature);
  }
}

/**
 * add layer visibility
 * @param layerValue new visible Layer name
 * @return
 */
function addLayer(layerChecked, layerValue, bReinit){
  if(bReinit)
    document.carteForm.reseau_id.value=0;
  var layerToShow = map.getLayersByName(layerValue);
  layerToShow[0].setVisibility(layerChecked);
}

/**
 * add layer visibility
 * @param layerValue new visible Layer name
 * @return
 */
function addMasseEauLayer(layerChecked, layerValue, nameDivHide){
	//qque soit la case coch�e, on cache le layer d'�tat des masses d'eau 
	//et on affiche le decoupage des masses d'eau
	var nbCheckBox = document.carteForm.masse_eau_check_id.value;
    
	if(layerChecked==true){
		var layerToHide = map.getLayersByName("masse_eau");
		layerToHide[0].setVisibility(false);
		var layerToShow = map.getLayersByName("CLASSEMENT_NONE");
        layerToShow[0].setVisibility(true);   
        for (var i=0;i<nbCheckBox;i++) {
          var checkBoxClassement = document.carteForm.elements["masse_eau_check_id_"+i];
          if(checkBoxClassement.value =="NONE" ){
        	  checkBoxClassement.checked = true;
          }
        }
	}
  if(layerValue == "NONE" && layerChecked==true){
	//Choix du decoupage des masses d'eau seul	
    for (var i=0;i<nbCheckBox;i++) {
      var checkBoxClassement = document.carteForm.elements["masse_eau_check_id_"+i];
      if(checkBoxClassement.value !="NONE" ){
        var layerToShow = map.getLayersByName("CLASSEMENT_"+checkBoxClassement.value);
        layerToShow[0].setVisibility(false);  
        checkBoxClassement.checked = false;
      }else {
    	var layerToShow = map.getLayersByName("CLASSEMENT_"+checkBoxClassement.value);
        layerToShow[0].setVisibility(true);          
      }
    }    
  } else{
	if(layerValue == "NONE"){
		//on decoche decoupage des masses d'eau donc on reaffiche la couche des �tats des me et on supprime les �ventuels autres classements coch�s
		var layerToShow = map.getLayersByName("masse_eau");
		layerToShow[0].setVisibility(true);   
		for (var i=0;i<nbCheckBox;i++) {
	      var checkBoxClassement = document.carteForm.elements["masse_eau_check_id_"+i];
	      if(checkBoxClassement.value !="NONE" ){
	        var layerToShow = map.getLayersByName("CLASSEMENT_"+checkBoxClassement.value);
	        layerToShow[0].setVisibility(false);  
	        checkBoxClassement.checked = false;
	      }
		}		
	}
    var layerToShow = map.getLayersByName("CLASSEMENT_"+layerValue);
    layerToShow[0].setVisibility(layerChecked);
    var oDiv = document.getElementById(nameDivHide);
    if (oDiv) {
      //to hide if not checked, to show if checked
      var oStyle = ( oDiv.style ? oDiv.style : oDiv );       
      if (layerChecked==true)
    	  oStyle.display = "block";
      else 
    	  oStyle.display = "none";      
    }
    //document.carteForm.elements["masse_eau_check_id_0"].checked = false;
  }
}

/**
 * change layer visibility
 * @param layerValue new visible Layer name
 * @return
 */
function changeLayer(layerValue){
  var nbCheckBox = document.carteForm.reseau_check_id.value;
  for (var i=0;i<nbCheckBox;i++) {
    var checkBoxReseau = document.carteForm.elements["reseau_check_id_"+i];
    if(checkBoxReseau.value == layerValue){
      checkBoxReseau.checked = true;
      addLayer(true, layerValue, false);
    }else{
      checkBoxReseau.checked = false;
      addLayer(false, checkBoxReseau.value, false);
    }
  }
    
}
/**
 * 
 * @brief recharge le layer masse_eau
 * @return
 */
function reloadLayer(){
  layerToRedraw = map.getLayersByName("masse_eau");
  layerToRedraw[0].redraw();
}

/**
 * Initialisation de la carte
 * @param prefixMap LB ou AG
 * @param xmin
 * @param ymin
 * @param xmax
 * @param ymax
 * @param strUrlWMS url du flux wms 
 * @param strLayersWMS nom des layers WMS � afficher
 * @param strProjection nom de la projection de la carte et des couches EPSG:27582
 * @param strAlert message � afficher sur la carte
 * @return
 */
function initMap(prefixMap, xmin, ymin, xmax, ymax, strUrlWMS, strLayersWMS, strProjection,  strAlert, repArchive, iRandom, iParamSession) {

   iSession = iParamSession;  
   var options = {
        projection: new OpenLayers.Projection(strProjection),
        units: "m",
       // maxResolution: 156543.0339,
        maxExtent: new OpenLayers.Bounds(xmin, ymin, xmax, ymax),
        maxScale: 30000,
        fractionalZoom : true,
        minScale: 3000000,
        controls : []
    };
    map = new OpenLayers.Map('carte', options);

    var scalebar = new OpenLayers.Control.ScaleLine();
    scalebar.displayClass = 'scaleline olControlScaleLine';
    map.addControl(scalebar);
    
    //map.addControl(new OpenLayers.Control.LayerSwitcher());
    panZoom = new OpenLayers.Control.PanZoom(); 
    map.addControl(panZoom);
    panZoom.moveTo(new OpenLayers.Pixel(296, 4));
    map.addControl(new OpenLayers.Control.Navigation());

    var strSrc = 'Sources : IGN, SHOM, Ifremer';
    if (prefixMap == 'MAY')
      strSrc = 'Sources : IGN, Gebco, NASA, Ifremer';
    
    var backLayer = new OpenLayers.Layer.WMS(
        "OpenLayers WMS",
        strUrlWMS+"?",
        {layers: strLayersWMS, srs: strProjection, format: 'image/png; mode=24bit'},
        {singleTile: true, 'attribution': '<div style="background:#FFFFFF; float:left" id="divlegendCarte" >'+strAlert+
          '<div style="margin-top:10px;font-weight:bold;text-align:right;">'+strSrc+'</div></div><div id="north" style="width:50px; float:right; text-align:right;" ><img style="width: 30px; height: 50px; " src="'+ALK_SIALKE_URL+'media/imgs/gen/icon_north.jpg"></div>'}
    );
      
    //style de masses d'eau, d�pend des �l�ments afficher et du contenu attributaire du gml
    var styleEtatMasseEau = new OpenLayers.Style(
        {
          strokeColor: "grey",
          strokeWidth: 0.5,
          strokeOpacity: 1,
          fillOpacity: 0.8,
          fillColor: "${getEtat}",
          label : "${getIndice}",
          fontColor: "#000000",
          fontSize: "12px",
          fontFamily: "Courier New, monospace",
          fontWeight: "bold"
        }
        ,
        {
          context: {
            getIndice : function(feature){     
             var iIc = "";
        	  //cas Indice de confiance
            if (document.carteForm.qualite_id.value==0){// aucun element de qualit� s�lectionn�
              if(document.carteForm.etat_masse_eau[0].checked) {
                /* DMI 09052016 pour mettre en phase cartes et fiches : le calcul pour l'indice de confiance est d�port�e dans alkmasseeau.php                 
                 
                //iIc = feature.attributes.ETAT_ECOLOGIQUE_IC;
            		var tabElementQualiteDuBassin = new Array();
            		var select = document.getElementById("qualite_id");
            		var optgr=select.getElementsByTagName('optgroup');
            		for (index in optgr) {
            			option = optgr[index];
            			if(option.label == "Etat biologique"){// recup des qualite_id de type �cologique
            				tabOption = option.getElementsByTagName('option');
            				for (index2 in tabOption) {
            					if(tabOption[index2].value)
            						tabElementQualiteDuBassin.push(tabOption[index2].value);
            				}
            			}
            		}
             		//faire une boucle pour tous les elements de qualit� �cologique (qualite_id) du bassin courant 
             		// et on garde l'ic le + faible
            		var IcMin=10;
             			for(i=0; i<tabElementQualiteDuBassin.length; i++ ){
             				iIconf = eval("feature.attributes.QUALITE_"+tabElementQualiteDuBassin[i]+"_IC");
             				if(iIconf && IcMin > iIconf){
             					IcMin = iIconf;
             				}
             			}
              	if(IcMin==10){
              		IcMin=null;
              	}
              	iIc = IcMin;
              	*/
              	iIc = feature.attributes.ETAT_ECOLOGIQUE_IC;
              }
              else if(document.carteForm.etat_masse_eau[1].checked) {
                iIc = feature.attributes.ETAT_CHIMIQUE_IC;
              }else{
                iIc = feature.attributes.ETAT_GENERAL_IC;
              }
            } else{ //cas El�ment de qualit�
               if(document.carteForm.subqualite_id.value==0){
                 iIc = eval("feature.attributes.QUALITE_"+document.carteForm.qualite_id.value+"_IC");
              }else{
                 iIc = eval("feature.attributes.QUALITE_"+document.carteForm.subqualite_id.value+"_IC");
               }
            }  
            return (iIc == null || iIc == 0) ? '' : iIc;
        	},
        	getEtat: function(feature) {
              //cas Classement d'�tat
              if (document.carteForm.qualite_id.value==0){
                if(document.carteForm.etat_masse_eau[0].checked) {
                  return feature.attributes.ETAT_ECOLOGIQUE;
                }
                else if(document.carteForm.etat_masse_eau[1].checked) {
                  return feature.attributes.ETAT_CHIMIQUE;
                }else{
                  return feature.attributes.ETAT_GENERAL;
                }
              } else{ //cas El�ment de qualit�
                 if(document.carteForm.subqualite_id.value==0)
                	 return eval("feature.attributes.QUALITE_"+document.carteForm.qualite_id.value);
                 else
                	 return eval("feature.attributes.QUALITE_"+document.carteForm.subqualite_id.value);
              }
            }
          }
        }
    );
    var styleMapEtatMasseEau = new OpenLayers.StyleMap({"default" : styleEtatMasseEau
    ,    "select" : {
      fillColor: "grey"
      }
      }
    );
        
    var masseEauLayer =  new OpenLayers.Layer.GML("masse_eau", ALK_SIALKE_URL+"upload/carte/"+prefixMap+repArchive+"/"+prefixMap+"_masse_eau_etat.gml?"+iRandom, 
        {styleMap: styleMapEtatMasseEau}
         );
    
    var stylesPoint = new OpenLayers.StyleMap({
      "default": {
        graphicName: "${graphic}",
        backgroundGraphic : ALK_SIALKE_URL+"upload/carte/${backgroundGraphic}",
        backgroundHeight : 35, 
        backgroundWidth : 35,
        graphicZIndex: 100,
        backgroundGraphicZIndex:10,
        pointRadius: "${taille}",
        strokeWidth: 0.5,
        strokeColor: "black",
        fillColor: "${color}",
        fillOpacity: 0.95
      },
      "select": {
        pointRadius: "${taille}",
        fillOpacity: 1
      }
    });
    
    var tabLayers = new Array();
    var tabMasseEauLayer = new Array();
    tabMasseEauLayer.push(masseEauLayer);
    
    //Point Layers
    var reseau = document.carteForm.reseau_id;
    for(var i=0; i<reseau.options.length; i++){
      if (reseau.options[i].value!="0"){
        var pointLayer =  new OpenLayers.Layer.GML(
            reseau.options[i].value,
            ALK_SIALKE_URL+"upload/carte/"+prefixMap+repArchive+"/"+prefixMap+"_reseau"+reseau.options[i].value+".gml",
            {projection: new OpenLayers.Projection("EPSG:4326"), visibility:false, styleMap : stylesPoint, rendererOptions: {zIndexing: true}}            
        );
        tabLayers.push(pointLayer);
      }
    }
    
    
    var stylesMasse = new OpenLayers.StyleMap({
      "default": {
        strokeWidth: 0.5,
        strokeColor: "black",
        fillColor: "${CLASSEMENT}",
        fillOpacity: 0.95
      },
      "select": {
        pointRadius: 12,
        fillOpacity: 1,
        fillColor: "blue"
      }
    });
    //Masse d'eau Layers
    var nbCheckBox = document.carteForm.masse_eau_check_id.value;
    for (var i=0;i<nbCheckBox;i++) {
      var checkBoxClassement = document.carteForm.elements["masse_eau_check_id_"+i];
      var checkLayer =  new OpenLayers.Layer.GML(
          "CLASSEMENT_"+checkBoxClassement.value,
          ALK_SIALKE_URL+"upload/carte/"+prefixMap+repArchive+"/"+prefixMap+"_masse_eau_classement_"+checkBoxClassement.value+".gml",
          {visibility:false, styleMap : stylesMasse}
      );
      tabMasseEauLayer.push(checkLayer);
    }
    map.addLayer(backLayer);
    map.addLayers(tabMasseEauLayer);
    map.addLayers(tabLayers);
   
    
    var vectors = tabLayers.concat(tabMasseEauLayer);
    
    selectControl = new OpenLayers.Control.SelectFeature(
        vectors,
        {
          onSelect: onFeatureSelect, 
          clickout: true, 
          toggle: true
    });
    
    highlightCtrl = new OpenLayers.Control.SelectFeature(
        vectors, {
        hover: true,
        highlightOnly: true,
        renderIntent: "temporary",
        eventListeners: {
            beforefeaturehighlighted: function(){},
            featurehighlighted: showPoup,
            featureunhighlighted: onPopupClose
        }
    });

    map.addControl(highlightCtrl);
    map.addControl(selectControl);
    map.addControl(new OpenLayers.Control.Attribution());

    map.zoomToMaxExtent();
    highlightCtrl.activate();
    selectControl.activate();
    
    
}

/** M�morise le niveau de la derni�re popup ouverte, =0 pas de popup ouverte */
var iLevelAlkPopup = 0;
/**
 * @brief Retourne la r�f�rence de la fen�tre popup de niveau iLevel
 * @param iLevel   Niveau de la popup
 * @return Retourne la r�f�rence sur la fen�tre de niveau iLevel
 */
function GetWindowByNiv(iLevel)
{
  if( iLevel <=0 ) {
    return window;
  } 
  return top.window.document.getElementById("alkIFrameNiv"+iLevel.toString()).contentWindow;
}

/**
 * @brief Cr�ation d'une popup de niveau iLevel
 * @param iLevel  Niveau de la popup =1 pour la premi�re
 * @return Retourne une r�f�rence sur le layer caract�risant la popup
 */
function addPopup(iLevel)
{
  var oWind = top.window;
  var oPopup = oWind.document.createElement("div");
  if( oPopup ) {
    oPopup.id        = "alkPopupNiv"+iLevel;
    oPopup.className = "windPopupNiv"+iLevel;
    oPopup.iAlkLevel = iLevel;
    var oStyle = ( oPopup.style ? oPopup.style : oPopup );
    oStyle.zIndex = 20000+(iLevel*2);
    oWind.document.body.appendChild(oPopup);
    return oPopup;
  }
  return null;
}

/**
 * @brief Ferme la derni�re popup ouverte
 * @param strUrlToken  url de r�affichage de l'opener. 
 *                     si la chaine ne contient pas .php, le param�tre est consid�r� comme un token
 *                     sur l'url alkanet.php
 *                     si la chaine = reload, un reload est effectu� sur l'opener
 */
function removePopup(strUrlToken)
{
  var bClose = false;
  var iLevel = top.iLevelAlkPopup;
  var oOpenerIFrame = null;
  var iLevelNext = 0;
  var oPopup = top.document.getElementById("alkPopupNiv"+iLevel.toString());
  if( oPopup ) {
    // reaffiche l'opener si le tokenClose ou l'url sont fournis
    if( typeof(strUrlToken)!="undefined" && strUrlToken != "" ) {
      var iLevelOpener = iLevel-1;
      oOpenerIFrame = top.window;
      if( iLevelOpener >= 1 ) {
        oOpenerIFrame = top.document.getElementById("alkIFrameNiv"+iLevelOpener.toString());
      }

      // le param�tre &r=1 est pr�sent pour �viter l'utilisation du cache navigateur
      if( strUrlToken == "reload" && oOpenerIFrame==top.window ) {
        oOpenerIFrame.location.reload();
      } else {
        var strUrl = ( strUrlToken.indexOf(".php")>-1 || strUrlToken.indexOf(".phtml")>-1 
                       ? strUrlToken.replace(/(\?|&)r=1/g, '')+(strUrlToken.indexOf("?")==-1 ? "?" : "&")+"r=1"
                       : "alkanet.php?token="+strUrlToken+"&r=1" );
        if( oOpenerIFrame==top.window ) {
          oOpenerIFrame.location = strUrl;
        } else {
          oOpenerIFrame.src = strUrl;
        }
      }
    }
    
    top.iLevelAlkPopup = top.iLevelAlkPopup - 1;
    iLevelNext = top.iLevelAlkPopup;
    if (oPopup.popupFrame){
      oPopup.popupFrame.parentNode.removeChild(oPopup.popupFrame);
    }
    if ( iLevel==1 && oPopup.popupMsg ) {
      oPopup.popupMsg.parentNode.removeChild(oPopup.popupMsg);
    }
    oPopup.parentNode.removeChild(oPopup);
    delete(oPopup);
    bClose = true;
  }
  return bClose;
}

/**
 * @brief Ferme toute les popup ouvertes
 */
function removeAllPopup()
{
  var bClose = false;
  for(iLevel=1; iLevel<=top.iLevelAlkPopup; iLevel++) {
    var oOpenerIFrame = null;
    var oPopup = top.document.getElementById("alkPopupNiv"+iLevel.toString());
    if( oPopup ) {
      if( oPopup.popupFrame ) {
        oPopup.popupFrame.parentNode.removeChild(oPopup.popupFrame);
      }
      if( iLevel==1 && oPopup.popupMsg ) {
        oPopup.popupMsg.parentNode.removeChild(oPopup.popupMsg);
      }
      oPopup.parentNode.removeChild(oPopup);
      delete(oPopup);
      bClose = true;
    }
  }
  top.iLevelAlkPopup = 0;      
  
  return bClose;
}


/**
 * @brief ferme une fen�tre popup
 * @param strUrlToken  url de r�affichage de l'opener. 
 *                     si la chaine ne contient pas .php, le param�tre est consid�r� comme un token
 *                     sur l'url alkanet.php
 */
function closeWindow(strUrlToken) 
{ 
  selectControl.unselect(selectedFeature);
  return removePopup(strUrlToken);
  
}

/**
 * @brief ferme toutes les fen�tres popup ouverte
 * @param strUrlToken  url de r�affichage de l'opener. 
 *                     si la chaine ne contient pas .php, le param�tre est consid�r� comme un token
 *                     sur l'url alkanet.php
 */
function closeAllWindow(strUrlToken) 
{ 
  if( typeof(strUrlToken)!="undefined" && strUrlToken != "" ) {
    var oOpenerIFrame = top.window;
    if( strUrlToken == "reload" ) {
      oOpenerIFrame.location.reload();
    } else {
      var strUrl = ( strUrlToken.indexOf(".php")>-1 
                     ? strUrlToken+(strUrlToken.substr(-4)==".php" ? "?" : "&")+"r=1"
                     : "alkanet.php?token="+strUrlToken+"&r=1" );
      oOpenerIFrame.location = strUrl;  
    }
    return true;
  }
  return removeAllPopup();
}

function OpenFooterIframe(strUrl, iHeight, iWidth, strName, strFormName)
{
  if(strFormName && eval("document."+strFormName)){
    var f = eval("document."+strFormName);
    f.target = strName;
    f.action = strUrl;
    f.submit();
  }else{
    eval(strName+".location ='"+ strUrl+"'");
  }
}
/**
 * @brief Simule l'ouverture d'une fen�re popup dans un layer et iframe combin�s
 * @param strUrl  url de iframe
 * @param iHeight hauteur de l'iframe
 * @param iWidth  largeur de l'iframe
 * @param strName Nom de l'iFrame
 * @return Une r�f�rence sur l'iframe ouverte
 */
function OpenIframe(strUrl, iHeight, iWidth, strName, strFormName)
{
  if( top.iLevelAlkPopup > 4 ) {
    alert("Conception � revoir. Trop de popup.");
    return null;
  }
  
  var oWind = top;
  var oPopup = addPopup(top.iLevelAlkPopup+1);
  if( oPopup ) {
    top.iLevelAlkPopup = top.iLevelAlkPopup + 1;
    
    var windowWidth = (top.window.innerWidth ? top.window.innerWidth : top.window.document.body.clientWidth);
    var windowHeight = (top.window.innerHeight ? top.window.innerHeight : top.window.document.body.clientHeight);

    var iLeft = Math.max(0, Math.floor((windowWidth - iWidth ) / 2));
    var iTop  = Math.max(0, Math.floor((windowHeight - iHeight ) / 2));
    
    var iWidth2 = 150, iHeight2 = 12;
    var iLeft2 = Math.floor((windowWidth - iWidth2 ) / 2);
    var iTop2  = Math.floor((windowHeight - iHeight2 ) / 2);

    var oIFrame = top.document.createElement("iframe");
    oIFrame.id = "alkIFrameNiv"+oPopup.iAlkLevel.toString();
    oIFrame.name = ( !strName ? oIFrame.id : strName );

    var oOpener = window;//GetWindowByNiv(top.iLevelAlkPopup-1);
    AddWindOpener(oIFrame.name, oOpener);

    oIFrame.className = oPopup.className;
    var oStyle = getStyleObj(oIFrame);
    oStyle.width  = iWidth+"px";
    oStyle.height = iHeight+"px";
    
    var oPopup2 = createAbsoluteDiv("alkPopupFrameNiv"+top.iLevelAlkPopup, 
                                    iWidth, iHeight, iTop, iLeft, 20001+(top.iLevelAlkPopup*2));
    if ( !oPopup2 ) return;
    var oStyle = getStyleObj(oPopup2);
    oStyle.display = "block";
    
    if ( oWind.document.getElementById("alkPopupMsg") ){
      oPopup3 = oWind.document.getElementById("alkPopupMsg");
      var oStyle = getStyleObj(oPopup3);
      oStyle.display = "block";
    }
    else {
      var oPopup3 = createAbsoluteDiv("alkPopupMsg", iWidth2, iHeight2, iTop2, iLeft2, 30000);
      if ( !oPopup3 ) return;
      oPopup3.className = "windPopupMsg";
      oPopup3.innerHTML = "Chargement en cours...";
    }
    oIFrame.popupMsg = oPopup3;
    oIFrame.popupFrame = oPopup2;
    oIFrame.popupBackground = oPopup;
    oIFrame.iLevel = top.iLevelAlkPopup;
    
    oPopup.popupMsg = oPopup3;
    oPopup.popupFrame = oPopup2;
    
    oWind.document.body.appendChild(oPopup2);
    oPopup2.appendChild(oIFrame);
    if(strFormName && eval("document."+strFormName)){
      var f = eval("document."+strFormName);
      f.target = oIFrame.name;
      f.action = strUrl;
      f.submit();
    }else{
      oIFrame.src = strUrl;
    }
    oWind.document.body.appendChild(oPopup3);
    oIFrame.contentWindow.name = oIFrame.name;
    
  }
  return oIFrame.contentWindow;
}

function HideLoadMsg()
{
  if ( top.document.getElementById("alkPopupMsg") ){
    var oMsg = top.document.getElementById("alkPopupMsg");
    var oStyle = getStyleObj(oMsg);
    oStyle.display = "none";
  }/*
  if ( typeof popupFrame != "undefined" ){
    var oStyle = getStyleObj( popupFrame );
    oStyle.display = "block";
  }*/
}

function createAbsoluteDiv(id, iWidth, iHeight, iTop, iLeft, zIndex){
  var oWind = top;
  var oPopup = oWind.document.createElement("div");
  oPopup.id = id;
  if( oPopup ) {
    var oStyle = getStyleObj(oPopup);
    oStyle.position = "absolute";
    oStyle.width  = iWidth+"px";
    oStyle.height = iHeight+"px";
    oStyle.top    = iTop+"px";
    oStyle.left   = iLeft+"px";
    oStyle.zIndex = zIndex;
  }
  return oPopup;
}

function getStyleObj(oObj)
{
  return ( oObj.style ? oObj.style : oObj );
}

/**
 * @brief M�morise l'opener pour une fen�tre
 * @param strName  Nom de la fen�tre
 * @param oOpener  R�f�rence sur sa fen�tre opener
 */
function AddWindOpener(strName, oOpener)
{
  if( typeof(top.navigator.AlkOpener) == "undefined" || top.navigator.AlkOpener.constructor.toString().indexOf("Array()") < 0 ) {
    top.navigator.AlkOpener = new Array();
  }
  top.navigator.AlkOpener[strName] = oOpener;
}

/**
 * @brief Retourne la r�f�rence sur la fen�tre ouvreuse de la popup courante
 * @return r�f�rence sur objet de type window
 */
function GetWindowOpener()
{
  var strName = ( window.name == "" ? "_top" : window.name );
  var oOpener = null;
  if( typeof(top.navigator.AlkOpener[strName]) != "undefined" ) {
    oOpener = top.navigator.AlkOpener[strName];
  }
  return oOpener;
}

function majScreenImpression(map, session_id, mode){

//  var oCtrl = document.getElementById("btImpression");
 if(mode == -1 ){// retour   
  var oAjax = new AlkAjax('majScreenImpr', 'fnMajScreenNotImpr', "GET", ALK_SIALKE_URL+'scripts/site/carte.php?map='+map+'&idSession='+session_id, null);   
 }else{
  var oAjax = new AlkAjax('majScreenImpr', 'fnMajScreenImpr', "GET", ALK_SIALKE_URL+'scripts/site/carte.php?map='+map+'&idSession='+session_id, null);
 }
}

function fnMajScreenNotImpr(strHTML)
{
  if(strHTML!=""){
    document.location.reload();
  }
}

function fnMajScreenImpr(strHTML)
{
  
  if(strHTML!=""){
    var Htmlmenu = document.getElementById("menu_carte");    
    Htmlmenu.id = "menu_carte_impression";

    var Htmlcarte = document.getElementById("carte");    
    Htmlcarte.id = "carte_impression";
    // MODIF DE LA CLASS classDivCarte
    var HtmlDivCarte = document.getElementById("divCarte");  
    HtmlDivCarte.className = "classDivCarteImp";
    // dep des deux balise dans menu_carte_impression
    var HtmlDivCarte1 = document.getElementById("divCarte1");  
    var HtmlDivCarte2 = document.getElementById("divCarte2");  
    
    // type de classement
    var tabClassement = document.getElementsByName("etat_masse_eau");
    for(var i=0; i<tabClassement.length;i++){
        if(tabClassement[i].checked){
          var idClassement = i;
        }
    }
    
    var labelTxt = '';
    var HtmlTypeClassement = document.getElementById("etat_masse_eau_"+idClassement);
    
   // recup du label du type de classement
    var labels=document.getElementsByTagName("label"),i;
    for( i=0; i<labels.length;i++ ){
     if(HtmlTypeClassement && labels[i].htmlFor==HtmlTypeClassement.id){
      var labelTxt = labels[i].innerHTML;
      }
    }    
    
    // element de qualite
    var txtQualite = '';
    var tabQualite = document.getElementById("qualite_id");
    if (tabQualite && tabQualite.selectedIndex>0){
      txtQualite = tabQualite.options[tabQualite.selectedIndex].text;
      labelTxt = '';
    }
    // element de ss qualite   
    var tabSSQualite = document.getElementById("subqualite_id");
    var txtssQualite = '';
    if (tabSSQualite && tabSSQualite.selectedIndex>0){
      txtssQualite = tabSSQualite.options[tabSSQualite.selectedIndex].text; 
      labelTxt = '';  
    }
    // bandeau titre 
    var Htmltitre_carte = document.getElementById("titre_carte");    

    Htmltitre_carte.innerHTML += "<div id='divCarte2' class='txt classDivCarte2'><p class='classDivCarte3 txtLabelCarte'></p><p>"+
                                  HtmlDivCarte2.innerHTML+"<p></p></div>";
    
    Htmltitre_carte.innerHTML += "<div id='divCarte3' class='txt classDivCarte1'>"+
                           "<h2 class='txtLabelCarte'>"+HtmlDivCarte1.innerHTML+"</h2>"+
                           "</div>";
                                        
    Htmltitre_carte.innerHTML += (labelTxt ? "<div id='divCarte4' class='txt classDivCarte1'><h2>Type de classement&nbsp;:&nbsp;"+
                                  labelTxt+"</h2></div>" : "");
             
    // affichage qualite
    Htmltitre_carte.innerHTML += (txtQualite != '' ? "<div id='divCarte4' class='txt classDivCarte1'><h2>Element de qualit�&nbsp;:&nbsp;"+
                                  txtQualite+(txtssQualite != '' ? "<br/>Sous-�l�ment de qualit�&nbsp;:&nbsp;"+txtssQualite : "")+"</h2></div>" : "");
        
    // bt imp de retour
    var HtmltbtImp = document.getElementById("btImpression");
    var href = HtmltbtImp.href;
    var elem = href.split('\'');
    var href = "javascript:majScreenImpression('"+elem[1]+"', '"+elem[3]+"', -1)";
    HtmlDivCarte1.innerHTML = '<a id="btImpression" href="'+href+'">Retour</a>';
    
    HtmltbtImp.style.display = "none";   
    HtmlDivCarte2.style.display = "none";     
        
    var Htmllogos = document.getElementById("logos");    
    var currentTime = new Date();
    var month = currentTime.getMonth()+1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();    
    if (parseInt(month) <= 9){month = "0" + month};
    if (parseInt(day) <= 9){day = "0" + day};
    var dt = (day + "/" + month + "/" + year);
    Htmllogos.innerHTML = "<p style='text-align:center;font-weight:bold'>Edit�e le "+dt+"</p>"+Htmllogos.innerHTML;    
        
    var trClassement = document.getElementById("trClassement");
    trClassement.innerHTML = "";
    
    // on masque les div
    var divQualite = document.getElementById("divQualite");
    divQualite.innerHTML = "";
    var divSubqualite = document.getElementById("divSubqualite");
    divSubqualite.innerHTML = "";        
    
    // suppression du table de navigation    
    var tabNavig = document.getElementById("OpenLayers.Control.PanZoom_5");
    tabNavig.style.display = "none";
    
    // creation d'une colonne � droite de la carte :   
    // creation du ocntenu
    // lecture des case
      // var reseau
    var tabInputs =document.getElementsByTagName("input");
    var tabInputSelected = new Array();
    var tabIdDesInput = new Array(); // on recup les id
 
    var k=0;
    for(var i=0; i<tabInputs.length;i++){     
        var input = tabInputs[i];
        var patt=/^reseau_check_id_\d*/g;
        if( patt.test(input.id) ){
          if ( input.checked ) {
           tabInputSelected[k] = input.parentElement.parentElement;
           var tabelement = input.id.split('reseau_check_id_');
           tabIdDesInput[k] = tabelement[1];
           k++;   
          }
        }
    }

    // var ME
    var tabInputs2 =document.getElementsByTagName("input");
    var tabInputSelected2 = new Array();
    var tabIdDesInput2 = new Array(); // on recup les id
    var k2=0;
    for(var i=0; i<tabInputs2.length;i++){     
        var input = tabInputs2[i];
        var patt2=/^masse_eau_check_id_\d*/g;
        if( patt2.test(input.id) ){
          console.log(input, input.checked);
          if ( input.checked ) {           
           tabInputSelected2.push(input.parentElement.parentElement);
           var tabelement2 = input.id.split('masse_eau_check_id_');
           tabIdDesInput2[k2] = tabelement2[1];
           k2++;   
          }
        }
    }

    //console.log(tabIdDesInput);   
    // contenu col droite    
    var HtmlForm = document.getElementById("carteForm");
        
    var titre1 = "<tr><td colspan='2'><h3>Contr�le de surveillance</h3></td></tr>";
    var titre2 = "<tr><td colspan='2'><h3>Contr�le d'enqu�te</h3></td></tr>";
    var titre3 = "<tr><td colspan='2'><h3>Autres suivis</h3></td></tr>";
    var bTitre1 = false;
    var bTitre2 = false;
    var bTitre3 = false;
    
    
    if(tabInputSelected.length>0 || tabInputSelected2.length>0 ){    
    
      // diminution taille de l'�cran

      var imgCarte = document.getElementById("carte_impression");

      var percent = (imgCarte.offsetWidth  - 280 - 280) / (imgCarte.offsetWidth -280);  
      var calLeft = 280-(280*percent);
      
      //var largmap = (imgCarte.offsetWidth - 280 - 280);   
      imgCarte.style.cssText="z-index:-3;  height:100%; position: absolute; top: 0px; ; bottom: 0; left: -"+calLeft+"px; transform: scale("+percent+");";
      // creation col droite
      HtmlForm.innerHTML += "<div id='colonneDroite' class='classDivColonneDroite'><div id='colonneDroiteContenu'><table id='tableColDroite'></table></div></div>";
      var HtmlcolDroite = document.getElementById("tableColDroite");
    }
    
    // var reseau
    if(tabInputSelected.length>0){
            HtmlcolDroite.innerHTML += "<tr><td colspan='2'><h2 class='txtLabel'>R�seaux</h2></td></tr>";
    }      
    for(var i=0; i<tabInputSelected.length; i++){
      if(tabIdDesInput[i]<9 && bTitre1 == false){
        HtmlcolDroite.innerHTML += titre1;
        bTitre1 = true;
      } 
      if(tabIdDesInput[i]==10 && bTitre2 == false){
        HtmlcolDroite.innerHTML += titre2;
        bTitre2 = true;
     }    
      if(tabIdDesInput[i]>10 && bTitre3 == false){
        HtmlcolDroite.innerHTML += titre3;
        bTitre3 = true;
      }
      HtmlcolDroite.innerHTML += "<tr><td>"+tabInputSelected[i].innerHTML+"</td></tr>";    
    }
    
    // var me
    if(tabInputSelected2.length>0){
      HtmlcolDroite.innerHTML += "<tr><td colspan='2'><h2 class='txtLabel'>D�coupage des masses d'eau</h2></td></tr>";
    }    
    
    
    
    for(var i=0; i<tabInputSelected2.length; i++){          
      HtmlcolDroite.innerHTML += "<tr><td>"+tabInputSelected2[i].innerHTML+"</td></tr>";    
    console.log(tabInputSelected2[i]);
      if(tabIdDesInput2[i] == 4){
        var legTYPOMET = document.getElementById("legTYPOMET");       
        // on ajoute div id = legTYPOMET
        HtmlcolDroite.innerHTML += "<tr><td>"+legTYPOMET.parentElement.parentElement.innerHTML+"</td></tr>"
      }
       if(tabIdDesInput2[i] == 3){
        var legTYPOMEC = document.getElementById("legTYPOMEC");       
        // on ajoute div id = legTYPOMET
        HtmlcolDroite.innerHTML += "<tr><td>"+legTYPOMEC.parentElement.parentElement.innerHTML+"</td></tr>"
      }
       if(tabIdDesInput2[i] == 2){
        var legTYPOATTOBJ = document.getElementById("legTYPOATTOBJ");       
        // on ajoute div id = legTYPOATTOBJ
        HtmlcolDroite.innerHTML += "<tr><td>"+legTYPOATTOBJ.parentElement.parentElement.innerHTML+"</td></tr>"
      }
    }
   
    // efface les contenus de legende
    var HtmldivDecoupME1 = document.getElementById("divDecoupME1");    
    HtmldivDecoupME1.style.display = "none";
    var HtmldivDecoupME2 = document.getElementById("divDecoupME2");
    HtmldivDecoupME2.style.display = "none";
    var HtmldivReseau1 = document.getElementById("divReseau1");    
    HtmldivReseau1.style.display = "none";
    var HtmldivReseau2 = document.getElementById("divReseau2");   
    HtmldivReseau2.style.display = "none";
    // efface les case � cocher 
    var tabInputs2 =document.getElementsByTagName("input");    
    //console.log(tabInputs2);
    
    for(var i=0; i<tabInputs2.length; i++){      
        tabInputs2[i].style.display = "none";
    }
    
    // slider 
    // modif
    targetElement = document.getElementById('slider_carte');
    targetElement.id = "slider_carte_impression";
    
    window.print();
  }
}




