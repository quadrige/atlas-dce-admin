<?php
include_once("../../classes/pattern/alkrequest.class.php");

 /**
   *  Retourne une r�f�rence sur un objet de type AlkHtml2Pdf
   * @param strUrlSrc          Url complete du fichier html source
   * @param strPathSrc         Chemin complet du fichier html source
   * @param strFileSrc         Nom du fichier html source
   * @param strHtml            Contenu html
   * @param strPathDest        Chemin complet du fichier pdf générer
   * @param strFileDest        Nom du fichier pdf générer
   * @param bDelFileSrc        Non utilisé
   * @param bUseLocalServ      vrai si utilisation du service local, faux utilisation du service html2pdf.alkante.com (par défaut)
   * @return AlkHtml2Pdf
   */
  function getHtml2Pdf($strUrlSrc, $strPathSrc="", $strFilerSrc="", $strHtml="",
                                      $strPathDest="", $strFileDest="", $bDelFileSrc=false, $bUseLocalServ=false)
  {
    include_once(ALK_SIALKE_PATH."classes/html2pdf/alkhtml2pdf.class.php");
    return new AlkHtml2Pdf($strUrlSrc, $strPathSrc, $strFilerSrc, $strHtml,
                           $strPathDest, $strFileDest, $bDelFileSrc, $bUseLocalServ);
  }

function affPDF($strHtml, $page_title)
{
   $strPathDest = ALK_SIALKE_PATH."upload/fiche/";
           
   $strHtml = preg_replace("!(<a)([^>]+)(>)!ie", "'\\1'.stripslashes(str_replace('&', '&amp;', '\\2')).'\\3'", $strHtml);
   $strHtml = preg_replace("!(<img)([^>]+)(>)!ie", "'\\1'.stripslashes(str_replace('&', '&amp;', '\\2')).'\\3'", $strHtml);

   $strHtml = str_replace("src=\"/upload", "src=\"".ALK_ROOT_URL."/upload", $strHtml);

   $tabMaps = array();
   preg_match_all("!layer(\d+)\s*=.*?\"([^\"]+\?map=[^\"]+)\"!usim", $strHtml, $tabMaps, PREG_SET_ORDER);

  foreach ($tabMaps as $tabMapDesc){
    $map_id = $tabMapDesc[1];
    $map_url = $tabMapDesc[2]."&layers=all&mode=map&map_imagetype=png";
    $strHtml = preg_replace("!(<div id=[\"']carte".$map_id."[\"'][^>]+?width:(\d+)px;height:(\d+)px[^>]+?>)!usim", "$1<img src='".$map_url."&map_size=$2+$3'/>", $strHtml);
  }
  // pour �viter les coupures de bas de page
  
  $strHtml = str_replace("<div id='carteFicheMeQual'>", "<div id='carteFicheMeQual' style='page-break-inside: avoid !important;'>"  , $strHtml); 
  $strHtml = str_replace("<div id='legendeEtat'>", "<div id='legendeEtat' style='page-break-inside: avoid !important;'>"  , $strHtml);
  $strHtml = str_replace("<div id='legendeMotif'>", "<div id='legendeMotif' style='page-break-inside: avoid !important;'>"  , $strHtml); 
  $strHtml = str_replace("<div id='legendeIndConfiance'>", "<div id='legendeIndConfiance' style='page-break-inside: avoid !important;'>"  , $strHtml);

  if ( $page_title=="" ){
    $page_title = ALK_APP_TITLE;
  }
  $page_title_file = strtr(utf8_decode($page_title), utf8_decode("éèêëàäâïîöôüûù"), "eeeeaaaiioouuu");
  $page_title_file = preg_replace("!\W!ui", "_", $page_title_file);
  
  $fileName = "alkanet_".session_id()."_".$page_title_file.".html";
    
  file_put_contents($strPathDest.$fileName, $strHtml);
  
  $strUrlSrc = str_replace(ALK_ROOT_PATH, ALK_ROOT_URL, $strPathDest).$fileName; 

  $oPdf = getHtml2Pdf($strUrlSrc, $strPathSrc="", $strFilerSrc="", $strHtml="",
                                    $strPathDest, $page_title_file, true, false);
  $oPdf->setSendSerializedMode(false);
  $oPdf->setExecuteByWkHtml(true);
  
  $oPdf->setModeOutput(Request("output", REQ_GET, "BROWSER"));
  $oPdf->requestParam();
  $oPdf->setParam("pixels", "800");

  echo nl2br($oPdf->getPdf());      
}
?>