<?
include_once("libconf/lib_affmenu.php");

/**
 * @brief Affiche le haut de la page
 *
 * @param tabEventBody  tableau contenant les couples nom de l'�v�nement / nom de la fonction javascript appel�e (pas n�cessaire d'ajouter 'javascript:') 
 */
function aff_menu_haut( $tabEventBody=null )
{
  global $cont_id, $cont_appli_id;
  aff_header();

?>
<script language='javascript' src='<?=ALK_SIALKE_URL?>lib/lib_js.js'></script>
</head>

<?
$strBodyEvent = "";

if (is_array($tabEventBody))
{
  while( list($strEvent, $strFunctionJs) = each($tabEventBody) )
    $strBodyEvent .= " ".$strEvent."=\"javascript:".$strFunctionJs."\"";
}
else
{
  $strBodyEvent = " onload=\"javascript:window.focus();\"";  
}
?>

<body
      <?=$strBodyEvent?>>
   <div id="global">   
<?
}

/**
 * @brief Affiche le bas de la page
 */
function aff_menu_bas()
{
  echo "</div></body></html>";
}



//
// les fonctions ci-dessous sont appel�es � disparaitre
//

/** Affiche le r�sultat de la fonction getHtmlHeaderPage (lib_affmenu.php) */
function aff_page_haut($bModif, $strTitre, $iLarg="606")
{
  echo getHtmlHeaderPage($bModif, $strTitre, $iLarg);
}

/** Affiche le r�sultat de la fonction getHtmlLineFrame (lib_affmenu.php) */
function Aff_page_cadre_separateur($iLarg="606")
{
  echo getHtmlLineFrame($iLarg);
}

/** Affiche le r�sultat de la fonction getHtmlHeaderFrame (lib_affmenu.php) */
function Aff_page_cadre_haut($iLarg="606", $ihaut="5")
{
  echo getHtmlHeaderFrame($iLarg, $ihaut);
}

/** Affiche le r�sultat de la fonction getHtmlFooterFrame (lib_affmenu.php) */
function Aff_page_cadre_bas($ihaut="5")
{
  echo getHtmlFooterFrame($ihaut);
}

/** Affiche le r�sultat de la fonction getHtmlFooterPage (lib_affmenu.php) */
function aff_page_bas()
{
  echo getHtmlFooterPage();
}

?>