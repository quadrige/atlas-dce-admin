<?php

  /**
   * @brief Construit un fichier PDF
   * 
   * @return Retourne le nom du fichier PDF
   */


  function getPdf($strHtml, $idReseau, $idPoint)
  {
    $docPDF = new HTML2FPDF();
    $docPDF->setDefaultFontSize(8);

    $docPDF->setFoot("Edit� le ".date("d/m/Y G:i"));        

    $docPDF->AddPage();
    $docPDF->writeHtml($strHtml, true);

    $strFileNamePDF = "fiche_".$idReseau."_".$idPoint.".pdf";   
    $docPDF->Output(ALK_SIALKE_PATH.ALK_PATH_UPLOAD_FICHE.$strFileNamePDF, 'F');    
    $docPDF->closeParsers();

    return $strFileNamePDF;
  }
  
  function getPdf2($strHtml, $idReseau, $idPoint, $codeMe="") {
    if ($codeMe != "")
  		$strFileNamePDF = "fiche_mequal".$codeMe;
  	else
  		$strFileNamePDF = "fiche_".$idReseau."_".$idPoint;    	
    
    $docPDF = new AlkHtml2Pdf(ALK_ROOT_PDF_URL.ALK_SIALKE_DIR.ALK_PATH_UPLOAD_FICHE, ALK_SIALKE_PATH.ALK_PATH_UPLOAD_FICHE, "", $strHtml, 
  														ALK_SIALKE_PATH.ALK_PATH_UPLOAD_FICHE, $strFileNamePDF, true, true);  																						  														
  	$docPDF->setModeOutput("BROWSER");
    $docPDF->getPdf();
  }
  

?>
