<?
function TronqueItemMenu($strItemMenu)
{
  // effectue des cesures tous les 15 caract�res
  $strItemMenu = wordwrap($strItemMenu, 15, "<br>", 1);

  // coupe la chaine avant le second <br>
  $tabStrMenu = explode("<br>", $strItemMenu);
  $strRes = $tabStrMenu[0];
  if( count($tabStrMenu) >= 2 )
    $strRes .= "<br>".$tabStrMenu[1];
  return $strRes;
}

function TronqueItemSsMenu($strItemMenu)
{
  // effectue des cesures tous les 15 caract�res
  $strItemMenu = wordwrap($strItemMenu, 25, "<br>", 1);

  // coupe la chaine avant le second <br>
  $tabStrMenu = explode("<br>", $strItemMenu);
  $strRes = $tabStrMenu[0];
  if( count($tabStrMenu) >= 2 )
    $strRes .= "<br>".$tabStrMenu[1];
  return $strRes;
}

/// <comment>
/// <summary>
/// Affiche le haut de la page
/// </summary>
/// <params name="tabNav" option>Tableau associatif contenant les liens de navigation</param>
/// <params name="iAdminBouton" option>Longueur en pixels du controle de saisie</param>
/// <params name="tabAppliMenu" option>Tableau associatif</param>
/// </comment>
function aff_menu_haut()
{
  global $cont_id;
  global $oSpace;
  
  $tabAppli = $oSpace->setAppliProperty();
  //------------
  // Constantes
  //------------
  aff_header();

?>
<script language="JavaScript">
function SelectAndPrint()
{
  javascript:window.focus();
  if( window.print )
    window.print();
  else
    alert("Utilisez l'impression du navigateur pour imprimer cette page.");
}
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" background="" 
      leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="SelectAndPrint()">

<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width="606">
    <img src="<?=ALK_URL_SI_MEDIA ?>transp.gif" border="0" width="606" height="5">
    </td>
  </tr>
  <tr>
    <td valign=top>
<?

}


///<comment>
///<summary>
/// Retourne la liste des liens a afficher. Classes de style utilisees : menuPath, menuPathBD
///</summary>
///<params name="tab" option>Tableau associatif contenant les liens-texte de navigation</param>
///<params name="cont_id">identifiant dy conteneur courant</param>
///<returns>Retourne une chaine HTML contenant les liens de navigation</returns>
///</comment>
function getLienNav($tab, $cont_id)
{
  global $conn;
  global $cont_appli_id; 
  global $_sit_urlHomePage;

  $strNav = "";
  $strSepar = " / ";
  
  // ajoute les espaces successifs
  if( $cont_id==1 || $cont_id==0 )
    //$strNav="<span class=divContenuTexteGras>SOMMAIRE".S_SIT."</span>";
    $strNav="<span class=divContenuTexteGras>SOMMAIRE</span>";
  else 
    {  
      $strSql = "select c.cont_id, c.cont_intitule, c.cont_niveau, c.cont_appli_defaut, at.atype_url,".
        "decode(a.appli_id, NULL, 0, 1) defaut, decode(ac.agent_id, null, 0, 1) droit".
        " from sit_appli a, sit_appli_type at, sit_agent_cont ac, ".
        "(select cont_id, cont_intitule, cont_niveau, cont_appli_defaut ".
        " from sit_conteneur ".
        " start with cont_id=".$cont_id.
        "  connect by prior cont_pere=cont_id".
        " ) c ".
        "where ".
        " ac.agent_id=".$_SESSION["sit_idUser"]." and c.cont_id=ac.cont_id(+) and ".
        " a.atype_id=at.atype_id(+) and c.cont_appli_defaut=a.appli_id(+) and c.cont_niveau>1".
        " order by c.cont_niveau";
      $rsCont = initrecordset($strSql, $conn);
      while( OCIFetch($rsCont) ) 
        {
          $iDroit = ociresult($rsCont, "DROIT");
          $icont_id = ociresult($rsCont, "CONT_ID");
          $icont_intitule = ociresult($rsCont, "CONT_INTITULE");
        
          if( $iDroit == 1 )
            {
              $strNav .= "<span class=\"divContenuTexteGras\">".$icont_intitule."</span>";   
              if( $cont_id != $icont_id ) 
                $strNav .= $strSepar;
            }
        }
    }

  // ajoute l'arbo des applications
  if( is_array($tab) ) 
    {
      foreach($tab as $t) 
        {
          if( $strNav != "" ) 
            $strNav .= $strSepar;
          $strNav .= "<span class=\"divContenuTexte\">".($t["titre"])."</span>";
        }
    }
  else
    $strNav .= $tab;

  
  return $strNav;
}


///<comment>
///<summary>
/// Affiche le bas de la page
///</summary>
///<params name="urlAide" option>Url vers l'aide</param>
///<params name="urlAjout" option>Url vers le mode ajout de la page en cours</param>
///</comment>
function aff_menu_bas()
{
  echo "</td></tr></table>";
  aff_footer();
}


/**
 * @brief Fonction d'affichage du menu propre � l'application
 */
function aff_menu_gauche($arbreMenu, $cont_id, $appli_id, $idCourant)
{
  // rien
}

/**
 * @brief Fonction d'affichage de la fin du menu propre � l'application
 */
function aff_fin_menu_gauche()
{
  //rien
}
?>