<?
///<comment>
///<summary>
/// Fonctions permettant la mise en forme du contenu des pages pour le moteur de recherche
///</summary>
///</comment>

function affActu($idActu, &$drActu, $strLgBdd)
{
	$actu_titre = $drActu->getValueName("TITRE".$strLgBdd);
	$actu_datedeb = $drActu->getValueName("DATEDEB");
	$actu_datefin = $drActu->getValueName("DATEFIN");
	$actu_descc = $drActu->getValueName("DESCC".$strLgBdd);
	$actu_descl = $drActu->getValueName("DESCL".$strLgBdd);
	$actu_visuel = $drActu->getValueName("VISUEL");
	$actu_url = $drActu->getValueName("URL");
	
	$strContenu = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr>
			<td width=\"340\" valign=\"top\">
				<table width=\"340\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	      	<tr>
	        	<td><img src=\"".ALK_URL_SI_MEDIA."transp.gif\" width=\"2\" height=\"6\" border=\"0\"></td>
	        </tr>
	      </table>";
	
	if ($actu_datedeb != "")
	{
		$strContenu .= "<table width=\"340\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				<tr>
					<td valign=\"top\" class=\"txt\"><b>".$actu_datedeb.($actu_datefin != ""?" - ".$actu_datefin:"")."</b></td>
				</tr>
			</table>";
	}
	
	$strContenu .= "<table width=\"340\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			<tr>
				<td><img src=\"".ALK_URL_SI_MEDIA."transp.gif\" width=\"2\" height=\"15\" border=\"0\"></td>
			</tr>
		</table>
		<table width=\"340\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			<tr>
				<td valign=\"top\"  class=\"txt\">";
	
	if ($actu_visuel != "") 
		$strContenu .= "<img src=\"".ALK_SIALKE_URL.ALK_PATH_UPLOAD_ACTU_LOGO.$actu_visuel."\" border=0 vspace=\"2\" hspace=\"5\" align=left>\n";
	
	$strContenu .= "<div align=justify><b>".$actu_descc."</b><br>".$actu_descl."</div>\n";
	
	if ($actu_url != "")
	{
		$strContenu .= "<div align=right><br><a href=\"".$actu_url."\" class=\"nav1\">";
		if (ALK_LG==1)
			$strContenu .= "En savoir plus";
		else
			$strContenu .= "To find out more";
		
		$strContenu .= "</a></div>\n";
	}
	
	$strContenu .= "</td>\n</tr>\n</table>\n</td>\n</tr>\n</table>\n";
	
	return $strContenu;
}

function affLiens(&$dsLiens, $strLgBdd)
{
	$strContenu = "";
	
	while ( $drLien = $dsLiens->getRowIter() )
	{
		$lien_id = $drLien->getValueName("LI_ID");
	  $lien_titre = $drLien->getValueName("TITRE".$strLgBdd);
	  $lien_desc = $drLien->getValueName("DESC".$strLgBdd);
	  $lien_url = $drLien->getValueName("URL");
	  
	  $strContenu .= "<table border=0 cellspacing=0 cellpadding=0><tr>\n".
	  	"<td><img src=".ALK_URL_SI_MEDIA."transp.gif width=2 height=15 border=0></td>\n".
	  	"</tr>\n".
	  	"</table>\n".
	  	"<table border=0 cellspacing=0 cellpadding=0>\n".
	  	"<tr><td valign=top  class=txt><b>".$lien_titre."</b>\n".
	  	"<div align=justify>".$lien_desc.
	  	(($lien_url != "")?"<div align=left><a href=".$lien_url." class=txt target=_blank>".$lien_url."</a></div>":"").
	  	"</div></td></tr></table>\n";
	}

	return $strContenu;
}

function affDoc(&$drDoc, $strLgBdd)
{
	$strContenu = "";

	$doc_intitule = $drDoc->getValueName("DOC_INTITULE");
	$doc_desc = $drDoc->getValueName("DOC_DESC");
	$doc_ref = $drDoc->getValueName("DOC_REF");
	$doc_auteur = $drDoc->getValueName("DOC_AUTEUR");
	$serv_intitule = $drDoc->getValueName("SERVICE_INTITULE");
	$doc_date_crea = $drDoc->getValueName("DATE_CREA");
	$doc_date_maj = $drDoc->getValueName("DATE_MAJ");
	$doc_freq_maj = $drDoc->getValueName("DOC_FREQ_MAJ");
	$doc_mot_cles = $drDoc->getValueName("DOC_MOTS_CLES");
	$theme_id = $drDoc->getValueName("THEME_ID");
	$doc_corbeille = $drDoc->getValueName("DOC_CORBEILLE");
	
	$strContenu .= "<table border=0 cellspacing=0 cellpadding=0><tr>\n".
  	"<td><img src=".ALK_URL_SI_MEDIA."transp.gif width=2 height=15 border=0></td>\n".
  	"</tr>\n".
  	"</table>\n".
  	"<table border=0 cellspacing=0 cellpadding=0>\n".
  	"<tr><td valign=top  class=txt><b>".$doc_intitule."</b>\n".
  	"<div align=justify>\n".
  	$doc_desc."<br>\n".
  	"R&eacute;f&eacute;rence : ". $doc_ref."<br>\n".
  	"Publicateur : ". $doc_auteur."<br>\n".
  	"Service &eacute;metteur : ". $serv_intitule."<br>\n".
  	"Date de cr&eacute;ation : ". $doc_date_crea."<br>\n".
  	"Date de derni&egrave;re modification : ". $doc_date_maj."<br>\n".
  	"Mots cl�s : ". $doc_mot_cles."<br>\n".
  	"</div></td></tr></table>\n";

	return $strContenu;
}

function affGlos(&$dsGlos, $strLgBdd)
{
  $strContenu = "";
  
  while ( $drGlos = $dsGlos->getRowIter() ) {
    $glos_id = $drGlos->getValueName("GL_ID");
    $glos_titre = $drGlos->getValueName("TITRE".$strLgBdd);
    $glos_def = $drGlos->getValueName("DEF".$strLgBdd);
    
    $strContenu .= "<table width=100% border=0 cellspacing=0 cellpadding=0>\n".
      "<tr>\n".
      "<td><img src=".ALK_URL_SI_MEDIA."transp.gif width=2 height=15 border=0></td>\n".
      "</tr>\n".
      "</table>\n".
      "<table width=100% border=0 cellspacing=0 cellpadding=0>\n".
      "<tr>\n".
      "<td valign=top class=txt><span class=telechargement>".$glos_titre."</span>\n".
      "<div align=justify>".$glos_def."</td>\n".
      "</tr>\n".
      "</table>\n";
  }
  
  return $strContenu;
}

function affFaq(&$dsFaq, $strLgBdd)
{
  $strContenu = "";
  
  while ($drFaq = $dsFaq->getRowIter()) {
    $faq_id = $drFaq->getValueName("FAQ_ID");
    $faq_question = $drFaq->getValueName("QUESTION".$strLgBdd);
    $faq_reponse = $drFaq->getValueName("REPONSE".$strLgBdd);
    
    $strContenu .= "<table width=100% border=0 cellspacing=0 cellpadding=0>\n".
      "<tr>\n".
      "<td><img src=".ALK_URL_SI_MEDIA."transp.gif width=2 height=15 border=0></td>\n".
      "</tr>\n".
      "</table>\n".
      "<table width=100% border=0 cellspacing=0 cellpadding=0>\n".
      "<tr>\n".
      "<td valign=top class=txt><span class=telechargement>".$faq_question."</span>\n".
      "<div align=justify>".$faq_reponse."</td>\n".
      "</tr>\n".
      "</table>\n";
  }
  
  return $strContenu;
}

function affRecette(&$drRecette, &$dsProduit, $strLgBdd)
{
  global $queryRecette;
  
  $strContenu = "";
  
  $recette_id = $drRecette->getValueName("RECETTE_ID");
  $recette_titre = $drRecette->getValueName("TITRE".$strLgBdd);
  $recette_ingredients = $drRecette->getValueName("INGREDIENTS".$strLgBdd);
  $recette_preparation = $drRecette->getValueName("PREPARATION".$strLgBdd);
  
  $strProduits = "";
  while ( $drProduit = $dsProduit->getRowIter() )
    $strProduits .= $drProduit->getValueName("PRODUIT".$strLgBdd); 
    
  $strContenu .= "<table width=100% border=0 cellspacing=0 cellpadding=0>\n".
    "<tr>\n".
    "<td><img src=".ALK_URL_SI_MEDIA."transp.gif width=2 height=15 border=0></td>\n".
    "</tr>\n".
    "</table>\n".
    "<table width=100% border=0 cellspacing=0 cellpadding=0>\n".
    "<tr>\n".
    "<td valign=top class=txt><span class=telechargement>".$recette_titre."</span>\n".
    "<p>".$strProduits."</p>\n".
    "<p>".$recette_ingredients."</p>\n".
    "<p>".$recette_preparation."</p></td>\n".
    "</tr>\n".
    "</table>\n";
  
  return $strContenu;
}

function getHeader($strtitre, $strUrlRedirect="", $bIndex=true)
{
	$strHeader = "<html>\n<head>\n".
		"<title>".$strtitre."</title>\n".
		"<meta name=\"title\" content=\"\">\n".
		"<meta name=\"description\" content=\"\">\n".
		"<meta NAME=\"language\" content=\"FR\">\n".
		"<meta name=\"robots\" content=\"".(($bIndex==false)?"noindex, follow":"")."\">\n".
		"<link rel=\"stylesheet\" href=\"".ALK_ROOT_URL."/styles/site_gen.css\" type=\"text/css\">\n".
		"</head>\n";
		
	if ( $strUrlRedirect!="")
		$strHeader .= "<script language=javascript>\n".
      "window.document.location='".$strUrlRedirect."'\n".
      "</script>\n";
	
	$strHeader .= "<body leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n";
	
	return $strHeader;
}

function getFooter()
{
	$strFooter = "</body>\n</html>\n";
	
	return $strFooter;
}

function getCleanFileName($strFile)
{
  $strFile = preg_replace('/&#(\d+);/me', "chr('\\1')", $strFile);
	$strFile = preg_replace( "/<[^>]*>/", "", trim($strFile) );
	$strFile = preg_replace("/ /","_",$strFile);
	$strFile = preg_replace("/'/","_",$strFile);
	$strFile = preg_replace("/\"/","_",$strFile);
	$strFile = preg_replace("/\//","_",$strFile);
  $strFile = preg_replace("/\n/","",$strFile);
  $strFile = strtr($strFile, "���", "aaa");
	$strFile = strtr($strFile, "����", "eeee");
  $strFile = strtr($strFile, "��", "ii");
	$strFile = strtr($strFile, "��", "oo");
	$strFile = strtr($strFile, "���", "uuu");
  $strFile = strtr($strFile, "?!", "__");
	
	return $strFile;
}

function getContenu($type, $fichier)
{
  $res = "";
  $resultat = "";

  switch ($type){
    case "dot":
		case "rtf":
    case "doc":
      exec("/usr/local/bin/catdoc -scp1250 -d8859-1 ".$fichier, $res, $i);
			break;
    case "pdf":
      exec("/usr/local/bin/pdftotext ".$fichier." - ", $res, $i);
			break;
    case "sxc":
    case "sxw":
      exec("/usr/bin/unzip -p ".$fichier, $res, $i);
      break;
    case "xlt":
    case "xls":
      exec("/usr/local/bin/xls2csv -scp1250 -d8859-1 ".$fichier, $res, $i);
			break;
  }

  for ($i=0; $i<count($res)-1; $i++) {
    $resultat .= $res[$i]."\n"; 
  }

  return $resultat;
}

function creationFicHtml($fichier,$chemin, $cont_id, $chaine)
{
  $ext = strtolower(substr($fichier,strrpos($fichier,".")+1,strlen($fichier)-strrpos($fichier,".")-1));
  //si l'extension correspond aux types affiches
  switch($ext) {
  	case "txt":
    case "dot":
		case "rtf":
    case "doc":
    case "pdf":
    case "xlt":
    case "xls":
      //on cr�� un fichier htm correspondant � ce fichier
      $strTitre = substr($fichier, 0, strpos($fichier, '.'));
      $strTitreFic = getCleanFileName(trim($strTitre));
      $strFile = "pj_".$strTitreFic.".html";
      
      $strUrl = preg_replace("/".ALK_ROOT_PATH."/",ALK_ROOT_URL,$chemin.$fichier);

      //on ajout un lien vers ce fichier dans le fichier index_pj.hml
      $chaine .= "<a href=\"".ALK_ROOT_HTDIG_URL."/search/".$cont_id."/".$strFile."\" class=\"txt\">".$strTitre."</a><br>\n";

      //on cr�� le fichier htm correspondant
      if (file_exists(ALK_ROOT_PATH."/search/".$cont_id."/" . $strFile)){
        unlink(ALK_ROOT_PATH."/search/".$cont_id."/" . $strFile);
      }

      $file=fopen(ALK_ROOT_PATH."/search/".$cont_id."/" . $strFile ,"wb"); // Ouverture du fichier avec le mode �criture
      $result = fread($file,8192);

			echo "Cr�ation du fichier /search/".$cont_id."/".$strFile."<br>\n";

      $strContenu = getHeader("Pi�ce jointe - ".$strTitre, $strUrl).getContenu($ext, $chemin.$fichier).getFooter();
      fwrite($file, $strContenu); 
      break;

      default:
      	//on indique dans un fichier log les fichiers qui ne sont pas index�s
      break;
	}
	return $chaine;
}