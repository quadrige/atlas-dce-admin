<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("lib/lib_pagination.php");
include_once("classes/alkmasseeau.class.php");

include_once("lib/lib_pdf.php");


$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$codeBassin = Request("bassin_id", REQ_GET, "LB");
$page =  Request("page", REQ_GET, "1", "is_numeric");
$bPdf = Request("bPdf", REQ_GET, "0", "is_numeric");
$bPagine =  Request("bPagine", REQ_GET, "0", "is_numeric");
$nbEltParPage =  Request("nbEltParPage", REQ_GET, "20", "is_numeric");
$iFirst = ($bPagine ? ($page-1)*$nbEltParPage : 0);
$iLast = ($bPagine ? $iFirst+$nbEltParPage-1 : -1);
    
$menu_id=0;
$iAcces = 0;
$strParam = "bassin_id=".$codeBassin;

$dsBassin = $queryAtlas->getDs_bassin($codeBassin);
if ($drBassin = $dsBassin->getRowIter()){
  $bassin_id = $drBassin->getValueName("BASSIN_ID");
  $bassin_nom = $drBassin->getValueName("BASSIN_NOM");
}

$tabPage = array();
$cpt = 0;
$dsMe = $queryAtlas->getDs_listeMasseEau($iFirst, $iLast, "", $codeBassin);
$nbElt = $dsMe->iCountTotDr;
$strAlert="";
while ($drMe = $dsMe->getRowIter()) {
	
	$oMasseEau = new AlkMasseEau($drMe->getValueName("MASSE_CODE"), $queryAtlas);
	
	$idMe = $drMe->getValueName("MASSE_ID");	
	$strNomMe = $drMe->getValueName("MASSE_NOM");
	$strTypeMe = $drMe->getValueName("MASSE_TYPE");
	$strSuiviMe = $drMe->getValueName("MASSE_TYPE_SUIVI");
	$strAlert = $drMe->getValueName("BASSIN_ALERT");

	$strHtmlNomMe = "<div onmouseover=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'show');\"
											  onmouseout=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'hide');\">".
											  "<a href=\"javascript:void(OpenWindow('".ALK_SIALKE_URL."scripts/site/fiche_etatme.php?code=".$drMe->getValueName("MASSE_CODE")."', '900', '900', 'ficheetatme'));\">".$strNomMe."</a>".
											  "<div id='me".$drMe->getValueName("MASSE_CODE")."' class='detaillisteme1' onmouseout=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'hide');\">
											  <img src='".ALK_SIALKE_URL."upload/carte/".$drMe->getValueName("BASSIN_CODE")."/imgms/vignette/".$drMe->getValueName("MASSE_CODE").".png' class='vignettedesc' align='right'/>
											  <p><b>".$drMe->getValueName("LIB")."</b><br/>											  
											  D�partement(s) : ".$drMe->getValueName("DEPT")."<br/>".
											  //"R�gion marine : ".$drMe->getValueName("REGION_LIBELLE")."<br/>".
											  "Bassin Hydrographique : ".$drMe->getValueName("BASSIN_NOM")."<br/>
											  Type : ".$drMe->getValueName("DESCR")."<br/></p>".
											  "</div></div>";

	$tabPage[$cpt] = array($drMe->getValueName("MASSE_CODE"),$strHtmlNomMe, 
      									 $strTypeMe);
	
  $tabClassement = $oMasseEau->getEtatQualite();	
  $tabResult = array();
  foreach($tabClassement as $nomClassement=>$tabEtat)        
    array_push($tabResult, "<div class='divLeg styleColorLeg' style='background-color:".$tabEtat["ETAT"]["COULEUR"].";'>&nbsp;</div>");    	
  								
	$tabPage[$cpt] = array_merge($tabPage[$cpt], $tabResult);   
	
  $cpt++;    									 
} 

reset($tabClassement);
$nbEltParPage = ($bPagine ? $nbEltParPage : $nbElt);
$tabAlign = array("", "left", "left", "center", "center", "center", "center");
    
$strHtml = "<script language=javascript>".
						"function OpenFichePdf()".
						"{".
	          //"OpenFooterExec('liste_etatme.php?".$strParam."&bPdf=1');".	
            "OpenPopupWindow('".ALK_SIALKE_URL."scripts/site/liste_etatme.php?".$strParam."&bPdf=1', '600','800', 'name');".   				            
            "}</script>".
						"<div class='popupTitle'>Atlas DCE ".$bassin_nom." - Liste des masses d'eau".
						($bPdf == 1 ? "" : 
	          "<a class=\"imprim\" title=\"Imprimer\" href=\"javascript:OpenFichePdf();\">
            <img id=\"imprimer\" height='19' width='19' onmouseout='MM_swapImgRestore()' onmouseover=\"MM_swapImage('imprimer','','".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif' ,1)\" alt='Exporter en pdf' title='Exporter en pdf' src='".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif'/>
            </a>").
            "<div class=\"popupBtClose\"><div onclick=\"javascript:window.close()\" class=\"btClose\"/></div></div></div>";						
	
$strHtml .= "<div id='conteneur'>";
$strHtml .= "<div id='titreME' class='titreMEdetaille'>".	
							"<h1>Bilan provisoire sur les r�sultats dans le cadre du programme de surveillance de la DCE 2000/60/CE							
							</h1>".
            "</div>".
						"<div id='contenuME' class='txt colon classMEdetaille clearfix'>".													
						"<p><font class='fontAlert' >".$strAlert."</font></p>".
						"<table class='table1'  cellpadding='0' cellspacing='0'>
						<thead>
						<tr class='trEntete1' >
						<td class='tdEntete1_300' rowspan=2 colspan=2>Identification de la masse d'eau</td>
						<td class='tdEntete1_100' rowspan=2 >Type</td>
						<td class='tdEntete1_300' colspan=3 >Etat provisoire</td>
						</tr>
						<tr>";
						
foreach($tabClassement as $nomClassement=>$tabEtat){
	$strHtml .= "<td class='tdEntete1_100' >".$nomClassement."</td>";
}

$strHtml .= "</tr></thead>".getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, $page, 
                               $_SERVER["PHP_SELF"]."?".$strParam, 
                               $tabAlign)."</table>".
            ($bPagine == 1 ? "<div class='clearPagine'><a href='".$_SERVER["PHP_SELF"]."?".$strParam."&bPagine=0'>Voir la liste compl�te</a></div>" : "").
            $oMasseEau->getLegende().
            "</div>";

  $strHtml .= "<div id='logos'>".
  							"<img id='logoIfremer' src='".ALK_SIALKE_URL."media/imgs/gen/Ifremer_logo.gif' width='110' />".
					  		"<img id='logoAE' src='".ALK_SIALKE_URL."media/imgs/gen/".$codeBassin."_logo_agence_de_leau.jpg' />".
            "</div>";

  $strHtml .= "</div>";
  $strHtml .= "<div id='pied_page'></div>";
  
if ($bPdf == 1) {
	$strHtml = str_replace(ALK_ROOT_URL, ALK_ROOT_PDF_URL, getHtmlHeader()).
			"</head>" .
      "<body class='bodyPdf'>".$strHtml.getHtmlFooter();

	//$strFile = getPdf2($strHtml, "", "", $codeBassin."_synthese");	
	affPdf($strHtml, $codeBassin."_synthese");	
} else {
	aff_menu_haut();
	echo $strHtml;
	aff_menu_bas();echo "<style> .tdPair1.stylegeneral{background-color: #DFF0FF;}</style>";
        echo "<script src=\"".ALK_SIALKE_URL."/libconf/lib/jquery-1.11.1.min.js\" type=\"text/javascript\"></script>";
        $strJquery = <<< ��
        <script type="text/javascript" >
          $(document).ready(function(){
            
            var myCurrentOverlayCell = null;
                
            var showOverLayCell = function(cellLeft, cellRight){

              var offSetLeft = cellLeft.offset();
              if( myCurrentOverlayCell ){
                myCurrentOverlayCell.remove();
              }
              
              var tmpClonePopup1 = null;
              var tmpOffsetPopup = {top: 0, left: 0};
              if( cellLeft.children('.popup1').length > 0 ){
                tmpOffsetPopup = cellLeft.children('.popup1').offset();
                var tmpTopOffsetPopup = tmpOffsetPopup.top - offSetLeft.top;
                var tmpLeftOffsetPopup = tmpOffsetPopup.left - offSetLeft.left;
                tmpOffsetPopup = {top: tmpTopOffsetPopup, left: tmpLeftOffsetPopup};
                tmpClonePopup1 = cellLeft.children('.popup1').clone();
                tmpClonePopup1.children('img').remove();
              }
                
              myCurrentOverlayCell = $('<div id="myTmpCellOverlay" ></div>');
              if( tmpClonePopup1 ){
                myCurrentOverlayCell.append(tmpClonePopup1.offset(tmpOffsetPopup).css({position: 'absolute', width:'11px', height: '10px'}));
              }
                
              var tmpCellWidth = cellLeft.innerWidth() + cellRight.innerWidth()+2;
              var tmpCellHeight = cellLeft.innerHeight()+2;
              var actionOnClick = cellRight.attr('onclick');
                
              $('body').append(myCurrentOverlayCell.offset(
                  {
                    top: offSetLeft.top-1,
                    left: offSetLeft.left-1
                  }
                ).css(
                  {
                    position: 'absolute',
                    width: tmpCellWidth+'px',
                    height: tmpCellHeight+'px',
                    
                    cursor: 'pointer',
                    'background-color': 'darkslategray',
                    opacity: 0.4,
                    filter: 'alpha(opacity=40)'
                  }
                ).attr('onclick', actionOnClick).click(function(event){
                  event.stopPropagation();
                  myCurrentOverlayCell.remove();
                }).hover(function(){
                  }, function(){
                    myCurrentOverlayCell.remove();
                  }
                )
              );
            };
                                
            $('table.listeEtat tr.trPair1 td.tdPair1:not(.tdEtat)').hover(function(){
              var tmpTdNotEtat = $(this);
              var tmpTdEtat = $(this).next('.tdPair1.tdEtat');
              var tmpAttrOnclick = tmpTdEtat.attr('onclick');
              if (typeof tmpAttrOnclick !== typeof undefined && tmpAttrOnclick !== false) {
                showOverLayCell(tmpTdNotEtat, tmpTdEtat);
              }
            },function(){});

            $('table.listeEtat tr.trPair1 td.tdPair1.tdEtat[onclick]').hover(function(){
              var tmpTdEtat = $(this);
              var tmpTdNotEtat = $(this).prev('.tdPair1:not(.tdEtat)');
              showOverLayCell(tmpTdNotEtat, tmpTdEtat);
            },function(){});
                
          });
        </script>
��;
        echo $strJquery;
}	

?>