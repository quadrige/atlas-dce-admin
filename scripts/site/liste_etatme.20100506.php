<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("lib/lib_pagination.php");
include_once("classes/alkmasseeau.class.php");

include_once("../../classes/html2pdf/html2fpdf.php");
include_once("../../classes/html2pdf/alkhtml2pdf.class.php");

$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$codeBassin = Request("bassin_id", REQ_GET, "LB");
$page =  Request("page", REQ_GET, "1", "is_numeric");
$bPagine =  Request("bPagine", REQ_GET, "1", "is_numeric");
$nbEltParPage =  Request("nbEltParPage", REQ_GET, "20", "is_numeric");
$iFirst = ($bPagine ? ($page-1)*$nbEltParPage : 0);
$iLast = ($bPagine ? $iFirst+$nbEltParPage-1 : -1);
    
$menu_id=0;
$iAcces = 0;
$strParam = "bassin_id=".$codeBassin;

$tabPage = array();
$cpt = 0;
$dsMe = $queryAtlas->getDs_listeMasseEau($iFirst, $iLast, "", $codeBassin);
$nbElt = $dsMe->iCountTotDr;
$strAlert="";
while ($drMe = $dsMe->getRowIter()) {
	
	$oMasseEau = new AlkMasseEau($drMe->getValueName("MASSE_CODE"), $queryAtlas);
	
	$idMe = $drMe->getValueName("MASSE_ID");	
	$strNomMe = $drMe->getValueName("LIB");
	$strTypeMe = $drMe->getValueName("MASSE_TYPE");
	$strSuiviMe = $drMe->getValueName("MASSE_TYPE_SUIVI");
	$strAlert = $drMe->getValueName("BASSIN_ALERT");
	
	$strHtmlNomMe = "<div onmouseover=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'show');\"
											  onmouseout=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'hide');\">".
											  "<a href='#' onclick=\"OpenWindow('fiche_etatme.php?code=".$drMe->getValueName("MASSE_CODE")."', 'ficheetatme', '900', '900', 'no', 'no');\">".$strNomMe."</a>".
											  "<div id='me".$drMe->getValueName("MASSE_CODE")."' class='detaillisteme' style='visibility:hidden;' onmouseout=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'hide');\">
											  <img src='/upload/carte/imgms/vignette/".$drMe->getValueName("MASSE_CODE").".png' bgcolor='#ffffff' width='100px' align='right'/>
											  <b>".$drMe->getValueName("LIB")."</b><br/>											  
											  D�partement(s) : ".$drMe->getValueName("DEPT")."<br/>".
											  //"R�gion marine : ".$drMe->getValueName("REGION_LIBELLE")."<br/>".
											  "Bassin Hydrographique : ".$drMe->getValueName("BASSIN_NOM")."<br/>
											  Type : ".$drMe->getValueName("DESCR")."<br/>". 											  
											  "</div></div>";

	$tabPage[$cpt] = array($strHtmlNomMe, 
      									 $strTypeMe);
	
  $tabClassement = $oMasseEau->getEtatQualite();	
  $tabResult = array();  									 
	foreach($tabClassement as $nomClassement=>$tabEtat)
		array_push($tabResult, "<div class='divLeg' style='background-color:".$tabEtat["ETAT"]["COULEUR"]."; top:0;float:none;'></div>");		
												
	$tabPage[$cpt] = array_merge($tabPage[$cpt], $tabResult);   
	
  $cpt++;    									 
} 
 
reset($tabClassement);
$nbEltParPage = ($bPagine ? $nbEltParPage : $nbElt);
$tabAlign = array("", "left", "center", "center", "center", "center");
    
$strHtml = "<div class=\"popupTitle\">Liste des masses d'eau</div>".
	           "<div class=\"popupBtClose\"><div onclick=\"javascript:top.closeWindow('')\" class=\"btClose\"/></div></div>";
	
$strHtml .= "<div class='txt colon' style='position: relative; top:10px; width: 720px;  margin-left: auto; margin-right: auto; '>".
							"<h1 style='float: left; width: 480px;height:80px;text-align:center;line-height: 25px;font-size:12px;'>Bilan provisoire sur les r�sultats dans le cadre du programme de surveillance de la DCE 2000/60/CE<br/>							
							</h1>".
						"<img id='logoAE' src='../../media/imgs/gen/".$codeBassin."_logo_agence_de_leau.jpg' style='float:right;margin-left:10px;margin-top: 0px;'>".
						"<img id='logoIfremer' src='../../media/imgs/gen/Ifremer_logo.gif' width='110' style='float:right;margin-top: 20px;'>".
						"<div style='clear:both'><font style='color:#9a3939;font-style:italic;'>$strAlert</font></div>".
						"<table class='table1'  cellpadding='0' cellspacing='0'>
						<tr class='trEntete1' >
						<td class='tdEntete1' width='300' rowspan=2 align=center>Identification de la masse d'eau</td>
						<td class='tdEntete1' width='100' rowspan=2 align=center>Type</td>
						<td class='tdEntete1' width='300' colspan=3 align=center>Etat provisoire</td>
						</tr>
						<tr>";
						
foreach($tabClassement as $nomClassement=>$tabEtat){
	$strHtml .= "<td class='tdEntete1' width='100' align=center>".$nomClassement."</td>";
}
$strHtml .= "</tr>".getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, $page, 
                               $_SERVER["PHP_SELF"]."?".$strParam, 
                               $tabAlign)."</table>".
            ($bPagine == 1 ? "<div style='clear:both;margin-left:auto;margin-top:-35px;text-align:center;width:75px;'><a href='".$_SERVER["PHP_SELF"]."?".$strParam."&bPagine=0'>Voir la liste compl�te</a></div>" : "").
            "<div id='legendeEtat' style='clear:both;margin-left:auto;float:none;'>".$oMasseEau->getLegende()."</div>".
            "</div>";

aff_menu_haut();
echo $strHtml;
aff_menu_bas();

?>