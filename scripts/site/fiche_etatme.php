<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("lib/lib_pagination.php");
include_once("lib/lib_pdf.php");

include_once("classes/alkmasseeau.class.php");

$tabEventBody["onLoad"] ="if(top.HideLoadMsg) top.HideLoadMsg();";

$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$codeMe = Request("code", REQ_GET, "");
$bPdf = Request("bPdf", REQ_GET, "0", "is_numeric");
$session_id = Request("idSession", REQ_GET, "-1", "is_numeric");

$strDirArchive = ($session_id != -1 ? "/archive_".$session_id : "");

$menu_id=0;
$iAcces = 0;
$strParam = "code=".$codeMe."&idSession=".$session_id;

$oMasseEau = new AlkMasseEau($codeMe, $queryAtlas, "", $session_id);
$strHtml = $strHtmlNomMe = "";

$lienFicheDescME ="";
$tabPage = array();
$idMe = -1;
$dsMe = $queryAtlas->getDs_MasseEauById("", $codeMe, $session_id);
if ($drMe = $dsMe->getRowIter()) {
	$idMe = $drMe->getValueName("ID");
	$strNomMe = $drMe->getValueName("LIB");
	$strTypeMe = $drMe->getValueName("MASSE_TYPE");
	$strSuiviMe = $drMe->getValueName("MASSE_TYPE_SUIVI");
	$strAlert = $drMe->getValueName("BASSIN_ALERT");
	$codeBassin = $drMe->getValueName("BASSIN_CODE");
	$bassin_nom = $drMe->getValueName("BASSIN_NOM");
  $me_commentaire = $drMe->getValueName("MASSE_COMMENTAIRE");

  $strMotifDerogation = $drMe->getValueName("MASSE_MOTIF_DEROGATION");
  $tabMotif = array();
  if ($strMotifDerogation != ""){
    $tabMotif = explode(";",$strMotifDerogation);
    $listMotif = array("CN" => "Conditions naturelles",
          						 "FT" => "Faisabilit� technique",
          						 "CD" => "Co�ts disproportionn�s");
    $tabMotif2 = array();
    foreach($tabMotif as $key){
      array_push($tabMotif2,$listMotif[$key]);
    }
  	$strMotifDerogation = implode(",", $tabMotif2);
  }

  $bassin_id = $drMe->getValueName("BASSIN_ID");
	$lienFicheContexteME = $drMe->getValueName("MASSE_DOC_DESC");
	$lienFicheDescME = $drMe->getValueName("MASSE_DOC_FICHE");

	$strHtmlGpReseau = "";
	$dsGpReseau = $queryAtlas->getDs_listeGroupeReseauByMe($idMe, $session_id);
	while ($drGpReseau = $dsGpReseau->getRowIter()) {
    $strHtmlGpReseau .= "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>".
	                      ($drGpReseau->getValueName("NB_POINT")>0
	                       ? "<a onmouseover=\"MM_showHideLayers('gp_reso_".$drGpReseau->getValueName("GROUPE_ID")."', '', 'show');\"
  											  onmouseout=\"MM_showHideLayers('gp_reso_".$drGpReseau->getValueName("GROUPE_ID")."', '', 'hide');\">".
  											  $drGpReseau->getValueName("GROUPE_NOM")."</a>"
  											: $drGpReseau->getValueName("GROUPE_NOM")).
	                      "</span><span>".($drGpReseau->getValueName("NB_POINT")>0 ? "Oui" : "Non")."</span></p>".
  											( $drGpReseau->getValueName("NB_POINT")>0
  											  ? "<div id='gp_reso_".$drGpReseau->getValueName("GROUPE_ID")."' class='detaillisteme stylepopup2 gp_reso' onmouseout=\"MM_showHideLayers('gp_reso_".$drGpReseau->getValueName("GROUPE_ID")."', '', 'hide');\">".
  	                        $drGpReseau->getValueName("LIST_RESO").
  											 		"</div>"
  	                      : "");
	}


  // tab de correspondance objectifs envrionnementaux
  $tabCorresp = array();
  $ds = $queryAtlas->getTableForCombo("ATTEINTE_OBJECTIF_DESCRIPTION", "ATTEINTE_OBJECTIF", true, false, "");
  while($dr = $ds->getRowIter()){
        $tabCorresp[$dr->getValueName("ATTEINTE_OBJECTIF_ID")] = $dr->getValueName("nom");
  }

  $strHtmlNomMe = "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Bassin Hydrographique</span><span>".$drMe->getValueName("BASSIN_NOM")."</span></p>
                  <p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>D�partement(s)</span><span>".$drMe->getValueName("DEPT")."</span></p>
	 			  				<p ".($bPdf != 1 ? "class='clearfix type'" : "class='type clearfix_pdf'")."><span class='item'>Type</span><span>".$drMe->getValueName("DESCR")."</span></p>".
                  ($drMe->getValueName("MASSE_B_FORT_MODIF") != ""
                    ? "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Masse d'eau fortement modifi�e</span><span>".( $drMe->getValueName("MASSE_B_FORT_MODIF")=="1" ? "Oui" : "Non")."</span></p>"
                    : "").
                  ($drMe->getValueName("MASSE_B_ATTEINTE_OBJ") != ""
                    ? "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Objectifs environnementaux</span><span>".$tabCorresp[$drMe->getValueName("MASSE_B_ATTEINTE_OBJ")]."</span></p>"
                    : "").
                  (is_array($tabMotif) && count($tabMotif)>0
                    ? "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Motif(s) d�rogation</span><span>".$strMotifDerogation."</span></p>"
                    : "").
                  //"<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Report</span><span>".( ($drMe->getValueName("MASSE_REPORT_OBJECTIF")!="") ? $drMe->getValueName("MASSE_REPORT_OBJECTIF") : "-")."</span></p>".
                  ($drMe->getValueName("MASSE_B_SUIVI") != ""
                    ? "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Suivie au titre du programme de surveillance de la DCE 2000/60/CE</span><span>".($drMe->getValueName("MASSE_B_SUIVI") == "1" ? "Oui" : "Non")."</span></p>"
                    : "").
                    "<br>".$strHtmlGpReseau.
                    ($lienFicheContexteME != "" && $bPdf != 1 ? "<p style=\"text-align:right;\"><a href=\"".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$codeBassin.$strDirArchive."/".$lienFicheContexteME."\" target=\"_blank\">Contexte physique de la masse d'eau</a></p>" : "").
                    ($lienFicheDescME != "" && $bPdf != 1 ? "<p style=\"text-align:right;\"><a href=\"".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$codeBassin.$strDirArchive."/".$lienFicheDescME."\" target=\"_blank\">Fiche de la masse d'eau</a></p>" : "");

	$tabEtat = $oMasseEau->getEtatQualiteGlobal();

  $strHtmlListeEtat = "<table class='table1 listeEtat' cellpadding='0' cellspacing='0'>
											<tr class='trEntete1' >
											<td class='tdEntete1_list_detail_me' colspan=7 >
											<h2>Bilan provisoire sur les r�sultats acquis dans le cadre du programme de surveillance de la DCE 2000/60/CE</h2>
											<p class='styleAlertDetailme'>".$strAlert."</p>
                      <p class='styleAlertDetailme'>".$me_commentaire."</p>".
                      //getLayerMethode($tabEtat, "global", "top:-60px;text-align:right;left:0px;").
                      getLayerMethode($tabEtat, "global", "classgetLayerMethode1").
											 "</td>
											<td class='tdEntete1 tdEtat popupBackg positionEtatGlobal' style='background-color:".$tabEtat["COULEUR"].";'>Etat global".
                      (array_key_exists("INDICE_CONFIANCE", $tabEtat) ? "<p id='pIndConfiance'>Niveau de confiance<br/>".$tabEtat["INDICE_CONFIANCE"]."</p>" : "").
											"</td>".
											"</tr>
											<tr>";

  $oMasseEau->setEtatQualite("", false, true);
	$tabClassement = $oMasseEau->getEtatQualite();

  $strEnteteType = "";
  $strIndConfiance = "";
	foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {
		if ($nomClassement != "Global"){
			foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
				if ($nomTypeElement != "ETAT"){
					$strEnteteType .= "<td class='tdEntete1_listMEdetail'><div class='tdTabMEdetaille' >".$nomTypeElement."</div>".getLayerMethode($tabEtat, $nomTypeElement, "classgetLayerMethode2").
														"</td><td class='tdEtat' style='background-color:".$tabEtat["COULEUR"].";'></td>"   ;

        }
			}
			$strHtmlListeEtat .= "<td colspan=".(count($tabClassement[$nomClassement])*2 - 3)." class='tdEntete1_listMEdetail' align='center'><div class='divstyleEtat'>Etat ".$nomClassement."</div>".
			                    getLayerMethode($tabClassement[$nomClassement]["ETAT"], $nomClassement, "classgetLayerMethode3")."</td>
												 <td class='tdEntete1 tdEtat' style='background-color:".$tabClassement[$nomClassement]["ETAT"]["COULEUR"].";'>&nbsp;</td>" ;

      $indice = (array_key_exists("INDICE_CONFIANCE", $tabClassement[$nomClassement]["ETAT"]) && $tabClassement[$nomClassement]["ETAT"]["INDICE_CONFIANCE"]!=""
                ? $tabClassement[$nomClassement]["ETAT"]["INDICE_CONFIANCE"]
                : "");
      $strIndConfiance .=  "<td colspan=".(count($tabClassement[$nomClassement])*2 - 3)." class='tdEntete1_listMEdetail' align='center'><div class='divstyleEtat'>Niveau de confiance</div></td>".
                         "<td class='tdEntete1' style='background-color:".($indice != "" ? "" : "#C2C2C2").";'>".
                          $indice."</td>" ;
		}
	}
	$strHtmlListeEtat .= "</tr>".
											"<tr>".
                      $strIndConfiance.
                      "</tr>".
                      "<tr>".
											$strEnteteType.
											"</tr>";



	$nbElement = 0;
	foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {
		foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
			if (array_key_exists("ELEMENTS", $tabClassement[$nomClassement][$nomTypeElement])){
			  $nbElement = max($nbElement, count($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"]));
			}

		}
	}

	for ($i=0; $i<$nbElement;$i++){
		$strHtmlListeEtat .= "<tr class='trPair1'>";
		reset($tabClassement);
		foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {
			foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
				if ($nomTypeElement != "ETAT"){
					if (array_key_exists($i, $tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"])){
						$urlSynthese = $tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["FICHE_SYNTHESE"];
						$strClass = ($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["NIVEAU"]==1 ? "stylegeneral" : "");
					  $strHtmlListeEtat .= "<td class='tdPair1 ".$strClass."' >".
					  											($urlSynthese != "" ? "<a href='#' onclick=\"OpenWindow('".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$codeBassin.$strDirArchive."/".$urlSynthese."', '800', '1000', 'synthesequal', 'no', 'no') ;\" title=\"Fiche de synth�se de l'�l�ment de qualit�\">".
																	$tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["NOM"]."</a>"
																	                    : $tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["NOM"]).
																	getLayerMethode($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i], $i, "classgetLayerMethode4").
																	"</td>".
																 "<td class='tdPair1 tdEtat ".
                                   ($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["NIVEAU"] == 1 ? "classpointer" : "")."' ".
																	($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["NIVEAU"] == 1
																	  ? "onclick=\"OpenWindow('".ALK_SIALKE_URL."scripts/site/fiche_etatmequal.php?".$strParam."&qualite_id=".$tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["ID"]."', '800', '1000', 'fichetatmequal', 'no', 'no') ;\" title=\"R�sultats pour l'�l�ment de qualit� pour ".$codeMe."\""
																	  : "").
																	" style='background-color:".$tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["COULEUR"].";'".
																	">".
																	"&nbsp;".
																	($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["MOTIF"] != "" ? "&nbsp;(".$tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["MOTIF"].")":"").
																	($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["STATUT"] != "" ? "&nbsp;(".$tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["STATUT"].")":"")."</td>";
					} else {
						$strHtmlListeEtat .= "<td colspan=2>&nbsp;</td>";
					}
				}
			}
		}
		$strHtmlListeEtat .= "</tr>";
	}

	$strHtmlListeEtat .= "</table>";

	$strHtml = "<script language=javascript>".
						 "function OpenFichePdf()".
						 "{".
						 "OpenPopupWindow('".ALK_SIALKE_URL."scripts/site/fiche_etatme.php?".$strParam."&bPdf=1','600','800','fiche_me');".
             //"OpenFooterExec('".ALK_SIALKE_URL."scripts/site/fiche_etatme.php?".$strParam."&bPdf=1');".
						"}</script>".
	          "<div class=\"popupTitle\">Atlas DCE ".$bassin_nom." - Bilan des r�sultats par masse d'eau".
	          ($bPdf == 1 ? "" :
	          "<a class=\"imprim\" title=\"Imprimer\" href=\"javascript:OpenFichePdf();\">
            <img id=\"imprimer\" height='19' width='19' onmouseout='MM_swapImgRestore()' onmouseover=\"MM_swapImage('imprimer','','".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif' ,1)\" alt='Exporter en pdf' title='Exporter en pdf' src='".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif'/>
            </a>").
	          "<div class=\"popupBtClose\"><div onclick=\"javascript:top.closeWindow('')\" class=\"btClose\"/></div></div></div>";

	$strHtml .= "<div id='conteneur'>";

	$strHtml .= "<div id='titreME' class='titreMEdetaille'>".
							"<h1>
							Masse d'eau ".($drMe->getValueName("MASSE_TYPE") == "MEC" ? "c�ti�re" : ($drMe->getValueName("MASSE_TYPE") == "MER" ? "r�cifale" : "de transition"))."&nbsp;".$drMe->getValueName("CODE")."<br/>".$drMe->getValueName("NOM")."</h1>".
							"</div>".
							"<div id='contenuME' class='txt colon classMEdetaille clearfix'>".
							"<div id='vignetteFicheMe' ><img src='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_CARTE.$drMe->getValueName("BASSIN_CODE")."/imgms/vignette/".$drMe->getValueName("CODE").".png'/></div>".
							"<div id='descFicheMe'>".$strHtmlNomMe."</div>".
							"<div id='listeEtatFicheMe' class='clearfix'>".$strHtmlListeEtat."</div>".
							$oMasseEau->getLegende().
							$oMasseEau->getLegendeMotif($bassin_id).
							$oMasseEau->getLegendeIndConfiance();

	if ( !in_array( $drMe->getValueName( "BASSIN_CODE"  ), array( 'MAY', 'LB' ) ) ) {
				$strHtml .= "<div id='carteFicheMeQual'><img src='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_CARTE.$drMe->getValueName("BASSIN_CODE")."/imgms/carte/" . $drMe->getValueName("CODE").".png'  align='right'/>" . $oMasseEau->getLegendeReseaux(1, $bassin_id, 1)."</div>";
							}
	$strHtml .= "</div>";

    // recherche de la derniere maj en rapport avec le classement de la me
  if($idMe != -1){
    $dsDateMajClassementME = $queryAtlas->getDs_DateMAJByMasseQualiteId("", $idMe, $session_id);

    if($drDateMajClassementME = $dsDateMajClassementME->getRowIter()){
      if($drDateMajClassementME->getValueName("CLASSEMENT_ME_DATE_MAJ") !=null)
      $strHtml .= "<div> Derni�re mise � jour : ".$drDateMajClassementME->getValueName("CLASSEMENT_ME_DATE_MAJ")."</div>";
    }
  }


	$strHtml .= "<div id='logos'>".
	              ($drMe->getValueName("BASSIN_CODE") == 'MAY' ?
             	    "<img id='logoIfremer2' class='classlogoONEMA' src='".ALK_SIALKE_URL."media/imgs/gen/".$drMe->getValueName("BASSIN_CODE")."_ONEMA.jpg'/>"
             	    : "").
  							"<img id='logoIfremer' src='".ALK_SIALKE_URL."media/imgs/gen/Ifremer_logo.gif' width='110' />".
					  		"<img id='logoAE' src='".ALK_SIALKE_URL."media/imgs/gen/".$drMe->getValueName("BASSIN_CODE")."_logo_agence_de_leau.jpg' />".
						"</div>";

  $strHtml .= "</div>";
  $strHtml .= "<div id='pied_page'>";


  $strHtml .= "</div>";

} else
	$strHtml = "<div class='txt colon classMEnonIdentifie' >Masse d'eau non identifi�e</div>";

if ($bPdf == 1) {
	$strHtml = str_replace(ALK_ROOT_URL, ALK_ROOT_PDF_URL, getHtmlHeader()).
			"</head>" .
			"<body text=\"#000000\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">".$strHtml.getHtmlFooter();

	//$strFile = getPdf2($strHtml, "", "", $codeMe);
	affPdf($strHtml, $codeMe."_qualite");
} else {
	aff_menu_haut($tabEventBody);
	echo $strHtml;
	aff_menu_bas();echo "<style> .tdPair1.stylegeneral{background-color: #DFF0FF;}</style>";
        echo "<script src=\"".ALK_SIALKE_URL."/libconf/lib/jquery-1.11.1.min.js\" type=\"text/javascript\"></script>";
        $strJquery = <<< ��
        <script type="text/javascript" >
          $(document).ready(function(){

            var myCurrentOverlayCell = null;

            var showOverLayCell = function(cellLeft, cellRight){

              var offSetLeft = cellLeft.offset();
              if( myCurrentOverlayCell ){
                myCurrentOverlayCell.remove();
              }

              var tmpClonePopup1 = null;
              var tmpOffsetPopup = {top: 0, left: 0};
              if( cellLeft.children('.popup1').length > 0 ){
                tmpOffsetPopup = cellLeft.children('.popup1').offset();
                var tmpTopOffsetPopup = tmpOffsetPopup.top - offSetLeft.top;
                var tmpLeftOffsetPopup = tmpOffsetPopup.left - offSetLeft.left;
                tmpOffsetPopup = {top: tmpTopOffsetPopup, left: tmpLeftOffsetPopup};
                tmpClonePopup1 = cellLeft.children('.popup1').clone();
                tmpClonePopup1.children('img').remove();
              }

              myCurrentOverlayCell = $('<div id="myTmpCellOverlay" ></div>');
              if( tmpClonePopup1 ){
                myCurrentOverlayCell.append(tmpClonePopup1.offset(tmpOffsetPopup).css({position: 'absolute', width:'11px', height: '10px'}));
              }

              var tmpCellWidth = cellLeft.innerWidth() + cellRight.innerWidth()+2;
              var tmpCellHeight = cellLeft.innerHeight()+2;
              var actionOnClick = cellRight.attr('onclick');

              $('body').append(myCurrentOverlayCell.offset(
                  {
                    top: offSetLeft.top-1,
                    left: offSetLeft.left-1
                  }
                ).css(
                  {
                    position: 'absolute',
                    width: tmpCellWidth+'px',
                    height: tmpCellHeight+'px',

                    cursor: 'pointer',
                    'background-color': 'darkslategray',
                    opacity: 0.4,
                    filter: 'alpha(opacity=40)'
                  }
                ).attr('onclick', actionOnClick).click(function(event){
                  event.stopPropagation();
                  myCurrentOverlayCell.remove();
                }).hover(function(){
                  }, function(){
                    //myCurrentOverlayCell.remove();
                  }
                )
              );
            };

            $('table.listeEtat tr.trPair1 td.tdPair1:not(.tdEtat)').hover(function(){
              var tmpTdNotEtat = $(this);
              var tmpTdEtat = $(this).next('.tdPair1.tdEtat');
              var tmpAttrOnclick = tmpTdEtat.attr('onclick');
              if (typeof tmpAttrOnclick !== typeof undefined && tmpAttrOnclick !== false) {
                showOverLayCell(tmpTdNotEtat, tmpTdEtat);
              }
            },function(){});

            $('table.listeEtat tr.trPair1 td.tdPair1.tdEtat[onclick]').hover(function(){
              var tmpTdEtat = $(this);
              var tmpTdNotEtat = $(this).prev('.tdPair1:not(.tdEtat)');
              showOverLayCell(tmpTdNotEtat, tmpTdEtat);
            },function(){});

          });
        </script>
��;
        echo $strJquery;
}

function getLayerMethode($tabEtat, $name="", $strClass="")
{
  global $bPdf;
  $strHtml = "";

  if (!$bPdf && array_key_exists("METHODE", $tabEtat) && $tabEtat["METHODE"]!=""){
$left = "left_0";
		if ($name == "global" || $name == "Etat physico-chimique" || $name=="�cologique")
      $left = "left_150";
    $strHtml = "<div class='popup1 ".$strClass."' onmouseover=\"MM_showHideLayers('methode_$name', '', 'show');\"
											  onmouseout=\"MM_showHideLayers('methode_$name', '', 'hide');\">
											  <img src='".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_glossaire.gif' class='imgPopup1'/>
											  <div id='methode_$name' class='detaillisteme stylepopup1 ".$left."' onmouseout=\"MM_showHideLayers('methode_$name', '', 'hide');\">".
	                      $tabEtat["METHODE"].
											 "</div>
											  </div>";
  }

  return $strHtml;
}
