<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("lib/lib_pagination.php");
include_once("classes/alkmasseeau.class.php");

include_once("../../classes/html2pdf/html2fpdf.php");
include_once("../../classes/html2pdf/alkhtml2pdf.class.php");

$tabEventBody["onLoad"] ="if(top.HideLoadMsg) top.HideLoadMsg();";

$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$codeMe = Request("code", REQ_GET, "");
$bPdf = Request("bPdf", REQ_GET, "0", "is_numeric");

$menu_id=0;
$iAcces = 0;
$strParam = "code=".$codeMe;

$oMasseEau = new AlkMasseEau($codeMe, $queryAtlas);
$strHtml = $strHtmlNomMe = "";

$tabPage = array();
$dsMe = $queryAtlas->getDs_MasseEauById("", $codeMe);
if ($drMe = $dsMe->getRowIter()) {
	$idMe = $drMe->getValueName("MASSE_ID");	
	$strNomMe = $drMe->getValueName("LIB");
	$strTypeMe = $drMe->getValueName("MASSE_TYPE");
	$strSuiviMe = $drMe->getValueName("MASSE_TYPE_SUIVI");
	$strAlert = $drMe->getValueName("BASSIN_ALERT");
	
	$strHtmlNomMe = "<table class='txt' cellpadding='0' cellspacing='0' style='width:380px;border:1px solid #c6c6c6;height:110px;'>".
									 "<tr><td width='150'><b>D�partement(s)</b></td><td>".$drMe->getValueName("DEPT")."</td></tr>".
									 //"<tr><td><b>R�gion marine</b></td><td>".$drMe->getValueName("REGION_LIBELLE")."</td></tr>".
									 "<tr><td><b>Bassin Hydrographique</b></td><td>".$drMe->getValueName("BASSIN_NOM")."</td></tr>".
									 "<tr><td><b>Type</b></td><td>".$drMe->getValueName("DESCR")."</td></tr></table>";

	$tabEtat = $oMasseEau->getEtatQualiteGlobal();	
	$strHtmlListeEtat = "<table class='table1 listeEtat' cellpadding='0' cellspacing='0'>
											<tr class='trEntete1' >
											<td class='tdEntete1' style='background-color:#ffffff;color:#000000;font-size:9pt;' colspan=7 align=center>Bilan provisoire sur les r�sultats dans le cadre du programme de surveillance de la DCE 2000/60/CE<br/>
											<font style='color:#9a3939;font-style:italic;font-size:11px;'>$strAlert</font></td>
											<td class='tdEntete1 tdEtat' style='background:".$tabEtat["COULEUR"]." url(".ALK_SIALKE_URL."media/imgs/gen/pictos/fond_rond.gif) center;'></td>
											</tr>
											<tr>";
	$oMasseEau->setEtatQualite($niveau=2);
	$tabClassement = $oMasseEau->getEtatQualite();	
	
	$strEnteteType = "";
	foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {		
		if ($nomClassement != "Global"){
			foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
				if ($nomTypeElement != "ETAT"){				
					$strEnteteType .= "<td class='tdEntete1' align=center>".$nomTypeElement."</td><td class='tdEtat' style='background-color:".$tabEtat["COULEUR"].";'></td>";						
				}
			}
			$strHtmlListeEtat .= "<td colspan=".(count($tabClassement[$nomClassement])*2 - 3)." class='tdEntete1' align='center'>Etat ".$nomClassement."</td>
												 <td class='tdEntete1 tdEtat' style='background-color:".$tabClassement[$nomClassement]["ETAT"]["COULEUR"].";'>&nbsp;</td>";					 
		}		
	}											
	$strHtmlListeEtat .= "</tr>".
											"<tr>".
											$strEnteteType.
											"</tr>";

	$nbElement = 0;		
	foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {
		foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
			if (array_key_exists("ELEMENTS", $tabClassement[$nomClassement][$nomTypeElement]))
				$nbElement = max($nbElement, count($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"]));							
		}	
	}																	
	for ($i=0; $i<$nbElement;$i++){
		$strHtmlListeEtat .= "<tr class='trPair1'>";
		reset($tabClassement);
		foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {
			foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
				if ($nomTypeElement != "ETAT"){
					if (array_key_exists($i, $tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"])){
						$urlSynthese = $tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["FICHE_SYNTHESE"];						
						$strHtmlListeEtat .= "<td class='tdPair1'><a href='#' onclick=\"OpenWindow('".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$urlSynthese."', '800', '1000', 'synthesequal', 'no', 'no') ;\" title=\"Fiche de synth�se de l'�l�ment de qualit�\">".
																	$tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["NOM"]."</a></td>".
																 "<td class='tdPair1 tdEtat' onclick=\"OpenWindow('fiche_etatmequal.php?code=".$codeMe."&qualite_id=".$tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["ID"]."', '800', '1000', 'fichetatmequal', 'no', 'no') ;\" title=\"R�sultats pour l'�l�ment de qualit� pour ".$codeMe."\" style='background-color:".$tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["COULEUR"].";'>".
																	"&nbsp;".
																	$tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["MOTIF"].
																	($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["STATUT"] != "" ? "&nbsp;(".$tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"][$i]["STATUT"].")":"")."</td>";
					} else {
						$strHtmlListeEtat .= "<td colspan=2>&nbsp;</td>";
					}
				}		
			}
		}										
		$strHtmlListeEtat .= "</tr>";
	}
											
	$strHtmlListeEtat .= "</table>";
	
	$strHtml = "<script language=javascript>".
						 "function OpenFichePdf()".
						 "{".
						 "OpenWindow('fiche_etatme.php?".$strParam."&bPdf=1','600','800','fiche_me');".
						"}</script>".
						"<div class=\"popupTitle\">Fiche de la masse d'eau</div>".
	           "<div class=\"popupBtClose\"><div onclick=\"javascript:top.closeWindow('')\" class=\"btClose\"/></div></div>";
	
	
	$strHtml .= "<div class='txt colon' style='position: relative; top:10px; width: 720px;  margin-left: auto; margin-right: auto; '>".
							"<h1 style='float: left; width: 480px;height:80px;'>
							<br/>Masse d'eau ".($drMe->getValueName("MASSE_TYPE") == "MEC" ? "c�ti�re" : "de transition")."&nbsp;".$drMe->getValueName("CODE")."<br/>".$drMe->getValueName("NOM")."</h1>".
							"<img id='logoAE' src='../../media/imgs/gen/".$drMe->getValueName("BASSIN_CODE")."_logo_agence_de_leau.jpg' style='float:right;margin-left:10px;margin-top: 0px;'>".
							"<img id='logoIfremer' src='../../media/imgs/gen/Ifremer_logo.gif' width='110' style='float:right;margin-top: 20px;'>".
							"<div style='clear:both'></div>".
							"<div id='descFicheMe'>".$strHtmlNomMe."</div>".
							"<div id='vignetteFicheMe'><img src='/upload/carte/imgms/vignette/".$drMe->getValueName("CODE").".png' bgcolor='#ffffff' width='110px' align='right'/></div>".
							"<div id='descSuiviFicheMe'>".	
							($drMe->getValueName("MASSE_B_SUIVI") == "1" ? "<div id='bSuiviFicheMe'>Masse d'eau suivie au titre du programme de surveillance de la DCE 2000/60/CE</div>" 
																														: "<div id='bSuiviFicheMe'>Masse d'eau non suivie au titre du programme de surveillance de la DCE 2000/60/CE</div>").
							($drMe->getValueName("MASSE_B_RISQUE") == "1" ? "<div id='bRisqueFicheMe'>Masse d'eau class�e en Risque de non Respect des Objectifs Environnementaux dans l'�tat des lieux de 2004</div>" 
																														: "<div id='bRisqueFicheMe'>Masse d'eau Class�e en Respect des Objectifs Environnementaux dans l'�tat des lieux de 2004</div>").
							"</div><div style='clear:both;'></div>".														
							"<div id='listeEtatFicheMe'>".$strHtmlListeEtat."</div>".
							"<div id='legendeEtat'>".$oMasseEau->getLegende()."</div>".
							"<div id='legendeMotif'>".$oMasseEau->getLegendeMotif()."</div>".
							"<div id='legendeMotif' style='clear:both;width:700px;margin-left:0;'>".$oMasseEau->getLegendeReseaux()."</div>".
							"<div style='clear:both'></div>".
							"<div id='carteFicheMe'><img src='/upload/carte/imgms/carte/".$drMe->getValueName("CODE").".png' width='240' bgcolor='#ffffff' align='right'/></div>".													
							"<div id='liensFicheMe'>
								<ul>
								<li>".($bPdf == 1 ? "" : "<a href='javascript:OpenFichePdf();'>G�n�rer le PDF</a>")."</li>								
								</ul>
								</div>".
							"<div style='clear:both'></div>".
						"</div>";
} else 
	$strHtml = "<div class='txt colon' style='position: absolute; width: 720px;'>Masse d'eau non identifi�e</div>";

if ($bPdf == 1) {
	$strHtml = str_replace(ALK_ROOT_URL, ALK_ROOT_PDF_URL, getHtmlHeader()).
			"</head>" .
			"<body text=\"#000000\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">".$strHtml.getHtmlFooter();
			
	$strFile = getPdf2($strHtml, "", "", $codeMe);	
} else {
	aff_menu_haut($tabEventBody);
	echo $strHtml;
	aff_menu_bas();
}	


?>