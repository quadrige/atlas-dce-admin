<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu.php");
include_once("lib/lib_aff.php");

$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$pt_reseau_id = Request("pt_reseau_id", REQ_POST, "-1");
$pt_masse_id = Request("pt_masse_id", REQ_POST, "-1", "is_numeric");
$pt_point_id = Request("pt_point_id", REQ_POST, "-1");
$reseau_id = Request("reseau_id", REQ_POST, "-1");

$strParam = "cont_appli_id=6&cont_id=".$cont_id;

$menu_id=0;
$iAcces = 0;

/*$oCtrlSelectReseau = new HtmlSelect($iAcces, "pt_reseau_id", $pt_reseau_id, "R�seau *");
$oCtrlSelectReseau->oValTxt = $queryAtlas->getReseauForCombo();
$oCtrlSelectReseau->tabValTxtDefault = array ("-1", "--- S�lectionner un r�seau ---");

$oCtrlSelectMasse = new HtmlSelect($iAcces, "pt_masse_id", $pt_masse_id, "Masse d'eau *");
$oCtrlSelectMasse->oValTxt = $queryAtlas->getMasseForCombo($pt_reseau_id);
$oCtrlSelectMasse->addEvent("onchange", "majPoint()");
$oCtrlSelectMasse->tabValTxtDefault = array ("-1", "--- S�lectionner une masse d'eau ---");

$oCtrlSelectPoint = new HtmlSelect($iAcces, "pt_point_id", $pt_point_id, "Point *");
$oCtrlSelectPoint->oValTxt = $queryAtlas->getPointForCombo($pt_reseau_id, $pt_masse_id);
$oCtrlSelectPoint->tabValTxtDefault = array ("-1", "--- S�lectionner un point ---");

$oCtrlSelect2 = new HtmlSelect($iAcces, "reseau_id", $reseau_id, "R�seau *");
$oCtrlSelect2->oValTxt = $queryAtlas->getReseauForCombo();
$oCtrlSelect2->addEvent("onchange", "Validation('2')");
$oCtrlSelect2->tabValTxtDefault = array ("-1", "--- S�lectionner un r�seau ---");

<!--<tr class="trPair1">
			<td class=txt valign=top>
				<table width=100% cellpadding=2 cellspacing=2>
					<tr class="trPair1">
						<td class=txt ><font class=txt>R�seau</font></td><td class=formCtrl><?=$oCtrlSelectReseau->getHtml()?></td>
					</tr>
					<tr class="trPair1">
						<td class=txt >Masse d'eau</td><td class=formCtrl><?=$oCtrlSelectMasse->getHtml()?></td>
					</tr>
					<tr class="trPair1">
						<td class=txt >Point</td><td class=formCtrl><?=$oCtrlSelectPoint->getHtml()?></td>
					</tr>
					<tr class="trPair1">
						<td height=35  class="navig-parents" align=center valign=bottom colspan=2><a href="javascript:Validation(1);" class="formCtrl">Voir la fiche</a></td>
					</tr>
				</table>
			</td>				
	  	<td class=txt valign=top>
	  		<table width=100% cellpadding=2 cellspacing=2>
					<tr class="trPair1">
						<td class=txt>R�seau</td><td class=formCtrl><?=$oCtrlSelect2->getHtml()?></td>
					</tr>					
					<tr class="trPair1">					
						<td height=90 align=center valign=bottom colspan=2><a href="<?=($reseau_id == -1 ? "javascript:Validation(2);" : $strUrlFileName)?>" class="formCtrl">Voir la carte</a></td>
					</tr>
				</table>
			</td>	
		</tr>-->
*/

$strUrlFileName = ALK_SIALKE_URL.ALK_PATH_UPLOAD_CARTE."atlas_dce.exe";

aff_menu_haut(true);
aff_menu_gauche($menu_id);
?>
<style type="text/css">
.tdPair1 {
	border:1px solid #D9D9D9;
	}
	
	body {
		background-color:#ffffff !important;
	}
	
</style>
<script language='javascript' src='../../lib/lib_formNumber.js'></script>
<script language='javascript' src='../../lib/lib_form.js'></script>
<script language="javascript">
var AlkNbClickValid = 0;

function majPoint()
{
  var f = document.atlas;
  f.submit();     
}

function Validation(iMode)
{
  var f = document.atlas;
  
  if ((iMode == '1' && f.pt_reseau_id.value == '-1') || (iMode == '2' && f.reseau_id.value == '-1')) {
  	alert("Le choix d'un r�seau est obligatoire.");
  	return;	
  }
  
  if (iMode == '1' && f.pt_point_id.value == '-1') {
  	alert("Le choix d'un point est obligatoire.");
  	return;	
  }
  
  if (iMode == '1') {
  	if( AlkNbClickValid == 0 ) {
   	 OpenWindow('point_fiche.php?id='+f.pt_point_id.value+'&reseau='+f.pt_reseau_id.value, '400', '800', 'point_fiche');
  	}  	
  } else {
  	if( AlkNbClickValid == 0 ) {
   	 var bRes = AlkVerifCtrl(f);
     if( bRes == true ) {
       AlkNbClickValid++;
       f.submit();
    	}  
  	}  	
  }      
}
</script>
<form name='atlas' action='01_accueil.php' method='post'>     
	<table width=80% cellpadding=0 cellspacing=0 border=0>
		<tr>
			
			<!--
			<td rowspan=2 width=15></td>
			<td rowspan=2 width=1 bgcolor=#3434aa><img src="<?=ALK_URL_SI_MEDIA?>imgs/gen/transp.gif" witdh=1></td>
			<td rowspan=2 width=15></td>			
	  	<td class=txt>
	  		<h3>Recherche par r�seau</h3>
			</td>
			-->	
		</tr>
		<tr><td class=txt><div width="400px;" align=justify>
<h3>Evaluation de la qualit� des masses d�eau dans le cadre du 
programme de surveillance de la DCE 2000/60/CE </h3><br/><br/>

<div style="float:left; width:370px;padding:10px;">
La Directive Cadre europ�enne sur l�Eau est un texte de droit communautaire c'est-�-dire 
qui s�applique � l�ensemble des membres de l�union Europ�enne. Elle �tablit un cadre pour 
une politique communautaire de l�eau en vue d�une meilleure gestion des milieux 
aquatiques. Les objectifs sont notamment de pr�venir toute d�gradation suppl�mentaire, de 
pr�server et am�liorer l��tat des �cosyst�mes aquatiques. <br/><br/> 

Au titre de cette directive (article 8 de la DCE), les Etats membres doivent mettre � en 
oeuvre les mesures n�cessaires pour pr�venir la d�t�rioration de l��tat de toutes les masses 
d�eau de surface � et doivent prot�ger, am�liorer et restaurer si n�cessaire toutes les 
masses d�eau de surface. Pour se faire les Etats membres ont mis en place un programme 
de surveillance � afin de dresser un tableau coh�rent et complet de l��tat des masses d�eau 
au sein de chaque district hydrographique �.  <br/><br/>

Un des volets du programme de surveillance est le contr�le de surveillance qui permet 
d��valuer l��tat chimique et �cologique des masses d�eau. 
A l��chelle du bassin hydrographique (lien vers le glossaire � cr�er en ligne) l��valuation s�effectue 
sur un nombre consid�r� comme suffisant de masses d�eau (lien vers le glossaire � cr�er en ligne) 
pour �valuer la qualit� du bassin concern�. Au niveau de la masse d�eau, la qualit� est 
�valu�e gr�ce � une batterie d� �l�ments de qualit� (lien vers le glossaire � cr�er en ligne) et 
param�tres soutenant la biologie (tableau 1). Les donn�es sont acquises sur une p�riode de 
6 ans qui est l��quivalent d�un plan de gestion. A partir de ces donn�es, des m�thodes de 
calcul permettent d�obtenir les indices (lien vers le glossaire � cr�er en ligne) dont la combinaison 
am�ne � un indicateur (lien vers le glossaire � cr�er en ligne) qui permet l��valuation des �l�ments 
de qualit� et param�tres soutenant la biologie. 
L�indicateur obtenue peut-�tre ensuite transform�e en un Ratio de Qualit� Ecologique 
(RQE) (lien vers le glossaire � cr�er en ligne) , qui est le rapport entre la valeur calcul�e et la valeur 
de r�f�rence (lien vers le glossaire � cr�er en ligne). Le r�sultat, compris entre 0 et 1, est compar� � 
une grille de r�f�rence d�finissant les fronti�res entre les �tats pour le classement de la 
masse d�eau (Cf. figure ci-dessous).  </div>
<div style="float:left;width:200px;padding:10px;">
	<div style="">
		<h2>Bassin Loire-Bretagne</h2><br/>
		<a href="#" onclick="OpenWindow('carte.php?map=LB', '800', '1000', 'carte', 'no', 'no') ;">Voir la carte</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatme.php?bassin_id=LB', '800', '1000', 'carte', 'no', 'no') ;">Voir la synth�se des classements des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatmedetail.php?bassin_id=LB', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste d�taill�e des classements des masses d'eau</a><br/>
		<a href="#" onclick="OpenWindow('liste_etatmet_mec.php?bassin_id=LB', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etat_reseau.php?bassin_id=LB', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des r�seaux</a><br/><br/>
	</div>
	<div style="">
		<h2>Bassin Adour-Garonne</h2><br/>
		<a href="#" onclick="OpenWindow('carte.php?map=AG', '800', '1000', 'carte', 'no', 'no') ;">Voir la carte</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatme.php?bassin_id=AG', '800', '1000', 'carte', 'no', 'no') ;">Voir la synth�se des classements des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatmedetail.php?bassin_id=AG', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste d�taill�e des classements des masses d'eau</a><br/>
		<a href="#" onclick="OpenWindow('liste_etatmet_mec.php?bassin_id=AG', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etat_reseau.php?bassin_id=AG', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des r�seaux</a><br/><br/>
	</div>
	<div style="">
		<h2>Bassin Seine-Normandie</h2><br/>
		<a href="#" onclick="OpenWindow('carte.php?map=SN', '800', '1000', 'carte', 'no', 'no') ;">Voir la carte</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatme.php?bassin_id=SN', '800', '1000', 'carte', 'no', 'no') ;">Voir la synth�se des classements des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatmedetail.php?bassin_id=SN', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste d�taill�e des classements des masses d'eau</a><br/>
		<a href="#" onclick="OpenWindow('liste_etatmet_mec.php?bassin_id=SN', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etat_reseau.php?bassin_id=SN', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des r�seaux</a><br/><br/>
	</div>
	<div style="">
		<h2>Bassin Artois-Picardie</h2><br/>
		<a href="#" onclick="OpenWindow('carte.php?map=AP', '800', '1000', 'carte', 'no', 'no') ;">Voir la carte</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatme.php?bassin_id=AP', '800', '1000', 'carte', 'no', 'no') ;">Voir la synth�se des classements des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatmedetail.php?bassin_id=AP', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste d�taill�e des classements des masses d'eau</a><br/>
		<a href="#" onclick="OpenWindow('liste_etatmet_mec.php?bassin_id=AP', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etat_reseau.php?bassin_id=AP', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des r�seaux</a><br/><br/>
	</div>
	<div style="">
		<h2>Bassin Rh�ne et c�tiers m�diterran�ens</h2><br/>
		<a href="#" onclick="OpenWindow('carte.php?map=RMC', '800', '1000', 'carte', 'no', 'no') ;">Voir la carte</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatme.php?bassin_id=RMC', '800', '1000', 'carte', 'no', 'no') ;">Voir la synth�se des classements des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatmedetail.php?bassin_id=RMC', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste d�taill�e des classements des masses d'eau</a><br/>
		<a href="#" onclick="OpenWindow('liste_etatmet_mec.php?bassin_id=RMC', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etat_reseau.php?bassin_id=RMC', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des r�seaux</a><br/><br/>
	</div>
	<div style="">
		<h2>Bassin Corse</h2><br/>
		<a href="#" onclick="OpenWindow('carte.php?map=RMCC', '800', '1000', 'carte', 'no', 'no') ;">Voir la carte</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatme.php?bassin_id=RMCC', '800', '1000', 'carte', 'no', 'no') ;">Voir la synth�se des classements des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etatmedetail.php?bassin_id=RMCC', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste d�taill�e des classements des masses d'eau</a><br/>
		<a href="#" onclick="OpenWindow('liste_etatmet_mec.php?bassin_id=RMCC', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des masses d'eau</a><br/><br/>
		<a href="#" onclick="OpenWindow('liste_etat_reseau.php?bassin_id=RMCC', '800', '1000', 'carte', 'no', 'no') ;">Voir la liste des r�seaux</a><br/><br/>
	</div>
</div>

<br/><br/>
<div style="clear:both"></div>
<table class='table1' width="700"  cellpadding='0' cellspacing='0'>
<tr >
<td class='tdEntete1'  style="">
5 classes pour les indicateurs 
physico-chimiques et biologiques
</td><td class='tdEntete1' style="background-color:#009ebf;">TRES BON</td>
<td class='tdEntete1' style="background-color:#6ab396">BON</td>
<td class='tdEntete1' style="background-color:#ffe075;">MOYEN</td>
<td class='tdEntete1' style="background-color:#faa61a;">MEDIOCRE</td>
<td class='tdEntete1' style="background-color:#ed1c24;">MAUVAIS</td>
</tr>
<tr ><td class='tdEntete1' style="">2 classes pour les indicateurs 
chimiques</td>
<td class='tdEntete1' style="background-color:#6ab396">BON</td>
<td class='tdEntete1' style="background-color:#ed1c24;">MAUVAIS</td>
<td class="tdPair1"  ></td>
<td class="tdPair1"  ></td>
<td class="tdPair1"  ></td>
</tr>
<tr >
<td class='tdEntete1' style="">2 classes pour les indicateurs 
morphos�dimentaires</td>
<td class='tdEntete1' style="background-color:#009ebf;">TRES BON</td>
<td class='tdEntete1' style="background-color:#6ab396;">BON </td>
<td class="tdPair1"  ></td>
<td class="tdPair1"  ></td>
<td class="tdPair1"  ></td>
</tr>
<tr >
<td class='tdEntete1' style="">Lorsque le nombre de donn�es est 
insuffisant alors on appose un �tat 
inconnu</td><td class='tdEntete1' style="background-color:#c2c2c2;">INCONNU</td>
<td class="tdPair1"  ></td>
<td class="tdPair1"  ></td>
<td class="tdPair1"  ></td>
<td class="tdPair1"  ></td>
</tr>
</table>
  
 
 <br/><br/>
Tableau 1. Les �l�ments permettant l��valuation de la qualit� des masses d�eau dans le 
cadre du programme de surveillance.  <br/><br/>

<table class='table1' width="700" cellpadding='0' cellspacing='0'>
<tr class='trEntete1' >
<td class='tdEntete1'  style="background-color:#666666;color:#ffffff"></td>
<td class='tdEntete1'  style="background-color:#666666;color:#ffffff"></td>
<td class='tdEntete1'  style="background-color:#666666;color:#ffffff">Eaux c�ti�res</td>
<td class='tdEntete1'  style="background-color:#666666;color:#ffffff">Eaux de transition
</td>
</tr>
<tr class="trPair1">
<td class='tdEntete1'  style="background-color:#666666;color:#ffffff">Etat
chimique</td>
<td  class='tdEntete1' style="background-color:#666666;color:#ffffff">El�ments de qualit�
chimiques
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">41 substances suivies dans l�eau<br/>
(8) prioritaires dangereuses<br/>
(33) prioritaires
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">41 substances suivies dans l�eau<br/>
(8) prioritaires dangereuses<br/>
(33) prioritaires
</td>
</tr>
<tr class="trPair1">
<td  class='tdEntete1' rowspan=11 style="background-color:#666666;color:#ffffff">Etat
Ecologique</td>
<td  class='tdEntete1' rowspan=5 style="background-color:#666666;color:#ffffff">El�ments de qualit�
biologiques
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Phytoplancton
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Phytoplancton
</td>
</tr>
<tr class="trPair1">
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Macroalgues (macroalgues intertidales
et subtidales)

</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Macroalgues (macroalgues
intertidales et subtidales)
</td>
</tr>
<tr class="trPair1">
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Angiospermes (Zostera noltii et Zostera
marina)

</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Angiospermes (Zostera noltii et Zostera
marina)
</td>
</tr><tr class="trPair1">
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Invert�br�s benthiques
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Invert�br�s benthiques
</td>
</tr><tr class="trPair1">
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">

</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Poissons
</td>
</tr>
<tr class="trPair1">
<td  class='tdEntete1' rowspan=5 style="background-color:#666666;color:#ffffff">Param�tres physicochimiques
soutenant la
biologie
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Temp�rature
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Temp�rature
</td>
</tr>
<tr class="trPair1"><td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Oxyg�ne dissous
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Oxyg�ne dissous
</td>
</tr>
<tr class="trPair1"><td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Nutriments
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Nutriments
</td>
</tr>
<tr class="trPair1"><td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Salinit�
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Salinit�
</td>
</tr>
<tr class="trPair1"><td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Turbidit�
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Turbidit�
</td>
</tr>
<tr class="trPair1">
<td  class='tdEntete1' rowspan=1 style="background-color:#666666;color:#ffffff">Hydromorphologie
</td>
<td class="tdPair1" style="background-color:#c2c2c2;color:#000000">Hydromorphologie
</td>
<td class="tdPair1"  style="background-color:#c2c2c2;color:#000000">Hydromorphologie
</td>
</tr>
</table>

		<br><br>
				
		<br>
		</div>
		</td></tr>		
	</table>
<?
aff_menu_droite();
aff_fin_menu_gauche();
aff_menu_bas();
?>