<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("lib/lib_pagination.php");
include_once("classes/alkmasseeau.class.php");

include_once("../../classes/html2pdf/html2fpdf.php");
include_once("../../classes/html2pdf/alkhtml2pdf.class.php");

$tabEventBody["onLoad"] ="if(top.HideLoadMsg) top.HideLoadMsg();";

$bPdf = Request("bPdf", REQ_GET, "0", "is_numeric");
$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$codeMe = Request("code", REQ_GET, "");
$idElementQualite = Request("qualite_id", REQ_GET, "");
    
$menu_id=0;
$iAcces = 0;
$strParam = "code=".$codeMe."&qualite_id=".$idElementQualite;

$oMasseEau = new AlkMasseEau($codeMe, $queryAtlas);
$strHtml = $strHtmlNomMe = "";

$tabPage = array();
$dsMe = $queryAtlas->getDs_MasseEauById("", $codeMe);
if ($drMe = $dsMe->getRowIter()) {
	$idMe = $drMe->getValueName("MASSE_ID");	
	$strNomMe = $drMe->getValueName("LIB");
	$strTypeMe = $drMe->getValueName("MASSE_TYPE");
	$strSuiviMe = $drMe->getValueName("MASSE_TYPE_SUIVI");
	$strAlert = $drMe->getValueName("BASSIN_ALERT");
	
	$strHtmlNomMe = "<table class='txt' cellpadding='0' cellspacing='0' style='width:380px;border:1px solid #c6c6c6;height:110px;'>".
									 "<tr><td width='150'><b>D�partement(s)</b></td><td>".$drMe->getValueName("DEPT")."</td></tr>".
									 //"<tr><td><b>R�gion marine</b></td><td>".$drMe->getValueName("REGION_LIBELLE")."</td></tr>".
									 "<tr><td><b>Bassin Hydrographique</b></td><td>".$drMe->getValueName("BASSIN_NOM")."</td></tr>".
									 "<tr><td><b>Type</b></td><td>".$drMe->getValueName("DESCR")."</td></tr></table>";

	$strNom = $strColor = $strEtat = $strStatut = $strDate = "";
	$strBilan = $docProtocole = $docMethodo = $docRef = $docDetResult = $urlSynthese = $idTypeClassement = "";
	$tabClassement = $oMasseEau->getEtatQualite();		
	foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {
		foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){			
			if (array_key_exists("ELEMENTS", $tabEtat) && array_key_exists("ID_".$idElementQualite, $tabEtat["ELEMENTS"]["BYID"])){	
				$strNom		 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["NOM"];
				$strColor	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["COULEUR"];
				$strStatut = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["STATUT"];
				$strEtat	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["ETAT"];
				$strDate	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["DATE"];
				$strBilan	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["BILAN"];
				$docDetResult	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["COMPLEMENT_BILAN"];
				$docRef	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["DOC_REF"];
				$idTypeClassement = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["TYPE_CLASSEMENT_ID"];
			} 			
		}					
	}
	
	if ($idTypeClassement == "")
		$strHtml = "<div class='txt colon' style='position: absolute; width: 720px;'>El�ment de qualit� non pertinent pour cette masse d'eau</div>";				
	else {
		$dsElement = $queryAtlas->getDs_ElementQualiteById($idElementQualite);
		if ($drElement = $dsElement->getRowIter()){
			$docProtocole = $drElement->getValueName("ELEMENT_QUALITE_DOC_PROTOCOLE");
			$docMethodo = $drElement->getValueName("ELEMENT_QUALITE_DOC_METHODO");
			$urlSynthese = $drElement->getValueName("ELEMENT_QUALITE_URL_FICHE");
		}
		
		$strHtml = "<script language=javascript>".
							 "function OpenFichePdf()".
							 "{".
							 "OpenWindow('fiche_etatmequal.php?".$strParam."&bPdf=1','600','800','fiche_me');".
							"}</script>".
							"<div class=\"popupTitle\">Fiche de la masse d'eau</div>".
							"<div class=\"popupBtClose\"><div onclick=\"javascript:top.closeWindow('')\" class=\"btClose\"/></div></div>";
			
		//TODO : remplacer le chemin vers path pour urlSynthese car il s'agira d'une url
		$strHtml .= "<div class='txt colon' style='position: relative; top:10px; width: 720px;  margin-left: auto; margin-right: auto; '>".
								"<h1 style='float: left; width: 480px;height:80px;'>
								<br/>Masse d'eau ".($drMe->getValueName("MASSE_TYPE") == "MEC" ? "c�ti�re" : "de transition")."&nbsp;".$drMe->getValueName("CODE")."<br/>".$drMe->getValueName("NOM")."</h1>".
								"<img id='logoAE' src='../../media/imgs/gen/".$drMe->getValueName("BASSIN_CODE")."_logo_agence_de_leau.jpg' style='float:right;margin-left:10px;margin-top: 0px;'>".
								"<img id='logoIfremer' src='../../media/imgs/gen/Ifremer_logo.gif' width='110' style='float:right;margin-top: 20px;'>".
								"<div style='clear:both'></div>".
								"<div id='descFicheMe'>".$strHtmlNomMe."</div>".
								"<div id='vignetteFicheMe'><img src='/upload/carte/imgms/vignette/".$drMe->getValueName("CODE").".png' bgcolor='#ffffff' width='110px' align='right'/></div>".
								"<div id='descSuiviFicheMe'>".	
								($drMe->getValueName("MASSE_B_SUIVI") == "1" ? "<div id='bSuiviFicheMe'>Masse d'eau suivie au titre du programme de surveillance de la DCE 2000/60/CE</div>" 
																														 : "<div id='bSuiviFicheMe'>Masse d'eau non suivie au titre du programme de surveillance de la DCE 2000/60/CE</div>").
								($drMe->getValueName("MASSE_B_RISQUE") == "1" ? "<div id='bRisqueFicheMe'>Masse d'eau class�e en Risque de non Respect des Objectifs Environnementaux dans l'�tat des lieux de 2004</div>" 
																															: "<div id='bRisqueFicheMe'>Masse d'eau Class�e en Respect des Objectifs Environnementaux dans l'�tat des lieux de 2004</div>").
								"</div><div style='clear:both;'></div>".
								"<div id='listeEtatFicheMe'>".
								"<table class='table1 listeEtat' cellpadding='0' cellspacing='0'>
									<tr class='trEntete1' >
									<td class='tdEntete1' style='background-color:#ffffff;color:#000000;font-size:9pt;width:600px;' align=center>Qualit� des ".$strNom." en date du ".$strDate."<br/>
									<font style='color:#9a3939;font-style:italic;font-size:11px;'>$strAlert</font></td>
									<td class='tdEntete1 tdEtat' style='background:".$strColor." url(../../media/imgs/gen/pictos/fond_rond.gif) center; text-align:right;'></td>
									</tr></table></div>".																								
								"<div id='listeEtatFicheMe' style='border:1px solid #c6c6c6;width:330px;padding:5px;height:320px;'>
									<h3>Explications sur l'�valuation</h3><br/>".
									($strBilan == "" ? "Aucune explication d�taill�e relative � ce classement n'est disponible pour le moment.<br/>" : $strBilan).
									"<div style='font-size:7pt;margin-top:220px;'>Les liens ci-dessous permettent d�acc�der � des informations compl�mentaires sur l��l�ment de qualit�, le calcul de l�indicateur et les r�sultats.</div>".
								"</div>".
								"<div id='carteFicheMeQual'><img src='/upload/carte/imgms/carte/".$drMe->getValueName("CODE").".png' width='190' bgcolor='#ffffff' align='right'/></div>".													
								"<div id='legendeEtat' style='width: 115px; margin-top: 0px; margin-right: 10px; margin-left: 10px;'>".$oMasseEau->getLegende($idTypeClassement)."</div>".
								"<div style='clear:both'></div>".	
									($bPdf == 1 ? "" : 							
								"<div id='liensFicheMe'>
									<ul>".
									($docProtocole != "" ? "<li><a href='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$docProtocole."' target='_blank'>Protocole d'�chantillonage</a></li>" : "").
									($docMethodo != "" ? "<li><a href='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$docMethodo."' target='_blank'>Calcul de l'indicateur</a></li>" : "").
									($urlSynthese != "" ? "<li><a href='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$urlSynthese."' target='_blank'>Evaluation de la qualit� des masses d'eau : $strNom</a></li>" : "").
									($docDetResult != "" ? "<li style='width:120px;'><a href='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$docDetResult."' target='_blank'>$strNom : r�sultats pour la masse d'eau ".$codeMe."</a></li>" : "").
									//($docRef != "" ? "<li><a href='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$docRef."' target='_blank'>Rapport des r�sultats</a></li>" : "").
									"<li><a href='javascript:OpenFichePdf();'>G�n�rer le PDF</a></li>
									</ul></div>").
							"</div>";
		}
} else 
	$strHtml = "<div class='txt colon' style='position: absolute; width: 720px;'>Masse d'eau non identifi�e</div>";

if ($bPdf == 1) {
	$strHtml = str_replace(ALK_ROOT_URL, ALK_ROOT_PDF_URL, getHtmlHeader()).
			"</head>" .
			"<body text=\"#000000\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">".$strHtml.getHtmlFooter();
			
	$strFile = getPdf2($strHtml, "", "", $codeMe);	
} else {
	aff_menu_haut($tabEventBody);
	echo $strHtml;
	aff_menu_bas();
}		

?>