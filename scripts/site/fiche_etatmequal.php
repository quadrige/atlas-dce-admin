<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("lib/lib_pagination.php");
include_once("classes/alkmasseeau.class.php");
include_once("lib/lib_pdf.php");


$tabEventBody["onLoad"] ="if(top.HideLoadMsg) top.HideLoadMsg();";

$bPdf = Request("bPdf", REQ_GET, "0", "is_numeric");
$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$codeMe = Request("code", REQ_GET, "");
$strElementQualite = utf8_decode(Request("element", REQ_GET, ""));
$idElementQualite = Request("qualite_id", REQ_GET, "");
$idSsElementQualite = Request("subqualite_id", REQ_GET, "");
$session_id = Request("idSession", REQ_GET, "-1", "is_numeric");

$strDirArchive = ($session_id != -1 ? "/archive_".$session_id : "");
$bAff = 1;

$menu_id=0;
$iAcces = 0;
$strParam = "code=".$codeMe."&qualite_id=".$idElementQualite."&idSession=".$session_id;

$oMasseEau = new AlkMasseEau($codeMe, $queryAtlas, "", $session_id);
$strHtml = $strHtmlNomMe = "";
$idMe = -1;
$tabPage = array();
$dsMe = $queryAtlas->getDs_MasseEauById("", $codeMe, $session_id);
if ($drMe = $dsMe->getRowIter()) {
	$idMe = $drMe->getValueName("ID");
	$strNomMe = $drMe->getValueName("LIB");
	$strTypeMe = $drMe->getValueName("MASSE_TYPE");
	$strSuiviMe = $drMe->getValueName("MASSE_TYPE_SUIVI");
	$strAlert = $drMe->getValueName("BASSIN_ALERT");
  $bassin_id = $drMe->getValueName("BASSIN_ID");
  $codeBassin = $drMe->getValueName("BASSIN_CODE");
  $lienFicheContexteME = $drMe->getValueName("MASSE_DOC_DESC");
	$lienFicheDescME = $drMe->getValueName("MASSE_DOC_FICHE");
	$me_commentaire = $drMe->getValueName("MASSE_COMMENTAIRE");

  $strMotifDerogation = $drMe->getValueName("MASSE_MOTIF_DEROGATION");
  $tabMotif = array();
  if ($strMotifDerogation != ""){
    $tabMotif = explode(";",$strMotifDerogation);
    $listMotif = array("CN" => "Conditions naturelles",
          								"FT" => "Faisabilit� technique",
          								"CD" => "Co�ts disproportionn�s");
    $tabMotif2 = array();
    foreach($tabMotif as $key){
      array_push($tabMotif2,$listMotif[$key]);
    }
  	$strMotifDerogation = implode(",", $tabMotif2);
  }

	$strHtmlGpReseau = "";

  if ($strElementQualite != "" && $idElementQualite == ''){
    //Cas de la r��criture d'URL => retrouver l'identifiant de l'�l�ment de qualit� par son nom
    $dsElement = $queryAtlas->getDs_ElementQualiteById("", $strElementQualite, "", $bassin_id);
    if ($drElement = $dsElement->getRowIter()){
      $idElementQualite = $drElement->getValueName("ELEMENT_QUALITE_ID");
      $strParam = "code=".$codeMe."&qualite_id=".$idElementQualite."&idSession=".$session_id;
    } else {
      $bAff = 0;
    }
  }

	$dsGpReseau = $queryAtlas->getDs_listeGroupeReseauByMe($idMe, $session_id);
	while ($drGpReseau = $dsGpReseau->getRowIter()) {
	  $strHtmlGpReseau .= "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>".
	                      ($drGpReseau->getValueName("NB_POINT")>0
	                       ? "<a onmouseover=\"MM_showHideLayers('gp_reso_".$drGpReseau->getValueName("GROUPE_ID")."', '', 'show');\"
  											  onmouseout=\"MM_showHideLayers('gp_reso_".$drGpReseau->getValueName("GROUPE_ID")."', '', 'hide');\">".
  											  $drGpReseau->getValueName("GROUPE_NOM")."</a>"
  											: $drGpReseau->getValueName("GROUPE_NOM")).
	                      "</span><span>".($drGpReseau->getValueName("NB_POINT")>0 ? "Oui" : "Non")."</span></p>".
  											( $drGpReseau->getValueName("NB_POINT")>0
  											  ? "<div id='gp_reso_".$drGpReseau->getValueName("GROUPE_ID")."' class='detaillisteme stylepopup2 gp_reso' onmouseout=\"MM_showHideLayers('gp_reso_".$drGpReseau->getValueName("GROUPE_ID")."', '', 'hide');\">".
  	                        $drGpReseau->getValueName("LIST_RESO").
  											 		"</div>"
  	                      : "");
	}

    // tab de correspondance objectifs envrionnementaux
  $tabCorresp = array();
  $ds = $queryAtlas->getTableForCombo("ATTEINTE_OBJECTIF_DESCRIPTION", "ATTEINTE_OBJECTIF", true, false, "");
  while($dr = $ds->getRowIter()){
        $tabCorresp[$dr->getValueName("ATTEINTE_OBJECTIF_ID")] = $dr->getValueName("nom");
  }


  $strHtmlNomMe = "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Bassin Hydrographique</span><span>".$drMe->getValueName("BASSIN_NOM")."</span></p>
                  <p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>D�partement(s)</span><span>".$drMe->getValueName("DEPT")."</span></p>
	 			  				<p ".($bPdf != 1 ? "class='clearfix type'" : "class='type clearfix_pdf'")."><span class='item'>Type</span><span>".$drMe->getValueName("DESCR")."</span></p>".
                  ($drMe->getValueName("MASSE_B_FORT_MODIF")!=""
                    ? "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Masse d'eau fortement modifi�e</span><span>".( $drMe->getValueName("MASSE_B_FORT_MODIF")=="1" ? "Oui" : "Non")."</span></p>"
                    : "").
                  ($drMe->getValueName("MASSE_B_ATTEINTE_OBJ")!=""
                    ? "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Objectifs environnementaux</span><span>".$tabCorresp[$drMe->getValueName("MASSE_B_ATTEINTE_OBJ")]."</span></p>"
                    : "").
                    (is_array($tabMotif) && count($tabMotif)>0
                    ? "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Motif(s) d�rogation</span><span>".$strMotifDerogation."</span></p>"
                    : "").
                    ($drMe->getValueName("MASSE_B_SUIVI") != ""
                    ? "<p ".($bPdf != 1 ? "class='clearfix'" : "class='clearfix_pdf'")."><span class='item'>Suivie au titre du programme de surveillance de la DCE 2000/60/CE</span><span>".($drMe->getValueName("MASSE_B_SUIVI") == "1" ? "Oui" : "Non")."</span></p>"
                    : "").
                    "<br>".$strHtmlGpReseau.
                    ($lienFicheContexteME != "" && $bPdf != 1 ? "<p style=\"text-align:right;\"><a href=\"".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$codeBassin.$strDirArchive."/".$lienFicheContexteME."\" target=\"_blank\">Contexte physique de la masse d'eau</a></p>" : "").
                    ($lienFicheDescME != "" && $bPdf != 1 ? "<p style=\"text-align:right;\"><a href=\"".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$codeBassin.$strDirArchive."/".$lienFicheDescME."\" target=\"_blank\">Fiche de la masse d'eau</a></p>" : "");

	$strNom = $strColor = $strEtat = $strStatut = $strDate = $ic = "";
	$strBilan = $docProtocole = $docMethodo = $docRef = $docDetResult = $urlSynthese = $idTypeClassement = "";

	$tabClassement = $oMasseEau->getEtatQualite();
	foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {
		foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
		  if ($idSsElementQualite != "") {
		    if (array_key_exists("ELEMENTS", $tabEtat) && array_key_exists("ID_".$idSsElementQualite, $tabEtat["ELEMENTS"]["BYID"])){
  				$strNomSSElt		 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idSsElementQualite]["NOM"];
  				$strNom		 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["NOM"];
		    }
		  } else if ($idElementQualite != ""){
  		  if (array_key_exists("ELEMENTS", $tabEtat) && array_key_exists("ID_".$idElementQualite, $tabEtat["ELEMENTS"]["BYID"])){
    				$strNom		 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["NOM"];
    				$strColor	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["COULEUR"];
    				$strStatut = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["STATUT"];
    				$strEtat	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["ETAT"];
    				$strDate	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["DATE"];
    				$strBilan	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["BILAN"];
    				$docDetResult	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["COMPLEMENT_BILAN"];
    				$docRef	 = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["DOC_REF"];
    				$idTypeClassement = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["TYPE_CLASSEMENT_ID"];
    				$ic = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["INDICE_CONFIANCE"];
    				$bAff = $tabEtat["ELEMENTS"]["BYID"]["ID_".$idElementQualite]["B_AFF"];
  		  }
		  }
		}
	}

	if ($idTypeClassement == "" or $bAff==0){
	  $strTxt = "El�ment de qualit� non pertinent pour cette masse d'eau";
	  $strNom = "";
	  if ($idSsElementQualite != ""){
	    $strNomEltParent = $strNom;
	    $strNom = $strNomSSElt;
	    $strTxt = "Il n'existe pas de fiche sp�cifique d�crivant l'�tat de qualit� de cet �l�ment.<br/>".
	              "Pour plus d'informations, consultez la fiche de l'�l�ment <a href='".ALK_SIALKE_URL."scripts/site/fiche_etatmequal.php?".$strParam."' title='".$strNomEltParent."'>".$strNomEltParent."</a>";
	  }
		$strHtml = "<div class=\"popupTitle\">".$strNom." - R�sultats par masse d'eau".
							"<div class=\"popupBtClose\"><div onclick=\"javascript:top.closeWindow('')\" class=\"btClose\"/></div></div></div>".
							"<div id='conteneur'>".
		          $strTxt;
	}
	else {
		$dsElement = $queryAtlas->getDs_ElementQualiteById($idElementQualite);
		if ($drElement = $dsElement->getRowIter()){
			$docProtocole = $drElement->getValueName("ELEMENT_QUALITE_DOC_PROTOCOLE");
			$docMethodo = $drElement->getValueName("ELEMENT_QUALITE_DOC_METHODO");
			$urlSynthese = $drElement->getValueName("ELEMENT_QUALITE_URL_FICHE");
		}

		$strHtml = "<script language=javascript>".
							 "function OpenFichePdf()".
							 "{".
							 "OpenPopupWindow('".ALK_SIALKE_URL."scripts/site/fiche_etatmequal.php?".$strParam."&bPdf=1','600','800','fiche_me');".
               //"OpenFooterExec('".ALK_SIALKE_URL."scripts/site/fiche_etatmequal.php?".$strParam."&bPdf=1');".
							"}</script>".
	          	"<div class=\"popupTitle\">".$strNom." - R�sultats par masse d'eau".
	          	($bPdf == 1 ? "" :
  	          "<a class=\"imprim\" title=\"Imprimer\" href=\"javascript:OpenFichePdf();\">
              <img id=\"imprimer\" height='19' width='19' onmouseout='MM_swapImgRestore()' onmouseover=\"MM_swapImage('imprimer','','".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif' ,1)\" alt='Exporter en pdf' title='Exporter en pdf' src='".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif'/>
              </a>").
	          	"<div class=\"popupBtClose\"><div onclick=\"javascript:top.closeWindow('')\" class=\"btClose\"/></div></div></div>";

	$strHtml .= "<div id='conteneur'>";

	$strHtml .= "<div id='titreME' class='titreMEdetaille'>".
  							"<h1>
  							Masse d'eau ".($drMe->getValueName("MASSE_TYPE") == "MEC" ? "c�ti�re" : ($drMe->getValueName("MASSE_TYPE") == "MER" ? "r�cifale" : "de transition"))."&nbsp;".$drMe->getValueName("CODE")."<br/>".$drMe->getValueName("NOM")."</h1>".
  							"</div>".
		            "<div id='contenuME' class='txt colon classMEdetaille clearfix'>".
    		            "<div id='vignetteFicheMe' ><img src='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_CARTE.$drMe->getValueName("BASSIN_CODE")."/imgms/vignette/".$drMe->getValueName("CODE").".png'/></div>".
  									"<div id='descFicheMe'>".$strHtmlNomMe."</div>".
    								"<div id='listeEtatFicheMe' class='etatlegende clearfix'>".
											"<table class='table1 listeEtat' cellpadding='0' cellspacing='0'>
    									<tr class='trEntete1' >
    									<td class='tdEntete1 tdEntete1_detail_qualite_me'>
    									<h2>Qualit� de la masse d'eau : ".$strNom." ".($strDate !="" ? "(en date du ".$strDate.")" : "")."</h2>
    									<p class='styleAlertDetailme'>".$strAlert."</p>
    									<p class='styleAlertDetailme'>".$me_commentaire."</p></td>
    									<td class='tdEntete1 tdEtat popupBackg' style='background-color:".$strColor.";'>".
	                      ($ic != "" ? "<p id='pIndConfiance'>Degr� de fiabilit�<br/><span>".$ic."</span>&nbsp;".$oMasseEau->getLibelleIc($ic)."</p>" : "").
	                    "</td>".
    									"</tr></table>".
    	                $oMasseEau->getLegende($idTypeClassement).
	              "</div>".
								"<div id='listeEtatFicheMeQual'>
    									<h2>Explications sur l'�valuation</h2>".
    									($strBilan == "" ? "Aucune explication d�taill�e relative � ce classement n'est disponible pour le moment." : $strBilan).
								"</div>".
								($bPdf == 1 || ($docProtocole == "" && $docMethodo == "" && $urlSynthese == "" && $docDetResult == "") ? "" :
    								"<div id='liensFicheMe' class= 'classDivLienFicheMeQual'>
    									<p>Les liens ci-dessous permettent d'acc�der � des informations compl�mentaires sur l'�l�ment de qualit�".
    									($docMethodo != "" ? ", le calcul de l'indicateur" : "").
    									($docDetResult != "" ? " et les r�sultats" : "").
    									".</p>
									<ul>".
									($docProtocole != "" ? "<li><a href='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$drMe->getValueName("BASSIN_CODE").$strDirArchive."/".$docProtocole."' target='_blank'>Protocole d'�chantillonage</a></li>" : "").
									($docMethodo != "" ? "<li><a href='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$drMe->getValueName("BASSIN_CODE").$strDirArchive."/".$docMethodo."' target='_blank'>Calcul de l'indicateur</a></li>" : "").
									($urlSynthese != "" ? "<li><a href='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$drMe->getValueName("BASSIN_CODE").$strDirArchive."/".$urlSynthese."' target='_blank'>Evaluation de la qualit� des masses d'eau : $strNom</a></li>" : "").
									($docDetResult != "" ? "<li class='classLiLienFicheMeQual_120'><a href='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$drMe->getValueName("BASSIN_CODE").$strDirArchive."/".$docDetResult."' target='_blank'>$strNom : r�sultats pour la masse d'eau ".$codeMe."</a></li>" : "").
									//($docRef != "" ? "<li><a href='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$docRef."' target='_blank'>Rapport des r�sultats</a></li>" : "").
									"</ul></div>");

	if ( !in_array( $drMe->getValueName( "BASSIN_CODE"  ), array( 'MAY', 'LB' ) ) ) {
				$strHtml .= "<div id='carteFicheMeQual'><img class='alignright' src='".ALK_SIALKE_URL.ALK_PATH_UPLOAD_CARTE.$drMe->getValueName("BASSIN_CODE")."/imgms/carte/".$drMe->getValueName("CODE").".png' />".
        $oMasseEau->getLegendeReseaux(1, $bassin_id, 1);
        "</div>";
      }

	$strHtml .= "</div>";
		}

  // recherche de la derniere maj en rapport avec le classement de la me
  if($idElementQualite != "" && $idMe!=-1){
    $dsDateMajClassementME = $queryAtlas->getDs_DateMAJByMasseQualiteId($idElementQualite, $idMe, $session_id);

    if($drDateMajClassementME = $dsDateMajClassementME->getRowIter()){
      if($drDateMajClassementME->getValueName("CLASSEMENT_ME_DATE_MAJ") !=null)
      $strHtml .= "<div >Derni�re mise � jour : ".$drDateMajClassementME->getValueName("CLASSEMENT_ME_DATE_MAJ")."</div>";
    }
  }

	$strHtml .= "<div id='logos'>".
        	     ($drMe->getValueName("BASSIN_CODE") == 'MAY' ?
                         "<img id='logoIfremer2' class='classlogoONEMA' src='".ALK_SIALKE_URL."media/imgs/gen/".$drMe->getValueName("BASSIN_CODE")."_ONEMA.jpg'/>"
                     	   : "").
  							"<img id='logoIfremer' src='".ALK_SIALKE_URL."media/imgs/gen/Ifremer_logo.gif' width='110' />".
					  		"<img id='logoAE' src='".ALK_SIALKE_URL."media/imgs/gen/".$drMe->getValueName("BASSIN_CODE")."_logo_agence_de_leau.jpg' />".
	            "</div>";




  $strHtml .= "</div>";
  $strHtml .= "<div id='pied_page'>";

  $strHtml .="</div>";// fin pied de page
} else {
	$strHtml = "<div class=\"popupTitle\">R�sultats par masse d'eau".
							"<div class=\"popupBtClose\"><div onclick=\"javascript:top.closeWindow('')\" class=\"btClose\"/></div></div></div>".
							"<div id='conteneur'>".
							"<div class='txt colon classMeNonIdentifie' >Masse d'eau non identifi�e</div>";
	$strHtml .= "</div>";
  $strHtml .= "<div id='pied_page'></div>";
}

if ($bPdf == 1) {
	$strHtml = str_replace(ALK_ROOT_URL, ALK_ROOT_PDF_URL, getHtmlHeader()).
			"</head>" .
			"<body text=\"#000000\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">".$strHtml.getHtmlFooter();

	//$strFile = getPdf2($strHtml, "", "", $codeMe);
	affPdf($strHtml, $codeMe."_sselemqualite");

} else {
	aff_menu_haut($tabEventBody);
	echo $strHtml;
	aff_menu_bas();echo "<style> .tdPair1.stylegeneral{background-color: #DFF0FF;}</style>";
        echo "<script src=\"".ALK_SIALKE_URL."/libconf/lib/jquery-1.11.1.min.js\" type=\"text/javascript\"></script>";
        $strJquery = <<< ��
        <script type="text/javascript" >
          $(document).ready(function(){

            var myCurrentOverlayCell = null;

            var showOverLayCell = function(cellLeft, cellRight){

              var offSetLeft = cellLeft.offset();
              if( myCurrentOverlayCell ){
                myCurrentOverlayCell.remove();
              }

              var tmpClonePopup1 = null;
              var tmpOffsetPopup = {top: 0, left: 0};
              if( cellLeft.children('.popup1').length > 0 ){
                tmpOffsetPopup = cellLeft.children('.popup1').offset();
                var tmpTopOffsetPopup = tmpOffsetPopup.top - offSetLeft.top;
                var tmpLeftOffsetPopup = tmpOffsetPopup.left - offSetLeft.left;
                tmpOffsetPopup = {top: tmpTopOffsetPopup, left: tmpLeftOffsetPopup};
                tmpClonePopup1 = cellLeft.children('.popup1').clone();
                tmpClonePopup1.children('img').remove();
              }

              myCurrentOverlayCell = $('<div id="myTmpCellOverlay" ></div>');
              if( tmpClonePopup1 ){
                myCurrentOverlayCell.append(tmpClonePopup1.offset(tmpOffsetPopup).css({position: 'absolute', width:'11px', height: '10px'}));
              }

              var tmpCellWidth = cellLeft.innerWidth() + cellRight.innerWidth()+2;
              var tmpCellHeight = cellLeft.innerHeight()+2;
              var actionOnClick = cellRight.attr('onclick');

              $('body').append(myCurrentOverlayCell.offset(
                  {
                    top: offSetLeft.top-1,
                    left: offSetLeft.left-1
                  }
                ).css(
                  {
                    position: 'absolute',
                    width: tmpCellWidth+'px',
                    height: tmpCellHeight+'px',

                    cursor: 'pointer',
                    'background-color': 'darkslategray',
                    opacity: 0.4,
                    filter: 'alpha(opacity=40)'
                  }
                ).attr('onclick', actionOnClick).click(function(event){
                  event.stopPropagation();
                  myCurrentOverlayCell.remove();
                }).hover(function(){
                  }, function(){
                    myCurrentOverlayCell.remove();
                  }
                )
              );
            };

            $('table.listeEtat tr.trPair1 td.tdPair1:not(.tdEtat)').hover(function(){
              var tmpTdNotEtat = $(this);
              var tmpTdEtat = $(this).next('.tdPair1.tdEtat');
              var tmpAttrOnclick = tmpTdEtat.attr('onclick');
              if (typeof tmpAttrOnclick !== typeof undefined && tmpAttrOnclick !== false) {
                showOverLayCell(tmpTdNotEtat, tmpTdEtat);
              }
            },function(){});

            $('table.listeEtat tr.trPair1 td.tdPair1.tdEtat[onclick]').hover(function(){
              var tmpTdEtat = $(this);
              var tmpTdNotEtat = $(this).prev('.tdPair1:not(.tdEtat)');
              showOverLayCell(tmpTdNotEtat, tmpTdEtat);
            },function(){});

          });
        </script>
��;
        echo $strJquery;
}

?>
