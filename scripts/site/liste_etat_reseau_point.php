<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("lib/lib_pagination.php");
include_once("classes/alkmasseeau.class.php");
include_once("lib/lib_pdf.php");

$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$reseau_code = Request("reseau_code", REQ_GET, "");
$page =  Request("page", REQ_GET, "1", "is_numeric");
$bPdf = Request("bPdf", REQ_GET, "0", "is_numeric");
$bPagine =  Request("bPagine", REQ_GET, "0", "is_numeric");
$nbEltParPage =  Request("nbEltParPage", REQ_GET, "20", "is_numeric");
$iFirst = ($bPagine ? ($page-1)*$nbEltParPage : 0);
$iLast = ($bPagine ? $iFirst+$nbEltParPage-1 : -1);
$codeBassin = Request("bassin_id", REQ_GET, ""); // en fait c'ets le bassin code'
$menu_id=0;
$iAcces = 0;
$strParam = "bassin_id=".$codeBassin."&reseau_code=".$reseau_code;

$dsBassin = $queryAtlas->getDs_bassin($codeBassin);
if ($drBassin = $dsBassin->getRowIter()){
  $bassin_id = $drBassin->getValueName("BASSIN_ID");
  $bassin_nom = $drBassin->getValueName("BASSIN_NOM");
}
  
$tabAlign = array("","center", "center", "center", "center", "center", "center");

$tabType = array("MEC" => "masses d'eau c�ti�res", 
								 "MET" => "masses d'eau de transition", 
								 "MER" => "masses d'eau r�cifales");

// liste
$strHtmlListe = "";
$bReseauNom = false;
$nbElt = 0;

foreach($tabType as $strType=>$strTitre){

  // liste MEC
  $dsMe = $queryAtlas->getDs_listePointByReseau($iFirst, $iLast, $reseau_code, $codeBassin, "MASSE_TYPE='".$strType."'") ;
  $nbEltMEC = $dsMe->iCountTotDr;
  $nbEltParPageMEC = ($bPagine ? $nbEltParPage : $nbEltMEC);
  $reseauNOM = "";
  $nbElt += $nbEltMEC;
  $tabPageMEC = array();
  
  while ($drMe = $dsMe->getRowIter()) {
  	    
  	$idMe = $drMe->getValueName("MASSE_ID");	  
    $pointNom = $drMe->getValueName("POINT_NOM"); 
    $pointCode = $drMe->getValueName("POINT_CODE");
    $X = $drMe->getValueName("POINT_LONGITUDE");
    $Y = $drMe->getValueName("POINT_LATITUDE");
    $masseCode = $drMe->getValueName("MASSE_CODE"); 
    $masseNOM = $drMe->getValueName("MASSE_NOM");     
    $reseauNOM = $drMe->getValueName("RESEAU_NOM");     
  
    $strHtmlNomMe = "<div onmouseover=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'show');\"
  											  onmouseout=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'hide');\">".
  											  "<a href=\"javascript:void(OpenWindow('".ALK_SIALKE_URL."scripts/site/fiche_etatme.php?code=".$drMe->getValueName("MASSE_CODE")."', '900', '900', 'ficheetatme'));\">".$masseNOM."</a>".
  											  "<div id='me".$drMe->getValueName("MASSE_CODE")."' class='detaillisteme1' onmouseout=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'hide');\">
  											  <img src='".ALK_SIALKE_URL."upload/carte/".$drMe->getValueName("BASSIN_CODE")."/imgms/vignette/".$drMe->getValueName("MASSE_CODE").".png' class='vignettedesc' align='right'/>
  											  <p><b>".$drMe->getValueName("LIB_MASSE")."</b><br/>											  
  											  D�partement(s) : ".$drMe->getValueName("DEPT")."<br/>".
  											  //"R�gion marine : ".$drMe->getValueName("REGION_LIBELLE")."<br/>".
  											  "Bassin Hydrographique : ".$drMe->getValueName("BASSIN_NOM")."<br/>
  											  Type : ".$drMe->getValueName("DESCR")."<br/></p>".
  											  "</div></div>";
    
    $tabPageMEC[] = array($pointNom, $pointCode, $masseCode, $strHtmlNomMe, $X, $Y);
    								 
  } 
  $bReseauNom = ($reseauNOM != "");
  
  // liste MEC  
  if( $nbEltMEC>0){
  $strHtmlListe .= "<div id='titreME' class='titreMEdetaille'>".	
  			"<h1>Liste des points de surveillance des ".$strTitre." pour le r�seau ".$reseauNOM.
  			"</h1>
        </div>
  			<div id='contenuME' class='txt colon classMEdetaille ".($bPdf != 1 ? "clearfix" : "")."'>													
              <table class='table1'  cellpadding='0' cellspacing='0'>
              <thead>
              <tr class='trEntete1' >
              <td class='tdEntete1_300' rowspan=1 >Point</td>
              <td class='tdEntete1_300' rowspan=1 >Code point</td>
              <td class='tdEntete1_300' rowspan=1 >Code masse d'eau</td>
              <td class='tdEntete1_300' rowspan=1 >Nom masse d'eau</td>
              <td class='tdEntete1_100' rowspan=1 >Longitude (WGS84)</td>
              <td class='tdEntete1_100' rowspan=1 >Latitude (WGS84)</td>
              </tr>
              <tr>";
  
  $strHtmlListe .= "</tr></thead>".getHtmlListePagine($tabPageMEC, $nbEltMEC, $nbEltParPageMEC, $page, 
                                 $_SERVER["PHP_SELF"]."?".$strParam, 
                                 $tabAlign)."</table>".
              ($bPagine == 1 ? "<div class='clearPagine'><a href='".$_SERVER["PHP_SELF"]."?".$strParam."&bPagine=0'>Voir la liste compl�te</a></div>" : "");
  $strHtmlListe .= "</div>";
  }
  
}

//si reseau nom non d�fini
if( ! $bReseauNom){
$dsRes = $queryAtlas->getReseauByCode($reseau_code, "(select BASSIN_ID from BASSIN_HYDROGRAPHIQUE where BASSIN_CODE = '".$codeBassin."')");
	if($drRes = $dsRes->getRowIter()) {
		$reseauNOM = $drRes->getValueName("RESEAU_NOM"); 
	}
}

$strHtml = "<script language=javascript>".
              "function OpenFichePdf()".
              "{".
              "OpenPopupWindow('".ALK_SIALKE_URL."scripts/site/liste_etat_reseau_point.php?".$strParam."&bPdf=1','600','800','fiche_me');".
              //"OpenFooterExec('liste_etat_reseau_point.php?".$strParam."&bPdf=1');".             
              "}</script>".
              "<div class='popupTitle'>Atlas DCE ".$bassin_nom." - Liste des points de surveillance par r�seau".
              ($bPdf == 1 ? "" : 
  	          "<a class=\"imprim\" title=\"Imprimer\" href=\"javascript:OpenFichePdf();\">
              <img id=\"imprimer\" height='19' width='19' onmouseout='MM_swapImgRestore()' onmouseover=\"MM_swapImage('imprimer','','".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif' ,1)\" alt='Exporter en pdf' title='Exporter en pdf' src='".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif'/>
              </a>").
              "<div class=\"popupBtClose\"><div onclick=\"javascript:window.close()\" class=\"btClose\"/></div></div></div>";						
                   
  $strHtml .= "<div id='conteneur'>";
  if( ($nbElt) == 0 )
    $strHtml .= "Aucun point de surveillance recens� pour le r�seau ".$reseauNOM.".";
  
 $strHtml .= $strHtmlListe;
 
 $strHtml .= "<div id='logos'>".
  				"<img id='logoIfremer' src='".ALK_SIALKE_URL."media/imgs/gen/Ifremer_logo.gif' width='110' />".
				"<img id='logoAE' src='".ALK_SIALKE_URL."media/imgs/gen/".$codeBassin."_logo_agence_de_leau.jpg' />".
            "</div>";

  $strHtml .= "</div>";
  $strHtml .= "<div id='pied_page'></div>";
  
if ($bPdf == 1) {
	$strHtml = str_replace(ALK_ROOT_URL, ALK_ROOT_PDF_URL, getHtmlHeader()).
			"</head>" .
      "<body class='bodyPdf'>".$strHtml.getHtmlFooter();

	//$strFile = getPdf2($strHtml, "", "", $codeBassin."_synthese");
	affPdf($strHtml, $codeBassin."_synthese");	
	
} else {
  aff_menu_haut();
  echo $strHtml;
  aff_menu_bas();
}	



?>