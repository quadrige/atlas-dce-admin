<?php
/*
* File : 01_accueil.php
* Create : 20/08/2004
* Author : DM

* Description : Page d'accueil du site en consultation
* Parameters :
*/
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_menu_popup.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_atlas.php");
include_once("lib/lib_pagination.php");
include_once("classes/alkmasseeau.class.php");
include_once("lib/lib_pdf.php");


$cont_id = Request("cont_id", REQ_GET, "1", "is_numeric");
$codeBassin = Request("bassin_id", REQ_GET, "LB");
$page =  Request("page", REQ_GET, "1", "is_numeric");
$bPdf = Request("bPdf", REQ_GET, "0", "is_numeric");
$bPagine =  Request("bPagine", REQ_GET, "0", "is_numeric");
$nbEltParPage =  Request("nbEltParPage", REQ_GET, "20", "is_numeric");
$iFirst = ($bPagine ? ($page-1)*$nbEltParPage : 0);
$iLast = ($bPagine ? $iFirst+$nbEltParPage-1 : -1);
    
$menu_id=0;
$iAcces = 0;
$strParam = "bassin_id=".$codeBassin;

$dsBassin = $queryAtlas->getDs_bassin($codeBassin);
if ($drBassin = $dsBassin->getRowIter()){
  $bassin_id = $drBassin->getValueName("BASSIN_ID");
  $bassin_nom = $drBassin->getValueName("BASSIN_NOM");
}

//Liste des masses d'eau selon type 
$tabType = array("MEC" => "masses d'eau c�ti�res", 
								 "MET" => "masses d'eau de transition", 
								 "MER" => "masses d'eau r�cifales");


// liste
$strHtmlListe = "";

foreach($tabType as $strType=>$strTitre){
  
  $dsMeMEC = $queryAtlas->getDs_listeMasseEau($iFirst, $iLast, "", $codeBassin, "MASSE_TYPE='".$strType."'");
  $nbEltMEC = $dsMeMEC->iCountTotDr;
  $strAlert="";
  $tabPageMEC = array();
  $nbEltParPageMEC = ($bPagine ? $nbEltParPage : $nbEltMEC);
  $tabAlign = array("","center", "center", "center", "center");
  
  while ($drMe = $dsMeMEC->getRowIter()) {
  	
  	$idMe = $drMe->getValueName("MASSE_ID");	
  	$strNomMe = $drMe->getValueName("LIB");
    $strCode = $drMe->getValueName("MASSE_CODE");
    $bAtteinteObj = $drMe->getValueName("MASSE_B_ATTEINTE_OBJECTIF");	
  	$bMasseSuivi = $drMe->getValueName("MASSE_B_SUIVI");  
      
    $strHtmlNomMe = "<div onmouseover=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'show');\"
  											  onmouseout=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'hide');\">".
  											  "<a href=\"javascript:void(OpenWindow('".ALK_SIALKE_URL."scripts/site/fiche_etatme.php?code=".$drMe->getValueName("MASSE_CODE")."', '900', '900', 'ficheetatme'));\">".$strNomMe."</a>".
  											  "<div id='me".$drMe->getValueName("MASSE_CODE")."' class='detaillisteme1' onmouseout=\"MM_showHideLayers('me".$drMe->getValueName("MASSE_CODE")."', '', 'hide');\">
  											  <img src='".ALK_SIALKE_URL."upload/carte/".$drMe->getValueName("BASSIN_CODE")."/imgms/vignette/".$drMe->getValueName("MASSE_CODE").".png' class='vignettedesc' align='right'/>
  											  <p><b>".$drMe->getValueName("LIB")."</b><br/>											  
  											  D�partement(s) : ".$drMe->getValueName("DEPT")."<br/>".
  											  //"R�gion marine : ".$drMe->getValueName("REGION_LIBELLE")."<br/>".
  											  "Bassin Hydrographique : ".$drMe->getValueName("BASSIN_NOM")."<br/>
  											  Type : ".$drMe->getValueName("DESCR")."<br/></p>".
  											  "</div></div>";  
  
    $styleCarreColor = ($bMasseSuivi==1 ? "styleBleuFonce" : "styleBleuClair");
    $atteintObj = ($bAtteinteObj==0 ? "NON" : "OUI");
    $tabPageMEC[] = array("<div class='divLeg styleColorLeg ".$styleCarreColor."' >&nbsp;</div>",$strCode, $strHtmlNomMe, $atteintObj);      									 
  } 
  
  if ($nbEltMEC > 0) {
    $strHtmlListe .= "<div id='titreME' class='titreMEdetaille'>".	
  				"<h1>Liste des ".$strTitre."<br/>							
  				</h1>".
  			"</div>".	
  			"<div id='contenuME' class='txt colon classMEdetaille ".($bPdf != 1 ? "clearfix" : "")."'>".					
  						"<table class='table1'  cellpadding='0' cellspacing='0'>
  						<thead>
  						<tr class='trEntete1' >
  						<td class='tdEntete1_100' rowspan=1 > </td>
  						<td class='tdEntete1_300' rowspan=1 >Code</td>
  						<td class='tdEntete1_300' rowspan=1 >Nom</td>
              <td class='tdEntete1_300' rowspan=1 >Atteinte des objectifs</td>
  						</tr>
  						<tr>";
  
    $strHtmlListe .= "</tr></thead>".getHtmlListePagine($tabPageMEC, $nbEltMEC, $nbEltParPageMEC, $page, 
                                   $_SERVER["PHP_SELF"]."?".$strParam, 
                                   $tabAlign)."</table>".
                ($bPagine == 1 ? "<div class='clearPagine'><a href='".$_SERVER["PHP_SELF"]."?".$strParam."&bPagine=0'>Voir la liste compl�te</a></div>" : "");
    
    $strHtmlListe .="<div>".$strTitre." (en bleu fonc�, masses d'eau suivies au titre du contr�le de surveillance DCE).</div>";
    $strHtmlListe .= "</div>";
  }
  
}

//img carte
$strHtmlImgCarte = "<img title=\"Masses d'eau du bassin\" alt=\"Masses d'eau du bassin\" style='border: 0px none;' src='".ALK_SIALKE_URL."media/imgs/gen/".$codeBassin."_carte_ME.jpg'>";

// liste MET
$strHtml = "<script language=javascript>".
						 "function OpenFichePdf()".
						 "{".
						 "OpenPopupWindow('".ALK_SIALKE_URL."scripts/site/liste_etatmet_mec.php?".$strParam."&bPdf=1','600','800','fiche_me');".
             //"OpenFooterExec('liste_etatmet_mec.php?".$strParam."&bPdf=1');".	
            "}</script>".
						"<div class='popupTitle'>Atlas DCE ".$bassin_nom." - Liste des masses d'eau".
						($bPdf == 1 ? "" : 
	          "<a class=\"imprim\" title=\"Imprimer\" href=\"javascript:OpenFichePdf();\">
            <img id=\"imprimer\" height='19' width='19' onmouseout='MM_swapImgRestore()' onmouseover=\"MM_swapImage('imprimer','','".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif' ,1)\" alt='Exporter en pdf' title='Exporter en pdf' src='".ALK_SIALKE_URL."media/imgs/gen/pictos/pict_pdf.gif'/>
            </a>").
            "<div class=\"popupBtClose\"><div onclick=\"javascript:window.close()\" class=\"btClose\"/></div></div></div>";						

$strHtml .= "<div id='conteneur'>";
             
$strHtml .= "<div style='text-align: center;'>".$strHtmlImgCarte."</div>";    

$strHtml .= $strHtmlListe;

$strHtml .= "</div>";
  $strHtml .= "<div id='pied_page'></div>";
  
if ($bPdf == 1) {
	$strHtml = str_replace(ALK_ROOT_URL, ALK_ROOT_PDF_URL, getHtmlHeader()).
			"</head>" .
      "<body class='bodyPdf'>".$strHtml.getHtmlFooter();

	//$strFile = getPdf2($strHtml, "", "", $codeBassin."_synthese");
	affPdf($strHtml, $codeBassin."_synthese");	
	
} else {
	aff_menu_haut();
	echo $strHtml;
	aff_menu_bas();
}


?>