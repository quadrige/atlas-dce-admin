function SupprElt(id, strParam, msg)
{
  if (msg== 1)
    var bRes = window.confirm("Ce param�tre est actuellement utilis� pour d�crire certains points. " +
        "Veuillez confirmer la suppression de cet �l�ment ?");
  else 
    var bRes = window.confirm("Veuillez confirmer la suppression de cet �l�ment ?");
 
  
  if( bRes ) {
    var f = document.formDico;
    f.action = "01_page_sql.php?iMode=3&"+strParam+"&id="+id;
    f.submit();
  }
}
AlkNbClickValid = 0;
function ValiderDico(iSSheet)
{
  var f = document.formDico;
  if( AlkNbClickValid == 0 ) {
    var bRes = AlkVerifCtrl(f);
    
    if (iSSheet == 6) {
    		if (f.masse.value == '-1') {
    			alert("Vous devez s�lectionner une masse d'eau");
    			return;
    		}    	
    }

    if (iSSheet == 4) { // cas des reseaux
    		if (f.img_symb.value == '') { // choix du parametrage d'un symb

      		  if(f.symbole.value != '-1' && f.couleur.value != '' && f.reseauSymboleTaille.value != ''){
    		       
    		    }else{
    			     alert("Vous devez renseigner l'ensemble des valeurs pour le choix du type de symbole, de la couleur et de la taille du symbole");
    			     return;
    		    }

    		}else{// choix du symb de type img
    		  
    		    if(f.symbole.value != '-1' || (f.reseauSymboleTaille.value != '' && f.reseauSymboleTaille.value != '0')){
    			     alert("Vous devez choisir entre le param�trage d'un symbole en s�lectionnant le symbole, la taille et la couleur ou bien par le biais d'une image.");
    			     f.img_symb.value = ''; 
    			     return;
    		    }	  
    		}    	
    }

    
    if( bRes == true ) {
      AlkNbClickValid++;
      f.submit();
    }
  }
}

function onChangeDico()
{
  var f = document.formDico;  
  f.submit();  
}

function ValiderAnnu(iSSheet)
{
  var f = document.formUser;
  if( AlkNbClickValid == 0 ) {
    var bRes = AlkVerifCtrl(f);
           
    if( bRes == true ) {
      AlkNbClickValid++;
      f.submit();
    }
  }
}

function SupprEltAnnu(id, strParam)
{
  var bRes = window.confirm("Veuillez confirmer la suppression de cet �l�ment ?");
  if( bRes ) {
    var f = document.formUser;
    f.action = "01_page_sql.php?iMode=3&"+strParam+"&id="+id;
    f.submit();
  }
}

function ValiderImport()
{
  var f = document.formDico;
  var strNomChFicImport = "imp_fic";
  if (arguments.length > 0)
	  strNomChFicImport = arguments[0];  
  if( AlkNbClickValid == 0 ) {
    var bRes = AlkVerifCtrl(f);
    
    var strNomFic = eval("f."+strNomChFicImport+".value"); 
    if( strNomFic=="" ) {
    	alert("Vous devez s�lectionner un fichier d'import.");
    	return;
  	}
    
    if( bRes == true ) {
      AlkNbClickValid++;
      f.submit();
    }
  }
}

function ValiderArchivage()
{
  var bRes = window.confirm("Veuillez confirmer l'archivage de la session courante et le passage � une nouvelle session ?");
  if( bRes ) {
    var f = document.formDico;    
    f.submit();
  }
}

function OpenFiche(strParam)
{
	 OpenWindow("01_page_popup.php?"+strParam, "200", "500", "fiche");
}

function majPoint()
{
  var f = document.formData;
  f.submit();     
}

function Recherche(strAction)
{
  var f = document.formData;
  if (strAction != '')
  	f.action = strAction;
  
 	if( AlkNbClickValid == 0 ) {
  	 var bRes = AlkVerifCtrl(f);
     if( bRes == true ) {
       AlkNbClickValid++;
       f.submit();
   	}  
 	}  	
}

function SupprEltData(iMode, idReseau, idPoint, idParam, strUrl)
{
	var strMsg = "";
	if (iMode == 2)
		strMsg = "Veuillez confirmer la suppression de tous les param�tres pour ce point et ce r�seau ?";
	else
		strMsg = "Veuillez confirmer la suppression du param�tre pour ce point et ce r�seau ?";
		
  var bRes = window.confirm(strMsg);
  if( bRes ) {
    var f = document.formData;
    f.action = strUrl+"&iMode="+iMode+"&fc_reseau="+idReseau+"&fc_point="+idPoint+"&sup_param="+idParam;
    f.submit();
  }
}

function majBassinCourant(){
  var oCtrl = document.getElementById("bassin_admin");
  if (oCtrl){
    var bassin_select = oCtrl.options[oCtrl.selectedIndex].value;
    var oAjax = new AlkAjax('majBassin', 'fnMajBassin', "GET", './bassin.php?bassin_id='+bassin_select, null);
  }
}

function fnMajBassin(strHTML)
{
  if(strHTML!=""){
    document.location.reload();
  }
}

function majListBassin(){
  
  var f = document.formUser;  
  console.log(f);
  var oDiv = document.getElementsByName("bassin_id[]")[0];
  //var oStyle = ( oDiv.style ? oDiv.style : oDiv );
  id1 = f.user_admin_all_bassin.checked;
  if ( !oDiv ) return;

    for(var i=0; i<oDiv.length;i++){
      if(id1 == true){
        oDiv.item(i).disabled='disabled';
        oDiv.item(i).selected=true;
      }else{
        oDiv.item(i).disabled='';
      }
    }
    if(id1 == true){
      oDiv.item(0).selected=false;
    }
  
}






