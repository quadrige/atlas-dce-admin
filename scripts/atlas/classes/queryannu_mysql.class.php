<?php
include_once('queryannu.class.php');

/**
 * @Class queryAnnuMysql
 *
 * @brief Ensemble des requetes s�lections li�es � l'application Actualites sp�cialis�es Mysql
 *         Les requetes standards se trouvent dans la classe m�re
 */
class queryAnnuMySql extends queryAnnu
{
  /**
   * @brief Constructeur
   *
   * @param dbConn    classe de connexion � la base
   * @param tabLangue tableau des langues utilis�es
   */
  function queryAnnuMySql(&$dbConn, $tabLangue=array()) 
  {
    parent::queryAnnu($dbConn, $tabLangue);
  }   

    /**
   * @brief Retourne la liste des bassin pour un agent
   *
   * @param idAg  Identifiant d'un Agent
   *
   * @return Retourne un dataset
   */
  function getDs_listeBassinByAgent($idAg) 
  {          
    $strSql = "select * ".
              " from ANNU_USER_BASSIN aub".
              " where USER_ID = ".$idAg;
    return $this->dbConn->initDataset( $strSql ) ;
  }  

}
?>