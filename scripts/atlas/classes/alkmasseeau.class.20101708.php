<?php

/**
 * @brief Classe regroupant des fonctionnalit�s li�e � la repr�sentation de la qualit� des masses d'eau
 */
class AlkMasseEau
{
  /** */
  var $codeMe;
  var $data;
  var $oQuery;
  var $tabClassement;

  /**
   * @brief Constructeur par d�faut
   *
   */
  function AlkMasseEau($codeMe, &$oQuery, $idMe="")
  {
    $this->codeMe = $codeMe;
    $this->oQuery = $oQuery;
    $dsMe = $this->oQuery->getDs_MasseEauById($idMe, $codeMe);
		if ($drMe = $dsMe->getRowIter()) {
			$this->data = $drMe;
			$this->codeMe = $drMe->getValueName("CODE");		
		}
		
		//D�termine tous les �tats de la masse d'eau
		$this->setEtatQualite();		
  }

  /**
   * @brief retourne le code html de la l�gende des �tats de qualit� d'une masse d'eau
   */
  function getLegende($idTypeClassement="")
  {
		$dsEtat = $this->oQuery->getDs_listeEtat();
		while ($drEtat = $dsEtat->getRowIter() ){
			//$strTypeClassement = strtr($drEtat->getValueName("CLASSEMENT"), "e����a���u���i���o�c�", "eeeeeaaaauuuuiiiooocc");
			if ($idTypeClassement != "" && $idTypeClassement==$drEtat->getValueName("TYPE_CLASSEMENT_ID") || $idTypeClassement=="")
				$tabLegende["Etat ".$drEtat->getValueName("CLASSEMENT")][] = array("LIB"=>$drEtat->getValueName("LIB"), 
																								"COLOR"=>$drEtat->getValueName("ETAT_COULEUR"));
		}
		$strHtmlLeg = "<table style='width:100%;padding:5px;margin:0;'><tr>";
		foreach ($tabLegende as $strTypeClassement=>$tabEtat){			
			//$strHtmlLeg .= "<td width='50%' valign='top' class=\"txt\">".$strTypeClassement."<br/><br/><ul class='legende'>";
			$strHtmlLeg .= "<td width='50%' valign='top' class=\"txt\">".$strTypeClassement."<br/><br/><table class='legende'>";
			foreach ($tabEtat as $tabLeg){
				//$strHtmlLeg .= "<li>".$tabLeg["LIB"]."<div class='divLeg' style='background-color:".$tabLeg["COLOR"].";'></div></li>";
				$strHtmlLeg .= "<tr><td>".$tabLeg["LIB"]."</td><td><div class='divLeg' style='top:0px;background-color:".$tabLeg["COLOR"].";'></div></td></tr>";
			}			
			//$strHtmlLeg .= "<div style='clear:both;'></div></ul></td>";
			$strHtmlLeg .= "<div style='clear:both;'></div></table></td>";
  	}
  	$strHtmlLeg .= "</tr></table>";
  	return $strHtmlLeg;
  }
  
  /**
   * @brief retourne le code html de la l�gende des motifs et statuts
   */
  function getLegendeMotif()
  {
		$dsEtat = $this->oQuery->getTableForCombo("ETAT_MOTIF", "MOTIF", true, true);
		while ($drEtat = $dsEtat->getRowIter() ){
			$tabLegende["Motifs"][] = array("LIB"=>$drEtat->getValueName("nom"));
		}
  	$dsStatut = $this->oQuery->getTableForCombo("CLASSEMENT_STATUT", "STATUT", true, true);
		while ($drEtat = $dsStatut->getRowIter() ){
			$tabLegende["Statuts"][] = array("LIB"=>$drEtat->getValueName("nom"));
		}
		$strHtmlLeg = "<table style='width:100%;padding:5px;margin:0;'><tr>";
		foreach ($tabLegende as $strTypeClassement=>$tabEtat){
			//$strHtmlLeg .= "<td width='50%' valign='top' class=\"txt\">".$strTypeClassement."<br/><br/><ul class='legende'>";
			$strHtmlLeg .= "<td width='50%' valign='top' class=\"txt\"><ul class='legende'>";
			foreach ($tabEtat as $tabLeg){
				if (strpos($tabLeg["LIB"], "Fin") === false)
					$strHtmlLeg .= "<li>".$tabLeg["LIB"]."</li>";				
			}			
			$strHtmlLeg .= "</ul></td>";
  	}
  	$strHtmlLeg .= "</tr></table>";
  	return $strHtmlLeg;
  }  
  
  /**
   * @brief retourne le code html de la l�gende des motifs et statuts
   */
  function getLegendeReseaux()
  {
		$dsReseaux = $this->oQuery->getReseauForCombo();
		while ($drReseau = $dsReseaux->getRowIter() ){
			$tabLegende["R�seaux"][] = array("LIB"=>$drReseau->getValueName("RESEAU_NOM"),
			                                 "SYMBOLE"=> "<div class='divLegReseau' style='width:15px;height:15px;top:0;background:".$drReseau->getValueName("RESEAU_COULEUR")." url(".ALK_SIALKE_URL."media/imgs/gen/pictos/legende/".$drReseau->getValueName("RESEAU_SYMBOLE").".gif) center no-repeat'></div>");
		}
  	
		$strHtmlLeg = "<table style='width:100%;padding:5px;margin:0;'><tr>";
		foreach ($tabLegende as $strTypeClassement=>$tabEtat){
			//$strHtmlLeg .= "<td width='50%' valign='top' class=\"txt\">".$strTypeClassement."<br/><br/><ul class='legende'>";
			$tabNbCol = array("0"=>0); 
			for ($i=1;$i<=2;$i++){
				$tabNbCol[$i] = ($i==1 ? count($tabEtat) / 2 : count($tabEtat) - (count($tabEtat) / 2));				
				$strHtmlLeg .= "<td width='50%' valign='top' class=\"txt\"><table  width='100%' class='legende'>";
				for($j=0;$j<$tabNbCol[$i];$j++){
					//$strHtmlLeg .= "<li>".$tabEtat[$tabNbCol[$i-1]+$j]["LIB"]."</li>";			
					$strHtmlLeg .= "<tr><td width='80%'>".$tabEtat[$tabNbCol[$i-1]+$j]["LIB"]."</td><td>".$tabEtat[$tabNbCol[$i-1]+$j]["SYMBOLE"]."</td></tr>";
				
				}			
				$strHtmlLeg .= "</table></td>";	
			}			
  	}
  	$strHtmlLeg .= "</tr></table>";
  	return $strHtmlLeg;
  }    
  
  /**
   * @brief retourne le code html de la l�gende des typologies des masses d'eau cotieres ou de transition
   */
  function getLegendeTypo($strTypeMe="C", $codeBassin)
  {
 		$dsTypeMe = $this->oQuery->getDs_listeTypologieMasseEau($strTypeMe, $codeBassin);
		while ($drTypeMe = $dsTypeMe->getRowIter()){
			$tabLegende["Typo"][] = array("LIB"=>$drTypeMe->getValueName("MASSE_DESC_CODE")." - ".$drTypeMe->getValueName("MASSE_DESC_LIBELLE"), 
																		"COLOR"=>$drTypeMe->getValueName("MASSE_DESC_COLOR"));			
		}		
		$strHtmlLeg = "<table style='width:100%;padding:5px;margin:0;'><tr>";
		foreach ($tabLegende as $strTypeClassement=>$tabTypo){			
			$strHtmlLeg .= "<td width='50%' valign='top' class=\"txt\"><table class='legende'>";
			foreach ($tabTypo as $tabLeg){
				$strHtmlLeg .= "<tr><td>".$tabLeg["LIB"]."</td><td><div class='divLeg' style='top:0px;background-color:".$tabLeg["COLOR"].";'></div></td></tr>";
			}			
			$strHtmlLeg .= "<div style='clear:both;'></div></table></td>";
  	}
  	$strHtmlLeg .= "</tr></table>";
  	return $strHtmlLeg;
  }  
    /**
   * @brief mise � jour des �tats de qualit� pour la masse d'eau
   * 				issus de la base de donn�es pour les �tats par type d'�l�ments de qualit� et par �l�ments de qualit�
   * 				calcul�s (sur le mode du plus d�classant) pour les �tats 
   * 					- par type de classement (chimique et �cologique) = le plus d�classant des types d'�l�ments de qualit� de ce type de classement
   * 					- global = le plus d�classant des types de classements chimique et �cologique 
   */
  function setEtatQualite($iNiveau="", $bAffDoublons=true){
  	
  	$dsEtat = $this->oQuery->getDs_listeEtatMasseEau($this->codeMe, $iNiveau);
  	$strLastElementNiv1 = ""; 
		while ($drEtat = $dsEtat->getRowIter()){
		  $strMethode = "";
			if ($drEtat->getValueName("ELEMENT_QUALITE_NOM") == "") {
				//Classement du type d'�l�ment de qualit� pour la masse d'eau
				
				$dsMethode = $this->oQuery->getDs_ficheMethode("", $drEtat->getValueName("TYPE_ELEMENT_QUALITE_ID"), "");
				if ($drMethode = $dsMethode->getRowIter())
				  $strMethode = $drMethode->getValueName("METHODE_LIBELLE");
				
				$tabClassement[$drEtat->getValueName("TYPE_CLASSEMENT_LIBELLE")][$drEtat->getValueName("TYPE_ELEMENT_QUALITE_NOM")] = array("ID"=>$drEtat->getValueName("TYPE_ELEMENT_QUALITE_ID"),
																																																																		"ETAT"=>$drEtat->getValueName("ETAT_LIBELLE"),
																																																																		"ETAT_ID"=>$drEtat->getValueName("ETAT_ID"), 
																																																																	  "COULEUR"=>$drEtat->getValueName("ETAT_COULEUR"), 
																																																																		"VALEUR"=>$drEtat->getValueName("ETAT_VALEUR"),
																																																																	  "STATUT"=>$drEtat->getValueName("STATUT_LIBELLE"),
																																																																		"STATUT_ID"=>$drEtat->getValueName("STATUT_ID"),
																																																																		"MOTIF_ID"=>$drEtat->getValueName("MOTIF_ID"),
																																																																		"DATE"=>$drEtat->getValueName("CLASSEMENT_DATE"),
																																																																		"TYPE_CLASSEMENT_ID"=>$drEtat->getValueName("TYPE_CLASSEMENT_ID"),
																																																																		"BILAN"=>$drEtat->getValueName("CLASSEMENT_BILAN"),
																																																																		"COMPLEMENT_BILAN"=>$drEtat->getValueName("CLASSEMENT_COMPLEMENT_BILAN"),
																																																																		"INDICE_CONFIANCE"=>$drEtat->getValueName("CLASSEMENT_INDICE_CONFIANCE"),
																																																																		"CLASSEMENT_ID"=>$drEtat->getValueName("CLASSEMENT_ID"),																																																																									
																																																																		"METHODE"=>$strMethode,
																																																																		"ELEMENTS"=>array());
				
			} else {
				//Classement de l'�l�ment de qualit� pour la masse d'eau 		
				
			  //Si tous les niveaux sont demand�s, on supprime les niveaux 2 qui ont les m�mes intitul�s que le niveau 1 correspondant.
			  if ($iNiveau == "" && $drEtat->getValueName("ELEMENT_QUALITE_NIVEAU")==2 && $drEtat->getValueName("ELEMENT_QUALITE_NOM") == $strLastElementNiv1 && !$bAffDoublons)
				  continue;
				$strLastElementNiv1 = ($drEtat->getValueName("ELEMENT_QUALITE_NIVEAU") == 1 ? $drEtat->getValueName("ELEMENT_QUALITE_NOM") : $strLastElementNiv1);

				$dsMethode = $this->oQuery->getDs_ficheMethode("", "", $drEtat->getValueName("ELEMENT_QUALITE_ID"));
			  if ($drMethode = $dsMethode->getRowIter())
				  $strMethode = $drMethode->getValueName("METHODE_LIBELLE");
				
				$tabClassement[$drEtat->getValueName("TYPE_CLASSEMENT_LIBELLE")][$drEtat->getValueName("TYPE_ELEMENT_QUALITE_NOM")]["ELEMENTS"]["BYID"]["ID_".$drEtat->getValueName("ELEMENT_QUALITE_ID")] = array("ID"=>$drEtat->getValueName("ELEMENT_QUALITE_ID"),
																																																																									"NOM"=>$drEtat->getValueName("ELEMENT_QUALITE_NOM"), 
																																																																									"NIVEAU"=>$drEtat->getValueName("ELEMENT_QUALITE_NIVEAU"),
																																																																									"ETAT"=>$drEtat->getValueName("ETAT_LIBELLE"),
																																																																									"ETAT_ID"=>$drEtat->getValueName("ETAT_ID"), 
																																																																									"COULEUR"=>$drEtat->getValueName("ETAT_COULEUR"), 
																																																																									"STATUT"=>$drEtat->getValueName("STATUT_CODE"),
																																																																									"STATUT_ID"=>$drEtat->getValueName("STATUT_ID"),					
																																																																									"MOTIF"=>$drEtat->getValueName("MOTIF_CODE"),
																																																																									"MOTIF_ID"=>$drEtat->getValueName("MOTIF_ID"),
																																																																									"DATE"=>$drEtat->getValueName("CLASSEMENT_DATE"),
																																																																									"TYPE_CLASSEMENT_ID"=>$drEtat->getValueName("TYPE_CLASSEMENT_ID"),
																																																																									"BILAN"=>$drEtat->getValueName("CLASSEMENT_BILAN"),
																																																																									"COMPLEMENT_BILAN"=>$drEtat->getValueName("CLASSEMENT_COMPLEMENT_BILAN"),
																																																																									"DOC_REF"=>$drEtat->getValueName("CLASSEMENT_DOC_REF"),
																																																																									"INDICE_CONFIANCE"=>$drEtat->getValueName("CLASSEMENT_INDICE_CONFIANCE"),
																																																																									"FICHE_SYNTHESE"=>$drEtat->getValueName("ELEMENT_QUALITE_URL_FICHE"),
																																																																									"CLASSEMENT_ID"=>$drEtat->getValueName("CLASSEMENT_ID"),
																																																																									"B_AFF"=>$drEtat->getValueName("B_AFF"),
																																																																									"METHODE"=>$strMethode);
				
				$tabClassement[$drEtat->getValueName("TYPE_CLASSEMENT_LIBELLE")][$drEtat->getValueName("TYPE_ELEMENT_QUALITE_NOM")]["ELEMENTS"]["BYNUM"][] = array("ID"=>$drEtat->getValueName("ELEMENT_QUALITE_ID"),
																																																																									"NOM"=>$drEtat->getValueName("ELEMENT_QUALITE_NOM"), 
																																																																									"NIVEAU"=>$drEtat->getValueName("ELEMENT_QUALITE_NIVEAU"),
																																																																									"ETAT"=>$drEtat->getValueName("ETAT_LIBELLE"),
																																																																									"ETAT_ID"=>$drEtat->getValueName("ETAT_ID"), 
																																																																									"COULEUR"=>$drEtat->getValueName("ETAT_COULEUR"), 
																																																																									"STATUT"=>$drEtat->getValueName("STATUT_CODE"),
																																																																									"STATUT_ID"=>$drEtat->getValueName("STATUT_ID"),
																																																																									"MOTIF"=>$drEtat->getValueName("MOTIF_CODE"),
																																																																									"MOTIF_ID"=>$drEtat->getValueName("MOTIF_ID"),
																																																																									"DATE"=>$drEtat->getValueName("CLASSEMENT_DATE"),
																																																																									"TYPE_CLASSEMENT_ID"=>$drEtat->getValueName("TYPE_CLASSEMENT_ID"),
																																																																									"BILAN"=>$drEtat->getValueName("CLASSEMENT_BILAN"),
																																																																									"COMPLEMENT_BILAN"=>$drEtat->getValueName("CLASSEMENT_COMPLEMENT_BILAN"),
																																																																									"DOC_REF"=>$drEtat->getValueName("CLASSEMENT_DOC_REF"),
																																																																									"INDICE_CONFIANCE"=>$drEtat->getValueName("CLASSEMENT_INDICE_CONFIANCE"),
																																																																									"FICHE_SYNTHESE"=>$drEtat->getValueName("ELEMENT_QUALITE_URL_FICHE"),
																																																																									"CLASSEMENT_ID"=>$drEtat->getValueName("CLASSEMENT_ID"),
																																																																									"B_AFF"=>$drEtat->getValueName("B_AFF"),
																																																																									"METHODE"=>$strMethode);				
		}
	}

	
	  //On applique la m�thode g�n�rale du plus d�classant pour les �tats interm�diaires (biolo, physico-chimique, hydro, ecolo + global)
		$etatGlobal = -1;	
		$etatGlobalCouleur = "";
		$etatGlobalLibelle = "";
				
		foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {		  
			$etatDeclassant = -1;
			$etatCouleur = "";	
			$etat_libelle = "";		
			$strMethode = "";
			
  		foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
			  if ($strMethode == ""){
			    $dsMethode = $this->oQuery->getDs_ficheMethode($tabEtat["TYPE_CLASSEMENT_ID"], "", "");
			    if ($drMethode = $dsMethode->getRowIter())
				    $strMethode = $drMethode->getValueName("METHODE_LIBELLE");
			  }
  		}
  		reset($tabTypeElementQualite);
			if ($nomClassement != "�cologique"){
			  //M�thode du plus d�classant
  			foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){  			 
  				if ($tabEtat["VALEUR"] > $etatDeclassant){
  					$etatCouleur = $tabEtat["COULEUR"];
  					$etatLibelle = $tabEtat["ETAT"];
  					$etatDeclassant = $tabEtat["VALEUR"];					
  				}
  			}
			}	else {
			  //Prise en compte du cas particulier du calcul de l'�tat ecologique (08/2010)
    		//Si etat biolo + etat physico + etat hydro = tres bon ==> ecolo = tres bon
    		//Si etat biolo + etat physico = tres bon ou bon ==> ecolo = bon
    		//Si etat biolo = bon ou tres bon + etat physico < bon ==> ecolo = moyen
    		//Si etat biolo <= moyen ==> ecolo = biolo    		
			  if ($tabTypeElementQualite["Etat biologique"]["VALEUR"]<=0) {
			    $etatCouleur = $tabTypeElementQualite["Etat biologique"]["COULEUR"];
  				$etatLibelle = $tabTypeElementQualite["Etat biologique"]["ETAT"];
  				$etatDeclassant = $tabTypeElementQualite["Etat biologique"]["VALEUR"];	
			  } else if ($tabTypeElementQualite["Etat biologique"]["VALEUR"] == $tabTypeElementQualite["Etat physico-chimique"]["VALEUR"] && 			  
			      $tabTypeElementQualite["Etat physico-chimique"]["VALEUR"] == $tabTypeElementQualite["Etat hydromorphologique"]["VALEUR"] && 
			      $tabTypeElementQualite["Etat hydromorphologique"]["VALEUR"] == 1){
			        //Si etat biolo + etat physico + etat hydro = tres bon ==> ecolo = tres bon
			        $etatCouleur = $tabTypeElementQualite["Etat biologique"]["COULEUR"];
  					  $etatLibelle = $tabTypeElementQualite["Etat biologique"]["ETAT"];
  					  $etatDeclassant = $tabTypeElementQualite["Etat biologique"]["VALEUR"];	  					    					 
			      } elseif ($tabTypeElementQualite["Etat biologique"]["VALEUR"]<=2 && $tabTypeElementQualite["Etat physico-chimique"]["VALEUR"]<=2) {
			        //Si etat biolo + etat physico = tres bon ou bon ==> ecolo = bon
			        $etatCouleur = "#6ab396";
  					  $etatLibelle = "Bon";
  					  $etatDeclassant = 2;  					
  				 } elseif ($tabTypeElementQualite["Etat biologique"]["VALEUR"]<=2 && $tabTypeElementQualite["Etat physico-chimique"]["VALEUR"]>2){
			        //Si etat biolo = bon ou tres bon + etat physico < bon ==> ecolo = moyen
			        $etatCouleur = "#ffe075";
  					  $etatLibelle = "Moyen";
  					  $etatDeclassant = 3;  					  		  
			      } elseif ($tabTypeElementQualite["Etat biologique"]["VALEUR"]>2){
			        //Si etat biolo <= moyen ==> ecolo = biolo
			        $etatCouleur = $tabTypeElementQualite["Etat biologique"]["COULEUR"];
  					  $etatLibelle = $tabTypeElementQualite["Etat biologique"]["ETAT"];
  					  $etatDeclassant = $tabTypeElementQualite["Etat biologique"]["VALEUR"];  					 				 
			      }			      
			}
			
			if ($etatDeclassant > $etatGlobal){
				$etatGlobal = $etatDeclassant;
				$etatGlobalCouleur = $etatCouleur;
				$etatGlobalLibelle = $etatLibelle;
			}								
			$tabClassement[$nomClassement]["ETAT"] = array("ETAT"=>$etatLibelle, 
																									   "COULEUR"=>$etatCouleur, 
																									   "VALEUR"=>$etatDeclassant, 
																										 "METHODE"=>$strMethode);						
		}
								
		$dsMethode = $this->oQuery->getDs_ficheMethode("", "", "");
		if ($drMethode = $dsMethode->getRowIter())
			$strMethode = $drMethode->getValueName("METHODE_LIBELLE");
		$tabClassement["Global"]["ETAT"] = array("ETAT"=>$etatGlobalLibelle, 
																		 "COULEUR"=>$etatGlobalCouleur, 
																		 "VALEUR"=>$etatGlobal,
																		 "METHODE"=>$strMethode);
		
		$this->tabClassement = $tabClassement;								
	}			

  /**
   * @brief retourne le tableau de l'�tat global de la masse d'eau 
   */
  function getEtatQualiteGlobal(){
  	return $this->tabClassement["Global"]["ETAT"];  	
  }
  /**
   * @brief retourne le tableau des �tats
   */
  function getEtatQualite(){  	
  	return $this->tabClassement;  	
  }
  
}

?>