<?
/**
 * @Class queryActuAction
 *
 * @brief  Ensemble des requetes actions li�es � l'application actualit�s
 *         Les requetes sp�cifiques se trouvent dans une classe h�rit�e queryactu_action_xxx.class.php
 */
class QueryAtlasAction
{
  // r�f�rence sur la connexion ouverte vers la source de donn�es
  var $dbConn;

  /**
   * @brief Constructeur
   *
   * @param oDb       classe de connexion � la base
   * @param tabLangue tableau des langues utilis�es
   */
  function QueryAtlasAction(&$oDb, $tabLangue)
  {
    $this->dbConn = $oDb;
    $this->tabLangue = $tabLangue ;
  }
    
  /**
   * @brief Retourne la partie affectation d'une requete update
   * 
   * @param tabValue Tableau contenant les infos (champ, valeur, type) de l'enregistrement
   * @return Retourne un string
   */
  function _getPartUpdateSql($tabValue)
  {
    $strSql = "";
    while( list($strField, $tabVal) = each($tabValue) ) {
      $idType = $tabVal[0];
      $strValue = $tabVal[1];
      if( $strValue."" != ALK_FIELD_NOT_VIEW ) {
        if( $idType == 0 ) { // string
          $strSql .= ", ".$strField."='".$this->dbConn->analyseSql($strValue)."'";
        } elseif( $idType == 1 ) { // number
          if( $strValue."" == "" )
            $strSql .= ", ".$strField."=null";
          else
            $strSql .= ", ".$strField."=".str_replace(",", ".", $strValue);
        } elseif( $idType == 2 ) { // date
          if( $strValue."" == "" )
            $strSql .= ", ".$strField."=null";
          else
            $strSql .= ", ".$strField."=".$this->dbConn->GetDateFormat("DD/MM/YYYY", "'".$strValue."'");
        } elseif( $idType == 3 ) { // sql
          $strSql .= ", ".$strField."=".$strValue;
        }
      }
    }
    if( $strSql != "" )
      return substr($strSql, 1);
    return "";
  }
  
       /**
   * @brief Retourne la partie affectation d'une requete insert
   * 
   * @param tabValue Tableau contenant les infos (champ, valeur, type) de l'enregistrement
   * @return Retourne un string
   */
  function _getPartInsertSql($tabValue)
  {
    $strListField = "";
    $strListValues = "";
    while( list($strField, $tabVal) = each($tabValue) ) {
      $idType = $tabVal[0];
      $strValue = $tabVal[1];
      $strListField .= ", ".$strField;
        if( $idType == 0 ) { // string
          $strListValues .= ",'".$this->dbConn->analyseSql($strValue)."'";
        } elseif( $idType == 1 ) { // number
          if( $strValue."" == "" )
            $strListValues .= ",null";
          else
            $strListValues .= ",".str_replace(",", ".", $strValue);            
        } elseif( $idType == 2 ) { // date
          if( $strValue."" == "" )
            $strListValues .= ",null";
          else
            $strListValues .= ", ".$this->dbConn->GetDateFormat("DD/MM/YYYY", "'".$strValue."'");
        }
    }
    if( $strListField != "" && $strListValues != "")
      return "(".substr($strListField, 1).") values (".substr($strListValues, 1).")";
    return "";
  }

  /**
   * @brief Ajoute une fiche dans un dictionnaire
   *
   * @param dico         nom de la table dictionnaire
   * @param strFieldKey  nom du champ cl� primaire
   * @param tabValue     Ensemble des valeurs de la table dictionnaire
   * @return Retourne l'identifiant de la fiche cr��e 
   */
  function add_ficheDico($strTable, $strFieldKey, $tabValue)
  {
    $id = $this->dbConn->getNextId($strTable, $strFieldKey, "SEQ_".$strTable);
    $tabValue[$strFieldKey] = array(1, $id);

    $strSql = $this->_getPartInsertSql($tabValue);
    $strSql = "insert into ".$strTable." ".$strSql;

    $this->dbConn->executeSql($strSql);
    return $id;
  }

  /**
   * @brief Modifie une fiche d'un dictionnaire
   *
   * @param dico         nom de la table dictionnaire
   * @param strFieldKey  nom du champ cl� primaire
   * @param tabValue     Ensemble des valeurs de la table dictionnaire
   * @return Retourne l'identifiant de la fiche cr��e 
   */
  function update_ficheDico($strTable, $strFieldKey, $id, $tabValue)
  {
  	
    $strSql = $this->_getPartUpdateSql($tabValue); 
    $strSql = "update ".$strTable." set ".$strSql." where ".$strFieldKey."=".$id;

    $this->dbConn->executeSql($strSql);
  }

  /**
   * @brief Modifie une fiche d'un dictionnaire
   *
   * @param dico         nom de la table dictionnaire
   * @param strFieldKey  nom du champ cl� primaire
   * @param tabValue     Ensemble des valeurs de la table dictionnaire
   * @return Retourne l'identifiant de la fiche cr��e 
   */
  function del_ficheDico($strTable, $strFieldKey, $id)
  {
    $strSql = "delete from ".$strTable." where ".$strFieldKey."=".$id; 
    return $this->dbConn->executeSql($strSql, false);
  }
    
  /**
   * @brief suppression d'une actualite
   *
   * @param idActu
   * @param queryPage
   * @param queryActu
   * @param strPathUpload
   *
   * @return Retourne un booleen
   */
  function delParam($idReseau, $idPoint, $idParam)
  {
  	if ($idParam != "ALL"){
			$strSql = "delete from RESEAU_POINT_PARAM " .
					" where POINT_ID = ".$idPoint." and RESEAU_ID = ".$idReseau." and PARAMETRE_ID = ".$idParam;
  	} else {
  		$strSql = "delete from RESEAU_POINT_PARAM " .
					" where POINT_ID = ".$idPoint." and RESEAU_ID = ".$idReseau;
  	}
  	
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    return $bRes;
  }
    
  /**
   * @brief suppression d'un parametre meme si present dans RESEAU_POINT_PARAM
   * @return Retourne un booleen
   */
  function delParamById($id)
  {

    $strSql = "delete from RESEAU_POINT_PARAM " .
          " where PARAMETRE_ID = ".$id;

    $bRes = $this->dbConn->executeSql($strSql) ;

    $strSql = "delete from PARAMETRE where PARAMETRE_ID =".$id; 
    return $this->dbConn->executeSql($strSql, false);
    

  }    
    
    
  /**
   * @brief  Ajoute un param�tre
   *
   * @param idActu Identifiant de l'actualite
   * 
   * @return Retourne un booleen, False if problem, true otherwise
   */
   function addParam($idReseau, $idPoint, $idParam)
  {
  	if ($idParam != "ALL"){
	    $strSql = "insert into RESEAU_POINT_PARAM (".
	      "POINT_ID,".
	      "RESEAU_ID,".
	      "PARAMETRE_ID,".
        "RESEAU_DATE_MAJ ".
	      ") values (".
	      $idPoint.
	      ",".$idReseau.
	      ",".$idParam.
        ",".$this->dbConn->getDateCur().
	      ")" ;
  	} else {
  		$strSql = "insert into RESEAU_POINT_PARAM (POINT_ID,".
	      "RESEAU_ID,".
	      "PARAMETRE_ID, RESEAU_DATE_MAJ)".	      		
      	" select ".$idPoint.", ".$idReseau.", PARAMETRE_ID,".$this->dbConn->getDateCur()." from PARAMETRE pa, SUPPORT su where su.SUPPORT_ID=pa.SUPPORT_ID".
      	" and su.RESEAU_ID = ".$idReseau;      	  	
  	}    
  	  	    
    return $this->dbConn->executeSql($strSql) ;
  }
  
  /**
   * @brief  Modifie un param�tre
   *
   * @param idActu Identifiant de l'actualite
   * 
   * @return boolean False if problem, true otherwise
   */
  function updateParam($idReseau, $idPoint, $idParam, $idOpLabo, $idOpTer, $strAnnee, $strFrequence, $strPeriode, $strFrequencePlan)
  {
     $strSql = "update RESEAU_POINT_PARAM set ".                  
      " FREQUENCE ='".$this->dbConn->analyseSql($strFrequence)."'".
      ", ANNEE_PRELEVEMENT ='".$this->dbConn->analyseSql($strAnnee)."'".
      ", FREQUENCE_PLAN ='".$this->dbConn->analyseSql($strFrequencePlan)."'".
      ", PERIODE ='".$this->dbConn->analyseSql($strPeriode)."'".
      ($idOpLabo != -1 ? ", OPERATEUR_LABO =".$idOpLabo : "").
      ($idOpTer != -1 ? ", OPERATEUR_TERRAIN =".$idOpTer : ""). 
      ", RESEAU_DATE_MAJ = ".$this->dbConn->getDateCur().
      " where POINT_ID = ".$idPoint." and RESEAU_ID = ".$idReseau." and PARAMETRE_ID = ".$idParam;
                        
    $res = $this->dbConn->executeSql($strSql) ;
      
    return $res;
  }
  
    
  /**
   * @brief suppression d'une actualite
   *
   * @param idActu
   * @param queryPage
   * @param queryActu
   * @param strPathUpload
   *
   * @return Retourne un booleen
   */
  function delQualiteLastSession($idMasse, $idElementQualite, $idTypeElementQualite, $idTypeClassement)
  {
  	$strSql = "delete from CLASSEMENT_MASSE_EAU " .
							" where MASSE_ID = ".$idMasse.
							($idElementQualite != "-1" 
							  ? ($idElementQualite != "" ? " and ELEMENT_QUALITE_ID = ".$idElementQualite : " and ELEMENT_QUALITE_ID is NULL") 
							  : "").
							($idTypeElementQualite != "-1" 
							  ? ($idTypeElementQualite != "" ? " and TYPE_ELEMENT_QUALITE_ID = ".$idTypeElementQualite : " and TYPE_ELEMENT_QUALITE_ID is NULL") 
							  : "").
							($idTypeClassement != "-1" 
							  ? ($idTypeClassement != "" ? " and TYPE_CLASSEMENT_ID = ".$idTypeClassement : " and TYPE_CLASSEMENT_ID is NULL")
							  : "").
							($idElementQualite == "-1" && $idTypeElementQualite == "-1" && $idTypeClassement == "-1"
								? " and ELEMENT_QUALITE_ID is NULL and TYPE_ELEMENT_QUALITE_ID is NULL and TYPE_CLASSEMENT_ID is NULL"
								: "").    
  						" and SESSION_ID = (SELECT SESSION_ID FROM SESSION_QUALITE sess, MASSE_EAU me where me.BASSIN_ID = sess.BASSIN_ID and me.MASSE_ID=".$idMasse." and SESSION_ARCHIVE=0 )";
  	
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    return $bRes;
  }  
  
  function delDoublonsClassement() {
    
    $bRes = true;
    $strSql = "SELECT DISTINCT cl.CLASSEMENT_ID FROM CLASSEMENT_MASSE_EAU cl
                INNER JOIN CLASSEMENT_MASSE_EAU bis ON cl.MASSE_ID = bis.MASSE_ID
                AND cl.ELEMENT_QUALITE_ID = bis.ELEMENT_QUALITE_ID
                AND cl.TYPE_ELEMENT_QUALITE_ID = bis.TYPE_ELEMENT_QUALITE_ID
                AND cl.TYPE_CLASSEMENT_ID = bis.TYPE_CLASSEMENT_ID
                AND cl.SESSION_ID = bis.SESSION_ID
                WHERE cl.CLASSEMENT_ID < bis.CLASSEMENT_ID
                and cl.CLASSEMENT_DATE_CREA < bis.CLASSEMENT_DATE_CREA";
    $dsDoublon = $this->dbConn->initDataset($strSql) ;
    while ($drDoublon = $dsDoublon->getRowIter()) {
       $bRes = $this->dbConn->executeSql("DELETE FROM CLASSEMENT_MASSE_EAU where CLASSEMENT_ID=".$drDoublon->getValueName("CLASSEMENT_ID")) ;
    }    

    return $bRes;
  }
  
function copieSessionDico($bassin_id, $session_old_id, $session_new_id) {

    $strSql = "insert into POINT_SESSION (POINT_ID, SESSION_ID, MASSE_ID, POINT_CODE, POINT_NOM, POINT_LONGITUDE, POINT_LATITUDE, POINT_URL_FICHE)".
  						" select POINT_ID, ".$session_old_id.",POINT.MASSE_ID, POINT_CODE, POINT_NOM, POINT_LONGITUDE, POINT_LATITUDE, POINT_URL_FICHE".
              " from POINT, MASSE_EAU me where me.MASSE_ID = POINT.MASSE_ID and me.BASSIN_ID = ".$bassin_id; 
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "insert into RESEAU_SESSION (RESEAU_ID, SESSION_ID, RESEAU_CODE, RESEAU_NOM, RESEAU_SYMBOLE, RESEAU_COULEUR, RESEAU_RANG, BASSIN_ID, GROUPE_ID, RESEAU_SYMBOLE_TAILLE)".
  						" select RESEAU_ID, ".$session_old_id.", RESEAU_CODE, RESEAU_NOM, RESEAU_SYMBOLE, RESEAU_COULEUR, RESEAU_RANG, RESEAU.BASSIN_ID, GROUPE_ID, RESEAU_SYMBOLE_TAILLE".
              " from RESEAU where RESEAU.BASSIN_ID = ".$bassin_id; 
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "insert into SUPPORT_SESSION (SUPPORT_ID,SESSION_ID,RESEAU_ID,SUPPORT_CODE,SUPPORT_NOM)".
  						" select SUPPORT_ID, ".$session_old_id.", SUPPORT.RESEAU_ID,SUPPORT_CODE,SUPPORT_NOM".
              " from SUPPORT, RESEAU reso where reso.RESEAU_ID = SUPPORT.RESEAU_ID and reso.BASSIN_ID = ".$bassin_id; 
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "insert into PARAMETRE_SESSION (PARAMETRE_ID, SESSION_ID, SUPPORT_ID, PARAMETRE_CODE, PARAMETRE_NOM)".
  						" select PARAMETRE_ID, ".$session_old_id.", suppor.SUPPORT_ID, PARAMETRE_CODE, PARAMETRE_NOM ".
    					" from PARAMETRE, SUPPORT suppor, RESEAU reso ".
    					" where reso.RESEAU_ID = suppor.RESEAU_ID and suppor.SUPPORT_ID=PARAMETRE.SUPPORT_ID and reso.BASSIN_ID = ".$bassin_id;    
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "insert into OPERATEUR_SESSION (OPERATEUR_ID,SESSION_ID,OPERATEUR_NOM,OPERATEUR_CODE,BASSIN_ID)".
  						" select OPERATEUR_ID, ".$session_old_id.", OPERATEUR_NOM,OPERATEUR_CODE,ope.BASSIN_ID".
    					" from OPERATEUR ope where ope.BASSIN_ID = ".$bassin_id;    
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "insert into RESEAU_POINT_PARAM_SESSION (POINT_ID, SESSION_ID,PARAMETRE_ID,RESEAU_ID,OPERATEUR_TERRAIN,OPERATEUR_LABO,FREQUENCE,ANNEE_PRELEVEMENT,FREQUENCE_PLAN, PERIODE, RESEAU_DATE_MAJ)".
  						" select rpp.POINT_ID, ".$session_old_id.", PARAMETRE_ID, RESEAU_ID, OPERATEUR_TERRAIN, OPERATEUR_LABO, FREQUENCE, ANNEE_PRELEVEMENT, FREQUENCE_PLAN, PERIODE, RESEAU_DATE_MAJ ".
    					" from RESEAU_POINT_PARAM rpp, POINT pt, MASSE_EAU me where pt.POINT_ID=rpp.POINT_ID and pt.MASSE_ID = me.MASSE_ID and me.BASSIN_ID = ".$bassin_id;
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "insert into METHODE_EVALUATION_SESSION (METHODE_ID,SESSION_ID,METHODE_LIBELLE,TYPE_CLASSEMENT_ID,ELEMENT_QUALITE_ID,TYPE_ELEMENT_QUALITE_ID,BASSIN_ID)".
  						" select METHODE_ID, ".$session_old_id.", METHODE_LIBELLE,TYPE_CLASSEMENT_ID,ELEMENT_QUALITE_ID,TYPE_ELEMENT_QUALITE_ID,meth.BASSIN_ID".
    					" from METHODE_EVALUATION meth where meth.BASSIN_ID = ".$bassin_id;
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "insert into ETAT_MOTIF_SESSION (MOTIF_ID,SESSION_ID,MOTIF_LIBELLE,MOTIF_CODE,BASSIN_ID)".
  						" select MOTIF_ID, ".$session_old_id.", MOTIF_LIBELLE,MOTIF_CODE, BASSIN_ID".
    					" from ETAT_MOTIF mot where mot.BASSIN_ID=".$bassin_id;
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "insert into CLASSEMENT_STATUT_SESSION (STATUT_ID, SESSION_ID,STATUT_CODE,STATUT_LIBELLE)".
  						" select STATUT_ID, ".$session_old_id.", STATUT_CODE,STATUT_LIBELLE".
    					" from CLASSEMENT_STATUT";
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "insert into ETAT_SESSION (ETAT_ID, SESSION_ID,TYPE_CLASSEMENT_ID,ETAT_LIBELLE,ETAT_COULEUR,ETAT_VALEUR)".
  						" select ETAT_ID, ".$session_old_id.", TYPE_CLASSEMENT_ID,ETAT_LIBELLE,ETAT_COULEUR,ETAT_VALEUR".
    					" from ETAT";
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "insert into ELEMENT_QUALITE_SESSION (ELEMENT_QUALITE_ID,SESSION_ID,TYPE_CLASSEMENT_ID,TYPE_ELEMENT_QUALITE_ID,ELEMENT_QUALITE_NOM,".
  						"	ELEMENT_QUALITE_NIVEAU, ELEMENT_QUALITE_PARENT, ELEMENT_QUALITE_ARBRE, ELEMENT_QUALITE_RANG,ELEMENT_QUALITE_URL_FICHE,ELEMENT_QUALITE_DOC_PROTOCOLE,".
  						"	ELEMENT_QUALITE_DOC_METHODO,ELEMENT_QUALITE_RESTRICT_TYPEME,BASSIN_ID)".
  						" select ELEMENT_QUALITE_ID, ".$session_old_id.", TYPE_CLASSEMENT_ID,TYPE_ELEMENT_QUALITE_ID,ELEMENT_QUALITE_NOM,".
              " ELEMENT_QUALITE_NIVEAU, ELEMENT_QUALITE_PARENT, ELEMENT_QUALITE_ARBRE, ELEMENT_QUALITE_RANG,ELEMENT_QUALITE_URL_FICHE,ELEMENT_QUALITE_DOC_PROTOCOLE,".
              " ELEMENT_QUALITE_DOC_METHODO, ELEMENT_QUALITE_RESTRICT_TYPEME, BASSIN_ID".
              " from ELEMENT_QUALITE".
              " where BASSIN_ID = ".$bassin_id;
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    return $bRes;   
  }
  
  
  function copieSessionClassement($bassin_id, $session_old_id, $session_new_id) {
    
    $strSql = "insert into CLASSEMENT_MASSE_EAU_SESSION (CLASSEMENT_ID, ELEMENT_QUALITE_ID, TYPE_ELEMENT_QUALITE_ID, TYPE_CLASSEMENT_ID, MASSE_ID, MOTIF_ID, STATUT_ID, ETAT_ID, 
     						CLASSEMENT_DATE, CLASSEMENT_DATE_CREA, CLASSEMENT_DATE_MAJ, CLASSEMENT_BILAN, CLASSEMENT_COMPLEMENT_BILAN, CLASSEMENT_DOC_REF, CLASSEMENT_INDICE_CONFIANCE, SESSION_ID, CLASSEMENT_ME_DATE_MAJ) 
               select CLASSEMENT_ID, ELEMENT_QUALITE_ID, TYPE_ELEMENT_QUALITE_ID, TYPE_CLASSEMENT_ID, MASSE_ID, MOTIF_ID, STATUT_ID, ETAT_ID, 
     						CLASSEMENT_DATE, CLASSEMENT_DATE_CREA, CLASSEMENT_DATE_MAJ, CLASSEMENT_BILAN, CLASSEMENT_COMPLEMENT_BILAN, CLASSEMENT_DOC_REF, CLASSEMENT_INDICE_CONFIANCE, ".$session_old_id.", CLASSEMENT_ME_DATE_MAJ ".
               " from CLASSEMENT_MASSE_EAU
               where MASSE_ID in (select MASSE_ID from MASSE_EAU where BASSIN_ID = ".$bassin_id.")
 							 and SESSION_ID = ".$session_old_id; 
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "update CLASSEMENT_MASSE_EAU set SESSION_ID = ".$session_new_id." where SESSION_ID=".$session_old_id;                
    $bRes = $this->dbConn->executeSql($strSql) ;
    
    $strSql = "update SEQUENCE set SEQ_CLASSEMENT_MASSE_EAU=(select max(CLASSEMENT_ID) from CLASSEMENT_MASSE_EAU) where SEQUENCE_ID = 1";
    $bRes2 = $this->dbConn->executeSql($strSql) ;
    
    return $bRes;   
  }
  
  function copieSessionMasseEau($bassin_id, $session_old_id, $session_new_id) {
    
    $strSql = "insert into MASSE_EAU_SESSION (MASSE_ID, MASSE_CODE, MASSE_NOM, MASSE_TYPE, MASSE_TYPE_SUIVI, MASSE_B_SUIVI,
  																					MASSE_B_RISQUE, MASSE_URL_FICHE, MASSE_DESC_ID, BASSIN_ID, REGION_ID, MASSE_RANG_GEO,
  																					MASSE_B_SUIVI_OP, MASSE_B_ATTEINTE_OBJECTIF, MASSE_OBJECTIF, MASSE_REPORT_OBJECTIF,
  																					MASSE_B_FORT_MODIF, MASSE_DATE_FORT_MODIF, MASSE_B_SUIVI_ENQUETE, MASSE_B_SUIVI_SURVEILLANCE,  MASSE_DOC_DESC, SESSION_ID, MASSE_B_ATTEINTE_OBJ)
               select MASSE_ID, MASSE_CODE, MASSE_NOM, MASSE_TYPE, MASSE_TYPE_SUIVI, MASSE_B_SUIVI,
  																					MASSE_B_RISQUE, MASSE_URL_FICHE, MASSE_DESC_ID, BASSIN_ID, REGION_ID, MASSE_RANG_GEO,
  																					MASSE_B_SUIVI_OP, MASSE_B_ATTEINTE_OBJECTIF, MASSE_OBJECTIF, MASSE_REPORT_OBJECTIF,
  																					MASSE_B_FORT_MODIF, MASSE_DATE_FORT_MODIF, MASSE_B_SUIVI_ENQUETE, MASSE_B_SUIVI_SURVEILLANCE,  MASSE_DOC_DESC, ".$session_old_id.", MASSE_B_ATTEINTE_OBJ ".
               " from MASSE_EAU me
                 where me.BASSIN_ID = ".$bassin_id; 
 
    $bRes = $this->dbConn->executeSql($strSql) ;    

    $strSql = "insert into MASSE_EAU_SESSION_SUIVI (MASSE_ID, SESSION_ID, MASSE_TYPE_SUIVI, MASSE_B_SUIVI_NBPOINT, MASSE_LIST_RESEAU)
               select me.MASSE_ID, ".$session_old_id." , rg.GROUPE_NOM, ".
               $this->dbConn->compareSql("SUM(NB_POINT)", "is", "NULL", "'0'", "SUM(NB_POINT)"). " as NB_POINT,".
                " GROUP_CONCAT(rpoint.RESEAU_NOM SEPARATOR '<br/>') as LIST_RESO".                              
                " from RESEAU_GROUPE rg
                  left join MASSE_EAU me on me.BASSIN_ID = ".$bassin_id."   
									left join RESEAU as r on rg.GROUPE_ID=r.GROUPE_ID and me.BASSIN_ID = r.BASSIN_ID	                
      	        	left join 
                   (select pt.MASSE_ID, COUNT(DISTINCT pt.POINT_ID) as NB_POINT, rpp.RESEAU_ID, reso.RESEAU_NOM 
                    from POINT as pt
                   	inner join RESEAU_POINT_PARAM as rpp on pt.POINT_ID=rpp.POINT_ID 
                    inner join RESEAU as reso on reso.RESEAU_ID=rpp.RESEAU_ID                
                   	group by pt.MASSE_ID, rpp.RESEAU_ID, reso.RESEAU_NOM) as rpoint on rpoint.RESEAU_ID = r.RESEAU_ID and me.MASSE_ID = rpoint.MASSE_ID             
                  where rg.GROUPE_B_PROG_SURV = 1   	
                  group by me.MASSE_ID, rg.GROUPE_ID"; 
                                                                           
    $bRes = $this->dbConn->executeSql($strSql) ;     
    
    return $bRes;   
  }
    
  /**
   * @brief  Modifie un param�tre
   *
   * @param idActu Identifiant de l'actualite
   * 
   * @return boolean False if problem, true otherwise
   */
  function updateProgSurvMe( $bassin_id, $tabMe=array())
  {
     // on met dans un premier tps toutes les valeurs de b_suivi � 0 pour les me du bassin
     $strSql = "update MASSE_EAU me set me.MASSE_B_SUIVI = 0 where me.BASSIN_ID = ".$bassin_id;      
     $this->dbConn->executeSql($strSql) ;
    
     //on recherche par le select les seules masses d'eau contenant des points surveill�s (RESEAU_POINT_PARAM)
     // pour des r�seaux de contr�le du programme de surveillance (RESEAU_GROUPE dont GROUPE_B_PROG_SURV=1
     $strSql = "update MASSE_EAU me".
  		" inner join".
  		" (select distinct pt.MASSE_ID ". 
      " from POINT as pt". 
      " inner join MASSE_EAU as me on pt.MASSE_ID=me.MASSE_ID".              
      " inner join RESEAU_POINT_PARAM as rpp on pt.POINT_ID=rpp.POINT_ID".
      " inner join RESEAU as r on r.RESEAU_ID=rpp.RESEAU_ID". 
      " inner join RESEAU_GROUPE as rg on r.GROUPE_ID=rg.GROUPE_ID and rg.GROUPE_B_PROG_SURV = 1".              
      " inner join BASSIN_HYDROGRAPHIQUE bas on bas.BASSIN_ID = me.BASSIN_ID and bas.BASSIN_ID = ".$bassin_id.
     " ) pt on pt.MASSE_ID = me.MASSE_ID". 
     " set me.MASSE_B_SUIVI = 1";      
                        
     $res = $this->dbConn->executeSql($strSql) ;
      
     return $res;
  }

    
    
}
?>