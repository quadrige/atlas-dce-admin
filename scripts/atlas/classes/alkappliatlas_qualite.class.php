<?php
include_once("classes/alkappliatlas.class.php");
/**
 * @brief Classe de l'application concours
 *        Classe regroupant des fonctionnalit�s de la rubrique param�trage des concours
 */
class AlkAppliAtlas_qualite extends AlkAppliAtlas
{

  /**
   * @brief Constructeur par d�faut
   *
   */
  function AlkAppliAtlas_qualite($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet)
  {
    parent::AlkAppliAtlas($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet);
  }

  /**
   * @brief M�thode virtuelle qui retourne un tableau de boutons htmllink
   *        plac� sur la droite des onglets
   * 
   * @param 
   * @return Retourne un array
   */
  function getTabCtrlBt()
  {
    $iCptBt = 0;
    $tabBt = array();

    return $tabBt;
  }

  /**
   * @brief M�thode virtuelle, retourne un tableau contenant les informations sur les sous onglets
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un array
   */
  function getTabSubSheet()
  {
    global $tabStrType;

    $tabSubSheet = array();

    $strParam = "iSheet=".ALK_SHEET_QUALITE;
    
    $i = 0;
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_PARAMETRE,
                               "text"     => "Param�trage g�n�ral",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_PARAMETRE,
                               "title"    => "Param�trage g�n�ral");
     $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_ELEMENT_QUALITE,
                               "text"     => "El�ments de qualit�",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_ELEMENT_QUALITE,
                               "title"    => "Carte");                                                   
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_DATA,
                               "text"     => "Classement des masses d'eau",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_DATA,
                               "title"    => "Classement des masses d'eau");
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_IMPORT,
                               "text"     => "Import&nbsp;donn�es classement",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_IMPORT."&iModeSSheet=".ALK_MODE_SSHEET_FORM,
                               "title"    => "Import&nbsp;donn�es classement");        
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_ARCHIVAGE,
                               "text"     => "Archivage de la session",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_ARCHIVAGE."&iModeSSheet=".ALK_MODE_SSHEET_FORM,
                               "title"    => "Archivage de la session");   
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_ARCHIVE,
                               "text"     => "Consultation des archives",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_ARCHIVE."&iModeSSheet=".ALK_MODE_SSHEET_LIST,
                               "title"    => "Consultation des archives");   
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_BASSIN,
                               "text"     => "Textes d'alerte",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_BASSIN."&iModeSSheet=".ALK_MODE_SSHEET_FORM."&iMode=2",
                               "title"    => "Textes d'alerte");                                                                                       
    if($_SESSION["bAdminUserAllBassin"] == 1)
      $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_CONTROLE,
                               "text"     => "Contr�le des fichiers",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_CONTROLE."&iModeSSheet=".ALK_MODE_SSHEET_LIST,
                               "title"    => "Textes d'alerte");
    return $tabSubSheet;
  }

  /**
   * @brief M�thode virtuelle, retourne le contenu html du corps de l'onglet s�lectionn�
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un string : code html des sous onglets
   */
  function getHtmlBodySheet()
  {
    $strHtml = "";
    switch( $this->iSSheet ) {        
    case ALK_SSHEET_PARAMETRE:      
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
          $strHtml = $this->traitementDico();
      break;
    case ALK_SSHEET_ELEMENT_QUALITE:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementDico();
      break;  
    case ALK_SSHEET_DATA:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeData();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheEtat();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL ){       
        $strHtml = $this->traitementData();              
      }
      break;		  
		case ALK_SSHEET_IMPORT:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheImport();      
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementImport();
      break;   		
    case ALK_SSHEET_ARCHIVAGE:
      if ($this->iModeSSheet == ALK_MODE_SSHEET_LIST) {
        $bRes = Request("bRes", REQ_GET, "0", "is_numeric");
        //retour apr�s archivage
        $strMessage = "L'archivage ne s'est pas d�roul� correctement. Faites une nouvelle tentative ou contacter l'administrateur.";
        if ($bRes) 
          $strMessage = "L'archivage des donn�es a �t� r�alis�. Vous aurez acc�s aux donn�es ant�rieures depuis la carte.";
        $strHtml = $this->_getFrameTitleSheet("Archivage des donn�es de classement des masses d'eau de l'atlas du bassin").      
      "<table width='580' border='0' cellspacing='0' cellpadding='4' align='center'>".
			  "<tr><td align='center'><div class='divContenuConseil'>".
        $strMessage.
			  "</div>".
			  "</td></tr></table>";          
      }
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheArchivage();      
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )        
        $strHtml = $this->traitementArchivage();                
      break;   
    case ALK_SSHEET_ARCHIVE:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeArchive();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheDico();      
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL ){
        $iMode = Request("iMode", REQ_GET, 1, "is_numeric");
        if ($iMode == 10)
          $strHtml = $this->traitementGenerationShape();
        else
          $strHtml = $this->traitementDico();
      }
      break;  
      
    case ALK_SSHEET_BASSIN:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = "";
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheDico();      
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementDico();
      break;        

    case ALK_SSHEET_CONTROLE:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeControle();      
      break;        
        
         
    default:
      break;
    }
    return $strHtml;
  }

  /**
   * @brief Affiche la liste des options pour une voie de concours
   *
   * @return Retourne un string
   */
  function getHtmlListeDico()
  {    
  	$iErr = Request("err", REQ_GET, "0", "is_numeric");
    $nbEltParPage = Request("nbEltParPage", REQ_POST_GET, 20, "is_numeric");
    $page = Request("page", REQ_GET, 1, "is_numeric");
    
    
    if($_SESSION["bAdminUserAllBassin"] == 1){ 
      $strTableDico = Request("tableDico", REQ_POST_GET, "ETAT");
    }else{
      $strTableDico = Request("tableDico", REQ_POST_GET, "ETAT_MOTIF");    	
    }
    $strParam = "iSheet=".$this->iSheet.
      "&nbEltParPage=".$nbEltParPage."&page=".$page."&tableDico=".$strTableDico."&iSSheet=".$this->iSSheet."&iModeSSheet=";
   	
   	// liste des �l�ments du dictionnaire s�lectionn�
    $cpt = 0;
    $tabPage = array();
    $iFirst = ($page-1)*$nbEltParPage;
    $iLast = $iFirst+$nbEltParPage-1;
      
    switch ($this->iSSheet) {
    	case ALK_SSHEET_PARAMETRE :
    		 switch ($strTableDico) {
    			case "ETAT" :
    				$strTitre = "�tats";  
    				$dsDico = $this->oQuery->getDs_listeEtat($iFirst, $iLast);
    				break;
    			case -1 :
          case "ETAT_MOTIF" :
    				$strTableDico = "ETAT_MOTIF";
            $strTitre = "motifs";
    				$strPrefixe = "MOTIF";
    				$dsDico = $this->oQuery->getDs_listeEtatParametre($strTableDico, $strPrefixe, $iFirst, $iLast, $_SESSION["idBassin"]);    				
    				break;
    			case "METHODE_EVALUATION" :
    				$strTitre = "m�thodes d'�valuation";
    				$strPrefixe = "METHODE";
    				$dsDico = $this->oQuery->getDs_listeMethode($iFirst, $iLast, $_SESSION["idBassin"]);    				
    				break;	
    		 }    		 
    		 
    		break; 
    	case ALK_SSHEET_ELEMENT_QUALITE :
    		$strTitre = "�l�ments de qualit�";
    		$dsDico = $this->oQuery->getDs_listeElementqualite(-1,0,-1,"",$_SESSION["idBassin"]);
    		$dsDico->setTree("ELEMENT_QUALITE_ID", "ELEMENT_QUALITE_PARENT");
    		$iFirst = 0;
    		$iLast = -1;     
    		$nbEltParPage = $dsDico->iCountTotDr; 
    		break;     	   
    } 
    
    $oBtAdd = new HtmlLink("01_page_form.php?iMode=1&".$strParam.ALK_MODE_SSHEET_FORM."&page=".$page,
                           "Ajouter un nouvel �l�ment � la liste",
                           "ajouter.gif", "ajouter_rol.gif");
    
    $nbElt = $dsDico->iCountTotDr;

    $oCtrlSelectDico = new HtmlSelect(0, "tableDico", $strTableDico, "");
    
    if($_SESSION["bAdminUserAllBassin"] == 1 ){   
      $oCtrlSelectDico->tabValTxt = array("ETAT"=>"libell�s des �tats"  , 
																				"ETAT_MOTIF"=>"libell�s des motifs",
																				"METHODE_EVALUATION"=>"m�thodes d'�valuation");
    }else{
      $oCtrlSelectDico->tabValTxt = array(
                                        "ETAT_MOTIF"=>"libell�s des motifs",
                                        "METHODE_EVALUATION"=>"m�thodes d'�valuation");    	
      
    }
    $oCtrlSelectDico->addEvent("onchange", "onChangeDico()");
		$oCtrlSelectDico->tabValTxtDefault = array ("-1", "--- S�lectionner un type de liste ---");
		
    $strHtml = $this->_getFrameTitleSheet("Gestion des ".$strTitre).    
      "<script language='javascript' src='lib/lib_atlas.js'></script>".  
      "<form name='formDico' action='' method='post'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='450' height='10'></td>".      
      ($this->iSSheet == ALK_SSHEET_PARAMETRE && $strTableDico=="ETAT" ? "<td width='140'></td>"
       : "").
      "<td width='120'></td>".      
      "</tr>".
      ( $iErr == "1"
        ? "<tr><td colspan='2' class='divContenuMsgErr' align='center'>".
        "Impossible de supprimer cet enregistrement car il est encore utilis�.<br><br></td></tr>"
        : "").
      "<tr class='trEntete1'>".
      "<td class='tdEntete1' align='left'><div class='divTabEntete1'>Liste des ".
      $strTitre."&nbsp;&nbsp;".$nbElt." enregistrement".($nbElt>1 ? "s" : "")."&nbsp;".($this->iSSheet == ALK_SSHEET_PARAMETRE ? $oCtrlSelectDico->getHtml() : "")."</div>".      
      "</td>".      
      ($this->iSSheet == ALK_SSHEET_PARAMETRE && $strTableDico=="ETAT"? "<td class='tdEntete1' align='center'><div class='divTabEntete1'>Type de classement</div></td>"      																					
       : "").
      "<td class='tdEntete1' align='center'>".$oBtAdd->getHtml()."</td>".
      "</tr>";
   
    while( $drDico = $dsDico->getRowIter() ) {
      $id = $drDico->getValueName("ID");
      $strLib = $drDico->getValueName("LIB");      
      
      $strLib = array(wordwrap($strLib, 100, "<br>"), 
                      "01_page_form.php?iMode=2&".$strParam.ALK_MODE_SSHEET_FORM.
                      "&id=".$id."&page=".$page);

      $iPage = ( $nbElt-1 <= ($page-1)*$nbEltParPage ? ($page-1>0 ? $page-1 : 1) : $page);
      $oBtSuppr = new HtmlLink("javascript:SupprElt('".$id."', '".$strParam.ALK_MODE_SSHEET_SQL.
                               "&page=".$iPage."')", 
                               "Supprimer cet �l�ment", "tab_supprimer.gif", "tab_supprimer_rol.gif");

      if ($this->iSSheet == ALK_SSHEET_PARAMETRE && $strTableDico=="ETAT")
      	$tabPage[$cpt] = array($strLib, $drDico->getValueName("CLASSEMENT"), $oBtSuppr->getHtml());
      else
      	$tabPage[$cpt] = array($strLib, $oBtSuppr->getHtml());
      	
      $cpt++;
    }
    
    if ($this->iSSheet == ALK_SSHEET_PARAMETRE && $strTableDico=="ETAT")
    	$tabAlign = array("", "left", "center", "center");
    else
    	$tabAlign = array("", "left", "center");
    	
    $strHtml .= getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, $page, 
                               $_SERVER["PHP_SELF"]."?".$strParam.ALK_MODE_SSHEET_LIST, 
                               $tabAlign).
      "</form><br>";

    $oBtAnnuler = new HtmlLink("01_page_form.php?".$strParam."&iModeSSheet=".ALK_MODE_SSHEET_LIST."&iMode=2", "Annuler", 
                               "annul_gen.gif", "annul_gen_rol.gif");
                               
    $strHtml .= "<div class='divTextContenu' style='margin-left:20px' align='center'><br>".$oBtAnnuler->getHtml()."</div>";

    return $strHtml;
  }
  
  function _getHtmlLigneCtrl($oCtrl) {
  	$strHtml = "<tr>".
		  							"<td align='right' class='formLabel'>".$oCtrl->label."</div></td>".
		  							"<td>".$oCtrl->getHtml()."</td>".
		  							"</tr>";
		return $strHtml;
  }

  /**
   * @brief Retourne le code html d'un formulaire dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function getHtmlFicheDico()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    $id = Request("id", REQ_GET, "-1", "is_numeric");
    $page =  Request("page", REQ_GET, "1", "is_numeric");
    $strTableDico = Request("tableDico", REQ_POST_GET, "ETAT");
    
    $strParam = "iMode=".$iMode."&page=".$page."&tableDico=".$strTableDico.
      "&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		$strParam2 = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		
		$iWidthTxt = 48;
    $iWidthMemo = 170;
    $iHeightMemo = 4;  
    
		$strNom = $strNomHtml = "";
    $strCode = "";
    $strHtmlCtrl = "";
    $strHtmlSuite = "";
		switch ($this->iSSheet) {
    	case ALK_SSHEET_PARAMETRE :
    		$oCtrl = new HtmlHidden("tableDico", $strTableDico);
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
    						
    		switch ($strTableDico) {
    			case "ETAT" :
    				$strTitre = "�tats";  
    				$dsDico = $this->oQuery->getDs_EtatById($id);
		    		$strCouleur = "";
		    		$strValeur = "";  
		    		$idType = "-1";  		    		
		   			if( $drDico = $dsDico->getRowIter() ) {      
		      			$strNom = $drDico->getValueName("LIB");		      			
		      			$idType = $drDico->getValueName("TYPE_CLASSEMENT_ID");
		      			$strCouleur = $drDico->getValueName("ETAT_COULEUR");		      			
		      			$strValeur = $drDico->getValueName("ETAT_VALEUR");
		    		}
		    		$oCtrl = new HtmlText(0, "libelle", $strNom, "Libell�", 1, $iWidthTxt, 255);
				    $oCtrl->addValidator("formDico", "text", true);
				    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
				    $oCtrlSelect = new HtmlSelect(0, "type_classement_id", $idType, "Type de classement", 1, $iWidthMemo);
						$oCtrlSelect->oValTxt = $this->oQuery->getTableForCombo("TYPE_CLASSEMENT", "TYPE_CLASSEMENT", true);
						$oCtrlSelect->tabValTxtDefault = array ("-1", " --- S�lectionner un type ---");
						$oCtrlSelect->addValidator("formDico", "select", true);											
						$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlSelect);							
				  	$oCtrl = new HtmlColor("couleur", $strCouleur,  "Couleur", 32, 22, "false");
				  	$oCtrl->addValidator("formDico", "text", true);
				    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);			
				  	$oCtrl = new HtmlText(0, "valeur", $strValeur, "Valeur", 1, 10, 3);
				    $oCtrl->addValidator("formDico", "text", true);
				    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);												
				  	
    				break;
    			case "ETAT_MOTIF" :
    				$strTitre = "motifs";
    				$strPrefixe = "MOTIF";
    				$dsDico = $this->oQuery->getDs_EtatMotifById($id);       				    	
		   			if( $drDico = $dsDico->getRowIter() ) {      
		      		$strNom = $drDico->getValueName("LIB");		      					      		
		      		$strCode = $drDico->getValueName("CODE");
		    		}
		    		$oCtrl = new HtmlText(0, "libelle", $strNom, "Libell�", 1, $iWidthTxt, 255);
				    $oCtrl->addValidator("formDico", "text", true);
				    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);				   
				    $oCtrl = new HtmlText(0, "code", $strCode, "Code", 1, 15, 255);
				    $oCtrl->addValidator("formDico", "text", true);
				    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);				     			
    				break;
    			case "METHODE_EVALUATION" :
    				$strTitre = "m�thodes d'�valuation";
    				
    				$dsDico = $this->oQuery->getDs_MethodeById($id);       				    	
		   			if( $drDico = $dsDico->getRowIter() ) {      
		      		$strNom = $drDico->getValueName("LIB");		      					      		
		      		$idTypeClassement = $drDico->getValueName("TYPE_CLASSEMENT_ID");
		      		$idTypeElement = $drDico->getValueName("TYPE_ELEMENT_QUALITE_ID");
		      		$idElement = $drDico->getValueName("ELEMENT_QUALITE_ID");
		    		}
		    		
		    		$oCtrlSelect = new HtmlSelect(0, "type_classement_id", $idTypeClassement, "Type de classement", 1, $iWidthMemo);
				    $oCtrlSelect->oValTxt = $this->oQuery->getTableForCombo("TYPE_CLASSEMENT", "TYPE_CLASSEMENT", true);
				    $oCtrlSelect->tabValTxtDefault = array ("-1", " --- Aucun type ---");
				    $oCtrlSelect->addValidator("formDico", "select", false);			
				    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlSelect);
				    
				    $oCtrlSelect = new HtmlSelect(0, "type_element_qualite_id", $idTypeElement, "Type de l'�l�ment de qualit�", 1, $iWidthMemo);
    				$oCtrlSelect->oValTxt = $this->oQuery->getTableForCombo("TYPE_ELEMENT_QUALITE", "TYPE_ELEMENT_QUALITE", false);
    				$oCtrlSelect->tabValTxtDefault = array ("-1", " --- Aucun type ---");
    				$oCtrlSelect->addValidator("formDico", "select", false);			
    				$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlSelect);
    				
    				$oCtrlSelect = new HtmlSelect(0, "element_qualite_id", $idElement, "El�ment de qualit�", 1, $iWidthMemo);
    				$oCtrlSelect->oValTxt = $this->oQuery->getTableForCombo("ELEMENT_QUALITE", "ELEMENT_QUALITE", false, false, "ELEMENT_QUALITE_NIVEAU=1 and BASSIN_ID=".$_SESSION["idBassin"]);
    				$oCtrlSelect->tabValTxtDefault = array ("0", " --- Aucun �l�ment ---");
    				$oCtrlSelect->addValidator("formDico", "select", false);			
    				$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlSelect);
				    
		    		$oCtrl = new HtmlText(0, "libelle", $strNom, "Description", 5, $iWidthTxt, 255);
				    $oCtrl->addValidator("formDico", "text", true);
				    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);				   
				        			
    				break;	
    		 }    		 
    		    		    		    		  							
    		break;
    	case ALK_SSHEET_ELEMENT_QUALITE :
    		$strTitre = "�l�ments de qualit�";
    		$dsDico = $this->oQuery->getDs_ElementQualiteById($id);
    		$idTypeClassement = "-1";    		
    		$idTypeElement = "-1";
    		$iNiveau = 0;
    		$idParent = $iRang = 0;

    		$strUrlFiche = $strDocProtocole = $strDocMethodo = $strRestrictTypeMe = "";
   			if( $drDico = $dsDico->getRowIter() ) {      
      			$strNom = $drDico->getValueName("ELEMENT_QUALITE_NOM"); 
      			$idTypeClassement = $drDico->getValueName("TYPE_CLASSEMENT_ID");     	
      			$idTypeElement = $drDico->getValueName("TYPE_ELEMENT_QUALITE_ID");
      			$iNiveau = $drDico->getValueName("ELEMENT_QUALITE_NIVEAU");
      			$idParent = $drDico->getValueName("ELEMENT_QUALITE_PARENT");
      			$strUrlFiche = $drDico->getValueName("ELEMENT_QUALITE_URL_FICHE");
      			$strDocProtocole = $drDico->getValueName("ELEMENT_QUALITE_DOC_PROTOCOLE");
      			$strDocMethodo = $drDico->getValueName("ELEMENT_QUALITE_DOC_METHODO");
      			$strRestrictTypeMe = $drDico->getValueName("ELEMENT_QUALITE_RESTRICT_TYPEME");
      			$iRang = $drDico->getValueName("ELEMENT_QUALITE_RANG");
    		}
    		
    		$dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
		    if ($drBassin = $dsBassin->getRowIter())
			    $bassin_code = $drBassin->getValueName("BASSIN_CODE"); 
    		$oCtrl = new HtmlHidden("old_element_qualite_doc_protocole", $strDocProtocole);
    		$oCtrl->addHidden("old_element_qualite_doc_methodo", $strDocMethodo);
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
    		
    		$oCtrl = new HtmlText(0, "nom", $strNom, "Nom", 1, $iWidthTxt, 255);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		    $oCtrlSelect = new HtmlSelect(0, "type_classement_id", $idTypeClassement, "Type de classement", 1, $iWidthMemo);
				$oCtrlSelect->oValTxt = $this->oQuery->getTableForCombo("TYPE_CLASSEMENT", "TYPE_CLASSEMENT", true);
				$oCtrlSelect->tabValTxtDefault = array ("-1", " --- S�lectionner un type ---");
				$oCtrlSelect->addValidator("formDico", "select", true);			
				$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlSelect);
				$oCtrlSelect = new HtmlSelect(0, "type_element_qualite_id", $idTypeElement, "Type d'�tat", 1, $iWidthMemo);
				$oCtrlSelect->oValTxt = $this->oQuery->getTableForCombo("TYPE_ELEMENT_QUALITE", "TYPE_ELEMENT_QUALITE", false);
				$oCtrlSelect->tabValTxtDefault = array ("-1", " --- S�lectionner un type ---");
				$oCtrlSelect->addValidator("formDico", "select", true);			
				$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlSelect);
				$oCtrlSelect = new HtmlSelect(0, "element_qualite_parent", $idParent, "Parent de l'�l�ment de qualit�", 1, $iWidthMemo);
				$oCtrlSelect->oValTxt = $this->oQuery->getTableForCombo("ELEMENT_QUALITE", "ELEMENT_QUALITE", false, false, "ELEMENT_QUALITE_NIVEAU=1 and BASSIN_ID=".$_SESSION["idBassin"]);
				$oCtrlSelect->tabValTxtDefault = array ("0", " --- Aucun parent ---");
				$oCtrlSelect->addValidator("formDico", "select", false);			
				$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlSelect);
				$oCtrl = new HtmlText(0, "element_qualite_url_fiche", $strUrlFiche, "Url de la fiche de synth�se de l'�l�ment de qualit�", 1, $iWidthTxt, 255);
		    $oCtrl->addValidator("formDico", "text", false);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		    $oCtrl = new HtmlFile(0, "element_qualite_doc_protocole", $strDocProtocole, "Document du protocole d'�chantillonnage et d'analyse des �chantillons", $iWidthTxt, 255);
		    $oCtrl->urlFile = ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$bassin_code."/".$strDocProtocole;
		    $oCtrl->bDualMode = true;
  			$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
  			$oCtrl = new HtmlFile(0, "element_qualite_doc_methodo", $strDocMethodo, "Document de m�thodologie de calcul de l'indicateur", $iWidthTxt, 255);
  			$oCtrl->urlFile = ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$bassin_code."/".$strDocMethodo;
  			$oCtrl->bDualMode = true;
  			$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
  			$oCtrlCB = new HtmlRadioGroup(0, "element_qualite_restrict_typeme", $strRestrictTypeMe, "Restriction � un type de masse d'eau");  	
  			$oCtrlCB->AddRadio("", "Aucune restriction");	
  			$oCtrlCB->AddRadio("MEC", "Masses d'eau c�ti�res uniquement");
  			$oCtrlCB->AddRadio("MET", "Masses d'eau de transition uniquement");
  			$oCtrlCB->addValidator("formDico", "radio", true);  	
				$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB);
				$oCtrl = new HtmlText(0, "rang", $iRang, "Rang d'affichage", 1, 5, 255);
		    $oCtrl->addValidator("formDico", "num", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);		
        
 
    		break; 
    		  
    	case ALK_SSHEET_ARCHIVE :
    		$strTitre = "sessions de qualit�";
    		$dsDico = $this->oQuery->getDs_SessionByBassin($_SESSION["idBassin"], "", 0,-1, $id);    		
    		if( $drDico = $dsDico->getRowIter() ) {      
      			$strNom = $drDico->getValueName("SESSION_LIBELLE");       			
    		}    		
    		$oCtrl = new HtmlText(0, "nom", $strNom, "Nom", 1, $iWidthTxt, 255);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);		    
    		break;   

    	case ALK_SSHEET_BASSIN :
        $strTitre = "phrases d'alerte par bassin.";
        $id = $_SESSION["idBassin"];
        $dsDico = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);                    
        if( $drDico = $dsDico->getRowIter() ) {      
          $strNom = $drDico->getValueName("BASSIN_ALERT_CARTE");               
          $strNomHtml = $drDico->getValueName("BASSIN_ALERT");
        }    
         
        $oCtrl = new HtmlText(0, "nonhtml", $strNom, "Texte", 5, $iWidthTxt, 255);
        $oCtrl->addValidator("formDico", "text", false);
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);     
       
        $oCtrlHtml = new HtmlText(0, "txtHtml", $strNomHtml, "Texte html", 7, $iWidthTxt, 255);
        $oCtrlHtml->addValidator("formDico", "text", false);
        $oCtrlHtml->setEditor(true);   
        
        $strPathUpload = ALK_PATH_UPLOAD_DOC;
        
        $oCtrlHtml->setParamEditor("openPopup", false);
        //$oCtrlHtml->setParamEditor("data_id", $this->data_id->value);
        //$oCtrlHtml->setParamEditor("tableName", "BASSIN_HYDROGRAPHIQUE");
        //$oCtrlHtml->setParamEditor("fieldPk", "DATA_ID");
        $oCtrlHtml->setParamEditor("userForm", false);
        $oCtrlHtml->setParamEditor("lg", "fr");
        $oCtrlHtml->setParamEditor("pathUpload", $strPathUpload);
        // $oCtrlHtml->setParamEditor("tokenStart", "cont_id=".$this->contID."&appli_id=".$this->appliId);
        //$oCtrlHtml->setParamEditor("styleCss", "editeur.css");
        $oCtrlHtml->setParamEditor("bodyId", ( defined("ALK_EDITEUR_BODYID") ? ALK_EDITEUR_BODYID : "" ));   
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlHtml);		 	  
    	  break;		
    } 	
    $strHtml = $this->_getFrameTitleSheet("<a class='LienSommaire' href='01_page_form.php?".
                                          $strParam.ALK_MODE_SSHEET_LIST."'>Liste ".
                                          $strTitre."</a> / Fiche ").
      "<script language='javascript' src='lib/lib_atlas.js'></script>".                                    
      "<script language='javascript' src='../../lib/lib_formTxt.js'></script>".
      "<script language='javascript' src='../../lib/lib_formSelect.js'></script>".
      "<script language='javascript' src='../../lib/lib_formNumber.js'></script>".
      "<script language='javascript' src='../../lib/lib_formDate.js'></script>".
      "<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<script language='javascript'>".
      	writeJsColor("formDico", "couleurs.php", ALK_SIALKE_URL."media/admin/transp.gif", ALK_SIALKE_URL."media/admin/transp.gif", "", false).
      "</script>".                                    
      "<form name='formDico' action='01_page_sql.php?".$strParam.ALK_MODE_SSHEET_SQL."&id=".$id.
      "' method='post'  enctype='multipart/form-data'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='200' height='10'></td>".
      "<td width='500'></td>".
      "</tr>";
          
    $strHtml .= $strHtmlCtrl;
    
    $oBtValid = new HtmlLink("javascript:ValiderDico('".$this->iSSheet."')", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    $oBtAnnul = new HtmlLink("01_page_form.php?".$strParam, "Annuler les modifications", 
                             "annul_gen.gif", "annul_gen_rol.gif");
		
    $strHtml .= "</table><br><div align='center'>".
     						$oBtValid->getHtml()."&nbsp;&nbsp;".$oBtAnnul->getHtml()."</div>".
      					"</form>";

		$strHtml .= $strHtmlSuite;
		
    return $strHtml;
  
  }
  
  /**
   * @brief Retourne le code html d'un formulaire dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function getHtmlFicheImport()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    
    $strParam = "iMode=".$iMode.
      "&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		
		$iWidthTxt = 48;
    $iWidthMemo = 46;
    $iHeightMemo = 4;  
    		
    $strHtmlCtrl = "";    
    
    $strPath = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT;
        
	  $oCtrl = new HtmlFile(0, "imp_fic_classement", "", "Fichier � importer", $iWidthTxt, 255);
  	$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		
		$oBtVal = new HtmlLink("javascript:Valider();", "Valider",
		                       "valid_gen.gif", "valid_gen_rol.gif");
		$oBtAnn = new HtmlLink("javascript:Annuler()", "Annuler", 
		                       "annul_gen.gif", "annul_gen_rol.gif");
		
		$strHtml = $this->_getFrameTitleSheet("Import de donn�es de classement des masses d'eau de l'atlas").
      "<script language='javascript' src='lib/lib_atlas.js'></script>".                                    
      "<script language='javascript' src='../../lib/lib_formTxt.js'></script>".
      "<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<form name='formDico' action='01_page_form.php?".$strParam.ALK_MODE_SSHEET_SQL.
      "' method='post'  enctype='multipart/form-data'>".
      "<table width='420' border='0' cellspacing='0' cellpadding='4' align='center'>".
			  "<tr><td align='center'><div class='divContenuConseil'>".
			  "Pour r&eacute;aliser un import de donn&eacute;es dans l'atlas, vous devez au pr&eacute;alable prendre".
			  " connaissance du <a class='aContenuConseil' href='import_format.pdf' target='blank'>format</a> et des".
			  " <a class='aContenuConseil' href='import_regles.pdf' target='blank'>r&egrave;gles d'importation".
			  "</a>.<br> Lorsque l'import est termin&eacute;, le syst&egrave;me renvoie une adresse vous permettant de".
			  " consulter le compte-rendu de l'importation.<br> Plusieurs exemples de fichiers d'import sont disponibles".
			  "&nbsp;: <a class='aContenuConseil' href='import_ex3.txt' target='blank'>exemple 1</a>.</div>".
			  "</td></tr></table>".
      
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='200' height='10'></td>".
      "<td width='500'></td>".
      "</tr>";
          
    $strHtml .= $strHtmlCtrl;
    
    $oBtValid = new HtmlLink("javascript:ValiderImport('imp_fic_classement')", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    $oBtAnnul = new HtmlLink("01_page_form.php?".$strParam, "Annuler", 
                             "annul_gen.gif", "annul_gen_rol.gif");

    $strHtml .= "</table><br><div align='center'>".
     						$oBtValid->getHtml()."&nbsp;&nbsp;".$oBtAnnul->getHtml()."</div>".
      					"</form>";

    return $strHtml;
  
  }  

    /**
   * @brief Retourne le code html d'un formulaire dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function getHtmlFicheArchivage()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    
    $strParam = "iMode=".$iMode.
      "&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		
		$iWidthTxt = 48;
    $iWidthMemo = 46;
    $iHeightMemo = 4;  
    		
    $strHtmlCtrl = "";    

    $dsSession = $this->oQuery->getDs_SessionByBassin($_SESSION["idBassin"]);
    if ($drSession = $dsSession->getRowIter()){
      $strNom = $drSession->getValueName("SESSION_LIBELLE");
      $strBassin = $drSession->getValueName("BASSIN_NOM");
      $session_id = $drSession->getValueName("SESSION_ID");
    }
    
    $strPath = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT;        
	 
    $oCtrl = new HtmlText(0, "libelle", $strNom, "Libell� de la prochaine session active", 1, $iWidthTxt, 255);
    $oCtrl->addValidator("formDico", "text", true);
    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
    
    $oCtrl = new HtmlHidden("old_session_id", $session_id);    		
    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
    		
    
		$oBtVal = new HtmlLink("javascript:Valider();", "Valider",
		                       "valid_gen.gif", "valid_gen_rol.gif");
		$oBtAnn = new HtmlLink("javascript:Annuler()", "Annuler", 
		                       "annul_gen.gif", "annul_gen_rol.gif");
		
		$strHtml = $this->_getFrameTitleSheet("Archivage des donn�es de classement des masses d'eau de l'atlas du bassin").
      "<script language='javascript' src='lib/lib_atlas.js'></script>".                                    
      "<script language='javascript' src='../../lib/lib_formTxt.js'></script>".
      "<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<form name='formDico' action='01_page_sql.php?".$strParam.ALK_MODE_SSHEET_SQL.
      "' method='post'  enctype='multipart/form-data'>".
      "<table width='580' border='0' cellspacing='0' cellpadding='4' align='center'>".
			  "<tr><td align='center'><div class='divContenuConseil'>".
			  "L'archivage permet de sauvegarder les �tats de classement pour le bassin ".$strBassin.".<br/>".
		    "Apr�s confirmation, vous allez archiver la session actuelle du bassin ".
		    " nomm�e \"".$strNom."\"<br/>".
			  " Les �tats de classement sont recopi�s pour la session suivante.<br/>".
		    "Seuls les administrateurs pourront consulter les sessions archiv�es.</div>".
			  "</td></tr></table>".
      
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='200' height='10'></td>".
      "<td width='500'></td>".
      "</tr>";
          
    $strHtml .= $strHtmlCtrl;
    
    $oBtValid = new HtmlLink("javascript:ValiderArchivage()", "Confirmer l'archivage", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    $oBtAnnul = new HtmlLink("01_page_form.php?".$strParam, "Annuler", 
                             "annul_gen.gif", "annul_gen_rol.gif");

    $strHtml .= "</table><br><div align='center'>".
     						$oBtValid->getHtml()."&nbsp;&nbsp;".$oBtAnnul->getHtml()."</div>".
      					"</form>";

    return $strHtml;
  
  }  
  
  /**
   * @brief Traitement des actions sur dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function traitementDico()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    $id = Request("id", REQ_GET, "-1", "is_numeric");
	  $page =  Request("page", REQ_GET, "1", "is_numeric");
    $strTableDico = Request("tableDico", REQ_POST_GET, "ETAT");
	  
    $strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_LIST."&page=".$page;
    $strUrlBack = "01_page_form.php?".$strParam;

    $oldRang = "-1";
		$tabValue = array();	    			
		
		switch ($this->iSSheet) {
			case ALK_SSHEET_PARAMETRE:
				switch ($strTableDico) {
    			case "ETAT" :
    				$tabValue["ETAT_LIBELLE"] = array( 0, Request("libelle", REQ_POST, ""));
	    			$tabValue["TYPE_CLASSEMENT_ID"] = array( 1, Request("type_classement_id", REQ_POST, ""));
	    			$tabValue["ETAT_COULEUR"] = array( 0, Request("couleur", REQ_POST, ""));
	    			$tabValue["ETAT_VALEUR"] = array( 1, Request("valeur", REQ_POST, ""));
	    			$strFieldKey = "ETAT_ID";
	    			$strTable = $strTableDico;	    			    				
    				break;
    			case "ETAT_MOTIF" :    			  
						$tabValue["MOTIF_LIBELLE"] = array( 0, Request("libelle", REQ_POST, ""));
			    	$tabValue["MOTIF_CODE"] = array( 0, Request("code", REQ_POST, ""));
			    	$tabValue["BASSIN_ID"] = array( 1, $_SESSION["idBassin"]);			    
			    	$strFieldKey = "MOTIF_ID";
			    	$strTable = $strTableDico;
			    	break;
			    case "METHODE_EVALUATION" :
						$tabValue["METHODE_LIBELLE"] = array( 0, Request("libelle", REQ_POST, ""));
			    	$tabValue["TYPE_CLASSEMENT_ID"] = array( 1, Request("type_classement_id", REQ_POST, ""));			    
			    	$tabValue["TYPE_ELEMENT_QUALITE_ID"] = array( 1, Request("type_element_qualite_id", REQ_POST, ""));
			    	$tabValue["ELEMENT_QUALITE_ID"] = array( 1, Request("element_qualite_id", REQ_POST, ""));
			    	$tabValue["BASSIN_ID"] = array( 1, $_SESSION["idBassin"]);
			    	$strFieldKey = "METHODE_ID";
			    	$strTable = $strTableDico;	
    		break;
				}
			break;
     case ALK_SSHEET_ELEMENT_QUALITE : 
        $dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
		    if ($drBassin = $dsBassin->getRowIter())
			    $bassin_code = $drBassin->getValueName("BASSIN_CODE");
			     
     		//Traitement des fichiers � upploader
     		$iSupProtocole = RequestCheckbox("element_qualite_doc_protocole", REQ_POST);
				$strFileNameProtocole = DoUpload("element_qualite_doc_protocole", "", ALK_PATH_UPLOAD_DOC.$bassin_code."/", $iSupProtocole, Request("old_element_qualite_doc_protocole", REQ_POST, ""));

				$iSupMethodo = RequestCheckbox("element_qualite_doc_methodo", REQ_POST);
				$strFileNameMethodo = DoUpload("element_qualite_doc_methodo", "", ALK_PATH_UPLOAD_DOC.$bassin_code."/", $iSupMethodo, Request("old_element_qualite_doc_methodo", REQ_POST, ""));

				$tabValue["BASSIN_ID"] = array( 1, $_SESSION["idBassin"]);
				$tabValue["ELEMENT_QUALITE_NOM"] = array( 0, Request("nom", REQ_POST, ""));
    		$tabValue["TYPE_CLASSEMENT_ID"] = array( 1, Request("type_classement_id", REQ_POST, ""));
    		$tabValue["TYPE_ELEMENT_QUALITE_ID"] = array( 1, Request("type_element_qualite_id", REQ_POST, ""));
    		$tabValue["ELEMENT_QUALITE_PARENT"] = array( 1, Request("element_qualite_parent", REQ_POST, ""));
    		$tabValue["ELEMENT_QUALITE_NIVEAU"] = array( 1, ($tabValue["ELEMENT_QUALITE_PARENT"][1] == 0 ? 1 : 2));      		
    		$tabValue["ELEMENT_QUALITE_URL_FICHE"] = array( 0, Request("element_qualite_url_fiche", REQ_POST, ""));
    		if ($iSupProtocole || $strFileNameProtocole != "") 
    			$tabValue["ELEMENT_QUALITE_DOC_PROTOCOLE"] = array( 0, $strFileNameProtocole);
    		if ($iSupMethodo || $strFileNameMethodo != "") 
    			$tabValue["ELEMENT_QUALITE_DOC_METHODO"] = array( 0, $strFileNameMethodo);    
    		$tabValue["ELEMENT_QUALITE_RESTRICT_TYPEME"] = array( 0, Request("element_qualite_restrict_typeme", REQ_POST, ""));
    		$tabValue["ELEMENT_QUALITE_RANG"] = array( 1, Request("rang", REQ_POST, "0"));	
	    	
    		$strFieldKey = "ELEMENT_QUALITE_ID";
	    	$strTable = "ELEMENT_QUALITE";
    		break;     			
     case ALK_SSHEET_ARCHIVE :  		
				$tabValue["SESSION_LIBELLE"] = array( 0, Request("nom", REQ_POST, ""));
    		
    		$strFieldKey = "SESSION_ID";
	    	$strTable = "SESSION_QUALITE";
    		break; 
     case ALK_SSHEET_BASSIN :               
       $tabValue["BASSIN_ALERT"] = array( 0, Request("txtHtml", REQ_POST_GET, ""));
    	 $tabValue["BASSIN_ALERT_CARTE"] = array( 0, Request("nonhtml", REQ_POST_GET, ""));			    
    	 $strFieldKey = "BASSIN_ID";
    	 $strTable = "BASSIN_HYDROGRAPHIQUE";	
			 break;		    	
		} 
    switch( $iMode ) {
    case "1": // mode ajout
    case "2": // mode modif           
      
      if( $iMode == "1" ) {
        $id = $this->oQueryAction->add_ficheDico($strTable, $strFieldKey, $tabValue);
      } else {
        $this->oQueryAction->update_ficheDico($strTable, $strFieldKey, $id, $tabValue);
      }
      if ($this->iSSheet == ALK_SSHEET_ELEMENT_QUALITE ) {
      	//maj apr�s ajout ou modif de l'arbre de parent�
      	$strArbre = ($tabValue["ELEMENT_QUALITE_PARENT"][1] == 0 ? "-".$id."-" : "-".$tabValue["ELEMENT_QUALITE_PARENT"][1]."-".$id."-");     	
        $idbassin = $tabValue["BASSIN_ID"][1];
        $tabValue = array();
      	$tabValue["ELEMENT_QUALITE_ARBRE"] 	= array( 0,  $strArbre);
      	$this->oQueryAction->update_ficheDico($strTable, $strFieldKey, $id, $tabValue);    		
      }
      break;

    case "3": // mode suppr
      $bRes = $this->oQueryAction->del_ficheDico($strTable, $strFieldKey, $id);
      if( $bRes == false ) {
        $strUrlBack .= "&err=1";
      }
      break;
    }
    return $strUrlBack;
  }  

  /**
   * @brief Affiche la liste des options pour une voie de concours
   *
   * @return Retourne un string
   */
  function getHtmlListeData()
  {    
  	$iErr = Request("err", REQ_GET, "0", "is_numeric");
  	$page =  Request("page", REQ_GET, "1", "is_numeric");
		$nbEltParPage = Request("nbEltParPage", REQ_POST_GET, 20, "is_numeric");
		  	
		$dept_id = Request("dept_id", REQ_POST, "-1", "is_numeric");
		$masse_id = Request("masse_id", REQ_POST_GET, "-1", "is_numeric");
		
    $strParam = "iSheet=".$this->iSheet.
      					"&iSSheet=".$this->iSSheet."&iModeSSheet=";
   	
     	// liste des �l�ments du dictionnaire s�lectionn�
      $cpt = 0;
      $tabPage = array();
          
      $oCtrlSelectDept = new HtmlSelect(0, "dept_id", $dept_id, "D�partement");
  		//$oCtrlSelectDept->oValTxt = $this->oQuery->getTableForCombo("DEPARTEMENT", "DEPT", false, true, "DEPT_VISIBLE=1");    		
  		$oCtrlSelectDept->oValTxt = $this->oQuery->getDeptForCombo($idFirst=0, $idLast=-1, $_SESSION["idBassin"], "DEPT_VISIBLE=1");
      $oCtrlSelectDept->addEvent("onchange", "majPoint();");
  		$oCtrlSelectDept->tabValTxtDefault = array ("-1", "--- S�lectionner un d�partement ---");
        
  		$oCtrlSelectMasse = new HtmlSelect(0, "masse_id", $masse_id, "Masse d'eau");
  		$oCtrlSelectMasse->oValTxt = $this->oQuery->getMasseForCombo(-1, 0, -1, $_SESSION["idBassin"], $dept_id);
  		$oCtrlSelectMasse->tabValTxtDefault = array ("-1", "--- S�lectionner une masse d'eau ---");
  		$oCtrlSelectMasse->addEvent("onchange", "Recherche('');");
      		
  		$strHtml = "<table cellpadding=0 cellspacing=0 border=0><tr><td>".			
  			"<script language='javascript' src='../../lib/lib_formNumber.js'></script>".
  			"<script language='javascript' src='../../lib/lib_formDate.js'></script>".
  			"<script language='javascript' src='../../lib/lib_form.js'></script>".
        "<script language='javascript' src='lib/lib_atlas.js'></script>".          
        "<form name='formData' action='01_page_form.php?iMode=1&".$strParam.ALK_MODE_SSHEET_LIST."' method='post' enctype='multipart/form-data'>".
        "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
        "<tr>".
        "<td width='400' valign=top>" .
        	$this->_getFrameTitleSheet("Recherche de la masse d'eau").          	
        	"<table width=100% cellpadding=2 cellspacing=2>" .
        	$this->_getHtmlLigneCtrl($oCtrlSelectDept).
        	$this->_getHtmlLigneCtrl($oCtrlSelectMasse).      	
        	"</table>" .
        "</td>".      
        "<td width='580' valign=top>";
        	
       if ($masse_id != "-1"){  	
  			$strHtml .= $this->_getFrameTitleSheet("Gestion du classement de la masse d'eau").      
  	      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
  	      "<tr>".
  	      "<td width='250' height='10'></td>".
  	      "<td width='150'></td>".	      
  				"<td width='50'></td>".
  				"<td width='50'></td>".
  				"<td width='50'></td>".
  	      "</tr>".
  		      ( $iErr == "1"
  		        ? "<tr><td colspan='2' class='divContenuMsgErr' align='center'>".
  		        "Impossible de supprimer cet enregistrement car il est encore utilis�.<br><br></td></tr>"
  		        : "").
  		      "<tr class='trEntete1'>".
  		      "<td class='tdEntete1' align='left'><div class='divTabEntete1'>Liste des types et �l�ments de qualit�</div></td>".            		    
  					"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Etat</div></td>".		      		      
  		      "<td class='tdEntete1' align='center'></td>".
  		      "<td class='tdEntete1' align='center'><div class='divTabEntete1'>Niveau confiance</div></td>".	
  		      "<td class='tdEntete1' align='center'><div class='divTabEntete1'>Motif</div></td>".
  		      "<td class='tdEntete1' align='center'><div class='divTabEntete1'>Statut</div></td>".
  		      "</tr>";
     
  		$oMasseEau = new AlkMasseEau("", $this->oQuery, $masse_id);
  		$oMasseEau->setEtatQualite("", false);
  		$tabClassement = $oMasseEau->getEtatQualite();
  		foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {		
  			if ($nomClassement == "Global"){
  			  $strLib = array($nomClassement,			      								 
  			                       "javascript:void(OpenWindow('01_page_popup.php?".$strParam.ALK_MODE_SSHEET_FORM."&codeMe=".$oMasseEau->codeMe."&nomElem=me&elem=me&masse_id=".$masse_id."', 250, 500))");

		      $tabPage[$cpt] = array($strLib, "Calcul�", "-",
		                          (array_key_exists("INDICE_CONFIANCE", $tabClassement[$nomClassement]["ETAT"]) ? $tabClassement[$nomClassement]["ETAT"]["INDICE_CONFIANCE"] : ""),  
		      										" - ",
		      										" - ");      	
		      $cpt++;					
  			} else {  
  				foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
  					if ($nomTypeElement != "ETAT"){						
  			      $strLib = array("--&nbsp;".$nomTypeElement,			      								 
  			                       "javascript:void(OpenWindow('01_page_popup.php?".$strParam.ALK_MODE_SSHEET_FORM."&codeMe=".$oMasseEau->codeMe."&nomElem=".$nomTypeElement."&elem=type&id=".$tabEtat["ID"]."&masse_id=".$masse_id."', 550, 700))");

  			      $tabPage[$cpt] = array($strLib, $tabEtat["ETAT"], "<div class='divLeg' style='background-color:".$tabEtat["COULEUR"].";float:right;
  																									height:16px;
  																									position:relative;	
  																									width:20px;																									
  																									z-index:0;'></div>",
  			                          $tabEtat["INDICE_CONFIANCE"],  
  			      										" - ",
  			      										" - ");      	
  			      $cpt++;												
  						if (count($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"]) > 0){
  							foreach ($tabClassement[$nomClassement][$nomTypeElement]["ELEMENTS"]["BYNUM"] as $i=>$tabEtatElement) {
  								$strLib = array(substr("-------------", 0, $tabEtatElement["NIVEAU"]*5)."&nbsp;".$tabEtatElement["NOM"],			      								 
                               "javascript:void(OpenWindow('01_page_popup.php?".$strParam.ALK_MODE_SSHEET_FORM."&codeMe=".$oMasseEau->codeMe."&nomElem=".$tabEtatElement["NOM"]."&elem=elem&id=".$tabEtatElement["ID"]."&masse_id=".$masse_id."', ".($tabEtatElement["NIVEAU"] == 1 ? "550" : "300").", 700))");
                 
                  $tabPage[$cpt] = array($strLib, $tabEtatElement["ETAT"],"<div class='divLeg' style='background-color:".$tabEtatElement["COULEUR"].";float:right;
  																											height:16px;
  																											position:relative;	
  																											width:20px;																											
  																											z-index:0;'></div>",
                                  $tabEtatElement["INDICE_CONFIANCE"],  
  			      										$tabEtatElement["MOTIF"],
  			      										$tabEtatElement["STATUT"]);
  	      	
  				      	$cpt++;					      														
  							}
  						}
  					}	else {
  					  $strLib = array($nomClassement,			      								 
  			                       "javascript:void(OpenWindow('01_page_popup.php?".$strParam.ALK_MODE_SSHEET_FORM."&codeMe=".$oMasseEau->codeMe."&nomElem=".$nomClassement."&elem=class&id=".$tabEtat["ID"]."&masse_id=".$masse_id."', 250, 500))");

  			      $tabPage[$cpt] = array($strLib, "Calcul�", "-",
  			                          (array_key_exists("INDICE_CONFIANCE", $tabClassement[$nomClassement]["ETAT"]) ? $tabClassement[$nomClassement]["ETAT"]["INDICE_CONFIANCE"] : ""),  
  			      										" - ",
  			      										" - ");      	
  			      $cpt++;												
  					}						 
  				}		
  			}
  		}					    
      
      // pas de pagination
      $nbEltParPage = $nbElt = $cpt;    
      $tabAlign = array("", "left", "center", "center", "center", "center", "center");
      	
      $strHtml .= getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, 1, 
                                 $_SERVER["PHP_SELF"]."?".$strParam.ALK_MODE_SSHEET_LIST, 
                                 $tabAlign).
        "<br>";
                 
      }                           
  		$strHtml .= "</td></tr></table></form>";
		
    return $strHtml;
  }  
  
  
  /**
   * @brief Affiche la liste des options pour une voie de concours
   *
   * @return Retourne un string
   */
  function getHtmlListeArchive()
  {    
  	$iErr = Request("err", REQ_GET, "0", "is_numeric");
    $nbEltParPage = Request("nbEltParPage", REQ_POST_GET, 20, "is_numeric");
    $page = Request("page", REQ_GET, 1, "is_numeric");
    
    $strParam = "iSheet=".$this->iSheet.
      "&nbEltParPage=".$nbEltParPage."&page=".$page."&iSSheet=".$this->iSSheet."&iModeSSheet=";
   	
   	// liste des �l�ments du dictionnaire s�lectionn�
    $cpt = 0;
    $tabPage = array();
    $iFirst = ($page-1)*$nbEltParPage;
    $iLast = $iFirst+$nbEltParPage-1;
          
    $dsDico = $this->oQuery->getDs_SessionByBassin($_SESSION["idBassin"], "", $iFirst, $iLast);    
    $nbElt = $dsDico->iCountTotDr;
    
    $strHtml = $this->_getFrameTitleSheet("Liste des sessions de qualit�").    
      "<script language='javascript' src='lib/lib_atlas.js'></script>".  
      "<form name='formDico' action='' method='post'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='250' height='10'></td>".
    	"<td width='100'></td>".      
    	"<td width='70'></td>".      
    	"<td width='70'></td>". 
      "<td width='100'></td>".
    	"<td width='150'></td>".
      "<td width='150'></td>".              
      "</tr>".      
      "<tr class='trEntete1'>".
      "<td class='tdEntete1' align='left'><div class='divTabEntete1'>Liste des sessions de qualit�".
      "&nbsp;&nbsp;".$nbElt." enregistrement".($nbElt>1 ? "s" : "")."&nbsp;</div>".      
      "</td>". 
    	"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Cr��e le</div></td>".        
    	"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Archiv�e le</div></td>".              
    	"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Archive</div></td>".
      "<td class='tdEntete1' align='center'><div class='divTabEntete1'>Acc�der � la carte</div></td>".
    	"<td class='tdEntete1' align='center'><div class='divTabEntete1'>T�l�charger les shapes pour Sextant</div></td>".
    	"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Reg�n�rer les shapes pour Sextant</div></td>".
      "</tr>";
   
    while( $drDico = $dsDico->getRowIter() ) {
      $id = $drDico->getValueName("SESSION_ID");
      $strLib = $drDico->getValueName("SESSION_LIBELLE");      
      
      $strLib = array(wordwrap($strLib, 100, "<br>"), 
                      "01_page_form.php?iMode=2&".$strParam.ALK_MODE_SSHEET_FORM.
                      "&id=".$id."&page=".$page);
      
      $iPage = ( $nbElt-1 <= ($page-1)*$nbEltParPage ? ($page-1>0 ? $page-1 : 1) : $page);
      $oBtConsult = new HtmlLink("javascript:void(OpenPopupWindow('../site/carte.php?map=".$drDico->getValueName("BASSIN_CODE").
      																														($drDico->getValueName("B_ARCHIVE")=='oui' ? "&idSession=".$id : "")."', '800', '1000', 'carte".$id."', 'no', 'no')) ;", 
                               "Consulter la carte", "rechercher_icon.gif", "rechercher_icon_rol.gif");

      // archive des shp en zip
      $strLibZip = "archive_shp_".$id.".zip";  
      $strPathRepArchive = ALK_SIALKE_PATH.'upload/sextant/'.$drDico->getValueName("BASSIN_CODE").'/archive_'.$id;       
      $oBtGenerer = new HtmlLink("01_page_sql.php?".$strParam.ALK_MODE_SSHEET_SQL."&iMode=10&old_session_id=".$id, "G�n�rer les shapes");
      
      if($drDico->getValueName("B_ARCHIVE")=="oui" && is_file($strPathRepArchive."/".$strLibZip)){
        $strLibZip = "archive_shp_".$id.".zip";       
        $strLibZip = array(wordwrap($strLibZip, 100, "<br>"), 
                      "../../upload/sextant/".$drDico->getValueName("BASSIN_CODE")."/archive_".$id."/archive_shp_".$id.".zip");
        $strNewGen = $oBtGenerer->getHtml();      
      }else if($drDico->getValueName("B_ARCHIVE")=="oui"){
      	$strLibZip = $oBtGenerer->getHtml();      	
      	$strNewGen = "";
      } else {
        $strLibZip = "";      	
      	$strNewGen = "";
      }
      $tabPage[$cpt] = array($strLib, 
                             $drDico->getValueName("SESSION_DATE")." par ".$drDico->getValueName("USER"),
      											 $drDico->getValueName("SESSION_DATE_ARCHIVE"),
                             $drDico->getValueName("B_ARCHIVE"),
                             $oBtConsult->getHtml(),
                             $strLibZip,
                             $strNewGen);
      	
      $cpt++;
    }
        
    $tabAlign = array("", "left", "left","center","center","center","center","center", "center");
    	
    $strHtml .= getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, $page, 
                               $_SERVER["PHP_SELF"]."?".$strParam.ALK_MODE_SSHEET_LIST, 
                               $tabAlign).
      "</form><br>";

    $oBtAnnuler = new HtmlLink("01_page_form.php?".$strParam."&iModeSSheet=".ALK_MODE_SSHEET_LIST."&iMode=2", "Annuler", 
                               "annul_gen.gif", "annul_gen_rol.gif");
                               
    $strHtml .= "<div class='divTextContenu' style='margin-left:20px' align='center'><br>".$oBtAnnuler->getHtml()."</div>";

    return $strHtml;
  }  
  
  /**
   * @brief Affiche la liste des options pour une voie de concours
   *
   * @return Retourne un string
   */
  function getHtmlFicheEtat()
  {    
  	$iErr = Request("err", REQ_GET, "0", "is_numeric");
  	$page =  Request("page", REQ_GET, "1", "is_numeric");
		$nbEltParPage = Request("nbEltParPage", REQ_POST_GET, 20, "is_numeric");
		  	
		$dept_id = Request("dept_id", REQ_POST, "-1", "is_numeric");
		$masse_id = Request("masse_id", REQ_POST_GET, "-1", "is_numeric");
		$id = Request("id", REQ_GET, "-1", "is_numeric");
		$typeElement = Request("elem", REQ_GET, "");
		
    $strParam = "iSheet=".$this->iSheet.
      					"&iSSheet=".$this->iSSheet."&iModeSSheet=";
   	
    
  	$strHtml = "<table cellpadding=0 cellspacing=0 border=0><tr><td>".			
  			"<script language='javascript' src='../../lib/lib_formNumber.js'></script>".
  			"<script language='javascript' src='../../lib/lib_formDate.js'></script>".
  			"<script language='javascript' src='../../lib/lib_form.js'></script>".
        "<script language='javascript' src='lib/lib_atlas.js'></script>".         
          "<div class='popinTitle'>&nbsp;Description d'un �l�ment de qualit�</div>".          
          "<div class='popupBtClose'>".
          "<div class='btClose' onclick='javascript:top.closeWindow()'></div>".
          "</div>".     
        "<form name='formData' action='01_page_form.php?iMode=1&".$strParam.ALK_MODE_SSHEET_LIST."' method='post' enctype='multipart/form-data'>".
        "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
        "<tr>".       
        "<td width='580' valign=top>";
  	
  	  $oMasseEau = new AlkMasseEau("", $this->oQuery, $masse_id);
      $oMasseEau->setEtatQualite();
      $tabClassement = $oMasseEau->getEtatQualite();
      $strHtml.= $this->_getHtmlFicheParamPoint($masse_id, $id, $typeElement, $tabClassement);
        	         		
  		$strHtml .= "</td></tr></table></form>";
      
    return $strHtml;
  }  
    
  function _getHtmlFicheParamPoint($masse_id, $element_qualite_id, $strTypeElement, $tabClassement) {

  	$tabPage = array();    
    $iMode =  Request("iMode", REQ_GET, "1", "is_numeric");
    $codeMe =  Request("codeMe", REQ_GET, "");
    $nomElement =  Request("nomElem", REQ_GET, "");
    
  	$strParam = "iSheet=".$this->iSheet.
      					"&iSSheet=".$this->iSSheet."&iModeSSheet=";
  	
  	$bassin_code = "";
    $dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
		if ($drBassin = $dsBassin->getRowIter())
		  $bassin_code = $drBassin->getValueName("BASSIN_CODE"); 
		
  	$iWidthTxt = 10;
    $iWidthMemo = 250;
    $iHeightMemo = 4;  
    $strHtmlCtrl = "";	
    	    		
    $idClassement = "";
  	switch ($strTypeElement) {
  	  case "elem":  	    
  	  case "type":
  	    $idEtat = -1;
      	$idMotif = -1;
      	$idStatut = -1;
      	$strBilan = "";
      	$strComplementBilan = "";
      	$strDocReference = "";
      	$strDate = "";
      	$strDateCrea = "";
      	$iIndiceConfiance = "";
      	
    	  foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {		
    			if ($nomClassement != "Global"){
    				foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
    					if ($nomTypeElement != "ETAT" && $strTypeElement=="type" && $tabEtat["ID"]==$element_qualite_id){
    						$idEtat = $tabEtat["ETAT_ID"];
      					$idMotif = $tabEtat["MOTIF_ID"];
      					$idStatut = $tabEtat["STATUT_ID"];
      					$strBilan = $tabEtat["BILAN"];
      					$strComplementBilan = $tabEtat["COMPLEMENT_BILAN"];  					
      					$strDate = $tabEtat["DATE"];
    				  	$iIndiceConfiance = $tabEtat["INDICE_CONFIANCE"];				
    				  	$idClassement = $tabEtat["CLASSEMENT_ID"];		
    						$idniveau = 0;
                break;
    					} elseif ($nomTypeElement != "ETAT" && $strTypeElement=="elem") {						
    						if (count($tabEtat["ELEMENTS"]["BYID"]) > 0 && 
    								array_key_exists("ID_".$element_qualite_id, $tabEtat["ELEMENTS"]["BYID"])){
    									$tabEtatElement = $tabEtat["ELEMENTS"]["BYID"]["ID_".$element_qualite_id];
    									$idEtat = $tabEtatElement["ETAT_ID"];
      								$idMotif = $tabEtatElement["MOTIF_ID"];
      								$idStatut = $tabEtatElement["STATUT_ID"];
      								$strBilan = $tabEtatElement["BILAN"];
      								$strComplementBilan = $tabEtatElement["COMPLEMENT_BILAN"];  	
      								$strDocReference = $tabEtatElement["DOC_REF"];				
      								$strDate = $tabEtatElement["DATE"];
    				  				$iIndiceConfiance = $tabEtatElement["INDICE_CONFIANCE"];
    				  				$idClassement = $tabEtatElement["CLASSEMENT_ID"];
                      $idniveau = $tabEtatElement["NIVEAU"];
    									break;			      			
    	     			}
    					}
    				}									
    			}							 
    		}		
    		$dsEtat = $this->oQuery->getDs_listeEtat(0, -1, ($strTypeElement == "type" ? $element_qualite_id : ""), 
  																									($strTypeElement == "elem" ? $element_qualite_id : ""));
  																									
  			$dsMotif = $this->oQuery->getTableForCombo("ETAT_MOTIF", "MOTIF", true, true);																								
      	$dsStatut = $this->oQuery->getTableForCombo("CLASSEMENT_STATUT", "STATUT", true, true);
      /*
       * Ctrl retir�s et mis en disable mai 2012 car uniqt mis � jour par import
       */
      

      	$oCtrlCB = new HtmlRadioGroup(0, "etat_id", $idEtat, "Etat provisoire");  	
      	while ($drEtat = $dsEtat->getRowIter()){
      		$oCtrlCB->AddRadio($drEtat->getValueName("ID"), $drEtat->getValueName("LIB"));	
      	}    
      	$oCtrlCB->addValidator("formDico", "radio", true); 
        $oCtrlCB->bReadOnly = true;
        $oCtrlCB->setMode(1);		
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB);
	
        $oCtrlSelectMotif = new HtmlSelect(0, "motif_id", $idMotif, "Motif", 1, $iWidthMemo);    
    		$oCtrlSelectMotif->oValTxt = $dsMotif;
    		$oCtrlSelectMotif->tabValTxtDefault = array ("-1", "Sans objet");
        $oCtrlSelectMotif->bReadOnly = true;
        $oCtrlSelectMotif->setMode(1);
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlSelectMotif);


      	$oCtrlCB = new HtmlRadioGroup(0, "statut_id", $idStatut, "Statut");
      	while ($drStatut = $dsStatut->getRowIter()){
      		$oCtrlCB->AddRadio($drStatut->getValueName("STATUT_ID"), $drStatut->getValueName("nom"));	
      	}    
        $oCtrlCB->bReadOnly = true;
        $oCtrlCB->setMode(1);        
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB);

 
        $oCtrlDate = new HtmlText(0, "classement_date",  $strDate, "Date de d�termination du classement", 1, 15, 15);
    		$oCtrlDate->addValidator("formData", "date10", true);
    		$oCtrlDate->labelAfterCtrl  = "&nbsp;(DD/MM/YYYY)";		
    		$oCtrlDate->bReadOnly = true;
    		$oCtrlDate->setMode(1);
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlDate);
    
    	
        $oCtrlIndice = new HtmlText(0, "classement_indice_confiance",  $iIndiceConfiance, "Indice de confiance", 1, 5, 5);
    		$oCtrlIndice->addValidator("formData", "int", false);
    		$oCtrlIndice->bReadOnly = true;
    		$oCtrlIndice->setMode(1);
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlIndice);
        
        
        if($idniveau<=1){			
    		$oCtrlCR = new HtmlText(0, "classement_bilan",  $strBilan, "Compte-rendu sur l'�tat provisoire", 10, 50, 255);
    		$oCtrlCR->addValidator("formData", "text", false);
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCR);
    		
    		$oCtrl = new HtmlFile(0, "classement_complement_bilan", $strComplementBilan, "Fichier compl�mentaire du bilan pour cet �l�ment de qualit�", $iWidthTxt, 255);
      	$oCtrl->urlFile = ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$bassin_code."/".$strComplementBilan;
      	$oCtrl->bDualMode = true;
      	$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
    
      	$oCtrl = new HtmlFile(0, "classement_doc_reference", $strDocReference, "Document de r�f�rence", $iWidthTxt, 255);
      	$oCtrl->urlFile = ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$bassin_code."/".$strDocReference;
      	$oCtrl->bDualMode = true;
      	$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
        }    		
    		$oCtrl = new HtmlHidden("old_classement_complement_bilan", $strComplementBilan);
        $oCtrl->addHidden("old_classement_doc_reference", $strDocReference);																						
  	    break;
  	  case "class":  
  	    $idClassement = (array_key_exists("CLASSEMENT_ID",$tabClassement[$nomElement]["ETAT"]) ? $tabClassement[$nomElement]["ETAT"]["CLASSEMENT_ID"] : "");		
  	    $iIndiceConfiance = (array_key_exists("INDICE_CONFIANCE",$tabClassement[$nomElement]["ETAT"]) ? $tabClassement[$nomElement]["ETAT"]["INDICE_CONFIANCE"] : "");    	    
  	    $type_classement_id = (array_key_exists("ID",$tabClassement[$nomElement]["ETAT"]) ? $tabClassement[$nomElement]["ETAT"]["ID"] : "");
  	    $oCtrlIndice = new HtmlText(0, "classement_indice_confiance",  $iIndiceConfiance, "Indice de confiance", 1, 5, 5);
    		$oCtrlIndice->addValidator("formData", "int", false);
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlIndice);	
  	    break;      
  	  case "me":
  	    $idClassement = (array_key_exists("CLASSEMENT_ID",$tabClassement["Global"]["ETAT"]) ? $tabClassement["Global"]["ETAT"]["CLASSEMENT_ID"] : "");		
  	    $iIndiceConfiance = (array_key_exists("INDICE_CONFIANCE",$tabClassement["Global"]["ETAT"]) ? $tabClassement["Global"]["ETAT"]["INDICE_CONFIANCE"] : "");    	    
  	    $oCtrlIndice = new HtmlText(0, "classement_indice_confiance",  $iIndiceConfiance, "Indice de confiance", 1, 5, 5);
    		$oCtrlIndice->addValidator("formData", "int", false);
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlIndice);
  	    break;  	      	  
  	}
  	
  	$oCtrlH = new HtmlHidden("masse_id", $masse_id);
		$oCtrlH->addHidden("element_qualite_id", ($strTypeElement == "elem" ? $element_qualite_id : ""));
		$oCtrlH->addHidden("type_element_qualite_id", ($strTypeElement == "type" ? $element_qualite_id : ""));
		$oCtrlH->addHidden("type_classement_id", ($strTypeElement == "class" ? $type_classement_id : ""));
		$oCtrlH->addHidden("type_element", $strTypeElement);
		$oCtrlH->addHidden("classement_id", $idClassement);
		  	  	  	  
		$oBtValid = new HtmlLink("javascript:Recherche('01_page_sql.php?".$strParam.ALK_MODE_SSHEET_SQL."&bPopup=1".($idClassement != "" ? "&iMode=2" : "&iMode=1")."')", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    
    $oBtRetour = new HtmlLink("javascript:closeWindow()", "Fermer la fiche", 
                             "retour.gif", "retour_rol.gif");
    // recup de la date de maj
    $strDateClassement = "";
    if($idClassement!=""){
    	$dsDateMaj = $this->oQuery->getDs_DateMAJByClassementId($idClassement);
      if($drDateMaj = $dsDateMaj->getrowIter()){
      	$strDateClassement = $drDateMaj->getValueName("CLASSEMENT_ME_DATE_MAJ");
      }
    }
    
         
    $strHtml = $this->_getFrameTitleSheet("Donn�es de la masse d'eau <u>".$codeMe."</u> pour l'�l�ment de qualit� <u>".$nomElement."</u>".($strDateClassement!=""? " mise � jour le ".$strDateClassement : ""));
  	$strHtml .= "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
					      "<tr>".
					      "<td width='300' height='10'></td>".
					      "<td width='600'>".$oCtrlH->getHtml()."</td>".
					      "</tr>".
  							$strHtmlCtrl."</table><br/><div align='center'>".
      					$oBtValid->getHtml()."&nbsp;".$oBtRetour->getHtml()."</div>";
     						  	  	
  	return $strHtml;
  }  
  
  /**
   * @brief Traitement des actions sur dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function traitementData()
  {
    $iMode = Request("iMode", REQ_GET, "-1", "is_numeric"); 
		$classement_id = Request("classement_id", REQ_POST, "-1", "is_numeric");
		$masse_id = Request("masse_id", REQ_POST, "-1", "is_numeric");
		$strTypeElement = Request("type_element", REQ_POST, "");		
		
    $strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_LIST."&masse_id=".$masse_id;
    
    if ($iMode == -1)  
    	return "01_page_form.php?".$strParam;
    if( $iMode != "1" && $classement_id=="-1") 
    	return "01_page_form.php?".$strParam;
    	
    $strUrlBack = "01_page_form.php?".$strParam;
    
    $bassin_code = "";
    $dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
		if ($drBassin = $dsBassin->getRowIter())
		  $bassin_code = $drBassin->getValueName("BASSIN_CODE"); 
			    
    //Enregistrement de la saisie             
    $element_qualite_id = Request("element_qualite_id", REQ_POST, "");
    $type_element_qualite_id = Request("type_element_qualite_id", REQ_POST, "");
    $etat_id = Request("etat_id", REQ_POST, "");
    $motif_id = (Request("motif_id", REQ_POST, "") == -1 ? "" : Request("motif_id", REQ_POST, ""));
    $statut_id = (Request("statut_id", REQ_POST, "") == -1 ? "" : Request("statut_id", REQ_POST, ""));
    $indice_confiance = Request("classement_indice_confiance", REQ_POST, "");
    $date = Request("classement_date", REQ_POST, "");
    $bilan = Request("classement_bilan", REQ_POST, "");
    //Traitement des fichiers � upploader
    $iSupBilan = RequestCheckbox("classement_complement_bilan", REQ_POST);
		$strFileNameBilan = DoUpload("classement_complement_bilan", "", ALK_PATH_UPLOAD_DOC.$bassin_code."/", $iSupBilan, Request("old_classement_complement_bilan", REQ_POST, ""));
		$iSupDocRef = RequestCheckbox("classement_doc_reference", REQ_POST);
		$strFileNameDocRef = DoUpload("classement_doc_reference", "", ALK_PATH_UPLOAD_DOC.$bassin_code."/", $iSupDocRef, Request("old_classement_doc_reference", REQ_POST, ""));
    $type_classement_id = Request("type_classement_id", REQ_POST, "");
    
		$tabValue["SESSION_ID"] = array( 1, "(SELECT SESSION_ID FROM SESSION_QUALITE where BASSIN_ID = ".$_SESSION["idBassin"]." and SESSION_ARCHIVE=0 )");
		$tabValue["MASSE_ID"] = array( 1, $masse_id);
		$tabValue["ELEMENT_QUALITE_ID"] = array( 1, $element_qualite_id);
		$tabValue["TYPE_ELEMENT_QUALITE_ID"] = array( 1, $type_element_qualite_id);
		$tabValue["TYPE_CLASSEMENT_ID"] = array( 1, $type_classement_id);
		$tabValue["CLASSEMENT_DATE_MAJ"] = array( 1, $this->oQueryAction->dbConn->getDateCur());
    $tabValue["CLASSEMENT_ME_DATE_MAJ"] = array( 1, $this->oQueryAction->dbConn->getDateCur());		
		/*$tabValue["ETAT_ID"] = array( 1, $etat_id);
		$tabValue["MOTIF_ID"] = array( 1, $motif_id);
		$tabValue["STATUT_ID"] = array( 1, $statut_id);
		$tabValue["CLASSEMENT_INDICE_CONFIANCE"] = array( 1, $indice_confiance);
		$tabValue["CLASSEMENT_BILAN"] = array( 0, $bilan);
		$tabValue["CLASSEMENT_DATE"] = array( 2, $date);
		*/
		switch ($strTypeElement){
		  case "elem":  	    
  	  case "type":
  	    $tabValue["CLASSEMENT_BILAN"] = array( 0, $bilan);
  	    break;
  	  case "class":
  	  case "me" :
  	    $tabValue["CLASSEMENT_INDICE_CONFIANCE"] = array( 1, $indice_confiance);
  	    break;
		}
				
    if ($iSupBilan || $strFileNameBilan != "") 
    	$tabValue["CLASSEMENT_COMPLEMENT_BILAN"] = array( 0, $strFileNameBilan);
    if ($iSupDocRef || $strFileNameDocRef != "") 
    	$tabValue["CLASSEMENT_DOC_REF"] = array( 0, $strFileNameDocRef);    		
	  
    $strFieldKey = "CLASSEMENT_ID";
	  $strTable = "CLASSEMENT_MASSE_EAU";
    
   	if( $iMode == "1" ) {
   			$tabValue["CLASSEMENT_DATE_CREA"] = array( 1, $this->oQueryAction->dbConn->getDateCur());
        $classement_id = $this->oQueryAction->add_ficheDico($strTable, $strFieldKey, $tabValue);
    } else {
        $this->oQueryAction->update_ficheDico($strTable, $strFieldKey, $classement_id, $tabValue);
    }
    
    // MAJ de la date de  MAJ de la table BASSIN_HYDROGRAPHIQUE en cas de succes.
    $tabValueMaj = array();
    $tabValueMaj["DATE_MAJ"]= array( 1, $this->oQueryAction->dbConn->getDateCur());
    $this->oQueryAction->update_ficheDico("BASSIN_HYDROGRAPHIQUE", "BASSIN_ID", $_SESSION["idBassin"], $tabValueMaj);
    

    //Effectuer la mise � jour des gml pour l'outil cartographique
		$this->traitementMajGml("qual");
		
    return $strUrlBack;
  }      
  
  function traitementImport() {
    
  	include_once("../../classes/appli/alkimport.class.php");
		include_once("classes/alkimportatlas_classement.class.php");
		include_once("../../classes/appli/alkimporterreur.class.php");
		include_once("../../classes/appli/alkimportwarning.class.php");

		$iMode = Request("iMode", REQ_POST, "");
  	$fin = Request("fin", REQ_GET, "0", "is_numeric");
  	
  	$strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_FORM;

  	$ag_id = $_SESSION["sit_idUser"];
		$urlFileError = ALK_SIALKE_URL.ALK_PATH_UPLOAD_IMPORT.$ag_id."classement_err.txt";
		$strFileError = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$ag_id."classement_err.txt";
		$urlFileWarning = ALK_SIALKE_URL.ALK_PATH_UPLOAD_IMPORT.$ag_id."classement_warning.txt";
		$strFileWarning = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$ag_id."classement_warning.txt";
		SupprFichier($strFileError);
		SupprFichier($strFileWarning);
		
		$pathFile = "";
		$strFileName = "";
		
		if( $fin != "1" ) {
		
			if( $iMode != "annuler" ) {
				//upload du fichier
				$strFileName = DoUpload("imp_fic_classement", $ag_id."_", ALK_PATH_UPLOAD_IMPORT, 0, "");
				if( !is_string($strFileName))
					$strFileName = "";
				if( $strFileName!="" )
					$pathFile = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$strFileName;
			}
		}
		$strFichier_Import = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$strFileName;
		$strFichier_Erreur = $strFileError;
		$strFichier_Warning = $strFileWarning; 
		
		$intSeparateur = 9;
		$tabImport = array (
		                    array("name" => "MODE", "oblig" => 1,"type" =>array("S","M","C") ,"lg" => 0,"index" => 0 ,"mode" => 1),
		                    array("name" => "ME", "oblig" => 1, "type" => $this->oQuery->GetDs_champsImport("MASSE_EAU","MASSE_CODE", "MASSE_ID", "BASSIN_ID=".$_SESSION["idBassin"]), "lg" => 20,"index" => 0 ,"mode" => 2),
		                    array("name" => "ELEMENT", "oblig" => 0, "type" => $this->oQuery->GetDs_champsImport("ELEMENT_QUALITE",$this->oQuery->dbConn->GetLowerCase("ELEMENT_QUALITE_NOM"), "ELEMENT_QUALITE_ID", "BASSIN_ID=".$_SESSION["idBassin"]), "lg" => 20,"index" => 0 ,"mode" => 2),
 		                    array("name" => "ELEMENT_NIVEAU", "oblig" => 0, "type" => array("1","2"), "lg" => 20,"index" => 0 ,"mode" => 1),		                    
		                    array("name" => "TYPE_ELEMENT", "oblig" => 0, "type" => $this->oQuery->GetDs_champsImport("TYPE_ELEMENT_QUALITE","TYPE_ELEMENT_QUALITE_NOM", "TYPE_ELEMENT_QUALITE_ID"), "lg" => 20,"index" => 0 ,"mode" => 2),
		                    array("name" => "TYPE_CLASSEMENT", "oblig" => 0, "type" => $this->oQuery->GetDs_champsImport("TYPE_CLASSEMENT","TYPE_CLASSEMENT_LIBELLE", "TYPE_CLASSEMENT_ID"), "lg" => 20,"index" => 0 ,"mode" => 2),		                    
		                    array("name" => "ETAT", "oblig" => 1, "type" => $this->oQuery->GetDs_champsImport("ETAT",$this->oQuery->dbConn->GetLowerCase("ETAT_LIBELLE"), "ETAT_ID"), "lg" => 20,"index" => 0 ,"mode" => 2),
		                    array("name" => "MOTIF", "oblig" => 0, "type" => $this->oQuery->GetDs_champsImport("ETAT_MOTIF","MOTIF_CODE", "MOTIF_ID", "BASSIN_ID=".$_SESSION["idBassin"]), "lg" => 20,"index" => 0 ,"mode" => 2),
		                    array("name" => "STATUT", "oblig" => 0, "type" =>  $this->oQuery->GetDs_champsImport("CLASSEMENT_STATUT","STATUT_CODE", "STATUT_ID"), "lg" => 20,"index" => 0 ,"mode"=> 2),
		                    array("name" => "INDICE", "oblig" => 0, "type" => "numeric","lg" => 20,"index" => 0 ,"mode" => 0),
		                    array("name" => "DATE", "oblig" => 1, "type" => "date","lg" => 10,"index" => 0 ,"mode" => 0)		                    
		                    );
		$bTes = true;
		$intTes = 1;
        
		$tabSup = array("ME", "ELEMENT");
		$tabAjout = array ("ME", "ETAT", "DATE");
		$tabModif = array("ME", "ETAT", "DATE");
		$objErreur = new AlkImportErreur($strFichier_Erreur);
		$objWarning = new AlkImportWarning($strFichier_Warning);
		$objImport = new AlkImportAtlasClassement($strFichier_Import, $tabImport, $tabSup, $tabAjout, $tabModif, 
		                               $objErreur,$objWarning, $this->oQuery, $this->oQueryAction );
   
		$iTes = 0;
		$intTest = $objImport->parseFile($intSeparateur);
		if( $intTest == 0 ) {
			$intTes = $objImport->parseLines($intSeparateur);
		  if( $intTes == 0 ) {
		    $iTes = $objImport->integrate($intSeparateur);
		  }
		}
				
        
		$strHtml = $this->_getFrameTitleSheet("Import de donn�es de classement de l'atlas").		
		  "<table width='606' border='0' cellspacing='0' cellpadding='4' align='center'>".
		  "<tr>".
		  "<td width=60>&nbsp;</td>".
		  "<td align='center'>";
		
		if( $intTes == 0 && $iTes == 0 ) {

      // MAJ de la date de  MAJ de la table BASSIN_HYDROGRAPHIQUE en cas de succes.
      $tabValueMaj = array();
      $tabValueMaj["DATE_MAJ"]= array( 1, $this->oQueryAction->dbConn->getDateCur());
      $this->oQueryAction->update_ficheDico("BASSIN_HYDROGRAPHIQUE", "BASSIN_ID", $_SESSION["idBassin"], $tabValueMaj);
      // MAJ des fiche des ELEMENTs de QUALITE du Bassin import�
		  $strHtml .= "<div class='divContenuTexte'>Importation effectu� avec succ�s.</div>";
		} else {
		  if( $intTes > 0 ) {
		    $strHtml .= "<div class='divContenuTexte'>Importation non effectu�e pour cause de fichier mal format�.</div>";
		  }
		  if( $iTes > 0 ) {
		    $strHtml .= "<div class='divContenuTexte'>Importation effectu�e avec des alertes ou des erreurs.</div>";    
		  }
		  $bFileAlert = file_exists($strFichier_Warning) && is_file($strFichier_Warning);
		  $bFileError = file_exists($strFichier_Erreur) && is_file($strFichier_Erreur);
		
		  if( $bFileAlert || $bFileError ) {
		    $strHtml .= "<div class='divContenuTexte'><font color=red>Attention</font> : Vous pouvez consulter ";
		    if( $bFileAlert ) {
		      $strHtml .= "<a class='aContenuLien' href=\"".$urlFileWarning."\" target='_blank'>les alertes</a>";
		      if( $bFileError )
		        $strHtml .= " et ";
		    }
		    if( $bFileError ) {
		       $strHtml .= "<a class='aContenuLien' href=\"".$urlFileError."\" target='_blank'>les erreurs</a>";
		    }
		    $strHtml .= ".";
		  }
		  
		  $strHtml .= "<br><a class='aContenuLien' href=\"01_page_form.php?".$strParam."\">R�essayer un nouvel import</a>";
		}
				
		//Effectuer la mise � jour des gml pour l'outil cartographique
		$this->traitementMajGml("qual");
		
		return $strHtml;
  }
  
  /**
   * @brief Archivage de la session courante
   *
   * @return Retourne un string : code html du sous onglet
   */
  function traitementArchivage()
  {    
    $iMode = Request("iMode", REQ_GET, "-1", "is_numeric"); 
		$old_session_id = Request("old_session_id", REQ_POST, "-1", "is_numeric");
		        	
    $strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_LIST;
            
    //Enregistrement de la saisie             
    $session_libelle = Request("libelle", REQ_POST, "");
    
    $tabValue["SESSION_LIBELLE"] = array( 0, $session_libelle);
		$tabValue["SESSION_USER_CREA"] = array( 1,  $_SESSION["sit_idUser"]);
		$tabValue["BASSIN_ID"] = array( 1,  $_SESSION["idBassin"]);		
		$tabValue["SESSION_DATE_CREA"] = array( 1, $this->oQueryAction->dbConn->getDateCur());
		    
		$tabValueUpdate["SESSION_ARCHIVE"] = array( 1, 1);
		$tabValueUpdate["SESSION_DATE_ARCHIVE"] = array( 1, $this->oQueryAction->dbConn->getDateCur());
		
    $strFieldKey = "SESSION_ID";
	  $strTable = "SESSION_QUALITE";
    
	  $this->oQueryAction->update_ficheDico($strTable, $strFieldKey, $old_session_id, $tabValueUpdate);	  
   	$session_id = $this->oQueryAction->add_ficheDico($strTable, $strFieldKey, $tabValue);    
   	
   	//Recopie des donn�es de la session old vers la nouvelle session
   	$this->oQueryAction->copieSessionDico($_SESSION["idBassin"], $old_session_id, $session_id);
   	$this->oQueryAction->copieSessionClassement($_SESSION["idBassin"], $old_session_id, $session_id);   	
   	$this->oQueryAction->copieSessionMasseEau($_SESSION["idBassin"], $old_session_id, $session_id);
   	
   	//Copie des fichiers physiques
    //Parcours du r�pertoire en cours et d�placement dans un sous r�pertoire archive du bassin
    $dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
		if ($drBassin = $dsBassin->getRowIter())
			$map = $drBassin->getValueName("BASSIN_CODE"); 
    
		//FICHIERS QUALITE
    $strPathRepSession = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC.$map;
    $strPathRepArchive = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC.$map.'/archive_'.$old_session_id;
    
    CreeRepertoire($strPathRepArchive);
    $tabFiles = getTabFilesByDir($strPathRepSession);
    foreach ($tabFiles as $file) {
      CopieFichier($strPathRepSession."/".$file, $strPathRepArchive."/".$file);
    }
    	
    //FICHIERS CARTO
	  $strPathRepSession = ALK_SIALKE_PATH.'upload/carte/'.$map;
    $strPathRepArchive = ALK_SIALKE_PATH.'upload/carte/'.$map.'/archive_'.$old_session_id;
    
    CreeRepertoire($strPathRepArchive);
    $tabFiles = getTabFilesByDir($strPathRepSession, array("gml", "xsd"));
    foreach ($tabFiles as $file) {
      CopieFichier($strPathRepSession."/".$file, $strPathRepArchive."/".$file);
    }	
   
    //LANCEMENT generation des shp � partir des gml
    $this->MajGmlSextant($old_session_id);
    
    $tabFiles = getTabFilesByDir($strPathRepArchive, array("gml", "xsd"));
    if (count($tabFiles)>0)
      $strParam .= "&bRes=1";        
      
    $strUrlBack = "01_page_form.php?".$strParam;
    return $strUrlBack;
  }      
  
  
  /**
   * @brief Archivage de la session courante
   *
   * @return Retourne un string : code html du sous onglet
   */
  function traitementGenerationShape()
  {    
    $iMode = Request("iMode", REQ_GET, "-1", "is_numeric");   
    $strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_LIST;
    $old_session_id = Request("old_session_id", REQ_GET, "-1", "is_numeric");
    $bDebug = Request("bDebug", REQ_GET, "0", "is_numeric");
		
    //LANCEMENT generation des shp � partir des gml
    $this->MajGmlSextant($old_session_id);
    
    if ($bDebug == 1)
      die();
      
    $strUrlBack = "01_page_form.php?".$strParam;
    return $strUrlBack;
            
  }
/***
 * @brief  Function g�n�rant des cartographies sous forme de gml des masse d'eau par Bassin et pour chaque �tat et �l�ment de qualit�
 * @brief  execute la g�n�ration des gml pour l'ensemble des bassins.
 * @brief  genere des lot shp � parti des gml g�n�r�s pr�cedemment et les stockes dans upload/sextant/BASSIN/archive_id session 
 * @brief  g�n�re un zip de l'ensmble des shp
 * @brief  200 sc // sont allou�es pour le traitement de chaque bassin
 * 
 * @return la cr�ation des fichiers gml dans upload/sextant/CODE_BASSIN/FIchier.gml
 * @return la cr�ation des fichiers shp + un fichier zip dans upload/sextant/CODE_BASSIN/archive_<num de session>
 **/

function MajGmlSextant($num_Session)
  {
    $bDebug = Request("bDebug", REQ_GET, "0", "is_numeric");
    if ($bDebug)
      echo $num_Session;
      
    global $bPhp5;
    $bPhp5 = false;
    if (PHP_VERSION>='5'){
       ini_set('zend.ze1_compatibility_mode', '0');       
       require_once("lib/domxml-php4-to-php5.php");
       $bPhp5 = true;
    }

    $map = "LB";
		$dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
		if ($drBassin = $dsBassin->getRowIter())
			$map = $drBassin->getValueName("BASSIN_CODE");
    			
		/*********
     // generation du zip destin� � recuillir les fichiers shp g�n�r�s pour chaque lement de qualit�
    ***********/
    // creation du repertoire archive
    $strPathRepArchive = ALK_SIALKE_PATH.'upload/sextant/'.$map.'/archive_'.$num_Session;
    CreeRepertoire($strPathRepArchive);

    if(is_file($strPathRepArchive."/".strtolower("archive_shp_".$num_Session).".zip")){
        unlink($strPathRepArchive."/".strtolower("archive_shp_".$num_Session).".zip");
    }  
    $oZip = new ZipArchive();

    if ( $res = $oZip->open($strPathRepArchive."/".strtolower("archive_shp_".$num_Session).".zip", ZIPARCHIVE::CREATE) !== TRUE ) {
    	echo '�chec, code:' . $res;
    }
        
        	
    // pour chaque �tat (global, �cologique ou chimique et pour chaque �lement de qualit�) on cr�� un gml
     
      set_time_limit(1500); // 200 sc max
      /*********** etat global************************/ 
    
           //recup de la couche masse d'eau de base
        //$dom = domxml_open_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/'.$map.'_masse_eau.gml');
        $dom = domxml_open_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/archive_'.$num_Session.'/'.$map.'_masse_eau.gml');
        $racine = $dom->document_element();  
        $tabElements = $racine->get_elements_by_tagname("shape");
    
        foreach($tabElements as $key => $member){// pour chaque masse d'eau
          
                $elementCode = $member->get_elements_by_tagname("code_me");
                $elementCodeEu = $member->get_elements_by_tagname("code_eu");
                $codeMe = "";
                if (array_key_exists(0, $elementCode))
                  $codeMe = $elementCode[0]->get_content(); 
                if (array_key_exists(0, $elementCodeEu))
                  $codeMe = $elementCodeEu[0]->get_content(); 
                // �tat global
                $oMasseEau = new AlkMasseEau($codeMe, $this->oQuery, $idMe="", $num_Session);
                $tabEtat = $oMasseEau->getEtatQualiteGlobal(); 
                $tabClassement = $oMasseEau->getEtatQualite(); 
                if ($bDebug){
                    echo "------------------------<br/>".$codeMe."<br/>------------------------<br/>";                    
                }
                    
                $generalElt = $dom->create_element("ogr:NOM_ME");
                $general = $member->append_child($generalElt);
                $general->set_content(utf8_encode($oMasseEau->data->getValueName("NOM")));
                
                $generalElt = $dom->create_element("ogr:COULEUR_QUAL");
                $general = $member->append_child($generalElt);
                $general->set_content(utf8_encode($tabEtat["COULEUR"]));
                
                $generalElt = $dom->create_element("ogr:ETAT_QUAL");
                $general = $member->append_child($generalElt);
                $general->set_content(utf8_encode($tabEtat["ETAT"]));
                      
                $generalElt = $dom->create_element("ogr:ETAT_VAL");
                $general = $member->append_child($generalElt);
                $general->set_content(utf8_encode($tabEtat["VALEUR"]));
                   
                $generalElt = $dom->create_element("ogr:ETAT_METHODE");
                $general = $member->append_child($generalElt);
                $general->set_content(utf8_encode($tabEtat["METHODE"]));
 
        }        
                
      // creation du gml etat glob
      $dom->dump_file(ALK_SIALKE_PATH.'upload/sextant/'.$map.'/'.$map.'_masse_eau_etat_global.gml');   

      /***********************
      // transformer le gml en shp + nettoyage
      **************************/
      if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.shp")){
      	unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.shp");
        unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.shx");
        unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.dbf");
      }

      if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.gml")){
        $exec = "ogr2ogr -f 'ESRI Shapefile' -overwrite ".ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.shp ".ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.gml ";
        if ($bDebug) echo $exec;
          
        exec($exec); 
        $bResZip = $oZip->addFile(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.shp", "masse_eau_etat_global.shp");
        $oZip->addFile(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.shx", "masse_eau_etat_global.shx");
        $oZip->addFile(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.dbf", "masse_eau_etat_global.dbf");
        
        if ($bDebug && $bResZip) echo "<br/>ajout au zip ".ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.shp<br/>";
        
      }          
      if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.gml") && !$bDebug){
        unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.gml");
      } 
      if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.gfs")){
        unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_global.gfs");
      }
      
      /*********** etat ecologique et chimique *************************/ 

      // recup des id des elemts de qualit� pour le bassin courant
           
      $dsTypeClassement = $this->oQuery->getTableForCombo("TYPE_CLASSEMENT", "TYPE_CLASSEMENT", true); 
      while($drTypeClassement = $dsTypeClassement->getRowIter()){ // pour chaque type de classement �cologique ou chimique
        $typeClassement_lib = strtoupper(strtr($drTypeClassement->getValueName("NOM"), "e����a���u���i���o�c�", "eeeeeaaaauuuuiiiooocc"));
        
        //recup de la couche masse d'eau de base
        //$dom = domxml_open_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/'.$map.'_masse_eau.gml');
        $dom = domxml_open_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/archive_'.$num_Session.'/'.$map.'_masse_eau.gml');
        $racine = $dom->document_element();  
        $tabElements = $racine->get_elements_by_tagname("shape");
         
        foreach($tabElements as $key => $member){// pour chaque masse d'eau
          
              $elementCode = $member->get_elements_by_tagname("code_me");
              $elementCodeEu = $member->get_elements_by_tagname("code_eu");
              $codeMe = "";
              if (array_key_exists(0, $elementCode))
                  $codeMe = $elementCode[0]->get_content(); 
              if (array_key_exists(0, $elementCodeEu))
                  $codeMe = $elementCodeEu[0]->get_content(); 
                // �tat �cologique 
              $oMasseEau = new AlkMasseEau($codeMe, $this->oQuery, $idMe="", $num_Session);
              $tabClassement = $oMasseEau->getEtatQualite(); 

               if ($bDebug){
                    echo "------------------------<br/>".$codeMe."<br/>------------------------<br/>";
                }
                    
                
              $generalElt = $dom->create_element("ogr:NOM_ME");
              $general = $member->append_child($generalElt);
              $general->set_content(utf8_encode($oMasseEau->data->getValueName("NOM")));

                
              foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {// si ecologique ou chimique    
                $strNomClassement = strtoupper(strtr($nomClassement, "e����a���u���i���o�c�", "eeeeeaaaauuuuiiiooocc"));
                
                if ($strNomClassement == $typeClassement_lib){
                  
                  $ecololElt = $dom->create_element("ogr:COULEUR_QUAL");
                  $ecolo = $member->append_child($ecololElt);
                  $ecolo->set_content(utf8_encode($tabClassement[$nomClassement]["ETAT"]["COULEUR"]));
                  
                  $ecololElt = $dom->create_element("ogr:ETAT_QUAL");
                  $ecolo = $member->append_child($ecololElt);
                  $ecolo->set_content(utf8_encode($tabClassement[$nomClassement]["ETAT"]["ETAT"]));
              
                  $ecololElt = $dom->create_element("ogr:ETAT_VAL");
                  $ecolo = $member->append_child($ecololElt);
                  $ecolo->set_content(utf8_encode($tabClassement[$nomClassement]["ETAT"]["VALEUR"]));                  
                
                  $ecololElt = $dom->create_element("ogr:ETAT_METHODE");
                  $ecolo = $member->append_child($ecololElt);
                  $ecolo->set_content(utf8_encode($tabClassement[$nomClassement]["ETAT"]["METHODE"]));  
                                    
                }
              }
            } 
          
        // creation du gml etat �colo ou chimique
        $typeClassement_lib = strtolower($typeClassement_lib);
        $dom->dump_file(ALK_SIALKE_PATH.'upload/sextant/'.$map.'/'.$map.'_masse_eau_etat_'.$typeClassement_lib.'.gml');

        	/***********************
          // transformer le gml en shp + nettoyage
          **************************/
          if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".shp")){
          	unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".shp");
            unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".shx");
            unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".dbf");
          }
          
          if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".gml")){
            $exec = "ogr2ogr -f 'ESRI Shapefile' -overwrite ".ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".shp ".ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".gml ";
            
            if ($bDebug) echo $exec;
          
            exec($exec); 
            $bResZip = $oZip->addFile(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".shp", "masse_eau_etat_".$typeClassement_lib.".shp");
            $oZip->addFile(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".shx", "masse_eau_etat_".$typeClassement_lib.".shx");
            $oZip->addFile(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".dbf", "masse_eau_etat_".$typeClassement_lib.".dbf");
            
            if ($bDebug && $bResZip) echo "<br/>ajout au zip ".ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".shp<br/>";
          }          
          if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".gml") && !$bDebug){
            unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".gml");
          } 
          if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".gfs")){
            unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_etat_".$typeClassement_lib.".gfs");
          }
      
        }

        /*********** pour chaque element de qualit� *************************/   
        // recup des id des elemts de qualit� pour le bassin courant

        $strDirArchive = ($num_Session != -1 ? "/archive_".$num_Session : "");
        
        $dsBassin = $this->oQuery->getDs_ElementQualiteById("", "", "", $_SESSION["idBassin"], $num_Session);        
        while($drBassin = $dsBassin->getRowIter()){ // pour chaque �lement de qualit� du Bassin on cr�� un gml
          $idElmtQualite = $drBassin->getValueName("ELEMENT_QUALITE_ID");         
          $nomQualite = strtoupper(strtr($drBassin->getValueName("ELEMENT_QUALITE_NOM"), "e����a���u���i���o�c� ", "eeeeeaaaauuuuiiiooocc_"));
          //recup de la couche masse d'eau de base
          //$dom = domxml_open_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/'.$map.'_masse_eau.gml');
          $dom = domxml_open_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/archive_'.$num_Session.'/'.$map.'_masse_eau.gml');
          $racine = $dom->document_element();  
          $tabElements = $racine->get_elements_by_tagname("shape");
          
          foreach($tabElements as $key => $member){// pour chaque masse d'eau
        
            $elementCode = $member->get_elements_by_tagname("code_me");
            $elementCodeEu = $member->get_elements_by_tagname("code_eu");
            $codeMe = "";
            if (array_key_exists(0, $elementCode))
                $codeMe = $elementCode[0]->get_content(); 
            if (array_key_exists(0, $elementCodeEu))
                $codeMe = $elementCodeEu[0]->get_content(); 
                
              
            $oMasseEau = new AlkMasseEau($codeMe, $this->oQuery, $idMe="", $num_Session);                       
            $generalElt = $dom->create_element("ogr:NOM_ME");
            $general = $member->append_child($generalElt);
            $general->set_content(utf8_encode($oMasseEau->data->getValueName("NOM")));

            $tabClassement = $oMasseEau->getEtatQualite(); 
           
            $strEnteteType = "";
            foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {    
              if ($nomClassement != "Global"){
                foreach ($tabTypeElementQualite as $iElement=>$tabEtat){
                  if (array_key_exists("ELEMENTS", $tabClassement[$nomClassement][$iElement])){
                    foreach($tabClassement[$nomClassement][$iElement]["ELEMENTS"]["BYNUM"] as $tabEtatqualite){
                      if($tabEtatqualite["ID"] == $idElmtQualite){

                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_NOM");
                        $elqualElt = $dom->create_element("ogr:NOM_QUAL");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode($tabEtatqualite["NOM"]));
        
                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_NIVEAU");
                        $elqualElt = $dom->create_element("ogr:NIV_QUAL");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode($tabEtatqualite["NIVEAU"]));
                        
                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_ETAT");
                        $elqualElt = $dom->create_element("ogr:ETAT_QUAL");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode($tabEtatqualite["ETAT"]));
                                                  
                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_COULEUR");
                        $elqualElt = $dom->create_element("ogr:COULEUR_QUAL");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode($tabEtatqualite["COULEUR"]));     
                        
                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_STATUT");
                        $elqualElt = $dom->create_element("ogr:STATUT_QUAL");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode($tabEtatqualite["STATUT"]));   

                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_DATE");
                        $elqualElt = $dom->create_element("ogr:DATE_QUAL");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode($tabEtatqualite["DATE"]));   

                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_BILAN");
                        $elqualElt = $dom->create_element("ogr:BILAN_QUAL");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode($tabEtatqualite["BILAN"]));   

                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_COMPLEMENT_BILAN");
                        $elqualElt = $dom->create_element("ogr:COMP_BILAN_QUAL");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode(($tabEtatqualite["COMPLEMENT_BILAN"]!="" ? ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$map.$strDirArchive.'/'.$tabEtatqualite["COMPLEMENT_BILAN"] : "")));  

                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_INDICE_CONFIANCE");
                        $elqualElt = $dom->create_element("ogr:ID_CONFIANCE");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode($tabEtatqualite["INDICE_CONFIANCE"]));  

                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_FICHE_SYNTHESE");                        
                        $elqualElt = $dom->create_element("ogr:FICHE_SYNTHESE");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode(($tabEtatqualite["FICHE_SYNTHESE"]!="" ? ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$map.$strDirArchive.'/'.$tabEtatqualite["FICHE_SYNTHESE"] : "")));                           
                        
                        //$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_METHODE");                        
                        $elqualElt = $dom->create_element("ogr:METHODE_QUAL");
                        $ecolo = $member->append_child($elqualElt);
                        $ecolo->set_content(utf8_encode($tabEtatqualite["METHODE"]));       
                      }
                    }           
                  } 
                }         
              }   
            }           
          }
         
          // creation du gml pour la qualite id
          $dom->dump_file(ALK_SIALKE_PATH.'upload/sextant/'.$map.'/'.$map.'_masse_eau_qualite_'.$nomQualite.'_temp.gml'); 
        
          /***********************
          // transformer le gml en shp + nettoyage
          **************************/

          if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".strtolower($nomQualite).".shp")){
          	unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".strtolower($nomQualite).".shp");
            unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".strtolower($nomQualite).".shx");
            unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".strtolower($nomQualite).".dbf");
       }
    
          if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_qualite_".$nomQualite."_temp.gml")){
            $exec = "ogr2ogr -f 'ESRI Shapefile' -overwrite ".ALK_SIALKE_PATH."upload/sextant/".$map."/".strtolower($nomQualite).".shp ".ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_qualite_".$nomQualite."_temp.gml ";
            exec($exec);             
            $oZip->addFile(ALK_SIALKE_PATH."upload/sextant/".$map."/".strtolower($nomQualite).".shp", strtolower($nomQualite).".shp");
            $oZip->addFile(ALK_SIALKE_PATH."upload/sextant/".$map."/".strtolower($nomQualite).".shx", strtolower($nomQualite).".shx");
            $oZip->addFile(ALK_SIALKE_PATH."upload/sextant/".$map."/".strtolower($nomQualite).".dbf", strtolower($nomQualite).".dbf");            
          }          
          if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_qualite_".$nomQualite."_temp.gml")){
            unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_qualite_".$nomQualite."_temp.gml");
          } 
          if(is_file(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_qualite_".$nomQualite."_temp.gfs")){
            unlink(ALK_SIALKE_PATH."upload/sextant/".$map."/".$map."_masse_eau_qualite_".$nomQualite."_temp.gfs");
          }
        }
        
        $oZip->close();
    if ($bPhp5)   
      ini_set('zend.ze1_compatibility_mode', '1'); 
  }

  
  /**
   * @brief Reg�n�ration des gml utilis�s par openLayer pour l'outil de consult cartographique suite � la mise � jour des �tats de classement des masses d'eau
   *
   * @param	 typeMaj : all (met � jour tous les fichiers gml) / 
   * 									qual (met � jour les fichiers gml qualit�) / 
   * 									reso (met � jour les fichiers gml surveillance r�seaux) / 
   * 									me (met � jour les fichiers gml des infos masses d'eau) 	 
   * @return Retourne un bool�en : 1 maj effectuee, 0 pb de maj
   */
  function traitementMajGml($typeMaj="all")
  {
    global $bPhp5;
    $bPhp5 = false;
    if (PHP_VERSION>='5'){
       ini_set('zend.ze1_compatibility_mode', '0');       
       require_once("lib/domxml-php4-to-php5.php");
       $bPhp5 = true;
    }
       
		//g�n�ration des couches de point
		$map = "LB";
		$dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
		if ($drBassin = $dsBassin->getRowIter())
			$map = $drBassin->getValueName("BASSIN_CODE");
			
		if ($typeMaj == "all" || $typeMaj == "reso"){
  		$dsReseau = $this->oQuery->getReseauForCombo(true, 0,-1, $drBassin->getValueName("BASSIN_ID"));
  		$urlImgSymb = "";
      while ($drReseau = $dsReseau->getRowIter()){
  		  $dom = domxml_open_file(ALK_SIALKE_PATH."upload/carte/modele_point.gml");
  		  $racine = $dom->document_element();
  		  $code = $drReseau->getValueName("RESEAU_CODE");
  		  $id = $drReseau->getValueName("RESEAU_ID");  
  		  $symbol = $drReseau->getValueName("RESEAU_SYMBOLE");
  		  $color = $drReseau->getValueName("RESEAU_COULEUR");  
  		  $taille = $drReseau->getValueName("RESEAU_SYMBOLE_TAILLE");
        $urlImgSymb = $drReseau->getValueName("RESEAU_IMG_SYMB");
  		  $dsPoint = $this->oQuery->getDs_ParametrePoint($id, "-1", "-1");       
        while ($drPoint = $dsPoint->getRowIter()){		    
  		    $this->createFeatureMember($dom, $racine, $drPoint, $symbol, $color, $taille, $code, $urlImgSymb);
  		  }
  		  //$dom = createFeatureMember($dom, $drPoint);
  		  $dom->dump_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/'.$map.'_reseau'.$code.'.gml');
  		}
		}
		
		if ($typeMaj == "all" || $typeMaj == "qual"){
  		//g�n�ration de la couche masse d'eau
  		$dom = domxml_open_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/'.$map.'_masse_eau.gml');
  		$racine = $dom->document_element();
  		$tabElements = $racine->get_elements_by_tagname("shape");
  		foreach($tabElements as $key => $member){
  			$elementCode = $member->get_elements_by_tagname("code_me");
  			$elementCodeEu = $member->get_elements_by_tagname("code_eu");
  		  $codeMe = "";
  		  if (array_key_exists(0, $elementCode))
  		  	$codeMe = $elementCode[0]->get_content();	
  		  if (array_key_exists(0, $elementCodeEu))
  		  	$codeMe = $elementCodeEu[0]->get_content();	
  			
  		  $oMasseEau = new AlkMasseEau($codeMe, $this->oQuery);
  			$tabEtat = $oMasseEau->getEtatQualiteGlobal();	
  			
  			$generalElt = $dom->create_element("ogr:nom_masse_eau");
  		  $general = $member->append_child($generalElt);
  		  $general->set_content(utf8_encode($oMasseEau->data->getValueName("NOM")));
  		  
  			$generalElt = $dom->create_element("ogr:ETAT_GENERAL");
  		  $general = $member->append_child($generalElt);
  		  $general->set_content($tabEtat["COULEUR"]);
  		  
  		  $generalElt = $dom->create_element("ogr:ETAT_GENERAL_IC");
  		  $general = $member->append_child($generalElt);
  		  $general->set_content((array_key_exists("INDICE_CONFIANCE", $tabEtat) ? $tabEtat["INDICE_CONFIANCE"] : ""));
  		  
  			$oMasseEau->setEtatQualite();
  			$tabClassement = $oMasseEau->getEtatQualite();	
  			
  			$strEnteteType = "";
  			foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {		
  				if ($nomClassement != "Global"){
  				  
  					$strNomClassement = strtoupper(strtr($nomClassement, "e����a���u���i���o�c�", "eeeeeaaaauuuuiiiooocc"));
  					$ecololElt = $dom->create_element("ogr:ETAT_".$strNomClassement);
  		  		$ecolo = $member->append_child($ecololElt);
  		  		$ecolo->set_content($tabClassement[$nomClassement]["ETAT"]["COULEUR"]);
  		  		
  		  		$ecololElt = $dom->create_element("ogr:ETAT_".$strNomClassement."_IC");
  		  		$ecolo = $member->append_child($ecololElt);
  		  		$ecolo->set_content((array_key_exists("INDICE_CONFIANCE", $tabClassement[$nomClassement]["ETAT"]) ? $tabClassement[$nomClassement]["ETAT"]["INDICE_CONFIANCE"] : ""));
  		
  					foreach ($tabTypeElementQualite as $iElement=>$tabEtat){
  						if (array_key_exists("ELEMENTS", $tabClassement[$nomClassement][$iElement])){
  							foreach($tabClassement[$nomClassement][$iElement]["ELEMENTS"]["BYNUM"] as $tabEtatqualite){
  								$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]);
  		  					$ecolo = $member->append_child($elqualElt);
  		  					$ecolo->set_content($tabEtatqualite["COULEUR"]);
  
  		  					$elqualElt = $dom->create_element("ogr:QUALITE_".$tabEtatqualite["ID"]."_IC");
  		  					$ecolo = $member->append_child($elqualElt);
  		  					$ecolo->set_content((array_key_exists("INDICE_CONFIANCE", $tabEtatqualite) ? $tabEtatqualite["INDICE_CONFIANCE"] : ""));	
  							}						
  						} 
  					}					
  				}		
  			}						
  		}
  		
  		$dom->dump_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/'.$map.'_masse_eau_etat.gml');
		}
		
		if ($typeMaj == "all" || $typeMaj == "me"){
  		
      //g�n�ration des autres couches masses d'eau		
  		$tabTypoMEC = $tabTypoMET = array();

      $dsTypeMe = $this->oQuery->getDs_listeTypologieMasseEau("C");
  		while ($drTypeMe = $dsTypeMe->getRowIter()){
  			$tabTypoMEC[$drTypeMe->getValueName("MASSE_DESC_CODE")] = $drTypeMe->getValueName("MASSE_DESC_COLOR");
  		}
  		$dsTypeMe = $this->oQuery->getDs_listeTypologieMasseEau("T");
  		while ($drTypeMe = $dsTypeMe->getRowIter()){
  			$tabTypoMET[$drTypeMe->getValueName("MASSE_DESC_CODE")] = $drTypeMe->getValueName("MASSE_DESC_COLOR");
  		}
		  $dsTypeMe = $this->oQuery->getDs_listeTypologieMasseEau("R");
  		while ($drTypeMe = $dsTypeMe->getRowIter()){
  			$tabTypoMER[$drTypeMe->getValueName("MASSE_DESC_CODE")] = $drTypeMe->getValueName("MASSE_DESC_COLOR");
  		}

      // sept2014
      $tabObjEnvAtteint = array();
      $dsTypeObj = $this->oQuery->getDs_listeTypologieAtteinteObjectifEnv($map, -1);
      while ($drTypeObj = $dsTypeObj->getRowIter()){
        $tabObjEnvAtteint[$drTypeObj->getValueName("ATTEINTE_OBJECTIF_CODE")] = $drTypeObj->getValueName("ATTEINTE_OBJECTIF_COLOR");
      }

  		//g�n�ration des autres couches masses d'eau
  		$tabTypeMe = array( "NONE" => array( "LIB" => "Aucun classement", "COLOR" => "#e0f5ff", "COND"=>""),  			
                          //"RISQUE" => array( "LIB" => "Classement RNAOE Risque", "COLOR" => "#cce666", "COND"=>"MASSE_B_RISQUE=1"), 
  												//"RESPECT" => array( "LIB" => "Classement RNAOE Respect", "COLOR" => "#50a48b", "COND"=>"MASSE_B_RISQUE=0"), 
  												//"DOUTE" => array( "LIB" => "Classement RNAOE Doute", "COLOR" => "#EBF1DE", "COND"=>"MASSE_B_RISQUE=2"),
                      	  "SURV" => array( "LIB" => "Retenue au titre du contr�le de surveillance", "COLOR" => "#bed2ff", "COND"=>"MASSE_B_SUIVI=1"),
  												"MEC" => array( "LIB"  => "C�ti�re (MEC)", "COLOR" => "#007d00", "COND"=>"MASSE_TYPE='MEC'"),
  												"MET" => array( "LIB" => "Transition (MET)", "COLOR" => "#ec8c2b", "COND"=>"MASSE_TYPE='MET'"),
  												"MER" => array( "LIB" => "R�cifale (MER)", "COLOR" => "#ec8c2b", "COND"=>"MASSE_TYPE='MER'"),
  												"TYPOMEC" => array( "LIB"  => "Typologie des masses d'eau c�ti�res", "COLOR" => $tabTypoMEC, "COND"=>"MASSE_TYPE='MEC'", "CHAMP"=>"MASSE_DESC_CODE"),
  												"TYPOMET" => array( "LIB"  => "Typologie des masses d'eau de transition", "COLOR" => $tabTypoMET, "COND"=>"MASSE_TYPE='MET'", "CHAMP"=>"MASSE_DESC_CODE"),
  												"TYPOMER" => array( "LIB"  => "Typologie des masses d'eau r�cifales", "COLOR" => $tabTypoMER, "COND"=>"MASSE_TYPE='MER'", "CHAMP"=>"MASSE_DESC_CODE"),                        
                          "TYPOATTOBJ" =>array( "LIB"  => "Atteinte des objectifs environnementaux", "COLOR" => $tabObjEnvAtteint, "COND"=>"MASSE_B_ATTEINTE_OBJ is not null", "CHAMP"=>"ATTEINTE_OBJECTIF_CODE")); 
  
  		foreach ($tabTypeMe as $code=>$tabParam){
  			
  			$tabMe = array();
        $dsMe = $this->oQuery->getDs_listeMasseEau(0, -1, "", $map, $tabParam["COND"]);
        while ($drMe = $dsMe->getRowIter()){
  				$strValue = (array_key_exists("CHAMP", $tabParam) && $tabParam["CHAMP"]!="" ? $drMe->getValueName($tabParam["CHAMP"]) : "present");
  				$tabMe[$drMe->getValueName("MASSE_CODE")] = $strValue;   
  			}	
        
  		  $dom = domxml_open_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/'.$map.'_masse_eau.gml');
  		  $racine = $dom->document_element();
  		  $tabElements = $racine->get_elements_by_tagname("shape");
  		  foreach($tabElements as $key => $member){
  		  	$elementCode = $member->get_elements_by_tagname("code_me");
  		  	$elementCodeEu = $member->get_elements_by_tagname("code_eu");
  		  	$codeMe = "";
  		  	if (array_key_exists(0, $elementCode))
  		  		$codeMe = $elementCode[0]->get_content();
  		  	if (array_key_exists(0, $elementCodeEu))
	  		  	$codeMe = $elementCodeEu[0]->get_content();		
	  		  	if ($codeMe != "") {
  			  		if (array_key_exists($codeMe, $tabMe)){
  			  			$oMasseEau = new AlkMasseEau($codeMe, $this->oQuery);
  			
  							$generalElt = $dom->create_element("ogr:nom_masse_eau");
  					  	$general = $member->append_child($generalElt);
  					  	$general->set_content(utf8_encode($oMasseEau->data->getValueName("NOM")));
  					  
  			  			if (is_array($tabParam["COLOR"])){
  			  				$strColor = $tabParam["COLOR"][$tabMe[$codeMe]];
  			  			} else {
  			  				$strColor = $tabParam["COLOR"];
  			  			}               
  			  			$generalElt = $dom->create_element("ogr:CLASSEMENT");
  			    		$general = $member->append_child($generalElt);
  			    		$general->set_content($strColor);
  		  		} else {
  		  			//suppression du noeud correspond � la masse d'eau pour ne pas l'afficher sur la carte
  		  			$member->unlink_node();
  		  		} 		
  		  	}    
  		  }
  		  $dom->dump_file(ALK_SIALKE_PATH.'upload/carte/'.$map.'/'.$map.'_masse_eau_classement_'.$code.'.gml');  
  		}

  	}
  	
  	// pour lancement genearatio gml sp�cifique Sextant
  	/*if ($type_maj != "reso")
      $this->MajGmlSextant($typeMaj);
    */
  	
  	if ($bPhp5)  	
  	  ini_set('zend.ze1_compatibility_mode', '1'); 
  }
  
	function createFeatureMember($dom, $nodeFeatureCollection, $dr, $graph, $color, $taille, $reseau_id, $urlImgSymb=""){
	  
	  $featureMemberElt = $dom->create_element("gml:featureMember");
	  $featureMember = $nodeFeatureCollection->append_child($featureMemberElt);
	  $citPointElt = $dom->create_element("cit:point");
	  $citPoint = $featureMember->append_child($citPointElt);
	  $citGeomElt = $dom->create_element("cit:the_geom");
	  $citGeom = $citPoint->append_child($citGeomElt);
	  $pointElt = $dom->create_element("gml:Point");
	  $point = $citGeom->append_child($pointElt);
	  $point->set_attribute("srsName", "4326");	  
	  $coordinatesElt = $dom->create_element("gml:coordinates");
	  $coordinates = $point->append_child($coordinatesElt);
	  $coordinates->set_content(($dr->getValueName("POINT_LONGITUDE")).",".($dr->getValueName("POINT_LATITUDE")));
	  
	  $reseauElt = $dom->create_element("cit:reseau_id");
	  $reseau = $citPointElt->append_child($reseauElt);
	  $reseau->set_content($reseau_id);
	  
	  $ptElt = $dom->create_element("cit:point_id");
	  $pt = $citPointElt->append_child($ptElt);
	  $pt->set_content($dr->getValueName("POINT_CODE"));
	  
	  $ptElt2 = $dom->create_element("cit:point_nom");
	  $pt2 = $citPointElt->append_child($ptElt2);
	  $pt2->set_content(utf8_encode($dr->getValueName("POINT_NOM")));

    if($urlImgSymb !=""){// cas d'une icone avec image
      
      $graphicElt = $dom->create_element("cit:graphic");
      $citGraphic = $citPointElt->append_child($graphicElt);
      $citGraphic->set_content("circle");
      
      $colorElt = $dom->create_element("cit:color");
      $citColor = $citPointElt->append_child($colorElt);
      $citColor->set_content("black");
      $tailleElt = $dom->create_element("cit:taille");
      $citTaille = $citPointElt->append_child($tailleElt);
      $citTaille->set_content("3");  
      // chemin img
      $dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
      if ($drBassin = $dsBassin->getRowIter()){
           $bassin_code = $drBassin->getValueName("BASSIN_CODE"); 
      }
      $dsBassin->moveFirst();
      $backgroundGraphicElt = $dom->create_element("cit:backgroundGraphic");
      $citbackgroundGraphicElt = $citPointElt->append_child($backgroundGraphicElt);
      $citbackgroundGraphicElt->set_content($bassin_code."/".$urlImgSymb);
            
    }else{ // cas d'une icone avec parametrage

      $graphicElt = $dom->create_element("cit:graphic");
      $citGraphic = $citPointElt->append_child($graphicElt);
      $citGraphic->set_content($graph);
      $colorElt = $dom->create_element("cit:color");
      $citColor = $citPointElt->append_child($colorElt);
      $citColor->set_content($color);
      $tailleElt = $dom->create_element("cit:taille");
      $citTaille = $citPointElt->append_child($tailleElt);
      $citTaille->set_content($taille);
    }
    

	}  

  /**
   * @brief Affiche la liste des options pour une voie de concours
   *
   * @return Retourne un string
   */
  function getHtmlListeControle()
  {    
  	$iErr = Request("err", REQ_GET, "0", "is_numeric");
    $nbEltParPage = Request("nbEltParPage", REQ_POST_GET, 20, "is_numeric");
    $page = Request("page", REQ_GET, 1, "is_numeric");
    
    $option_controle_id = Request("option_id", REQ_POST_GET, "-1", "is_numeric");
    
    $strParam = "iSheet=".$this->iSheet.
      "&nbEltParPage=".$nbEltParPage."&page=".$page."&iSSheet=".$this->iSSheet."&iModeSSheet=";
   	
   	// liste des �l�ments du dictionnaire s�lectionn�
    $cpt = 0;
    $tabPage = array();
    $iFirst = ($page-1)*$nbEltParPage;
    $iLast = $iFirst+$nbEltParPage-1;

    
		$oCtrlSelectFichier = new HtmlSelect(0, "option_id", $option_controle_id, "S�lectionnez");
		$oCtrlSelectFichier->tabValTxt = array ("0"=>"Seulement les fichiers manquants");
		$oCtrlSelectFichier->tabValTxtDefault = array ("-1", " Voir tous les fichiers");
		$oCtrlSelectFichier->addEvent("onchange", "Recherche('');");
    		
  		$strHtml = "<table cellpadding=0 cellspacing=0 border=0><tr><td>".			
  			"<script language='javascript' src='../../lib/lib_formNumber.js'></script>".
  			"<script language='javascript' src='../../lib/lib_formDate.js'></script>".
  			"<script language='javascript' src='../../lib/lib_form.js'></script>".
        "<script language='javascript' src='lib/lib_atlas.js'></script>".          
        "<form name='formData' action='01_page_form.php?iMode=1&".$strParam.ALK_MODE_SSHEET_LIST."' method='post' enctype='multipart/form-data'>".
        "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
        "<tr>".
        "<td width='400' valign=top>" .
        	$this->_getFrameTitleSheet("Recherche de la masse d'eau").          	
        	"<table width=100% cellpadding=2 cellspacing=2>" .
        	$this->_getHtmlLigneCtrl($oCtrlSelectFichier).
        	"</table>" .
        "</td>".      
        "<td width='580' valign=top>";
        	
    $dsBassin  = $this->oQuery->getTableForCombo("BASSIN_HYDROGRAPHIQUE", "BASSIN", false, true);
    while( $drBassin = $dsBassin->getRowIter() ) {
      $dsDico = $this->oQuery->getDs_listeFichierByBassin($drBassin->getValueName("BASSIN_ID"), $iFirst, $iLast);    
      $nbElt = $dsDico->iCountTotDr;
      $strHtmlListe = "";
      $strHtmlListe .= $this->_getFrameTitleSheet("BASSIN ".$drBassin->getValueName("CODE")." / Liste des fichiers utilis�s").    
        "<script language='javascript' src='lib/lib_atlas.js'></script>".  
        "<form name='formDico' action='' method='post'>".
        "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
        "<tr>".
        "<td width='350' height='10'></td>".
      	"<td width='200'></td>".      
      	"<td width='70'></td>".         
        "</tr>".      
        "<tr class='trEntete1'>".
        "<td class='tdEntete1' align='left'><div class='divTabEntete1'>Liste des fichiers".
        "&nbsp;&nbsp;".$nbElt." enregistrement".($nbElt>1 ? "s" : "")."&nbsp;</div>".      
        "</td>". 
      	"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Type de fichier</div></td>".        
      	"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Pr�sent</div></td>".        
        "</tr>";
     
      $tabPage = array();
      $cpt = 0;
      while( $drDico = $dsDico->getRowIter() ) {        
        $strLib = $drDico->getValueName("FICHIER");      
        
        $strLib = array(wordwrap($strLib, 100, "<br>"), 
                        ALK_SIALKE_URL.'upload/doc/'.$drBassin->getValueName("CODE").'/'.$drDico->getValueName("FICHIER"));
        
        $strFile = ALK_SIALKE_PATH.'upload/doc/'.$drBassin->getValueName("CODE").'/'.$drDico->getValueName("FICHIER");   

        $iPage = ( $nbElt-1 <= ($page-1)*$nbEltParPage ? ($page-1>0 ? $page-1 : 1) : $page);
        
        $bPresent = ( file_exists($strFile) && is_file($strFile));
        if ($option_controle_id == -1 || ($option_controle_id == 0 && !$bPresent)) {                                      
          $tabPage[$cpt] = array($strLib,                                
        	  										 $drDico->getValueName("TYPE_DOC"),
                                 ($bPresent ?"OK" : "NOK") );        	
          $cpt++;
        }
      }
          
      $tabAlign = array("", "left", "center","center","center");
      	
      $strHtmlListe .= getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, $page, 
                                 $_SERVER["PHP_SELF"]."?".$strParam.ALK_MODE_SSHEET_LIST, 
                                 $tabAlign);
      $strHtml .= $strHtmlListe;                           
      
    }
    $strHtml .= "</form><br>";

    $oBtAnnuler = new HtmlLink("01_page_form.php?".$strParam."&iModeSSheet=".ALK_MODE_SSHEET_LIST."&option_id=".$option_controle_id."&iMode=2", "Annuler", 
                               "annul_gen.gif", "annul_gen_rol.gif");
                               
    $strHtml .= "<div class='divTextContenu' style='margin-left:20px' align='center'><br>".$oBtAnnuler->getHtml()."</div>";

    return $strHtml;
  }  
	
	/*
  function &OpenDocumentGml($filename){
     global $bPhp5;
     if ($bPhp5){ 
       $dom =& new DOMDocument();
       $dom->load($filename);
       $dom->formatOutput = true;
     }
      else 
       $dom = domxml_open_file($filename);

     return $dom;  
  }
  
  function saveDocumentGml($filename, &$dom){
     global $bPhp5;
     if ($bPhp5){ 
       $res = $dom->save($filename);
     }
      else 
       $res = $dom->dump_file($filename);

     return $res;  
  }  
  
  function &getRacine(&$node){
     global $bPhp5;
     if ($bPhp5){        
       $racine =& $node;
     }
      else 
       $racine =& $node->document_element();

     return $racine;  
  }  
  
  function createElement(&$dom, $name){
    print_r($dom);
     global $bPhp5;
     if ($bPhp5){        
       $element = $dom->createElement($name);
     }
      else 
       $element = $dom->create_element($name);

     return $element;  
  }    
    
  function &appendChild($node, $element){
     global $bPhp5;
     if ($bPhp5){        
       $newNode = $node->appendChild($element);
     }
      else 
       $newNode = $node->append_child($element);

     return $newNode;  
  }      
  */
}

?>