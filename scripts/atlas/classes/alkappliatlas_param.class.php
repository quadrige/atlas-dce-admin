<?php
include_once("classes/alkappliatlas.class.php");

/**
 * @brief Classe de l'application concours
 *        Classe regroupant des fonctionnalit�s de la rubrique param�trage des concours
 */
class AlkAppliAtlas_param extends AlkAppliAtlas
{

  /**
   * @brief Constructeur par d�faut
   *
   */
  function AlkAppliAtlas_param($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet)
  {
    parent::AlkAppliAtlas($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet);
  }

  /**
   * @brief M�thode virtuelle qui retourne un tableau de boutons htmllink
   *        plac� sur la droite des onglets
   * 
   * @param 
   * @return Retourne un array
   */
  function getTabCtrlBt()
  {
    $iCptBt = 0;
    $tabBt = array();

    return $tabBt;
  }

  /**
   * @brief M�thode virtuelle, retourne un tableau contenant les informations sur les sous onglets
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un array
   */
  function getTabSubSheet()
  {
    global $tabStrType;

    $tabSubSheet = array();

    $strParam = "iSheet=".ALK_SHEET_ATLAS;
    
    $i = 0;
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_DATA,
                               "text"     => "Donn�es par&nbsp;point",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_DATA,
                               "title"    => "Donn�es par point");
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_IMPORT,
                               "text"     => "Import&nbsp;donn�es par&nbsp;point",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_IMPORT."&iModeSSheet=".ALK_MODE_SSHEET_FORM,
                               "title"    => "Import donn�es par point");                                                     
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_OPERATEUR,
                               "text"     => "Op�rateurs",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_OPERATEUR,
                               "title"    => "Liste des op�rateurs");
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_ARCHIVE,
                               "text"     => "Groupe r�seaux",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_ARCHIVE."&iModeSSheet=".ALK_MODE_SSHEET_LIST,
                               "title"    => "Groupe r�seaux");   
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_RESEAU,
                               "text"     => "R�seaux",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_RESEAU,
                               "title"    => "Liste des r�seaux");
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_PARAMETRE,
                               "text"     => "Param�tres",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_PARAMETRE,
                               "title"    => "Liste des param�tres");
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_POINT,
                               "text"     => "Points",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_POINT,
                               "title"    => "Liste des points");      
                                                                               
    return $tabSubSheet;
  }

  /**
   * @brief M�thode virtuelle, retourne le contenu html du corps de l'onglet s�lectionn�
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un string : code html des sous onglets
   */
  function getHtmlBodySheet()
  {
    $strHtml = "";
    switch( $this->iSSheet ) {        
    case ALK_SSHEET_DATA:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeData();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = "";
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementData();
      break;
		case ALK_SSHEET_IMPORT:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheImport();      
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementImport();
      break;
    case ALK_SSHEET_CARTE:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = "";
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheCarte();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementCarte();
      break;  		
		case ALK_SSHEET_OPERATEUR:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementDico();
      break;
		case ALK_SSHEET_RESEAU:   
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_POP )
        $strHtml = $this->getHtmlFicheSupport();  
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_POP_SQL )
        $strHtml = $this->traitementSupport();  
      break;
		case ALK_SSHEET_PARAMETRE:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementDico();
      break;
		case ALK_SSHEET_POINT:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementDico();
			break;
		case ALK_SSHEET_CARTE:
     	if( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheCarte();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementCarte();
      break;	

    case ALK_SSHEET_ARCHIVE:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementDico();
      break;  
		
    default:
      break;
    }
    return $strHtml;
  }

  /**
   * @brief Affiche la liste des options pour une voie de concours
   *
   * @return Retourne un string
   */
  function getHtmlListeDico()
  {    
  	$iErr = Request("err", REQ_GET, "0", "is_numeric");
    $nbEltParPage = Request("nbEltParPage", REQ_POST_GET, 20, "is_numeric");
    $page = Request("page", REQ_GET, 1, "is_numeric");
    
    $strParam = "iSheet=".$this->iSheet.
      "&nbEltParPage=".$nbEltParPage."&page=".$page."&iSSheet=".$this->iSSheet."&iModeSSheet=";
   	
   	// liste des �l�ments du dictionnaire s�lectionn�
    $cpt = 0;
    $tabPage = array();
    $iFirst = ($page-1)*$nbEltParPage;
    $iLast = $iFirst+$nbEltParPage-1;
      
    switch ($this->iSSheet) {    	
    	case ALK_SSHEET_OPERATEUR :
    		$strTitre = "op�rateurs";
    		$dsDico = $this->oQuery->getDs_listeOperateur($iFirst, $iLast, $_SESSION["idBassin"]);
    		break; 
    	case ALK_SSHEET_ARCHIVE :
    		$strTitre = "groupes de r�seaux";
        $dsDico = $this->oQuery->getDs_listeGroupe($iFirst, $iLast);
        break;     		
    	case ALK_SSHEET_RESEAU :
    		$strTitre = "r�seaux";
    		$dsDico = $this->oQuery->getDs_listeReseau($iFirst, $iLast, $_SESSION["idBassin"]);
    		break; 
    	case ALK_SSHEET_POINT :
    		$strTitre = "points";
    		$dsDico = $this->oQuery->getDs_listePoint($iFirst, $iLast, $_SESSION["idBassin"]);
    		break;
    	case ALK_SSHEET_PARAMETRE :
    		$strTitre = "param�tres";
    		$dsDico = $this->oQuery->getDs_listeParametre($iFirst, $iLast, $_SESSION["idBassin"]);
    		// recherche si les param existe dans la table poit reseau pour anticiper message d'erreur'
        $tabParamUsed = array();
        $dsPointParam = $this->oQuery->getDsReseauPoint($_SESSION["idBassin"]);
        while ($drPointParam = $dsPointParam->getRowIter()){
        	$tabParamUsed[] = $drPointParam->getValueName("PARAMETRE_ID");
        }
        
        break; 			 		 			    		    		    
    } 
    
    $oBtAdd = new HtmlLink("01_page_form.php?iMode=1&".$strParam.ALK_MODE_SSHEET_FORM."&page=".$page,
                           "Ajouter un nouvel �l�ment � la liste",
                           "ajouter.gif", "ajouter_rol.gif");
    
    $nbElt = $dsDico->iCountTotDr;

    $strHtml = $this->_getFrameTitleSheet("Gestion des ".$strTitre).    
      "<script language='javascript' src='lib/lib_atlas.js'></script>".  
      "<form name='formDico' action='' method='post'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='450' height='10'></td>".      
      ($this->iSSheet == ALK_SSHEET_PARAMETRE ? "<td width='140'></td><td width='140'></td>"
       : "").
      "<td width='120'></td>".      
      "</tr>".
      ( $iErr == "1"
        ? "<tr><td colspan='2' class='divContenuMsgErr' align='center'>".
        "Impossible de supprimer cet enregistrement car il est encore utilis�.<br><br></td></tr>"
        : "").
      "<tr class='trEntete1'>".
      "<td class='tdEntete1' align='left'><div class='divTabEntete1'>Liste des ".
      $strTitre."&nbsp;&nbsp;".$nbElt." enregistrement".($nbElt>1 ? "s" : "")."</div></td>".      
      ($this->iSSheet == ALK_SSHEET_PARAMETRE ? "<td class='tdEntete1' align='center'><div class='divTabEntete1'>R�seau</div></td>" .
      																					"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Support</div></td>"
       : "").
      "<td class='tdEntete1' align='center'>".$oBtAdd->getHtml()."</td>".
      "</tr>";
   
    while( $drDico = $dsDico->getRowIter() ) {
      $id = $drDico->getValueName("ID");
      $strLib = $drDico->getValueName("LIB");      
      
      $strLib = array(wordwrap($strLib, 100, "<br>"), 
                      "01_page_form.php?iMode=2&".$strParam.ALK_MODE_SSHEET_FORM.
                      "&id=".$id."&page=".$page);

      $iPage = ( $nbElt-1 <= ($page-1)*$nbEltParPage ? ($page-1>0 ? $page-1 : 1) : $page);
      
      // v�rif si le param est utilis� dans liste RESEAU_POINT_PARAM
      $msg = "''";
      if($this->iSSheet == ALK_SSHEET_PARAMETRE){
        if(in_array($id, $tabParamUsed)){
        	$msg = "1";
        }
      }
      
      $oBtSuppr = new HtmlLink("javascript:SupprElt('".$id."', '".$strParam.ALK_MODE_SSHEET_SQL.
                               "&page=".$iPage."',".$msg.")", 
                               "Supprimer cet �l�ment", "tab_supprimer.gif", "tab_supprimer_rol.gif");

      if ($this->iSSheet == ALK_SSHEET_PARAMETRE)
      	$tabPage[$cpt] = array($strLib, $drDico->getValueName("RESEAU_NOM"), $drDico->getValueName("SUPPORT_NOM"), $oBtSuppr->getHtml());
      else
      	$tabPage[$cpt] = array($strLib, $oBtSuppr->getHtml());
      	
      $cpt++;
    }

    // pas de pagination
    //$nbEltParPage = $nbElt;
    if ($this->iSSheet == ALK_SSHEET_PARAMETRE)
    	$tabAlign = array("", "left", "center", "center", "center");
    else
    	$tabAlign = array("", "left", "center");
    	
    $strHtml .= getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, $page, 
                               $_SERVER["PHP_SELF"]."?".$strParam.ALK_MODE_SSHEET_LIST, 
                               $tabAlign).
      "</form><br>";

    $oBtAnnuler = new HtmlLink("01_page_form.php?".$strParam."&iModeSSheet=".ALK_MODE_SSHEET_LIST."&iMode=2", "Annuler", 
                               "annul_gen.gif", "annul_gen_rol.gif");
                               
    $strHtml .= "<div class='divTextContenu' style='margin-left:20px' align='center'><br>".$oBtAnnuler->getHtml()."</div>";

    return $strHtml;
  }
  
  function _getHtmlLigneCtrl($oCtrl) {
  	$strHtml = "<tr>".
		  							"<td align='right' class='formLabel'>".$oCtrl->label."</div></td>".
		  							"<td>".$oCtrl->getHtml()."</td>".
		  							"</tr>";
		return $strHtml;
  }
  
  
  
  
  /**
   * @brief Retourne le code html d'un formulaire dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function getHtmlFicheDico()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    $id = Request("id", REQ_GET, "-1", "is_numeric");
    $page =  Request("page", REQ_GET, "1", "is_numeric");
    
    $strParam = "iMode=".$iMode."&page=".$page.
      "&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		$strParam2 = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		
		$bassin_code = "";
    $dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
		if ($drBassin = $dsBassin->getRowIter())
		  $bassin_code = $drBassin->getValueName("BASSIN_CODE"); 
		  
		$iWidthTxt = 48;
    $iWidthMemo = 200;
    $iHeightMemo = 4;  
    
		$strNom = "";
    $strCode = $iRang = $strCouleur = $strPicto = "";
    $strHtmlCtrl = "";
    $strHtmlSuite = "";
		switch ($this->iSSheet) {    	
    	case ALK_SSHEET_OPERATEUR :
    		$strTitre = "op�rateurs";
    		$dsDico = $this->oQuery->getDs_OperateurById($id);    		
   			if( $drDico = $dsDico->getRowIter() ) {      
      			$strNom = $drDico->getValueName("NOM"); 
      			$strCode = $drDico->getValueName("CODE");     	
    		}
    		$oCtrl = new HtmlText(0, "nom", $strNom, "Nom", 1, $iWidthTxt, 255);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		    $oCtrl = new HtmlText(0, "code", $strCode, "Code", 1, $iWidthTxt, 20);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);	
    		break; 
    	case ALK_SSHEET_ARCHIVE :
    		$strTitre = "groupes de r�seau";
    		$dsDico = $this->oQuery->getDs_listeGroupe(0, -1, $id);    		
   			if( $drDico = $dsDico->getRowIter() ) {      
      			$strNom = $drDico->getValueName("LIB");       			     	
    		}
    		$oCtrl = new HtmlText(0, "nom", $strNom, "Nom", 1, $iWidthTxt, 255);
		    $oCtrl->addValidator("formDico", "text", true);		    
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);	
    		break; 	
    	case ALK_SSHEET_RESEAU :
    		$strTitre = "r�seaux";
    		$iGp = "-1";
    		$taillePicto = "8";
        $strCouleur = "";
        $iRang = "";
        $urlImg = "";
    		$dsDico = $this->oQuery->getDs_ReseauById($id);
        $reseauDateMAJ = null;
        if( $drDico = $dsDico->getRowIter() ) {      
      			$strNom = $drDico->getValueName("NOM");      	
      			$strCode = $drDico->getValueName("CODE");
      			$iRang = $drDico->getValueName("RANG");
      			$strCouleur = $drDico->getValueName("COULEUR");
      			$strPicto = $drDico->getValueName("PICTO");
            $iGp = $drDico->getValueName("GROUPE_ID");
            $taillePicto = $drDico->getValueName("RESEAU_SYMBOLE_TAILLE");
            $urlImg = $drDico->getValueName("IMG_SYMB");
    		}    		
        
        $oCtrl = new HtmlText(0, "nom", $strNom, "Nom", 1, $iWidthTxt, 255);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		    $oCtrl = new HtmlText(0, "code", $strCode, "Code", 1, $iWidthTxt, 20);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);	
        	
		    $oCtrlSelectGp = new HtmlSelect(0, "groupeId", $iGp, "Groupe du r�seau", 1, $iWidthMemo);
        //$oCtrlSelectGp->tabValTxt = array("square"=>"carr�", "triangle"=>"triangle", "circle"=>"rond", "cross"=>"croix", "star"=>"�toile", "x"=>"x");
        $oCtrlSelectGp->oValTxt = $this->oQuery->getTableForCombo("RESEAU_GROUPE", "GROUPE", false, false, "");   
        $oCtrlSelectGp->tabValTxtDefault = array ("-1", " --- S�lectionner un groupe ---");
        $oCtrlSelectGp->addValidator("formDico", "select", true);     
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlSelectGp);
         
        $oCtrlSelect = new HtmlSelect(0, "symbole", $strPicto, "Symbole*", 1, $iWidthMemo);
				$oCtrlSelect->tabValTxt = array("square"=>"carr�", "triangle"=>"triangle", "circle"=>"rond", "cross"=>"croix", "star"=>"�toile", "x"=>"x");
				$oCtrlSelect->tabValTxtDefault = array ("-1", " --- S�lectionner un symbole ---");
//				$oCtrlSelect->addValidator("formDico", "select", true);											
				$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlSelect);		

        $oCtrl = new HtmlText(0, "reseauSymboleTaille", $taillePicto, "Taille du symbole*", 1, 10, 3);
//        $oCtrl->addValidator("formDico", "int", true);  
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);

		  	$oCtrl = new HtmlColor("couleur", $strCouleur,  "Couleur*", 32, 22, "false");
//		  	$oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);			
        
        $oCtrl = new HtmlText(0, "rang", $iRang, "Rang*", 1, 10, 3);
//		    $oCtrl->addValidator("formDico", "int", true);  
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);

        $oCtrl = new HtmlFile(0, "img_symb", $urlImg, "Fichier image du symbole", $iWidthTxt, 255);
        $oCtrl->urlFile = ALK_SIALKE_URL.ALK_PATH_UPLOAD_CARTE.$bassin_code."/".$urlImg;
        $oCtrl->bDualMode = true;
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);

		    $strHtmlSuite = $this->_getFrameTitleSheet("Supports associ�s");	
		    $tabPage = array();
      
    		$dsSupport = $this->oQuery->getDs_listeSupportByReseau($id);
    		
    		$oBtAdd = new HtmlLink("javascript:OpenFiche('iMode=1&idReseau=".$id."&".$strParam2.ALK_MODE_SSHEET_POP."')",
                           "Ajouter un nouvel �l�ment � la liste",
                           "ajouter.gif", "ajouter_rol.gif");    
    		$nbElt = $dsSupport->iCountTotDr;
    		$strHtmlSuite .= "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
		      "<tr>".
		      "<td width='300' height='10'></td>".
		      "<td width='120'></td>".		     
		      "</tr>".		      
		      "<tr class='trEntete1'>".
		      "<td class='tdEntete1' align='left'><div class='divTabEntete1'>Liste des supports" .
		      "&nbsp;&nbsp;".$nbElt." enregistrement".($nbElt>1 ? "s" : "")."</div></td>".      
		      "<td class='tdEntete1' align='center'>".$oBtAdd->getHtml()."</td>".
		      "</tr>";
		   $cpt = 0;
		    while( $drSupport = $dsSupport->getRowIter() ) {
		      $idSupport = $drSupport->getValueName("ID");
		      $strLib = $drSupport->getValueName("LIB");      
		      
		      $strLib = array(wordwrap($strLib, 100, "<br>"), 
		                      "javascript:OpenFiche('iMode=2&idReseau=".$id."&".$strParam2.ALK_MODE_SSHEET_POP.
		                      "&id=".$idSupport."');");
		
		      $oBtSuppr = new HtmlLink("javascript:SupprElt('".$idSupport."', '".$strParam2.ALK_MODE_SSHEET_POP_SQL."&idReseau=".$id."')", 
		                               "Supprimer cet �l�ment", "tab_supprimer.gif", "tab_supprimer_rol.gif");
		
		      $tabPage[$cpt] = array($strLib, $oBtSuppr->getHtml());
		      $cpt++;
		    }
		
		    // pas de pagination
		    //$nbEltParPage = $nbElt;
		    $tabAlign = array("", "left", "center");
		    $strHtmlSuite .= getHtmlListePagine($tabPage, $nbElt, $cpt, 1, 
		                               $_SERVER["PHP_SELF"]."?".$strParam2.ALK_MODE_SSHEET_LIST, 
		                               $tabAlign);
				    
    		break; 
    	case ALK_SSHEET_POINT :
    		$strTitre = "points";
    		$dsDico = $this->oQuery->getDs_PointById($id);	
    		$strLong = "";
    		$strLat = "";    		
    		$idMasse = "-1";
   			if( $drDico = $dsDico->getRowIter() ) {      
      			$strNom = $drDico->getValueName("NOM");
      			$strCode = $drDico->getValueName("CODE");
      			$strLong = $drDico->getValueName("POINT_LONGITUDE");
      			$strLat = $drDico->getValueName("POINT_LATITUDE");
      			$idMasse = $drDico->getValueName("MASSE_ID");
    		}
    		$oCtrl = new HtmlText(0, "nom", $strNom, "Nom", 1, $iWidthTxt, 255);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		  	$oCtrl = new HtmlText(0, "code", $strCode, "Code", 1, $iWidthTxt, 20);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);	
		  	$oCtrl = new HtmlText(0, "longitude", $strLong, "Longitude", 1, $iWidthTxt, 45);
		    $oCtrl->addValidator("formDico", "text", false);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);					
		  	$oCtrl = new HtmlText(0, "latitude", $strLat, "Latitude", 1, $iWidthTxt, 45);
		    $oCtrl->addValidator("formDico", "text", false);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);				
  			$oCtrl = new HtmlSelect(0, "masse", $idMasse, "Masse d'eau", 1);
        $oCtrl->oValTxt = $this->oQuery->getMasseForCombo(-1, 0, -1, $_SESSION["idBassin"]);
        $oCtrl->tabValTxtDefault = array ("-1", "--- S�lectionner une masse d'eau ---");
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);				
    		break;     	
    	case ALK_SSHEET_PARAMETRE :
    		$strTitre = "param�tres";
    		$dsDico = $this->oQuery->getDs_ParametreById($id);	    		
    		$idSupport = "-1";
   			if( $drDico = $dsDico->getRowIter() ) {      
      			$strNom = $drDico->getValueName("NOM");
      			$strCode = $drDico->getValueName("CODE");
      			$idSupport = $drDico->getValueName("SUPPORT_ID");
    		}
    		$oCtrl = new HtmlText(0, "nom", $strNom, "Nom", 1, $iWidthTxt, 255);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		  	$oCtrl = new HtmlText(0, "code", $strCode, "Code", 1, $iWidthTxt, 20);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);			  	
  			$oCtrl = new HtmlSelect(0, "support", $idSupport, "Support", 1);
        $oCtrl->oValTxt = $this->oQuery->getSupportForCombo(-1, 0, -1, $_SESSION["idBassin"]);
        $oCtrl->tabValTxtDefault = array ("-1", "--- S�lectionner un support ---");
        $oCtrl->AddValidator("formDico", "select", true, "-1");
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);				
    		break;     		
    } 	
    $strHtml = $this->_getFrameTitleSheet("<a class='LienSommaire' href='01_page_form.php?".
                                          $strParam.ALK_MODE_SSHEET_LIST."'>Liste ".
                                          $strTitre."</a> / Fiche ").
      "<script language='javascript' src='lib/lib_atlas.js'></script>".                                    
      "<script language='javascript' src='../../lib/lib_formTxt.js'></script>".
      "<script language='javascript' src='../../lib/lib_formSelect.js'></script>".
      "<script language='javascript' src='../../lib/lib_formNumber.js'></script>".
      "<script language='javascript' src='../../lib/lib_formDate.js'></script>".
      "<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<script language='javascript'>".
      	writeJsColor("formDico", "couleurs.php", ALK_SIALKE_URL."media/admin/transp.gif", ALK_SIALKE_URL."media/admin/transp.gif", "", false).
      "</script>".       
      "<form name='formDico' action='01_page_sql.php?".$strParam.ALK_MODE_SSHEET_SQL."&id=".$id.
      "' method='post'  enctype='multipart/form-data'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='200' height='10'></td>".
      "<td width='500'></td>".
      "</tr>";
          
    $strHtml .= $strHtmlCtrl;
    
    $oBtValid = new HtmlLink("javascript:ValiderDico('".$this->iSSheet."')", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    $oBtAnnul = new HtmlLink("01_page_form.php?".$strParam, "Annuler les modifications", 
                             "annul_gen.gif", "annul_gen_rol.gif");
		
    $strHtml .= "</table><br><div align='center'>".
     						$oBtValid->getHtml()."&nbsp;&nbsp;".$oBtAnnul->getHtml()."</div>".
      					"</form>";

		$strHtml .= $strHtmlSuite;
		
    return $strHtml;
  
  }

  /**
   * @brief Retourne le code html d'un formulaire dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function getHtmlFicheCarte()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    
    $strParam = "iMode=".$iMode.
      "&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		
		$iWidthTxt = 48;
    $iWidthMemo = 46;
    $iHeightMemo = 4;  
    		
    $strHtmlCtrl = "";    
    
    $strPath = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_CARTE."atlas_dce.exe";
    $bFic = file_exists($strPath);
    
    if( $bFic ) {	    
	    $oCtrlFile = new HtmlFile(1, "sup_carte", "atlas_dce.exe", "Carte");
	    $oCtrlFile->urlFile = ALK_SIALKE_URL.ALK_PATH_UPLOAD_CARTE."atlas_dce.exe";
	    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlFile);
	    $strLabel = "Autre carte (atlas_dce.exe)";
	  } else
	    $strLabel = "Carte (atlas_dce.exe)";
	  $oCtrl = new HtmlFile(0, "carte", "", $strLabel, $iWidthTxt, 255);
  	$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		
		
		$strHtml = $this->_getFrameTitleSheet("Carte").
      "<script language='javascript' src='lib/lib_atlas.js'></script>".                                    
      "<script language='javascript' src='../../lib/lib_formTxt.js'></script>".
      "<script language='javascript' src='../../lib/lib_formSelect.js'></script>".
      "<script language='javascript' src='../../lib/lib_formNumber.js'></script>".
      "<script language='javascript' src='../../lib/lib_formDate.js'></script>".
      "<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<form name='formDico' action='01_page_sql.php?".$strParam.ALK_MODE_SSHEET_SQL.
      "' method='post'  enctype='multipart/form-data'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='200' height='10'></td>".
      "<td width='500'></td>".
      "</tr>";
          
    $strHtml .= $strHtmlCtrl;
    
    $oBtValid = new HtmlLink("javascript:ValiderDico('0')", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    $oBtAnnul = new HtmlLink("01_page_form.php?".$strParam, "Annuler les modifications", 
                             "annul_gen.gif", "annul_gen_rol.gif");

    $strHtml .= "</table><br><div align='center'>".
     						$oBtValid->getHtml()."&nbsp;&nbsp;".$oBtAnnul->getHtml()."</div>".
      					"</form>";

    return $strHtml;
  
  }
  
  /**
   * @brief Retourne le code html d'un formulaire dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function getHtmlFicheImport()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    
    $strParam = "iMode=".$iMode.
      "&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		
		$iWidthTxt = 48;
    $iWidthMemo = 46;
    $iHeightMemo = 4;  
    		
    $strHtmlCtrl = "";    
    
    $strPath = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT;
        
	  $oCtrl = new HtmlFile(0, "imp_fic", "", "Fichier � importer", $iWidthTxt, 255);
  	$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		
		$oBtVal = new HtmlLink("javascript:Valider();", "Valider",
		                       "valid_gen.gif", "valid_gen_rol.gif");
		$oBtAnn = new HtmlLink("javascript:Annuler()", "Annuler", 
		                       "annul_gen.gif", "annul_gen_rol.gif");
		
		$strHtml = $this->_getFrameTitleSheet("Import de donn�es de l'atlas").
      "<script language='javascript' src='lib/lib_atlas.js'></script>".                                    
      "<script language='javascript' src='../../lib/lib_formTxt.js'></script>".
      "<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<form name='formDico' action='01_page_form.php?".$strParam.ALK_MODE_SSHEET_SQL.
      "' method='post'  enctype='multipart/form-data'>".
      "<table width='420' border='0' cellspacing='0' cellpadding='4' align='center'>".
			  "<tr><td align='center'><div class='divContenuConseil'>".
			  "Pour r&eacute;aliser un import de donn&eacute;es dans l'atlas, vous devez au pr&eacute;alable prendre".
			  " connaissance du <a class='aContenuConseil' href='import_format.pdf' target='blank'>format</a> et des".
			  " <a class='aContenuConseil' href='import_regles.pdf' target='blank'>r&egrave;gles d'importation".
			  "</a>.<br> Lorsque l'import est termin&eacute;, le syst&egrave;me renvoie une adresse vous permettant de".
			  " consulter le compte-rendu de l'importation.<br> Plusieurs exemples de fichiers d'import sont disponibles".
			  "&nbsp;: <a class='aContenuConseil' href='import_ex1.txt' target='blank'>exemple 1</a>,".
			  " <a class='aContenuConseil' href='import_ex2.txt' target='blank'> exemple 2</a>.</div>".
			  "</td></tr></table>".
      
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='200' height='10'></td>".
      "<td width='500'></td>".
      "</tr>";
          
    $strHtml .= $strHtmlCtrl;
    
    $oBtValid = new HtmlLink("javascript:ValiderImport()", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    $oBtAnnul = new HtmlLink("01_page_form.php?".$strParam, "Annuler", 
                             "annul_gen.gif", "annul_gen_rol.gif");

    $strHtml .= "</table><br><div align='center'>".
     						$oBtValid->getHtml()."&nbsp;&nbsp;".$oBtAnnul->getHtml()."</div>".
      					"</form>";

    return $strHtml;
  
  }
    
  /**
   * @brief Retourne le code html d'un formulaire dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function getHtmlFicheSupport()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    $id = Request("id", REQ_GET, "-1", "is_numeric");
    $idReseau = Request("idReseau", REQ_GET, "-1", "is_numeric");
       
    $strParam = "iMode=".$iMode.
      "&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		
		$iWidthTxt = 48;

		$strNom = "";
    $strCode = "";
		$dsDico = $this->oQuery->getDs_SupportById($id);    		
   	if( $drDico = $dsDico->getRowIter() ) {      
    	$strNom = $drDico->getValueName("NOM");      	
    	$strCode = $drDico->getValueName("CODE");
    }
    $strHtmlCtrl = "";
    $oCtrlH = new HtmlHidden("idReseau", $idReseau);		
    $oCtrl = new HtmlText(0, "nom", $strNom, "Nom", 1, $iWidthTxt, 255);
		$oCtrl->addValidator("formDico", "text", true);
		$strHtmlCtrl .= $oCtrlH->getHtml().$this->_getHtmlLigneCtrl($oCtrl);
    $oCtrl = new HtmlText(0, "code", $strCode, "Code", 1, $iWidthTxt, 20);
		$oCtrl->addValidator("formDico", "text", true);
		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);		  
		
		$strHtml = $this->_getFrameTitleSheet("Fiche support").
      "<script language='javascript' src='lib/lib_atlas.js'></script>".                                    
      "<script language='javascript' src='../../lib/lib_formTxt.js'></script>".
      "<script language='javascript' src='../../lib/lib_formSelect.js'></script>".
      "<script language='javascript' src='../../lib/lib_formNumber.js'></script>".
      "<script language='javascript' src='../../lib/lib_formDate.js'></script>".
      "<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<form name='formDico' action='01_page_sql.php?".$strParam.ALK_MODE_SSHEET_POP_SQL."&id=".$id.
      "' method='post'  enctype='multipart/form-data'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='200' height='10'></td>".
      "<td width='500'></td>".
      "</tr>";
          
    $strHtml .= $strHtmlCtrl;
    
    $oBtValid = new HtmlLink("javascript:ValiderDico()", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    $oBtAnnul = new HtmlLink("javascript:closeWindow();", "Annuler les modifications", 
                             "annul_gen.gif", "annul_gen_rol.gif");
		
    $strHtml .= "</table><br><div align='center'>".
     						$oBtValid->getHtml()."&nbsp;&nbsp;".$oBtAnnul->getHtml()."</div>".
      					"</form>";
		
    return $strHtml;
  
  }
  
  
  /**
   * @brief Traitement des actions sur dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function traitementDico()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    $id = Request("id", REQ_GET, "-1", "is_numeric");
	  $page =  Request("page", REQ_GET, "1", "is_numeric");

    $strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_LIST."&page=".$page;
    $strUrlBack = "01_page_form.php?".$strParam;

    $oldRang = "-1";
		$tabValue = array();
		
		switch ($this->iSSheet) {			
     case ALK_SSHEET_OPERATEUR :
    		$tabValue["OPERATEUR_NOM"] = array( 0, Request("nom", REQ_POST, ""));
    		$tabValue["OPERATEUR_CODE"] = array( 0, Request("code", REQ_POST, ""));
    		$tabValue["BASSIN_ID"] = array( 1, $_SESSION["idBassin"]);
	    	$strFieldKey = "OPERATEUR_ID";
	    	$strTable = "OPERATEUR";
    		break; 
    	case ALK_SSHEET_RESEAU :

        $bDel = Request("img_symb", REQ_POST, 0); // si case efface cochee
        $strFileName = "";
        $IMG_SYMB = "";
        $dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
        if ($drBassin = $dsBassin->getRowIter()){
            $bassin_code = $drBassin->getValueName("BASSIN_CODE"); 
        }
        $strPathUpload = ALK_PATH_UPLOAD_CARTE.$bassin_code."/";            
        $idReseau = Request("id", REQ_POST_GET, "");   
        $dsReseau = $this->oQuery->getDs_ReseauById($idReseau);
        if($drReseau = $dsReseau ->getRowIter()){
        	$IMG_SYMB = $drReseau->getValueName("IMG_SYMB"); // pour effacer l'ancien fichie importer si besoin
        } 
        if((isset($_FILES["img_symb"]) && $_FILES["img_symb"]["name"]!="" )|| $bDel==1){         
          $strFileName = DoUpload("img_symb", "", $strPathUpload, 1, $IMG_SYMB);
        }else{// pas d'upload'
        	$strFileName = $IMG_SYMB;
        }
        $tabValue["RESEAU_IMG_SYMB"]= array(0, $strFileName);
    		$tabValue["RESEAU_NOM"] = array( 0, Request("nom", REQ_POST, ""));
    		$tabValue["RESEAU_CODE"] = array( 0, Request("code", REQ_POST, ""));
    		$tabValue["RESEAU_RANG"] = array( 1, Request("rang", REQ_POST, ""));
    		$tabValue["RESEAU_SYMBOLE"] = array( 0, Request("symbole", REQ_POST, ""));
    		$tabValue["RESEAU_COULEUR"] = array( 0, Request("couleur", REQ_POST, ""));
    		$tabValue["BASSIN_ID"] = array( 1, $_SESSION["idBassin"]);
        $tabValue["GROUPE_ID"] = array( 1, Request("groupeId", REQ_POST, ""));        
        $tabValue["RESEAU_SYMBOLE_TAILLE"] = array( 1, Request("reseauSymboleTaille", REQ_POST, ""));           
	    	$strFieldKey = "RESEAU_ID";
	    	$strTable = "RESEAU";
    		break; 
    	case ALK_SSHEET_POINT :
    		$tabValue["POINT_NOM"] = array( 0, Request("nom", REQ_POST, ""));
    		$tabValue["POINT_CODE"] = array( 0, Request("code", REQ_POST, ""));
    		$tabValue["POINT_LONGITUDE"] = array( 0, Request("longitude", REQ_POST, ""));
    		$tabValue["POINT_LATITUDE"] = array( 0, Request("latitude", REQ_POST, ""));
    		$tabValue["MASSE_ID"] = array( 1, Request("masse", REQ_POST, ""));
	    	$strFieldKey = "POINT_ID";
	    	$strTable = "POINT";
	    	break;		
	   case ALK_SSHEET_PARAMETRE :
    		$tabValue["PARAMETRE_NOM"] = array( 0, Request("nom", REQ_POST, ""));
    		$tabValue["PARAMETRE_CODE"] = array( 0, Request("code", REQ_POST, ""));
    		$tabValue["SUPPORT_ID"] = array( 1, Request("support", REQ_POST, ""));
	    	$strFieldKey = "PARAMETRE_ID";
	    	$strTable = "PARAMETRE";        
        break;		 	
      case ALK_SSHEET_ARCHIVE :
    		$tabValue["GROUPE_NOM"] = array( 0, Request("nom", REQ_POST, ""));
    		$strFieldKey = "GROUPE_ID";
	    	$strTable = "RESEAU_GROUPE";        
        break;		 	   
		}
    switch( $iMode ) {
    case "1": // mode ajout
    case "2": // mode modif           
   		
      if( $iMode == "1" ) {
        $id = $this->oQueryAction->add_ficheDico($strTable, $strFieldKey, $tabValue);
      } else {
        $this->oQueryAction->update_ficheDico($strTable, $strFieldKey, $id, $tabValue);
      }

      break;

    case "3": // mode suppr
      if($this->iSSheet == ALK_SSHEET_PARAMETRE){// cas des parametre on efface en cascade
      	$this->oQueryAction->delParamById($id);
        
      }else{
        $bRes = $this->oQueryAction->del_ficheDico($strTable, $strFieldKey, $id);
        if( $bRes == false ) {
          $strUrlBack .= "&err=1";
        }
      }
      break;
    }
    
    switch ($this->iSSheet) {			
      case ALK_SSHEET_RESEAU :
      case ALK_SSHEET_POINT :
        $this->traitementMajGml();
        $this->traitementMajMe();
        break;        
    }

    return $strUrlBack;
  }  

  /**
   * @brief Traitement Sql des options, cas fonction du iMode pass� en GET.
   *
   * @return Retourne une chaine : l'url de redirection ou vide si redirection en javascript
   */
  function traitementCarte()
  {
  	 $strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_FORM;
    $strUrlBack = "01_page_form.php?".$strParam;
    
    $iMode = Request("iMode", REQ_GET, "-1", "is_numeric");
    $iSup = RequestCheckbox("sup_carte", REQ_POST);
		$strPathUpload = ALK_PATH_UPLOAD_CARTE;    

		//renommage de l'ancien fichier avant upload ??
    $strFileName = DoUpload("carte", "new_", $strPathUpload, $iSup, "atlas_dce.exe");
    
    echo $strFileName;            
     
    if (substr($strFileName, -4) == '.tgz'){        
        //extraction des fichiers
        include_once("../../classes/archive/archive.php");

				$strPath = ALK_ROOT_PATH.ALK_SIALKE_DIR.$strPathUpload;
				
				$strPathFileLog = $strPath."logzip.txt";
				$strPathFileZip = $strFileName;
				
				$oZip = new gzip_file($strFileName);
				
				$oZip->set_options(array("basedir" => $strPath,
				                         "overwrite" => 1));
				
				$oZip->extract_files();
				
    } else {
    	//renommage du fichier    
    	$strPathUpload = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_CARTE;
    	if (file_exists($strPathUpload)) {
    		rename($strPathUpload.$strFileName, $strPathUpload."atlas_dce.exe");
	    }    	    
    }
       die();
    return $strUrlBack;
  }

  /**
   * @brief Traitement des actions sur dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function traitementSupport()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    $id = Request("id", REQ_GET, "-1", "is_numeric");
	  $page =  Request("page", REQ_GET, "1", "is_numeric");

		$idReseau = Request("idReseau", REQ_POST_GET, "-1", "is_numeric");
		
    $strParam = "iMode=2&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_FORM."&id=".$idReseau;
    $strUrlBack = "01_page_form.php?".$strParam;
    
		$tabValue = array();
		
		$tabValue["SUPPORT_NOM"] = array( 0, Request("nom", REQ_POST, ""));
    $tabValue["SUPPORT_CODE"] = array( 0, Request("code", REQ_POST, ""));
    $tabValue["RESEAU_ID"] = array( 1, $idReseau);
	  $strFieldKey = "SUPPORT_ID";
	  $strTable = "SUPPORT";
    
    switch( $iMode ) {
    case "1": // mode ajout
    case "2": // mode modif           
   		
      if( $iMode == "1" ) {
        $id = $this->oQueryAction->add_ficheDico($strTable, $strFieldKey, $tabValue);
      } else {
        $this->oQueryAction->update_ficheDico($strTable, $strFieldKey, $id, $tabValue);
      }
      break;

    case "3": // mode suppr
      $bRes = $this->oQueryAction->del_ficheDico($strTable, $strFieldKey, $id);
      if( $bRes == false ) {
        $strUrlBack .= "&err=1";
      }
      break;
    }
    return $strUrlBack;
  }    
  
  /**
   * @brief Affiche la liste des options pour une voie de concours
   *
   * @return Retourne un string
   */
  function getHtmlListeData()
  {    
  	$iErr = Request("err", REQ_GET, "0", "is_numeric");
  	$page =  Request("page", REQ_GET, "1", "is_numeric");
		$nbEltParPage = Request("nbEltParPage", REQ_POST_GET, 20, "is_numeric");		  	
		$iFirst = ($page-1)*$nbEltParPage;
    $iLast = $iFirst+$nbEltParPage-1;
    
	  $pt_reseau_id = Request("pt_reseau_id", REQ_POST_GET, "-1");
		$pt_masse_id = Request("pt_masse_id", REQ_POST_GET, "-1", "is_numeric");
		$pt_point_id = Request("pt_point_id", REQ_POST_GET, "-1");

	  $fc_reseau_id = Request("fc_reseau", REQ_GET, "-1", "is_numeric");
		$fc_point_id = Request("fc_point", REQ_GET, "-1", "is_numeric");

    $strParam = "iSheet=".$this->iSheet.
      					"&iSSheet=".$this->iSSheet."&page=".$page.
    						"&pt_reseau_id=".$pt_reseau_id.
    						"&pt_masse_id=".$pt_masse_id.
    						"&pt_point_id=".$pt_point_id.
      					"&iModeSSheet=";
   	
   	// liste des �l�ments du dictionnaire s�lectionn�
    $cpt = 0;
    $tabPage = array();
    
    $oCtrlSelectReseau = new HtmlSelect(0, "pt_reseau_id", $pt_reseau_id, "R�seau");
		$oCtrlSelectReseau->oValTxt = $this->oQuery->getReseauForCombo(false, $idFirst=0, $idLast=-1, $_SESSION["idBassin"]);
		$oCtrlSelectReseau->tabValTxtDefault = array ("-1", "--- S�lectionner un r�seau ---");

		$oCtrlSelectMasse = new HtmlSelect(0, "pt_masse_id", $pt_masse_id, "Masse d'eau");
		$oCtrlSelectMasse->oValTxt = $this->oQuery->getMasseForCombo($pt_reseau_id, 0, -1, $_SESSION["idBassin"]);
		$oCtrlSelectMasse->addEvent("onchange", "majPoint()");
		$oCtrlSelectMasse->tabValTxtDefault = array ("-1", "--- S�lectionner une masse d'eau ---");

		$oCtrlSelectPoint = new HtmlSelect(0, "pt_point_id", $pt_point_id, "Point");
		$oCtrlSelectPoint->oValTxt = $this->oQuery->getPointForCombo($pt_reseau_id, $pt_masse_id, false, 0, -1, $_SESSION["idBassin"]);
		$oCtrlSelectPoint->tabValTxtDefault = array ("-1", "--- S�lectionner un point ---");
      
    $oBtRech = new HtmlLink("javascript:Recherche('')",
                           "Rechercher",
                           "rechercher.gif", "rechercher_rol.gif");
                              
    $strHtmlAdd = "";                              
    $dsData = $this->oQuery->getDs_ParametrePoint($pt_reseau_id, $pt_masse_id, $pt_point_id, $bGroupBy = true, $param_id = -1, $iFirst, $iLast, $_SESSION["idBassin"]);	    
    $nbElt = $dsData->iCountTotDr;
		if ($nbElt < 1 && $pt_reseau_id != "-1" && $pt_point_id != "-1") {
			 $oBtAdd = new HtmlLink("javascript:Recherche('01_page_sql.php?iMode=1&".$strParam.ALK_MODE_SSHEET_SQL."');",
                           "Ajouter des param�tres pour le r�seau et le point s�lectionn�",
                           "ajouter.gif", "ajouter_rol.gif");
       $strHtmlAdd = $oBtAdd->getHtml();                 
		}
	
		$strHtml = "<table cellpadding=0 cellspacing=0 border=0><tr><td>".			
			"<script language='javascript' src='../../lib/lib_formNumber.js'></script>".
			"<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<script language='javascript' src='lib/lib_atlas.js'></script>".  
      "<form name='formData' action='01_page_form.php?iMode=1&".$strParam.ALK_MODE_SSHEET_LIST."' method='post'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='400' valign=top>" .
      	$this->_getFrameTitleSheet("Recherche des points").          	
      	"<table width=100% cellpadding=2 cellspacing=2>" .
      	$this->_getHtmlLigneCtrl($oCtrlSelectReseau).
      	$this->_getHtmlLigneCtrl($oCtrlSelectMasse).
      	$this->_getHtmlLigneCtrl($oCtrlSelectPoint).
      	"<tr>" .
      	"<td height=35  class=\"navig-parents\" align=center valign=bottom colspan=2>".$oBtRech->getHtml()."</td>" .
      	"</tr>" .
      	"</table>" .
      "</td>".
      "<td width='580' valign=top>".
				$this->_getFrameTitleSheet("Gestion des donn�es").      
	      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
	      "<tr>".
	      "<td width='250' height='10'></td>".
	      "<td width='80'></td>".
	      "<td width='80'></td>".	   	   
	      "</tr>".
		      ( $iErr == "1"
		        ? "<tr><td colspan='2' class='divContenuMsgErr' align='center'>".
		        "Impossible de supprimer cet enregistrement car il est encore utilis�.<br><br></td></tr>"
		        : "").
		      "<tr class='trEntete1'>".
		      "<td class='tdEntete1' align='left'><div class='divTabEntete1'>Liste des donn�es&nbsp;&nbsp;".$nbElt." enregistrement".($nbElt>1 ? "s" : "")."</div></td>".            		    
					"<td class='tdEntete1' align='center'></td>".
		      "<td class='tdEntete1' align='center'>".$strHtmlAdd."</td>".		      
		      "</tr>";
   
    while( $drData = $dsData->getRowIter() ) {
      $idReseau = $drData->getValueName("RESEAU_ID");
      $idPoint = $drData->getValueName("POINT_ID");
      $codePoint = $drData->getValueName("POINT_CODE");
      $nomReseau = $drData->getValueName("RESEAU_NOM");
     
      $strLib = array(wordwrap($nomReseau." - ".$codePoint, 100, "<br>"), 
                      "javascript:Recherche('01_page_form.php?".$strParam.ALK_MODE_SSHEET_LIST."&fc_reseau=".$idReseau."&fc_point=".$idPoint."')");

      $iPage = ( $nbElt-1 <= ($page-1)*$nbEltParPage ? ($page-1>0 ? $page-1 : 1) : $page);
      
      $oBtDupliquer = new HtmlLink("javascript:DupliquerAllPointParam('".$idReseau."', '".$idPoint."', '".$strParam.ALK_MODE_SSHEET_SQL."')", 
                               "Dupliquer les donn�es", "tab_appliquer.gif", "tab_appliquer_rol.gif");
                               
      $oBtSuppr = new HtmlLink("javascript:SupprEltData(2, '".$idReseau."', '".$idPoint."', '', '01_page_sql.php?".$strParam.ALK_MODE_SSHEET_SQL."')", 
                               "Supprimer les donn�es", "tab_supprimer.gif", "tab_supprimer_rol.gif");

      $tabPage[$cpt] = array($strLib, $oBtDupliquer->getHtml(), $oBtSuppr->getHtml());
      	
      $cpt++;
    }
    
    // pas de pagination
    //$nbEltParPage = $nbElt;    
    $tabAlign = array("", "left", "center", "center");
    	
    $strHtml .= getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, $page, 
                               $_SERVER["PHP_SELF"]."?".$strParam.ALK_MODE_SSHEET_LIST, 
                               $tabAlign).
      "<br>";
               
		$strHtml .= "</td></tr>" .		
				(($fc_reseau_id != "-1" && $fc_point_id != "-1") ? "<tr><td colspan=2>".$this->_getHtmlFicheParamPoint($fc_reseau_id, $fc_point_id)."</td></tr>" : "").							    
				"</table></form>";
		
    return $strHtml;
  }  
  
  function _getHtmlFicheParamPoint($fc_reseau_id, $fc_point_id) {
  	
  	$tabPage = array();
    
    $iMode =  Request("iMode", REQ_GET, "1", "is_numeric");
    $page =  Request("page", REQ_GET, "1", "is_numeric");
  	$strParam = "iSheet=".$this->iSheet.
      					"&iSSheet=".$this->iSSheet."&page=".$page."&iModeSSheet=";
      					  	                        
  	$dsData = $this->oQuery->getDs_ParametrePoint($fc_reseau_id, "", $fc_point_id, false);
  	$nbElt = $dsData->iCountTotDr;
  	
    $codePoint = "";
    $nomReseau = "";
    if ($nbElt < 1){
      $dsPoint = $this->oQuery->getDs_PointById($fc_point_id);
      if ($drPoint = $dsPoint->getRowIter())
        $codePoint = $drPoint->getValueName("CODE");
      $dsReseau = $this->oQuery->getDs_ReseauById($fc_reseau_id);
      if ($drReseau = $dsReseau->getRowIter())
        $nomReseau = $drReseau->getValueName("NOM");  
    }
    
  	$iWidthTxt = 10;
    $iWidthMemo = 150;
    $iHeightMemo = 4;  
    		    		
  	$dsOperateur = $this->oQuery->getOperateurForCombo();
  	$dateMaj = "";  	  	  	
  	$cpt = 0;
  	while( $drData = $dsData->getRowIter() ) {
      $idReseau = $drData->getValueName("RESEAU_ID");
      $idPoint = $drData->getValueName("POINT_ID");
      $codePoint = $drData->getValueName("POINT_CODE");
      $nomReseau = $drData->getValueName("RESEAU_NOM");
      $nomParam = $drData->getValueName("PARAM_NOM_LG");
      $idParam = $drData->getValueName("PARAMETRE_ID");
      $dateMaj = $drData->getValueName("RESEAU_DATE_MAJ");
     
      $strLib = $nomParam;
            
      $oCtrlSelectOpLabo = new HtmlSelect(0, $idParam."_op_labo_id", $drData->getValueName("LABO_ID"), "", 1, $iWidthMemo);
			$oCtrlSelectOpLabo->oValTxt = $dsOperateur;
			$oCtrlSelectOpLabo->tabValTxtDefault = array ("-1", "");
			$oCtrlSelectOpTer = new HtmlSelect(0, $idParam."_op_ter_id", $drData->getValueName("TERRAIN_ID"), "", 1, $iWidthMemo);
			$oCtrlSelectOpTer->oValTxt = $dsOperateur;
			$oCtrlSelectOpTer->tabValTxtDefault = array ("-1", "");
			$oCtrlAnnee = new HtmlText(0, $idParam."_annee",  $drData->getValueName("ANNEE_PRELEVEMENT"), "Ann�e", 1, $iWidthTxt, 255);			
			$oCtrlFrequence = new HtmlText(0, $idParam."_freq",  $drData->getValueName("FREQUENCE"), "Fr�quence", 1, $iWidthTxt, 255);
			$oCtrlPeriode = new HtmlText(0, $idParam."_periode",  $drData->getValueName("PERIODE"), "P�riode", 1, $iWidthTxt, 255);
			$oCtrlFreqPlan = new HtmlText(0, $idParam."_freqplan",  $drData->getValueName("FREQUENCE_PLAN"), "Fr�quence plan", 1, $iWidthTxt, 255);
			                         
      $oBtSuppr = new HtmlLink("javascript:SupprEltData(3, '".$fc_reseau_id."', '".$fc_point_id."', '".$idParam."', '01_page_sql.php?".$strParam.ALK_MODE_SSHEET_SQL."')", 
                               "Supprimer les donn�es", "tab_supprimer.gif", "tab_supprimer_rol.gif");

      $tabPage[$cpt] = array($strLib, 
      											 $oCtrlSelectOpLabo->getHtml(), $oCtrlSelectOpTer->getHtml(),
      											 $oCtrlAnnee->getHtml(), $oCtrlFrequence->getHtml(),
      											 $oCtrlPeriode->getHtml(), $oCtrlFreqPlan->getHtml(),    
      											 $oBtSuppr->getHtml());
      	
      $cpt++;
    }
    
    $nbEltParPage = $nbElt;
    $tabAlign = array("", "left", "center", "center", "center", "center", "center", "center", "center");
    
    $oCtrlSelectParam = new HtmlSelect(0, "add_param", "-1", "", 1, 300);
		$oCtrlSelectParam->oValTxt = $this->oQuery->getParamForCombo($fc_reseau_id, $fc_point_id);
		$oCtrlSelectParam->tabValTxtDefault = array ("-1", "--- S�lectionner le param�tre � ajouter ---");
		
		$oCtrlH = new HtmlHidden("fc_reseau", $fc_reseau_id);
		$oCtrlH->addHidden("fc_point", $fc_point_id);
		
    $oBtAdd = new HtmlLink("javascript:Recherche('01_page_sql.php?iMode=5&".$strParam.ALK_MODE_SSHEET_SQL."');",
                           "Ajouter un nouvel �l�ment � la liste",
                           "ajouter.gif", "ajouter_rol.gif");
    
    $strHtml = $this->_getFrameTitleSheet("Donn�es du point <u>".$codePoint."</u> pour le r�seau <u>".$nomReseau."</u>".($dateMaj!="" ? " mise � jour le ".$dateMaj : ""));
  	$strHtml .=  "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
	      "<tr>".
	      "<td width='180' height='10'></td>".
	      "<td width='80'></td>".
	      "<td width='80'></td>".
	      "<td width='60'></td>".	   	   
	      "<td width='60'></td>".	   	   
	      "<td width='60'></td>".	   	   
	      "<td width='60'></td>".	   	   	   	   
	      "</tr>".		
	      	"<tr class='trEntete1'>".
		      "<td height=30 class='tdEntete1' align='right' colspan=7>".$oCtrlH->getHtml().$oCtrlSelectParam->getHtml()."</td>".
		      "<td class='tdEntete1' align='center'>".$oBtAdd->getHtml()."</td>".		      
		      "</tr>".     
		      "<tr class='trEntete1'>".
		      "<td class='tdEntete1' align='left'><div class='divTabEntete1'>Param�tre</div></td>".            		    
					"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Op�rateur labo</div></td>".
					"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Op�rateur terrain</div></td>".
					"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Derni�re ann�e</div></td>".
					"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Fr�quence / ann�e</div></td>".
					"<td class='tdEntete1' align='center'><div class='divTabEntete1'>P�riode</div></td>".
					"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Fr�quence / plan</div></td>".
		      "<td class='tdEntete1' align='center'></td>".		      
		      "</tr>";
		      
    $strHtml .= getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, 1, 
                               $_SERVER["PHP_SELF"]."?".$strParam.ALK_MODE_SSHEET_LIST, 
                               $tabAlign).
      "<br>";
    $oBtValid = new HtmlLink("javascript:Recherche('01_page_sql.php?iMode=6&".$strParam.ALK_MODE_SSHEET_SQL."')", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    
    $strHtml .= "</table><br><div align='center'>".
     						$oBtValid->getHtml()."</div>";
     						  	  	
  	return $strHtml;
  }  
  
  /**
   * @brief Traitement des actions sur dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function traitementData()
  {
    $iMode = Request("iMode", REQ_GET, "-1", "is_numeric"); 
  	$page =  Request("page", REQ_GET, "1", "is_numeric");
    
		$pt_reseau_id = Request("pt_reseau_id", REQ_POST, "-1");
		$pt_masse_id = Request("pt_masse_id", REQ_POST, "-1", "is_numeric");
		$pt_point_id = Request("pt_point_id", REQ_POST, "-1");

	  $fc_reseau_id = Request("fc_reseau", REQ_POST_GET, "-1", "is_numeric");
		$fc_point_id = Request("fc_point", REQ_POST_GET, "-1", "is_numeric");
		
    $strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_LIST."&page=".$page;
    $strParam2 = "&fc_reseau=".$fc_reseau_id."&fc_point=".$fc_point_id;
    
    if ($iMode == -1)  
    	return "01_page_form.php?".$strParam;
    
    $strUrlBack = "01_page_form.php?".$strParam;
    
    switch ($iMode) {
    	case 1 :
    		//ajout des param�tres par d�faut li�s � un r�seau pour le point et le reseau pass�s en get
    		$this->oQueryAction->addParam($pt_reseau_id, $pt_point_id, "ALL");
    		$strUrlBack .= $strParam2;
    		break;
    	case 2 :
        //Suppression de tous les param�tres pour le point et le reseau pass�s en get
    		$this->oQueryAction->delParam($fc_reseau_id, $fc_point_id, "ALL");
    		break;
    	case 3 :
        //Suppression du param�tre pour le param�tre, le point et le reseau pass�s en get    		       
        $param_id = Request("sup_param", REQ_GET, "-1", "is_numeric");
    		if ($param_id != "-1"){
    			$this->oQueryAction->delParam($fc_reseau_id, $fc_point_id, $param_id);
    		}
    		$strUrlBack .= $strParam2;
    		break;
    	case 4 :
    		//Dupliquer les param�tres d'un point et r�seau pour un autre couple point / r�seau
    		break;
    	case 5 :
    		//Ajout d'un param�tre pour un point / r�seau
    		$param_id = Request("add_param", REQ_POST, "-1", "is_numeric");
    		if ($param_id != "-1"){
    			$this->oQueryAction->addParam($fc_reseau_id, $fc_point_id, $param_id);
    		}
    		$strUrlBack .= $strParam2;
    		break;
    	case 6 :
    		//Enregistrement de la saisie compl�te de tous les param�tres d'un point / r�seau
    		//Parcoure des param�tres existants 
    		$dsData = $this->oQuery->getDs_ParametrePoint($fc_reseau_id, "", $fc_point_id, false);
    		while ($drData = $dsData->getRowIter()) {
    			$idParam = $drData->getValueName("PARAMETRE_ID");
    			$labo_id = Request($idParam."_op_labo_id", REQ_POST, "-1", "is_numeric");
    			$ter_id = Request($idParam."_op_ter_id", REQ_POST, "-1", "is_numeric");
    			$strAnnee = Request($idParam."_annee", REQ_POST, "");
    			$strFreq = Request($idParam."_freq", REQ_POST, "");
    			$strPeriode = Request($idParam."_periode", REQ_POST, "");
    			$strFreqPlan = Request($idParam."_freqplan", REQ_POST, "");
    			
    			$this->oQueryAction->updateParam($fc_reseau_id, $fc_point_id, $idParam, $labo_id, $ter_id, $strAnnee, $strFreq, $strPeriode, $strFreqPlan);
    		}
    		break;
    }
    
    $this->traitementMajGml();
    $this->traitementMajMe();
		
    return $strUrlBack;
  }      
  
  function traitementImport() {
  	
  	include_once("../../classes/appli/alkimport.class.php");
		include_once("classes/alkimportatlas.class.php");
		include_once("../../classes/appli/alkimporterreur.class.php");
		include_once("../../classes/appli/alkimportwarning.class.php");
		
		$iMode = Request("iMode", REQ_POST, "");
  	$fin = Request("fin", REQ_GET, "0", "is_numeric");
  	
  	$strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_FORM;
      
  	$ag_id = $_SESSION["sit_idUser"];
		$urlFileError = ALK_SIALKE_URL.ALK_PATH_UPLOAD_IMPORT.$ag_id."_err.txt";
		$strFileError = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$ag_id."_err.txt";
		$urlFileWarning = ALK_SIALKE_URL.ALK_PATH_UPLOAD_IMPORT.$ag_id."_warning.txt";
		$strFileWarning = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$ag_id."_warning.txt";
		SupprFichier($strFileError);
		SupprFichier($strFileWarning);
		
		$pathFile = "";
		$strFileName = "";
		
		if( $fin != "1" ) {
		
			if( $iMode != "annuler" ) {
				//upload du fichier
				$strFileName = DoUpload("imp_fic", $ag_id."_", ALK_PATH_UPLOAD_IMPORT, 0, "");
				if( !is_string($strFileName))
					$strFileName = "";
				if( $strFileName!="" )
					$pathFile = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$strFileName;
			}
		}
		$strFichier_Import = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$strFileName;
		$strFichier_Erreur = $strFileError;
		$strFichier_Warning = $strFileWarning; 
		
		$intSeparateur = 9;
		$tabImport = array (
		                    array("name" => "MODE", "oblig" => 1,"type" =>array("S","M","C") ,"lg" => 0,"index" => 0 ,"mode" => 1),
		                    array("name" => "RESEAU", "oblig" => 1, "type" => $this->oQuery->GetDs_champsImport("RESEAU","RESEAU_CODE", "RESEAU_ID", "BASSIN_ID=".$_SESSION["idBassin"]), "lg" => 20,"index" => 0 ,"mode" => 2),
		                    array("name" => "POINT", "oblig" => 1, "type" => $this->oQuery->GetDs_champsImport("POINT","POINT_CODE", "POINT_ID", "MASSE_ID in (select MASSE_ID from MASSE_EAU where BASSIN_ID=".$_SESSION["idBassin"].")"), "lg" => 20,"index" => 0 ,"mode" => 2),
		                    array("name" => "SUPPORT", "oblig" => 1, "type" => $this->oQuery->GetDs_champsImport("SUPPORT","SUPPORT_CODE", "SUPPORT_ID", "RESEAU_ID in (select RESEAU_ID from RESEAU where BASSIN_ID=".$_SESSION["idBassin"].")"), "lg" => 20,"index" => 0 ,"mode" => 2),
		                    array("name" => "PARAM", "oblig" => 1, "type" => $this->oQuery->GetDs_champsImport("PARAMETRE","PARAMETRE_CODE", "PARAMETRE_ID", "SUPPORT_ID in (select SUPPORT_ID from SUPPORT supp, RESEAU reso where reso.RESEAU_ID = supp.RESEAU_ID and BASSIN_ID=".$_SESSION["idBassin"].")"), "lg" => 20,"index" => 0 ,"mode" => 2),
		                    array("name" => "OPE_TERRAIN", "oblig" => 0, "type" =>  $this->oQuery->GetDs_champsImport("OPERATEUR","OPERATEUR_CODE", "OPERATEUR_ID", "BASSIN_ID=".$_SESSION["idBassin"]), "lg" => 20,"index" => 0 ,"mode"=> 2),
		                    array("name" => "OPE_LABO", "oblig" => 0, "type" =>  $this->oQuery->GetDs_champsImport("OPERATEUR","OPERATEUR_CODE", "OPERATEUR_ID", "BASSIN_ID=".$_SESSION["idBassin"]),"lg" => 20,"index" => 0 ,"mode" => 2),
		                    array("name" => "ANNEE", "oblig" => 0, "type" => "string","lg" => 20,"index" => 0 ,"mode" => 0),
		                    array("name" => "FREQUENCE", "oblig" => 0, "type" => "string","lg" => 100,"index" => 0 ,"mode" => 0),
		                    array("name" => "PERIODE", "oblig" => 0, "type" => "string","lg" => 100,"index" => 0 ,"mode" => 0),
		                    array("name" => "FREQUENCE_PLAN", "oblig" => 0, "type" => "string","lg" => 50,"index" => 0 ,"mode" => 0)		                    
		                    );
		$bTes = true;
		$intTes = 1;
		$tabSup = array("RESEAU", "POINT", "SUPPORT", "PARAM");
		$tabAjout = array ("RESEAU", "POINT", "SUPPORT", "PARAM");
		$tabModif = array("RESEAU", "POINT", "SUPPORT", "PARAM");
		$objErreur = new AlkImportErreur($strFichier_Erreur);
		$objWarning = new AlkImportWarning($strFichier_Warning);
		$objImport = new AlkImportAtlas($strFichier_Import, $tabImport, $tabSup, $tabAjout, $tabModif, 
		                               $objErreur,$objWarning, $this->oQuery, $this->oQueryAction );
		
		$iTes = 0;
		$intTest = $objImport->parseFile($intSeparateur);
		if( $intTest == 0 ) {
			$intTes = $objImport->parseLines($intSeparateur);
		  if( $intTes == 0 ) {
		    $iTes = $objImport->integrate($intSeparateur);
		  }
		}
				
		$strHtml = $this->_getFrameTitleSheet("Import de donn�es de l'atlas").		
		  "<table width='606' border='0' cellspacing='0' cellpadding='4' align='center'>".
		  "<tr>".
		  "<td width=60>&nbsp;</td>".
		  "<td align='center'>";
		
		if( $intTes == 0 && $iTes == 0 ) {
		  $strHtml .= "<div class='divContenuTexte'>Importation effectu� avec succ�s.</div>";
		  $this->traitementMajGml();
		  $this->traitementMajMe();      
		} else {
		  if( $intTes > 0 ) {
		    $strHtml .= "<div class='divContenuTexte'>Importation non effectu�e pour cause de fichier mal format�.</div>";
		  }
		  if( $iTes > 0 ) {
		    $strHtml .= "<div class='divContenuTexte'>Importation effectu�e avec des alertes ou des erreurs.</div>";   
		    $this->traitementMajGml();
		    $this->traitementMajMe(); 
		  }
		  $bFileAlert = file_exists($strFichier_Warning) && is_file($strFichier_Warning);
		  $bFileError = file_exists($strFichier_Erreur) && is_file($strFichier_Erreur);
		
		  if( $bFileAlert || $bFileError ) {
		    $strHtml .= "<div class='divContenuTexte'><font color=red>Attention</font> : Vous pouvez consulter ";
		    if( $bFileAlert ) {
		      $strHtml .= "<a class='aContenuLien' href=\"".$urlFileWarning."\" target='_blank'>les alertes</a>";
		      if( $bFileError )
		        $strHtml .= " et ";
		    }
		    if( $bFileError ) {
		       $strHtml .= "<a class='aContenuLien' href=\"".$urlFileError."\" target='_blank'>les erreurs</a>";
		    }
		    $strHtml .= ".";
		  }
		  
		  $strHtml .= "<br><a class='aContenuLien' href=\"01_page_form.php?".$strParam."\">R�essayer un nouvel import</a>";
		}
		return $strHtml;
  }
  
  function traitementMajGml(){
    include_once("classes/alkappliatlas_qualite.class.php");
    $oAppli = new AlkAppliAtlas_qualite($appli_id=0, $agent_id=0, $iSheet=0, $iSSheet=0, $iModeSSheet=0);
    return $oAppli->traitementMajGml("reso");
  }
  
  function traitementMajMe($tabMe=array()){
    
    //Mise � jour de la propri�t� MASSE_B_SUIVI des masses d'eau pass�s en param�tre
    //(si elle contient des points �tudi�s dans le cadre de contr�le op�rationnel, contr�le d'enquete; controle de surveillance
    //alors la valeur est oui
    $this->oQueryAction->updateProgSurvMe($_SESSION["idBassin"]);     		  	  			
  }  
}

?>