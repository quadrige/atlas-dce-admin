<?php
include_once('queryannu_action.class.php');

/**
 * @Class queryAnnuMysql
 *
 * @brief Ensemble des requetes s�lections li�es � l'application Actualites sp�cialis�es Mysql
 *         Les requetes standards se trouvent dans la classe m�re
 */
class queryAnnuActionMySql extends queryAnnuAction
{
  /**
   * @brief Constructeur
   *
   * @param dbConn    classe de connexion � la base
   * @param tabLangue tableau des langues utilis�es
   */
  function queryAnnuActionMySql(&$dbConn, $tabLangue=array()) 
  {
    parent::queryAnnuAction($dbConn, $tabLangue);
  }   
}
?>