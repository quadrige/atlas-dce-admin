<?php
include_once("../../classes/appli/alkimport.class.php");

/**
 * @class AlkImportAnnu
 * @copyright Alkante
 * @brief Classe d'import pour l'annuaire
 */
class AlkImportAtlasMe extends AlkImport
{
	
	
	/**
   * @brief Constructeur de la classe Import : initialisation des attributs de Import.
   *
   * @param strFichier_Import Emplacement  du fichier d'import
   * @param tabImport  Tableau contenant les propriet�s des  colonnes
   * @param tabSup  Tableau contenant les colonnes obligatoires pour la suppression
   * @param tabAjoutt  Tableau contenant les colonnes obligatoires pour l'ajouts
   * @param objErreur  Nom de l'objet  ImportErreur
   */
	function AlkImportAtlasMe($strFichier_Import,  $tabAlkImportCol, $tabSup, $tabAjout, 
                         $tabModif, &$obj_Erreur,&$objWarning, &$oQuery, &$oQueryAction )
	{
		parent::AlkImport($strFichier_Import,  $tabAlkImportCol, $tabSup, $tabAjout, 
                      $tabModif, $obj_Erreur,$objWarning, $oQuery, $oQueryAction, $oQuery );
                                      
	}
	
  /**
   * @brief Fonction qui rend un tableaux de champs valeurs.
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return tabChampsData  un tableau de champs valeurs
   */
	function getChampsData($tabLigne, $tabCopy, $tabLigneRef, $bUpdate=false)
	{	  	      
    
    if( $bUpdate == false ) {
      // mode ajout
      $tabChampsData = array("MASSE_CODE"    => "-1", "MASSE_NOM" => "", "MASSE_TYPE" => "", /*"MASSE_TYPE_SUIVI"=>"",*/ "MASSE_B_SUIVI"  => "",
      											 "MASSE_B_RISQUE"  => "", "MASSE_DESC_ID"  => "", "BASSIN_ID"=>$_SESSION["idBassin"], "REGION_ID"=>"",
      											 "MASSE_B_ATTEINTE_OBJECTIF" => "", "MASSE_OBJECTIF"  => "",                                
                             "MASSE_REPORT_OBJECTIF" => "",   "MASSE_B_FORT_MODIF" => "",
                             "MASSE_DATE_FORT_MODIF" => "", "MASSE_DEPT" => "", "DEPT_ID" => array(), "MASSE_COMMENTAIRE" => "", "MASSE_B_ATTEINTE_OBJ" =>"");
    } else {
      // mode modif
      $tabChampsData = array("MASSE_CODE"    => "-2", "MASSE_NOM" => "-2", "MASSE_TYPE" => "-2", /*"MASSE_TYPE_SUIVI"=>"-2",*/ "MASSE_B_SUIVI"  => "-2",
      											 "MASSE_B_RISQUE"  => "-2", "MASSE_DESC_ID"  => "-2", "REGION_ID"=>"-2",
      											 "MASSE_B_ATTEINTE_OBJECTIF" => "-2", "MASSE_OBJECTIF"  => "-2",                                
                             "MASSE_REPORT_OBJECTIF" => "-2",   "MASSE_B_FORT_MODIF" => "-2",
                             "MASSE_DATE_FORT_MODIF" => "-2", "MASSE_DEPT" => "-2", "DEPT_ID" => array(), "MASSE_COMMENTAIRE" => "", "MASSE_B_ATTEINTE_OBJ" =>"-2");
    }
		$tabChamps = array();
        
		if (in_array("ME",$tabLigneRef) && ($tabLigne[$tabCopy["ME"]] != "")) 
      $tabChampsData["MASSE_CODE"] = $tabLigne[$tabCopy["ME"]];

		if (in_array("NOM",$tabLigneRef) && ($tabLigne[$tabCopy["NOM"]] != "")) 
      $tabChampsData["MASSE_NOM"] = $tabLigne[$tabCopy["NOM"]];		    
						
    if (in_array("NATURE",$tabLigneRef) && ($tabLigne[$tabCopy["NATURE"]] != "")) 
      $tabChampsData["MASSE_TYPE"] = $tabLigne[$tabCopy["NATURE"]];

     if (in_array("RISQUE",$tabLigneRef)) 
      $tabChampsData["MASSE_B_RISQUE"] = ($tabLigne[$tabCopy["RISQUE"]] == "" ? "" : ($tabLigne[$tabCopy["RISQUE"]]=="OUI" ? 1 : ($tabLigne[$tabCopy["RISQUE"]]=="DOUTE" ? 2 : 0)));
        
    if (in_array("TYPE",$tabLigneRef) && ($tabLigne[$tabCopy["TYPE"]] != "")) 
      $tabChampsData["MASSE_DESC_ID"] = $tabLigne[$tabCopy["TYPE"]];
      
    if (in_array("REGION",$tabLigneRef) && ($tabLigne[$tabCopy["REGION"]] != "")) 
      $tabChampsData["REGION_ID"] = $tabLigne[$tabCopy["REGION"]];
          
    if (in_array("ATTEINTE_OBJECTIF",$tabLigneRef)) 
      $tabChampsData["MASSE_B_ATTEINTE_OBJECTIF"] = ($tabLigne[$tabCopy["ATTEINTE_OBJECTIF"]] == "" ? "" : ($tabLigne[$tabCopy["ATTEINTE_OBJECTIF"]]=="OUI" ? 1 : 0));
    
    if (in_array("OBJECTIF",$tabLigneRef)) 
      $tabChampsData["MASSE_OBJECTIF"] = $tabLigne[$tabCopy["OBJECTIF"]];
    
    if (in_array("REPORT",$tabLigneRef)) 
      $tabChampsData["MASSE_REPORT_OBJECTIF"] = $tabLigne[$tabCopy["REPORT"]];
      
    if (in_array("FORTEMENT_MODIFIE",$tabLigneRef) && ($tabLigne[$tabCopy["FORTEMENT_MODIFIE"]] != "")) 
      $tabChampsData["MASSE_B_FORT_MODIF"] = ($tabLigne[$tabCopy["FORTEMENT_MODIFIE"]]=="OUI" ? 1 : 0);

    if (in_array("DATE_PASSAGE",$tabLigneRef)) 
      $tabChampsData["MASSE_DATE_FORT_MODIF"] = $tabLigne[$tabCopy["DATE_PASSAGE"]];
       
    if (in_array("DEPT",$tabLigneRef) && ($tabLigne[$tabCopy["DEPT"]] != "")) 
      $tabChampsData["MASSE_DEPT"] = $tabLigne[$tabCopy["DEPT"]];

    if (in_array("COMMENTAIRE",$tabLigneRef)) 
      $tabChampsData["MASSE_COMMENTAIRE"] = $tabLigne[$tabCopy["COMMENTAIRE"]];

    if (in_array("ATTEINTE_OBJECTIF_DESCRIPTION",$tabLigneRef)) 
      $tabChampsData["MASSE_B_ATTEINTE_OBJ"] = $tabLigne[$tabCopy["ATTEINTE_OBJECTIF_DESCRIPTION"]];
                  
		return 	$tabChamps = array($tabChampsData);
	}
	
  /**
   * @brief Fonction insertion de la ligne courante .
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return vrai si ibsertion faux sinon
   * 
   * DMI : 06/10/2010 : modification en cas d'existence pas de suppression mais modification cause suppression intempestive des textes explicatifs d�j� saisis
   */
	function insert($tabLigne, $tabCopy, $tabLigneRef, $t)
	{
		$tabChamps = $this->getChampsData($tabLigne, $tabCopy, $tabLigneRef);

		$tabChampsData = $tabChamps[0];

    $iWarning = 0;
		
    $tabErr = $this->_VerificationCode($tabChampsData, $t);
		if ($tabErr[0])
		  return 2;
		if ($tabErr[1])
		  $iWarning = 1;
    	
    $dsMe = $this->oQuery->getDs_MasseEauById("", $tabChampsData["MASSE_CODE"]);		
		if( $drMe = $dsMe->getRowIter() ) {
		  if( $drMe->getValueName("BASSIN_ID") == $_SESSION["idBassin"])		    		 
			  return $this->update($tabLigne, $tabCopy, $tabLigneRef, $t);
			else {
			  $strMes = "Erreur : Ligne ".$t.", impossible de cr�er la masse d'eau ".$tabChampsData["MASSE_CODE"].", une masse d'eau existe d�j� avec ce code. Ligne rejet�e.";
        $this->obj_Erreur->erreur(7, $strMes);      
        return 2;
			}			
		} 
		
		//ajout 
		$tabValue=array();
		$tabValue["MASSE_CODE"] = array( 0, $tabChampsData["MASSE_CODE"]);
		$tabValue["MASSE_NOM"] = array( 0, $tabChampsData["MASSE_NOM"]);
		$tabValue["MASSE_TYPE"] = array( 0, $tabChampsData["MASSE_TYPE"]);
		//$tabValue["MASSE_TYPE_SUIVI"] = array( 0, $tabChampsData["MASSE_TYPE_SUIVI"]);
		$tabValue["MASSE_DESC_ID"] = array( 1, $tabChampsData["MASSE_DESC_ID"]);
		$tabValue["BASSIN_ID"] = array( 1, $tabChampsData["BASSIN_ID"]);
		$tabValue["REGION_ID"] = array( 1, $tabChampsData["REGION_ID"]);
		$tabValue["MASSE_B_RISQUE"] = array( 1, $tabChampsData["MASSE_B_RISQUE"]);
		$tabValue["MASSE_B_ATTEINTE_OBJECTIF"] = array( 1, $tabChampsData["MASSE_B_ATTEINTE_OBJECTIF"]);		
		$tabValue["MASSE_OBJECTIF"] = array( 0, $tabChampsData["MASSE_OBJECTIF"]);
		$tabValue["MASSE_REPORT_OBJECTIF"] = array( 0, $tabChampsData["MASSE_REPORT_OBJECTIF"]);
		$tabValue["MASSE_B_FORT_MODIF"] = array( 1, $tabChampsData["MASSE_B_FORT_MODIF"]);
		$tabValue["MASSE_DATE_FORT_MODIF"] = array( 2, $tabChampsData["MASSE_DATE_FORT_MODIF"]);		
    $tabValue["MASSE_COMMENTAIRE"] = array( 0, $tabChampsData["MASSE_COMMENTAIRE"]);  
    $tabValue["MASSE_B_ATTEINTE_OBJ"] = array( 0, $tabChampsData["MASSE_B_ATTEINTE_OBJ"]);  
    $strFieldKey = "MASSE_ID";
	  $strTable = "MASSE_EAU";
     
    $masse_id = $this->oQueryAction->add_ficheDico($strTable, $strFieldKey, $tabValue);
    		
    //Association des d�partements
	  $bRes = $this->oQueryAction->del_ficheDico("MASSE_EAU_DEPT", "MASSE_ID", $masse_id);		
		$tabValue = array();
		$tabValue["MASSE_ID"]=array( 1, $masse_id);		
		foreach($tabChampsData["DEPT_ID"] as $dept_id){
			$tabValue["DEPT_ID"] = array( 1, $dept_id);	
			$strSql = $this->oQueryAction->_getPartInsertSql($tabValue);
  		$strSql = "insert into MASSE_EAU_DEPT ".$strSql;
  		$this->oQueryAction->dbConn->executeSql($strSql);
		}
  
    if( $masse_id < 1 ) {
      $strMes = "Erreur : Ligne ".$t.", impossible d'ajouter les donn�es. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);      
      return 2;
    }
    				
		if( $iWarning != 0 ) {
			return 1;
		} else {
			return 0;
		}
	}
	
	/**
   * @brief Fonction de modification de la ligne courante dans la base .
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return vrai si ibsertion faux sinon
   */
	function update($tabLigne, $tabCopy, $tabLigneRef,$t)
	{
		$tabChamps = $this->getChampsData($tabLigne, $tabCopy, $tabLigneRef, true);
		$tabChampsData = $tabChamps[0];
    $iWarning = 0;
		$tabErr = $this->_VerificationCode($tabChampsData, $t, true);
		if ($tabErr[0])
		  return 2;
		if ($tabErr[1])
		  $iWarning = 1;
    
    // charge la fiche
		$dsMe = $this->oQuery->getDs_MasseEauById("", $tabChampsData["MASSE_CODE"]);		
		if( $drMe = $dsMe->getRowIter() ) {
			$masse_id = $drMe->getValueName("ID");
			if( $drMe->getValueName("BASSIN_ID") != $_SESSION["idBassin"]){		    		 			  
			  $strMes = "Erreur : Ligne ".$t.", impossible de modifier la masse d'eau ".$tabChampsData["MASSE_CODE"].", elle n'appartient pas au bassin courant. Ligne rejet�e.";
        $this->obj_Erreur->erreur(7, $strMes);      
        return 2;
			}			
		} else {      
      $strMes = "Erreur : Ligne ".$t.", fiche non trouv�e pour modification. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);
      return 2;
    }
    
    $tabValue=array();		
		if ($tabChampsData["MASSE_NOM"] != "-2") $tabValue["MASSE_NOM"] = array( 0, $tabChampsData["MASSE_NOM"]);
		if ($tabChampsData["MASSE_TYPE"] != "-2") $tabValue["MASSE_TYPE"] = array( 0, $tabChampsData["MASSE_TYPE"]);
		/*if ($tabChampsData["MASSE_TYPE_SUIVI"] != "-2") $tabValue["MASSE_TYPE_SUIVI"] = array( 0, $tabChampsData["MASSE_TYPE_SUIVI"]);*/
		if ($tabChampsData["MASSE_DESC_ID"] != "-2") $tabValue["MASSE_DESC_ID"] = array( 1, $tabChampsData["MASSE_DESC_ID"]);
		if ($tabChampsData["REGION_ID"] != "-2") $tabValue["REGION_ID"] = array( 1, $tabChampsData["REGION_ID"]);
		if ($tabChampsData["MASSE_B_ATTEINTE_OBJECTIF"] != "-2") $tabValue["MASSE_B_ATTEINTE_OBJECTIF"] = array( 1, $tabChampsData["MASSE_B_ATTEINTE_OBJECTIF"]);
		if ($tabChampsData["MASSE_B_RISQUE"] != "-2") $tabValue["MASSE_B_RISQUE"] = array( 1, $tabChampsData["MASSE_B_RISQUE"]);
		if ($tabChampsData["MASSE_OBJECTIF"] != "-2") $tabValue["MASSE_OBJECTIF"] = array( 0, $tabChampsData["MASSE_OBJECTIF"]);
		if ($tabChampsData["MASSE_REPORT_OBJECTIF"] != "-2") $tabValue["MASSE_REPORT_OBJECTIF"] = array( 0, $tabChampsData["MASSE_REPORT_OBJECTIF"]);
		if ($tabChampsData["MASSE_B_FORT_MODIF"] != "-2") $tabValue["MASSE_B_FORT_MODIF"] = array( 1, $tabChampsData["MASSE_B_FORT_MODIF"]);
		if ($tabChampsData["MASSE_DATE_FORT_MODIF"] != "-2") $tabValue["MASSE_DATE_FORT_MODIF"] = array( 2, $tabChampsData["MASSE_DATE_FORT_MODIF"]);				
    if ($tabChampsData["MASSE_COMMENTAIRE"] != "-2") $tabValue["MASSE_COMMENTAIRE"] = array( 0, $tabChampsData["MASSE_COMMENTAIRE"]);
    if ($tabChampsData["MASSE_B_ATTEINTE_OBJ"] != "-2") $tabValue["MASSE_B_ATTEINTE_OBJ"] = array( 0, $tabChampsData["MASSE_B_ATTEINTE_OBJ"]);
    $strFieldKey = "MASSE_ID";
	  $strTable = "MASSE_EAU";
    
		$this->oQueryAction->update_ficheDico($strTable, $strFieldKey, $masse_id, $tabValue);    
	  
	  //Association des d�partements
	  $bRes = $this->oQueryAction->del_ficheDico("MASSE_EAU_DEPT", "MASSE_ID", $masse_id);		
		$tabValue = array();
		$tabValue["MASSE_ID"]=array( 1, $masse_id);		
		foreach($tabChampsData["DEPT_ID"] as $dept_id){
			$tabValue["DEPT_ID"] = array( 1, $dept_id);	
			$strSql = $this->oQueryAction->_getPartInsertSql($tabValue);
  		$strSql = "insert into MASSE_EAU_DEPT ".$strSql;
  		$this->oQueryAction->dbConn->executeSql($strSql);
		}
		
   if( $iWarning != 0 ) {
			return 2;
		} else {
			return 0;
		}
	}
	
	/**
   * @brief Fonction de suppresion de la ligne courante dans la base .
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return vrai si insertion faux sinon
   */
	function delete($tabLigne, $tabCopy, $tabLigneRef,$t)
	{
		$tabChamps = $this->getChampsData($tabLigne, $tabCopy, $tabLigneRef);

		$tabChampsData = $tabChamps[0];		
		
		$strMes = "Erreur : Ligne ".$t.", impossible de supprimer la fiche. Ligne rejet�e.";
    $this->obj_Erreur->erreur(7, $strMes);      
    return 2;
		
	}
	
	/**
   * @brief retourne la reference d'un champ..
   *
   * @param  champ  l'intitule du champs
   * @param  tabType tableau contenant le champs 
   * @return tabRes[0] la reference recherche dans la tabType et celle de la premiere case.
   */
	function getIdChamp($champ,$tabType)
	{
		$tabRes = array_keys($tabType, $champ);
    if( count($tabRes) > 0 ) 
      return $tabRes[0];
    return -1;
	}
	
	function _VerificationCode(&$tabChampsData, $t, $bUpdate=false){
		$bErr = false;
		$bWarning = false;
		
	  // v�rifie si le code masse d'eau est existant si modif		
		if ($bUpdate && $tabChampsData["MASSE_CODE"] != "-1" && $tabChampsData["MASSE_CODE"] != "-2") {
      $tabId = $this->oQuery->GetDs_champsImport("MASSE_EAU","MASSE_CODE", "MASSE_ID", "BASSIN_ID=".$_SESSION["idBassin"]." and MASSE_CODE = '".$tabChampsData["MASSE_CODE"]."'");
      if(count($tabId) != 1 ) {
        $strMes = "Erreur : Ligne ".$t.", code masse d'eau inexistant. Ligne rejet�e.";
        $this->obj_Erreur->erreur(7, $strMes);
        return array(true, $bWarning);
      }
		}      				
		
		// v�rifie si le code description existe		
		if ($tabChampsData["MASSE_DESC_ID"] != "" && $tabChampsData["MASSE_DESC_ID"] != "-2") {
      $tabId = $this->oQuery->GetDs_champsImport("MASSE_EAU_DESCRIPTION","MASSE_DESC_CODE", "MASSE_DESC_ID", "MASSE_DESC_CODE = '".$tabChampsData["MASSE_DESC_ID"]."'");
      if(count($tabId) != 1 ) {
        $strMes = "Erreur : Ligne ".$t.", code type masse d'eau inexistant. Ligne rejet�e.";
        $this->obj_Erreur->erreur(7, $strMes);
        return array(true, $bWarning);
      } else {
        $key = array_keys($tabId);
      	$tabChampsData["MASSE_DESC_ID"] = $key[0];
      }
		}      

		// v�rifie si la r�gion existe		
		if ($tabChampsData["REGION_ID"] != "" && $tabChampsData["REGION_ID"] != "-2") {
      $tabId = $this->oQuery->GetDs_champsImport("REGION_MARINE","REGION_LIBELLE", "REGION_ID", "REGION_LIBELLE = '".$tabChampsData["REGION_ID"]."'");
      if(count($tabId) != 1 ) {
        $strMes = "Erreur : Ligne ".$t.", r�gion marine inexistante. Ligne rejet�e.";
        $this->obj_Erreur->erreur(7, $strMes);
        return array(true, $bWarning);
      } else {
        $key = array_keys($tabId);
      	$tabChampsData["REGION_ID"] = $key[0];
      }
		}            
	  		
		// v�rifie si les d�partements existent
		if ($tabChampsData["MASSE_DEPT"] != "" && $tabChampsData["MASSE_DEPT"] != "-2") {
		  $tabDept = explode(",", $tabChampsData["MASSE_DEPT"]);
		  foreach($tabDept as $codeDept){
  		  $tabId = $this->oQuery->GetDs_champsImport("DEPARTEMENT","DEPT_CODE", "DEPT_ID", "DEPT_CODE = '".$codeDept."' and DEPT_VISIBLE = 1");
        if(count($tabId) == 1 ) {          
          $key = array_keys($tabId);
        	$tabChampsData["DEPT_ID"][] = $key[0];
        }    
		  }  
		  if (count($tabChampsData["DEPT_ID"]) <= 0){
		    $strMes = "Warning : Ligne ".$t.", aucun d�partement n'a pu �tre associ� � la masse d'eau.";
        $this->obj_Warning->erreur(7, $strMes);
        $bWarning = true;
		  }  
		}

		  // v�rifie si le code atteinte objectif existe		
		if ($tabChampsData["MASSE_B_ATTEINTE_OBJ"] != "" && $tabChampsData["MASSE_B_ATTEINTE_OBJ"] != "-2") {
      $tabId = $this->oQuery->GetDs_champsImport("ATTEINTE_OBJECTIF_DESCRIPTION","ATTEINTE_OBJECTIF_CODE", "ATTEINTE_OBJECTIF_ID", "ATTEINTE_OBJECTIF_CODE = '".$tabChampsData["MASSE_B_ATTEINTE_OBJ"]."'");
      if(count($tabId) != 1 ) {
        $strMes = "Erreur : Ligne ".$t.", code atteinte objectif inexistant. Ligne rejet�e.";
        $this->obj_Erreur->erreur(7, $strMes);
        return array(true, $bWarning);
      } else {
        $key = array_keys($tabId);
      	$tabChampsData["MASSE_B_ATTEINTE_OBJ"] = $key[0];
      }
		}      	         	  
	  
		return array($bErr, $bWarning);
	}
}

?>