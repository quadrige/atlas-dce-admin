<?php
/**
 * @Class queryAtlas
 *
 * @brief  Ensemble des requetes s�lections li�es � l'application actualit�s
 *         Les requetes sp�cifiques se trouvent dans une classe h�rit�e queryAtlas_action_xxx.class.php
 */
class queryAnnu {

  var $dbConn; // Connection to the database
  var $tabLangue;
  
  /**
   * @brief Constructeur
   *
   * @param dbConn    classe de connexion � la base
   * @param tabLangue tableau des langues utilis�es
   */
  function queryAnnu($dbConn, $tabLangue) 
  {
    $this->dbConn = $dbConn;
    $this->tabLangue = $tabLangue;
  }
  
  /**
   * @brief Affecte la connexion
   *
   * @param dbConn classe de connexion � la base
   *
   * @return Retourne true
   */
  function setConnection($dbConn) 
  {
    $this->dbConn = &$dbConn ;
    return true ;
  }
    
  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  /**
   * Retourne le dataset de l'agent trouv� par son login et son mot de passe
   * sit_menu/login_verif.php
   *
   * @param $strLogin : login saisi
   * @param $strPwd : mot de passe saisi
   */
  function GetDs_ficheAgentByLoginPwd($strLogin, $strPwd, $strCondAgentValide="=1")
  {
    $strSql = "select USER_ID, USER_NOM, USER_PRENOM, USER_LOGIN, USER_ADMIN, USER_ADMIN_ANNU, USER_ADMIN_ALL_BASSIN ".//,BASSIN_ID
      " from ANNU_USER".
      " where USER_VALIDE".$strCondAgentValide." and ".
      $this->dbConn->GetLowerCase("USER_LOGIN")."=".
      $this->dbConn->GetLowerCase("'".$this->dbConn->analyseSQL($strLogin)."'").
			" and USER_PWD='".$this->dbConn->analyseSQL($strPwd)."'";

    return $this->dbConn->InitDataSet($strSql);
  } 

  
  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  /**
   * Retourne le dataset de l'agent trouv� par son login et son mot de passe
   * sit_menu/login_verif.php
   *
   * @param $strLogin : login saisi
   * @param $strPwd : mot de passe saisi
   */
  function GetDs_listeUser( $idFirst=0, $idLast=-1)
  {
    $strSql = "select * ".
      " from ANNU_USER "
      ;

    return $this->dbConn->InitDataSet($strSql, $idFirst, $idLast);
  }   

  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  /**
   * Retourne le dataset de l'agent trouv� par son login et son mot de passe
   * sit_menu/login_verif.php
   *
   * @param $strLogin : login saisi
   * @param $strPwd : mot de passe saisi
   */
  function GetDs_UserById($idUser)
  {
    $strSql = "select * ".
      " from ANNU_USER ".
      " where USER_ID = ".$idUser;

    return $this->dbConn->InitDataSet($strSql);
  }

  /**
   * @brief Retourne la liste d'une table nomm�e pour un combo
   *
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getTableForCombo($strTable, $strLibelle, $idFirst=0, $idLast=-1)
  {    
    $strSql = "select ".$strLibelle."_NOM as nom, ".
    	$strLibelle."_ID ".
      " from ".$strTable.      
      " order by nom";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
 
 
   /**
   * @brief Retourne la liste d'une table nomm�e pour un combo appel effectu� apr un admin d'un bassin
   *
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   * @param $idBassin  indice d'un bassin
   * @return Retourne un dataset
   */
  function getTableForComboByBassinId($strTable, $strLibelle, $idFirst=0, $idLast=-1, $idBassin)
  {    
    $strSql = "select ".$strLibelle."_NOM as nom, ".
      $strLibelle."_ID ".
      " from ".$strTable. 
      " where BASSIN_ID=".$idBassin.     
      " order by nom";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }   
  
     /**
   * @brief Retourne la liste des bassins disponibles par user_id
   * @return Retourne un dataset
   */
  function getListBassinByUserForCombo($user_id)
  {    
    $strSql = "select BASSIN_NOM as nom, ".
      "BH.BASSIN_ID ".
      " from BASSIN_HYDROGRAPHIQUE BH ".      
      " LEFT JOIN ANNU_USER_BASSIN AUB ON BH.BASSIN_ID = AUB.BASSIN_ID".
      " WHERE USER_ID = ".$user_id.
      " order by nom";
    return $this->dbConn->initDataset( $strSql) ;
  }   
}
?>