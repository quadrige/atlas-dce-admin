<?php
/**
 * @Class queryAtlas
 *
 * @brief  Ensemble des requetes s�lections li�es � l'application actualit�s
 *         Les requetes sp�cifiques se trouvent dans une classe h�rit�e queryAtlas_action_xxx.class.php
 */

require_once(ALK_SIALKE_PATH."classes/appli/alkquery.class.php");

class queryAtlas extends AlkQuery{

  var $tabLangue;
  
  /**
   * @brief Constructeur
   *
   * @param dbConn    classe de connexion � la base
   * @param tabLangue tableau des langues utilis�es
   */
  function queryAtlas(&$dbConn, $tabLangue) 
  {
    parent::AlkQuery($dbConn);
    $this->tabLangue = $tabLangue;
  }
  
  /**
   * @brief Affecte la connexion
   *
   * @param dbConn classe de connexion � la base
   *
   * @return Retourne true
   */
  function setConnection($dbConn) 
  {
    $this->dbConn = &$dbConn ;
    return true ;
  }
    
  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getReseauForCombo($bCode=true, $idFirst=0, $idLast=-1, $bassin_id="", $session_id=-1, $idMasseEau="") 
  { 
    $strWhere = " where 1=1".
                ($bassin_id!="" ? " and BASSIN_ID=".$bassin_id : "").
                ($session_id != -1 ? " and r.SESSION_ID=".$session_id : "");

    $strFrom = "";                
    if ($idMasseEau != ""){
      $strFrom = " INNER JOIN RESEAU_POINT_PARAM".($session_id != -1 ? "_SESSION" : "")." rpp on rpp.RESEAU_ID = r.RESEAU_ID".($session_id != -1 ? " and rpp.SESSION_ID=r.SESSION_ID" : "");
                 " INNER JOIN POINT".($session_id != -1 ? "_SESSION" : "")." point on point.POINT_ID = rpp.POINT_ID ".($session_id != -1 ? " and point.SESSION_ID=rpp.SESSION_ID" : "")." and point.MASSE_ID = ".$idMasseEau;
    }            
                
    $strSql = "select distinct r.RESEAU_NOM, " .
    	($bCode ? "r.RESEAU_CODE ," : "").
    	" r.RESEAU_ID, r.RESEAU_SYMBOLE , r.RESEAU_COULEUR, r.RESEAU_SYMBOLE_TAILLE, rg.GROUPE_NOM, r.RESEAU_IMG_SYMB ".
      " from RESEAU".($session_id != -1 ? "_SESSION" : "")." r".
    	$strFrom.      
    	" LEFT JOIN RESEAU_GROUPE rg on r.GROUPE_ID=rg.GROUPE_ID".      
    	$strWhere.
      " order by rg.GROUPE_ID, RESEAU_RANG, RESEAU_CODE, RESEAU_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
  
  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getMasseForCombo($reseau_id=-1, $idFirst=0, $idLast=-1, $bassin_id="", $dept_id="-1") 
  {    
    $strSql = "select distinct ".$this->dbConn->GetConcat("MASSE_CODE", "' - '", "MASSE_NOM")." as MASSE_NOM_LG, me.MASSE_ID".
      " from MASSE_EAU me".
    	" left join MASSE_EAU_DEPT dept on dept.MASSE_ID = me.MASSE_ID".
    	" where 1=1".   
    	($bassin_id != "" ? " and me.BASSIN_ID=".$bassin_id : "").
    	($dept_id != "-1" ? " and dept.DEPT_ID=".$dept_id : "").
      " order by MASSE_NOM_LG";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
  
    
  /**
   * @brief Retourne la liste des dept en fonction d'un bassin_id pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getDeptForCombo($idFirst=0, $idLast=-1, $bassin_id="", $strSqlWhere="") 
  {    
    $strSql = "select distinct dep.DEPT_NOM as nom, dep.DEPT_ID ".
      " from MASSE_EAU me " .
      " left join MASSE_EAU_DEPT dept on dept.MASSE_ID = me.MASSE_ID".
      " left join DEPARTEMENT dep on dep.DEPT_ID = dept.DEPT_ID".
      " where 1=1".   
      ($strSqlWhere != "" ? " and ".$strSqlWhere : "").      
      ($bassin_id != "" ? " and me.BASSIN_ID=".$bassin_id : "").
      " order by nom";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  
  }
  
  
  
  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getPointForCombo($reseau_id=-1, $masse_id=-1, $bCode=true, $idFirst=0, $idLast=-1, $bassin_id="") 
  {    
    $strSql = "select ".$this->dbConn->GetConcat("POINT_CODE", "' - '", "POINT_NOM")." as POINT_NOM_LG, " .
    	($bCode ? "POINT_CODE, POINT_ID" : "POINT_ID, POINT_CODE").
      " from POINT as p".
    	" inner join MASSE_EAU as me on p.MASSE_ID=me.MASSE_ID".
    	($bassin_id != "" ? " and me.BASSIN_ID = ".$bassin_id : "").          	
      ($masse_id != -1 ? " where p.MASSE_ID = ".$masse_id : "").      
      " order by POINT_NOM_LG";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }    
  
  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getSupportForCombo($reseau_id=-1, $idFirst=0, $idLast=-1, $bassin_id="") 
  {    
    $strSql = "select ".$this->dbConn->GetConcat("RESEAU_NOM", "' - '", "SUPPORT_NOM")." as SUPPORT_NOM_LG, SUPPORT_ID".
      " from SUPPORT, RESEAU". 
      " where SUPPORT.RESEAU_ID =  RESEAU.RESEAU_ID". 
      ($bassin_id != "" ? " and BASSIN_ID = ".$bassin_id : "").  
      " order by SUPPORT_NOM_LG";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
  
    /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getOperateurForCombo($idFirst=0, $idLast=-1) 
  {    
    $strSql = "select OPERATEUR_NOM, OPERATEUR_ID".
      " from OPERATEUR".        
      " order by OPERATEUR_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
  
      /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getParamForCombo($idReseau, $idPoint, $idFirst=0, $idLast=-1) 
  {    
    $strSql = "select ".$this->dbConn->GetConcat("SUPPORT_NOM", "' - '", "PARAMETRE_NOM")." as PARAMETRE, PARAMETRE_ID ID from PARAMETRE par".
    					" left join SUPPORT s on par.SUPPORT_ID = s.SUPPORT_ID".
    					" left join RESEAU r on r.RESEAU_ID = s.RESEAU_ID".
    					" where r.RESEAU_ID = ".$idReseau.
    					" and PARAMETRE_ID not in (select PARAMETRE_ID from RESEAU_POINT_PARAM where RESEAU_ID = ".$idReseau." and POINT_ID = ".$idPoint.")".
    					" order by s.SUPPORT_NOM, PARAMETRE_NOM";    					
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
  
  /**
   * @brief Retourne la liste d'une table nomm�e pour un combo
   *
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getTableForCombo($strTable, $strPrefixe, $bLibelle=false, $bConcatCode=false, $strSqlSuppl="", $idFirst=0, $idLast=-1)
  {    
    $strSql = "select ".
    	($bConcatCode ? $this->dbConn->GetConcat($strPrefixe."_CODE", "' - '", ($bLibelle ? $strPrefixe."_LIBELLE" : $strPrefixe."_NOM"))
    								: ($bLibelle ? $strPrefixe."_LIBELLE" : $strPrefixe."_NOM"))." as nom, ".
    	$strPrefixe."_ID ".
    	($bConcatCode ? ", ".$strPrefixe."_CODE as code" : "").
      " from ".$strTable.      
    	($strSqlSuppl != "" ? " where ".$strSqlSuppl : "").
      " order by nom";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
    
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getReseauByCode($codeReseau, $bassin_id, $session_id=-1) 
  {    
    $strExtSession = ($session_id != -1 ? "_SESSION" : "");
    $strWhereSession = ($session_id != -1 ? " and r.SESSION_ID = ".$session_id : "");
         
    $strSql = "select * from RESEAU".$strExtSession." r".
      " LEFT JOIN RESEAU_GROUPE rg on r.GROUPE_ID=rg.GROUPE_ID".
      " where RESEAU_CODE = '".$this->dbConn->analyseSql($codeReseau)."'".
      " and BASSIN_ID = ".$bassin_id.
       $strWhereSession;
    return $this->dbConn->initDataset( $strSql ) ;
  } 
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getPointByCode($codePoint, $idReseau="", $bassin_id="", $session_id=-1) 
  { 
    $strExtSession = ($session_id != -1 ? "_SESSION" : "");
    
    $strFrom = "";
    $strWhere = "";
    $strSelect = "";
    if ($idReseau != ""){
        $strSelect .= ", (".$this->dbConn->GetDateFormat("DD/MM/YYYY", "RESEAU_DATE_MAJ" , false).") as RESEAU_DATE_MAJ";
        $strFrom .= " , RESEAU_POINT_PARAM".$strExtSession." rpp ";
        $strWhere .= " and rpp.POINT_ID=pt.POINT_ID and rpp.RESEAU_ID=".$idReseau.($session_id != -1 ? " and rpp.SESSION_ID = ".$session_id : "");
    }        
    if ($bassin_id != ""){
      $strWhere .= " and me.BASSIN_ID = ".$bassin_id;
    }
    
    $strSql = "select * " .$strSelect.      
      " from POINT".$strExtSession." pt ".
      ", MASSE_EAU".$strExtSession." me".
      ", BASSIN_HYDROGRAPHIQUE bas".
      $strFrom.
      " where pt.MASSE_ID = me.MASSE_ID" .
      " and me.BASSIN_ID = bas.BASSIN_ID" .
      " and POINT_CODE = '".$this->dbConn->analyseSql($codePoint)."'".            
      $strWhere.
      ($session_id != -1 ? " and pt.SESSION_ID = ".$session_id." and me.SESSION_ID = ".$session_id : "");      
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getParametreByCode($codeParam, $codeSupport, $bassin_id) 
  {          
    $strSql = "select * from PARAMETRE pa, SUPPORT su".
      " where pa.SUPPORT_ID = su.SUPPORT_ID" .
      " and pa.PARAMETRE_CODE = '".$this->dbConn->analyseSql($codeParam)."'".
      " and su.SUPPORT_CODE = '".$this->dbConn->analyseSql($codeSupport)."'".
    	" and su.RESEAU_ID in (select RESEAU_ID from RESEAU where BASSIN_ID = ".$bassin_id.")";
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getOperateurByCode($codeOperateur, $bassin_id) 
  {          
    $strSql = "select * from OPERATEUR ".
      " where OPERATEUR_CODE = '".$this->dbConn->analyseSql($codeOperateur)."'".
      " and BASSIN_ID = ".$bassin_id;
    return $this->dbConn->initDataset( $strSql ) ;
  }  
    
    /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_listeMasseEau($idFirst, $idLast, $idBassin="", $codeBassin="", $strCond="") 
  {          
  	$strWhere = " where 1=1";
  	$strWhere .= ($idBassin != "" ? " and me.BASSIN_ID=".$idBassin : "" );
  	$strWhere .= ($codeBassin != "" ? " and bas.BASSIN_CODE= '".$codeBassin."'" : "" );  	
  	$strWhere .= ($strCond != "" ? " and ".$strCond : "" );
  	
    $strSql = "select *, me.MASSE_ID ID, ".$this->dbConn->getConcat("MASSE_NOM", "' - '", "MASSE_CODE")." LIB".
    					", GROUP_CONCAT(dept.DEPT_NOM SEPARATOR ', ') DEPT".
    					", bas.BASSIN_NOM, reg.REGION_LIBELLE, ".
    					$this->dbConn->getConcat("descr.MASSE_DESC_CODE", "' - '", "descr.MASSE_DESC_LIBELLE")." DESCR".  
    					", descr.MASSE_DESC_CODE".  					
    					" from MASSE_EAU me".
    					" left join MASSE_EAU_DEPT medept on medept.MASSE_ID = me.MASSE_ID". 
    					" left join DEPARTEMENT dept on dept.DEPT_ID = medept.DEPT_ID".
    					" left join BASSIN_HYDROGRAPHIQUE bas on bas.BASSIN_ID = me.BASSIN_ID".
    					" left join REGION_MARINE reg on reg.REGION_ID = me.REGION_ID".            
              " left join MASSE_EAU_DESCRIPTION descr on descr.MASSE_DESC_ID = me.MASSE_DESC_ID".  
              ($strCond == "MASSE_B_ATTEINTE_OBJ is not null" ? " left join ATTEINTE_OBJECTIF_DESCRIPTION objdescr on objdescr.ATTEINTE_OBJECTIF_ID = me.MASSE_B_ATTEINTE_OBJ" : "").               
    					$strWhere.
    					" GROUP BY me.MASSE_ID".
    					" order by MASSE_RANG_GEO, MASSE_NOM";             
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }
  
    /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_MasseEauById($idMe, $codeMe="", $idSession=-1) 
  {          
    
   $strTable = "MASSE_EAU";
   $strWhere = "";
   if ($idSession != -1){
     $strTable = "MASSE_EAU_SESSION";
     $strWhere = " and me.SESSION_ID = ".$idSession;
   } 
   
   $strSql = "select me.MASSE_ID ID, MASSE_NOM NOM, MASSE_DOC_DESC, MASSE_DOC_FICHE, MASSE_CODE CODE, MASSE_TYPE, MASSE_TYPE_SUIVI, MASSE_B_FORT_MODIF, MASSE_B_ATTEINTE_OBJECTIF, MASSE_REPORT_OBJECTIF, MASSE_OBJECTIF, MASSE_B_ATTEINTE_OBJ,".
    					$this->dbConn->GetDateFormat("DD/MM/YYYY", "MASSE_DATE_FORT_MODIF" , false)." as MASSE_DATE_FORT_MODIF ".
              ", MASSE_B_SUIVI, MASSE_B_RISQUE, MASSE_COMMENTAIRE, MASSE_MOTIF_DEROGATION, me.BASSIN_ID, me.REGION_ID, me.MASSE_DESC_ID, MASSE_URL_FICHE, ".
              $this->dbConn->getConcat("MASSE_NOM", "' - '", "MASSE_CODE")." LIB".
    					", GROUP_CONCAT(dept.DEPT_NOM SEPARATOR ', ') DEPT".
    					", bas.BASSIN_NOM, bas.BASSIN_CODE, bas.BASSIN_ALERT, reg.REGION_LIBELLE, ".
    					$this->dbConn->getConcat("descr.MASSE_DESC_CODE", "' - '", "descr.MASSE_DESC_LIBELLE")." DESCR".
    					" from ".$strTable." me".
    					" left join MASSE_EAU_DEPT medept on medept.MASSE_ID = me.MASSE_ID". 
    					" left join DEPARTEMENT dept on dept.DEPT_ID = medept.DEPT_ID".
    					" left join BASSIN_HYDROGRAPHIQUE bas on bas.BASSIN_ID = me.BASSIN_ID".
    					" left join REGION_MARINE reg on reg.REGION_ID = me.REGION_ID".
     					" left join MASSE_EAU_DESCRIPTION descr on descr.MASSE_DESC_ID = me.MASSE_DESC_ID".     
              " where 1=1 ".	
    					$strWhere.			
    					($idMe != "" ? " and  me.MASSE_ID = ".$idMe : ($codeMe != "" ? " and me.MASSE_CODE = '".$codeMe."'" : "")).
    					" GROUP BY me.MASSE_ID";               
    return $this->dbConn->initDataset( $strSql ) ;
  }

  
    /**
   * @brief Retourne la liste des d�partements d'une masse d'eau
   *
   * @param idMe  Identifiant de la masse d'eau
   *
   * @return Retourne un dataset
   */
  function getDs_listeDeptMasseEau($idMe) 
  {          
    $strSql = "select * ".
    					" from MASSE_EAU_DEPT dept".
    					" where MASSE_ID = ".$idMe;
    return $this->dbConn->initDataset( $strSql ) ;
  }


  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   * @param $bassin_id : identifiant du bassin de l'utilisateur courant
   *
   * @return Retourne un dataset
   */
  function getDs_listeOperateur($idFirst, $idLast, $bassin_id="") 
  {          
    $strSql = "select OPERATEUR_ID ID, OPERATEUR_NOM LIB from OPERATEUR op".
              ($bassin_id != "" ? " where BASSIN_ID=".$bassin_id : "").      					
              " order by OPERATEUR_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }  

    /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_OperateurById($id) 
  {          
    $strSql = "select OPERATEUR_ID ID, OPERATEUR_NOM NOM, OPERATEUR_CODE CODE from OPERATEUR ".
    					" where OPERATEUR_ID = ".$id;
    return $this->dbConn->initDataset( $strSql ) ;
  }
    
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   * @param $bassin_id : identifiant du bassin de l'utilisateur courant
   * @return Retourne un dataset
   */
  function getDs_listeReseau($idFirst, $idLast, $bassin_id="") 
  {          
    $strSql = "select RESEAU_ID ID, RESEAU_NOM LIB, RESEAU_CODE CODE from RESEAU ".
              ($bassin_id != "" ? " where BASSIN_ID=".$bassin_id : "").   
    					" order by RESEAU_RANG, RESEAU_CODE, RESEAU_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param $bassin_id : identifiant du code de bassin
   * @return Retourne un dataset
   */
  function getDs_listeReseauByBassinCode($bassin_code="") 
  {          
    $strSql = "select RESEAU_ID, RESEAU_NOM, RESEAU_CODE,  RESEAU_SYMBOLE,  RESEAU_COULEUR, RESEAU_SYMBOLE_TAILLE, RESEAU_IMG_SYMB from RESEAU inner join BASSIN_HYDROGRAPHIQUE on ".
              " BASSIN_HYDROGRAPHIQUE.BASSIN_ID =  RESEAU.BASSIN_ID".
              ($bassin_code != "" ? " where BASSIN_CODE='".$bassin_code."'" : "").   
              " order by RESEAU_RANG, RESEAU_CODE, RESEAU_NOM";
    return $this->dbConn->initDataset( $strSql);
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_ReseauById($id) 
  {          
    $strSql = "select RESEAU_ID ID, GROUPE_ID, RESEAU_NOM NOM, RESEAU_CODE CODE, RESEAU_RANG RANG, RESEAU_COULEUR COULEUR, RESEAU_SYMBOLE PICTO, RESEAU_SYMBOLE_TAILLE, RESEAU_IMG_SYMB IMG_SYMB ".      
              " from RESEAU ".
    					" where RESEAU_ID = ".$id;
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_listePoint($idFirst, $idLast, $bassin_id="") 
  {          
    $strSql = "select POINT_ID ID, ".$this->dbConn->getConcat("POINT_NOM", "' - '", "POINT_CODE")." LIB from POINT pt".
    					" inner join MASSE_EAU as me on pt.MASSE_ID=me.MASSE_ID".
    	        ($bassin_id != "" ? " and me.BASSIN_ID = ".$bassin_id : "").      
    					" order by POINT_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }   
  
  
    /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param $reseau_code  Code du reseau
   * @param $bassin_id id du bassin
   * @return Retourne un dataset
   */
  function getDs_listePointByReseau($idFirst, $idLast, $reseau_code="", $bassin_code="", $strWhere="") 
  {          
    $strSql = "select rpp.POINT_ID, POINT_NOM, POINT_CODE, POINT_LONGITUDE, POINT_LATITUDE, MASSE_CODE, MASSE_NOM, BASSIN_NOM, BASSIN_CODE, RESEAU_NOM " .
              ", medept.DEPT, ".              
              $this->dbConn->getConcat("MASSE_NOM", "' - '", "MASSE_CODE")." LIB_MASSE, ".
              $this->dbConn->getConcat("descr.MASSE_DESC_CODE", "' - '", "descr.MASSE_DESC_LIBELLE")." DESCR ".  
              " from POINT as pt".
              " inner join MASSE_EAU as me on pt.MASSE_ID=me.MASSE_ID".              
              " left join RESEAU_POINT_PARAM as rpp on pt.POINT_ID=rpp.POINT_ID".
              " left join RESEAU as r on r.RESEAU_ID=rpp.RESEAU_ID".              
              " left join BASSIN_HYDROGRAPHIQUE bas on bas.BASSIN_ID = me.BASSIN_ID".
              " left join MASSE_EAU_DESCRIPTION descr on descr.MASSE_DESC_ID = me.MASSE_DESC_ID".
              " left join (select medept.MASSE_ID, GROUP_CONCAT(dept.DEPT_NOM SEPARATOR ', ') DEPT from ".
              " MASSE_EAU_DEPT medept". 
              " left join DEPARTEMENT dept on dept.DEPT_ID = medept.DEPT_ID".
              " group by medept.MASSE_ID) medept on medept.MASSE_ID = me.MASSE_ID".   
              " where 1=1". 
              ($reseau_code != "" ? " and RESEAU_CODE = '".strtoupper($this->dbConn->analyseSql($reseau_code))."'" : ""). 
              ($bassin_code != "" ? " and bas.BASSIN_CODE ='".strtoupper($this->dbConn->analyseSql($bassin_code))."'": "").      
              ($strWhere != "" ? " and ".$strWhere : "").  
              " group by me.MASSE_ID, pt.POINT_ID".                               
              " order by me.MASSE_RANG_GEO, me.MASSE_CODE, POINT_NOM";
              
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }  
  
    /**
   * @brief Retourne la liste des groupes de r�seaux associ�s ou non � une me
   *
   * @param $masse_id id de la masse d'eau
   * @return Retourne un dataset
   */
  function getDs_listeGroupeReseauByMe($masse_id="", $session_id="-1") 
  {          
    $strSql = ($session_id == -1 
                ? "select rg.GROUPE_ID, rg.GROUPE_NOM, ".
                  $this->dbConn->compareSql("SUM(NB_POINT)", "is", "NULL", "'0'", "SUM(NB_POINT)"). " as NB_POINT,".
                  " GROUP_CONCAT(rpoint.RESEAU_NOM SEPARATOR '<br/>') as LIST_RESO".                              
                  " from RESEAU_GROUPE rg".
        					" inner join RESEAU as r on rg.GROUPE_ID=r.GROUPE_ID".
                  " left join ".
                  " (select COUNT(DISTINCT pt.POINT_ID) as NB_POINT, rpp.RESEAU_ID, reso.RESEAU_NOM from ".
                  "  POINT as pt".
                  " inner join RESEAU_POINT_PARAM as rpp on pt.POINT_ID=rpp.POINT_ID". 
                  " inner join RESEAU as reso on reso.RESEAU_ID=rpp.RESEAU_ID".
                  " where pt.MASSE_ID = ".$masse_id.
                  " group by rpp.RESEAU_ID, reso.RESEAU_NOM) as rpoint on rpoint.RESEAU_ID = r.RESEAU_ID".              
                  " where rg.GROUPE_B_PROG_SURV = 1".   	
                  " group by rg.GROUPE_ID"
                              
                : "select MASSE_TYPE_SUIVI as GROUPE_NOM, MASSE_B_SUIVI_NBPOINT as NB_POINT, MASSE_LIST_RESEAU as LIST_RESO".                              
                  " from MASSE_EAU_SESSION_SUIVI".
                  " where MASSE_ID = ".$masse_id.
                  " and SESSION_ID = ".$session_id
              );    
              
    return $this->dbConn->initDataset( $strSql) ;
  }  
    
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   * @param $bassin_id : identifiant du bassin de l'utilisateur courant
   * @return Retourne un dataset
   */
  function getDs_listeParametre($idFirst, $idLast, $bassin_id="") 
  {          
    $strSql = "select par.PARAMETRE_ID ID, par.PARAMETRE_NOM LIB, s.SUPPORT_NOM, r.RESEAU_NOM from PARAMETRE par".
    					" left join SUPPORT s on par.SUPPORT_ID = s.SUPPORT_ID".
    					" left join RESEAU r on r.RESEAU_ID = s.RESEAU_ID".
              ($bassin_id != "" ? " where BASSIN_ID = ".$bassin_id : "").  
    					" order by r.RESEAU_NOM, s.SUPPORT_NOM, PARAMETRE_NOM";
    
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }     

  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_PointById($id) 
  {          
    $strSql = "select POINT_ID ID, POINT_NOM NOM, POINT_CODE CODE, POINT_LONGITUDE, POINT_LATITUDE, MASSE_ID from POINT ".
    					" where POINT_ID = ".$id;
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_SupportById($id) 
  {          
    $strSql = "select SUPPORT_ID ID, SUPPORT_NOM NOM, SUPPORT_CODE CODE from SUPPORT ".
    					" where SUPPORT_ID = ".$id;    					
    return $this->dbConn->initDataset( $strSql ) ;
  }  

  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_ParametreById($id) 
  {          
    $strSql = "select PARAMETRE_ID ID, PARAMETRE_NOM NOM, PARAMETRE_CODE CODE, SUPPORT_ID from PARAMETRE ".
    					" where PARAMETRE_ID = ".$id;
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_listeSupportByReseau($idReseau) 
  {          
    $strSql = "select SUPPORT_ID ID, SUPPORT_NOM LIB, SUPPORT_CODE CODE from SUPPORT ".
    					" where RESEAU_ID = ".$idReseau.
    					" order by SUPPORT_NOM";
    return $this->dbConn->initDataset( $strSql, 0, -1 ) ;
  }
  
 /**
   * @brief Retourne la liste des dictiionnaires relatifs � l'�tat de qualit� des masses d'eau
   *
   * @param strTabledico  Nom de la table dictionnaire 
   * @param strPrefixe  	Pr�fixe pr�c�dant le nom du champ 
   *
   * @return Retourne un dataset
   */
  function getDs_listeEtat($idFirst=0, $idLast=-1, $idTypeElementQualite="", $idElementQualite="", $strEtat="", $session_id=-1)
  {          
    $strSql = "select ETAT_LIBELLE LIB, ETAT_ID ID,  cl.TYPE_CLASSEMENT_LIBELLE CLASSEMENT, cl.TYPE_CLASSEMENT_ID, ETAT_COULEUR, ETAT_VALEUR,  cl.TYPE_CLASSEMENT_AFF CLASSEMENT_LEG". 
    					"	from ETAT".($session_id != -1 ? "_SESSION" : "")." eta, TYPE_CLASSEMENT cl".
    					" where eta.TYPE_CLASSEMENT_ID = cl.TYPE_CLASSEMENT_ID".
              ($session_id != -1 ? " and SESSION_ID = ".$session_id : "").
    					($idTypeElementQualite != "" ? " and eta.TYPE_CLASSEMENT_ID in (select TYPE_CLASSEMENT_ID from TYPE_ELEMENT_QUALITE where TYPE_ELEMENT_QUALITE_ID = ".$idTypeElementQualite.")" : "").
    					($idElementQualite != "" ? " and eta.TYPE_CLASSEMENT_ID in (select TYPE_CLASSEMENT_ID from ELEMENT_QUALITE where ELEMENT_QUALITE_ID = ".$idElementQualite.")" : "").
    					($strEtat != "" ? " and ".$this->dbConn->getLowerCase("ETAT_LIBELLE")." = '".strtolower($this->dbConn->analyseSql($strEtat))."'" : "").    					
    					" order by eta.TYPE_CLASSEMENT_ID, ETAT_VALEUR";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }  

 /**
   * @brief Retourne la fiche d'un etat
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_EtatById($id) 
  {          
    $strSql = "select ETAT_ID ID, ETAT_LIBELLE LIB, TYPE_CLASSEMENT_ID, ETAT_COULEUR, ETAT_VALEUR from ETAT ".
    					" where ETAT_ID = ".$id;
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
 /**
   * @brief Retourne la fiche d'un etat
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_EtatMotifById($id, $code="", $bassin_id=-1) 
  {          
    $strSql = "select MOTIF_ID ID, MOTIF_LIBELLE LIB, MOTIF_CODE CODE from ETAT_MOTIF".
    					" where 1=1".
    					($id != "" ? " and MOTIF_ID = ".$id : "").    					
    					($code != "" ? " and ".$this->dbConn->getUpperCase("MOTIF_CODE")." = '".strtoupper($this->dbConn->analyseSql($code))."'" : "").
    					($bassin_id!=-1 ? " and BASSIN_ID =".$bassin_id : "");    					
    					
    return $this->dbConn->initDataset( $strSql ) ;
  }
 /**
   * @brief Retourne la fiche d'un statut
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_StatutById($id, $code="") 
  {          
    $strSql = "select STATUT_ID ID, STATUT_LIBELLE LIB, STATUT_CODE CODE from CLASSEMENT_STATUT".
    					" where 1=1".
    					($id != "" ? " and STATUT_ID = ".$id : "").    					
    					($code != "" ? " and ".$this->dbConn->getUpperCase("STATUT_CODE")." = '".strtoupper($this->dbConn->analyseSql($code))."'" : "");    					
    					
    return $this->dbConn->initDataset( $strSql ) ;
  }    
  /**
   * @brief Retourne la liste des dictiionnaires relatifs � l'�tat de qualit� des masses d'eau
   *
   * @param strTabledico  Nom de la table dictionnaire 
   * @param strPrefixe  	Pr�fixe pr�c�dant le nom du champ 
   *
   * @return Retourne un dataset
   */
  function getDs_listeEtatParametre($strTableDico, $strPrefixe, $idFirst=0, $idLast=-1, $bassin_id=-1)
  {          
    $strSql = "select ".$strPrefixe."_ID ID, ".$strPrefixe."_LIBELLE LIB from ".$strTableDico.
              ($bassin_id != -1 ? " where BASSIN_ID = ".$bassin_id : "").
    					" order by LIB";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }  
  
     
  /**
   * @brief fonction pour le recuperation de  l'intitule des champs et l'id leurs correspandants.
   *        11_import
   *
   * @param $strTable nom de la table.
   * @param $strChamp nom du champs.
   * @param $strIdChamp nom de l'id du champ.
   * @return un tableau 
   */
	function GetDs_champsImport($strTable, $strChamp, $strIdChamp, $strCond = "")
	{
		$tabChamp =array();
    $strSql = "select " .$strChamp." , ".$strIdChamp." from ".$strTable.
              ($strCond != "" ? " where ".$strCond : "") ;
    $dsImport = $this->dbConn->InitDataSet($strSql);
		while( $drImport = $dsImport->getRowIter() ) {
			$strName = $drImport->getValueName($strChamp); 
			$idName = $drImport->getValueName($strIdChamp);
			$tabChamp[$idName] = $strName;
		}
		return $tabChamp;
	}  
	
 /**
   * @brief Retourne la fiche d'un etat
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_ElementQualiteById($id, $element_nom="", $iNiveau="", $bassin_id=-1, $session_id=-1) 
  {          
     $strSql = "select * from ELEMENT_QUALITE".($session_id != -1 ? "_SESSION" : "").
    					" where 1=1".
    					($id != "" ? " and ELEMENT_QUALITE_ID = ".$id : "").    					
    					($element_nom != "" ? " and ".$this->dbConn->getLowerCase("ELEMENT_QUALITE_NOM")." like '".$this->dbConn->analyseSql(strtolower(strtr($element_nom, "����������i���o�� ", "___________________")))."'" : "").
    					($iNiveau != "" ? " and ELEMENT_QUALITE_NIVEAU = ".$iNiveau : "").
    					($bassin_id!=-1 ? " and BASSIN_ID =".$bassin_id : "").
    					($session_id!=-1 ? " and SESSION_ID =".$session_id : "");
    return $this->dbConn->initDataset( $strSql ) ;
  }
  	
 /**
   * @brief Retourne la fiche d'un etat
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_TypeElementQualiteById($id, $element_nom="") 
  {          
    $strSql = "select * from TYPE_ELEMENT_QUALITE ".
    					" where 1=1".
    					($id != "" ? " and TYPE_ELEMENT_QUALITE_ID = ".$id : "").
    					($element_nom != "" ? " and ".$this->dbConn->getUpperCase("TYPE_ELEMENT_QUALITE_NOM")." = '".$this->dbConn->analyseSql(strtoupper($element_nom))."'" : "");
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la liste des �l�ments de qualit�
   *
   * iNiveau specifie le niveau des �l�ments de qualit� � retourner
   *
   * @return Retourne un dataset
   */
  function getDs_listeElementqualite($iNiveau=-1, $idFirst=0, $idLast=-1, $strOrderBy="", $bassin_id=-1, $idParent =-1, $session_id=-1) 
  {          
    $strSql = "select ".
    					$this->dbConn->CompareSql("ELEMENT_QUALITE_NIVEAU", ">", "1", $this->dbConn->getConcat("'---&nbsp;'", "ELEMENT_QUALITE_NOM"), "ELEMENT_QUALITE_NOM")." as LIB".
    					", ELEMENT_QUALITE_ID ID, ELEMENT_QUALITE_ID, ELEMENT_QUALITE_PARENT, ELEMENT_QUALITE_NIVEAU from ELEMENT_QUALITE".($session_id != -1 ? "_SESSION" : "")." qual".
              " where 1=1 ".
    					($bassin_id!=-1 ? " and BASSIN_ID =".$bassin_id : "").
    					($idParent!=-1 ? " and ELEMENT_QUALITE_PARENT=".$idParent : "").
    					($iNiveau != -1 ? " and ELEMENT_QUALITE_NIVEAU = ".$iNiveau : "").
    					($session_id!=-1 ? " and SESSION_ID =".$session_id : "").
    					($strOrderBy != "" ? " order by ".$strOrderBy : "");
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }    


  /**
   * @brief Retourne la liste des �l�ments de qualit�
   *
   * iNiveau specifie le niveau des �l�ments de qualit� � retourner
   *
   * @return Retourne un dataset
   */
  function getDs_listeElementqualiteCarte($iNiveau=-1, $idFirst=0, $idLast=-1, $strOrderBy="", $bassin_id=-1, $idParent =-1, $session_id=-1) 
  {          
    $strSql = "select  ".
              $this->dbConn->CompareSql("ELEMENT_QUALITE_NIVEAU", ">", "1", $this->dbConn->getConcat("'---&nbsp;'", "ELEMENT_QUALITE_NOM"), "ELEMENT_QUALITE_NOM")." as LIB".
              ", ELEMENT_QUALITE_ID ID, ELEMENT_QUALITE_ID, ELEMENT_QUALITE_PARENT, ELEMENT_QUALITE_NIVEAU, TYPE_CLASSEMENT_LIBELLE, TYPE_ELEMENT_QUALITE_NOM " .
              " from ELEMENT_QUALITE".($session_id != -1 ? "_SESSION" : "")." qual".
              " LEFT JOIN TYPE_ELEMENT_QUALITE tqual ON tqual.TYPE_ELEMENT_QUALITE_ID = qual.TYPE_ELEMENT_QUALITE_ID ".
              " LEFT JOIN TYPE_CLASSEMENT tp ON tqual.TYPE_CLASSEMENT_ID = tp.TYPE_CLASSEMENT_ID ".
              " where 1=1 ".
              ($bassin_id!=-1 ? " and BASSIN_ID =".$bassin_id : "").
              ($idParent!=-1 ? " and ELEMENT_QUALITE_PARENT=".$idParent : "").
              ($iNiveau != -1 ? " and ELEMENT_QUALITE_NIVEAU = ".$iNiveau : "").
              ($session_id!=-1 ? " and SESSION_ID =".$session_id : "").
              ($strOrderBy != "" ? " order by ".$strOrderBy : "");
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }  




  
  /**
   * @brief Retourne le ds d'un bassin
   *
   * bassin_code code bassin
   *
   * @return Retourne un dataset
   */
  function getDs_bassin($bassin_code="", $bassin_id="") 
  {          
    $strSql = "select * from BASSIN_HYDROGRAPHIQUE where 1=1".
    					($bassin_code != "" ? " and BASSIN_CODE = '".$bassin_code."'" : "").
    					($bassin_id != "" ? " and BASSIN_ID = '".$bassin_id."'" : "");
    return $this->dbConn->initDataset( $strSql) ;
  }      	

  /**
   * @brief Retourne la liste des �tats par �l�ment de qualit� pour une masse d'eau et la session en cours
   * 				L'�tat est inconnu si non saisi
   * 				Tous les �l�ments de qualit� sont retourn�s dans le dataset
   *
   * codeMe code de la masse d'eau
   *
   * @return Retourne un dataset 
   */
  function getDs_listeEtatMasseEau($codeMe="", $iNiveau="", $idSession=-1, $idFirst=0, $idLast=-1) 
  {    
    $strSqlSession = ($idSession == -1 
                        ? "SELECT SESSION_ID
													 FROM SESSION_QUALITE session, MASSE_EAU meau 
													 where session.SESSION_ARCHIVE = 0 and session.BASSIN_ID = meau.BASSIN_ID and meau.MASSE_CODE = '".$codeMe."'"
                        : "SELECT session.SESSION_ID
													 FROM SESSION_QUALITE session, MASSE_EAU_SESSION meau 
													 where session.SESSION_ID = ".$idSession." and meau.SESSION_ID=session.SESSION_ID and session.BASSIN_ID = meau.BASSIN_ID and meau.MASSE_CODE = '".$codeMe."'");
                        
    $strExtSession = ($idSession == -1 ? "" : "_SESSION");
    $strWhereSession = ($idSession == -1 ? "" : " and SESSION_ID = ".$idSession);
                       
  	$strSqlTypeElement = "SELECT '1_TYPE_ELEMENT_QUALITE' as TYPE_ETAT, b.BASSIN_CODE, b.BASSIN_ID, me.MASSE_CODE, classement.TYPE_CLASSEMENT_LIBELLE, tele.TYPE_CLASSEMENT_ID, tele.TYPE_ELEMENT_QUALITE_ID, tele.TYPE_ELEMENT_QUALITE_NOM, NULL as ELEMENT_QUALITE_NOM, 
  						 NULL as ELEMENT_QUALITE_NIVEAU, NULL as ELEMENT_QUALITE_ID, NULL as ELEMENT_QUALITE_URL_FICHE, 0 as ELEMENT_QUALITE_RANG, NULL as ELEMENT_QUALITE_PARENT, ".
  						 $this->dbConn->GetDateFormat("DD/MM/YYYY", "cl.CLASSEMENT_DATE" , false)." as CLASSEMENT_DATE, ".
  						 "cl.CLASSEMENT_BILAN, cl.CLASSEMENT_COMPLEMENT_BILAN, cl.CLASSEMENT_DOC_REF, cl.CLASSEMENT_INDICE_CONFIANCE, ".  						 
  						 	$this->dbConn->compareSql("etat.ETAT_ID", "is", "NULL", "etat_indefini.ETAT_ID", "etat.ETAT_ID"). " as ETAT_ID, ".
  							$this->dbConn->compareSql("etat.ETAT_LIBELLE", "is", "NULL", "etat_indefini.ETAT_LIBELLE", "etat.ETAT_LIBELLE"). " as ETAT_LIBELLE, ".
  							$this->dbConn->compareSql("etat.ETAT_COULEUR", "is", "NULL", "etat_indefini.ETAT_COULEUR", "etat.ETAT_COULEUR"). " as ETAT_COULEUR, ".
  							$this->dbConn->compareSql("etat.ETAT_VALEUR", "is", "NULL", "etat_indefini.ETAT_VALEUR", "etat.ETAT_VALEUR"). " as ETAT_VALEUR, ".  							
  						" st.STATUT_CODE, st.STATUT_ID, motif.MOTIF_CODE, motif.MOTIF_ID, cl.CLASSEMENT_ID, 1 as B_AFF  
							FROM TYPE_ELEMENT_QUALITE tele
							LEFT JOIN TYPE_CLASSEMENT classement ON classement.TYPE_CLASSEMENT_ID = tele.TYPE_CLASSEMENT_ID
							LEFT JOIN CLASSEMENT_MASSE_EAU".$strExtSession." cl ON cl.TYPE_ELEMENT_QUALITE_ID = tele.TYPE_ELEMENT_QUALITE_ID and cl.SESSION_ID = (".$strSqlSession.") 
							AND cl.MASSE_ID = (SELECT MASSE_ID FROM MASSE_EAU".$strExtSession." me where me.MASSE_CODE = '".$codeMe."'".($idSession == -1 ? "" : " and me.SESSION_ID = ".$idSession).")
							LEFT JOIN MASSE_EAU".$strExtSession." me ON me.MASSE_ID = cl.MASSE_ID".($idSession == -1 ? "" : " and me.SESSION_ID = ".$idSession)."
							LEFT JOIN BASSIN_HYDROGRAPHIQUE b ON b.BASSIN_ID = me.BASSIN_ID
							LEFT JOIN CLASSEMENT_STATUT".$strExtSession." st ON st.STATUT_ID = cl.STATUT_ID ".($idSession == -1 ? "" : " and cl.SESSION_ID = ".$idSession)."
							LEFT JOIN ETAT_MOTIF".$strExtSession."  motif ON motif.MOTIF_ID = cl.MOTIF_ID".($idSession == -1 ? "" : " and motif.SESSION_ID = ".$idSession)."
							LEFT JOIN ETAT".$strExtSession." etat ON etat.ETAT_ID = cl.ETAT_ID".($idSession == -1 ? "" : " and etat.SESSION_ID = ".$idSession)."
							LEFT JOIN ETAT".$strExtSession." etat_indefini ON etat_indefini.TYPE_CLASSEMENT_ID = tele.TYPE_CLASSEMENT_ID and etat_indefini.ETAT_VALEUR = 0 and (etat_indefini.ETAT_ID = 8 or etat_indefini.ETAT_ID = 9)".($idSession == -1 ? "" : " and etat_indefini.SESSION_ID = ".$idSession);
            
  	$strSqlElement = "SELECT '2_ELEMENT_QUALITE' as TYPE_ETAT, b.BASSIN_CODE, b.BASSIN_ID, me.MASSE_CODE, classement.TYPE_CLASSEMENT_LIBELLE, ele.TYPE_CLASSEMENT_ID, tele.TYPE_ELEMENT_QUALITE_ID, tele.TYPE_ELEMENT_QUALITE_NOM, ele.ELEMENT_QUALITE_NOM,  						 
  						 ele.ELEMENT_QUALITE_NIVEAU,  ele.ELEMENT_QUALITE_ID, ele.ELEMENT_QUALITE_URL_FICHE, ele.ELEMENT_QUALITE_RANG as ELEMENT_QUALITE_RANG, ele.ELEMENT_QUALITE_PARENT as ELEMENT_QUALITE_PARENT, ".
  						 $this->dbConn->GetDateFormat("DD/MM/YYYY", "cl.CLASSEMENT_DATE" , false)." as CLASSEMENT_DATE, ".
  						 "cl.CLASSEMENT_BILAN, ".
  						 "cl.CLASSEMENT_COMPLEMENT_BILAN, ".
  						 "cl.CLASSEMENT_DOC_REF, ".
  						 " cl.CLASSEMENT_INDICE_CONFIANCE, ".  	
  						 $this->dbConn->compareSql("etat.ETAT_ID", "is", "NULL", "etat_indefini.ETAT_ID", "etat.ETAT_ID"). " as ETAT_ID, ".  						 
  						 $this->dbConn->compareSql("etat.ETAT_LIBELLE", "is", "NULL", "etat_indefini.ETAT_LIBELLE", "etat.ETAT_LIBELLE"). " as ETAT_LIBELLE, ".
  						 $this->dbConn->compareSql("etat.ETAT_COULEUR", "is", "NULL", "etat_indefini.ETAT_COULEUR", "etat.ETAT_COULEUR"). " as ETAT_COULEUR, ".
  						 $this->dbConn->compareSql("etat.ETAT_VALEUR", "is", "NULL", "etat_indefini.ETAT_VALEUR", "etat.ETAT_VALEUR"). " as ETAT_VALEUR, ".
							" st.STATUT_CODE, st.STATUT_ID, motif.MOTIF_CODE, motif.MOTIF_ID, cl.CLASSEMENT_ID, ".
  						"(ele.ELEMENT_QUALITE_RESTRICT_TYPEME is NULL or ele.ELEMENT_QUALITE_RESTRICT_TYPEME='' or ele.ELEMENT_QUALITE_RESTRICT_TYPEME=me.MASSE_TYPE) as B_AFF
							FROM ELEMENT_QUALITE".$strExtSession." ele
							LEFT JOIN TYPE_ELEMENT_QUALITE tele ON tele.TYPE_ELEMENT_QUALITE_ID = ele.TYPE_ELEMENT_QUALITE_ID
							LEFT JOIN TYPE_CLASSEMENT classement ON classement.TYPE_CLASSEMENT_ID = ele.TYPE_CLASSEMENT_ID
							LEFT JOIN CLASSEMENT_MASSE_EAU".$strExtSession."  cl ON cl.ELEMENT_QUALITE_ID = ele.ELEMENT_QUALITE_ID and cl.SESSION_ID = (".$strSqlSession.")
							AND cl.MASSE_ID = (SELECT MASSE_ID FROM MASSE_EAU".$strExtSession." me where me.MASSE_CODE = '".$codeMe."'".($idSession == -1 ? "" : " and me.SESSION_ID = ".$idSession).")
							LEFT JOIN MASSE_EAU".$strExtSession." me ON me.MASSE_CODE = '".$codeMe."'".($idSession == -1 ? "" : " and me.SESSION_ID = ".$idSession)."
							LEFT JOIN BASSIN_HYDROGRAPHIQUE b ON b.BASSIN_ID = me.BASSIN_ID
							LEFT JOIN CLASSEMENT_STATUT".$strExtSession." st ON st.STATUT_ID = cl.STATUT_ID".($idSession == -1 ? "" : " and st.SESSION_ID = ".$idSession)."
							LEFT JOIN ETAT_MOTIF".$strExtSession." motif ON motif.MOTIF_ID = cl.MOTIF_ID".($idSession == -1 ? "" : " and motif.SESSION_ID = ".$idSession)."							
							LEFT JOIN ETAT".$strExtSession." etat ON etat.ETAT_ID = cl.ETAT_ID".($idSession == -1 ? "" : " and etat.SESSION_ID = ".$idSession)."
							LEFT JOIN ETAT".$strExtSession." etat_indefini ON etat_indefini.TYPE_CLASSEMENT_ID = ele.TYPE_CLASSEMENT_ID and etat_indefini.ETAT_VALEUR = 0 and (etat_indefini.ETAT_ID = 8 or etat_indefini.ETAT_ID = 9)".($idSession == -1 ? "" : " and etat_indefini.SESSION_ID = ".$idSession).  						
  						" where 1=1 ". 
  						 " and ele.BASSIN_ID = (select BASSIN_ID from MASSE_EAU".$strExtSession." me WHERE me.MASSE_CODE = '".$codeMe."'".($idSession == -1 ? "" : " and me.SESSION_ID = ".$idSession).")".($idSession == -1 ? "" : " and ele.SESSION_ID = ".$idSession).              
               ($iNiveau != "" ? " and ele.ELEMENT_QUALITE_NIVEAU=".$iNiveau : "");
               
    $strSqlTypeClassement = "SELECT '3_TYPE_CLASSEMENT' as TYPE_ETAT, b.BASSIN_CODE, b.BASSIN_ID, me.MASSE_CODE, classement.TYPE_CLASSEMENT_LIBELLE, classement.TYPE_CLASSEMENT_ID, NULL as TYPE_ELEMENT_QUALITE_ID, NULL as TYPE_ELEMENT_QUALITE_NOM, NULL as ELEMENT_QUALITE_NOM, 
  						 NULL as ELEMENT_QUALITE_NIVEAU, NULL as ELEMENT_QUALITE_ID, NULL as ELEMENT_QUALITE_URL_FICHE, 0 as ELEMENT_QUALITE_RANG, NULL as ELEMENT_QUALITE_PARENT, ".
  						 $this->dbConn->GetDateFormat("DD/MM/YYYY", "cl.CLASSEMENT_DATE" , false)." as CLASSEMENT_DATE, ".
  						 "cl.CLASSEMENT_BILAN, cl.CLASSEMENT_COMPLEMENT_BILAN, cl.CLASSEMENT_DOC_REF, cl.CLASSEMENT_INDICE_CONFIANCE, ".  						 
  						 	$this->dbConn->compareSql("etat.ETAT_ID", "is", "NULL", "etat_indefini.ETAT_ID", "etat.ETAT_ID"). " as ETAT_ID, ".
  							$this->dbConn->compareSql("etat.ETAT_LIBELLE", "is", "NULL", "etat_indefini.ETAT_LIBELLE", "etat.ETAT_LIBELLE"). " as ETAT_LIBELLE, ".
  							$this->dbConn->compareSql("etat.ETAT_COULEUR", "is", "NULL", "etat_indefini.ETAT_COULEUR", "etat.ETAT_COULEUR"). " as ETAT_COULEUR, ".
  							$this->dbConn->compareSql("etat.ETAT_VALEUR", "is", "NULL", "etat_indefini.ETAT_VALEUR", "etat.ETAT_VALEUR"). " as ETAT_VALEUR, ".  							
  						" st.STATUT_CODE, st.STATUT_ID, motif.MOTIF_CODE, motif.MOTIF_ID, cl.CLASSEMENT_ID, 1 as B_AFF
							FROM TYPE_CLASSEMENT classement
							LEFT JOIN CLASSEMENT_MASSE_EAU".$strExtSession." cl ON cl.TYPE_CLASSEMENT_ID = classement.TYPE_CLASSEMENT_ID and cl.SESSION_ID = (".$strSqlSession.") 
							AND cl.MASSE_ID = (SELECT MASSE_ID FROM MASSE_EAU".$strExtSession." me where me.MASSE_CODE = '".$codeMe."'".($idSession == -1 ? "" : " and me.SESSION_ID = ".$idSession).")
							LEFT JOIN MASSE_EAU".$strExtSession." me ON me.MASSE_CODE = '".$codeMe."'".($idSession == -1 ? "" : " and me.SESSION_ID = ".$idSession)."
							LEFT JOIN BASSIN_HYDROGRAPHIQUE b ON b.BASSIN_ID = me.BASSIN_ID
							LEFT JOIN CLASSEMENT_STATUT".$strExtSession." st ON st.STATUT_ID = cl.STATUT_ID".($idSession == -1 ? "" : " and st.SESSION_ID = ".$idSession)."
							LEFT JOIN ETAT_MOTIF".$strExtSession." motif ON motif.MOTIF_ID = cl.MOTIF_ID".($idSession == -1 ? "" : " and motif.SESSION_ID = ".$idSession)."							
							LEFT JOIN ETAT".$strExtSession." etat ON etat.ETAT_ID = cl.ETAT_ID".($idSession == -1 ? "" : " and etat.SESSION_ID = ".$idSession)."
							LEFT JOIN ETAT".$strExtSession." etat_indefini ON etat_indefini.TYPE_CLASSEMENT_ID = cl.TYPE_CLASSEMENT_ID and etat_indefini.ETAT_VALEUR = 0 and (etat_indefini.ETAT_ID = 8 or etat_indefini.ETAT_ID = 9)".($idSession == -1 ? "" : " and etat_indefini.SESSION_ID = ".$idSession);

  	$strSqlGlobal = "SELECT '4_TYPE_GLOBAL' as TYPE_ETAT, b.BASSIN_CODE, b.BASSIN_ID, me.MASSE_CODE, NULL as TYPE_CLASSEMENT_LIBELLE, NULL as TYPE_CLASSEMENT_ID, NULL as TYPE_ELEMENT_QUALITE_ID, NULL as TYPE_ELEMENT_QUALITE_NOM, NULL as ELEMENT_QUALITE_NOM, 
  						 NULL as ELEMENT_QUALITE_NIVEAU, NULL as ELEMENT_QUALITE_ID, NULL as ELEMENT_QUALITE_URL_FICHE, 0 as ELEMENT_QUALITE_RANG, NULL as ELEMENT_QUALITE_PARENT, ".
  						 $this->dbConn->GetDateFormat("DD/MM/YYYY", "cl.CLASSEMENT_DATE" , false)." as CLASSEMENT_DATE, ".
  						 "cl.CLASSEMENT_BILAN, cl.CLASSEMENT_COMPLEMENT_BILAN, cl.CLASSEMENT_DOC_REF, cl.CLASSEMENT_INDICE_CONFIANCE, ".  						 
  						 	$this->dbConn->compareSql("etat.ETAT_ID", "is", "NULL", "etat_indefini.ETAT_ID", "etat.ETAT_ID"). " as ETAT_ID, ".
  							$this->dbConn->compareSql("etat.ETAT_LIBELLE", "is", "NULL", "etat_indefini.ETAT_LIBELLE", "etat.ETAT_LIBELLE"). " as ETAT_LIBELLE, ".
  							$this->dbConn->compareSql("etat.ETAT_COULEUR", "is", "NULL", "etat_indefini.ETAT_COULEUR", "etat.ETAT_COULEUR"). " as ETAT_COULEUR, ".
  							$this->dbConn->compareSql("etat.ETAT_VALEUR", "is", "NULL", "etat_indefini.ETAT_VALEUR", "etat.ETAT_VALEUR"). " as ETAT_VALEUR, ".  							
  						" st.STATUT_CODE, st.STATUT_ID, motif.MOTIF_CODE, motif.MOTIF_ID, cl.CLASSEMENT_ID, 1 as B_AFF
							FROM CLASSEMENT_MASSE_EAU".$strExtSession." cl 
							LEFT JOIN MASSE_EAU".$strExtSession." me ON me.MASSE_CODE = '".$codeMe."'".($idSession == -1 ? "" : " and me.SESSION_ID = ".$idSession)."
							LEFT JOIN BASSIN_HYDROGRAPHIQUE b ON b.BASSIN_ID = me.BASSIN_ID
							LEFT JOIN CLASSEMENT_STATUT".$strExtSession." st ON st.STATUT_ID = cl.STATUT_ID".($idSession == -1 ? "" : " and cl.SESSION_ID = ".$idSession)."
							LEFT JOIN ETAT_MOTIF".$strExtSession." motif ON motif.MOTIF_ID = cl.MOTIF_ID".($idSession == -1 ? "" : " and motif.SESSION_ID = ".$idSession)."							
							LEFT JOIN ETAT".$strExtSession." etat ON etat.ETAT_ID = cl.ETAT_ID".($idSession == -1 ? "" : " and etat.SESSION_ID = ".$idSession)."
							LEFT JOIN ETAT".$strExtSession." etat_indefini ON etat_indefini.TYPE_CLASSEMENT_ID = cl.TYPE_CLASSEMENT_ID and etat_indefini.ETAT_VALEUR = 0 and (etat_indefini.ETAT_ID = 8 or etat_indefini.ETAT_ID = 9)".($idSession == -1 ? "" : " and etat_indefini.SESSION_ID = ".$idSession)."
  						WHERE cl.SESSION_ID = (".$strSqlSession.") 
							AND cl.MASSE_ID = (SELECT MASSE_ID FROM MASSE_EAU".$strExtSession." me where me.MASSE_CODE = '".$codeMe."'".($idSession == -1 ? "" : " and me.SESSION_ID = ".$idSession).")".
  						" AND cl.TYPE_CLASSEMENT_ID is NULL and cl.TYPE_ELEMENT_QUALITE_ID is NULL and cl.ELEMENT_QUALITE_ID is NULL";
  													
  	$strSql = "select * from (".
  						$strSqlTypeElement.
  						" UNION ".
  						$strSqlElement.
  						" UNION ".
  						$strSqlTypeClassement.
  						" UNION ".
  						$strSqlGlobal.") etat ".
  						" order by etat.TYPE_ETAT, etat.TYPE_CLASSEMENT_LIBELLE, etat.TYPE_ELEMENT_QUALITE_NOM, ELEMENT_QUALITE_NIVEAU, ELEMENT_QUALITE_PARENT, ELEMENT_QUALITE_RANG ";  						
    
    $ds = $this->dbConn->initDataset( $strSql, $idFirst, $idLast );
    $ds->setTree("ELEMENT_QUALITE_ID", "ELEMENT_QUALITE_PARENT"); 						
		return $ds ;
  }  	
  
  function getDs_QualiteMasseEau($codeMe="", $element_qualite_id="", $type_element_qualite_id="", $type_classement_id="") 
  {      
  	$strSql = "SELECT cl.*  
							FROM CLASSEMENT_MASSE_EAU cl where 1=1 ".
  						($type_element_qualite_id != "" ? " and cl.TYPE_ELEMENT_QUALITE_ID = ".$type_element_qualite_id 
  																						: " and cl.TYPE_ELEMENT_QUALITE_ID is NULL".
  						          ($element_qualite_id != "" ? " and cl.ELEMENT_QUALITE_ID = ".$element_qualite_id 
  						                                     : " and cl.ELEMENT_QUALITE_ID is NULL".
  						              ($type_classement_id != "" ? " and cl.TYPE_CLASSEMENT_ID = ".$type_classement_id 
  						                                         : " and cl.TYPE_CLASSEMENT_ID is NULL"))).
  						($element_qualite_id == "" && $type_element_qualite_id == "" && $type_classement_id == ""
								? " and ELEMENT_QUALITE_ID is NULL and TYPE_ELEMENT_QUALITE_ID is NULL and TYPE_CLASSEMENT_ID is NULL"
								: "").                                                                      
  						" and cl.SESSION_ID =  (
							SELECT SESSION_ID
							FROM SESSION_QUALITE session, MASSE_EAU meau 
							where session.SESSION_ARCHIVE = 0 and session.BASSIN_ID = meau.BASSIN_ID and meau.MASSE_CODE = '".$codeMe."')".
  					  " AND cl.MASSE_ID = (SELECT MASSE_ID FROM MASSE_EAU me WHERE me.MASSE_CODE = '".$codeMe."')";
		return $this->dbConn->initDataset( $strSql ) ;  																					
  }	

  /**
   * @brief Retourne la liste des typologies des masses d'eau 
   *
   * @param typeMe  C pour cotieres, T pour transition
   *
   * @return Retourne un dataset
   */
  function getDs_listeTypologieMasseEau($typeMe = "C", $bassin_code="", $session_id=-1) 
  {          
    $strSql = "select distinct descr.MASSE_DESC_ID, MASSE_DESC_CODE, MASSE_DESC_LIBELLE, MASSE_DESC_COLOR ".
    					" from MASSE_EAU_DESCRIPTION descr, MASSE_EAU".($session_id != -1 ? "_SESSION" : "")." me".    		
    					" where MASSE_DESC_TYPE = '".$typeMe."' and me.MASSE_DESC_ID = descr.MASSE_DESC_ID".
    					($bassin_code != '' ? " and me.BASSIN_ID in (select BASSIN_ID from BASSIN_HYDROGRAPHIQUE where BASSIN_CODE = '".$bassin_code."')" : "").
    					($session_id != -1 ? " and me.SESSION_ID = ".$session_id : "");
    return $this->dbConn->initDataset( $strSql ) ;
  }  
  
  /**
   * @brief Retourne la liste des r�seaux d'appartenance pour un point
   *
   * @param codePoint  Code du point
   *
   * @return Retourne un dataset
   */
  function getDs_listeReseauByPoint($codePoint, $session_id=-1) 
  {           
    $strExtSession = ($session_id != -1 ? "_SESSION" : "");
    $strWhereSession = ($session_id != -1 ? " and pt.SESSION_ID = ".$session_id." and me.SESSION_ID = pt.SESSION_ID and rpp.SESSION_ID = pt.SESSION_ID and reso.SESSION_ID = pt.SESSION_ID" : "");
    
    $strSql = "select distinct RESEAU_CODE from POINT".$strExtSession." pt, MASSE_EAU".$strExtSession." me, RESEAU_POINT_PARAM".$strExtSession." rpp, RESEAU".$strExtSession." reso".    
      " where pt.MASSE_ID = me.MASSE_ID" .
      " and pt.POINT_CODE = '".$this->dbConn->analyseSql($codePoint)."'".
      " and rpp.POINT_ID=pt.POINT_ID".
      " and reso.RESEAU_ID=rpp.RESEAU_ID".
      $strWhereSession;
    return $this->dbConn->initDataset( $strSql ) ;
  }  
  
 /**
   * @brief Retourne la liste des methodes d'�valuation pour une entite (type classement, type etat, element de qualite)
   *
   * @param type_classement_id
   * @param TYPE_ELEMENT_QUALITE_ID 
   * @param ELEMENT_QUALITE_ID 
   *
   * @return Retourne un dataset
   */
  function getDs_ficheMethode($type_classement_id, $type_element_qualite_id, $element_qualite_id, $bassin_id=-1, $session_id=-1) 
  {           
    $strSql = "select * from METHODE_EVALUATION".($session_id != -1 ? "_SESSION" : "").    
      " where (TYPE_CLASSEMENT_ID ".($type_classement_id != '' ? " = " .$type_classement_id.")" : " is NULL or TYPE_CLASSEMENT_ID=0)").
      " and (TYPE_ELEMENT_QUALITE_ID ".($type_element_qualite_id != '' ? " = " .$type_element_qualite_id.")" : " is NULL or TYPE_ELEMENT_QUALITE_ID=0)").
      " and (ELEMENT_QUALITE_ID ".($element_qualite_id != '' ? " = " .$element_qualite_id.")" : " is NULL or ELEMENT_QUALITE_ID=0)").
    	($bassin_id != '-1' && $bassin_id != '' ? " and BASSIN_ID = ".$bassin_id : "").
    	($session_id != -1 ? " and SESSION_ID = ".$session_id : "");
    return $this->dbConn->initDataset( $strSql ) ;
  }    
  
 /**
   * @brief Retourne la fiche d'un etat
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_MethodeById($id) 
  {          
    $strSql = "select *, METHODE_ID ID, METHODE_LIBELLE LIB from METHODE_EVALUATION".
    					" where 1=1".
    					($id != "" ? " and METHODE_ID = ".$id : "");    					
    					
    return $this->dbConn->initDataset( $strSql ) ;
  }  
  
  /**
   * @brief Retourne la liste des dictiionnaires relatifs � l'�tat de qualit� des masses d'eau
   *
   * @param strTabledico  Nom de la table dictionnaire 
   * @param strPrefixe  	Pr�fixe pr�c�dant le nom du champ 
   *
   * @return Retourne un dataset
   */
  function getDs_listeMethode($idFirst=0, $idLast=-1, $bassin_id=-1)
  {          
    $strSql = "select METHODE_ID ID, ".
              $this->dbConn->getConcat("'M�thode pour '",  
    					$this->dbConn->CompareSql("tc.TYPE_CLASSEMENT_ID", "is not", "NULL", $this->dbConn->getConcat("'�tat '", "tc.TYPE_CLASSEMENT_LIBELLE"), 
    					    $this->dbConn->CompareSql("tqual.TYPE_ELEMENT_QUALITE_ID", "is not", "NULL", $this->dbConn->getConcat("'�tat '", "tqual.TYPE_ELEMENT_QUALITE_NOM", "' (type �l�ment qualit�)'"), 
    					        $this->dbConn->CompareSql("equ.ELEMENT_QUALITE_ID", "is not", "NULL", $this->dbConn->getConcat("'�l�ment de qualit� '", "equ.ELEMENT_QUALITE_NOM"), "'�tat global'"))))." as LIB ".              
    					" from METHODE_EVALUATION meth".
              " LEFT JOIN TYPE_CLASSEMENT tc on tc.TYPE_CLASSEMENT_ID = meth.TYPE_CLASSEMENT_ID".
              " LEFT JOIN TYPE_ELEMENT_QUALITE tqual on tqual.TYPE_ELEMENT_QUALITE_ID = meth.TYPE_ELEMENT_QUALITE_ID".
              " LEFT JOIN ELEMENT_QUALITE equ on equ.ELEMENT_QUALITE_ID = meth.ELEMENT_QUALITE_ID ".
              ($bassin_id != -1 ? " where meth.BASSIN_ID = ".$bassin_id : "").
    					" order by LIB";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }   
  
   /**
   * @brief Retourne le ds de la table pointreseau pour savoir si un parmetre est utilis� ou pas
   *
   * @return Retourne un dataset
   */
  function getDsReseauPoint($bassin_id)
  {          
    $strSql = "select rpp.* from RESEAU_POINT_PARAM	rpp".
              " inner join RESEAU reso on reso.RESEAU_ID = rpp.RESEAU_ID and reso.BASSIN_ID = ".$bassin_id;    
    return $this->dbConn->initDataset( $strSql ) ;
  }   
  
     /**
   * @brief Retourne la session courante du bassin pass� en param�tre
   *
   * @return Retourne un dataset
   */
  function getDs_SessionByBassin($bassin_id, $bArchive="0", $idFirst=0, $idLast=-1, $session_id="")
  {          
    $strSql = "select sess.*, bas.*, ".
    					$this->dbConn->getConcat("annu.USER_NOM","' '","annu.USER_PRENOM")." as USER, ".   
    					$this->dbConn->GetDateFormat("DD/MM/YYYY", "SESSION_DATE_CREA" , false)." as SESSION_DATE, ".
    					$this->dbConn->GetDateFormat("DD/MM/YYYY", "SESSION_DATE_ARCHIVE" , false)." as SESSION_DATE_ARCHIVE, ".    					
    					$this->dbConn->CompareSql("sess.SESSION_ARCHIVE", "=", "1", "'oui'", "'non'")." as B_ARCHIVE".
    					" from SESSION_QUALITE sess".
    					" inner join BASSIN_HYDROGRAPHIQUE bas on sess.BASSIN_ID = bas.BASSIN_ID".
    					" left join ANNU_USER annu on sess.SESSION_USER_CREA = annu.USER_ID".
              " where sess.BASSIN_ID = ".$bassin_id.              
    					($bArchive != "" ? " and SESSION_ARCHIVE = ".$bArchive : "").
    					($session_id != "" ? " and SESSION_ID = ".$session_id : "").
    					" order by SESSION_DATE_CREA desc";
    return $this->dbConn->initDataset( $strSql , $idFirst, $idLast) ;
  }   
  
    /**
   * @brief Retourne la liste des groupe de r�seau
   *
   * @param $id  Identifiant d'un groupe
   *
   * @return Retourne un dataset
   */
  function getDs_listeGroupe($idFirst=0, $idLast=-1, $id=-2) 
  {          
    $strSql = "select * , GROUPE_ID as ID, GROUPE_NOM as LIB".
              " from RESEAU_GROUPE ".
             ($id!=-2 ? " where GROUPE_ID = ".$id : " order by GROUPE_NOM");
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }  
  
     /**
   * @brief permet de v�rifier si un group id est utilis� pour pouvoir l'effacer'
   *
   * @return Retourne un dataset
   */
   function getDs_GroupeUsed() 
  {          
    $strSql = "select distinct GROUPE_ID".
              " from RESEAU ";

    return $this->dbConn->initDataset( $strSql) ;
  }  
   /**
   * @brief Retourne la liste des bassins disponibles par user_id
   * @return Retourne un dataset
   */
  function getListBassinByUserForCombo($user_id)
  {    
    $strSql = "select BASSIN_NOM as nom, ".
      "BH.BASSIN_ID ".
      " from BASSIN_HYDROGRAPHIQUE BH ".      
      " LEFT JOIN ANNU_USER_BASSIN AUB ON BH.BASSIN_ID = AUB.BASSIN_ID".
      " WHERE USER_ID = ".$user_id.
      " order by nom";
    return $this->dbConn->initDataset( $strSql) ;
  }  

 /**
   * @brief Retourne la liste des fichiers utilis�s par bassin
   *
   *
   * @return Retourne un dataset
   */
  function getDs_listeFichierByBassin($bassin_id=-1, $idFirst=0, $idLast=-1) 
  {          
    $strSql = "select ELEMENT_QUALITE_DOC_PROTOCOLE as FICHIER, 'Document de protocole de l\'�l�ment de qualit�' as TYPE_DOC from ELEMENT_QUALITE".
    					" where 1=1 and ELEMENT_QUALITE_DOC_PROTOCOLE is not null and ELEMENT_QUALITE_DOC_PROTOCOLE != ''".
    					($bassin_id!=-1 ? " and BASSIN_ID =".$bassin_id : "").
    					" UNION ".
    					"select ELEMENT_QUALITE_DOC_METHODO as FICHIER, 'Document de m�thodo de l\'�l�ment de qualit�' as TYPE_DOC from ELEMENT_QUALITE".
    					" where 1=1 and ELEMENT_QUALITE_DOC_METHODO is not null and ELEMENT_QUALITE_DOC_METHODO != ''".
    					($bassin_id!=-1 ? " and BASSIN_ID =".$bassin_id : "").
    					" UNION ".
    					"select CLASSEMENT_COMPLEMENT_BILAN as FICHIER, 'Compl�ment d\'information du classement' as TYPE_DOC from CLASSEMENT_MASSE_EAU".
    					" where 1=1 and CLASSEMENT_COMPLEMENT_BILAN is not null and CLASSEMENT_COMPLEMENT_BILAN != ''".
    					($bassin_id!=-1 ? " and SESSION_ID = (select SESSION_ID from SESSION_QUALITE where SESSION_ARCHIVE=0 and BASSIN_ID = ".$bassin_id.")" : "").
    					" UNION ".
    					"select CLASSEMENT_DOC_REF as FICHIER, 'Document de r�f�rence du classement' as TYPE_DOC from CLASSEMENT_MASSE_EAU".
    					" where 1=1 and CLASSEMENT_DOC_REF is not null and CLASSEMENT_DOC_REF != ''".
    					($bassin_id!=-1 ? " and SESSION_ID = (select SESSION_ID from SESSION_QUALITE where SESSION_ARCHIVE=0 and BASSIN_ID = ".$bassin_id.")" : "").
    					" UNION ".
    					"select MASSE_DOC_DESC as FICHIER, 'Document de description de masse d\'eau' as TYPE_DOC from MASSE_EAU".
    					" where 1=1 and MASSE_DOC_DESC is not null and MASSE_DOC_DESC != ''".
    					($bassin_id!=-1 ? " and BASSIN_ID =".$bassin_id : "");
    					
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }    
  
  
  /**
   * @brief Retourne la liste des types d'objectifs environnementaux des masses d'eau 
   * @param $bassin_code
   * @param $session_id
   * @param $b_atteinte_obj : id de l'objectif d'atteinte
   * @return Retourne un dataset
   */
  function getDs_listeTypologieAtteinteObjectifEnv($bassin_code="", $session_id=-1) 
  {          
    $strSql = "select distinct descr.ATTEINTE_OBJECTIF_ID, ATTEINTE_OBJECTIF_CODE, ATTEINTE_OBJECTIF_LIBELLE, ATTEINTE_OBJECTIF_COLOR ".
              " from ATTEINTE_OBJECTIF_DESCRIPTION descr, MASSE_EAU".($session_id != -1 ? "_SESSION" : "")." me".       
              " where me.MASSE_B_ATTEINTE_OBJ = descr.ATTEINTE_OBJECTIF_ID ".
              ($bassin_code != '' ? " and me.BASSIN_ID in (select BASSIN_ID from BASSIN_HYDROGRAPHIQUE where BASSIN_CODE = '".$bassin_code."')" : "").
              ($session_id != -1 ? " and me.SESSION_ID = ".$session_id : "");

    return $this->dbConn->initDataset( $strSql ) ;
  }  
  
    /**
   * @brief Retourne la date de maj du classement de la masse en fonction de l'element de qualite et de la msse d'eau
   * @param $idElementQualite
   * @param $idMe
   * @return Retourne un dataset
   */
   function getDs_DateMAJByMasseQualiteId($idElementQualite="", $idMe=-1, $session_id =-1){
   	 
     $strWhere = "".($session_id != -1 ? " and SESSION_ID=".$session_id : "");
     
     if($idElementQualite!=""){// cas des type de classement uniquement 
       $strSql = "select ".$this->dbConn->GetDateFormat("DD/MM/YYYY", "CLASSEMENT_ME_DATE_MAJ" , false)." as CLASSEMENT_ME_DATE_MAJ from CLASSEMENT_MASSE_EAU".($session_id != -1 ? "_SESSION" : "")." where ELEMENT_QUALITE_ID = ".$idElementQualite." AND MASSE_ID =".$idMe.$strWhere;
     }else{// si id element et idMe
     	 $strSql = "select MAX(".$this->dbConn->GetDateFormat("DD/MM/YYYY", "CLASSEMENT_ME_DATE_MAJ" , false).") as CLASSEMENT_ME_DATE_MAJ from CLASSEMENT_MASSE_EAU".($session_id != -1 ? "_SESSION" : "")." where MASSE_ID =".$idMe.$strWhere;
     }
     return $this->dbConn->initDataset($strSql) ;
    
   }
  
  
   /**
   * @brief Retourne la date de maj la plus recente que ce soit au niveau des reseau ou element de qualit� en fonction du bassin_id
   * @param $idElementQualite
   * @param $idMe
   * @return Retourne un dataset
   */
   function getDs_DateMAJByBassinId($idBassin = -1, $session_id =-1){
     
     $strWhere = "".($session_id != -1 ? " and SESSION_ID=".$session_id : "");
                
     if($idBassin!=-1){
       $strSql = "select MAX(MAX_DATE_MAJ) as DATE_MAJ_RECENTE from (".
          " select MAX(".$this->dbConn->GetDateFormat("DD/MM/YYYY", "CLASSEMENT_ME_DATE_MAJ" , false).") as MAX_DATE_MAJ from CLASSEMENT_MASSE_EAU".($session_id != -1 ? "_SESSION" : "")." cme" .
          " left join MASSE_EAU me on me.MASSE_ID = cme.MASSE_ID" .
          " where me.BASSIN_ID= ".$idBassin.$strWhere.
          " UNION ".
          " select MAX(".$this->dbConn->GetDateFormat("DD/MM/YYYY", "RESEAU_DATE_MAJ" , false).") as MAX_DATE_MAJ from RESEAU_POINT_PARAM".($session_id != -1 ? "_SESSION" : "")." rpp ".
          " left join RESEAU rs on rs.RESEAU_ID = rpp.RESEAU_ID".
          " where rs.BASSIN_ID= ".$idBassin.$strWhere.") as selectionDateMax";
   
     }
     return $this->dbConn->initDataset($strSql) ;
    
   }
  
      /**
   * @brief Retourne la date de maj du classement de la masse en fonction de l'id du classement
   * @param $classementId
   * @return Retourne un dataset
   */
   function getDs_DateMAJByClassementId($classementId){

     
     if($classementId!=""){// cas des type de classement uniquement 
       $strSql = "select ".$this->dbConn->GetDateFormat("DD/MM/YYYY", "CLASSEMENT_ME_DATE_MAJ" , false)." as CLASSEMENT_ME_DATE_MAJ from CLASSEMENT_MASSE_EAU where CLASSEMENT_ID = ".$classementId;
     }
     return $this->dbConn->initDataset($strSql) ;
    
   }
  
  
  
  
}
?>