<?php

/**
 * @brief Classe regroupant des fonctionnalit�s li�e � la repr�sentation de la qualit� des masses d'eau
 */
class AlkMasseEau
{
  /** */
  var $codeMe;
  var $data;
  var $oQuery;
  var $tabClassement;
  var $idSession;

  /**
   * @brief Constructeur par d�faut
   *
   */
  function AlkMasseEau($codeMe, &$oQuery, $idMe="", $idSession=-1)
  {
    $this->codeMe = $codeMe;
    $this->idSession = $idSession;
    $this->oQuery = $oQuery;
    $dsMe = $this->oQuery->getDs_MasseEauById($idMe, $codeMe, $idSession);
		if ($drMe = $dsMe->getRowIter()) {
			$this->data = $drMe;
			$this->codeMe = $drMe->getValueName("CODE");		
		}
		
		//D�termine tous les �tats de la masse d'eau
		$this->setEtatQualite();		
  }

  /**
   * @brief retourne le code html de la l�gende des �tats de qualit� d'une masse d'eau
   */
  function getLegende($idTypeClassement="", $typeLegende="ulTabLeg")
  {	
    $dsEtat = $this->oQuery->getDs_listeEtat(0, -1, "", "", "", $this->idSession); 
    while ($drEtat = $dsEtat->getRowIter() ){
			//$strTypeClassement = strtr($drEtat->getValueName("CLASSEMENT"), "e����a���u���i���o�c�", "eeeeeaaaauuuuiiiooocc");
			if ($idTypeClassement != "" && $idTypeClassement==$drEtat->getValueName("TYPE_CLASSEMENT_ID") || $idTypeClassement=="")
				$tabLegende["Etat ".$drEtat->getValueName("CLASSEMENT_LEG")][] = array("LIB"=>$drEtat->getValueName("LIB"), 
																								"COLOR"=>$drEtat->getValueName("ETAT_COULEUR"),
                                                "VALEUR"=>$drEtat->getValueName("ETAT_VALEUR"));
		}
    $strHtmlLeg ="<div id='legendeEtat'".($idTypeClassement != "" ? " class='legendeTypeUnique'" : "").">";   
    $iCol = 0;
    foreach ($tabLegende as $strTypeClassement=>$tabEtat){			
      $iCol ++;			
      $strHtmlLeg .= "<ul class='".$typeLegende.($iCol > 1 ? " droite" : "")."'><li class='liTabLeg txt'>".$strTypeClassement."<ul class='ulLegende'>";
      foreach ($tabEtat as $tabLeg){
		$strHtmlLeg .= "<li class='liLeg'><div class='floatleft'>".$tabLeg["LIB"]."</div><div class='divLeg' style='background-color:".$tabLeg["COLOR"].";'></div></li>";
      }			
      $strHtmlLeg .= "</ul></li></ul>";
  	}
    $strHtmlLeg .= "</div>";   
    return $strHtmlLeg;
  }
  
  /**
   * @brief retourne le code html de la l�gende des motifs et statuts
   */
  function getLegendeMotif($bassin_id)
  {
    $strExtSession = ($this->idSession != -1 ? "_SESSION" : "");
    $strWhereSession = ($this->idSession != -1 ? " and SESSION_ID = ".$this->idSession : "");
    
		$dsEtat = $this->oQuery->getTableForCombo("ETAT_MOTIF".$strExtSession, "MOTIF", true, true, "BASSIN_ID=".$bassin_id.$strWhereSession);
    while ($drEtat = $dsEtat->getRowIter() ){
			$tabLegende["Motifs"][] = array("LIB"=>$drEtat->getValueName("nom"));
		}
  	$dsStatut = $this->oQuery->getTableForCombo("CLASSEMENT_STATUT".$strExtSession, "STATUT", true, true, "1=1".$strWhereSession);
		while ($drEtat = $dsStatut->getRowIter() ){
			$tabLegende["Statuts"][] = array("LIB"=>$drEtat->getValueName("nom"));
		}

    $strHtmlLeg = "<div id='legendeMotif' class='divLegendeMotif'>";
    foreach ($tabLegende as $strTypeClassement=>$tabEtat){
      $strHtmlLeg .= "<ul  valign='top' class='txt legende'>";
      foreach ($tabEtat as $tabLeg){
        if (strpos($tabLeg["LIB"], "Fin") === false)
          $strHtmlLeg .= "<li>".$tabLeg["LIB"]."</li>";       
      }     
      $strHtmlLeg .= "</ul>";
    }
    $strHtmlLeg .= "</div>";
		
    return $strHtmlLeg;
  }  
 
   /**
   * @brief retourne le code html de la l�gende des motifs et statuts
   */
  function getLegendeIndConfiance($bGlobal = true)
  {

    $tabLegende[] = ($bGlobal ? "Niveau de confiance" : "Degr� de fiabilit�");
    $tabLegende[] = "1: faible";
    $tabLegende[] = "2: moyen";
    $tabLegende[] = "3: �lev�";
    $tabLegende[] = "gris : pas d'information";
    
    $strHtmlLeg = "<div id='legendeIndConfiance' class='divLegendeIndConfiance'>";
    $strHtmlLeg .= "<ul  valign='top' class='txt legende'>";
    $strHtmlLeg .= "<li class='liTabLeg txt'>".$tabLegende[0];
    $strHtmlLeg .= "<ul class='ulLegende'>";    
    foreach ($tabLegende as $i=>$lib){  
        if ($i>0)     
          $strHtmlLeg .= "<li class='liLeg'>".$lib."</li>";       
      }     
    $strHtmlLeg .= "</ul></li></ul>";
    $strHtmlLeg .= "</div>";
    return $strHtmlLeg;
  }
  
   /**
   * @brief retourne le libell� de l'indice
   */
  function getLibelleIc($ic)
  {
    $str = "";
    switch ($ic){
      case 1:
        $str = "(faible)";
        break;
      case 2:
        $str = "(moyen)";
        break;
      case 3:
        $str = "(�lev�)";
        break;
    }    
    return $str;
  } 
 
  /**
   * @brief retourne le code html de la l�gende des motifs et statuts
   * @brief 100516 DMI ajout du param optionnel bRestrictMe qui permet de restreindre la l�gende aux r�seaux pr�sents sur la masse d'eau courante
   */
  function getLegendeReseaux($iNbCol=2, $bassin_id="", $bRestrictMe = "")
  {
    $bassin_code = "";
    $tabLegende = array();
    
    $dsBassin = $this->oQuery->getDs_bassin("", $bassin_id);
		if ($drBassin = $dsBassin->getRowIter())
		  $bassin_code = $drBassin->getValueName("BASSIN_CODE");
			    
		$dsReseaux = $this->oQuery->getReseauForCombo(true, 0, -1, $bassin_id, $this->idSession, 
		                                              ($bRestrictMe == 1 && isset($this->data) && substr(get_class($this->data),0,2)== "Dr" ? $this->data->getValueName("ID") : ""));

    while ($drReseau = $dsReseaux->getRowIter() ){
			$tabLegende["R�seaux"][] = array("LIB"=>$drReseau->getValueName("RESEAU_NOM"),
			                                 "SYMBOLE"=> 
			                                    ($drReseau->getValueName("RESEAU_IMG_SYMB")!=""  
                                        	? "<img class='classImgCarte classImgCarteSymb' src=\"".ALK_SIALKE_URL."upload/carte/".$bassin_code."/".$drReseau->getValueName("RESEAU_IMG_SYMB")."\"/>"                                                                      
			                                 		: "<div class='divLegReseauME' style='background:".$drReseau->getValueName("RESEAU_COULEUR")." url(".ALK_SIALKE_URL."media/imgs/gen/pictos/legende/".$drReseau->getValueName("RESEAU_SYMBOLE").".gif) center no-repeat' ></div>"));
		}
		
    $strHtmlLeg = "<div id='legendeReseau' class='classDivLegend'>";
    if (count($tabLegende) > 0){
      foreach ($tabLegende as $strTypeClassement=>$tabEtat){  
        $tabNbCol = array("0"=>0); 
        for ($i=1;$i<=$iNbCol;$i++){
          $tabNbCol[$i] = ($i==1 ? count($tabEtat) / $iNbCol : count($tabEtat) - (count($tabEtat) / $iNbCol));        
          $strHtmlLeg .= "<ul valign='top' class='txt'>";  
          for($j=0;$j<$tabNbCol[$i];$j++){
            $strHtmlLeg .= "<li><div class='floatright'>".$tabEtat[$tabNbCol[$i-1]+$j]["SYMBOLE"]."</div>".$tabEtat[$tabNbCol[$i-1]+$j]["LIB"]."</li>";
          
          }     
          $strHtmlLeg .= "</ul>";         
        }     
      }
    }
  	$strHtmlLeg .= "</div>";                
  	return $strHtmlLeg;
  }    
 
  /**
   * @brief retourne le code html de la l�gende des typologies des masses d'eau cotieres ou de transition
   */
  function getLegendeTypo($strTypeMe="C", $codeBassin)
  {
    $tabLegende = array();
 		$dsTypeMe = $this->oQuery->getDs_listeTypologieMasseEau($strTypeMe, $codeBassin, $this->idSession);
    while ($drTypeMe = $dsTypeMe->getRowIter()){
			$tabLegende["Typo"][] = array("LIB"=>$drTypeMe->getValueName("MASSE_DESC_CODE")." - ".$drTypeMe->getValueName("MASSE_DESC_LIBELLE"), 
																		"COLOR"=>$drTypeMe->getValueName("MASSE_DESC_COLOR"));			
		}		
		$strHtmlLeg = "<table style='width:100%;padding:5px;margin:0;'><tr>";
		foreach ($tabLegende as $strTypeClassement=>$tabTypo){			
			$strHtmlLeg .= "<td width='50%' valign='top' class=\"txt\"><table class='legende'>";
			foreach ($tabTypo as $tabLeg){
				$strHtmlLeg .= "<tr><td>".$tabLeg["LIB"]."</td><td><div class='divLeg' style='top:0px;background-color:".$tabLeg["COLOR"].";'></div></td></tr>";
			}			
			$strHtmlLeg .= "</table></td>";
  	}
  	$strHtmlLeg .= "</tr></table>";
  	return $strHtmlLeg;
  }  
  
  
    
    /**
   * @brief retourne le code html de la l�gende des typologies des objectifs environnementaux des masses d'eau 
   */
  function getLegendeTypoAtteinteObj($codeBassin)
  {
    $tabLegende = array();
    $dsTypeMe = $this->oQuery->getDs_listeTypologieAtteinteObjectifEnv($codeBassin, -1);
    while ($drTypeMe = $dsTypeMe->getRowIter()){
      $tabLegende["Typo"][] = array("LIB"=>$drTypeMe->getValueName("ATTEINTE_OBJECTIF_LIBELLE"), 
                                    "COLOR"=>$drTypeMe->getValueName("ATTEINTE_OBJECTIF_COLOR"));      
    }   
    $strHtmlLeg = "<table style='width:100%;padding:5px;margin:0;'><tr>";
    foreach ($tabLegende as $strTypeClassement=>$tabTypo){      
      $strHtmlLeg .= "<td width='50%' valign='top' class=\"txt\"><table class='legende'>";
      foreach ($tabTypo as $tabLeg){
        $strHtmlLeg .= "<tr><td>".$tabLeg["LIB"]."</td><td><div class='divLeg' style='top:0px;background-color:".$tabLeg["COLOR"].";'></div></td></tr>";
      }     
      $strHtmlLeg .= "</table></td>";
    }
    $strHtmlLeg .= "</tr></table>";
    return $strHtmlLeg;
  }  
  
  
    /**
   * @brief mise � jour des �tats de qualit� pour la masse d'eau
   * 				issus de la base de donn�es pour les �tats par type d'�l�ments de qualit� et par �l�ments de qualit�
   * 				calcul�s (sur le mode du plus d�classant) pour les �tats 
   * 					- par type de classement (chimique et �cologique) = le plus d�classant des types d'�l�ments de qualit� de ce type de classement
   * 					- global = le plus d�classant des types de classements chimique et �cologique 
   */
  function setEtatQualite($iNiveau="", $bAffDoublons=true, $bSupprEltQualSpec=false){

  	$dsEtat = $this->oQuery->getDs_listeEtatMasseEau($this->codeMe, $iNiveau, $this->idSession);
    $strLastElementNiv1 = ""; 
		while ($drEtat = $dsEtat->getRowIter()){
		  
		  $bassin_id = $drEtat->getValueName("BASSIN_ID");
		  $strMethode = "";
			if ($drEtat->getValueName("TYPE_ETAT") == "1_TYPE_ELEMENT_QUALITE") {
			  
				//Classement du type d'�l�ment de qualit� pour la masse d'eau
				
				$dsMethode = $this->oQuery->getDs_ficheMethode("", $drEtat->getValueName("TYPE_ELEMENT_QUALITE_ID"), "", $bassin_id, $this->idSession);
				if ($drMethode = $dsMethode->getRowIter())
				  $strMethode = $drMethode->getValueName("METHODE_LIBELLE");
				
				$tabClassement[$drEtat->getValueName("TYPE_CLASSEMENT_LIBELLE")][$drEtat->getValueName("TYPE_ELEMENT_QUALITE_NOM")] = array("ID"=>$drEtat->getValueName("TYPE_ELEMENT_QUALITE_ID"),
																																																																		"ETAT"=>$drEtat->getValueName("ETAT_LIBELLE"),
																																																																		"ETAT_ID"=>$drEtat->getValueName("ETAT_ID"), 
																																																																	  "COULEUR"=>$drEtat->getValueName("ETAT_COULEUR"), 
																																																																		"VALEUR"=>$drEtat->getValueName("ETAT_VALEUR"),
																																																																	  "STATUT"=>$drEtat->getValueName("STATUT_LIBELLE"),
																																																																		"STATUT_ID"=>$drEtat->getValueName("STATUT_ID"),
																																																																		"MOTIF_ID"=>$drEtat->getValueName("MOTIF_ID"),
																																																																		"DATE"=>$drEtat->getValueName("CLASSEMENT_DATE"),
																																																																		"TYPE_CLASSEMENT_ID"=>$drEtat->getValueName("TYPE_CLASSEMENT_ID"),
																																																																		"BILAN"=>$drEtat->getValueName("CLASSEMENT_BILAN"),
																																																																		"COMPLEMENT_BILAN"=>$drEtat->getValueName("CLASSEMENT_COMPLEMENT_BILAN"),
																																																																		"INDICE_CONFIANCE"=>$drEtat->getValueName("CLASSEMENT_INDICE_CONFIANCE"),
																																																																		"CLASSEMENT_ID"=>$drEtat->getValueName("CLASSEMENT_ID"),																																																																									
																																																																		"METHODE"=>$strMethode,
																																																																		"ELEMENTS"=>array());
				
			} else if ($drEtat->getValueName("TYPE_ETAT") == "2_ELEMENT_QUALITE") {
				//Classement de l'�l�ment de qualit� pour la masse d'eau 		
				 
			  //Si tous les niveaux sont demand�s, on supprime les niveaux 2 qui ont les m�mes intitul�s que le niveau 1 correspondant.
			  if ($iNiveau == "" && $drEtat->getValueName("ELEMENT_QUALITE_NIVEAU")==2 && $drEtat->getValueName("ELEMENT_QUALITE_NOM") == $strLastElementNiv1 && !$bAffDoublons)
				  continue;
				$strLastElementNiv1 = ($drEtat->getValueName("ELEMENT_QUALITE_NIVEAU") == 1 ? $drEtat->getValueName("ELEMENT_QUALITE_NOM") : $strLastElementNiv1);

				$dsMethode = $this->oQuery->getDs_ficheMethode("", "", $drEtat->getValueName("ELEMENT_QUALITE_ID"), $bassin_id, $this->idSession);
			  if ($drMethode = $dsMethode->getRowIter())
				  $strMethode = $drMethode->getValueName("METHODE_LIBELLE");
				
				if ($bSupprEltQualSpec && $drEtat->getValueName("B_AFF")==0)
				  continue;
				  
				$tabClassement[$drEtat->getValueName("TYPE_CLASSEMENT_LIBELLE")][$drEtat->getValueName("TYPE_ELEMENT_QUALITE_NOM")]["ELEMENTS"]["BYID"]["ID_".$drEtat->getValueName("ELEMENT_QUALITE_ID")] = array("ID"=>$drEtat->getValueName("ELEMENT_QUALITE_ID"),
																																																																									"NOM"=>$drEtat->getValueName("ELEMENT_QUALITE_NOM"), 
																																																																									"NIVEAU"=>$drEtat->getValueName("ELEMENT_QUALITE_NIVEAU"),
																																																																									"ETAT"=>$drEtat->getValueName("ETAT_LIBELLE"),
																																																																									"ETAT_ID"=>$drEtat->getValueName("ETAT_ID"), 
																																																																									"COULEUR"=>$drEtat->getValueName("ETAT_COULEUR"), 
																																																																									"STATUT"=>$drEtat->getValueName("STATUT_CODE"),
																																																																									"STATUT_ID"=>$drEtat->getValueName("STATUT_ID"),					
																																																																									"MOTIF"=>$drEtat->getValueName("MOTIF_CODE"),
																																																																									"MOTIF_ID"=>$drEtat->getValueName("MOTIF_ID"),
																																																																									"DATE"=>$drEtat->getValueName("CLASSEMENT_DATE"),
																																																																									"TYPE_CLASSEMENT_ID"=>$drEtat->getValueName("TYPE_CLASSEMENT_ID"),
																																																																									"BILAN"=>$drEtat->getValueName("CLASSEMENT_BILAN"),
																																																																									"COMPLEMENT_BILAN"=>$drEtat->getValueName("CLASSEMENT_COMPLEMENT_BILAN"),
																																																																									"DOC_REF"=>$drEtat->getValueName("CLASSEMENT_DOC_REF"),
																																																																									"INDICE_CONFIANCE"=>$drEtat->getValueName("CLASSEMENT_INDICE_CONFIANCE"),
																																																																									"FICHE_SYNTHESE"=>$drEtat->getValueName("ELEMENT_QUALITE_URL_FICHE"),
																																																																									"CLASSEMENT_ID"=>$drEtat->getValueName("CLASSEMENT_ID"),
																																																																									"B_AFF"=>$drEtat->getValueName("B_AFF"),
																																																																									"METHODE"=>$strMethode);
				
				$tabClassement[$drEtat->getValueName("TYPE_CLASSEMENT_LIBELLE")][$drEtat->getValueName("TYPE_ELEMENT_QUALITE_NOM")]["ELEMENTS"]["BYNUM"][] = array("ID"=>$drEtat->getValueName("ELEMENT_QUALITE_ID"),
																																																																									"NOM"=>$drEtat->getValueName("ELEMENT_QUALITE_NOM"), 
																																																																									"NIVEAU"=>$drEtat->getValueName("ELEMENT_QUALITE_NIVEAU"),
																																																																									"ETAT"=>$drEtat->getValueName("ETAT_LIBELLE"),
																																																																									"ETAT_ID"=>$drEtat->getValueName("ETAT_ID"), 
																																																																									"COULEUR"=>$drEtat->getValueName("ETAT_COULEUR"), 
																																																																									"STATUT"=>$drEtat->getValueName("STATUT_CODE"),
																																																																									"STATUT_ID"=>$drEtat->getValueName("STATUT_ID"),
																																																																									"MOTIF"=>$drEtat->getValueName("MOTIF_CODE"),
																																																																									"MOTIF_ID"=>$drEtat->getValueName("MOTIF_ID"),
																																																																									"DATE"=>$drEtat->getValueName("CLASSEMENT_DATE"),
																																																																									"TYPE_CLASSEMENT_ID"=>$drEtat->getValueName("TYPE_CLASSEMENT_ID"),
																																																																									"BILAN"=>$drEtat->getValueName("CLASSEMENT_BILAN"),
																																																																									"COMPLEMENT_BILAN"=>$drEtat->getValueName("CLASSEMENT_COMPLEMENT_BILAN"),
																																																																									"DOC_REF"=>$drEtat->getValueName("CLASSEMENT_DOC_REF"),
																																																																									"INDICE_CONFIANCE"=>$drEtat->getValueName("CLASSEMENT_INDICE_CONFIANCE"),
																																																																									"FICHE_SYNTHESE"=>$drEtat->getValueName("ELEMENT_QUALITE_URL_FICHE"),
																																																																									"CLASSEMENT_ID"=>$drEtat->getValueName("CLASSEMENT_ID"),
																																																																									"B_AFF"=>$drEtat->getValueName("B_AFF"),
																																																																									"METHODE"=>$strMethode);
																																																																													
		} else if ($drEtat->getValueName("TYPE_ETAT") == "3_TYPE_CLASSEMENT") {
		  $tabClassement[$drEtat->getValueName("TYPE_CLASSEMENT_LIBELLE")]["ETAT"]["INDICE_CONFIANCE"] = $drEtat->getValueName("CLASSEMENT_INDICE_CONFIANCE");
		  $tabClassement[$drEtat->getValueName("TYPE_CLASSEMENT_LIBELLE")]["ETAT"]["ID"] = $drEtat->getValueName("TYPE_CLASSEMENT_ID");
		  $tabClassement[$drEtat->getValueName("TYPE_CLASSEMENT_LIBELLE")]["ETAT"]["CLASSEMENT_ID"] = $drEtat->getValueName("CLASSEMENT_ID");
		} else if ($drEtat->getValueName("TYPE_ETAT") == "4_TYPE_GLOBAL") {
		  $tabClassement["Global"]["ETAT"]["INDICE_CONFIANCE"] = $drEtat->getValueName("CLASSEMENT_INDICE_CONFIANCE");
		  $tabClassement["Global"]["ETAT"]["CLASSEMENT_ID"] = $drEtat->getValueName("CLASSEMENT_ID");
		}
	}

	  //On applique la m�thode g�n�rale du plus d�classant pour les �tats interm�diaires (biolo, physico-chimique, hydro, ecolo + global)
		$etatGlobal = -1;	
		$etatGlobalCouleur = "";
		$etatGlobalLibelle = "";
				
		foreach ($tabClassement as $nomClassement=>$tabTypeElementQualite) {

		  if ($nomClassement != "Global"){
  			$etatDeclassant = -1;
  			$etatCouleur = "";	
  			$etat_libelle = "";		
  			$strMethode = "";
  			$iIcDeclassant = 100;  			
  			
    		foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
  			  if ($strMethode == "" && $nomTypeElement != "ETAT"){
  			    $dsMethode = $this->oQuery->getDs_ficheMethode($tabEtat["TYPE_CLASSEMENT_ID"], "", "", $bassin_id, $this->idSession);
  			    if ($drMethode = $dsMethode->getRowIter())
  				    $strMethode = $drMethode->getValueName("METHODE_LIBELLE");
  			  }
    		}
    		
    		reset($tabTypeElementQualite);
  			if ($nomClassement != "�cologique"){
  			  //M�thode du plus d�classant
    			foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){
    			  if ($nomTypeElement != "ETAT") {			 
      				if ($tabEtat["VALEUR"] > $etatDeclassant){
      					$etatCouleur = $tabEtat["COULEUR"];
      					$etatLibelle = $tabEtat["ETAT"];
      					$etatDeclassant = $tabEtat["VALEUR"];			      					
      				}
      				if ($tabEtat["INDICE_CONFIANCE"]!="" &&  $tabEtat["INDICE_CONFIANCE"] < $iIcDeclassant)      					
    			        $iIcDeclassant = $tabEtat["INDICE_CONFIANCE"];		        				    			
    			  }    			
    			}
  			}	else {
  			  
  			  //Indice de confiance //M�thode du plus d�classant
  			  foreach ($tabTypeElementQualite as $nomTypeElement=>$tabEtat){  
    			  if ($nomTypeElement != "ETAT") {			       				
    			    if ($tabEtat["INDICE_CONFIANCE"]!="" &&  $tabEtat["INDICE_CONFIANCE"] < $iIcDeclassant)      					
    			      $iIcDeclassant = $tabEtat["INDICE_CONFIANCE"];
    			  }    			
    			}
  			  
  			  //Prise en compte du cas particulier du calcul de l'�tat ecologique (08/2010)
      		//Si etat biolo = tres bon + etat physico = tres bon ou inconnu + etat hydro = tres bon ou inconnu ==> ecolo = tres bon
      		//Si etat biolo + etat physico = tres bon ou bon ==> ecolo = bon
      		//Si etat biolo = bon ou tres bon + etat physico < bon ==> ecolo = moyen
      		//Si etat biolo <= moyen ==> ecolo = biolo    		
  			  if (($tabTypeElementQualite["Etat biologique"]["VALEUR"] == 1 || $tabTypeElementQualite["Etat biologique"]["VALEUR"]<=0) && 			  
  			      ($tabTypeElementQualite["Etat physico-chimique"]["VALEUR"] == 1 || $tabTypeElementQualite["Etat physico-chimique"]["VALEUR"]<=0) && 
  			      ($tabTypeElementQualite["Etat hydromorphologique"]["VALEUR"] == 1 || $tabTypeElementQualite["Etat hydromorphologique"]["VALEUR"] == 0)){
           		  //Si etat biolo = tres bon + etat physico = tres bon + etat hydro = tres bon ou inconnu ==> ecolo = tres bon
  			        $etatCouleur = $tabTypeElementQualite["Etat biologique"]["COULEUR"];
    					  $etatLibelle = $tabTypeElementQualite["Etat biologique"]["ETAT"];
    					  $etatDeclassant = $tabTypeElementQualite["Etat biologique"]["VALEUR"];	  					    					 
  			      } elseif ($tabTypeElementQualite["Etat biologique"]["VALEUR"]<=2 && $tabTypeElementQualite["Etat biologique"]["VALEUR"]>0 && $tabTypeElementQualite["Etat physico-chimique"]["VALEUR"]<=2) {
  			        //Si etat biolo + etat physico = tres bon ou bon ==> ecolo = bon
  			        $etatCouleur = "#6ab396";
    					  $etatLibelle = "Bon";
    					  $etatDeclassant = 2;  					
    				 } elseif ($tabTypeElementQualite["Etat biologique"]["VALEUR"]<=2  && $tabTypeElementQualite["Etat biologique"]["VALEUR"]>0 && $tabTypeElementQualite["Etat physico-chimique"]["VALEUR"]>2){
  			        //Si etat biolo = bon ou tres bon + etat physico < bon ==> ecolo = moyen
  			        $etatCouleur = "#ffe075";
    					  $etatLibelle = "Moyen";
    					  $etatDeclassant = 3;  					  		  
  			      } elseif ($tabTypeElementQualite["Etat biologique"]["VALEUR"]>2){
  			        //Si etat biolo <= moyen ==> ecolo = biolo
  			        $etatCouleur = $tabTypeElementQualite["Etat biologique"]["COULEUR"];
    					  $etatLibelle = $tabTypeElementQualite["Etat biologique"]["ETAT"];
    					  $etatDeclassant = $tabTypeElementQualite["Etat biologique"]["VALEUR"];  					 				 
  			      }	elseif ($tabTypeElementQualite["Etat biologique"]["VALEUR"]<=0){
  			        //Si etat biolo inconnu ou non pertinent et physico chimique different de tres bon (sinon 1er cas)
  			        $etatCouleur = $tabTypeElementQualite["Etat physico-chimique"]["COULEUR"];
    					  $etatLibelle = $tabTypeElementQualite["Etat physico-chimique"]["ETAT"];
    					  $etatDeclassant = $tabTypeElementQualite["Etat physico-chimique"]["VALEUR"];  					 				 
  			      }			  		      
  			}
  			
  			if ($etatDeclassant > $etatGlobal){
  				$etatGlobal = $etatDeclassant;
  				$etatGlobalCouleur = $etatCouleur;
  				$etatGlobalLibelle = $etatLibelle;
  			}					  			
  			$tabClassement[$nomClassement]["ETAT"]["ETAT"] = $etatLibelle;
		    $tabClassement[$nomClassement]["ETAT"]["COULEUR"] = $etatCouleur;
		    $tabClassement[$nomClassement]["ETAT"]["VALEUR"] = $etatDeclassant;
		    $tabClassement[$nomClassement]["ETAT"]["METHODE"] = $strMethode; 	
		    $tabClassement[$nomClassement]["ETAT"]["ETAT"] = $etatLibelle;		
		    $tabClassement[$nomClassement]["ETAT"]["INDICE_CONFIANCE"] = ($iIcDeclassant == 100 ? 0 : $iIcDeclassant); 				
  		}
		}
								
		$dsMethode = $this->oQuery->getDs_ficheMethode("", "", "", $bassin_id, $this->idSession);
		if ($drMethode = $dsMethode->getRowIter())
			$strMethode = $drMethode->getValueName("METHODE_LIBELLE");
		  $tabClassement["Global"]["ETAT"]["ETAT"] = $etatGlobalLibelle;
		  $tabClassement["Global"]["ETAT"]["COULEUR"] = $etatGlobalCouleur;
		  $tabClassement["Global"]["ETAT"]["VALEUR"] = $etatGlobal;		 
      $tabClassement["Global"]["ETAT"]["METHODE"] = $strMethode; 	 																 
		
		$this->tabClassement = $tabClassement;	
	}			

  /**
   * @brief retourne le tableau de l'�tat global de la masse d'eau 
   */
  function getEtatQualiteGlobal(){
  	return $this->tabClassement["Global"]["ETAT"];  	
  }
  /**
   * @brief retourne le tableau des �tats
   */
  function getEtatQualite(){  	
  	return $this->tabClassement;  	
  }
  
}

?>