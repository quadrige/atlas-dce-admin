<?php
include_once("classes/alkappliatlas.class.php");

/**
 * @brief Classe de l'application concours
 *        Classe regroupant des fonctionnalit�s de la rubrique param�trage des concours
 */
class AlkAppliAtlas_masse extends AlkAppliAtlas
{

  /**
   * @brief Constructeur par d�faut
   *
   */
  function AlkAppliAtlas_masse($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet)
  {
    parent::AlkAppliAtlas($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet);
  }

  /**
   * @brief M�thode virtuelle qui retourne un tableau de boutons htmllink
   *        plac� sur la droite des onglets
   * 
   * @param 
   * @return Retourne un array
   */
  function getTabCtrlBt()
  {
    $iCptBt = 0;
    $tabBt = array();

    return $tabBt;
  }

  /**
   * @brief M�thode virtuelle, retourne un tableau contenant les informations sur les sous onglets
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un array
   */
  function getTabSubSheet()
  {
    global $tabStrType;

    $tabSubSheet = array();

    $strParam = "iSheet=".ALK_SHEET_MASSE_EAU;
    
    $i = 0;                                                 
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_ME,
                               "text"     => "Masses&nbsp;d'eau",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_ME,
                               "title"    => "Liste des masses d'eau");      
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_IMPORT,
                               "text"     => "Import&nbsp;donn�es masses d'eau",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_IMPORT."&iModeSSheet=".ALK_MODE_SSHEET_FORM,
                               "title"    => "Import&nbsp;donn�es masses d'eau");                                                                                       
    return $tabSubSheet;
  }

  /**
   * @brief M�thode virtuelle, retourne le contenu html du corps de l'onglet s�lectionn�
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un string : code html des sous onglets
   */
  function getHtmlBodySheet()
  {
    $strHtml = "";
    switch( $this->iSSheet ) {            
		case ALK_SSHEET_ME:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheDico();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementDico();
      break;		
    case ALK_SSHEET_IMPORT:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheImport();      
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementImport();
      break;   		  
    default:
      break;
    }
    return $strHtml;
  }

  /**
   * @brief Affiche la liste des options pour une voie de concours
   *
   * @return Retourne un string
   */
  function getHtmlListeDico()
  {    
  	$iErr = Request("err", REQ_GET, "0", "is_numeric");
    $nbEltParPage = Request("nbEltParPage", REQ_POST_GET, 20, "is_numeric");
    $page = Request("page", REQ_GET, 1, "is_numeric");
    
    $strParam = "iSheet=".$this->iSheet.
      "&nbEltParPage=".$nbEltParPage."&page=".$page."&iSSheet=".$this->iSSheet."&iModeSSheet=";
   	
   	// liste des �l�ments du dictionnaire s�lectionn�
    $cpt = 0;
    $tabPage = array();
    $iFirst = ($page-1)*$nbEltParPage;
    $iLast = $iFirst+$nbEltParPage-1;
      
    switch ($this->iSSheet) {
    	case ALK_SSHEET_ME :
    		$strTitre = "masses d'eau";
    		$dsDico = $this->oQuery->getDs_listeMasseEau($iFirst, $iLast, $_SESSION["idBassin"]);
    		break;     		    
    } 
    
    $oBtAdd = new HtmlLink("01_page_form.php?iMode=1&".$strParam.ALK_MODE_SSHEET_FORM."&page=".$page,
                           "Ajouter un nouvel �l�ment � la liste",
                           "ajouter.gif", "ajouter_rol.gif");
    
    $nbElt = $dsDico->iCountTotDr;

    $strHtml = $this->_getFrameTitleSheet("Gestion des ".$strTitre).    
      "<script language='javascript' src='lib/lib_atlas.js'></script>".  
      "<form name='formDico' action='' method='post'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='450' height='10'></td>".      
      ($this->iSSheet == ALK_SSHEET_PARAMETRE ? "<td width='140'></td><td width='140'></td>"
       : "").
      "<td width='120'></td>".      
      "</tr>".
      ( $iErr == "1"
        ? "<tr><td colspan='2' class='divContenuMsgErr' align='center'>".
        "Impossible de supprimer cet enregistrement car il est encore utilis�.<br><br></td></tr>"
        : "").
      ( $iErr == "2"
        ? "<tr><td colspan='2' class='divContenuMsgErr' align='center'>".
        "Impossible de cr�er cette masse d'eau, une masse d'eau existe d�j� avec le m�me code (dans ce bassin ou un autre).<br><br></td></tr>"
        : "").  
      "<tr class='trEntete1'>".
      "<td class='tdEntete1' align='left'><div class='divTabEntete1'>Liste des ".
      $strTitre."&nbsp;&nbsp;".$nbElt." enregistrement".($nbElt>1 ? "s" : "")."</div></td>".      
      ($this->iSSheet == ALK_SSHEET_PARAMETRE ? "<td class='tdEntete1' align='center'><div class='divTabEntete1'>R�seau</div></td>" .
      																					"<td class='tdEntete1' align='center'><div class='divTabEntete1'>Support</div></td>"
       : "").
      "<td class='tdEntete1' align='center'>".$oBtAdd->getHtml()."</td>".
      "</tr>";
   
    while( $drDico = $dsDico->getRowIter() ) {
      $id = $drDico->getValueName("ID");
      $strLib = $drDico->getValueName("LIB");      
      
      $strLib = array(wordwrap($strLib, 100, "<br>"), 
                      "01_page_form.php?iMode=2&".$strParam.ALK_MODE_SSHEET_FORM.
                      "&id=".$id."&page=".$page);

      $iPage = ( $nbElt-1 <= ($page-1)*$nbEltParPage ? ($page-1>0 ? $page-1 : 1) : $page);
      $oBtSuppr = new HtmlLink("javascript:SupprElt('".$id."', '".$strParam.ALK_MODE_SSHEET_SQL.
                               "&page=".$iPage."')", 
                               "Supprimer cet �l�ment", "tab_supprimer.gif", "tab_supprimer_rol.gif");

      if ($this->iSSheet == ALK_SSHEET_PARAMETRE)
      	$tabPage[$cpt] = array($strLib, $drDico->getValueName("RESEAU_NOM"), $drDico->getValueName("SUPPORT_NOM"), $oBtSuppr->getHtml());
      else
      	$tabPage[$cpt] = array($strLib, $oBtSuppr->getHtml());
      	
      $cpt++;
    }

    // pas de pagination
    //$nbEltParPage = $nbElt;
    if ($this->iSSheet == ALK_SSHEET_PARAMETRE)
    	$tabAlign = array("", "left", "center", "center", "center");
    else
    	$tabAlign = array("", "left", "center");
    	
    $strHtml .= getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, $page, 
                               $_SERVER["PHP_SELF"]."?".$strParam.ALK_MODE_SSHEET_LIST, 
                               $tabAlign).
      "</form><br>";

    $oBtAnnuler = new HtmlLink("01_page_form.php?".$strParam."&iModeSSheet=".ALK_MODE_SSHEET_LIST."&iMode=2", "Annuler", 
                               "annul_gen.gif", "annul_gen_rol.gif");
                               
    $strHtml .= "<div class='divTextContenu' style='margin-left:20px' align='center'><br>".$oBtAnnuler->getHtml()."</div>";

    return $strHtml;
  }
  
  function _getHtmlLigneCtrl($oCtrl) {
  	$strHtml = "<tr>".
		  							"<td align='right' class='formLabel'>".$oCtrl->label."</div></td>".
		  							"<td>".$oCtrl->getHtml()."</td>".
		  							"</tr>";
		return $strHtml;
  }
  /**
   * @brief Retourne le code html d'un formulaire dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function getHtmlFicheDico()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    $id = Request("id", REQ_GET, "-1", "is_numeric");
    $page =  Request("page", REQ_GET, "1", "is_numeric");
    
    $strParam = "iMode=".$iMode."&page=".$page.
      "&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		$strParam2 = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		
		$bassin_code = "";
    $dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
		if ($drBassin = $dsBassin->getRowIter())
		  $bassin_code = $drBassin->getValueName("BASSIN_CODE"); 
		  
		$iWidthTxt = 48;
    $iWidthMemo = 46;
    $iHeightMemo = 4;  
    
		$strNom = "";
    $strCode = "";
    $strHtmlCtrl = "";
    $strHtmlSuite = "";
    $bassin_id = $_SESSION["idBassin"];
    $masse_desc_id = "";
    $bRisque = "";
    $region_id = "";
    $strDatePassage = "";
    $me_objectif="";
    $Report="";
    $bModif="1";
    $bAtteinte="1";
    $strMotifDerogation = "";
    $tabDept = array();
    $tabMotif = array();
    
		switch ($this->iSSheet) {
    	case ALK_SSHEET_ME :
    		$strTitre = "masses d'eau";
    		$dsDico = $this->oQuery->getDs_MasseEauById($id);    		    		

        $strType = "";
    		$strTypeSuivi = "";    		
   			if( $drDico = $dsDico->getRowIter() ) {      
      			$strNom = $drDico->getValueName("NOM");
      			$strCode = $drDico->getValueName("CODE");
      			$strType = $drDico->getValueName("MASSE_TYPE");
      			$strTypeSuivi = $drDico->getValueName("MASSE_TYPE_SUIVI");
      			$masse_desc_id = $drDico->getValueName("MASSE_DESC_ID");
      			//$bRisque = $drDico->getValueName("MASSE_B_RISQUE");
      			$bAtteinteObj = $drDico->getValueName("MASSE_B_ATTEINTE_OBJ");
            $bassin_id = $drDico->getValueName("BASSIN_ID");
      			$region_id = $drDico->getValueName("REGION_ID");
      			$strDatePassage = $drDico->getValueName("MASSE_DATE_FORT_MODIF");           
            $Report = $drDico->getValueName("MASSE_REPORT_OBJECTIF");
            $me_objectif = $drDico->getValueName("MASSE_OBJECTIF");
            $bModif = $drDico->getValueName("MASSE_B_FORT_MODIF");           
            //$bAtteinte = $drDico->getValueName("MASSE_B_ATTEINTE_OBJECTIF");   
            $masseDocDesc = $drDico->getValueName("MASSE_DOC_DESC"); 
            $masseDocFiche = $drDico->getValueName("MASSE_DOC_FICHE");
            $strCommentaire = $drDico->getValueName("MASSE_COMMENTAIRE"); 
            
            //Ajout Mai 2016
            $strMotifDerogation = $drDico->getValueName("MASSE_MOTIF_DEROGATION");
            $tabMotif = explode(";",$strMotifDerogation);     
               
      			$dsDept = $this->oQuery->getDs_listeDeptMasseEau($id);
      			while ($drDept=$dsDept->getRowIter()){
      				array_push($tabDept, $drDept->getValueName("DEPT_ID"));
      			}
    		}
    		$oCtrlH = new HtmlHidden("bassin_id", $bassin_id);
    		$strHtmlCtrl .= $oCtrlH->getHtml();
    		
    		$oCtrl = new HtmlText(0, "nom", $strNom, "Nom", 1, $iWidthTxt, 255);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		  	$oCtrl = new HtmlText(0, "code", $strCode, "Code", 1, $iWidthTxt, 20);
		    $oCtrl->addValidator("formDico", "text", true);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);			
		  	$oCtrl = new HtmlText(0, "type", $strType, "Nature (MEC ou MET)", 1, $iWidthTxt, 45);
		    $oCtrl->addValidator("formDico", "text", false);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);												
		  	/*$oCtrl = new HtmlText(0, "type_suivi", $strTypeSuivi, "Type de suivi", 1, $iWidthTxt, 45);
		    $oCtrl->addValidator("formDico", "text", false);
		    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
  				*/			
          
		    $oCtrl = new HtmlSelect(0, "masse_desc_id", $masse_desc_id, "Type", 1);
    		$oCtrl->oValTxt = $this->oQuery->getTableForCombo("MASSE_EAU_DESCRIPTION", "MASSE_DESC", true, true);
    		$oCtrl->tabValTxtDefault = array ("-1", "--- S�lectionner une description ---");
    		$oCtrl->AddValidator("formDico", "select", true, "-1");
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
    		
    		$oCtrl = new HtmlSelect(0, "region_id", $region_id, "R�gion marine", 1);
    		$oCtrl->oValTxt = $this->oQuery->getTableForCombo("REGION_MARINE", "REGION", true);
    		$oCtrl->tabValTxtDefault = array ("-1", "--- S�lectionner une r�gion ---");
    		$oCtrl->AddValidator("formDico", "select", true, "-1");
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
    		
    		$oCtrl = new HtmlSelect(0, "dept_id[]", $tabDept, "D�partement(s)", 5);
    		$oCtrl->oValTxt = $this->oQuery->getTableForCombo("DEPARTEMENT", "DEPT", false, true, "DEPT_VISIBLE=1");    		
    		$oCtrl->tabValTxtDefault = array ("-1", "--- S�lectionner un ou plusieurs d�partements ---");
    		$oCtrl->bMultiple = true;
    		$oCtrl->AddValidator("formDico", "select", true, "-1");
    		$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
    		
        // retire sept2014
        /*$oCtrlCB = new HtmlRadioGroup(0, "b_risque", $bRisque, "Risque / respect des objectifs environnementaux aux horizons 2015");
		  	$oCtrlCB->AddRadio("1", "Risque");
    		$oCtrlCB->AddRadio("0", "Respect");
        $oCtrlCB->AddRadio("2", "Doute");
		  	$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB);	 
        */  
        // ajout Sept 2014  
        $oCtrlCB = new HtmlRadioGroup(0, "b_atteinte_obj", $bAtteinteObj, "Atteinte des objectifs environnementaux");        
        $ds = $this->oQuery->getTableForCombo("ATTEINTE_OBJECTIF_DESCRIPTION", "ATTEINTE_OBJECTIF", true, true, "");   
        while($dr = $ds->getRowIter()){
          $oCtrlCB->AddRadio($dr->getValueName("ATTEINTE_OBJECTIF_ID"), $dr->getValueName("nom"));
        } 
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB); 
    	  
        $oCtrlCB = new HtmlCheckboxGroup(0, "motif_derogation_obj[]", $tabMotif, "Motif de d�rogation");        
        $listCB = array("CN" => "Conditions naturelles",
        								"FT" => "Faisabilit� technique",
        								"CD" => "Co�ts disproportionn�s");           
        foreach ($listCB as $idMotif => $lib){          
          $oCtrlCB->AddCheckbox($idMotif, $lib);
        } 
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB); 
        
        // ajout des champs 11/2011
        $oCtrlCB = new HtmlRadioGroup(0, "b_modif", $bModif, "Masse d'eau fortement modifi�e");
        $oCtrlCB->AddRadio("1", "Oui");
        $oCtrlCB->AddRadio("0", "Non");
        $oCtrlCB->addValidator("formDico", "radio", true); 
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB);        

        $oCtrlDate = new HtmlText(0, "date_fort_modif",  $strDatePassage, "Date du passage", 1, 15, 15);
        $oCtrlDate->addValidator("formDico", "date10", false);
        $oCtrlDate->labelAfterCtrl  = "&nbsp;(DD/MM/AAAA)";   
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlDate);
      
        /*$oCtrlCB = new HtmlRadioGroup(0, "b_atteinte", $bAtteinte, "Atteinte des objectifs en 2015");
        $oCtrlCB->addValidator("formDico", "radio", false);
        $oCtrlCB->AddRadio("1", "Oui");
        $oCtrlCB->AddRadio("0", "Non");
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB);      
        */
        $oCtrl = new HtmlText(0, "me_objectif", $me_objectif, "Description de l'objectif", 1, $iWidthTxt, 45);
        $oCtrl->addValidator("formDico", "text", false);
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl); 
      
        $oCtrl = new HtmlText(0, "report", $Report, "Ann�e de report", 1, 15, 4);
        $oCtrl->addValidator("formDico", "text", false);
        $oCtrl->labelAfterCtrl  = "&nbsp;(AAAA)";   
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl); 

        $oCtrl = new HtmlHidden("old_element", $masseDocDesc);        
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
        $oCtrl = new HtmlHidden("old_element_fiche", $masseDocFiche);        
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);

        $oCtrl = new HtmlFile(0, "masse_doc_desc", $masseDocDesc, "Contexte physique de la masse", 55);
        $oCtrl->urlFile = ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$bassin_code."/".$masseDocDesc;
        $oCtrl->bDualMode = true;
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl); 
        
        $oCtrl = new HtmlFile(0, "masse_doc_fiche", $masseDocFiche, "Fiche de la masse d'eau", 55);
        $oCtrl->urlFile = ALK_SIALKE_URL.ALK_PATH_UPLOAD_DOC.$bassin_code."/".$masseDocFiche;
        $oCtrl->bDualMode = true;
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl); 

        $oCtrl = new HtmlText(0, "commentaire", $strCommentaire, "Commentaire sur la masse d'eau", 4, $iWidthTxt, 45);
        $oCtrl->addValidator("formDico", "text", false);
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);   
                
    		break;    	     		
    } 	
    $strHtml = $this->_getFrameTitleSheet("<a class='LienSommaire' href='01_page_form.php?".
                                          $strParam.ALK_MODE_SSHEET_LIST."'>Liste ".
                                          $strTitre."</a> / Fiche ").
      "<script language='javascript' src='lib/lib_atlas.js'></script>".                                    
      "<script language='javascript' src='../../lib/lib_formTxt.js'></script>".
      "<script language='javascript' src='../../lib/lib_formSelect.js'></script>".
      "<script language='javascript' src='../../lib/lib_formNumber.js'></script>".
      "<script language='javascript' src='../../lib/lib_formDate.js'></script>".
      "<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<form name='formDico' action='01_page_sql.php?".$strParam.ALK_MODE_SSHEET_SQL."&id=".$id.
      "' method='post'  enctype='multipart/form-data'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='200' height='10'></td>".
      "<td width='500'></td>".
      "</tr>";
          
    $strHtml .= $strHtmlCtrl;
    
    $oBtValid = new HtmlLink("javascript:ValiderDico('".$this->iSSheet."')", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    $oBtAnnul = new HtmlLink("01_page_form.php?".$strParam, "Annuler les modifications", 
                             "annul_gen.gif", "annul_gen_rol.gif");
		
    $strHtml .= "</table><br><div align='center'>".
     						$oBtValid->getHtml()."&nbsp;&nbsp;".$oBtAnnul->getHtml()."</div>".
      					"</form>";

		$strHtml .= $strHtmlSuite;
		
    return $strHtml;
  
  }

  /**
   * @brief Retourne le code html d'un formulaire dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function getHtmlFicheImport()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    
    $strParam = "iMode=".$iMode.
      "&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		
		$iWidthTxt = 48;
    $iWidthMemo = 46;
    $iHeightMemo = 4;  
    		
    $strHtmlCtrl = "";    
    
    $strPath = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT;
        
	  $oCtrl = new HtmlFile(0, "imp_fic_me", "", "Fichier � importer", $iWidthTxt, 255);
  	$strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		
		$oBtVal = new HtmlLink("javascript:Valider();", "Valider",
		                       "valid_gen.gif", "valid_gen_rol.gif");
		$oBtAnn = new HtmlLink("javascript:Annuler()", "Annuler", 
		                       "annul_gen.gif", "annul_gen_rol.gif");
		
		$strHtml = $this->_getFrameTitleSheet("Import de donn�es des masses d'eau de l'atlas").
      "<script language='javascript' src='lib/lib_atlas.js'></script>".                                    
      "<script language='javascript' src='../../lib/lib_formTxt.js'></script>".
      "<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<form name='formDico' action='01_page_form.php?".$strParam.ALK_MODE_SSHEET_SQL.
      "' method='post'  enctype='multipart/form-data'>".
      "<table width='420' border='0' cellspacing='0' cellpadding='4' align='center'>".
			  "<tr><td align='center'><div class='divContenuConseil'>".
			  "Pour r&eacute;aliser un import de donn&eacute;es dans l'atlas, vous devez au pr&eacute;alable prendre".
			  " connaissance du <a class='aContenuConseil' href='import_format.pdf' target='blank'>format</a> et des".
			  " <a class='aContenuConseil' href='import_regles.pdf' target='blank'>r&egrave;gles d'importation".
			  "</a>.<br> Lorsque l'import est termin&eacute;, le syst&egrave;me renvoie une adresse vous permettant de".
			  " consulter le compte-rendu de l'importation.<br> Plusieurs exemples de fichiers d'import sont disponibles".
			  "&nbsp;: <a class='aContenuConseil' href='import_ex4.txt' target='blank'>exemple 1</a>.</div>".
			  "</td></tr></table>".
      
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='200' height='10'></td>".
      "<td width='500'></td>".
      "</tr>";
          
    $strHtml .= $strHtmlCtrl;
    
    $oBtValid = new HtmlLink("javascript:ValiderImport('imp_fic_me')", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
    $oBtAnnul = new HtmlLink("01_page_form.php?".$strParam, "Annuler", 
                             "annul_gen.gif", "annul_gen_rol.gif");

    $strHtml .= "</table><br><div align='center'>".
     						$oBtValid->getHtml()."&nbsp;&nbsp;".$oBtAnnul->getHtml()."</div>".
      					"</form>";

    return $strHtml;
  
  }  
        
  /**
   * @brief Traitement des actions sur dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function traitementDico()
  {
    set_time_limit(0); // pas de limit
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    $id = Request("id", REQ_GET, "-1", "is_numeric");
	  $page =  Request("page", REQ_GET, "1", "is_numeric");

    $strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_LIST."&page=".$page;
    $strUrlBack = "01_page_form.php?".$strParam;
        
    $bassin_code = "";
    $dsBassin = $this->oQuery->getDs_bassin("", $_SESSION["idBassin"]);
		if ($drBassin = $dsBassin->getRowIter())
		  $bassin_code = $drBassin->getValueName("BASSIN_CODE"); 
		  
    $oldRang = "-1";
		$tabValue = array();
		
		switch ($this->iSSheet) {
			case ALK_SSHEET_ME:
      
      
				$tabValue["MASSE_NOM"] = array( 0, Request("nom", REQ_POST, ""));
	    	$tabValue["MASSE_CODE"] = array( 0, Request("code", REQ_POST, ""));
	    	$tabValue["MASSE_TYPE"] = array( 0, Request("type", REQ_POST, ""));
	    	//$tabValue["MASSE_TYPE_SUIVI"] = array( 0, Request("type_suivi", REQ_POST, ""));
	    	$tabValue["MASSE_DESC_ID"] = array( 1, Request("masse_desc_id", REQ_POST, ""));
	    	$tabValue["REGION_ID"] = array( 1, Request("region_id", REQ_POST, ""));
	    	$tabValue["BASSIN_ID"] = array( 1, Request("bassin_id", REQ_POST, ""));

	    	//$tabValue["MASSE_B_RISQUE"] = array( 1, Request("b_risque", REQ_POST, ""));	 
        $tabValue["MASSE_B_ATTEINTE_OBJ"] = array( 1, Request("b_atteinte_obj", REQ_POST, ""));  
        $tabValue["MASSE_B_FORT_MODIF"] = array( 1, Request("b_modif", REQ_POST, ""));  		 
        $tabValue["MASSE_DATE_FORT_MODIF"] = array( 2, Request("date_fort_modif", REQ_POST, ""));       	
	    	$tabValue["MASSE_REPORT_OBJECTIF"] = array( 0, Request("report", REQ_POST, ""));   
        $tabValue["MASSE_OBJECTIF"] = array( 0, Request("me_objectif", REQ_POST, ""));   
        //$tabValue["MASSE_B_ATTEINTE_OBJECTIF"] = array( 1, Request("b_atteinte", REQ_POST, ""));  
        $tabValue["MASSE_COMMENTAIRE"] = array(0, Request("commentaire", REQ_POST, "")); 
        
        $tabMotif = Request("motif_derogation_obj", REQ_POST, array());
        $tabValue["MASSE_MOTIF_DEROGATION"] = array(0,  implode( ";" , $tabMotif ));

        // traitement du fichier doc desc � uploader
        $iSupProtocole = RequestCheckbox("masse_doc_desc", REQ_POST);    
        $strFileNameDocDesc = DoUpload("masse_doc_desc", $tabValue["MASSE_CODE"][1]."_", ALK_PATH_UPLOAD_DOC.$bassin_code."/", $iSupProtocole, Request("old_element", REQ_POST, ""));
                
        if ($strFileNameDocDesc != "" || $iSupProtocole) 
          $tabValue["MASSE_DOC_DESC"] = array(0, $strFileNameDocDesc);
          
        $iSupProtocole = RequestCheckbox("masse_doc_fiche", REQ_POST);    
        $strFileNameDocDesc = DoUpload("masse_doc_fiche", $tabValue["MASSE_CODE"][1]."_fiche_", ALK_PATH_UPLOAD_DOC.$bassin_code."/", $iSupProtocole, Request("old_element_fiche", REQ_POST, ""));
                
        if ($strFileNameDocDesc != "" || $iSupProtocole) 
          $tabValue["MASSE_DOC_FICHE"] = array(0, $strFileNameDocDesc);  

        $strFieldKey = "MASSE_ID";
	    	$strTable = "MASSE_EAU";
    		break;     
		}
    switch( $iMode ) {
    case "1": // mode ajout
    case "2": // mode modif           

      if( $iMode == "1" ) {        
        //contr�le si le code masse d'eau est unique
        $dsMe = $this->oQuery->getDs_MasseEauById("", $tabValue["MASSE_CODE"][1]);		
    		if( $drMe = $dsMe->getRowIter() ) {  
    		  $strUrlBack .= "&err=2";
    		  return $strUrlBack;  		 
		    } 
        $id = $this->oQueryAction->add_ficheDico($strTable, $strFieldKey, $tabValue);
      } else {
        $this->oQueryAction->update_ficheDico($strTable, $strFieldKey, $id, $tabValue);
      }
      //gestion des d�partements 
      $bRes = $this->oQueryAction->del_ficheDico("MASSE_EAU_DEPT", "MASSE_ID", $id);			$tabDept = Request("dept_id", REQ_POST, array());
			$tabValue = array();
			$tabValue["MASSE_ID"]=array( 1, $id);		
			foreach($tabDept as $dept_id){
				$tabValue["DEPT_ID"] = array( 1, $dept_id);	
				$strSql = $this->oQueryAction->_getPartInsertSql($tabValue);
    		$strSql = "insert into MASSE_EAU_DEPT ".$strSql;
    		$this->oQueryAction->dbConn->executeSql($strSql);
			}
      break;

    case "3": // mode suppr
    	$bRes = $this->oQueryAction->del_ficheDico("MASSE_EAU_DEPT", "MASSE_ID", $id);			
      $bRes = $this->oQueryAction->del_ficheDico($strTable, $strFieldKey, $id);
      if( $bRes == false ) {
        $strUrlBack .= "&err=1";
      }
      break;
    }
    
    // MAJ de la date de  MAJ de la table BASSIN_HYDROGRAPHIQUE en cas de succes.
    $tabValueMaj = array();
    $tabValueMaj["DATE_MAJ"]= array( 1, $this->oQueryAction->dbConn->getDateCur());
    $this->oQueryAction->update_ficheDico("BASSIN_HYDROGRAPHIQUE", "BASSIN_ID", $_SESSION["idBassin"], $tabValueMaj);
    
    $this->traitementMajGml();
    
    return $strUrlBack;
  }  
  
  function traitementImport() {
  	
  	include_once("../../classes/appli/alkimport.class.php");
		include_once("classes/alkimportatlas_me.class.php");
		include_once("../../classes/appli/alkimporterreur.class.php");
		include_once("../../classes/appli/alkimportwarning.class.php");
		
		$iMode = Request("iMode", REQ_POST, "");
  	$fin = Request("fin", REQ_GET, "0", "is_numeric");
  	
  	$strParam = "iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet.
      "&iModeSSheet=".ALK_MODE_SSHEET_FORM;
      
  	$ag_id = $_SESSION["sit_idUser"];
		$urlFileError = ALK_SIALKE_URL.ALK_PATH_UPLOAD_IMPORT.$ag_id."me_err.txt";
		$strFileError = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$ag_id."me_err.txt";
		$urlFileWarning = ALK_SIALKE_URL.ALK_PATH_UPLOAD_IMPORT.$ag_id."me_warning.txt";
		$strFileWarning = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$ag_id."me_warning.txt";
		SupprFichier($strFileError);
		SupprFichier($strFileWarning);
		
		$pathFile = "";
		$strFileName = "";
		
		if( $fin != "1" ) {
		
			if( $iMode != "annuler" ) {
				//upload du fichier
				$strFileName = DoUpload("imp_fic_me", $ag_id."_", ALK_PATH_UPLOAD_IMPORT, 0, "");
				if( !is_string($strFileName))
					$strFileName = "";
				if( $strFileName!="" )
					$pathFile = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$strFileName;
			}
		}
		$strFichier_Import = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_IMPORT.$strFileName;
		$strFichier_Erreur = $strFileError;
		$strFichier_Warning = $strFileWarning; 
		
		$intSeparateur = 9;
		$tabImport = array (
		                    array("name" => "MODE", "oblig" => 1,"type" =>array("S","M","C") ,"lg" => 0,"index" => 0 ,"mode" => 1),
		                    array("name" => "ME", "oblig" => 1, "type" => "", "lg" => 20,"index" => 0 ,"mode" => 0),
		                    array("name" => "NOM", "oblig" => 0, "type" => "", "lg" => 255,"index" => 0 ,"mode" => 0),
 		                    array("name" => "NATURE", "oblig" => 0, "type" => array("MEC","MET"), "lg" => 3,"index" => 0 ,"mode" => 1),		                    
		                    //array("name" => "TYPE_SUIVI", "oblig" => 0,  "type" => "", "lg" => 45,"index" => 0 ,"mode" => 0),
		                    array("name" => "TYPE", "oblig" => 0, "type" => $this->oQuery->GetDs_champsImport("MASSE_EAU_DESCRIPTION","MASSE_DESC_CODE", "MASSE_DESC_ID"), "lg" => 20,"index" => 0 ,"mode" => 2),		                    
		                    array("name" => "REGION", "oblig" => 0, "type" => $this->oQuery->GetDs_champsImport("REGION_MARINE","REGION_LIBELLE", "REGION_ID"), "lg" => 20,"index" => 0 ,"mode" => 2),
		                    array("name" => "DEPT", "oblig" => 0, "type" => "", "lg" => 20,"index" => 0 ,"mode" => 0),
		                    //array("name" => "RISQUE", "oblig" => 0,"type" =>array("OUI","NON", "DOUTE","") ,"lg" => 0,"index" => 0 ,"mode" => 1),
                        array("name" => "FORTEMENT_MODIFIE", "oblig" => 0,"type" =>array("OUI","NON") ,"lg" => 0,"index" => 0 ,"mode" => 1),		                    
		                    array("name" => "DATE_PASSAGE", "oblig" => 0, "type" => "date","lg" => 10,"index" => 0 ,"mode" => 0),
		                    //array("name" => "ATTEINTE_OBJECTIF", "oblig" => 0,"type" =>array("OUI","NON", "") ,"lg" => 0,"index" => 0 ,"mode" => 1),
		                    array("name" => "OBJECTIF", "oblig" => 0,"type" => "" ,"lg" => 255,"index" => 0 ,"mode" => 0),
		                    array("name" => "REPORT", "oblig" => 0,"type" => "" ,"lg" => 255,"index" => 0 ,"mode" => 0),		
                        array("name" => "COMMENTAIRE", "oblig" => 0,"type" => "" ,"lg" => 255,"index" => 0 ,"mode" => 0), 
                        array("name" => "ATTEINTE_OBJECTIF_DESCRIPTION", "oblig" => 0, "type" => $this->oQuery->GetDs_champsImport("ATTEINTE_OBJECTIF_DESCRIPTION","ATTEINTE_OBJECTIF_CODE", "ATTEINTE_OBJECTIF_ID"), "lg" => 20,"index" => 0 ,"mode" => 2),                                    
		                    );
		$bTes = true;
		$intTes = 1;
		$tabSup = array("ME");
		$tabAjout = array ("ME", "NOM", "NATURE", "TYPE", "REGION", "DEPT", /*"RISQUE",*/ "FORTEMENT_MODIFIE", /*"ATTEINTE_OBJECTIF"*/ "ATTEINTE_OBJECTIF_DESCRIPTION");
		$tabModif = array("ME");
		$objErreur = new AlkImportErreur($strFichier_Erreur);
		$objWarning = new AlkImportWarning($strFichier_Warning);
		$objImport = new AlkImportAtlasMe($strFichier_Import, $tabImport, $tabSup, $tabAjout, $tabModif, 
		                               $objErreur,$objWarning, $this->oQuery, $this->oQueryAction );
		
		$iTes = 0;
		$intTest = $objImport->parseFile($intSeparateur);
		if( $intTest == 0 ) {
			$intTes = $objImport->parseLines($intSeparateur);
		  if( $intTes == 0 ) {
		    $iTes = $objImport->integrate($intSeparateur);
		  }
		}
				
		$strHtml = $this->_getFrameTitleSheet("Import de donn�es des masses d'eau de l'atlas").		
		  "<table width='606' border='0' cellspacing='0' cellpadding='4' align='center'>".
		  "<tr>".
		  "<td width=60>&nbsp;</td>".
		  "<td align='center'>";
		
		if( $intTes == 0 && $iTes == 0 ) {
		  $strHtml .= "<div class='divContenuTexte'>Importation effectu� avec succ�s.</div>";
      // MAJ de la date de  MAJ de la table BASSIN_HYDROGRAPHIQUE en cas de succes.
      $tabValueMaj = array();
      $tabValueMaj["DATE_MAJ"]= array( 1, $this->oQueryAction->dbConn->getDateCur());
      $this->oQueryAction->update_ficheDico("BASSIN_HYDROGRAPHIQUE", "BASSIN_ID", $_SESSION["idBassin"], $tabValueMaj);
		} else {
		  if( $intTes > 0 ) {
		    $strHtml .= "<div class='divContenuTexte'>Importation non effectu�e pour cause de fichier mal format�.</div>";
		  }
		  if( $iTes > 0 ) {
		    $strHtml .= "<div class='divContenuTexte'>Importation effectu�e avec des alertes ou des erreurs.</div>";    
		  }
		  $bFileAlert = file_exists($strFichier_Warning) && is_file($strFichier_Warning);
		  $bFileError = file_exists($strFichier_Erreur) && is_file($strFichier_Erreur);
		
		  if( $bFileAlert || $bFileError ) {
		    $strHtml .= "<div class='divContenuTexte'><font color=red>Attention</font> : Vous pouvez consulter ";
		    if( $bFileAlert ) {
		      $strHtml .= "<a class='aContenuLien' href=\"".$urlFileWarning."\" target='_blank'>les alertes</a>";
		      if( $bFileError )
		        $strHtml .= " et ";
		    }
		    if( $bFileError ) {
		       $strHtml .= "<a class='aContenuLien' href=\"".$urlFileError."\" target='_blank'>les erreurs</a>";
		    }
		    $strHtml .= ".";
		  }
		  
		  $strHtml .= "<br><a class='aContenuLien' href=\"01_page_form.php?".$strParam."\">R�essayer un nouvel import</a>";
		}
		
		//Effectuer la mise � jour des gml pour l'outil cartographique
		$this->traitementMajGml();
		
		return $strHtml;
  }    
  
  function traitementMajGml(){
    include_once("classes/alkappliatlas_qualite.class.php");
    $oAppli = new AlkAppliAtlas_qualite($appli_id=0, $agent_id=0, $iSheet=0, $iSSheet=0, $iModeSSheet=0);
    return $oAppli->traitementMajGml("me");
  }  
}

?>