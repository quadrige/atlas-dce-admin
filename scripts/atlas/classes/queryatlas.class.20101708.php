<?php
/**
 * @Class queryAtlas
 *
 * @brief  Ensemble des requetes s�lections li�es � l'application actualit�s
 *         Les requetes sp�cifiques se trouvent dans une classe h�rit�e queryAtlas_action_xxx.class.php
 */
class queryAtlas {

  var $dbConn; // Connection to the database
  var $tabLangue;
  
  /**
   * @brief Constructeur
   *
   * @param dbConn    classe de connexion � la base
   * @param tabLangue tableau des langues utilis�es
   */
  function queryAtlas($dbConn, $tabLangue) 
  {
    $this->dbConn = $dbConn;
    $this->tabLangue = $tabLangue;
  }
  
  /**
   * @brief Affecte la connexion
   *
   * @param dbConn classe de connexion � la base
   *
   * @return Retourne true
   */
  function setConnection($dbConn) 
  {
    $this->dbConn = &$dbConn ;
    return true ;
  }
    
  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getReseauForCombo($bCode=true, $idFirst=0, $idLast=-1) 
  {    
    $strSql = "select RESEAU_NOM, " .
    	($bCode ? "RESEAU_CODE ," : "").
    	" RESEAU_ID, RESEAU_SYMBOLE , RESEAU_COULEUR".
      " from RESEAU".      
      " order by RESEAU_RANG, RESEAU_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
  
  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getMasseForCombo($reseau_id=-1, $idFirst=0, $idLast=-1, $bassin_id="", $dept_id="-1") 
  {    
    $strSql = "select distinct ".$this->dbConn->GetConcat("MASSE_CODE", "' - '", "MASSE_NOM")." as MASSE_NOM_LG, me.MASSE_ID".
      " from MASSE_EAU me".
    	" left join MASSE_EAU_DEPT dept on dept.MASSE_ID = me.MASSE_ID".
    	" where 1=1".   
    	($bassin_id != "" ? " and me.BASSIN_ID=".$bassin_id : "").
    	($dept_id != "-1" ? " and dept.DEPT_ID=".$dept_id : "").
      " order by MASSE_NOM_LG";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
  
  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getPointForCombo($reseau_id=-1, $masse_id=-1, $bCode=true, $idFirst=0, $idLast=-1, $bassin_id="") 
  {    
    $strSql = "select ".$this->dbConn->GetConcat("POINT_CODE", "' - '", "POINT_NOM")." as POINT_NOM_LG, " .
    	($bCode ? "POINT_CODE, POINT_ID" : "POINT_ID, POINT_CODE").
      " from POINT as p".
    	" inner join MASSE_EAU as me on p.MASSE_ID=me.MASSE_ID".
    	($bassin_id != "" ? " and me.BASSIN_ID = ".$bassin_id : "").          	
      ($masse_id != -1 ? " where p.MASSE_ID = ".$masse_id : "").      
      " order by POINT_NOM_LG";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }    
  
  /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getSupportForCombo($reseau_id=-1, $idFirst=0, $idLast=-1) 
  {    
    $strSql = "select ".$this->dbConn->GetConcat("RESEAU_NOM", "' - '", "SUPPORT_NOM")." as SUPPORT_NOM_LG, SUPPORT_ID".
      " from SUPPORT, RESEAU". 
      " where SUPPORT.RESEAU_ID =  RESEAU.RESEAU_ID".  
      " order by SUPPORT_NOM_LG";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
  
    /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getOperateurForCombo($idFirst=0, $idLast=-1) 
  {    
    $strSql = "select OPERATEUR_NOM, OPERATEUR_ID".
      " from OPERATEUR".        
      " order by OPERATEUR_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
  
      /**
   * @brief Retourne la liste des thematiques d'une appli pour un combo
   *
   * @param appli_id Identifiant de l'application
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getParamForCombo($idReseau, $idPoint, $idFirst=0, $idLast=-1) 
  {    
    $strSql = "select ".$this->dbConn->GetConcat("SUPPORT_NOM", "' - '", "PARAMETRE_NOM")." as PARAMETRE, PARAMETRE_ID ID from PARAMETRE par".
    					" left join SUPPORT s on par.SUPPORT_ID = s.SUPPORT_ID".
    					" left join RESEAU r on r.RESEAU_ID = s.RESEAU_ID".
    					" where r.RESEAU_ID = ".$idReseau.
    					" and PARAMETRE_ID not in (select PARAMETRE_ID from RESEAU_POINT_PARAM where RESEAU_ID = ".$idReseau." and POINT_ID = ".$idPoint.")".
    					" order by s.SUPPORT_NOM, PARAMETRE_NOM";    					
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
  
  /**
   * @brief Retourne la liste d'une table nomm�e pour un combo
   *
   * @param idFirst indice de d�but pour la pagination
   * @param idLast  indice de fin pour la pagination
   *
   * @return Retourne un dataset
   */
  function getTableForCombo($strTable, $strPrefixe, $bLibelle=false, $bConcatCode=false, $strSqlSuppl="", $idFirst=0, $idLast=-1)
  {    
    $strSql = "select ".
    	($bConcatCode ? $this->dbConn->GetConcat($strPrefixe."_CODE", "' - '", ($bLibelle ? $strPrefixe."_LIBELLE" : $strPrefixe."_NOM"))
    								: ($bLibelle ? $strPrefixe."_LIBELLE" : $strPrefixe."_NOM"))." as nom, ".
    	$strPrefixe."_ID ".
    	($bConcatCode ? ", ".$strPrefixe."_CODE as code" : "").
      " from ".$strTable.      
    	($strSqlSuppl != "" ? " where ".$strSqlSuppl : "").
      " order by nom";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast) ;
  }
    
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getReseauByCode($codeReseau) 
  {          
    $strSql = "select * from RESEAU ".
      " where RESEAU_CODE = '".$this->dbConn->analyseSql($codeReseau)."'";
    return $this->dbConn->initDataset( $strSql ) ;
  } 
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getPointByCode($codePoint, $idReseau="") 
  { 
    $strFrom = "";
    $strWhere = "";
    if ($idReseau != ""){
        $strFrom .= " , RESEAU_POINT_PARAM rpp ";
        $strWhere .= " and rpp.POINT_ID=pt.POINT_ID and rpp.RESEAU_ID=".$idReseau;
    }        
    $strSql = "select * from POINT pt, MASSE_EAU me".
      $strFrom.
      " where pt.MASSE_ID = me.MASSE_ID" .
      " and POINT_CODE = '".$this->dbConn->analyseSql($codePoint)."'".
      $strWhere;
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getParametreByCode($codeParam, $codeSupport) 
  {          
    $strSql = "select * from PARAMETRE pa, SUPPORT su".
      " where pa.SUPPORT_ID = su.SUPPORT_ID" .
      " and pa.PARAMETRE_CODE = '".$this->dbConn->analyseSql($codeParam)."'".
      " and su.SUPPORT_CODE = '".$this->dbConn->analyseSql($codeSupport)."'";
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getOperateurByCode($codeOperateur) 
  {          
    $strSql = "select * from OPERATEUR ".
      " where OPERATEUR_CODE = '".$this->dbConn->analyseSql($codeOperateur)."'";
    return $this->dbConn->initDataset( $strSql ) ;
  }  
    
    /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_listeMasseEau($idFirst, $idLast, $idBassin="", $codeBassin="", $strCond="") 
  {          
  	$strWhere = " where 1=1";
  	$strWhere .= ($idBassin != "" ? " and me.BASSIN_ID=".$idBassin : "" );
  	$strWhere .= ($codeBassin != "" ? " and bas.BASSIN_CODE= '".$codeBassin."'" : "" );  	
  	$strWhere .= ($strCond != "" ? " and ".$strCond : "" );
  	
    $strSql = "select *, me.MASSE_ID ID, ".$this->dbConn->getConcat("MASSE_NOM", "' - '", "MASSE_CODE")." LIB".
    					", GROUP_CONCAT(dept.DEPT_NOM SEPARATOR ', ') DEPT".
    					", bas.BASSIN_NOM, reg.REGION_LIBELLE, ".
    					$this->dbConn->getConcat("descr.MASSE_DESC_CODE", "' - '", "descr.MASSE_DESC_LIBELLE")." DESCR".  
    					", descr.MASSE_DESC_CODE".  					
    					" from MASSE_EAU me".
    					" left join MASSE_EAU_DEPT medept on medept.MASSE_ID = me.MASSE_ID". 
    					" left join DEPARTEMENT dept on dept.DEPT_ID = medept.DEPT_ID".
    					" left join BASSIN_HYDROGRAPHIQUE bas on bas.BASSIN_ID = me.BASSIN_ID".
    					" left join REGION_MARINE reg on reg.REGION_ID = me.REGION_ID".
     					" left join MASSE_EAU_DESCRIPTION descr on descr.MASSE_DESC_ID = me.MASSE_DESC_ID".    
    					$strWhere.
    					" GROUP BY me.MASSE_ID".
    					" order by MASSE_RANG_GEO, MASSE_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }
  
    /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_MasseEauById($idMe, $codeMe="") 
  {          
   $strSql = "select me.MASSE_ID ID, MASSE_NOM NOM, MASSE_CODE CODE, MASSE_TYPE, MASSE_TYPE_SUIVI, ".
    					" MASSE_B_SUIVI, MASSE_B_RISQUE, me.BASSIN_ID, me.REGION_ID, me.MASSE_DESC_ID, MASSE_URL_FICHE, ".
   						$this->dbConn->getConcat("MASSE_NOM", "' - '", "MASSE_CODE")." LIB".
    					", GROUP_CONCAT(dept.DEPT_NOM SEPARATOR ', ') DEPT".
    					", bas.BASSIN_NOM, bas.BASSIN_CODE, bas.BASSIN_ALERT, reg.REGION_LIBELLE, ".
    					$this->dbConn->getConcat("descr.MASSE_DESC_CODE", "' - '", "descr.MASSE_DESC_LIBELLE")." DESCR".
    					" from MASSE_EAU me".
    					" left join MASSE_EAU_DEPT medept on medept.MASSE_ID = me.MASSE_ID". 
    					" left join DEPARTEMENT dept on dept.DEPT_ID = medept.DEPT_ID".
    					" left join BASSIN_HYDROGRAPHIQUE bas on bas.BASSIN_ID = me.BASSIN_ID".
    					" left join REGION_MARINE reg on reg.REGION_ID = me.REGION_ID".
     					" left join MASSE_EAU_DESCRIPTION descr on descr.MASSE_DESC_ID = me.MASSE_DESC_ID".        					
    					($idMe != "" ? " where me.MASSE_ID = ".$idMe : ($codeMe != "" ? " where me.MASSE_CODE = '".$codeMe."'" : "")).
    					" GROUP BY me.MASSE_ID";
    return $this->dbConn->initDataset( $strSql ) ;
  }

  
    /**
   * @brief Retourne la liste des d�partements d'une masse d'eau
   *
   * @param idMe  Identifiant de la masse d'eau
   *
   * @return Retourne un dataset
   */
  function getDs_listeDeptMasseEau($idMe) 
  {          
    $strSql = "select * ".
    					" from MASSE_EAU_DEPT dept".
    					" where MASSE_ID = ".$idMe;
    return $this->dbConn->initDataset( $strSql ) ;
  }
    
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_listeOperateur($idFirst, $idLast) 
  {          
    $strSql = "select OPERATEUR_ID ID, OPERATEUR_NOM LIB from OPERATEUR op".
    					" order by OPERATEUR_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }  

    /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_OperateurById($id) 
  {          
    $strSql = "select OPERATEUR_ID ID, OPERATEUR_NOM NOM, OPERATEUR_CODE CODE from OPERATEUR ".
    					" where OPERATEUR_ID = ".$id;
    return $this->dbConn->initDataset( $strSql ) ;
  }
    
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_listeReseau($idFirst, $idLast) 
  {          
    $strSql = "select RESEAU_ID ID, RESEAU_NOM LIB from RESEAU ".
    					" order by RESEAU_RANG, RESEAU_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }

  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_ReseauById($id) 
  {          
    $strSql = "select RESEAU_ID ID, RESEAU_NOM NOM, RESEAU_CODE CODE from RESEAU ".
    					" where RESEAU_ID = ".$id;
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_listePoint($idFirst, $idLast) 
  {          
    $strSql = "select POINT_ID ID, ".$this->dbConn->getConcat("POINT_NOM", "' - '", "POINT_CODE")." LIB from POINT pt".
    					" order by POINT_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }   
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_listeParametre($idFirst, $idLast) 
  {          
    $strSql = "select PARAMETRE_ID ID, PARAMETRE_NOM LIB, s.SUPPORT_NOM, r.RESEAU_NOM from PARAMETRE par".
    					" left join SUPPORT s on par.SUPPORT_ID = s.SUPPORT_ID".
    					" left join RESEAU r on r.RESEAU_ID = s.RESEAU_ID".
    					" order by r.RESEAU_NOM, s.SUPPORT_NOM, PARAMETRE_NOM";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }     

  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_PointById($id) 
  {          
    $strSql = "select POINT_ID ID, POINT_NOM NOM, POINT_CODE CODE, POINT_LONGITUDE, POINT_LATITUDE, MASSE_ID from POINT ".
    					" where POINT_ID = ".$id;
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_SupportById($id) 
  {          
    $strSql = "select SUPPORT_ID ID, SUPPORT_NOM NOM, SUPPORT_CODE CODE from SUPPORT ".
    					" where SUPPORT_ID = ".$id;    					
    return $this->dbConn->initDataset( $strSql ) ;
  }  

  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_ParametreById($id) 
  {          
    $strSql = "select PARAMETRE_ID ID, PARAMETRE_NOM NOM, PARAMETRE_CODE CODE, SUPPORT_ID from PARAMETRE ".
    					" where PARAMETRE_ID = ".$id;
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getDs_listeSupportByReseau($idReseau) 
  {          
    $strSql = "select SUPPORT_ID ID, SUPPORT_NOM LIB, SUPPORT_CODE CODE from SUPPORT ".
    					" where RESEAU_ID = ".$idReseau.
    					" order by SUPPORT_NOM";
    return $this->dbConn->initDataset( $strSql, 0, -1 ) ;
  }
  
 /**
   * @brief Retourne la liste des dictiionnaires relatifs � l'�tat de qualit� des masses d'eau
   *
   * @param strTabledico  Nom de la table dictionnaire 
   * @param strPrefixe  	Pr�fixe pr�c�dant le nom du champ 
   *
   * @return Retourne un dataset
   */
  function getDs_listeEtat($idFirst=0, $idLast=-1, $idTypeElementQualite="", $idElementQualite="", $strEtat="")
  {          
    $strSql = "select ETAT_LIBELLE LIB, ETAT_ID ID,  cl.TYPE_CLASSEMENT_LIBELLE CLASSEMENT, cl.TYPE_CLASSEMENT_ID, ETAT_COULEUR, ETAT_VALEUR from ETAT eta, TYPE_CLASSEMENT cl".
    					" where eta.TYPE_CLASSEMENT_ID = cl.TYPE_CLASSEMENT_ID".
    					($idTypeElementQualite != "" ? " and eta.TYPE_CLASSEMENT_ID in (select TYPE_CLASSEMENT_ID from TYPE_ELEMENT_QUALITE where TYPE_ELEMENT_QUALITE_ID = ".$idTypeElementQualite.")" : "").
    					($idElementQualite != "" ? " and eta.TYPE_CLASSEMENT_ID in (select TYPE_CLASSEMENT_ID from ELEMENT_QUALITE where ELEMENT_QUALITE_ID = ".$idElementQualite.")" : "").
    					($strEtat != "" ? " and ".$this->dbConn->getUpperCase("ETAT_LIBELLE")." = '".strtoupper($this->dbConn->analyseSql($strEtat))."'" : "").    					
    					" order by eta.TYPE_CLASSEMENT_ID, ETAT_VALEUR";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }  

 /**
   * @brief Retourne la fiche d'un etat
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_EtatById($id) 
  {          
    $strSql = "select ETAT_ID ID, ETAT_LIBELLE LIB, TYPE_CLASSEMENT_ID, ETAT_COULEUR, ETAT_VALEUR from ETAT ".
    					" where ETAT_ID = ".$id;
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
 /**
   * @brief Retourne la fiche d'un etat
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_EtatMotifById($id, $code="") 
  {          
    $strSql = "select MOTIF_ID ID, MOTIF_LIBELLE LIB, MOTIF_CODE CODE from ETAT_MOTIF".
    					" where 1=1".
    					($id != "" ? " and MOTIF_ID = ".$id : "").    					
    					($code != "" ? " and ".$this->dbConn->getUpperCase("MOTIF_CODE")." = '".strtoupper($this->dbConn->analyseSql($code))."'" : "");    					
    					
    return $this->dbConn->initDataset( $strSql ) ;
  }
 /**
   * @brief Retourne la fiche d'un statut
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_StatutById($id, $code="") 
  {          
    $strSql = "select STATUT_ID ID, STATUT_LIBELLE LIB, STATUT_CODE CODE from CLASSEMENT_STATUT".
    					" where 1=1".
    					($id != "" ? " and STATUT_ID = ".$id : "").    					
    					($code != "" ? " and ".$this->dbConn->getUpperCase("STATUT_CODE")." = '".strtoupper($this->dbConn->analyseSql($code))."'" : "");    					
    					
    return $this->dbConn->initDataset( $strSql ) ;
  }    
  /**
   * @brief Retourne la liste des dictiionnaires relatifs � l'�tat de qualit� des masses d'eau
   *
   * @param strTabledico  Nom de la table dictionnaire 
   * @param strPrefixe  	Pr�fixe pr�c�dant le nom du champ 
   *
   * @return Retourne un dataset
   */
  function getDs_listeEtatParametre($strTableDico, $strPrefixe, $idFirst=0, $idLast=-1)
  {          
    $strSql = "select ".$strPrefixe."_ID ID, ".$strPrefixe."_LIBELLE LIB from ".$strTableDico.
    					" order by LIB";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }  
  
     
  /**
   * @brief fonction pour le recuperation de  l'intitule des champs et l'id leurs correspandants.
   *        11_import
   *
   * @param $strTable nom de la table.
   * @param $strChamp nom du champs.
   * @param $strIdChamp nom de l'id du champ.
   * @return un tableau 
   */
	function GetDs_champsImport($strTable, $strChamp, $strIdChamp)
	{
		$tabChamp =array();
    $strSql = "select " .$strChamp." , ".$strIdChamp." from ".$strTable ;
    $dsImport = $this->dbConn->InitDataSet($strSql);
		while( $drImport = $dsImport->getRowIter() ) {
			$strName = $drImport->getValueName($strChamp); 
			$idName = $drImport->getValueName($strIdChamp);
			$tabChamp[$idName] = $strName;
		}
		return $tabChamp;
	}  
	
 /**
   * @brief Retourne la fiche d'un etat
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_ElementQualiteById($id, $element_nom="", $iNiveau="") 
  {          
    $strSql = "select * from ELEMENT_QUALITE ".
    					" where 1=1".
    					($id != "" ? " and ELEMENT_QUALITE_ID = ".$id : "").
    					($element_nom != "" ? " and ".$this->dbConn->getUpperCase("ELEMENT_QUALITE_NOM")." = '".$this->dbConn->analyseSql(strtoupper($element_nom))."'" : "").
    					($iNiveau != "" ? " and ELEMENT_QUALITE_NIVEAU = ".$iNiveau : "");
    return $this->dbConn->initDataset( $strSql ) ;
  }
  	
 /**
   * @brief Retourne la fiche d'un etat
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_TypeElementQualiteById($id, $element_nom="") 
  {          
    $strSql = "select * from TYPE_ELEMENT_QUALITE ".
    					" where 1=1".
    					($id != "" ? " and TYPE_ELEMENT_QUALITE_ID = ".$id : "").
    					($element_nom != "" ? " and ".$this->dbConn->getUpperCase("TYPE_ELEMENT_QUALITE_NOM")." = '".$this->dbConn->analyseSql(strtoupper($element_nom))."'" : "");
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  /**
   * @brief Retourne la liste des �l�ments de qualit�
   *
   * iNiveau specifie le niveau des �l�ments de qualit� � retourner
   *
   * @return Retourne un dataset
   */
  function getDs_listeElementqualite($iNiveau=-1, $idFirst=0, $idLast=-1, $strOrderBy="") 
  {          
    $strSql = "select ".
    					$this->dbConn->CompareSql("ELEMENT_QUALITE_NIVEAU", ">", "1", $this->dbConn->getConcat("'---&nbsp;'", "ELEMENT_QUALITE_NOM"), "ELEMENT_QUALITE_NOM")." as LIB".
    					", ELEMENT_QUALITE_ID ID, ELEMENT_QUALITE_ID, ELEMENT_QUALITE_PARENT, ELEMENT_QUALITE_NIVEAU from ELEMENT_QUALITE qual".
    					($iNiveau != -1 ? " where ELEMENT_QUALITE_NIVEAU = ".$iNiveau : "").
    					($strOrderBy != "" ? " order by ".$strOrderBy : "");
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }    
  
  /**
   * @brief Retourne le ds d'un bassin
   *
   * bassin_code code bassin
   *
   * @return Retourne un dataset
   */
  function getDs_bassin($bassin_code="", $bassin_id="") 
  {          
    $strSql = "select * from BASSIN_HYDROGRAPHIQUE where 1=1".
    					($bassin_code != "" ? " and BASSIN_CODE = '".$bassin_code."'" : "").
    					($bassin_id != "" ? " and BASSIN_ID = '".$bassin_id."'" : "");
    return $this->dbConn->initDataset( $strSql) ;
  }      	

  /**
   * @brief Retourne la liste des �tats par �l�ment de qualit� pour une masse d'eau et la session en cours
   * 				L'�tat est inconnu si non saisi
   * 				Tous les �l�ments de qualit� sont retourn�s dans le dataset
   *
   * codeMe code de la masse d'eau
   *
   * @return Retourne un dataset 
   */
  function getDs_listeEtatMasseEau($codeMe="", $iNiveau="", $idFirst=0, $idLast=-1) 
  {      
  	$strSqlTypeElement = "SELECT b.BASSIN_CODE, me.MASSE_CODE, classement.TYPE_CLASSEMENT_LIBELLE, tele.TYPE_CLASSEMENT_ID, tele.TYPE_ELEMENT_QUALITE_ID, tele.TYPE_ELEMENT_QUALITE_NOM, NULL as ELEMENT_QUALITE_NOM, 
  						 NULL as ELEMENT_QUALITE_NIVEAU, NULL as ELEMENT_QUALITE_ID, NULL as ELEMENT_QUALITE_URL_FICHE, 0 as ELEMENT_QUALITE_RANG, NULL as ELEMENT_QUALITE_PARENT, ".
  						 $this->dbConn->GetDateFormat("DD/MM/YYYY", "cl.CLASSEMENT_DATE" , false)." as CLASSEMENT_DATE, ".
  						 "cl.CLASSEMENT_BILAN, cl.CLASSEMENT_COMPLEMENT_BILAN, cl.CLASSEMENT_DOC_REF, cl.CLASSEMENT_INDICE_CONFIANCE, ".  						 
  						 	$this->dbConn->compareSql("etat.ETAT_ID", "is", "NULL", "etat_indefini.ETAT_ID", "etat.ETAT_ID"). " as ETAT_ID, ".
  							$this->dbConn->compareSql("etat.ETAT_LIBELLE", "is", "NULL", "etat_indefini.ETAT_LIBELLE", "etat.ETAT_LIBELLE"). " as ETAT_LIBELLE, ".
  							$this->dbConn->compareSql("etat.ETAT_COULEUR", "is", "NULL", "etat_indefini.ETAT_COULEUR", "etat.ETAT_COULEUR"). " as ETAT_COULEUR, ".
  							$this->dbConn->compareSql("etat.ETAT_VALEUR", "is", "NULL", "etat_indefini.ETAT_VALEUR", "etat.ETAT_VALEUR"). " as ETAT_VALEUR, ".  							
  						" st.STATUT_CODE, st.STATUT_ID, motif.MOTIF_CODE, motif.MOTIF_ID, cl.CLASSEMENT_ID, 1 as B_AFF  
							FROM TYPE_ELEMENT_QUALITE tele
							LEFT JOIN TYPE_CLASSEMENT classement ON classement.TYPE_CLASSEMENT_ID = tele.TYPE_CLASSEMENT_ID
							LEFT JOIN CLASSEMENT_MASSE_EAU cl ON cl.TYPE_ELEMENT_QUALITE_ID = tele.TYPE_ELEMENT_QUALITE_ID and cl.SESSION_ID =  (
							SELECT max( SESSION_ID ) AS SESSION_ID
							FROM SESSION_QUALITE ) AND cl.MASSE_ID = (SELECT MASSE_ID FROM MASSE_EAU me WHERE me.MASSE_CODE = '".$codeMe."')
							LEFT JOIN MASSE_EAU me ON me.MASSE_ID = cl.MASSE_ID
							LEFT JOIN BASSIN_HYDROGRAPHIQUE b ON b.BASSIN_ID = me.BASSIN_ID
							LEFT JOIN CLASSEMENT_STATUT st ON st.STATUT_ID = cl.STATUT_ID
							LEFT JOIN ETAT_MOTIF motif ON motif.MOTIF_ID = cl.MOTIF_ID
							LEFT JOIN ETAT etat ON etat.ETAT_ID = cl.ETAT_ID
							LEFT JOIN ETAT etat_indefini ON etat_indefini.TYPE_CLASSEMENT_ID = tele.TYPE_CLASSEMENT_ID and etat_indefini.ETAT_VALEUR = 0 and (etat_indefini.ETAT_ID = 8 or etat_indefini.ETAT_ID = 9)";
  							
  	$strSqlElement = "SELECT b.BASSIN_CODE, me.MASSE_CODE, classement.TYPE_CLASSEMENT_LIBELLE, ele.TYPE_CLASSEMENT_ID, tele.TYPE_ELEMENT_QUALITE_ID, tele.TYPE_ELEMENT_QUALITE_NOM, ele.ELEMENT_QUALITE_NOM,  						 
  						 ele.ELEMENT_QUALITE_NIVEAU,  ele.ELEMENT_QUALITE_ID, ele.ELEMENT_QUALITE_URL_FICHE, ele.ELEMENT_QUALITE_RANG as ELEMENT_QUALITE_RANG, ele.ELEMENT_QUALITE_PARENT as ELEMENT_QUALITE_PARENT, ".
  						 $this->dbConn->GetDateFormat("DD/MM/YYYY", "cl.CLASSEMENT_DATE" , false)." as CLASSEMENT_DATE, ".
  						 "cl.CLASSEMENT_BILAN, ".
  						 "cl.CLASSEMENT_COMPLEMENT_BILAN, ".
  						 "cl.CLASSEMENT_DOC_REF, ".
  						 " cl.CLASSEMENT_INDICE_CONFIANCE, ".  	
  						 $this->dbConn->compareSql("etat.ETAT_ID", "is", "NULL", "etat_indefini.ETAT_ID", "etat.ETAT_ID"). " as ETAT_ID, ".  						 
  						 $this->dbConn->compareSql("etat.ETAT_LIBELLE", "is", "NULL", "etat_indefini.ETAT_LIBELLE", "etat.ETAT_LIBELLE"). " as ETAT_LIBELLE, ".
  						 $this->dbConn->compareSql("etat.ETAT_COULEUR", "is", "NULL", "etat_indefini.ETAT_COULEUR", "etat.ETAT_COULEUR"). " as ETAT_COULEUR, ".
  						 $this->dbConn->compareSql("etat.ETAT_VALEUR", "is", "NULL", "etat_indefini.ETAT_VALEUR", "etat.ETAT_VALEUR"). " as ETAT_VALEUR, ".
							" st.STATUT_CODE, st.STATUT_ID, motif.MOTIF_CODE, motif.MOTIF_ID, cl.CLASSEMENT_ID, ".
  						"(ele.ELEMENT_QUALITE_RESTRICT_TYPEME is NULL or ele.ELEMENT_QUALITE_RESTRICT_TYPEME='' or ele.ELEMENT_QUALITE_RESTRICT_TYPEME=me.MASSE_TYPE) as B_AFF
							FROM ELEMENT_QUALITE ele
							LEFT JOIN TYPE_ELEMENT_QUALITE tele ON tele.TYPE_ELEMENT_QUALITE_ID = ele.TYPE_ELEMENT_QUALITE_ID
							LEFT JOIN TYPE_CLASSEMENT classement ON classement.TYPE_CLASSEMENT_ID = ele.TYPE_CLASSEMENT_ID
							LEFT JOIN CLASSEMENT_MASSE_EAU cl ON cl.ELEMENT_QUALITE_ID = ele.ELEMENT_QUALITE_ID and cl.SESSION_ID =  (
							SELECT max( SESSION_ID ) AS SESSION_ID
							FROM SESSION_QUALITE ) AND cl.MASSE_ID = (SELECT MASSE_ID FROM MASSE_EAU me WHERE me.MASSE_CODE = '".$codeMe."')
							LEFT JOIN MASSE_EAU me ON me.MASSE_ID = (SELECT MASSE_ID FROM MASSE_EAU me WHERE me.MASSE_CODE = '".$codeMe."')
							LEFT JOIN BASSIN_HYDROGRAPHIQUE b ON b.BASSIN_ID = me.BASSIN_ID
							LEFT JOIN CLASSEMENT_STATUT st ON st.STATUT_ID = cl.STATUT_ID
							LEFT JOIN ETAT_MOTIF motif ON motif.MOTIF_ID = cl.MOTIF_ID							
							LEFT JOIN ETAT etat ON etat.ETAT_ID = cl.ETAT_ID
							LEFT JOIN ETAT etat_indefini ON etat_indefini.TYPE_CLASSEMENT_ID = ele.TYPE_CLASSEMENT_ID and etat_indefini.ETAT_VALEUR = 0 and (etat_indefini.ETAT_ID = 8 or etat_indefini.ETAT_ID = 9)".  						
  						" where 1=1 ".
  						 ($iNiveau != "" ? " and ele.ELEMENT_QUALITE_NIVEAU=".$iNiveau : "");
  	
  		$strSql = "select * from (".
  						$strSqlTypeElement.
  						" UNION ".
  						$strSqlElement.") etat ".
  						" order by etat.TYPE_CLASSEMENT_LIBELLE, etat.TYPE_ELEMENT_QUALITE_NOM, ELEMENT_QUALITE_NIVEAU, ELEMENT_QUALITE_PARENT, ELEMENT_QUALITE_RANG ";  						

    $ds = $this->dbConn->initDataset( $strSql, $idFirst, $idLast );
    $ds->setTree("ELEMENT_QUALITE_ID", "ELEMENT_QUALITE_PARENT"); 						
		return $ds ;
  }  	
  
  function getDs_QualiteMasseEau($codeMe="", $element_qualite_id="", $type_element_qualite_id="") 
  {      
  	$strSql = "SELECT cl.*  
							FROM CLASSEMENT_MASSE_EAU cl where 1=1 ".
  						($type_element_qualite_id != "" ? " and cl.TYPE_ELEMENT_QUALITE_ID = ".$type_element_qualite_id 
  																						: ($element_qualite_id != "" ? " and cl.ELEMENT_QUALITE_ID = ".$element_qualite_id : "")).
  						" and cl.SESSION_ID =  (
							SELECT max( SESSION_ID ) AS SESSION_ID
							FROM SESSION_QUALITE ) AND cl.MASSE_ID = (SELECT MASSE_ID FROM MASSE_EAU me WHERE me.MASSE_CODE = '".$codeMe."')";
		return $this->dbConn->initDataset( $strSql ) ;  																					
  }	

  /**
   * @brief Retourne la liste des typologies des masses d'eau 
   *
   * @param typeMe  C pour cotieres, T pour transition
   *
   * @return Retourne un dataset
   */
  function getDs_listeTypologieMasseEau($typeMe = "C", $bassin_code="") 
  {          
    $strSql = "select distinct descr.MASSE_DESC_ID, MASSE_DESC_CODE, MASSE_DESC_LIBELLE, MASSE_DESC_COLOR ".
    					" from MASSE_EAU_DESCRIPTION descr, MASSE_EAU me".    		
    					" where MASSE_DESC_CODE like '".$typeMe."%' and me.MASSE_DESC_ID = descr.MASSE_DESC_ID".
    					($bassin_code != '' ? " and me.BASSIN_ID in (select BASSIN_ID from BASSIN_HYDROGRAPHIQUE where BASSIN_CODE = '".$bassin_code."')" : "");
    return $this->dbConn->initDataset( $strSql ) ;
  }  
  
  /**
   * @brief Retourne la liste des r�seaux d'appartenance pour un point
   *
   * @param codePoint  Code du point
   *
   * @return Retourne un dataset
   */
  function getDs_listeReseauByPoint($codePoint) 
  {           
    $strSql = "select distinct RESEAU_CODE from POINT, MASSE_EAU me, RESEAU_POINT_PARAM rpp, RESEAU reso".    
      " where POINT.MASSE_ID = me.MASSE_ID" .
      " and POINT.POINT_CODE = '".$this->dbConn->analyseSql($codePoint)."'".
      " and rpp.POINT_ID=POINT.POINT_ID".
      " and reso.RESEAU_ID=rpp.RESEAU_ID";
    return $this->dbConn->initDataset( $strSql ) ;
  }  
  
 /**
   * @brief Retourne la liste des methodes d'�valuation pour une entite (type classement, type etat, element de qualite)
   *
   * @param type_classement_id
   * @param TYPE_ELEMENT_QUALITE_ID 
   * @param ELEMENT_QUALITE_ID 
   *
   * @return Retourne un dataset
   */
  function getDs_ficheMethode($type_classement_id, $type_element_qualite_id, $element_qualite_id) 
  {           
    $strSql = "select * from METHODE_EVALUATION".    
      " where TYPE_CLASSEMENT_ID ".($type_classement_id != '' ? " = " .$type_classement_id : " is NULL").
      " and TYPE_ELEMENT_QUALITE_ID ".($type_element_qualite_id != '' ? " = " .$type_element_qualite_id : " is NULL").
      " and ELEMENT_QUALITE_ID ".($element_qualite_id != '' ? " = " .$element_qualite_id : " is NULL");
    return $this->dbConn->initDataset( $strSql ) ;
  }    
  
 /**
   * @brief Retourne la fiche d'un etat
   *
   * @param id  identifiant de l'�tat 
   *
   * @return Retourne un dataset
   */
  function getDs_MethodeById($id) 
  {          
    $strSql = "select *, METHODE_ID ID, METHODE_LIBELLE LIB from METHODE_EVALUATION".
    					" where 1=1".
    					($id != "" ? " and METHODE_ID = ".$id : "");    					
    					
    return $this->dbConn->initDataset( $strSql ) ;
  }  
  
  /**
   * @brief Retourne la liste des dictiionnaires relatifs � l'�tat de qualit� des masses d'eau
   *
   * @param strTabledico  Nom de la table dictionnaire 
   * @param strPrefixe  	Pr�fixe pr�c�dant le nom du champ 
   *
   * @return Retourne un dataset
   */
  function getDs_listeMethode($idFirst=0, $idLast=-1)
  {          
    $strSql = "select METHODE_ID ID, ".
              $this->dbConn->getConcat("'M�thode pour '",  
    					$this->dbConn->CompareSql("tc.TYPE_CLASSEMENT_ID", "is not", "NULL", $this->dbConn->getConcat("'�tat '", "tc.TYPE_CLASSEMENT_LIBELLE"), 
    					    $this->dbConn->CompareSql("tqual.TYPE_ELEMENT_QUALITE_ID", "is not", "NULL", $this->dbConn->getConcat("'�tat '", "tqual.TYPE_ELEMENT_QUALITE_NOM", "' (type �l�ment qualit�)'"), 
    					        $this->dbConn->CompareSql("equ.ELEMENT_QUALITE_ID", "is not", "NULL", $this->dbConn->getConcat("'�l�ment de qualit� '", "equ.ELEMENT_QUALITE_NOM"), "'�tat global'"))))." as LIB from METHODE_EVALUATION meth".
              " LEFT JOIN TYPE_CLASSEMENT tc on tc.TYPE_CLASSEMENT_ID = meth.TYPE_CLASSEMENT_ID".
              " LEFT JOIN TYPE_ELEMENT_QUALITE tqual on tqual.TYPE_ELEMENT_QUALITE_ID = meth.TYPE_ELEMENT_QUALITE_ID".
              " LEFT JOIN ELEMENT_QUALITE equ on equ.ELEMENT_QUALITE_ID = meth.ELEMENT_QUALITE_ID".
    					" order by LIB";
    return $this->dbConn->initDataset( $strSql, $idFirst, $idLast ) ;
  }    
}
?>