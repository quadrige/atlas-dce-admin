<?php

/**
 * @brief Classe de l'application atlas
 *        Classe regroupant des fonctionnalit�s de l'appli atlas
 */
class AlkAppliAtlas
{
  /** */
  var $oSpace;
  var $appli_id;
  var $atype_id;
  var $oQuery;
  var $oQueryAction;
  var $agent_id;

  /** identifiant de l'onglet s�lectionn� */
  var $iSheet;

  /** identifiant du sous onglet s�lectionn� */
  var $iSSheet;

  /** mode du sheet : ALK_MODE_SSHEET_LIST, ALK_MODE_SSHEET_FORM, ALK_MODE_SSHEET_SQL, ALK_MODE_SSHEET_HIDDEN */
  var $iModeSSheet;

  /** identifiant du type en cours (-1 si pas de type particulier) */
  var $type_id;

  /** identifiant du type en cours (-1 si pas de type particulier) */
  var $iModeForm;

  /**
   * @brief Constructeur par d�faut
   *
   */
  function AlkAppliAtlas($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet)
  {
    global $queryAtlas, $queryAtlasAction;

    $this->appli_id = $appli_id;
    $this->atype_id = -1;
    $this->oQuery =& $queryAtlas;
    $this->oQueryAction =& $queryAtlasAction;
    $this->agent_id = $agent_id;

    $this->iSheet = $iSheet;
    $this->iSSheet = $iSSheet;
    $this->iModeSSheet = $iModeSSheet;

    $this->initVarGetPost();    
  }

  /**
   * @brief Initialise les attributs provenant le l'url
   */
  function initVarGetPost()
  {
    $this->iMode = ( isset($_REQUEST["iMode"]) ? $_REQUEST["iMode"] : "-1");    
  }

  //
  // Ensemble des m�thodes concernant l'interface
  //

  /**
   * @brief Retourne le code html de l'entete de page
   *
   * @return Retourne un string
   */
  function getHtmlPageHeader()
  {
    global $PHP_SELF;  
    
    $oCtrl = new HtmlSelect(0, "bassin_admin", $_SESSION["idBassin"], "bassin_admin", 1);
    
    $oCtrl->bWriteId=true;
		//$oCtrl->oValTxt = $this->oQuery->getTableForCombo("BASSIN_HYDROGRAPHIQUE", "BASSIN");
    
    if($_SESSION["bAdminUserAllBassin"] == 1){
      // selction de tous les bassin
      $oCtrl->oValTxt = $this->oQuery->getTableForCombo("BASSIN_HYDROGRAPHIQUE", "BASSIN");
    }else{
      $oCtrl->oValTxt = $this->oQuery->getListBassinByUserForCombo($_SESSION["sit_idUser"]);     
    }
    
    $oCtrl->addEvent("onchange", "majBassinCourant();");

	if ($_SESSION["idBassin"]==-1) {
      if( $oCtrl->oValTxt!=null &&
          is_object($oCtrl->oValTxt) &&
          method_exists($oCtrl->oValTxt, "getRowIter")==true ) {
            $drSelect = $oCtrl->oValTxt->getRowIter(); 
            $_SESSION["idBassin"] = $drSelect->getValueNum(1);       
          }     
    }   
    $oCtrl->oValTxt->MoveFirst();
    	
    $strNom = $_SESSION["sit_userprenom"]."&nbsp;".$_SESSION["sit_usernom"]."&nbsp;";
    $oCtrlDeconnect = new HtmlLink("javascript:CloseSession()", 
                               "Se d�connecter", "", "");
    
    $strHtml = "<html><head>".
      "<title>".ALK_APP_NAME."</title>".
      "<meta name='title' content=''>".
      "<meta name='description' content=''>".
      "<meta NAME='language' content='FR'>";

    if( STRNAVIGATOR == NAV_NETSCAPE4 ) { 
      $strHtml .= "<link rel='stylesheet' href='".ALK_SIALKE_URL."styles/menu.css' type='text/css'>";
    } else {
      $strHtml .= "<link rel='stylesheet' href='".ALK_SIALKE_URL."styles/menu_ie.css' type='text/css'>";
    } 

    $strHtml .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>".
      "<script language='javascript' src='../../lib/lib_js.js'></script>".
      "<script language='javascript' src='../../lib/lib_jsaddon.js'></script>".
      "<script language='javascript' src='../../lib/lib_ajax.js'></script>".
      "<script language='JavaScript'>".
      " navigateur = null;".
      " x = null;".
      " y = null;".
      " bWinLoaded = 0;".
      " menuBar = null;".
      " ALK_SIALKE_URL = '".ALK_SIALKE_URL."';".
      " ALK_URL_IDENTIFICATION = '".ALK_URL_IDENTIFICATION."';".
      " ALK_ROOT_URL = '".ALK_ROOT_URL."';".
      
      "function onLoadInit() { bWinLoaded = 1; document.onmousemove = position;  ".
      
      /** Actions sp�cifiques � l'appli sur body onload */
      ( method_exists($this, "getHtmlJsOnLoad") 
        ? $this->getHtmlJsOnLoad() 
        : ( function_exists("getHtmlJsOnLoad") ? getHtmlJsOnLoad() : "" )).
        
      " } ".
      
      "</script>".
      "</head>".

      "<body bgcolor='#FFFFFF' text='#000000' ".
      " leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'".
      " onload=\"onLoadInit();\">".

      "<table border='0' cellpadding='0' cellspacing='0'  align='center' width='1000'><tr>".      
      "<td width='1000' bgcolor='#ffffff' ><img src='".ALK_URL_SI_MEDIA."menu/bandeau_haut.jpg'></td>".
      "</tr>".
      "</table>".
      "<table border='0' cellpadding='0' cellspacing='0'  align='center' width='1000'><tr>".        
      
      /*($_SESSION["bAdminUserAllBassin"] == 1 
      ?"<tr><td align='left' width='500' class='formLabel'>".$strNom.$oCtrlDeconnect->getHtml()."</td><td align='right' colspan='2' class='formLabel' height='2' width='500' >S�lectionner le bassin de travail&nbsp;".$oCtrl->getHtml()."</td></tr>"
      :"<tr><td align='left' width='500' class='formLabel'>".$strNom.$oCtrlDeconnect->getHtml()."</td><td colspan='2' height='2' width='500' ></td></tr>").            
      */
      "<tr><td align='left' width='500' class='formLabel'>".$strNom.$oCtrlDeconnect->getHtml()."</td><td align='right' colspan='2' class='formLabel' height='2' width='500' >S�lectionner le bassin de travail&nbsp;".$oCtrl->getHtml()."</td></tr>".
      "</table>";
    
    return $strHtml;
  }

  /**
   * @brief Retourne le code html de l'entete de page
   *
   * @return Retourne un string
   */
  function getHtmlPagePopupHeader()
  {
    global $PHP_SELF;

    $strHtml = "<html><head>".
      "<title>".ALK_APP_NAME."</title>".
      "<meta name='title' content=''>".
      "<meta name='description' content=''>".
      "<meta NAME='language' content='FR'>";

		if( ALK_B_PRINT==true && isset($_GET["print"]) && $_GET["print"]=="1" ) {
      $strHtml .= "<link rel='stylesheet' href='".ALK_SIALKE_URL."styles/si_print.css' type='text/css'>";
    } elseif( STRNAVIGATOR == NAV_NETSCAPE4 ) { 
      $strHtml .= "<link rel='stylesheet' href='".ALK_SIALKE_URL."styles/menu.css' type='text/css'>";
    } else {
      $strHtml .= "<link rel='stylesheet' href='".ALK_SIALKE_URL."styles/menu_ie.css' type='text/css'>";
    } 
    $strHtml .= "<link rel='stylesheet' href='".ALK_SIALKE_URL."styles/menu.css' type='text/css'>";
    //$strHtml .= "<link rel='stylesheet' href='".ALK_SIALKE_URL."styles/site_gen.css' type='text/css'>";
    $strHtml .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>".
      "<script language='javascript' src='../../lib/lib_js.js'></script>".
      "<script language='JavaScript'>".
      " navigateur = null;".
      " x = null;".
      " y = null;".
      " bWinLoaded = 0;".
      " menuBar = null;".
      " ALK_SIALKE_URL = '".ALK_SIALKE_URL."';".
      " ALK_URL_IDENTIFICATION = '".ALK_URL_IDENTIFICATION."';".
      " ALK_ROOT_URL = '".ALK_ROOT_URL."';".
      
      "function onLoadInit() { bWinLoaded = 1; document.onmousemove = position; } ".
      "</script>".
      "</head>".

      "<body bgcolor='#E2E6F4' text='#000000' ".
      " leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'".
      " onload=\"onLoadInit();\"><div align='center'>";
    
    return $strHtml;
  }

  /**
   * @brief M�thode virtuelle qui retourne un tableau de boutons htmllink
   *        plac� sur la droite des onglets
   * 
   * @param 
   * @return Retourne un array
   */
  function getTabCtrlBt()
  {
    $iCptBt = 0;
    $tabBt = array();

    return $tabBt;
  }

  /**
   * @brief Retourne le code html de la partie onglets + boutons
   *
   * @param 
   * @param 
   * @param 
   * @param 
   */
  function getHtmlSheetHeader()
  {
    $strImgSheetDeb = "ongletdeb.gif";
    $strImgSheetDebRol = "ongletdeb_rol.gif";
    $strImgSheet = "onglet.gif";
    $strImgSheetRol = "onglet_rol.gif";
    $strImgSheetEnd = "onglet_end.gif";
    $strImgSheetEndRol = "onglet_end_rol.gif";
    $strImgSheetToSheet = "onglet_onglet.gif";
    $strImgSheetRolToSheet = "ongletrol_onglet.gif";
    $strImgSheetToSheetRol = "onglet_ongletrol.gif";

    $strParam = "";

    $tabSheet = array();
    $i = 0;
    $tabSheet[$i++] = array("idSheet" => ALK_SHEET_ATLAS, 
                              "text"    => "R�seaux de surveillance",
                              "url"     => "01_page_form.php?".$strParam."&iSheet=".ALK_SHEET_ATLAS,
                              "title"   => "");
		$tabSheet[$i++] = array("idSheet" => ALK_SHEET_QUALITE, 
                              "text"    => "Qualit� des masses d'eau",
                              "url"     => "01_page_form.php?".$strParam."&iSheet=".ALK_SHEET_QUALITE,
                              "title"   => "");
		$tabSheet[$i++] = array("idSheet" => ALK_SHEET_MASSE_EAU, 
                              "text"    => "Gestion des masses d'eau",
                              "url"     => "01_page_form.php?".$strParam."&iSheet=".ALK_SHEET_MASSE_EAU,
                              "title"   => "");
    $tabSheet[$i++] = array("idSheet" => ALK_SHEET_ANNU, 
                              "text"    => "Annuaire",
                              "url"     => "01_page_form.php?".$strParam."&iSheet=".ALK_SHEET_ANNU."&iSSheet=".ALK_SSHEET_ANNU_LISTE,
                              "title"   => "");
        
    $strHtml = "<table border='0' cellpadding='0' cellspacing='0' width='1000' align='center'><tr>";

    for($i=0; $i<count($tabSheet); $i++) {
      if( $i == 0 ) {
        // bordure du premier onglet
        if( $this->iSheet == $tabSheet[$i]["idSheet"] ) {
          $strHtml .= "<td height='21' width='1'><img src='".ALK_URL_SI_IMAGES.$strImgSheetDebRol."'></td>";
        } else {
          $strHtml .= "<td height='21' width='1'><img src='".ALK_URL_SI_IMAGES.$strImgSheetDeb."'></td>";
        }
      }
      if( $this->iSheet == $tabSheet[$i]["idSheet"] )
        $strHtml .= "<td height='21' background='".ALK_URL_SI_IMAGES.$strImgSheetRol."'".
          " style='padding-left:15px; padding-right:15px' valign='bottom'>".
          "<span class='aSheet'>".$tabSheet[$i]["text"]."</span></td>";
      else
        $strHtml .= "<td height='21' background='".ALK_URL_SI_IMAGES.$strImgSheet."' valign='bottom'".
          " style='padding-left:15px; padding-right:15px' valign='bottom'>".
          "<a class='aSheetRol' title=\"".$tabSheet[$i]["title"]."\" href=\"".$tabSheet[$i]["url"]."\"".
          ">".$tabSheet[$i]["text"]."</a></td>";

      // transition avec l'onglet suivant
      if( ($i+1) == count($tabSheet) ) {
        // dernier onglet
        if( $this->iSheet == $tabSheet[$i]["idSheet"] )
          // dernier select
          $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$strImgSheetEndRol."' width='17' height='21'>".
            "<img src='".ALK_URL_SI_MEDIA."transp.gif' width='17' height='1'></td>";
        else
          //dernier normal
          $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$strImgSheetEnd."' width='17' height='21'>".
            "<img src='".ALK_URL_SI_MEDIA."transp.gif' width='17' height='1'></td>";
      } else {
        // transition intermediaire
        if( $this->iSheet == $tabSheet[$i]["idSheet"] ) {
          // select puis normal
          $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$strImgSheetRolToSheet."' width='17' height='21'>".
            "<img src='".ALK_URL_SI_MEDIA."transp.gif' width='17' height='1'></td>";
        } elseif( $i+1<count($tabSheet) && $this->iSheet == $tabSheet[$i+1]["idSheet"]) {
          // normal puis select
          $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$strImgSheetToSheetRol."' width='17' height='21'>".
            "<img src='".ALK_URL_SI_MEDIA."transp.gif' width='17' height='1'></td>";
        } else {
          // normal puis normal
          $strHtml .= "<td background='".ALK_URL_SI_IMAGES.$strImgSheetToSheet."' width='17' height='21'>".
            "<img src='".ALK_URL_SI_MEDIA."transp.gif' width='17' height='1'></td>";
        }
      }
    }

    // transition elastique entre onglets et boutons
    $tabBt = $this->getTabCtrlBt();

    if( count($tabBt) > 0 )
      $strHtml .= "<td width='100%'></td>";

    for($i=0; $i< count($tabBt); $i++) {
      $strHtml .= "<td align='right' style='padding-left:1px; padding-right:1px;'>";
      if( is_object($tabBt[$i]) && get_class($tabBt[$i]) == "htmllink" )
        $strHtml .= $tabBt[$i]->getHtml();
      else
        $strHtml .= $tabBt[$i];
      $strHtml .= "</td>"; 
    }
    $strHtml .= "</tr></table>".
      "<table border='0' cellpadding='0' cellspacing='0' class='tableSheet'".
      " height='470' width='1000' align='center'>".
      "<tr><td valign='top' align='left'>";

    return $strHtml;
  }

  /**
   * @brief M�thode virtuelle, retourne un tableau contenant les informations sur les sous onglets
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un array
   */
  function getTabSubSheet()
  {
    global $tabStrType;

    $tabSubSheet = array();
    return $tabSubSheet;
  }

  /**
   * @brief Retourne la liste des sous onglets.
   *        Si l'un des sous-onglets sont vides, ne l'affiche pas.
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un string : code html des sous onglets
   */
  function getHtmlSubSheet()
  {
    $tabSubSheet = $this->getTabSubSheet();

    if( count($tabSubSheet) == 0 ) return "";

    $strHtml = "<table border='0' cellpadding='0' cellspacing='2' class='tableSubSheet' width='100%'><tr>";
    for($i=0; $i<count($tabSubSheet); $i++) {
      if( $tabSubSheet[$i]["idSSheet"] == $this->iSSheet )
        $strHtml .= "<td class='tdSubSheetRol'><a class='aSSheetRol'";
      else
        $strHtml .= "<td class='tdSubSheet'><a class='aSSheet'";
        
      $strHtml .= "title=\"".$tabSubSheet[$i]["title"]."\" href=\"".$tabSubSheet[$i]["url"]."\">".
        $tabSubSheet[$i]["text"]."</a></td>";
    }
    $strHtml .= "<td width='100%'></td></tr></table>";

    return $strHtml;
  }

  /**
   * @brief M�thode virtuelle, retourne le contenu html du corps de l'onglet s�lectionn�
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un string : code html des sous onglets
   */
  function getHtmlBodySheet()
  {
    $strHtml = "";
    return $strHtml;
  }

  /**
   * @brief Retourne le code html du d�but de l'onglet
   *
   * @return Retourne un string
   */
  function getHtmlStartofSheet($bWithSSheet=true)
  {
    $iPrint = Request("print", REQ_GET, "0", "is_numeric");
    $iModeFormValide = Request("iModeForm", REQ_GET, "0", "is_numeric");

    if( $bWithSSheet == true && $iPrint=="0" && $iModeFormValide == "0" ) 
      $strHtml = "<div>"; // style='overflow:auto; height:400px'>";
    else 
      $strHtml = "<div>";
    return $strHtml;
  }
  
  /**
   * @brief Retourne la liste des sous onglets.
   *        Si l'un des sous-onglets sont vides, ne l'affiche pas.
   *
   *
   */
  function getHtmlEndofSheet()
  {
    $strHtml = "</div></td></tr></table>";
    return $strHtml;
  }
 
  /**
   * @brief Retourne le code html de bas de page
   *
   * @return Retourne un string
   */
  function getHtmlPageFooter()
  {
    $strHtml = "</body></html>";

    return $strHtml;
  }

  /**
   * @brief Retourne le code html de bas de page
   *
   * @return Retourne un string
   */
  function getHtmlPagePopupFooter()
  {
    $strHtml = "</div></body></html>";

    return $strHtml;
  }

  /**
   * @brief Retourne l'entete d'une table liste de r�sultats
   * 
   * @param tabLibelle libelle de la colonne
   * @param tabAlign   contient l'alignement des colonnes
   * @param tabWidth   tableau des largeurs de colonnes
   * @param bAffNbRes  =vrai par d�faut, pour afficher le nombre d'�l�ments dans la liste
   * @return Retourne un string
   */
  function GetHtmlEnteteTableList($tabLibelle, $tabAlign, $tabWidth, $bAffNbRes=true)
  {
    global $nbElt;
    $nbCol = count($tabLibelle);

    $strHtml = "";
    if( $bAffNbRes == true )
      $strHtml .= "<div align=center><div class=divContenuTexte>".
        "Nombre de r�ponses &agrave; vos crit&egrave;res : ".$nbElt."</div>";

    $strHtml .= "<table class=table1 style='margin-left:8px' border=0 cellpadding=2 cellspacing=1><tr>";
    
    foreach ($tabWidth as $i => $width) {
      $strHtml .= "<td width=".$width." height=0></td>";
    } 

    $strHtml .= "</tr><tr class=trEntete1>";
        
    foreach ($tabLibelle as $i => $libelle) {
      $strHtml .= "<td class='tdEntete1' align=".$tabAlign[$i+1]."><div class=divTabEntete1>".$libelle."</div></td>";
    }
        
    $strHtml .= "</tr>";
    
    return $strHtml;
  }

  //
  // Ensemble des m�thodes priv�es
  //

  /**
   * @brief Retourne le code html du cadre titre � l'interieur d'un onglet
   *
   * @param strTitle Titre du cadre
   * @return Retourne un string
   */ 
  function _getFrameTitleSheet($strTitle, $bForm=false)
  {
    $strHtml = "<table border='0' cellpadding='0' cellspacing='0' style='margin-top:10px'>".
      "<tr>".
      "<td width='10' rowspan='2'><img src='".ALK_URL_SI_MEDIA."transp.gif' width='10' height='1'></td>".
      "<td width='100%' class='".($bForm==true ? "tdFrameTitleSheetForm" : "tdFrameTitleSheet" )."'>".
        $strTitle."</td>".
      "</tr>".
      "</table>";

    return $strHtml;
  }

  function getHtmlJsOnLoad()
  {
    return "";
  }

}
?>