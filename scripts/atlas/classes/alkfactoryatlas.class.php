<?php

/**
 * @brief Classe de l'application concours
 *        Classe regroupant des fonctionnalités de l'appli concours
 */
class AlkFactoryAtlas
{
  /** */
  var $oAppli;

  /**
   * @brief Constructeur par défaut
   *
   */
  function AlkFactoryAtlas()
  {
    $this->oAppli = null;

    $this->initAppli();
  }

  /**
   * @brief Initialise les attributs provenant le l'url
   */
  function initAppli()
  {
    $iSheet = Request("iSheet", REQ_GET, 0, "is_numeric");
    $iSSheet = Request("iSSheet", REQ_GET, 0, "is_numeric");
    $iModeSSheet = Request("iModeSSheet", REQ_GET, 0, "is_numeric");
    $agent_id = 0; //$_SESSION["concours_agent_id"];
    $appli_id = 0;

    switch( $iSheet ) {
    case ALK_SHEET_ATLAS:
      include_once("classes/alkappliatlas_param.class.php");
      $this->oAppli = new AlkAppliAtlas_param($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet);
      break;
    case ALK_SHEET_QUALITE:
      include_once("classes/alkappliatlas_qualite.class.php");
      $this->oAppli = new AlkAppliAtlas_qualite($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet);
      break;
    case ALK_SHEET_MASSE_EAU:
      include_once("classes/alkappliatlas_masse.class.php");
      $this->oAppli = new AlkAppliAtlas_masse($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet);
      break;  
    case ALK_SHEET_ANNU:
      include_once("classes/alkappliatlas_annu.class.php");
      $this->oAppli = new AlkAppliAtlas_annu($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet);
      break;    
    }
  }
}

?>