<?
include_once "queryatlas_action.class.php";

/**
 * @Class queryActuActionMysql
 *
 * @brief Ensemble des requetes actions li�es � l'application Actualites sp�cialis�es Mysql
 *         Les requetes standards se trouvent dans la classe m�re
 */
class QueryAtlasActionMysql extends QueryAtlasAction
{
  /**
   * @brief Constructeur
   *
   * @param oDb       classe de connexion � la base
   * @param tabLangue tableau des langues utilis�es
   */ 
  function QueryAtlasActionMysql(&$oDb, $tabLangue)
  {
    parent::QueryAtlasAction($oDb, $tabLangue);
  }
}
?>