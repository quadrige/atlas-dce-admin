<?php
include_once("../../classes/appli/alkimport.class.php");

/**
 * @class AlkImportAnnu
 * @copyright Alkante
 * @brief Classe d'import pour l'annuaire
 */
class AlkImportAtlas extends AlkImport
{
	
	
	/**
   * @brief Constructeur de la classe Import : initialisation des attributs de Import.
   *
   * @param strFichier_Import Emplacement  du fichier d'import
   * @param tabImport  Tableau contenant les propriet�s des  colonnes
   * @param tabSup  Tableau contenant les colonnes obligatoires pour la suppression
   * @param tabAjoutt  Tableau contenant les colonnes obligatoires pour l'ajouts
   * @param objErreur  Nom de l'objet  ImportErreur
   */
	function AlkImportAtlas($strFichier_Import,  $tabAlkImportCol, $tabSup, $tabAjout, 
                         $tabModif, &$obj_Erreur,&$objWarning, &$oQuery, &$oQueryAction )
	{
		parent::AlkImport($strFichier_Import,  $tabAlkImportCol, $tabSup, $tabAjout, 
                      $tabModif, $obj_Erreur,$objWarning, $oQuery, $oQueryAction, $oQuery );
	}
	
  /**
   * @brief Fonction qui rend un tableaux de champs valeurs.
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return tabChampsData  un tableau de champs valeurs
   */
	function getChampsData($tabLigne, $tabCopy, $tabLigneRef, $bUpdate=false)
	{
    if( $bUpdate == false ) {
      // mode ajout
      $tabChampsData = array("RESEAU_CODE"    => "-1", "SUPPORT_CODE"      => "-1", "POINT_CODE" => "-1", "PARAM_CODE"  => "-1",
      											 "RESEAU_ID"    => "-1", "SUPPORT_ID"      => "-1", "POINT_ID" => "-1", "PARAM_ID"  => "-1",
                             "OPERATEUR_TERRAIN" => "",   "OPERATEUR_LABO" => "",   
                             "OPERATEUR_TERRAIN_ID" => "",   "OPERATEUR_LABO_ID" => "",
														 "ANNEE"    => "0", "FREQUENCE" => "", "FREQUENCE_PLAN" => "", "PERIODE" => "");
    } else {
      // mode modif
      $tabChampsData = array("RESEAU_CODE"    => "-2", "SUPPORT_CODE"      => "-2", "POINT_CODE" => "-2", "PARAM_CODE"  => "-2",
                             "RESEAU_ID"    => "-1", "SUPPORT_ID"      => "-1", "POINT_ID" => "-1", "PARAM_ID"  => "-1",
														 "OPERATEUR_TERRAIN" => "-2",   "OPERATEUR_LABO" => "-2",   
														 "OPERATEUR_TERRAIN_ID" => "-2",   "OPERATEUR_LABO_ID" => "-2",
														 "ANNEE"    => "-2", "FREQUENCE" => "-2", "FREQUENCE_PLAN" => "-2", "PERIODE" => "-2");
    }
		$tabChamps = array();
		
		if (in_array("RESEAU",$tabLigneRef) && ($tabLigne[$tabCopy["RESEAU"]] != "")) 
      $tabChampsData["RESEAU_CODE"] = $tabLigne[$tabCopy["RESEAU"]];

		if (in_array("SUPPORT",$tabLigneRef) && ($tabLigne[$tabCopy["SUPPORT"]] != "")) 
      $tabChampsData["SUPPORT_CODE"] = $tabLigne[$tabCopy["SUPPORT"]];

		if (in_array("POINT",$tabLigneRef) && ($tabLigne[$tabCopy["POINT"]] != "")) 
      $tabChampsData["POINT_CODE"] = $tabLigne[$tabCopy["POINT"]];

		if (in_array("PARAM", $tabLigneRef) && ($tabLigne[$tabCopy["PARAM"]] != "")) 
      $tabChampsData["PARAM_CODE"] = $tabLigne[$tabCopy["PARAM"]];

		if (in_array("OPE_TERRAIN",$tabLigneRef) && ($tabLigne[$tabCopy["OPE_TERRAIN"]] != "")) 
      $tabChampsData["OPERATEUR_TERRAIN"] = $tabLigne[$tabCopy["OPE_TERRAIN"]];

		if (in_array("OPE_LABO",$tabLigneRef) && ($tabLigne[$tabCopy["OPE_LABO"]] != "")) 
      $tabChampsData["OPERATEUR_LABO"] = $tabLigne[$tabCopy["OPE_LABO"]];
		
		if (in_array("ANNEE",$tabLigneRef) && ($tabLigne[$tabCopy["ANNEE"]] != "")) 
      $tabChampsData["ANNEE"] = $tabLigne[$tabCopy["ANNEE"]];
      
    if (in_array("FREQUENCE",$tabLigneRef) && ($tabLigne[$tabCopy["FREQUENCE"]] != "")) 
      $tabChampsData["FREQUENCE"] = $tabLigne[$tabCopy["FREQUENCE"]];
      
    if (in_array("FREQUENCE_PLAN",$tabLigneRef) && ($tabLigne[$tabCopy["FREQUENCE_PLAN"]] != "")) 
      $tabChampsData["FREQUENCE_PLAN"] = $tabLigne[$tabCopy["FREQUENCE_PLAN"]];    		

    if (in_array("PERIODE",$tabLigneRef) && ($tabLigne[$tabCopy["PERIODE"]] != "")) 
      $tabChampsData["PERIODE"] = $tabLigne[$tabCopy["PERIODE"]];    		
		
		return 	$tabChamps = array($tabChampsData);
	}
	
  /**
   * @brief Fonction insertion de la ligne courante .
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return vrai si ibsertion faux sinon
   */
	function insert($tabLigne, $tabCopy, $tabLigneRef, $t)
	{
		$tabChamps = $this->getChampsData($tabLigne, $tabCopy, $tabLigneRef);

		$tabChampsData = $tabChamps[0];

    $iWarning = 0;
		
		$bErr = $this->_VerificationCode($tabChampsData, $t);
		if ($bErr)
		  return 2;
		 
		//Suppression de l'�ventuelle association d�j� existante  
    $bRes = $this->oQueryAction->delParam($tabChampsData["RESEAU_ID"], 
    																				 $tabChampsData["POINT_ID"],
    																				 $tabChampsData["PARAM_ID"]);
    
		//ajout 
		$bRes = $this->oQueryAction->addParam($tabChampsData["RESEAU_ID"], 
    																		  $tabChampsData["POINT_ID"],
    																			$tabChampsData["PARAM_ID"]);
    																			   
		$bRes = $this->oQueryAction->updateParam($tabChampsData["RESEAU_ID"], 
    																		  $tabChampsData["POINT_ID"],
    																			$tabChampsData["PARAM_ID"], $tabChampsData["OPERATEUR_LABO_ID"], $tabChampsData["OPERATEUR_TERRAIN_ID"], 
    																			$tabChampsData["ANNEE"], $tabChampsData["FREQUENCE"], $tabChampsData["PERIODE"], $tabChampsData["FREQUENCE_PLAN"]);
    if( $bRes < 1 ) {
      $strMes = "Erreur : Ligne ".$t.", impossible d'ajouter les donn�es. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);      
      return 2;
    }
    				
		if( $iWarning != 0 ) {
			return 1;
		} else {
			return 0;
		}
	}
	
	/**
   * @brief Fonction de modification de la ligne courante dans la base .
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return vrai si ibsertion faux sinon
   */
	function update($tabLigne, $tabCopy, $tabLigneRef,$t)
	{
		$tabChamps = $this->getChampsData($tabLigne, $tabCopy, $tabLigneRef, true);
		$tabChampsData = $tabChamps[0];
    $iWarning = 0;
		$bErr = $this->_VerificationCode($tabChampsData, $t);
		if ($bErr)
		  return 2;
    
    // charge la fiche
		$dsParam = $this->oQuery->getDs_ParametrePoint($tabChampsData["RESEAU_ID"], 
    																		  "-1",    																		  
    																		  $tabChampsData["POINT_ID"],
    																		  false,
    																			$tabChampsData["PARAM_ID"]);
		if( $drParam = $dsParam->getRowIter() ) {
			$strAnnee = $drParam->getValueName("ANNEE_PRELEVEMENT");
			$strFrequence = $drParam->getValueName("FREQUENCE");
			$strFrequencePlan = $drParam->getValueName("FREQUENCE_PLAN");
			$strPeriode = $drParam->getValueName("PERIODE");
			$idOpeTerrain = $drParam->getValueName("OPERATEUR_TERRAIN");
			$idOpeLabo = $drParam->getValueName("OPERATEUR_LABO");
		} else {      
      $strMes = "Erreur : Ligne ".$t.", fiche non trouv�e pour modification. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);
      return 2;
    }
    
		if( $tabChampsData["ANNEE"] != "-2" ) 		$strAnnee = $tabChampsData["ANNEE"];
		if( $tabChampsData["FREQUENCE"] != "-2" ) $strFrequence = $tabChampsData["FREQUENCE"];
		if( $tabChampsData["FREQUENCE_PLAN"] != "-2" ) $strFrequencePlan = $tabChampsData["FREQUENCE_PLAN"];
		if( $tabChampsData["PERIODE"] != "-2" ) 	$strPeriode = $tabChampsData["PERIODE"];
		if( $tabChampsData["OPERATEUR_TERRAIN_ID"] != "-2" ) $idOpeTerrain = $tabChampsData["OPERATEUR_TERRAIN_ID"];
		if( $tabChampsData["OPERATEUR_LABO_ID"] != "-2" ) $idOpeLabo = $tabChampsData["OPERATEUR_LABO_ID"];		

    $this->oQueryAction->updateParam($tabChampsData["RESEAU_ID"], 
    																 $tabChampsData["POINT_ID"],
    																 $tabChampsData["PARAM_ID"], 
    																 $idOpeLabo, $idOpeTerrain, 
    																 $strAnnee, $strFrequence, $strPeriode, $strFrequencePlan);
    																		
   if( $iWarning != 0 ) {
			return 2;
		} else {
			return 0;
		}
	}
	
	/**
   * @brief Fonction de suppresion de la ligne courante dans la base .
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return vrai si insertion faux sinon
   */
	function delete($tabLigne, $tabCopy, $tabLigneRef,$t)
	{
		$tabChamps = $this->getchampsData($tabLigne, $tabCopy, $tabLigneRef);

		$tabChampsData = $tabChamps[0];
		$bErr = $this->_VerificationCode($tabChampsData, $t);
		if ($bErr)
		  return 2;
		
		$bRes = $this->oQueryAction->delParam($tabChampsData["RESEAU_ID"], 
    																				 $tabChampsData["POINT_ID"],
    																				 $tabChampsData["PARAM_ID"]);
    
		if( ! $bRes ) {			
      $strMes = "Erreur : Ligne ".$t.", impossible de supprimer la fiche. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);      
      return 2;
		}
		
    return 0;
	}
	
	/**
   * @brief retourne la reference d'un champ..
   *
   * @param  champ  l'intitule du champs
   * @param  tabType tableau contenant le champs 
   * @return tabRes[0] la reference recherche dans la tabType et celle de la premiere case.
   */
	function getIdChamp($champ,$tabType)
	{
		$tabRes = array_keys($tabType, $champ);
    if( count($tabRes) > 0 ) 
      return $tabRes[0];
    return -1;
	}
	
	function _VerificationCode(&$tabChampsData, $t){
		$bErr = false;
		
		// v�rifie si le code parametre existe
    $dsParam = $this->oQuery->getParametreByCode($tabChampsData["PARAM_CODE"], $tabChampsData["SUPPORT_CODE"], $_SESSION["idBassin"]);
    if(! $drParam = $dsParam->getRowIter()) {
      $strMes = "Erreur : Ligne ".$t.", code param�tre ou code support inexistant ou param�tre n'appartenant pas au support. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);
      return true;
    } else {
    	$tabChampsData["PARAM_ID"] = $drParam->getValueName("PARAMETRE_ID");
    	$tabChampsData["SUPPORT_ID"] = $drParam->getValueName("SUPPORT_ID");
    }
    
    // v�rifie si le code reseau existe
    $dsParam = $this->oQuery->getReseauByCode($tabChampsData["RESEAU_CODE"], $_SESSION["idBassin"]);
    if(! $drParam = $dsParam->getRowIter()) {
      $strMes = "Erreur : Ligne ".$t.", code r�seau inexistant. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);
      return true;
    } else {
    	$tabChampsData["RESEAU_ID"] = $drParam->getValueName("RESEAU_ID");
    }
    
    // v�rifie si le code point existe
    $dsParam = $this->oQuery->getPointByCode($tabChampsData["POINT_CODE"], "", $_SESSION["idBassin"]);
    if(! $drParam = $dsParam->getRowIter()) {
      $strMes = "Erreur : Ligne ".$t.", code point inexistant. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);
      return true;
    } else {
    	$tabChampsData["POINT_ID"] = $drParam->getValueName("POINT_ID");
    }
    
    // v�rifie si le code op�rateur existe
    $dsParam = $this->oQuery->getOperateurByCode($tabChampsData["OPERATEUR_TERRAIN"], $_SESSION["idBassin"]);
    if($drParam = $dsParam->getRowIter()) {      
    	$tabChampsData["OPERATEUR_TERRAIN_ID"] = $drParam->getValueName("OPERATEUR_ID");
    }
    
    // v�rifie si le code op�rateur existe
    $dsParam = $this->oQuery->getOperateurByCode($tabChampsData["OPERATEUR_LABO"], $_SESSION["idBassin"]);
    if($drParam = $dsParam->getRowIter()) {
    	$tabChampsData["OPERATEUR_LABO_ID"] = $drParam->getValueName("OPERATEUR_ID");
    }
    
		return $bErr;
	}
}

?>