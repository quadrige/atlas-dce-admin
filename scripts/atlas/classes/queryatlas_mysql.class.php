<?php
include_once('queryatlas.class.php');

/**
 * @Class queryAtlasMysql
 *
 * @brief Ensemble des requetes s�lections li�es � l'application Actualites sp�cialis�es Mysql
 *         Les requetes standards se trouvent dans la classe m�re
 */
class queryAtlasMySql extends queryAtlas
{
  /**
   * @brief Constructeur
   *
   * @param dbConn    classe de connexion � la base
   * @param tabLangue tableau des langues utilis�es
   */
  function queryAtlasMySql(&$dbConn, $tabLangue=array()) 
  {
    parent::queryAtlas($dbConn, $tabLangue);
  } 
  
  /**
   * @brief Retourne la fiche d'un r�seau
   *
   * @param codeReseau  Code du reseau
   *
   * @return Retourne un dataset
   */
  function getParamByPointReseau($idReseau, $idPoint, $session_id=-1) 
  {  
    $strExtSession = ($session_id != -1 ? "_SESSION" : "");
            
    $strSql = "select rpp.*, par.*, supp.*" .
    	", oplab.OPERATEUR_ID LABO_ID, oplab.OPERATEUR_NOM LABO_NOM" .
    	", opter.OPERATEUR_ID TERRAIN_ID, opter.OPERATEUR_NOM TERRAIN_NOM".
    	" from " .
    	" RESEAU_POINT_PARAM".$strExtSession." rpp " .
    	" left join	PARAMETRE".$strExtSession." par on rpp.PARAMETRE_ID = par.PARAMETRE_ID".($session_id != -1 ? " and par.SESSION_ID=".$session_id : "").
    	" left join SUPPORT".$strExtSession." supp on par.SUPPORT_ID = supp.SUPPORT_ID".($session_id != -1 ? " and supp.SESSION_ID=".$session_id : "").
    	" left join OPERATEUR".$strExtSession." oplab on oplab.OPERATEUR_ID = rpp.OPERATEUR_LABO".($session_id != -1 ? " and oplab.SESSION_ID=".$session_id : "").
    	" left join OPERATEUR".$strExtSession." opter on opter.OPERATEUR_ID = rpp.OPERATEUR_TERRAIN".($session_id != -1 ? " and opter.SESSION_ID=".$session_id : "").
      " where rpp.RESEAU_ID = ".$idReseau.
      " and rpp.POINT_ID = ".$idPoint.
      ($session_id != -1 ? " and rpp.SESSION_ID=".$session_id : ""); 
          
    return $this->dbConn->initDataset( $strSql ) ;
  }
  
  function getDs_ParametrePoint($reseau_id, $masse_id, $point_id, $bGroupBy = true, $param_id = -1, $idFirst=0, $idLast=-1, $bassin_id=-1)
  {
  	$strWhere = "";
  	
  	if ($reseau_id != -1){
  		$strWhere .= " and rpp.RESEAU_ID = ".$reseau_id;
  	}
  	
  	if ($point_id != -1){
  		$strWhere .= " and rpp.POINT_ID = ".$point_id;
  	} else {
  		if ($masse_id != -1){
  			$strWhere .= " and rpp.POINT_ID IN (select POINT_ID from POINT where MASSE_ID = ".$masse_id.") ";
  		} 
  	}
  	
  	if ($param_id != -1){
  		$strWhere .= " and par.PARAMETRE_ID = ".$param_id;
  	}

  	if ($bassin_id != -1){
  		$strWhere .= " and pt.MASSE_ID in (select MASSE_ID from MASSE_EAU where BASSIN_ID=".$bassin_id.")";
  	}
  	
  	$strSql = "select rpp.*, par.*, supp.*, pt.POINT_CODE, pt.POINT_NOM, pt.POINT_LONGITUDE, pt.POINT_LATITUDE, res.RESEAU_NOM" .
    	", oplab.OPERATEUR_ID LABO_ID, oplab.OPERATEUR_NOM LABO_NOM" .
    	", opter.OPERATEUR_ID TERRAIN_ID, opter.OPERATEUR_NOM TERRAIN_NOM".
    	",".$this->dbConn->getConcat("SUPPORT_NOM", "' - '", "PARAMETRE_NOM")." PARAM_NOM_LG, rpp.RESEAU_ID".
    	" from " .
    	" RESEAU_POINT_PARAM rpp " .
    	" left join	PARAMETRE par on rpp.PARAMETRE_ID = par.PARAMETRE_ID" .
    	" left join SUPPORT supp on par.SUPPORT_ID = supp.SUPPORT_ID" .
    	" left join OPERATEUR oplab on oplab.OPERATEUR_ID = rpp.OPERATEUR_LABO" .
    	" left join OPERATEUR opter on opter.OPERATEUR_ID = rpp.OPERATEUR_TERRAIN".
    	" left join POINT pt on pt.POINT_ID = rpp.POINT_ID" .
    	" left join RESEAU res on res.RESEAU_ID = rpp.RESEAU_ID" .
      " where 1=1".
     	$strWhere.
     	($bGroupBy ? " group by rpp.RESEAU_ID, rpp.POINT_ID" : ""); 
         
    return $this->dbConn->initDataset( $strSql , $idFirst, $idLast) ;
  }
}
?>