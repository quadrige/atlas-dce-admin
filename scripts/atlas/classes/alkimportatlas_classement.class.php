<?php
include_once("../../classes/appli/alkimport.class.php");

/**
 * @class AlkImportAnnu
 * @copyright Alkante
 * @brief Classe d'import pour l'annuaire
 */
class AlkImportAtlasClassement extends AlkImport
{
	
	
	/**
   * @brief Constructeur de la classe Import : initialisation des attributs de Import.
   *
   * @param strFichier_Import Emplacement  du fichier d'import
   * @param tabImport  Tableau contenant les propriet�s des  colonnes
   * @param tabSup  Tableau contenant les colonnes obligatoires pour la suppression
   * @param tabAjoutt  Tableau contenant les colonnes obligatoires pour l'ajouts
   * @param objErreur  Nom de l'objet  ImportErreur
   */
	function AlkImportAtlasClassement($strFichier_Import,  $tabAlkImportCol, $tabSup, $tabAjout, 
                         $tabModif, &$obj_Erreur,&$objWarning, &$oQuery, &$oQueryAction )
	{
		parent::AlkImport($strFichier_Import,  $tabAlkImportCol, $tabSup, $tabAjout, 
                      $tabModif, $obj_Erreur,$objWarning, $oQuery, $oQueryAction, $oQuery );
                      
    $this->oQueryAction->delDoublonsClassement();                     
	}
	
  /**
   * @brief Fonction qui rend un tableaux de champs valeurs.
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return tabChampsData  un tableau de champs valeurs
   */
	function getChampsData($tabLigne, $tabCopy, $tabLigneRef, $bUpdate=false)
	{
    if( $bUpdate == false ) {
      // mode ajout
      $tabChampsData = array("MASSE_CODE"    => "-1", "ELEMENT_QUALITE_NOM" => "-1", "TYPE_ELEMENT_QUALITE_NOM" => "-1", "ELEMENT_QUALITE_NIVEAU"=>"-1", "ETAT_LIBELLE"  => "-1",
      											 "MOTIF_CODE"  => "-1", "STATUT_CODE"  => "-1", "CLASSEMENT_DATE"=>"-1", "CLASSEMENT_INDICE_CONFIANCE"=>"",
      											 "MASSE_ID"    => "-1", "ELEMENT_QUALITE_ID" => "", "TYPE_ELEMENT_QUALITE_ID" => "", "ETAT_ID"  => "-1",                                
                             "MOTIF_ID" => "",   "STATUT_ID" => "",
                             "TYPE_CLASSEMENT_LIBELLE" => "-1","TYPE_CLASSEMENT_ID" => "");
    } else {
      // mode modif
      $tabChampsData = array("MASSE_CODE"    => "-2", "ELEMENT_QUALITE_NOM" => "-2", "TYPE_ELEMENT_QUALITE_NOM" => "-2", "ELEMENT_QUALITE_NIVEAU"=>"-2", "ETAT_LIBELLE"  => "-2",
                             "MOTIF_CODE"  => "-2", "STATUT_CODE"  => "-2", "CLASSEMENT_DATE"=>"-2", "CLASSEMENT_INDICE_CONFIANCE"=>"-2",
      											 "MASSE_ID"    => "-1", "ELEMENT_QUALITE_ID" => "", "TYPE_ELEMENT_QUALITE_ID" => "", "ETAT_ID"  => "-1",
       											 "MOTIF_ID" => "-2",   "STATUT_ID" => "-2",
                             "TYPE_CLASSEMENT_LIBELLE" => "-2","TYPE_CLASSEMENT_ID" => "");
    }
		$tabChamps = array();
		
		if (in_array("ME",$tabLigneRef) && ($tabLigne[$tabCopy["ME"]] != "")) 
      $tabChampsData["MASSE_CODE"] = $tabLigne[$tabCopy["ME"]];

		if (in_array("ELEMENT",$tabLigneRef) && ($tabLigne[$tabCopy["ELEMENT"]] != "")) 
      $tabChampsData["ELEMENT_QUALITE_NOM"] = $tabLigne[$tabCopy["ELEMENT"]];
		
    if (in_array("ELEMENT_NIVEAU",$tabLigneRef) && ($tabLigne[$tabCopy["ELEMENT_NIVEAU"]] != "")) 
    	$tabChampsData["ELEMENT_QUALITE_NIVEAU"] = $tabLigne[$tabCopy["ELEMENT_NIVEAU"]];
      
		if (in_array("TYPE_ELEMENT",$tabLigneRef) && ($tabLigne[$tabCopy["TYPE_ELEMENT"]] != "")) 
      $tabChampsData["TYPE_ELEMENT_QUALITE_NOM"] = $tabLigne[$tabCopy["TYPE_ELEMENT"]];
		
    if (in_array("ETAT",$tabLigneRef) && ($tabLigne[$tabCopy["ETAT"]] != "")) 
      $tabChampsData["ETAT_LIBELLE"] = $tabLigne[$tabCopy["ETAT"]];
      
		if (in_array("MOTIF", $tabLigneRef)) 
      $tabChampsData["MOTIF_CODE"] = $tabLigne[$tabCopy["MOTIF"]];

		if (in_array("STATUT",$tabLigneRef)) 
      $tabChampsData["STATUT_CODE"] = $tabLigne[$tabCopy["STATUT"]];

		if (in_array("DATE",$tabLigneRef) && ($tabLigne[$tabCopy["DATE"]] != "")) 
      $tabChampsData["CLASSEMENT_DATE"] = $tabLigne[$tabCopy["DATE"]];
      
    if (in_array("INDICE",$tabLigneRef)) 
      $tabChampsData["CLASSEMENT_INDICE_CONFIANCE"] = $tabLigne[$tabCopy["INDICE"]];  
      
    if (in_array("TYPE_CLASSEMENT",$tabLigneRef) && ($tabLigne[$tabCopy["TYPE_CLASSEMENT"]] != "")) 
      $tabChampsData["TYPE_CLASSEMENT_LIBELLE"] = $tabLigne[$tabCopy["TYPE_CLASSEMENT"]];      
						
		return 	$tabChamps = array($tabChampsData);
	}
	
  /**
   * @brief Fonction insertion de la ligne courante .
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return vrai si ibsertion faux sinon
   * 
   * DMI : 06/10/2010 : modification en cas d'existence pas de suppression mais modification cause suppression intempestive des textes explicatifs d�j� saisis
   */
	function insert($tabLigne, $tabCopy, $tabLigneRef, $t)
	{
		$tabChamps = $this->getChampsData($tabLigne, $tabCopy, $tabLigneRef);

		$tabChampsData = $tabChamps[0];

    $iWarning = 0;
		
    $bErr = $this->_VerificationCode($tabChampsData, $t);
		if ($bErr)
		  return 2;		
        
    $dsParam = $this->oQuery->getDs_QualiteMasseEau($tabChampsData["MASSE_CODE"], 						  
    																		  		$tabChampsData["ELEMENT_QUALITE_ID"],
    																					$tabChampsData["TYPE_ELEMENT_QUALITE_ID"],
    																					$tabChampsData["TYPE_CLASSEMENT_ID"]);		
		if( $drParam = $dsParam->getRowIter() ) {
			return $this->update($tabLigne, $tabCopy, $tabLigneRef, $t);			
		} 
				
		//Suppression de l'�ventuelle association d�j� existante  
    $bRes = $this->oQueryAction->delQualiteLastSession($tabChampsData["MASSE_ID"],
    																									 $tabChampsData["ELEMENT_QUALITE_ID"], 
    																				 					 $tabChampsData["TYPE_ELEMENT_QUALITE_ID"],
    																					         $tabChampsData["TYPE_CLASSEMENT_ID"] );    																				 					
		//ajout 
		$tabValue=array();
		$tabValue["SESSION_ID"] = array( 1, "(SELECT SESSION_ID FROM SESSION_QUALITE where BASSIN_ID = ".$_SESSION["idBassin"]." and SESSION_ARCHIVE=0 )");
		$tabValue["MASSE_ID"] = array( 1, $tabChampsData["MASSE_ID"]);
		$tabValue["ELEMENT_QUALITE_ID"] = array( 1, $tabChampsData["ELEMENT_QUALITE_ID"]);
		$tabValue["TYPE_ELEMENT_QUALITE_ID"] = array( 1, $tabChampsData["TYPE_ELEMENT_QUALITE_ID"]);
		$tabValue["TYPE_CLASSEMENT_ID"] = array( 1, $tabChampsData["TYPE_CLASSEMENT_ID"]);
		$tabValue["ETAT_ID"] = array( 1, $tabChampsData["ETAT_ID"]);
		$tabValue["MOTIF_ID"] = array( 1, $tabChampsData["MOTIF_ID"]);
		$tabValue["STATUT_ID"] = array( 1, $tabChampsData["STATUT_ID"]);
		$tabValue["CLASSEMENT_INDICE_CONFIANCE"] = array( 1, $tabChampsData["CLASSEMENT_INDICE_CONFIANCE"]);
		$tabValue["CLASSEMENT_DATE"] = array( 2, $tabChampsData["CLASSEMENT_DATE"]);
		$tabValue["CLASSEMENT_DATE_MAJ"] = array( 1, $this->oQueryAction->dbConn->getDateCur());		    
    $tabValue["CLASSEMENT_DATE_CREA"] = array( 1, $this->oQueryAction->dbConn->getDateCur());
    $tabValue["CLASSEMENT_ME_DATE_MAJ"] = array( 1, $this->oQueryAction->dbConn->getDateCur());
    $strFieldKey = "CLASSEMENT_ID";
	  $strTable = "CLASSEMENT_MASSE_EAU";
      	
    $classement_id = $this->oQueryAction->add_ficheDico($strTable, $strFieldKey, $tabValue);
    		
    if( $classement_id < 1 ) {
      $strMes = "Erreur : Ligne ".$t.", impossible d'ajouter les donn�es. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);      
      return 2;
    }
    				
		if( $iWarning != 0 ) {
			return 1;
		} else {
			return 0;
		}
	}
	
	/**
   * @brief Fonction de modification de la ligne courante dans la base .
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return vrai si ibsertion faux sinon
   */
	function update($tabLigne, $tabCopy, $tabLigneRef,$t)
	{
		$tabChamps = $this->getChampsData($tabLigne, $tabCopy, $tabLigneRef, true);
		$tabChampsData = $tabChamps[0];
    $iWarning = 0;
		$bErr = $this->_VerificationCode($tabChampsData, $t);
		if ($bErr)
		  return 2;
    
    // charge la fiche
		$dsParam = $this->oQuery->getDs_QualiteMasseEau($tabChampsData["MASSE_CODE"], 						  
    																		  		$tabChampsData["ELEMENT_QUALITE_ID"],
    																					$tabChampsData["TYPE_ELEMENT_QUALITE_ID"],
    																					$tabChampsData["TYPE_CLASSEMENT_ID"]);	
		if( $drParam = $dsParam->getRowIter() ) {
			$classement_id = $drParam->getValueName("CLASSEMENT_ID");
		} else {      
      $strMes = "Erreur : Ligne ".$t.", fiche non trouv�e pour modification. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);
      return 2;
    }
    
    $tabValue=array();
		$tabValue["ETAT_ID"] = array( 1, $tabChampsData["ETAT_ID"]);
		if ($tabChampsData["MOTIF_ID"] != "-2") $tabValue["MOTIF_ID"] = array( 1, $tabChampsData["MOTIF_ID"]);
		if ($tabChampsData["STATUT_ID"] != "-2") $tabValue["STATUT_ID"] = array( 1, $tabChampsData["STATUT_ID"]);
		if ($tabChampsData["CLASSEMENT_INDICE_CONFIANCE"] != "-2") $tabValue["CLASSEMENT_INDICE_CONFIANCE"] = array( 1, $tabChampsData["CLASSEMENT_INDICE_CONFIANCE"]);
		if ($tabChampsData["CLASSEMENT_DATE"] != "-2") $tabValue["CLASSEMENT_DATE"] = array( 2, $tabChampsData["CLASSEMENT_DATE"]);
		$tabValue["CLASSEMENT_DATE_MAJ"] = array( 1, $this->oQueryAction->dbConn->getDateCur());	
		$tabValue["CLASSEMENT_ME_DATE_MAJ"] = array( 1, $this->oQueryAction->dbConn->getDateCur());  
    		    		
    $strFieldKey = "CLASSEMENT_ID";
	  $strTable = "CLASSEMENT_MASSE_EAU";
    
		$this->oQueryAction->update_ficheDico($strTable, $strFieldKey, $classement_id, $tabValue);    
    																		
   if( $iWarning != 0 ) {
			return 2;
		} else {
			return 0;
		}
	}
	
	/**
   * @brief Fonction de suppresion de la ligne courante dans la base .
   *
   * @param tabLigne Tableau contenant ligne courante
   * @param tabCopy  Tableau contenant l'intersection du  tabImport et de ligne reference   
   * @param tabLigneRef  Tableau contenant la ligne reference
   * @return vrai si insertion faux sinon
   */
	function delete($tabLigne, $tabCopy, $tabLigneRef,$t)
	{
		$tabChamps = $this->getchampsData($tabLigne, $tabCopy, $tabLigneRef);

		$tabChampsData = $tabChamps[0];
		$bErr = $this->_VerificationCode($tabChampsData, $t);
		if ($bErr)
		  return 2;
		
		$bRes = $this->oQueryAction->delQualiteLastSession($tabChampsData["MASSE_ID"],
    																									 $tabChampsData["ELEMENT_QUALITE_ID"], 
    																				 					 $tabChampsData["TYPE_ELEMENT_QUALITE_ID"],
    																					         $tabChampsData["TYPE_CLASSEMENT_ID"]);
    
		if( ! $bRes ) {			
      $strMes = "Erreur : Ligne ".$t.", impossible de supprimer la fiche. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);      
      return 2;
		}
		
    return 0;
	}
	
	/**
   * @brief retourne la reference d'un champ..
   *
   * @param  champ  l'intitule du champs
   * @param  tabType tableau contenant le champs 
   * @return tabRes[0] la reference recherche dans la tabType et celle de la premiere case.
   */
	function getIdChamp($champ,$tabType)
	{
		$tabRes = array_keys($tabType, $champ);
    if( count($tabRes) > 0 ) 
      return $tabRes[0];
    return -1;
	}
	
	function _VerificationCode(&$tabChampsData, $t){
		$bErr = false;
		
		// v�rifie si le code masse d'eau existe
    $dsParam = $this->oQuery->getDs_MasseEauById("", $tabChampsData["MASSE_CODE"]);
    if(! $drParam = $dsParam->getRowIter()) {
      $strMes = "Erreur : Ligne ".$t.", code masse d'eau inexistant. Ligne rejet�e.";
      $this->obj_Erreur->erreur(7, $strMes);
      return true;
    } else {
    	$tabChampsData["MASSE_ID"] = $drParam->getValueName("ID");
    }
    
    $tabChampsData["ETAT_ID"] = "";
    
    // v�rifie si l'�l�ment de qualit� existe
    if ($tabChampsData["ELEMENT_QUALITE_NOM"] != "-1" && $tabChampsData["ELEMENT_QUALITE_NOM"] != "-2") {  
	    $dsParam = $this->oQuery->getDs_ElementQualiteById("", $tabChampsData["ELEMENT_QUALITE_NOM"], $tabChampsData["ELEMENT_QUALITE_NIVEAU"], $_SESSION["idBassin"]);
	    if(! $drParam = $dsParam->getRowIter()) {
	      $strMes = "Erreur : Ligne ".$t.", �l�ment de qualit� inexistant. Ligne rejet�e.";
	      $this->obj_Erreur->erreur(7, $strMes);
	      return true;
	    } else {
	    	$tabChampsData["ELEMENT_QUALITE_ID"] = $drParam->getValueName("ELEMENT_QUALITE_ID");
	    	
		    // v�rifie si l'�tat existe pour l'�l�ment de qualit� en cours
	    	$dsParam = $this->oQuery->getDs_listeEtat(0, -1, "", $tabChampsData["ELEMENT_QUALITE_ID"], $tabChampsData["ETAT_LIBELLE"]);
	    	if(! $drParam = $dsParam->getRowIter()) {
		      $strMes = "Erreur : Ligne ".$t.", �tat inexistant pour l'�l�ment de qualit�. Ligne rejet�e.";
	  	    $this->obj_Erreur->erreur(7, $strMes);
	    	  return true;
	    	} else {
	    		$tabChampsData["ETAT_ID"] = $drParam->getValueName("ID");
	    	}
	    }	

	    if (($tabChampsData["TYPE_ELEMENT_QUALITE_NOM"] != "-1" && $tabChampsData["TYPE_ELEMENT_QUALITE_NOM"] != "-2") ||
	        ($tabChampsData["TYPE_CLASSEMENT_LIBELLE"] != "-1" && $tabChampsData["TYPE_CLASSEMENT_LIBELLE"] != "-2")) {  
	        $strMes = "Erreur : Ligne ".$t.", l'�tat ne peut pas �tre d�fini en m�me temps pour un �l�ment de qualit� et un type d'�l�ment de qualit� ou un type de classement. Ligne rejet�e.";
	  	    $this->obj_Erreur->erreur(7, $strMes);
	    	  return true;
	    }
    }
    
		//v�rifie si le type d'�l�ment de qualit� existe
    if ($tabChampsData["TYPE_ELEMENT_QUALITE_NOM"] != "-1" && $tabChampsData["TYPE_ELEMENT_QUALITE_NOM"] != "-2") {  
	    $dsParam = $this->oQuery->getDs_TypeElementQualiteById("", $tabChampsData["TYPE_ELEMENT_QUALITE_NOM"]);
	    if(! $drParam = $dsParam->getRowIter()) {
	      $strMes = "Erreur : Ligne ".$t.", type d'�l�ment de qualit� inexistant. Ligne rejet�e.";
	      $this->obj_Erreur->erreur(7, $strMes);
	      return true;
	    } else {
	    	$tabChampsData["TYPE_ELEMENT_QUALITE_ID"] = $drParam->getValueName("TYPE_ELEMENT_QUALITE_ID");
	    	
		    // v�rifie si l'�tat existe pour l'�l�ment de qualit� en cours
	    	$dsParam = $this->oQuery->getDs_listeEtat(0, -1, $tabChampsData["TYPE_ELEMENT_QUALITE_ID"], "", $tabChampsData["ETAT_LIBELLE"]);
	    	if(! $drParam = $dsParam->getRowIter()) {
		      $strMes = "Erreur : Ligne ".$t.", �tat inexistant pour le type d'�l�ment de qualit�. Ligne rejet�e.";
	    		$this->obj_Erreur->erreur(7, $strMes);
	    	  return true;
	    	} else {
	    		$tabChampsData["ETAT_ID"] = $drParam->getValueName("ID");
	    	}
	    }
	    
      if ($tabChampsData["TYPE_CLASSEMENT_LIBELLE"] != "-1" && $tabChampsData["TYPE_CLASSEMENT_LIBELLE"] != "-2") {  
	        $strMes = "Erreur : Ligne ".$t.", l'�tat ne peut pas �tre d�fini en m�me temps pour un type d'�l�ment de qualit� et un type de classement. Ligne rejet�e.";
	  	    $this->obj_Erreur->erreur(7, $strMes);
	    	  return true;
	    }
    }
    
    // v�rifie si le type de classement existe
    if ($tabChampsData["TYPE_CLASSEMENT_LIBELLE"] != "-1" && $tabChampsData["TYPE_CLASSEMENT_LIBELLE"] != "-2") {  
	    $dsParam = $this->oQuery->getTableForCombo("TYPE_CLASSEMENT", "TYPE_CLASSEMENT", true, false, "TYPE_CLASSEMENT_LIBELLE = '".$this->oQuery->dbConn->AnalyseSql($tabChampsData["TYPE_CLASSEMENT_LIBELLE"])."'");
	    if(! $drParam = $dsParam->getRowIter()) {
	      $strMes = "Erreur : Ligne ".$t.", type de classement inexistant. Ligne rejet�e.";
	      $this->obj_Erreur->erreur(7, $strMes);
	      return true;
	    } else {
	    	$tabChampsData["TYPE_CLASSEMENT_ID"] = $drParam->getValueName("TYPE_CLASSEMENT_ID");
	    	$tabChampsData["ETAT_ID"] = "";
	    }
    }    
    
    // v�rifie si le motif existe
    if ($tabChampsData["MOTIF_CODE"] == "")
      $tabChampsData["MOTIF_ID"] = $tabChampsData["MOTIF_CODE"];
    if ($tabChampsData["MOTIF_CODE"] != "-1" && $tabChampsData["MOTIF_CODE"] != "-2" && $tabChampsData["MOTIF_CODE"] != "") {
	    $dsParam = $this->oQuery->getDs_EtatMotifById("",$tabChampsData["MOTIF_CODE"], $_SESSION["idBassin"]);
	    if(! $drParam = $dsParam->getRowIter()) {
	      $strMes = "Erreur : Ligne ".$t.", code motif inexistant. Ligne rejet�e.";
	      $this->obj_Erreur->erreur(7, $strMes);
	      return true;
	    } else {
	    	$tabChampsData["MOTIF_ID"] = $drParam->getValueName("ID");
	    }
	  }

    // v�rifie si le statut existe
    if ($tabChampsData["STATUT_CODE"] == "")
      $tabChampsData["STATUT_ID"] = $tabChampsData["STATUT_CODE"];
	  if ($tabChampsData["STATUT_CODE"] != "-1" && $tabChampsData["STATUT_CODE"] != "-2" && $tabChampsData["STATUT_CODE"] != "") {
	    $dsParam = $this->oQuery->getDs_StatutById("", $tabChampsData["STATUT_CODE"]);
	    if(! $drParam = $dsParam->getRowIter()) {
	      $strMes = "Erreur : Ligne ".$t.", code statut inexistant. Ligne rejet�e.";
	      $this->obj_Erreur->erreur(7, $strMes);
	      return true;
	    } else {
	    	$tabChampsData["STATUT_ID"] = $drParam->getValueName("ID");
	    }
	  }
	  
		return $bErr;
	}
}

?>