<?php
/**
 * @Class queryAnnu
 *
 * @brief  Ensemble des requetes s�lections li�es � l'application actualit�s
 *         Les requetes sp�cifiques se trouvent dans une classe h�rit�e queryAtlas_action_xxx.class.php
 */
class queryAnnuAction {

  var $dbConn; // Connection to the database
  var $tabLangue;
  
  /**
   * @brief Constructeur
   *
   * @param dbConn    classe de connexion � la base
   * @param tabLangue tableau des langues utilis�es
   */
  function queryAnnuAction($dbConn, $tabLangue) 
  {
    $this->dbConn = $dbConn;
    $this->tabLangue = $tabLangue;
  }
  
  /**
   * @brief Affecte la connexion
   *
   * @param dbConn classe de connexion � la base
   *
   * @return Retourne true
   */
  function setConnection($dbConn) 
  {
    $this->dbConn = &$dbConn ;
    return true ;
  }
  
      /**
   * @brief Met � jour la date de connexion de l'agent en cours
   * sit_menu/login_verif.php
   *
   * @param user_id    Identifiant de l'agent 
   */  
  function maj_agentDateConn($user_id)
  {   
    $strSql = "update ANNU_USER set".
      " USER_DATE_CONN=".$this->dbConn->GetDateCur().      
      " where USER_ID=".$user_id;
    $this->dbConn->ExecuteSql($strSql);
  }
  
}
?>