<?php
include_once("classes/alkappliatlas.class.php");

/**
 * @brief Classe de l'application concours
 *        Classe regroupant des fonctionnalit�s de la rubrique param�trage des concours
 */
class AlkAppliAtlas_annu extends AlkAppliAtlas
{

  /**
   * @brief Constructeur par d�faut
   *
   */
  function AlkAppliAtlas_annu($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet)
  {
    parent::AlkAppliAtlas($appli_id, $agent_id, $iSheet, $iSSheet, $iModeSSheet);
    
    global $queryAnnu, $queryAnnuAction;
    
    $this->oQuery =& $queryAnnu;
    $this->oQueryAction =& $queryAnnuAction;
  }

  /**
   * @brief M�thode virtuelle qui retourne un tableau de boutons htmllink
   *        plac� sur la droite des onglets
   * 
   * @param 
   * @return Retourne un array
   */
  function getTabCtrlBt()
  {
    $iCptBt = 0;
    $tabBt = array();

    return $tabBt;
  }

  /**
   * @brief M�thode virtuelle, retourne un tableau contenant les informations sur les sous onglets
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un array
   */
  function getTabSubSheet()
  {
    global $tabStrType;

    $tabSubSheet = array();

    $strParam = "iSheet=".ALK_SHEET_ANNU;
    $i = 0;
    $tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_ANNU_LISTE,
                               "text"     => "Liste&nbsp;des&nbsp;administrateurs",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_ANNU_LISTE,
                               "title"    => "Liste");            
    if ($_SESSION["bAdminUserBassinCourant"]==1 || $_SESSION["bAdminUserAllBassin"]==1){
    	$tabSubSheet[$i++] = array("idSSheet" => ALK_SSHEET_ANNU_NEW,
                               "text"     => "Nouvelle&nbsp;fiche",
                               "url"      => "01_page_form.php?".$strParam."&iSSheet=".ALK_SSHEET_ANNU_NEW."&iModeSSheet=".ALK_MODE_SSHEET_FORM,
                               "title"    => "Nouvelle fiche");
    }                                                      
    return $tabSubSheet;
  }



  /**
   * @brief M�thode virtuelle, retourne le contenu html du corps de l'onglet s�lectionn�
   *
   * @param iSSheet Identifiant du sous onglet s�lectionn�
   * @return Retourne un string : code html des sous onglets
   */
  function getHtmlBodySheet()
  {
    $strHtml = "";
    switch( $this->iSSheet ) {        
    case ALK_SSHEET_ANNU_LISTE:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_LIST )
        $strHtml = $this->getHtmlListeUser();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheUser();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementUser();
      break;
		case ALK_SSHEET_ANNU_NEW:
      if( $this->iModeSSheet == ALK_MODE_SSHEET_FORM )
        $strHtml = $this->getHtmlFicheUser();
      elseif( $this->iModeSSheet == ALK_MODE_SSHEET_SQL )
        $strHtml = $this->traitementUser();
      break;
    default:
      break;
    }
    return $strHtml;
  }

  /**
   * @brief Affiche la liste des options pour une voie de concours
   *
   * @return Retourne un string
   */
  function getHtmlListeUser()
  {    
  	$iErr = Request("err", REQ_GET, "0", "is_numeric");
    $nbEltParPage = Request("nbEltParPage", REQ_POST_GET, 20, "is_numeric");
    $page = Request("page", REQ_GET, 1, "is_numeric");
    
    $strParam = "iSheet=".$this->iSheet.
      "&nbEltParPage=".$nbEltParPage."&page=".$page."&iSSheet=".$this->iSSheet."&iModeSSheet=";
   	
   	// liste des �l�ments du dictionnaire s�lectionn�
    $cpt = 0;
    $tabPage = array();
    $iFirst = ($page-1)*$nbEltParPage;
    $iLast = $iFirst+$nbEltParPage-1;
      
   	$dsUser = $this->oQuery->getDs_listeUser($iFirst, $iLast);
    // seul un admin peut ajouter
    if ( $_SESSION["bAdminUserBassinCourant"]==1 || $_SESSION["bAdminUserAllBassin"]==1){
    $oBtAdd = new HtmlLink("01_page_form.php?iMode=1&".$strParam.ALK_MODE_SSHEET_FORM."&page=".$page,
                           "Ajouter un nouvel �l�ment � la liste",
                           "ajouter.gif", "ajouter_rol.gif");
      $htmlBtAdd = $oBtAdd->getHtml();
    }else{
    	$htmlBtAdd = "";
    }
    $nbElt = $dsUser->iCountTotDr;

    $strHtml = $this->_getFrameTitleSheet("Gestion des utilisateurs").    
      "<script language='javascript' src='lib/lib_atlas.js'></script>".  
      "<form name='formUser' action='' method='post'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='450' height='10'></td>".            
      "<td width='120'></td>".      
      "</tr>".
      ( $iErr == "1"
        ? "<tr><td colspan='2' class='divContenuMsgErr' align='center'>".
        "Impossible de supprimer cet enregistrement car il est encore utilis�.<br><br></td></tr>"
        : "").
        ( $iErr == "2"
        ? "<tr><td colspan='2' class='divContenuMsgErr' align='center'>".
        "Impossible d'effectuer cet enregistrement car le login et le mot de passe sont d�j� utilis�s.<br><br></td></tr>"
        : "").     
      "<tr class='trEntete1'>".
      "<td class='tdEntete1' align='left'><div class='divTabEntete1'>Liste des ".
      "Utilisateurs&nbsp;&nbsp;".$nbElt." enregistrement".($nbElt>1 ? "s" : "")."</div></td>".            
      "<td class='tdEntete1' align='center'>".$htmlBtAdd."</td>".
      "</tr>";

    while( $drUser = $dsUser->getRowIter() ) {
      $id = $drUser->getValueName("USER_ID");
      $strLib = $drUser->getValueName("USER_NOM")." ".$drUser->getValueName("USER_PRENOM");      
      $bassin_id = $drUser->getValueName("BASSIN_ID");
      $strLib = array(wordwrap($strLib, 100, "<br>"), 
                      "01_page_form.php?iMode=2&".$strParam.ALK_MODE_SSHEET_FORM.
                      "&id=".$id."&page=".$page);

      $iPage = ( $nbElt-1 <= ($page-1)*$nbEltParPage ? ($page-1>0 ? $page-1 : 1) : $page);
      $strSuppr = "&nbsp;";
      if ( $_SESSION["bAdminUserBassinCourant"]==1 || $_SESSION["bAdminUserAllBassin"]==1){
				
          if($_SESSION["bAdminUserBassinCourant"]==1 && $_SESSION["idBassin"]==$bassin_id){	
            $oBtSuppr = new HtmlLink("javascript:SupprEltAnnu('".$id."', '".$strParam.ALK_MODE_SSHEET_SQL.
                               "&page=".$iPage."')", 
                                 "Supprimer cet �l�ment", "tab_supprimer.gif", "tab_supprimer_rol.gif");
					  $strSuppr = $oBtSuppr->getHtml();                                
          }elseif($_SESSION["bAdminUserAllBassin"]==1){
            $oBtSuppr = new HtmlLink("javascript:SupprEltAnnu('".$id."', '".$strParam.ALK_MODE_SSHEET_SQL.
                               "&page=".$iPage."')", 
                               "Supprimer cet �l�ment", "tab_supprimer.gif", "tab_supprimer_rol.gif");
            $strSuppr = $oBtSuppr->getHtml();         
          }
       }
     
     	$tabPage[$cpt] = array($strLib, $strSuppr);
      	
      $cpt++;
    }

    // pas de pagination
    //$nbEltParPage = $nbElt;
    $tabAlign = array("", "left", "center");
    	
    $strHtml .= getHtmlListePagine($tabPage, $nbElt, $nbEltParPage, $page, 
                               $_SERVER["PHP_SELF"]."?".$strParam.ALK_MODE_SSHEET_LIST, 
                               $tabAlign).
      "</form><br>";

    $oBtAnnuler = new HtmlLink("01_page_form.php?".$strParam."&iModeSSheet=".ALK_MODE_SSHEET_LIST."&iMode=2", "Annuler", 
                               "annul_gen.gif", "annul_gen_rol.gif");
                               
    $strHtml .= "<div class='divTextContenu' style='margin-left:20px' align='center'><br>".$oBtAnnuler->getHtml()."</div>";

    return $strHtml;
  }
  
  function _getHtmlLigneCtrl($oCtrl) {
  	$strHtml = "<tr>".
		  							"<td align='right' class='formLabel'>".$oCtrl->label."</div></td>".
		  							"<td>".$oCtrl->getHtml()."</td>".
		  							"</tr>";
		return $strHtml;
  }
  /**
   * @brief Retourne le code html d'un formulaire dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function getHtmlFicheUser()
  {
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    $id = Request("id", REQ_GET, "-1", "is_numeric");
    $page =  Request("page", REQ_GET, "1", "is_numeric");
    
    $strParam = "iMode=".$iMode."&page=".$page.
      "&iSheet=".$this->iSheet."&iSSheet=".$this->iSSheet."&iModeSSheet=";
		$strParam2 = "iSheet=".$this->iSheet."&iSSheet=".ALK_SSHEET_ANNU_LISTE."&iModeSSheet=";
		
		$iWidthTxt = 48;
    $iWidthMemo = 46;
    $iHeightMemo = 4;  
    
		$strNom = "";
    $strPrenom = "";
    $strLogin = "";
    $strPwd = "";
    $bValide = 0;
    $bAdmin = 0;
    $bAdminAnnu = 0; // admin de l'annuaire du bassin courant
    $bAdminAllBassin = 0 ; // admin de l'annuaire de tous les bassins
    $bassin_id = -1;
    $tabBassin = array();
    $strHtmlCtrl = "";
		$dsUser = $this->oQuery->getDs_UserById($id);    		    		
    if( $drUser = $dsUser->getRowIter() ) {      
    		$strNom = $drUser->getValueName("USER_NOM");
    		$strPrenom = $drUser->getValueName("USER_PRENOM");
    		$strLogin = $drUser->getValueName("USER_LOGIN");
    		$strPwd = $drUser->getValueName("USER_PWD");
    		$bValide = $drUser->getValueName("USER_VALIDE");
    		$bAdmin = $drUser->getValueName("USER_ADMIN");
        $bAdminAnnu = $drUser->getValueName("USER_ADMIN_ANNU");
        $bAdminAllBassin = $drUser->getValueName("USER_ADMIN_ALL_BASSIN");       
    		$bassin_id = $drUser->getValueName("BASSIN_ID");
        
        $dsBassin = $this->oQuery->getDs_listeBassinByAgent($id);
            while ($drBassin=$dsBassin->getRowIter()){
              array_push($tabBassin, $drBassin->getValueName("BASSIN_ID"));
        }
    }
   
    $oCtrlH = new HtmlHidden("hid", "1");
		$oCtrl = new HtmlText(0, "nom", $strNom, "Nom", 1, $iWidthTxt, 100);
	  $oCtrl->addValidator("formUser", "text", true);
	  $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
		$oCtrl = new HtmlText(0, "prenom", $strPrenom, "Pr�nom", 1, $iWidthTxt, 40);
	  $oCtrl->addValidator("formUser", "text", true);
	  $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);		
    // login visible uniqt sur sa fiche ou mode ajout
    if(($iMode == 2 && $_SESSION["sit_idUser"] == $id) || $iMode == 1/*|| ($_SESSION["bAdminUserBassinCourant"]==1 && $_SESSION["idBassin"]==$bassin_id) || $_SESSION["bAdminUserAllBassin"]==1*/){

     $oCtrl = new HtmlText(0, "login", $strLogin, "Login", 1, $iWidthTxt, 40);
	   $oCtrl->addValidator("formUser", "text", true);
	   $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
    }else{
      $oCtrlH->addHidden("login", $strLogin);
    }
    // en mode update 
    if (( $iMode == 1)){ // en mode cr�ation
      $intitule = "Mot de passe";
	    $oCtrl = new HtmlText(0, "pwd", $strPwd, $intitule, 1, $iWidthTxt, 40);
      $oCtrl->bPassword = true;
      $oCtrl->addValidator("formUser", "text", true);
      $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
    
    } elseif(($iMode == 2 && $_SESSION["sit_idUser"] == $id) /*|| ($_SESSION["bAdminUserBassinCourant"]==1 && $_SESSION["idBassin"]==$bassin_id) || $_SESSION["bAdminUserAllBassin"]==1*/){
	  	$intitule = "Mot de passe";
      $oCtrl = new HtmlText(0, "pwd", "********", $intitule, 1, $iWidthTxt, 40);
      $oCtrl->bPassword = true;
      $oCtrl->addValidator("formUser", "text", true);
      $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);
	  }else{
	  	$oCtrlH->addHidden("pwd", $strPwd);
	  }

	  if ($_SESSION["bAdminUserBassinCourant"]==1 || $_SESSION["bAdminUserAllBassin"]==1){
		  $oCtrlCB = new HtmlCheckBox(0, "valide", $bValide, "Compte valide&nbsp;");
		  $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB);
		  
     // $oCtrlCB = new HtmlCheckBox(0, "admin", $bAdmin, "Administrateur annuaire");
		 // $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB);	  	  
	  
      $oCtrlCB = new HtmlCheckBox(0, "user_admin_annu", $bAdminAnnu, "Administrateur de l'annuaire de son bassin");
      $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB);        
      
      if($_SESSION["bAdminUserAllBassin"]==1){
        $oCtrlCB = new HtmlCheckBox(0, "user_admin_all_bassin", $bAdminAllBassin, "Administrateur de tous les bassins");
        $oCtrlCB->addEvent("onclick", "majListBassin();");
        $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB); 

      }else{
      	if($iMode!=1){
          $oCtrlCB = new HtmlCheckBox(1, "user_admin_all_bassin", $bAdminAllBassin, "Administrateur de tous les bassins");
          $oCtrlCB->addEvent("onclick", "majListBassin();");
          $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrlCB); 
        }else{
        	$oCtrlH->addHidden("user_admin_all_bassin", $bAdminAllBassin);  
        }
      }    

    } else {
	  	$oCtrlH->addHidden("valide", $bValide);
	  	$oCtrlH->addHidden("admin", $bAdmin);
      $oCtrlH->addHidden("user_admin_annu", $bAdminAnnu);
      $oCtrlH->addHidden("user_admin_all_bassin", $bAdminAllBassin);	  
    }
    $oCtrl = new HtmlSelect(0, "bassin_id[]", $tabBassin, "Gestionnaire pour le ou les bassin(s)", 5);	  
    //$oCtrl = new HtmlSelect(0, "bassin", $bassin_id, "Gestionnaire pour le bassin", 1);
    if($_SESSION["bAdminUserAllBassin"]!=1 &&(($_SESSION["bAdminUserBassinCourant"]==1 && $_SESSION["idBassin"] == $bassin_id ) || ($_SESSION["bAdminUserBassinCourant"]==1 && $iMode==1 ) || (($iMode == 2 && $_SESSION["sit_idUser"] == $id)))){ 
      //$oCtrl->oValTxt = $this->oQuery->getTableForComboByBassinId("BASSIN_HYDROGRAPHIQUE", "BASSIN", 0, -1, $_SESSION["idBassin"]);   
      $oCtrl->oValTxt = $this->oQuery->getListBassinByUserForCombo($id);
    }else{    // prevoir pour la consult et adminall
    	$oCtrl->oValTxt = $this->oQuery->getTableForCombo("BASSIN_HYDROGRAPHIQUE", "BASSIN");
    }
    $oCtrl->bMultiple = true;
    $oCtrl->tabValTxtDefault = array ("-1", "--- S�lectionner un bassin ---");
    
    $oCtrl->AddValidator("formUser", "select", true, "-1");
    
    $strHtmlCtrl .= $this->_getHtmlLigneCtrl($oCtrl);				
        
	  $strHtmlCtrl .= $oCtrlH->getHtml();
	  
	  $strHtml = $this->_getFrameTitleSheet("<a class='LienSommaire' href='01_page_form.php?".
                                          $strParam.ALK_MODE_SSHEET_LIST."'>Liste ".
                                          "utilisateurs</a> / Fiche ").
      "<script language='javascript' src='lib/lib_atlas.js'></script>".                                    
      "<script language='javascript' src='../../lib/lib_formTxt.js'></script>".
      "<script language='javascript' src='../../lib/lib_formSelect.js'></script>".
      "<script language='javascript' src='../../lib/lib_formNumber.js'></script>".
      "<script language='javascript' src='../../lib/lib_formDate.js'></script>".
      "<script language='javascript' src='../../lib/lib_form.js'></script>".
      "<form name='formUser' action='01_page_sql.php?".$strParam.ALK_MODE_SSHEET_SQL."&id=".$id.
      "' method='post'  enctype='multipart/form-data'>".
      "<table class='table1' border='0' cellpadding='2' cellspacing='1' align='center'>".
      "<tr>".
      "<td width='200' height='10'></td>".
      "<td width='500'></td>".
      "</tr>";
          
    $strHtml .= $strHtmlCtrl;
    if($iMode == 1){
      $oBtValid = new HtmlLink("javascript:ValiderAnnu('".$this->iSSheet."')", "Valider la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
      $htmlBtValid = $oBtValid->getHtml();
    }elseif(($iMode == 2 && $_SESSION["sit_idUser"] == $id)|| ($_SESSION["bAdminUserBassinCourant"]==1 && $_SESSION["idBassin"]==$bassin_id)|| $_SESSION["bAdminUserAllBassin"]==1){
      $oBtValid = new HtmlLink("javascript:ValiderAnnu('".$this->iSSheet."')", "Mettre � jour la fiche", 
                             "valid_gen.gif", "valid_gen_rol.gif");
      $htmlBtValid = $oBtValid->getHtml();
    
    }else{
      $htmlBtValid = "";
    }
    
    /*$oBtAnnul = new HtmlLink("01_page_form.php?".$strParam2, "Retour", 
                             "annul_gen.gif", "annul_gen_rol.gif");
		*/
    
    $oBtRetour = new HtmlLink("01_page_form.php?".$strParam2, "Retour", 
                             "retour.gif", "retour_rol.gif");
    
    $strHtml .= "</table><br><div align='center'>".
     						$htmlBtValid."&nbsp;&nbsp;".$oBtRetour->getHtml()."</div>".
      					"</form>";
		
    return $strHtml;
  
  }
  /**
   * @brief Traitement des actions sur dictionnaire simple
   *
   * @return Retourne un string : code html du sous onglet
   */
  function traitementUser()
  {
  	global $queryAtlasAction;
  	
    $iMode = Request("iMode", REQ_GET, "1", "is_numeric");
    $id = Request("id", REQ_GET, "-1", "is_numeric");
	  $page =  Request("page", REQ_GET, "1", "is_numeric");

    $strParam = "iSheet=".$this->iSheet."&iSSheet=".ALK_SSHEET_ANNU_LISTE.
      "&iModeSSheet=".ALK_MODE_SSHEET_LIST."&page=".$page;
    $strUrlBack = "01_page_form.php?".$strParam;

    $oldRang = "-1";
		$tabValue = array();
					  
		$tabValue["USER_NOM"] = array( 0, Request("nom", REQ_POST, ""));
	  $tabValue["USER_PRENOM"] = array( 0, Request("prenom", REQ_POST, ""));
	  $tabValue["USER_LOGIN"] = array( 0, Request("login", REQ_POST, ""));

	  $tabValue["USER_VALIDE"] = array( 1, RequestCheckbox("valide", REQ_POST));
	  $tabValue["USER_ADMIN"] = array( 1, RequestCheckbox("admin", REQ_POST));
    $tabValue["USER_ADMIN_ANNU"] = array( 1, RequestCheckbox("user_admin_annu", REQ_POST));
    $tabValue["USER_ADMIN_ALL_BASSIN"] = array( 1, RequestCheckbox("user_admin_all_bassin", REQ_POST));
	  //$tabValue["BASSIN_ID"] = array( 0, Request("bassin", REQ_POST, "-1"));
	  $strFieldKey = "USER_ID";
	  $strTable = "ANNU_USER";
   
    $tabLogin = array();
    $tabPwd = array();
    $dsListUser = $this->oQuery->getDs_listeUser();
    while( $drListUser = $dsListUser->getRowIter() ) {      
        $tabLogin[] = $drListUser->getValueName("USER_LOGIN");
        $tabPwd[] = $drListUser->getValueName("USER_PWD");
    }

    switch( $iMode ) {
    case "1": // mode ajout
    case "2": // mode modif           
   		
      if( $iMode == "1" ) {
        
        if(in_array(Request("login", REQ_POST, ""),$tabLogin) && in_array($this->encrypt(Request("pwd", REQ_POST, "")),$tabPwd)){
        	return $strUrlBack.= "&err=2";
        }else{//Login OK et pwd
        // en mode creation on encrypt pwd
        $tabValue["USER_PWD"] = array( 0, $this->encrypt(Request("pwd", REQ_POST, ""))); 
        $id = $queryAtlasAction->add_ficheDico($strTable, $strFieldKey, $tabValue);
        }
      } else {// mode modif
        // en mode update si pwd# de celui en base on encrypte
        if(in_array(Request("login", REQ_POST, ""),$tabLogin) && (in_array($this->encrypt(Request("pwd", REQ_POST, "")),$tabPwd) || in_array(Request("pwd", REQ_POST, ""),$tabPwd))){
        
        //  return $strUrlBack.= "&err=2"; --> pas de raison en mode modif
        
        }else{//Login OK    et pwd ok
          if(Request("pwd", REQ_POST, "")!="********"){
            $tabValue["USER_PWD"] = array( 0, $this->encrypt(Request("pwd", REQ_POST, "")));
          }          
        }
        $queryAtlasAction->update_ficheDico($strTable, $strFieldKey, $id, $tabValue);
      }
      
      //gestion des bassins  
      $bRes = $queryAtlasAction->del_ficheDico("ANNU_USER_BASSIN", "USER_ID", $id);
      $tabBassin = Request("bassin_id", REQ_POST, array());

      $tabValue = array();
      $tabValue["USER_ID"]=array( 1, $id);   
      foreach($tabBassin as $bassin_id){
      $tabValue["BASSIN_ID"] = array( 1, $bassin_id); 
        $strSql = $queryAtlasAction->_getPartInsertSql($tabValue);
        $strSql = "insert into ANNU_USER_BASSIN ".$strSql;
        $this->oQueryAction->dbConn->executeSql($strSql);
      }
      return $strUrlBack;
      
      break;

    case "3": // mode suppr
      $bRes = $queryAtlasAction->del_ficheDico($strTable, $strFieldKey, $id);
      if( $bRes == false ) {
        $strUrlBack .= "&err=1";
      }
      return $strUrlBack;
      
      break;
    }
    return $strUrlBack;
  }  
 
   /**
   * Encrypte une chaine de caract�re selon la m�thode param�tr�e (md5 par d�faut, sha1 sinon)
   * @param str  Cha�ne � encrypyter
   * @return string
   */
  function encrypt($str)
  {
    if( !defined("ALK_ANNU_PWD_ENCRYPTION") ) {
      define("ALK_ANNU_PWD_ENCRYPTION", "md5"); 
    }
    return call_user_func(ALK_ANNU_PWD_ENCRYPTION, $str);
  }
 
  function getHtmlJsOnLoad(){
  	//echo  $this->iSSheet." ".ALK_SSHEET_ANNU_LISTE;
  
  	if( $this->iSSheet==ALK_SSHEET_ANNU_LISTE && $this->iModeSSheet == ALK_MODE_SSHEET_FORM ) { 

      return " majListBassin(); ";
  	}
    return "";
  }
}

?>