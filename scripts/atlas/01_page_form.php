<?php
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_pagination.php");
include_once("lib/lib_fichier.php");
include_once("classes/alkfactoryatlas.class.php");
include_once("classes/alkmasseeau.class.php");

$oFactory = new alkFactoryAtlas();
$oAppli =& $oFactory->oAppli;


//-----------------
// Debut code HTML
//-----------------

$strHtml = $oAppli->getHtmlPageHeader();

$strHtml .= $oAppli->getHtmlSheetHeader();
$strHtml .= $oAppli->getHtmlSubSheet();

// contenu
$strHtml .= $oAppli->getHtmlStartofSheet();
$strHtml .= $oAppli->getHtmlBodySheet();
$strHtml .= $oAppli->getHtmlEndOfSheet();

$strHtml .= $oAppli->getHtmlPageFooter();

echo $strHtml;

?>