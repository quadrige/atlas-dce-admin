<?php
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_fichier.php");
include_once("classes/alkfactoryatlas.class.php");
include_once("classes/alkmasseeau.class.php");

$iModeSSheet = Request("iModeSSheet", REQ_GET, ALK_MODE_SSHEET_SQL, "is_numeric");
$iMode = Request("iMode", REQ_GET, "1", "is_numeric");
$bPopup = Request("bPopup", REQ_GET, "0", "is_numeric");

$oFactory = new alkFactoryAtlas();

$oAppli = $oFactory->oAppli;

//-----------------
// Debut code HTML
//-----------------

$strUrlRet = $oAppli->getHtmlBodySheet();

if( $strUrlRet != "" ) {
	if ($bPopup==0 && ($iModeSSheet == ALK_MODE_SSHEET_SQL ||$iMode == 3)) {
  	//echo "<a href='".$strUrlRet."'>retour</a>";
  	header("location: ".$strUrlRet);
	} else {

    echo "<script language = javascript>" .
				"window.top.location = '".$strUrlRet."';".
				"window.close();" .
				"</script>";
	}
}
	
exit();
?>