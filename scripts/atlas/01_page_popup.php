<?php
include_once("lib/lib_session.php");
include_once("api/gen_con.php");
include_once("lib/lib_aff.php");
include_once("lib/lib_pagination.php");
include_once("lib/lib_fichier.php");
include_once("classes/alkfactoryatlas.class.php");
include_once("classes/alkmasseeau.class.php");

$oFactory = new alkFactoryAtlas();
$oAppli =& $oFactory->oAppli;

//-----------------
// Debut code HTML
//-----------------

$strHtml = $oAppli->getHtmlPagePopupHeader();

$strHtml .= "<script language='javascript' src='lib/lib_atlas.js'></script>";

// contenu
$strHtml .= $oAppli->getHtmlBodySheet();

$strHtml .= $oAppli->getHtmlPagePopupFooter();

echo $strHtml;

?>