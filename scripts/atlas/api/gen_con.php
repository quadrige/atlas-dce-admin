<?php

  $dbConn = null;

  include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."alkobject.class.php");
	include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."alkquery.class.php");
	include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."alkdataform.class.php");

  switch( ALK_TYPE_BDD ) {
  case SGBD_ORACLE:
    include_once("./../../classes/err/errOracle.class.php");
    include_once("./../../classes/db/droracle.class.php");
    include_once("./../../classes/db/dsoracle.class.php");
    include_once("./../../classes/db/dboracle.class.php");

    $dbConn = new DbOracle(ALK_ORA_LOGIN, ALK_ORA_PWD, ALK_ORA_SID);
    $dbConn->connect();

    break;

  case SGBD_MYSQL:
    include_once("./../../classes/err/errMysql.class.php");
    include_once("./../../classes/db/drmysql.class.php");
    include_once("./../../classes/db/dsmysql.class.php");
    include_once("./../../classes/db/dbmysql.class.php");
    $dbConn = new DbMySql(ALK_MYSQL_LOGIN,  ALK_MYSQL_HOST,  ALK_MYSQL_PWD,  ALK_MYSQL_BD,  "",  ALK_MYSQL_PORT);
    $dbConn->connect();

    break;
    
  case SGBD_POSTGRES:
    include_once("./../../classes/err/errPgsql.class.php");
    include_once("./../../classes/db/drpgsql.class.php");
    include_once("./../../classes/db/dspgsql.class.php");
    include_once("./../../classes/db/dbpgsql.class.php");
    $dbConn = new DbPgSql(ALK_POSTGRES_LOGIN,  ALK_POSTGRES_HOST,  ALK_POSTGRES_PWD,  ALK_POSTGRES_BD,  ALK_POSTGRES_PORT);
    $dbConn->connect();

    break;
  }

include_once("./classes/queryatlas_".ALK_TYPE_BDD.".class.php");
include_once("./classes/queryatlas_action_".ALK_TYPE_BDD.".class.php");

$queryAtlas = null;
$queryAtlasAction = null;
switch( ALK_TYPE_BDD ) {
 case SGBD_ORACLE :
   $queryAtlas = new QueryAtlasOracle($dbConn, $_LG_tab_langue);
   $queryAtlasAction = new QueryAtlasActionOracle($dbConn, $_LG_tab_langue);
   break;
   
 case SGBD_MYSQL :
   $queryAtlas = new QueryAtlasMysql($dbConn, $_LG_tab_langue);
   $queryAtlasAction = new QueryAtlasActionMysql($dbConn, $_LG_tab_langue);
   break;
   
 default:
   echo "Impossible d'instancier les classes atlas.";
   exit();
}

include_once("./classes/queryannu_".ALK_TYPE_BDD.".class.php");
include_once("./classes/queryannu_action_".ALK_TYPE_BDD.".class.php");

$queryAnnu = null;
$queryAnnuAction = null;
switch( ALK_TYPE_BDD ) {
 case SGBD_ORACLE :
   $queryAnnu = new QueryAnnuOracle($dbConn, $_LG_tab_langue);
   $queryAnnuAction = new QueryAnnuOracle($dbConn, $_LG_tab_langue);
   break;
   
 case SGBD_MYSQL :
   $queryAnnu = new QueryAnnuMysql($dbConn, $_LG_tab_langue);
   $queryAnnuAction = new QueryAnnuActionMysql($dbConn, $_LG_tab_langue);
   break;
   
 default:
   echo "Impossible d'instancier les classes annu.";
   exit();
}
?>