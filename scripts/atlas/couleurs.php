<?
include_once("lib/lib_session.php");
?>
<html>
<!-- DATE DE CREATION: 25/06/00 -->
<head>
<title>S�lectionnez une couleur</title>
<metaname="Description" content="">
<metaname="Keywords" content="">
<metaname="Author" content="BERNARD MARTIN-RABAUD">
<metaname="Generator" content="WebExpert">
<script language="Javascript">
var ALK_URL_SI_MEDIA = '<?=ALK_URL_SI_MEDIA?>';
var ALK_URL_SI_IMAGES = '<?=ALK_URL_SI_IMAGES?>';
</script>
<script language="Javascript" src="lib/couleurs.js"></script>
<style type="text/css">
<!--

body {
  margin: 10px 5px;
    font-family:Verdana,Arial,Helvetica,sans-serif;
  font-size:12px; 
  background-color: #fff;
} 

/* partie gauche : palette de couleurs */
/* attention : la largeur de la palette est seulement indicative */
/* elle est corrig�e par le script */
/* si les bo�tes se placent sous la palette, il faudra �largir la page dans l'appel de la popup */
#palette {
  float: left;
  width: 300px;
}

/* cellules de la palette de couleurs */
#palette span {
  margin:0;
  padding:0;
  border:0;
}

#palette span a {
}

#palette span a:hover {
}

#palette span img.normal {
  border-right: 1px #fff solid;
  border-bottom: 1px #fff solid;
}

#palette span img.selection {
  border-right: 1px #000 solid;
  border-bottom: 1px #000 solid;
}

#palette .palette-erreur {
  color: #f00;
}


/* partie droite : bo�tes */
#boites {
  float: left;
  width: 60px;
}

/* chaque bo�te */
#boites div {
  margin-bottom: 2px;
  width: 100%;
}

/* bo�tes t�moin */

#boites #temoin_survol {
  width: 100%;
  height: 20px;
  margin: 0;
  border-right: 1px #777 solid;
}

#boites #temoin {
  width: 100%;
  height: 20px;
  margin-bottom: 2px;
  border-right: 1px #777 solid;
  border-bottom: 1px #777 solid;
}

#boites input {
  border: 1px #777 solid;
  font-size: 11px;
  color: #000;
}

/* bo�te composante rouge */
#boites input.inputr {
  background-color: #f00;
  color: #fff;
}

/* bo�te composante vert */
#boites input.inputv {
  background-color: #00a000;
  color: #fff;
}

/* bo�te composante bleu */
#boites input.inputb {
  background-color: #00f;
  color: #fff;
}

/* listes */
#boites select {
  border: 1px #777 solid;
  font-size: 11px;
}

#boites .saturation {
  margin-top: 3px;
  font-size: 11px;
}

#boites .saturation select {
  text-align: right;
  font-size: 10px;
}

/* boutons */
.boutons input {
  border: 2px #ccc outset;
  font-size: 11px;
  width: 100%;
}

/* uniquement gecko */
.boutons input:hover {
  border: 2px #ccc inset;
}

-->
</style>
</head>
<body>
<script language="Javascript">
<!-- Debut script
// affichage de la page
afficherPage();
//  Fin script -->
</script>


</body>
</html>