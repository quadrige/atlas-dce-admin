<?php

/**
 * @brief Retourne l'url compl�te du fichier html d'aide.
 *        Le nom du fichier d'aide correspond au nom du fichier php avec l'extension .xml
 *
 * @return Retourne l'url relative de l'aide associ�e � la page en cours
 */
function get_strLienAide()
{
  //	global $iType;
  
  $strPageCur = $_SERVER["PHP_SELF"];

  /** version 1 : aide xml alkante
	$strPageCur = substr($strPageCur, 1); // enleve le premier caract�re = /

	// remplace le dernier / par /aide/
	$iPos = strrpos($strPageCur, "/");
	if( !($iPos === FALSE) ) {
		$strPageCur = substr($strPageCur, 0, $iPos)."/aide".substr($strPageCur, $iPos);
	}

	// enleve l'extension du fichier courant
	$iPos = strpos($strPageCur, '.');
	if( !($iPos === FALSE) ) {
		$strPageCur = substr($strPageCur, 0, $iPos);
	}

	if( $iType!= "" ) {
		$strPageAide = $strPageCur.$iType.".xml";
  } else {
		$strPageAide = $strPageCur.".xml";
  }
  */

  /** version 2 : avec help maker */
  
  $strPageAide = "";
  $iPos = strpos($strPageCur, "scripts/");
  if( !($iPos === false) ) {
    $strPageAide = strtolower(substr($strPageCur, $iPos+8));
    $strPageAide = str_replace(".php", "", $strPageAide);
    $strPageAide = str_replace("/", "_", $strPageAide);
  }

	return $strPageAide;
}

/**
 * @brief Affiche la fonction javascript d'ouverture de la popup Aide
 *
 */
function Aff_AideJS()
{
  echo getHtmlAideJS();
}

/**
 * @brief Retourne la fonction JS permettant d'ouvrir la fen�tre d'aide de la page en cours
 *
 * @return Retourne un string
 */
function getHtmlAideJS()
{
  return " function OpenAide() {".
    " var ht = 400;".
    " var lg = 950;".
    " var t = (screen.height-ht)/2;".
    " var l = (screen.width-lg)/2;".
    " var strPage = \"".ALK_SIALKE_URL."scripts/help/index.php?url=".urlEncode(get_strLienAide())."\";".
    " window.open(strPage, 'WindowHelp',".
    "  'status=no,scrollbars=yes,resizable=yes,height='+ht+',width='+lg+',top='+t+',left='+l);".
    " }";
}

?>