<?
if( ALK_B_API_FORM_CLASS == true ) {
  include_once("./../../classes/appli/alkobject.class.php");
  include_once("./../../classes/form/htmlbase.class.php");

  include_once("./../../classes/form/htmlpanel.class.php");
  include_once("./../../classes/form/htmlpage.class.php");

  include_once("./../../classes/form/htmlctrlbase.class.php");
  include_once("./../../classes/form/html.class.php");
  include_once("./../../classes/form/htmllink.class.php");
  include_once("./../../classes/form/htmltext.class.php");
  include_once("./../../classes/form/htmlfile.class.php");
  include_once("./../../classes/form/htmlselect.class.php");
  include_once("./../../classes/form/htmlselectdepend.class.php");
  include_once("./../../classes/form/htmlcheckbox.class.php");
  include_once("./../../classes/form/htmlradio.class.php");
  include_once("./../../classes/form/htmlhidden.class.php");
  include_once("./../../classes/form/htmlupload.class.php");
  include_once("./../../classes/form/htmlradiogroup.class.php");
  include_once("./../../classes/form/htmlcheckboxgroup.class.php");
  include_once("./../../classes/form/htmltexteditor.class.php");
  include_once("./../../classes/form/htmldate.class.php");  
  include_once("./../../classes/form/htmldatetime.class.php");  
  include_once("./../../classes/form/htmlgroup.class.php");

  include_once("./../../classes/form/htmlpagine.class.php");
  include_once("./../../classes/form/htmlform.class.php");  
  include_once("./../../classes/form/htmlblock.class.php");  

  include_once("./../../classes/form/htmlsheets.class.php");  
  include_once("./../../classes/form/htmlformsheets.class.php");  
}

/**
 * @brief Classe utitlis� par GetStrWithUrl
 */
function GetStrUrl($strTxt, $iDeb, &$iPosDeb, &$iPosFin, $strIntro, $strClassA, $strTarget)
{
  $strRes = "";

  // recherche la position de $strIntro � partir de la position $iDeb
  $iPosDeb = strpos($strTxt, $strIntro, $iDeb);
  if( $iPosDeb === false )
    $strRes = "";
  else {
    // recherche la position de fin : premier espace apr�s $intro
    $iPosFin = strpos($strTxt, " ", $iPosDeb+1);
    if( $iPosFin===false )
      $iPosFin = strlen($strTxt);
    $strUrl = substr($strTxt, $iPosDeb+strlen($strIntro), $iPosFin-$iPosDeb-strlen($strIntro));

    $strRes = "<a ";
    if( $strClassA != "" )
      $strRes .= "class=\"".$strClassA."\" ";
    if( $strTarget != "" && $strIntro!="mailto:" )
      $strRes .= "target=\"".$strTarget."\" ";
    $strRes .= "href=\"".$strIntro.$strUrl."\">".$strUrl."</a>";
  }
  return $strRes;
}

/**
 * @brief Retourne une chaine de caracteres apr�s avoir ajout� le tag <a></a>
 *        entre les textes suivant:
 *          [espace|debligne]http://....[espace|finligne]
 *          [espace|debligne]ftp://....[espace|finligne]
 *          [espace|debligne]mailto://....[espace|finligne]
 *
 * @param strParam  : chaine source � parser
 * @param strClassA : classe de style � appliquer sur le lien
 * @param strTarget : target �ventuel
 * @return Retourne la chaine eventuellement modifi�e
 */
function GetStrWithUrl($strParam, $strClassA, $strTarget)
{
  if( strlen($strParam) < 8 )
    return $strParam;

  $tabIntro = array("http://", "ftp://", "mailto:");

  for($i=0; $i<3; $i++) {
    $iDeb = 0;
    $strRes = "";
    while( $iDeb<strlen($strParam) ) {
      // recherche le d�but de la position
      $strUrl = GetStrUrl($strParam, $iDeb, $iPosDeb, $iPosFin, $tabIntro[$i], $strClassA, $strTarget);
      if( $strUrl != "" ) {
        $strRes .= substr($strParam, $iDeb, $iPosDeb-$iDeb).$strUrl;
        $iDeb = $iPosFin;
      } else {
        $strRes .= substr($strParam, $iDeb, strlen($strParam)-$iDeb);
        $iDeb = strlen($strParam);
      }
    }
    $strParam = $strRes;
  }
  return $strParam;
}

/**
 * @brief Retourne le code HTML API v2 de l'entete d'un tableau 
 *        formulaire de saisie  avec une largeur d�finie par 
 *        d�faut � 580 = 180+5+390+5
 *        deux colonnes : label - valeur suivi d'un trait
 * @param iWidthLabel Largeur colonne label (=180 par d�faut)
 * @param iWidthCtrl  Largeur colonne controle (=390 par d�faut)
 * @return Retourne chaine caract�re
 */
function GetHtmlTableCtrlHeader($iWidthLabel=180, $iWidthCtrl=390)
{
  $iWidth = ( $iWidthCtrl=="" ? "100%" : $iWidthCtrl-6 );

  return "<table border='0' cellpadding='0' cellspacing='0'".
    ($iWidth=="100%" ? " width='100%'" : "").">".
    "<tr>".
    "<td width='".$iWidthLabel."' height='0'><img width='".$iWidthLabel."' height='0'></td>".
    "<td width='5'></td>".
    "<td width='1'></td>".
    "<td width='5'></td>".
    "<td width='".$iWidth."'></td>".
    "<td width='5'></td>".
    "</tr>";
}

function GetHtmlTableCtrlTitle($strTitle)
{
   return "<tr>".
     "<td colspan='6' class='tdFormTitle'>".
     $strTitle.
     "</td>".
     "</tr>";
}

function GetHtmlTableCtrlConseil($strConseil)
{
   return "<tr>".
     "<td colspan='6' class='divContenuConseil'>".
     $strConseil.
     "</td>".
     "</tr>";
}

/**
 * @brief Retourne le code HTML API v2 de la fin de tableau
 *        formulaire de saisie
 *
 * @return Retourne chaine caract�re
 */
function GetHtmlTableCtrlFooter()
{
  return "</table>";
}

/**
 * @brief Retourne le code HTML API v2 d'une ligne vide de hauteur
 *        param�trable d'un tableau formulaire de saisie
 *
 * @param iHeight Hauteur en pixels de la ligne vide (=5 par d�faut)
 * @return Retourne chaine caract�re
 */
function GetHtmlTableCtrlSpaceLine($iHeight=5)
{
  return "<tr><td colspan='6' height='".$iHeight."'></td></tr>";
}

/**
 * @brief Retourne le code HTML API v2 d'une ligne de tableau
 *        formulaire de saisie :  label - controle de saisie
 *
 * @param strLab    Intitule du label de la valeur
 * @param strVal    Valeur de la donnees
 * @param bVertival =true : affiche le controle sur 2 lignes, sur 1 ligne sinon (=false par d�faut)
 * @param strBt     code html du bouton � afficher (=vide par d�faut)
 * @return Retourne chaine caract�re
 */
function GetHtmlTableCtrlLine($strLab, $strVal, $bVertical=false, $strBt="", $strClassLabel="formLabel")
{
  $strClass = " class='divContenuTexte'";
  if( substr($strVal, 0, 1) == "<" ) $strClass = "";

  if( $bVertical == false ) {
    return "<tr>".
      "<td align='right' valign='top'>".
      "<span class='".$strClassLabel."'>".$strLab."&nbsp;</span>".
      "</td>".
      "<td></td>".
      "<td valign='top' colspan='3' ".$strClass." style='padding-bottom:2px'>".
      ($strVal=="" ? "&nbsp;" : $strVal)."</td>".
      "<td></td>".
      "</tr>";
  }
    
  return "<tr>".
    "<td align='right' valign='top'><span class='".$strClassLabel."'>".$strLab."&nbsp;</span></td>".
    "<td></td>".
    "<td colspan='3' align='left' valign='top'>".
    "<table width='90%' border='0' cellpadding='0' cellspacing='0'>".
    "<tr>".
    "<td align='right' valign='top'><span class='".$strClassLabel."'>".$strBt."&nbsp;</span></td>".
    "</tr>".
    "</table>".
    "</td>".
    "<td></td>".
    "</tr>".
    "<tr>".
    "<td colspan='2'></td>".
    "<td colspan='3' align='left' style='padding-bottom:2px' valign='top' ".$strClass.">".
    ($strVal=="" ? "&nbsp;" : $strVal)."</td>".
    "<td></td>".
    "</tr>";
}

/**
 * @brief Retourne le code HTML API v2 d'un label suivi d'un controle de saisie
 *        affich�s sur 2 lignes et sur toutes la largeur du tableau
 * @param strLab        Intitule du label de la valeur
 * @param strVal        Valeur de la donnees
 * @param strClassLabel Nom de la classe CSS du controle de saisie (=formLabel par d�faut)
 * @return Retourne un string
 */
function GetHtmlTableCtrlOn2Lines($strLab, $strVal="Pas de Valeur", $strClassLabel="formLabel")
{
  $strClass = " class='divContenuTexte'";
  if( substr($strVal, 0, 1) == "<" ) $strClass = "";

  return "<tr>".
    "<td colspan='6' valign='top'><span class='".$strClassLabel."'>".$strLab."&nbsp;</span></td>".
    "</tr>".
    ( $strVal == "Pas de Valeur"
      ? ""
      : "<tr><td colspan='6' valign='top'>".($strVal=="" ? "&nbsp;" : $strVal)."</td></tr>" );
}

/**
 * @brief Retourne le code HTML API v2 d'une ligne de tableau
 *        formulaire de saisie en mode lecture :  label - controle de saisie
 *        le label est class� CSS tdEntete1
 *
 * @param strLab    Intitule du label de la valeur
 * @param strVal    Valeur de la donnees
 * @param bAffSep   =true : affiche un trait s�parateur avant la ligne
 * @return Retourne chaine caract�re
 */
function GetHtmlTableCtrlLinePopup($strLab, $strVal, $bAffSep=false, $bAffIfEmpty=false)
{
  if( $strVal == "" && $bAffIfEmpty==false ) 
    return "";

  $strClass = "tdCtrlPopupLine";
  if( substr($strVal, 0, 1) == "<" )
    $strClass = "";

  $strHtml = "";
  if( $bAffSep == true )
    $strHtml = "<tr>".
      "<td colspan='6' height='1' class='tdBordureFine'><img width='1' height='1'></td>".
      "</tr>";

  $strHtml .= "<tr>".
    "<td class='tdLabelPopupLine' align='right' valign='top' height='20' style='padding:2px;' colspan='2'>".
    $strLab."&nbsp;</td>".
    "<td class='tdBordureFine'><img width='1' height='1'></td>".
    "<td class='".$strClass."' valign='top' style='padding:2px; padding-left:4px' colspan='3'>".
    ($strVal=="" ? "&nbsp;" : $strVal)."</td>".
    "</tr>".
    "<tr>".
    "<td colspan='6' height='1' class='tdBordureFine'><img width='1' height='1'></td>".
    "</tr>";
  return $strHtml;
}

/** Affiche le r�sultat de GetHtmlTableCtrlHeader */
function AffTableCtrlEntete($iWidthLabel=180, $iWidthCtrl=390)
{
  echo GetHtmlTableCtrlHeader($iWidthLabel, $iWidthCtrl);
}

/** Affiche le r�sultat de GetHtmlTableCtrlTitle */
function AffTableCtrlTitle($strTitle)
{
  echo GetHtmlTableCtrlTitle($strTitle);
}

/** Affiche le r�sultat de GetHtmlTableCtrlFooter */
function AffTableCtrlPied()
{
  echo GetHtmlTableCtrlFooter();
}

/** Affiche le r�sultat de GetHtmlTableCtrlSpaceLine */
function AffTableCtrlLigneVide($iHeight=5)
{
  echo GetHtmlTableCtrlSpaceLine($iHeight);
}

/** Affiche le r�sultat de GetHtmlTableCtrlLine */
function AffTableCtrlLigne($strLab, $strVal, $bVertical=false, $strBt="", $strClassLabel="formLabel")
{
  echo GetHtmlTableCtrlLine($strLab, $strVal, $bVertical, $strBt, $strClassLabel);
}

/** Affiche le r�sultat de GetHtmlTableCtrlLinePopup */
function AffTableCtrlLignePopup($strLab, $strVal, $bAffSep=false)
{
  echo GetHtmlTableCtrlLinePopup($strLab, $strVal, $bAffSep);
}



//
// les fonctions ci-dessous sont appel�es � dispara�tre
//


//-----------------------------------------------------------------------------------------
// IsIE
// but : renvoie 1 si le navigateur est IE
// Sans param�tre
//-----------------------------------------------------------------------------------------
function IsIE()
{
  $strNavigateur = STRNAVIGATOR;
  if( substr($strNavigateur, 0, 3) != "Net" )
    return 1;
  return 0;
}


//-----------------------------------------------------------------------------------------
// accent
// but : remplace les voyelles accentu�es par leur equivalent non accentu� 
// param�tres :
//    $str : chaine � traiter
// Retourne :
//      la chaine trait�e
//-----------------------------------------------------------------------------------------
function accent($str)
{
  return strtr($str, "���������������", "eeeeaaauuuiiooc");
}

///<comment>
/// API v1
/// Affiche dans un tableau de largeur 580, 1 lignes de 4 colonnes de tableau vide de hauteur param�trable
/// <param name="iHauteur">hauteur en pixels de la ligne vide</param>
///</comment>
function AffLigneDataCtrlVide($iHauteur)
{
  echo "<tr>".
    "<td colspan=4 height=".$iHauteur.">".
    "<img width=1 height=1></td>".
    "</tr>";
}

///<comment>
/// API v1
/// Affiche dans un tableau de largeur 580, deux lignes de tableau : label - valeur suivi d'un trait
/// <param name="strLib">Intitule du label de la valeur</param>
/// <param name="strVal">Valeur de la donnees</param>
///</comment>
function AffLigneDataCtrl($strLib, $strVal)
{
  $strClasse = "divContenuTexte";
  if( substr($strVal, 0, 1) == "<" )
    $strClasse = "\"\"";
  echo"
    <tr>
      <td width=180 bgcolor=\"#FFFFFF\" align=\"right\" valign=top height=\"20\">
        <span class=\"formLabel\">".$strLib."&nbsp;</span>
      </td>
      <td width=5 bgcolor=\"#ffffff\">
        <img width=1 height=1></td>
      <td width=390 bgcolor=\"#ffffff\" valign=top class=".$strClasse.">".($strVal=="" ? "&nbsp;" : $strVal)."</td>
      <td width=5 bgcolor=\"#ffffff\">
        <img width=1 height=1></td>
    </tr>
    <tr>
      <td colspan=4 height=2>
        <img width=1 height=1></td>
    </tr>";
}

///<comment>
/// API v1
/// Affiche dans un tableau de largeur 580, deux lignes de tableau : label - valeur suivi d'un trait
///</comment>
function AffLigneData($strLib, $strVal)
{
  $strClasse = "divContenuTexte";
  if( substr($strVal, 0, 1) == "<" )
    $strClasse = "\"\"";
  echo"
    <tr> 
      <td width=1 background=\"".ALK_URL_SI_MEDIA."pictos/tab_filet_gris.gif\">
        <img width=1 height=1></td>
      <td width=180 class=tdEntete1 align=\"right\" valign=top height=\"20\">
        <span class=\"divTabEntete1\">".($strLib=="" ? "&nbsp;" : $strLib)."</span>
      </td>
      <td width=5 class=tdEntete1>
        <img width=1 height=1></td>
      <td width=1 background=\"".ALK_URL_SI_MEDIA."pictos/tab_filet_gris.gif\">
        <img width=1 height=1></td>
      <td width=5 bgcolor=\"#ffffff\">
        <img width=1 height=1></td>
      <td width=382 bgcolor=\"#ffffff\" valign=top class=".$strClasse.">".($strVal=="" ? "&nbsp;" : $strVal)."</td>
      <td width=5 bgcolor=\"#ffffff\">
        <img width=1 height=1></td>
      <td width=1 background=\"".ALK_URL_SI_MEDIA."pictos/tab_filet_gris.gif\">
        <img width=1 height=1></td>
    </tr>
    <tr> 
      <td colspan=8 height=1 background=\"".ALK_URL_SI_MEDIA."pictos/tab_filet_gris.gif\">
        <img width=1 height=1></td>
    </tr>";
}


//-----------------------------------------------------------------------------------------
// GetStrCtrlHtml
// but : retourne le code html d'un controle de saisie
// Param�tres :
//    $iFlag    0 la fonction ne fait rien, 1 elle fonctionne -> gestion des droits avec $iMode
//    $iType    1->memo, 2->text, 3->checkbox, 4->file input, 5-> file voir/supr
//              6-> liste deroulante tableau, 7 -> Liste d�roulante Recordset, 8 -> Date/Heure
//              9-> password
//    $iMode    1->lecture, 2-> modifiable
//    $strname  nom de l'objet
//    $strTitre  titre de la ligne (1ere cellule)
// Param�tres optionnels :
//    $strValue  valeur de l'objet
//    $iLongueur   longueur de l'objet
//    $varOption1  hauteur du memo, longueur max d un input text, tableau pour le type 6(pas implemente)
//    $varOption2 onkeyup pour un memo
//    $varOption3
//    $varOption4 hauteur de la liste � choix multiple (type 9)
//-----------------------------------------------------------------------------------------
function GetStrCtrlHtml($iFlag, $iType, $iMode, $strName, $strTitre)
{
  $strClass = "class=\"formCtrl\"";
  $bIE = true;
  if( STRNAVIGATOR == NAV_NETSCAPE4 ) {
    $strClass = "";
    $bIE = false;
  }

  $strHtml = "";

  $strValue = "";
  $iLongueur = 30;
  $iHauteur = 0;
  $varOption1 = 0;
  $varOption2 = "";
  $varOption3 = "";
  $varOption4 = "";
  $varOption5 = "";
  $numargs = func_num_args()-1;

  if( $numargs>=5 ) $strValue = func_get_arg (5);
  if( $numargs>=6 ) $iLongueur = func_get_arg (6);
  if( $numargs>=7 ) $varOption1 = func_get_arg (7);
  if( $numargs>=8 ) $varOption2 = func_get_arg (8);
  if( $numargs>=9 ) $varOption3 = func_get_arg (9);
  if( $numargs>=10 ) $varOption4 = func_get_arg (10);
  if( $numargs>=11 ) $varOption5 = func_get_arg (11);

  $iDelta = 0;
  if( $iLongueur>30 ) $iDelta = -24;

  if( $iFlag!=0 ) {
    if( $iMode!=2 ) { // mode lecture
      switch( $iType ) {
      case 1 : //memo
      case 2 : //text
        $strHtml .= $strValue;
        break;

      case 3 : //checkbox
        if ($strValue=="1")
          $strHtml .= " Oui ";
        else
          $strHtml .= " Non ";
        break;

      case 4 : //file
        break;

      case 5 : //file voir/supr
        if( $strName!="" )
          $strHtml .= "<a class=\"aContenuLien\" href=\"$strValue\">$strName</a>";
        else
          $strHtml .= "<span class=divContenuTexte>(pas de fichier disponible)</span>";
        break;

      case 6 : //liste deroulante tableau
        $strHtml .= get_LDTab($strName, $varOption1, $strValue, $iLongueur, $iType);
        break;

      case 7 : //liste deroulante recordset
        $strHtml .= get_LDRS($strName, $varOption1, "", "", $strValue, $iLongueur, $iMode, "", "", $varOption4, $varOption5);
        break;

      case 8 : //date heure
        $strHtml .= $strValue;
        if( $varOption2 != "" )
          $strHtml .= "&nbsp;&agrave;&nbsp;".$varOption2;
        break;

      case 9: // type password
        $strHtml .= "**********";
        break;

      default:
        break;
      }
    } else { // mode maj
      switch( $iType ) {
      case 1 : //memo
        $strHtml .= "<TEXTAREA $strClass rows=$varOption1 cols=$iLongueur name=\"$strName\" onkeyup=\"$varOption2\"";
        if( $bIE == true )
          $strHtml .= " style=\"width:".(($iLongueur+2)*8+$iDelta)."px\"";
        $strHtml .= ">".$strValue;
        $strHtml .= "</TEXTAREA>";
        break;

      case 2 : //text
        $strHtml .= "<input type=\"text\" $strClass name=\"$strName\" value=\"$strValue\" size=\"$iLongueur\"";
        if( $varOption1>0 )
          $strHtml .= " maxlength=\"$varOption1\" ";
        if( $varOption2=="readonly" )
          $strHtml .= " readonly ";
        if( $bIE == true )
          $strHtml .= " style=\"width:".($iLongueur*8+$iDelta)."px\"";
        $strHtml .= ">";
        break;
              
      case 3 : //checkbox
        $strHtml .= "<input type=\"checkbox\" $strClass name=\"$strName\" value=\"1\"";
        if( $strValue=="1" )
          $strHtml .= " checked ";
        if ($varOption1!="")
          $strHtml .= " onclick=\"javascript:$varOption1\" ";
        $strHtml .= ">";
        break;
              
      case 4 : //file input
        $strHtml .= "<input type=\"file\" $strClass name=\"$strName\" value=\"\"";
        $strHtml .= " size=\"".($iLongueur-10)."\"";
        if( $bIE == true )
          $strHtml .= " style=\"width:".($iLongueur*8+$iDelta)."px\"";
        $strHtml .= ">";
        break;
              
      case 5 : //file voir/supr
        if( $strName!="" ) {
          $strHtml .= "<a class=\"aContenuLien\" href=\"$strValue\">$strName</a>";
          if( $varOption1!="" )
            $strHtml .= "&nbsp;&nbsp;<input $strClass type=\"checkbox\" name=\"$varOption1\" value=\"1\">".
              "<span class=divContenuTexte>&nbsp;Effacer</span>";
        } else
          $strHtml .= "<span class=divContenuTexte>(pas de fichier disponible)</span>";
        break;
              
      case 6 : //liste deroulante tableau
        $strHtml .= get_LDTab($strName, $varOption1, $strValue, $iLongueur);
        break;

      case 7 : //liste deroulante recordset
        //$strHtml .= get_LDRS($strName, $varOption1, "", "", $strValue, $iLongueur, $iMode,$varOption2,$varOption3);
        $strHtml .= get_LDRS($strName, $varOption1, "", "", $strValue, $iLongueur, $iMode, $varOption2, $varOption3, $varOption4, $varOption5);
        break;
              
      case 8 : //date heure
        $strHtml .= "<input $strClass type=\"text\" name=\"$strName\" value=\"$strValue\" size=\"$iLongueur\"";
        $strHtml .= " maxlength=\"10\" >&nbsp;&nbsp;Heure : ";
        $strHtml .= "<input $strClass type=\"text\" name=\"$varOption1\" value=\"$varOption2\" size=\"5\"";
        $strHtml .= " maxlength=\"5\" >";
        break;

      case 9: // type password
        $strHtml .= "<input $strClass type=\"password\" name=\"$strName\" value=\"$strValue\" size=\"$iLongueur\"";
        if( $varOption1>0 )
          $strHtml .= " maxlength=\"$varOption1\" ";
        if( $bIE == true )
          $strHtml .= " style=\"width:".($iLongueur*8+$iDelta)."px\"";
        $strHtml .= ">";              
        break;

      default:
        break;
      }
    }
  }
  return $strHtml;
}


//-----------------------------------------------------------------------------------------
// aff_bouton
// but : affiche une image (avec roll +lien en option). Pas de roll pour netscape version < 6
// param�tres :
//    $strImage    nom de l'image
// parametres optionnels :
//    $strImageRol  nom de l'image rol
//    $url      lien
//    $strAlt      info bulle 
//    $target      cible du lien
//-----------------------------------------------------------------------------------------
function aff_bouton($strImage)
{
  global $_aff_bouton_;

  if( ALK_B_PRINT==true && isset($_GET["print"]) && $_GET["print"]=="1" ) {
    echo "&nbsp;";
    return;
  }

  $strUrlBaseImg = ALK_URL_SI_IMAGES;
  
  if( !isset($_aff_bouton_) ) $_aff_bouton_ = 0;
  $_aff_bouton_++;
  $url = "#";
  $strImageRol = "";
  $iRoll = 0;
  $strAlt = "";
  $strTarget = "";
  $numargs = func_num_args();
  
  if( $numargs>=1 ) $strImage = func_get_arg(0);
  if( $numargs>=2 ) {
    $strImageRol = func_get_arg(1);
    if( $strImageRol!="" ) $iRoll = 1;
  }
  if( $numargs>=3 ) $url = func_get_arg(2);
  if( $numargs>=4 ) $strAlt = " title=\"".func_get_arg(3)."\"";
  if( $numargs>=5 ) $strTarget = " target=\"".func_get_arg(4)."\"";

  if( $url != "#" && $url != "" ) {
    if( $iRoll==1 )
      echo "<a ". $strTarget ." href=\"".$url."\" ".
        "onMouseOut=\"MM_swapImgRestore()\"". 
        "onMouseOver=\"MM_swapImage('".$strImage.$_aff_bouton_."', '', '".$strUrlBaseImg.$strImageRol."',1)\"><img ".
        $strAlt." align=absmiddle name=\"".$strImage.$_aff_bouton_."\" border=0 src=\"".
        $strUrlBaseImg.$strImage."\"></a>";
    else
      echo "<a ". $strTarget ." href=\"".$url."\" ".
        $strAlt." align=absmiddle name=\"".$strImage.$_aff_bouton_."\" border=0 src=\"".
        $strUrlBaseImg.$strImage."\"></a>";
  }        
  else
    echo "<img ".$strAlt." name=\"".$strImage.$_aff_bouton_."\" border=0 src=\"".$strUrlBaseImg.$strImage."\">";
}

//-----------------------------------------------------------------------------------------
// get_bouton
// but : affiche une image (avec roll +lien en option). Pas de roll pour netscape version < 6
// param�tres :
//    $strImage    nom de l'image
// parametres optionnels :
//    $strImageRol  nom de l'image rol
//    $url      lien
//    $strAlt      info bulle 
//-----------------------------------------------------------------------------------------
function get_bouton($strImage)
{
  global $_aff_bouton_;

  if( ALK_B_PRINT==true && isset($_GET["print"]) && $_GET["print"]=="1" ) return "&nbsp;";
  
  $strUrlBaseImg = ALK_URL_SI_IMAGES;
  
  if( !isset($_aff_bouton_) ) $_aff_bouton_ = 0;
  $_aff_bouton_++;
  $url = "#";
  $strImageRol = "";
  $iRoll = 0;
  $strAlt = "";
  $strTarget = "";
  $numargs = func_num_args();
  
  if( $numargs>=1 ) $strImage = func_get_arg(0);
  if( $numargs>=2 ) {
    $strImageRol = func_get_arg(1);
    if( $strImageRol!="" ) $iRoll = 1;
  }
  if( $numargs>=3 ) $url = func_get_arg(2);
  if( $numargs>=4 ) $strAlt = " title=\"".func_get_arg(3)."\"";
  if( $numargs>=5 ) $strTarget = " target=\"".func_get_arg(4)."\"";

  if( $url != "#" && $url != "" ) {
    if( $iRoll==1 )
      $strHtml = "<a ". $strTarget ." href=\"".$url."\" ".
        "onMouseOut=\"MM_swapImgRestore()\"". 
        "onMouseOver=\"MM_swapImage('".$strImage.$_aff_bouton_."', '', '".$strUrlBaseImg.$strImageRol."',1)\"><img ".
        $strAlt." align=absmiddle name=\"".$strImage.$_aff_bouton_."\" border=0 src=\"".
        $strUrlBaseImg.$strImage."\"></a>";
    else
      $strHtml = "<a ". $strTarget ." href=\"".$url."\" ".
        $strAlt." align=absmiddle name=\"".$strImage.$_aff_bouton_."\" border=0 src=\"".
        $strUrlBaseImg.$strImage."\"></a>";
  } else
    $strHtml = "<img ".$strAlt." name=\"".$strImage.$_aff_bouton_."\" border=0 src=\"".$strUrlBaseImg.$strImage."\">";

  return $strHtml;
}


//-----------------------------------------------------------------------------------------
// get_LDTab
// but : retourne le code html d'une liste d�roulante par rapport � une table d'association
// param�tres :
//    $strName  nom du control <select>
//    $varOption1  tableau de texte <option>
//    $strValue  tableau de valeur <option>
// param�tres optionnels :
//    $iLongueur  longueur du control en pixels (style) = 250px par d�faut
//-----------------------------------------------------------------------------------------
function get_LDTab($strName, $varOption1, $strValue)
{
  $strClass = "class=\"formCtrl\"";
  if( STRNAVIGATOR == NAV_NETSCAPE4 ) $strClass = "";

  $strHtml = "";
  $iLongueur = 250;
  $numargs = func_num_args();
  if( $numargs>=4 ) $iLongueur=func_get_arg (3);
    
  $strHtml .= "<select $strClass name=\"$strName\" style=\"WIDTH: $iLongueur px\">";
  for ($i=0; $i<count($varOption1); $i++) {
    $strHtml .= "<option value=\"".$varOption1[$i]["id"]."\"";
    if( $varOption1[$i]["id"]==$strValue ) $strHtml .= " selected";
    $strHtml .= ">".$varOption1[$i]["titre"]."</option>";
  }
  $strHtml .= "</select>";
  return $strHtml;
}


//-----------------------------------------------------------------------------------------
// get_LDRS
// but : retourne le code html d'une liste d�roulante par rapport � un recordset
// param�tres :
//   $strname  nom de l'objet
//   $rs    recordset
//   $strTitreChamp  nom du champ BDD correspondant � l'intitul�(sinon 1)
//   $strValueChamp  nom du champ BDD correspondant � la valeur(sinon 2)
//   $strValue  soit valeur s�lectionn�e soit tableau des valeurs s�lectionn�es
// parametres optionnels :
//   $iLongueur   longueur de l'objet
//   $iMode    lecture=1 edition=2
//   $strChange  action declench� sur l'�v�enement onChange
//   $iHauteur  Hauteur de la liste
//   $Multiple  1=liste � s�lection multiple
//-----------------------------------------------------------------------------------------
function get_LDRS($strName, $rs, $strTitreChamp, $strValueChamp, $strValue)
{
  $strClass = "class=\"formCtrl\"";
  if( STRNAVIGATOR == NAV_NETSCAPE4 ) $strClass = "";

  $strHtml = "";

  $iLongueur = 250;
  $iMode = 2;
  $strChange = "";
  
  // iTableauValeur = 1 si la variable $strValue est un tableau
  // sinon = 0
  $iTableauValeur = is_array ($strValue);

  $numargs = func_num_args();
  if( $numargs>=6 ) $iLongueur = func_get_arg(5);
  if( $numargs>=7 ) $iMode = func_get_arg(6);
  if( $numargs>=8 ) {
    if( func_get_arg (7)!="" ) $strChange = "onChange=\"javascript:".func_get_arg (7)."\"";
  }
  $iOptionFlag=0;
  if( $numargs>=9 ) {
    $tabLigne = func_get_arg(8);
    if ($tabLigne!="") $iOptionFlag=1;
  }

  if( ( $numargs>=10 ) && ( func_get_arg (9)!="" ) )
    $iHauteur = "size=\"".func_get_arg(9)."\"";
  else
    $iHauteur = "";

  if( ( $numargs>=11 ) && ( func_get_arg (10)=="1" ) )
    $multiple = "multiple";
  else
    $multiple = "";

  if( $strTitreChamp == "" ) {
    $code = 2;
    $texte = 1;
  } else {
    $code = strtoupper($strValueChamp);
    $texte = strtoupper($strTitreChamp);
  }

  if( $iMode==2 ) {
    $strHtml .= "<select $strClass name=\"$strName\" style=\"WIDTH: $iLongueur px\" $strChange $iHauteur $multiple>";
    if( $iOptionFlag==1 ) {
      if (is_array($tabLigne))
        for ($i=0;$i<count($tabLigne);$i++)
          $strHtml .= "<option value=\"".$tabLigne[$i][0]."\">".$tabLigne[$i][1]."</option>";
      else
        $strHtml .= "<option value=\"0\">$tabLigne</option>";
    }
    
    if( isset($rs) && $rs!="" ) {
      while( ocifetch($rs) ) {
        $strHtml .= "<option value=\"".ociresult($rs, $code)."\"";
        
        if ($iTableauValeur == 1) {
          if (in_array (ociresult($rs, $code), $strValue)) {
            $strHtml .= " selected";
          }
        } else {
          if( ociresult($rs, $code) == $strValue ) $strHtml .= " selected";
        }
              
        $strHtml .= ">".ociresult($rs, $texte)."</option>";
      }
    }
      
    $strHtml .= "</select>";
  }
  
  if( $iMode==1 ) {
    while( ocifetch($rs) ) {
      $strTmp = ociresult($rs,$code);
      if( $strTmp == $strValue )
        $strHtml .= ociresult($rs, $texte);
    }
  }
  
  if( $iMode==3 ) {
    while( ocifetch($rs) )
      $strHtml .= ociresult($rs, $texte)."<br>";
  }
  return $strHtml;
}

//---------------------------------------------------------
// getPictoFile
// but : retourne l'identifiant du type du fichier, determine
//       en fonction de l'extension du fichier
// Param�tres:
//  $fichier : nom du fichier
//---------------------------------------------------------
function getTypeFile($fichier)
{
  $iType = 0;
  $longueur = strlen($fichier);
  if( $longueur>2 ) {
    $i = strrpos($fichier,".");
    if( $i ) {
      $strExt = strtolower(substr($fichier,$i+1));
    
      switch( $strExt ) {
      case "doc":
      case "rtf":
        $iType=1;
        break;

      case "xls":
      case "csv":
        $iType=2;
        break;
      
      case "pdf":
        $iType=3;
        break;

      case "jpg":
      case "gif":
      case "bmp":
        $iType=4;
        break;
        
      case "txt":
        $iType=5;
        break;
      case "htm":
      case "html":
        $iType=6;
        break;

      case "zip":
        $iType=7;
        break;

      case "svg":
      case "jsvg":
        $iType=8; // couche SIG
        //iType = 9 : projet SIG par convention puisque pas de piece jointe de ce type
        break;
      
      case "js":
      case "frm":
      case "style":
        $iType = 99;
        break;
      }
    }
  }
  return ($iType);
}


//---------------------------------------------------------
// getPictoFile
// but : retourne le nom de l'image icone en fonction de
//       l'extension du fichier
// Param�tres:
//  $fichier : nom du fichier
//---------------------------------------------------------
function getPictoFile($fichier)
{
  $strPic = "icon_doc_0.gif";
  $longueur = strlen($fichier);
  if( $longueur>2 ) {
    $iPos = strrpos($fichier, ".");
    if( !($iPos===false) ) {
      $strExt = strtolower(substr($fichier, $iPos+1));
      
      switch( $strExt ) {
      case "doc":
      case "rtf":
        $strPic="icon_doc_2.gif";
        break;

      case "xls":
      case "csv":
        $strPic="icon_doc_1.gif";
        break;
        
      case "pdf":
        $strPic="icon_doc_3.gif";
        break;

      case "png":
      case "tif":
      case "jpg":
      case "gif":
      case "bmp":
        $strPic="icon_doc_4.gif";
        break;
          
      case "txt":
        $strPic="icon_doc_5.gif";
        break;

      case "htm":
      case "html":
        $strPic="icon_doc_6.gif";
        break;

      case "zip":
        $strPic="icon_doc_7.gif";
        break;

      case "svg":
      case "jsvg":
        $strPic="icon_doc_8.gif";
        break;

      case "js":
      case "frm":
      case "style":
        $strPic="icon_doc_8.gif";
        break;
        
      }
    }
  }
  return ($strPic);
}


/**
 * @brief Pr�sente une date sous forme textuelle (d�pendant de la langue)
 * @param date10  Date au format JJ/MM/AAAA
 * @param bAbrev  si vrai : abr�ge le mois (defaut false)
 * @param lg      Langue d'arriv�e (defaut fr)
 * 
 * @return Date sous forme textuelle (JJ Mois AAAA)
 */
function ConvertDateToText($date10, $bAbrev=false, $lg="fr")
{
  if ($bAbrev){
    $tabMonths = array(
      "fr" => array("", "Janv.", "F�v.", "Mars", "Avril", "Mai", "Juin", "Juil.", "Ao�t", "Sept.", "Oct.", "Nov.", "D�c."),
      "en" => array("", "Jan.", "Feb.", "Mar.", "Apr.", "May", "June", "July", "Aug.", "Sept.", "Oct.", "Nov.", "Dec."),
    );
  }
  else {
    $tabMonths = array(
      "fr" => array("", "Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre"),
      "en" => array("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"),
    );
  }
  
  $tabDate = explode("/", $date10);
  if (count($tabDate)!=3) return $date10;
  $strMonth = $tabMonths[$lg][intval($tabDate[1])];
        
  return $tabDate[0]." ".$strMonth." ".$tabDate[2];
}
?>