function OpenWriteToLayer(objDoc, idLayer) {
    if( objDoc.all ) { objDoc.all[idLayer].innerHTML = ""; return; }
    if( objDoc.layers ) { objDoc.layers[idLayer].clear(); return; }    
    else { var objLayer = objDoc.getElementById(idLayer); objLayer.innerHTML = ""; }
}
function WriteToLayer(objDoc, idLayer, strHtml) {
    if( objDoc.all ) { objDoc.all[idLayer].innerHTML = objDoc.all[idLayer].innerHTML + strHtml;    return; }
    if( objDoc.layers ) { objDoc.layers[idLayer].write(strHtml); return; }
    else { var objLayer = objDoc.getElementById(idLayer); objLayer.innerHTML = objLayer.innerHTML + strHtml; }
}
function CloseWriteToLayer(objDoc, idLayer) {
    if( objDoc.layers ) objDoc.layers[idLayer].close();
}
function alertMsg(oCtrl, strCalc, strMsg) {
    alert(strMsg); SelectSsForm(strCalc); oCtrl.focus();
}
function isNumValid(strNumParam, iBorneInf, iBorneSup, bInt) {
    var bOk = false;
    strNum = new String(strNumParam);
    strNum = strNum.replace(/,/i, ".");
    var n = new Number(strNum);
    if( n.toString() == "NaN" ) bOk = false;
    else {if( bInt==true && n!=Math.floor(n) ) { return false; }
    if( !(iBorneInf == 0 && iBorneSup == 0) )
    if( iBorneInf <= n && n <= iBorneSup ) bOk = true;
    else bOk = false; else bOk = true; }
    return bOk;
}
function isDateValidJJMMAAAA(strDateParam) {
    strDate = new String(strDateParam);
    if( strDate.length != 10 ) return false;
    var bOk = false; var bBisex = false; var dd = new Number(strDate.substring(0, 2));
    var mm = new Number(strDate.substring(3, 5)); var aaaa = new Number(strDate.substring(6, 10));
    if( strDate.charAt(2) != "\/" || strDate.charAt(5) != "\/" ) bOk = false;
    else if( aaaa.toString() == "NaN" || aaaa<1800 || aaaa>2200 ) bOk = false;
    else { if( (aaaa%4==0 && aaaa%100!=0) || (aaaa%400==0 && aaaa%100==0) ) bBisex = true;
    if( mm.toString() == "NaN" || mm<1 || mm>12 )    bOk = false;
    else if( dd.toString() == "NaN" || (dd<1 || dd>31) || ((dd<1 || dd>30) && (mm==4 || mm==6 || mm==9 || mm==11)) ||
    ((dd<1 || dd>29) && mm==2 && bBisex==true) || ((dd<1 || dd>28) && mm==2 && bBisex==false) )    bOk = false;
    else bOk = true; }
    delete dd; delete mm; delete aaaa; return bOk;
}

function isURLValid(oCtrl, layerName) {
  var bOk = false;
  strURL = new String(oCtrl.value); alert("l'URL ou Mail est : "+strURL);
  if ((strURL.indexOf("@")>1)&&(strURL.lastIndexOf(".")>strURL.indexOf("@")+1)&&(strURL.lastIndexOf(".")<strURL.length-1)) {
    bOk = true;
  } else {
    if (strURL.indexOf("tp:\/\/")>=0) {
      bOk = true;
    } else {
      bOk = false;
      alertMsg(oCtrl, layerName, "URL ou Mail invalide !");
    }
  }
  if (strURL.length>511) {
    bOk = false;
    alertMsg(oCtrl, layerName, "Ce champ est limit� � 512 caract�res.");
  }
  return bOk;
}

function isRadioCheck(oCtrl) {
    var nbElt = oCtrl.length;    var bRes = false;
    for(var i=0; i<nbElt; i++) bRes = bRes || oCtrl[i].checked;
    return bRes;
}
function ReplaceSpecialChar(oCtrl, type) {
  if( type=="memo" || type=="text" || type=="pj" || type=="url" ) {
        var strTexte = new String(oCtrl.value);
        var strNew = "";
        for (j=0; j<strTexte.length; j++) {
        if( strTexte.charCodeAt(j) == 8217 ) strNew = strNew + "'";
      else if( strTexte.charCodeAt(j) > 255 ) strNew = strNew + " ";
      else strNew = strNew + strTexte.charAt(j);
    }
    oCtrl.value = strNew;
  }
}
function VerifCtrl(f, nbCtrl) {
    for(i=0; i<nbCtrl; i++) {
        var layerName = tabCtrl[i][0];
        var ctrlName = tabCtrl[i][1];
        var type = tabCtrl[i][2];
        var bReq = tabCtrl[i][3];
        var oCtrl = eval("f.elements['"+ctrlName+"'];");
        if( oCtrl && !(typeof(oCtrl.numCtrl)=="undefined") ) {
            ReplaceSpecialChar(oCtrl, type);
            if( bReq==true ) {
                if( type=="memo" || type=="text" || type=="date10" || type=="int" || type=="url" || type=="pj") {
                    if( oCtrl.value=="" )    { alertMsg(oCtrl, layerName, "Ce champ est obligatoire."); return false; }
                } else
                    if( type=="radio" ) {
                        if( isRadioCheck(oCtrl) == false )
                          alertMsg(oCtrl, layerName, "Les choix Oui / Non sont obligatoires."); return false; }
            } 
            // v�rifie le bon format pour une information existante
            switch( type ) {
                case "memo" : if( oCtrl.value.length>3999 ) { alertMsg(oCtrl, layerName, "Ce champ est limit� � 4000 caract�res."); return false; } break;
                case "int" : if( !isNumValid(oCtrl.value, 0, 0, true) ) { alertMsg(oCtrl, layerName, "Ce champ n�cessite un nombre entier."); return false; } break;
                case "date10" : if( oCtrl.value!="" && !isDateValidJJMMAAAA(oCtrl.value) ) { alertMsg(oCtrl, layerName, "Ce champ n�cessite une date au format jj/mm/aaaa."); return false; } break;
                case "url" : if( !isURLValid(oCtrl, layerName) ) { alertMsg(oCtrl, layerName, "Ce champ est limit� � 512 caract�res."); return false; } break;
                case "pj" : if( oCtrl.value.length>511 ) { alertMsg(oCtrl, layerName, "Ce champ est limit� � 512 caract�res."); return false; } break;
            }
        }
    }
    return true;
}