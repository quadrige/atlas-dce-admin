<?php
include_once("lib_fichier.php");

///<comment>
///<summary>
///Fichier contenant toutes les fonctionnalit�s
///li�es aux SIG en SVG sur les SIT
///</summary>
///</comment>

///<comment>
///<summary>
///Liste des constantes globales propres au fichier :
///
///SIG_ERREUR_AUCUNE : Pas d'erreur
///</summary>
///</comment>

$SIG_ERREUR_AUCUNE = "e0" ;
$SIG_ERREUR_EXECUTE = "e1" ;
$SIG_ERREUR_COUCHE_NOEXIST = "e2" ;
$SIG_ERREUR_FICHIER_NOEXIST = "e3" ;
$SIG_ERREUR_PROJET_NOCOUCHE = "e4" ;

///<comment>
///<summary>
/// Extrait le contenu d'un fichier SVG,
/// g�n�r� depuis notre appli d'extraction
/// MapInfo ou ArcView, dans 3 fichiers
/// distincts : 1 fichier .js qui ne contiendra
/// que les donn�es attributaire de la couche,
/// 1 fichier .frm qui contiendra les donn�es de forme,
/// et 1 fichier .style qui contiendra les styles
/// � appliquer aux formes.
///</summary>
///<params name="strFichierSource">
///Chemin d'acc�s au fichier � s�parer
///</params>
///<params name="strRep">
/// R�pertoire dans lequel se mettront les fichiers une fois g�n�r�s.
///</params>
///<params name="strNouveauNom">
/// Nom � donner aux fichiers g�n�r�s
///</params>
///<params name="idCouche">
/// identifiant de la couche cr��e.
///</params>
///<params name="conn">
/// Connection � la base de donn�es.
///</params>
///<returns>
/// Renvoie le code erreur de l'ex�cution
///</returns>
///</comment>

function ExtractCouche($strNomFichier, $strRep, $strNouveauNom, $idCouche, &$dbConn) 
{
  //V�rification si le fichier existe
  if (file_exists($strNomFichier)) {
    $bProprietes = 0 ;
    $tabLignes = file($strNomFichier) ;
    $bStyle = 0 ;
    $bEcrireJs = 0 ;
    $bEcrireSvg = 0 ;
    $bEcrireStyle = 0 ;
    $iNumFichierJs = 0 ;
    $iNumFichierSvg = 0 ;
    $iNumFichierStyle = 0 ;
    for($i=0; $i<count($tabLignes); $i++) {

      //fermeture du fichier .svg
      if (substr($tabLignes[$i],0,strlen($tabLignes[$i])-2) == "<desc>fin couche</desc>") {
        $iErreur = CloseFichier($iNumFichierSvg) ;
        $bEcrireSvg = 0 ;
      }

      //fermeture du fichier .style
      if ((substr($tabLignes[$i],0,strlen($tabLignes[$i])-2) == "]]>")&&($bStyle==1)) {
        $iErreur = CloseFichier($iNumFichierStyle) ;
        $bEcrireStyle = 0 ;
        $bStyle = 0 ;
      }

      //�criture dans le fichier .js
      if ($bEcrireJs) {
        $iErreur = EcritFichier($iNumFichierJs,$tabLignes[$i]) ;
      }

      //�criture dans le fichier .frm
      if ($bEcrireSvg) {
        if (substr($tabLignes[$i],0,strlen($tabLignes[$i])-2) == "<g id=\"lyrLayers\">" ) {
          //on ne fait rien
        } else if ((substr($tabLignes[$i],0,strlen($tabLignes[$i])-2) == "</g>") && 
                   (substr($tabLignes[$i+1],0,strlen($tabLignes[$i+1])-2) == "<desc>fin couche</desc>")) {
          // on ne fait rien
        } else {
          $iErreur = EcritFichier($iNumFichierSvg,$tabLignes[$i]) ;
        }
      }

      //�criture dans le fichier .style
      if ($bEcrireStyle) {
        $iErreur = EcritFichier($iNumFichierStyle,$tabLignes[$i]) ;
      }

      //cr�ation du fichier .js
      if (substr($tabLignes[$i],0,strlen($tabLignes[$i])-2) == "tabLayers = new Array(") {
        SupprFichier($strRep.$strNouveauNom.".js") ;
        $iNumFichierJs = OpenFichier($strRep.$strNouveauNom.".js",$GLOBALS["FICHIER_MODE_APPEND"]) ;
        $bEcrireJs = 1 ;
      }

      //fermeture du fichier .js
      if (substr($tabLignes[$i],0,strlen($tabLignes[$i])-2) == "]]") {
        $iErreur = CloseFichier($iNumFichierJs) ;
        $bEcrireJs = 0 ;
      }

      //cr�ation du fichier .frm
      if (substr($tabLignes[$i],0,strlen($tabLignes[$i])-2) == "<desc>debut couche</desc>" ) {
        SupprFichier($strRep.$strNouveauNom.".frm") ;
        $iNumFichierSvg = OpenFichier($strRep.$strNouveauNom.".frm",$GLOBALS["FICHIER_MODE_APPEND"]) ;
        $bEcrireSvg = 1 ;
      }

      //cr�ation du fichier .style
      if ((substr($tabLignes[$i],0,strlen($tabLignes[$i])-2) == "<![CDATA[")&&($bStyle==1)) {
        SupprFichier($strRep.$strNouveauNom.".style") ;
        $iNumFichierStyle = OpenFichier($strRep.$strNouveauNom.".style",$GLOBALS["FICHIER_MODE_APPEND"]) ;
        $bEcrireStyle = 1 ;
      }

      //D�tection du d�but de la parie concernant les styles
      if (substr($tabLignes[$i],0,strlen($tabLignes[$i])-2) == '<style type="text/css">' ) {
        $bStyle = 1 ;
      }

      //D�tection du tag svg
      if (substr($tabLignes[$i],0,5) == "<svg ") {
        $tabProprietes = ExtractDonneesFromLigneSvg(substr($tabLignes[$i],5,strlen($tabLignes[$i])-2)) ;
        $bProprietes = 1 ;
      }
    }
  } else {
    return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"] ;
  }
  if ($bProprietes == 1) {
    return SaveCouche($idCouche, $strNouveauNom, $strRep, $tabProprietes, $dbConn) ;
  }
  return $GLOBALS["FICHIER_ERREUR_AUCUNE"] ;
}

/**
 *
 *
 */
function SaveCouche($idCouche, $strNomFichier, $strRep, $tabProprietes, &$dbConn)
{
  //requ�te pour d�terminer s'il existe d�ja une couche associ�e � ce document.
  $strSql = "select COUCHE_ID from SIG_01_COUCHE where COUCHE_ID=".$idCouche;
  $dsExist = $dbConn->InitDataSet($strSql);

  if ( $drExist = $dsExist->getRowIter() ) {
    //si oui, on modifie la couche associ�e,
    $strSql = "update SIG_01_COUCHE " ;
    $strSql .= "set COUCHE_NOMFICHIER='".$strNomFichier."', " ;
    $strSql .= "COUCHE_REPERTOIRE='".$strRep."', " ;
    $strSql .= "COUCHE_WIDTH='".$tabProprietes[0]."', " ;
    $strSql .= "COUCHE_HEIGHT='".$tabProprietes[1]."', " ;
    $strSql .= "COUCHE_VIEWBOX='".$tabProprietes[2]."' " ;
    $strSql .= "where COUCHE_ID=".$idCouche ;
    $iErreur = $dbConn->executeSql($strSql);
    if ($iErreur<0) {
      return $GLOBALS["SIG_ERREUR_EXECUTE"] ;
    } else {
      return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
    }
  } else {
    return $GLOBALS["SIG_ERREUR_COUCHE_NOEXIST"] ;
  }
}

/**
 *
 *
 */
function ExtractDonneesFromLigneSvg($strLigne)
{
  $tabRetour = array("","","") ;
  $tabChaine = preg_split('/"/',$strLigne) ;
  for ($i=0; $i<count($tabChaine); $i++) {
    if (trim($tabChaine[$i]) == "width=") {
      $tabRetour[0] = $tabChaine[$i+1] ;
    } else if (trim($tabChaine[$i]) == "height=") {
      $tabRetour[1] = $tabChaine[$i+1] ;
    } else if (trim($tabChaine[$i]) == "viewBox=") {
      $tabRetour[2] = $tabChaine[$i+1] ;
    }
  }
  return $tabRetour ;
}

/**
 *
 *
 */
function GenereCoucheSvg($strRepCouches,$strNomCouche,$iWidth,$iHeight,$strViewBox,
                         $iEchelleMin,$iEchelleMax,$strCheminSvg,$idCouche,$tabRasters)
{
  if (file_exists($strRepCouches.$strNomCouche.".js"))
    {
      if (file_exists($strRepCouches.$strNomCouche.".frm"))
        {
          if (file_exists($strRepCouches.$strNomCouche.".style"))
            {
              SupprFichier($strCheminSvg) ;
              $iNumFichierNewSvg = OpenFichier($strCheminSvg,$GLOBALS["FICHIER_MODE_APPEND"]) ;

              $iErreur = EcritEnteteSvg($iNumFichierNewSvg,$iWidth,$iHeight,$strViewBox) ;

              $tabJavascript = array(file($strRepCouches.$strNomCouche.".js")) ;
              $tabIdCouche = array(array($idCouche,$iEchelleMin,$iEchelleMax)) ;
              $iErreur = EcritContenuJavaScript($iNumFichierNewSvg,$tabJavascript,$tabIdCouche,false) ;

              $tabStyle = array(file($strRepCouches.$strNomCouche.".style")) ;
              $iErreur = EcritContenuStyle($iNumFichierNewSvg,$tabStyle) ;

              $tabFormes = array(file($strRepCouches.$strNomCouche.".frm")) ;
              $iErreur = EcritContenuFormes($iNumFichierNewSvg,$tabFormes,$tabRasters) ;

              $iErreur = EcritFinSvg($iNumFichierNewSvg) ;

              $iErreur = CloseFichier($iNumFichierNewSvg) ;
            } else {
              return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
            }
        } else {
          return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
        }
    } else {
      return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
    }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function GetCoucheSvg($strRepCouches,$strNomCouche,$iWidth,$iHeight,$strViewBox)
{
  if (file_exists($strRepCouches.$strNomCouche.".js"))
    {
      if (file_exists($strRepCouches.$strNomCouche.".frm"))
        {
          if (file_exists($strRepCouches.$strNomCouche.".style"))
            {
              GetEnteteSvg($iWidth,$iHeight,$strViewBox) ;

              $tabJavascript = array(file($strRepCouches.$strNomCouche.".js")) ;
              GetContenuJavaScript($tabJavascript) ;

              $tabStyle = array(file($strRepCouches.$strNomCouche.".style")) ;
              GetContenuStyle($tabStyle) ;

              $tabFormes = array(file($strRepCouches.$strNomCouche.".frm")) ;
              GetContenuFormes($tabFormes) ;

              GetFinSvg($iNumFichierNewSvg) ;
            } else {
              echo "fichier ".$strRepCouches.$strNomCouche.".style inexistant" ;
            }
        } else {
          echo "fichier ".$strRepCouches.$strNomCouche.".frm inexistant" ;
        }
    } else {
      echo "fichier ".$strRepCouches.$strNomCouche.".js inexistant" ;
    }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function GenereProjetSvg($tabProjet,$strCheminSvg,$tabCouches,$tabRasters)
{
  $tabJavascript = array() ;
  $tabIdCouches = array() ;
  $tabStyle = array() ;
  $tabFormes = array() ;
  $tabListeCouches = array() ;
  $tabCouchesOK = array() ;
  for ($i=0; $i<count($tabCouches); $i++)
    {
      $tabCouche = $tabCouches[$i][0] ;
      if (file_exists($tabCouche[9].$tabCouche[8].".js"))
        {
          if (file_exists($tabCouche[9].$tabCouche[8].".frm"))
            {
              if (file_exists($tabCouche[9].$tabCouche[8].".style"))
                {
                  $tabCouchesOK[count($tabCouchesOK)] = $tabCouches[$i] ;
                  $tabListeCouches[count($tabListeCouches)] = $tabCouche ;
                  $tabJavascript[count($tabJavascript)] = file($tabCouche[9].$tabCouche[8].".js") ;
                  $tabIdCouches[count($tabIdCouches)] = $tabCouche[0] ;
                  $tabStyle[count($tabStyle)] = file($tabCouche[9].$tabCouche[8].".style") ;
                  $tabFormes[count($tabFormes)] = file($tabCouche[9].$tabCouche[8].".frm") ;
                } else {
                  return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
                }
            } else {
              return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
            }
        } else {
          return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
        }
    }
  if (count($tabJavascript)>0) //il y a donc des couches dans ce projet
    {
      SupprFichier($strCheminSvg) ;
      $iNumFichierNewSvg = OpenFichier($strCheminSvg,$GLOBALS["FICHIER_MODE_APPEND"]) ;
      $iErreur = EcritEnteteSvg($iNumFichierNewSvg,$tabProjet[0],$tabProjet[1],$tabProjet[2]) ;
      $iErreur = EcritContenuJavaScript($iNumFichierNewSvg,$tabJavascript,$tabListeCouches,true) ;
      $iErreur = EcritContenuStyle($iNumFichierNewSvg,$tabStyle) ;
      $iErreur = EcritContenuFormes($iNumFichierNewSvg,$tabFormes,$tabRasters) ;
      $iErreur = EcritJavascriptProjet($iNumFichierNewSvg,$tabProjet[4],$tabCouchesOK) ;
      $iErreur = EcritFinSvg($iNumFichierNewSvg) ;
      $iErreur = CloseFichier($iNumFichierNewSvg) ;
    } else {
      return $GLOBALS["SIG_ERREUR_PROJET_NOCOUCHE"] ;
    }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function GenereProjetSvgForPdf($tabProjet,$strCheminSvg,$tabCouches,$tabRasters)
{
  //Constantes
  
  //Variables
  $tabJavascript = array() ;
  $tabIdCouches = array() ;
  $tabStyle = array() ;
  $tabFormes = array() ;
  $tabListeCouches = array() ;
  $tabCouchesOK = array() ;
  for ($i=0; $i<count($tabCouches); $i++)
    {
      $tabCouche = $tabCouches[$i][0] ;
      if (file_exists($tabCouche[9].$tabCouche[8].".js"))
        {
          if (file_exists($tabCouche[9].$tabCouche[8].".frm"))
            {
              if (file_exists($tabCouche[9].$tabCouche[8].".style"))
                {
                  $tabCouchesOK[count($tabCouchesOK)] = $tabCouches[$i] ;
                  $tabListeCouches[count($tabListeCouches)] = $tabCouche ;
                  $tabJavascript[count($tabJavascript)] = file($tabCouche[9].$tabCouche[8].".js") ;
                  $tabIdCouches[count($tabIdCouches)] = $tabCouche[0] ;
                  $tabStyle[count($tabStyle)] = file($tabCouche[9].$tabCouche[8].".style") ;
                  $tabFormes[count($tabFormes)] = file($tabCouche[9].$tabCouche[8].".frm") ;
                } else {
                  return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
                }
            } else {
              return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
            }
        } else {
          return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
        }
    }
  if (count($tabJavascript)>0) //il y a donc des couches dans ce projet
    {
      SupprFichier($strCheminSvg) ;
      $iNumFichierNewSvg = OpenFichier($strCheminSvg,$GLOBALS["FICHIER_MODE_APPEND"]) ;
      $iErreur = EcritEnteteSvgForPdf($iNumFichierNewSvg,$tabProjet[0],$tabProjet[1],$tabProjet[2]) ;
      $iErreur = EcritContenuFormesForPdf($iNumFichierNewSvg,$tabFormes,$tabStyle,$tabJavaScript,$tabCouchesOK,$tabRasters) ;
      $iErreur = EcritFinSvg($iNumFichierNewSvg) ;
      $iErreur = CloseFichier($iNumFichierNewSvg) ;
    } else {
      return $GLOBALS["SIG_ERREUR_PROJET_NOCOUCHE"] ;
    }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function GenereProjetSvgForPdf2($tabProjet,$strCheminSvg,$tabCouches,$tabRasters)
{
  $tabCouchesOK = array() ;
  for ($i=0; $i<count($tabCouches); $i++)
    {
      $tabCouche = $tabCouches[$i][0] ;
      if (file_exists($tabCouche[9].$tabCouche[8].".js"))
        {
          if (file_exists($tabCouche[9].$tabCouche[8].".frm"))
            {
              if (file_exists($tabCouche[9].$tabCouche[8].".style"))
                {
                  $tabCouchesOK[count($tabCouchesOK)] = $tabCouches[$i] ;
                } else {
                  return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
                }
            } else {
              return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
            }
        } else {
          return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
        }
    }
  if (count($tabCouchesOK)>0) //il y a donc des couches dans ce projet
    {
      SupprFichier($strCheminSvg) ;
      $iNumFichierNewSvg = OpenFichier($strCheminSvg,$GLOBALS["FICHIER_MODE_APPEND"]) ;
      $iErreur = EcritEnteteSvgForPdf($iNumFichierNewSvg,$tabProjet[0],$tabProjet[1],$tabProjet[2]) ;
      $iErreur = EcritContenuFormesForPdf2($iNumFichierNewSvg,$tabCouchesOK,$tabRasters) ;
      $iErreur = EcritFinSvg($iNumFichierNewSvg) ;
      $iErreur = CloseFichier($iNumFichierNewSvg) ;
    } else {
      return $GLOBALS["SIG_ERREUR_PROJET_NOCOUCHE"] ;
    }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

///<comment>
///<summary>
/// A partir d'une liste d'identifiants de documents pass�s en param�tre,
/// cette fonction recherche les rasters qui seraient �ventuellement associ�s, 
/// recalcule � partir de chaque nom de rasters le nom d'origine, 
/// et renvoi un tableau contenant l'ancien nom et le nouveau nom tel qu'il est 
/// enregistr� dans la base.
///</summary>
///<params name="tabIdDocs">Liste des identifiants des documents � �tudier</params>
///<params name="conn">Connection � la base de donn�es</params>
///<returns>
/// Renvoi un tableau contenant l'ancien nom et le nouveau nom de chaque raster trouv�
///</returns>
///</comment>
function GetTabRasters($tabIdDocs, $strPathRasters, &$dbConn)
{
  $tabRetour = array() ;
  if( count($tabIdDocs) > 0 ) {
    $strListeIdDocs = join(",", $tabIdDocs) ;
    $strSQL = "select pj_nom_fdoc_pj " ;
    $strSQL .= "from fdoc_01_pj " ;
    $strSQL .= "where pj_type_fdoc_pj = 4 " ;
    $strSQL .= "and doc_id in (".$strListeIdDocs.") " ;
    $dsRasters = $dbConn->InitDataset($strSQL);
    while( $drRasters = $dsRasters->getRowIter() ) {
      $strNomRaster = $drRasters->getValueName("PJ_NOM_FDOC_PJ") ;

      //Calcul de l'ancien nom
      $tabNom = preg_split("/_/", $strNomRaster) ;
      $iDebut = strlen($tabNom[0])+1+strlen($tabNom[1])+1 ;
      $tabRetour[count($tabRetour)] = array() ;
      $tabRetour[count($tabRetour)-1][0] = substr($strNomRaster,$iDebut,strlen($strNomRaster)-$iDebut) ;
      $tabRetour[count($tabRetour)-1][1] = $strPathRasters.$strNomRaster ;
    }
  }
  
  return $tabRetour ;
}

/**
 *
 *
 */
function EcritEnteteSvg($iNumFichier, $iWidth, $iHeight, $strViewBox)
{
  $strNoViewSource = "".
    "<defs>".
    "<menu id=\"CustomMenu\">".
    "<header>Carte en SVG</header>".
    "<item action=\"Help\">Aide...</item>".
    "<item action=\"About\">A propos de SVG Viewer...</item>".
    "</menu>".
    "</defs>".
    "<script>".
    "<![CDATA[".
    "var newMenuRoot = parseXML( printNode( document.getElementById( 'CustomMenu' ) ), contextMenu );".
    "contextMenu.replaceChild( newMenuRoot, contextMenu.firstChild );".
    "]]>".
    "</script>" ;

  if ($iNumFichier) {
    $iErreur = EcritFichier($iNumFichier,"<?xml version='1.0' encoding='iso-8859-1'?>") ;
    $iErreur = EcritFichier($iNumFichier,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\" >") ;
    $iErreur = EcritFichier($iNumFichier,"<svg width='$iWidth' height='$iHeight' viewBox='$strViewBox' zoomAndPan='disable' onload='LoadThisSvg(evt)'>") ;
    //$iErreur = EcritFichier($iNumFichier,$strNoViewSource) ;
    $iErreur = EcritFichier($iNumFichier,"<script><![CDATA[") ;
    $iErreur = EcritFichier($iNumFichier,"var bIsLoaded=0;") ;
    $iErreur = EcritFichier($iNumFichier,"function LoadThisSvg(evt) { bIsLoaded=1; }") ;
    $iErreur = EcritFichier($iNumFichier,"function IsLoaded() { return bIsLoaded==1; }") ;
    $iErreur = EcritFichier($iNumFichier,"]]></script>") ;
  }

  //  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

/**
 *
 *
 */
function EcritEnteteSvgForPdf($iNumFichier, $iWidth, $iHeight, $strViewBox)
{
  if ($iNumFichier) {
    $iErreur = EcritFichier($iNumFichier,"<?xml version='1.0' encoding='iso-8859-1'?>") ;
    $iErreur = EcritFichier($iNumFichier,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\" >") ;
    $iErreur = EcritFichier($iNumFichier,"<svg width='$iWidth' height='$iHeight' viewBox='$strViewBox'>") ;
  }

  //  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

/**
 *
 *
 */
function GetEnteteSvg($iWidth, $iHeight, $strViewBox)
{
  $strNoViewSource = "".
    "<defs>".
    "<menu id=\"CustomMenu\">".
    "<header>Carte en SVG</header>".
    "<item action=\"Help\">Aide...</item>".
    "<item action=\"About\">A propos de SVG Viewer...</item>".
    "</menu>".
    "</defs>".
    "<script>".
    "<![CDATA[".
    "var newMenuRoot = parseXML( printNode( document.getElementById( 'CustomMenu' ) ), contextMenu );".
    "contextMenu.replaceChild( newMenuRoot, contextMenu.firstChild );".
    "]]>".
    "</script>" ;

  echo "<?xml version='1.0' encoding='iso-8859-1'?>" ;
  echo "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\" >" ;
  echo "<svg xmlns:xlink=\"http://www.w3.org/1999/xlink\" width='$iWidth' height='$iHeight' viewBox='$strViewBox' zoomAndPan='disable' onload='LoadThisSvg(evt)'>" ;
  //$iErreur = EcritFichier($iNumFichier,$strNoViewSource) ;
  echo "<script><![CDATA[" ;
  echo "var bIsLoaded=0;" ;
  echo "function LoadThisSvg(evt) { bIsLoaded=1; }" ;
  echo "function IsLoaded() { return bIsLoaded==1; }" ;
  echo "]]></script>" ;

  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

/**
 *
 *
 */
function EcritContenuJavaScript($iNumFichier, $tableau, $tabCouches, $bProjet)
{
  if ($iNumFichier) {
    $iErreur = EcritFichier($iNumFichier,"<script><![CDATA[") ;
    $iErreur = EcritFichier($iNumFichier,"tabLayers = new Array(") ;
    for ($i=0; $i<count($tableau); $i++) {
      if ($i>0) {
        $iErreur = EcritFichier($iNumFichier,",") ;
      }
      $n = count($tableau[$i]) ;
      for ($j=0; $j<$n; $j++) {
        if ($j==0) {
          if ($bProjet==true) {
            $tabSplit = preg_split("/,/",$tableau[$i][$j]) ;
            $tabSplit[0] = "[[\"".$tabCouches[$i][2]."\"" ; //Nom de la couche
            if ($tabCouches[$i][4]=="") {
              $tabSplit[2] = "null" ; //Echelle min
            } else {
              $tabSplit[2] = "\"".$tabCouches[$i][4]."\"" ; //Echelle min
            }
            if ($tabCouches[$i][5]=="") {
              $tabSplit[3] = "null" ; //Echelle max
            } else {
              $tabSplit[3] = "\"".$tabCouches[$i][5]."\"" ; //Echelle max
            }
            $tabSplit[4] = $tabCouches[$i][3] ; // Dans la localisation ?
            if ($tabCouches[$i][6]=="") {
              $tabSplit[5] = "null" ; // infobulles
            } else {
              $tabSplit[5] = "\"".$tabCouches[$i][6]."\"" ; // infobulles
            }
            $tabSplit[6] = $tabCouches[$i][1] ; // Visible ?
            $tabSplit[count($tabSplit)-1] = $tabCouches[$i][0]."]" ; // num�ro de couche dans Oracle
            $tableau[$i][$j] = "" ;
            for ($k=0; $k<count($tabSplit); $k++) {
              if ($k>0) {
                $tableau[$i][$j] .= "," ;
              }
              $tableau[$i][$j] .= $tabSplit[$k] ;
            }
          } else {
            $tabParamJs = preg_split("/,/",$tableau[$i][$j]) ;
            //Remplacement de l'�chelle Min
            if ($tabCouches[$i][1]!="") {
              $tabParamJs[2] = $tabCouches[$i][1] ;
            }  
            //Remplacement de l'�chelle Max
            if ($tabCouches[$i][2]!="") {
              $tabParamJs[3] = $tabCouches[$i][2] ;
            }
            //Remplacement de l'id de la couche
            //$tabParamJs[count($tabParamJs)-1] = ereg_replace(",0]",",".$tabCouches[$i][0]."]",$tabParamJs[count($tabParamJs)-1]) ;
            $tabParamJs[count($tabParamJs)-1] = $tabCouches[$i][0]."]" ;
            $tableau[$i][$j] = join(",",$tabParamJs) ;
          }
        }
        $iErreur = EcritFichier($iNumFichier,$tableau[$i][$j]) ;
      }
    }
    $iErreur = EcritFichier($iNumFichier,");]]></script>") ;
  }
  //  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

/**
 *
 *
 */
function GetContenuJavaScript($tableau)
{
  echo "<script><![CDATA[\n" ;
  echo "tabLayers = new Array(\n" ;
  for ($i=0; $i<count($tableau); $i++) {
    $n = count($tableau[$i]) ;
    for ($j=0; $j<$n; $j++) {
      echo $tableau[$i][$j] ;
    }
  }
  echo ");]]></script>\n" ;
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

/**
 *
 *
 */
function EcritContenuStyle($iNumFichier,$tableau)
{
  if ($iNumFichier) {
    $iErreur = EcritFichier($iNumFichier,"<defs>") ;
    $iErreur = EcritFichier($iNumFichier,"<style type='text/css'>") ;
    $iErreur = EcritFichier($iNumFichier,"<![CDATA[") ;
    for ($i=0; $i<count($tableau); $i++) {
      $n = count($tableau[$i]) ;
      for ($j=0; $j<$n; $j++) {
        $iErreur = EcritFichier($iNumFichier,$tableau[$i][$j]) ;
      }
    }
    $iErreur = EcritFichier($iNumFichier,"]]>") ;
    $iErreur = EcritFichier($iNumFichier,"</style>") ;
    $iErreur = EcritFichier($iNumFichier,"</defs>") ;
  }
  //  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

/**
 *
 *
 */
function GetContenuStyle($tableau)
{
  echo "<defs>" ;
  echo "<style type='text/css'>" ;
  echo "<![CDATA[" ;
  for ($i=0; $i<count($tableau); $i++) {
    $n = count($tableau[$i]) ;
    for ($j=0; $j<$n; $j++) {
      echo $tableau[$i][$j] ;
    }
  }
  echo "]]>" ;
  echo "</style>" ;
  echo "</defs>" ;
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

/**
 *
 *
 */
function EcritContenuFormes($iNumFichier,$tableau,$tabRasters)
{
  if ($iNumFichier) {
    $iErreur = EcritFichier($iNumFichier,"<g id=\"lyrLayers\">") ;
    for ($i=0; $i<count($tableau); $i++) {
      $n = count($tableau[$i]) ;
      for ($j=0; $j<$n; $j++) {
        if (count($tabRasters)>0) {
          for ($k=0; $k<count($tabRasters); $k++) {
            $tableau[$i][$j] = preg_replace($tabRasters[$k][0],$tabRasters[$k][1],$tableau[$i][$j]) ;
          }
        }
        $iErreur = EcritFichier($iNumFichier,$tableau[$i][$j]) ;
      }
    }
    $iErreur = EcritFichier($iNumFichier,"</g>") ;
  }
  //return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

/**
 *
 *
 */
function EcritContenuFormesForPdf($iNumFichier, $tableau, $tabRasters, $tabStyles)
{
  if ($iNumFichier) {
    $iErreur = EcritFichier($iNumFichier,"<g id=\"lyrLayers\">") ;
    for ($i=0; $i<count($tableau); $i++) {
      $n = count($tableau[$i]) ;
      for ($j=0; $j<$n; $j++) {
        if (count($tabRasters)>0) {
          for ($k=0; $k<count($tabRasters); $k++) {
            $tableau[$i][$j] = preg_replace("/".$tabRasters[$k][0]."/",$tabRasters[$k][1],$tableau[$i][$j]) ;
          }
        }
        $strNomClasse = GetClassForme($tableau[$i][$j]) ;
        if ($strNomClasse!="") {
          $tableau[$i][$j] = preg_replace("/class=\"".$strNomClasse."\"/","style=\"".GetStyleClass($strNomClasse, $tabStyles)."\"",$tableau[$i][$j]) ;
        }
        $iErreur = EcritFichier($iNumFichier,$tableau[$i][$j]) ;
      }
    }
    $iErreur = EcritFichier($iNumFichier,"</g>") ;
  }
  //return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

/**
 *
 *
 */
function EcritContenuFormesForPdf2($iNumFichier, $tabCouches, $tabRasters)
{
  if ($iNumFichier) {
    $iErreur = EcritFichier($iNumFichier,"<g id=\"lyrLayers\">") ;
    for ($i=0; $i<count($tabCouches); $i++) {
      ExtractFormesForPdf($iNumFichier,$tabCouches[$i],$tabRaster) ;
    }
    $iErreur = EcritFichier($iNumFichier,"</g>") ;
  }
  //return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

/**
 *
 *
 */
function GetContenuFormes($tableau)
{
  for ($i=0; $i<count($tableau); $i++) {
    $n = count($tableau[$i]) ;
    for ($j=0; $j<$n; $j++) {
      echo $tableau[$i][$j] ;
    }
  }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

/**
 *
 *
 */
function EcritJavascriptProjet($iNumFichier, $iCoucheEditable, $tabListeCouches)
{
  if ($iNumFichier) {
    $iErreur = EcritFichier($iNumFichier,"<script><![CDATA[\n") ;
    $iErreur = EcritFichier($iNumFichier,"tabProjet = new Array(\n") ;
    $iErreur = EcritFichier($iNumFichier,"[".$iCoucheEditable."],") ;
    $iErreur = EcritFichier($iNumFichier,"[\n") ;
    $iNbAnalyse = 0 ;
    for ($i=0; $i<count($tabListeCouches); $i++) {
      if(count($tabListeCouches[$i][1])>0) {
        $tabAnalyse = $tabListeCouches[$i][1] ;

        //Ecriture d'une nouvelle analyse th�matique
        if ($iNbAnalyse>0) {
          $iErreur = EcritFichier($iNumFichier,",\n") ;
        }

        //Ecriture de l'identifiant de la couche
        $strAnalyse = "[[".$i."],\n" ;
        
        //Ecriture du tableau "tabAnalyse"
        $strAnalyse .=  "[" ;
        for ($j=0; $j<count($tabAnalyse); $j++) {
          if ($j>0) {
            $strAnalyse .= "," ;
          }
          $strAnalyse .= $tabAnalyse[$j] ;
        }
        $strAnalyse .= "],\n" ;
        
        //Ecriture du tableau "tabClasse"
        $strAnalyse .= "[" ;
        if(count($tabListeCouches[$i][2])>0) {
          $tabClasses = $tabListeCouches[$i][2] ;
          for ($j=0; $j<count($tabClasses); $j++) {
            if ($j>0) {
              $strAnalyse .= "," ;
            }
            $strAnalyse .= "[" ;  
            for ($k=0; $k<count($tabClasses[$j]); $k++) {
              if ($k>0) {
                $strAnalyse .= "," ;
              }
              $strAnalyse .= $tabClasses[$j][$k] ;
            }
            $strAnalyse .= "]" ;  
          }
        }
        $strAnalyse .= "],\n" ;

        //Ecriture du tableau tabSymbol
        $strAnalyse .=  "[" ;
        if(count($tabListeCouches[$i][3])>0) {
          $tabSymbol = $tabListeCouches[$i][3] ;
          for ($j=0; $j<count($tabSymbol); $j++) {
            if ($j>0) {
              $strAnalyse .= "," ;
            }
            $strAnalyse .= $tabSymbol[$j] ;
          }
        }
        $strAnalyse .= "],\n" ;


        //Ecriture du tableau des jointures
        $strAnalyse .=  "[" ;
        if(count($tabListeCouches[$i][4])>0) {
          $tabJointures = $tabListeCouches[$i][4] ;
          for ($j=0; $j<count($tabJointures); $j++) {
            if ($j>0) {
              $strAnalyse .= "," ;
            }
            $strAnalyse .= "[".$tabJointures[$j][0].",".$tabJointures[$j][1].",".$tabJointures[$j][2].",".$tabJointures[$j][3]."]\n";
          }
        }
        $strAnalyse .= "]]\n" ;

        //Ecriture de la table 
        $iErreur = EcritFichier($iNumFichier,$strAnalyse) ;
        $iNbAnalyse += 1 ;
      }
    }
    $iErreur = EcritFichier($iNumFichier,"]") ;
    $iErreur = EcritFichier($iNumFichier,");]]></script>") ;
  }
  return $iErreur ;
}

/**
 *
 *
 *
 */
function EcritFinSvg($iNumFichier)
{
  if ($iNumFichier) {
    $iErreur = EcritFichier($iNumFichier,"</svg>") ;
  }
  //  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

/**
 *
 *
 */
function GetFinSvg()
{
  echo "</svg>" ;
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}


/**
 *
 *
 */
function EcritEnteteSvgZip($iNumFichier,$iWidth,$iHeight,$strViewBox)
{
  if ($iNumFichier) {
    $iErreur = EcritFichierZip($iNumFichier,"<?xml version='1.0' encoding='iso-8859-1'?>") ;
    $iErreur = EcritFichierZip($iNumFichier,"<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 20000303 Stylable//EN' 'http://www.w3.org/TR/2000/03/WD-SVG-20000303/DTD/svg-20000303-stylable.dtd'>") ;
    $iErreur = EcritFichierZip($iNumFichier,"<svg width='$iWidth' height='$iHeight' viewBox='$strViewBox' onload='LoadThisSvg(evt)'>") ;
    $iErreur = EcritFichierZip($iNumFichier,"<script><![CDATA[") ;
    $iErreur = EcritFichierZip($iNumFichier,"var bIsLoaded=0;") ;
    $iErreur = EcritFichierZip($iNumFichier,"function LoadThisSvg(evt) { bIsLoaded=1; }") ;
    $iErreur = EcritFichierZip($iNumFichier,"function IsLoaded() { return bIsLoaded==1; }") ;
    $iErreur = EcritFichierZip($iNumFichier,"]]></script>") ;
  }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

/**
 *
 *
 */
function EcritContenuJavaScriptZip($iNumFichier,$tableau)
{
  if ($iNumFichier) {
    $iErreur = EcritFichierZip($iNumFichier,"<script><![CDATA[") ;
    $iErreur = EcritFichierZip($iNumFichier,"tabLayers = new Array(") ;
    for ($i=0; $i<count($tableau); $i++) {
      $n = count($tableau[$i]) ;
      for ($j=0; $j<$n; $j++) {
        $iErreur = EcritFichierZip($iNumFichier,$tableau[$i][$j]) ;
      }
    }
    $iErreur = EcritFichierZip($iNumFichier,");]]></script>") ;
  }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

/**
 *
 *
 *
 */
function EcritContenuStyleZip($iNumFichier,$tableau)
{
  if ($iNumFichier) {
    $iErreur = EcritFichierZip($iNumFichier,"<defs>") ;
    $iErreur = EcritFichierZip($iNumFichier,"<style type='text/css'>") ;
    $iErreur = EcritFichierZip($iNumFichier,"<![CDATA[") ;
    for ($i=0; $i<count($tableau); $i++) {
      $n = count($tableau[$i]) ;
      for ($j=0; $j<$n; $j++) {
        $iErreur = EcritFichierZip($iNumFichier,$tableau[$i][$j]) ;
      }
    }
    $iErreur = EcritFichierZip($iNumFichier,"]]>") ;
    $iErreur = EcritFichierZip($iNumFichier,"</style>") ;
    $iErreur = EcritFichierZip($iNumFichier,"</defs>") ;
  }

  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

/**
 *
 *
 */
function EcritContenuFormesZip($iNumFichier,$tableau)
{
  if ($iNumFichier) {
    for ($i=0; $i<count($tableau); $i++) {
      $n = count($tableau[$i]) ;
      for ($j=0; $j<$n; $j++) {
        $iErreur = EcritFichierZip($iNumFichier,$tableau[$i][$j]) ;
      }
    }
  }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

/**
 *
 *
 */
function EcritFinSvgZip($iNumFichier)
{
  if ($iNumFichier) {
    $iErreur = EcritFichierZip($iNumFichier,"</svg>") ;
  }
}

/**
 *
 *
 */
function SaveProjet()
{

}

/**
 *
 *
 */
function EcraseFichier($strNomFichier, $tabLignes)
{
  //Suppression du fichier d'origine
  SupprFichier($strNomFichier) ;

  //Cr�ation du fichier
  $iNumFichier = OpenFichier($strNomFichier,$GLOBALS["FICHIER_MODE_APPEND"]) ;

  //Remplissage du fichier
  for($i=0; $i<count($tabLignes); $i++) {
    //�criture dans le fichier
    $iErreur = EcritFichier($iNumFichier,$tabLignes[$i]) ;
  }

  //Fermeture du fichier
  $iErreur = CloseFichier($iNumFichier) ;
}

/**
 *
 *
 */
function TraiteFichierSVGLocal($strFichierSvg, $strCheminFichier)
{
  $iLigneImage = 17 ;
  if ( $strFichierSvg != "" ) {
    $strFichierSvg = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_SIG.$strFichierSvg ;
    if (file_exists($strFichierSvg)) {
      //R�cup�ration des lignes du fichier
      $tabLignes = file($strFichierSvg) ;
          
      //R�cup�ration de la ligne contenant la d�finition de l'image
      $strLigneImage = $tabLignes[$iLigneImage] ;
          
      //Si cette ligne commence par image, alors le fichier est correct
      if (substr($strLigneImage,0,6) == "<image") {
        //Recherche du r�pertoire local
        $strCheminTemp = preg_replace("/\//","\\",$strCheminFichier) ;
        $iLastSlash = strrpos($strCheminTemp,"\\") ;
        $strCheminTemp = substr($strCheminTemp,0,$iLastSlash+1) ;

        //Ajout du r�pertoire local au nom de l'image
        $tabLignes[$iLigneImage] = preg_replace("/xlink:href=\"/","xlink:href=\"".$strCheminTemp,$tabLignes[$iLigneImage]) ;

        //On �crase le fichier
        EcraseFichier($strFichierSvg, $tabLignes) ;
        return true ;
      } else {
        return false ;
      }
    } else {
      return false ;
    }
  } else {
    return false ;
  }
}

?>