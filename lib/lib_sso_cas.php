<?php

/**
 * @brief Authentification SSO CAS
 *
 * @param strService Url local � appeler pour recevoir le ticket
 * @return Retourne un string si succ�s : nom du login
 *         Retourne false sinon
 */
function authenticateCAS($strService)
{
  // r�cup�ration du ticket (retour du serveur CAS)
  if( !isset($_GET['ticket']) ) {
    // pas de ticket : on redirige le navigateur web vers le serveur CAS
    header('Location: '.ALK_SSO_URL.'/login?service='.$strService);
    exit() ;
  }
  
  $strTicket = $_GET['ticket'];
  
  // un ticket est transmis, on essaie de le valider aupr�s du serveur CAS
  $fpage = fopen (ALK_SSO_URL.'/serviceValidate?service='.
    preg_replace('/&/','%26',$strService).'&ticket='.$strTicket,'r');
  if( $fpage ) {
    $strPage = "";
    $tabLoginMatch = array();
    while( !feof($fpage) ) {
      $strPage .= fgets ($fpage, 1024);
    }

    // analyse de la r�ponse du serveur CAS
    if( preg_match('|<cas:authenticationSuccess>.*</cas:authenticationSuccess>|mis', $strPage) ) {
      if( preg_match('|<cas:user>(.*)</cas:user>|', $strPage, $tabLoginMatch) ) {
        return $tabLoginMatch[1];
      }
    }
  }
  
  // probl�me de validation
  return false;
}

?>