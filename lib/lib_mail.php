<?
/*
{br} represent the HTML-tag for line break and should be replaced,
but I did not know how to not get the original tag  parsed here.

function SendMail($From, $FromName, $To, $ToName, $Subject, $Text, $Html, $AttmFiles)
$From      ... sender mail address like "my@address.com"
$FromName  ... sender name like "My Name"
$To        ... recipient mail address like "your@address.com"
$ToName    ... recipients name like "Your Name"
$Subject   ... subject of the mail like "This is my first testmail"
$Text      ... text version of the mail
$Html      ... html version of the mail
$AttmFiles ... array containing the filenames to attach like array("file1","file2")
*/

function SendMail($From,$FromName,$To,$ToName,$Subject,$Text,$Html,$AttmFiles){
$OB="----=_OuterBoundary_000";
$IB="----=_InnerBoundery_001";
$Html=$Html?$Html:preg_replace("/\n/","<br>",$Text)
 or die("version texte OU HTML non pr�sente.");
$Text=$Text?$Text:"Vous devez poss�dez un lecteur de courrier permettant d'interpr�ter le format HTML pour lire ce mail.";
$From or die("Exp�diteur manquant");
$To or die("Destinataire manquant");

$headers ="MIME-Version: 1.0\r\n";
$headers.="From: ".$FromName." <".$From.">\n";
$headers.="To: ".$ToName." <".$To.">\n";
$headers.="Reply-To: ".$FromName." <".$From.">\n";
$headers.="X-Priority: 1\n";
$headers.="X-MSMail-Priority: High\n";
$headers.="X-Mailer: My PHP Mailer\n";
$headers.="Content-Type: multipart/mixed;\n\tboundary=\"".$OB."\"\n";

//Messages start with text/html alternatives in OB
$Msg ="This is a multi-part message in MIME format.\n";
$Msg.="\n--".$OB."\n";
$Msg.="Content-Type: multipart/alternative;\n\tboundary=\"".$IB."\"\n\n";

//plaintext section
$Msg.="\n--".$IB."\n";
$Msg.="Content-Type: text/plain;\n\tcharset=\"iso-8859-1\"\n";
$Msg.="Content-Transfer-Encoding: quoted-printable\n\n";
// plaintext goes here
$Msg.=$Text."\n\n";

// html section
$Msg.="\n--".$IB."\n";
$Msg.="Content-Type: text/html;\n\tcharset=\"iso-8859-1\"\n";
$Msg.="Content-Transfer-Encoding: base64\n\n";
// html goes here
$Msg.=chunk_split(base64_encode($Html))."\n\n";

// end of IB
$Msg.="\n--".$IB."--\n";

// attachments
if($AttmFiles)
{
	foreach($AttmFiles as $AttmFile)
 	{
 		$patharray = explode ("/", $AttmFile);
  	$FileName=$patharray[count($patharray)-1];
  	$Msg.= "\n--".$OB."\n";
  	$Msg.="Content-Type: application/octetstream;\n\tname=\"".$FileName."\"\n";
 		$Msg.="Content-Transfer-Encoding: base64\n";
  	$Msg.="Content-Disposition: attachment;\n\tfilename=\"".$FileName."\"\n\n";

 		//file goes here
  	$fd=fopen ($AttmFile, "r");
  	$FileContent=fread($fd,filesize($AttmFile));
  	fclose ($fd);
  	$FileContent=chunk_split(base64_encode($FileContent));
  	$Msg.=$FileContent;
  	$Msg.="\n\n";
 	}
}

//message ends
$Msg.="\n--".$OB."--\n";
//echo $To."<br>";
//echo $Subject."<br>";
//echo $Msg."<br>";
//echo $headers."<br><br>";

 if( !isset($_SESSION["mode_debug"]) )
	 mail($To,$Subject,$Msg,$headers);

//syslog(LOG_INFO,"Mail: Message sent to $ToName <$To>");
}

/**
 * @brief Ajoute une piece jointe au mail
 *
 * @param boundary Cl� du mail - chaine alphanum.
 * @param filename Nom du fichier qui apparait dans le mail
 * @param file     Chemin complet du fichier � envoyer
 */
function addFileMail($boundary, $filename, $file)
{
	$msg = "";
	if( $file!="" )	{
		$fileSys = $file;
		$fp = fopen($fileSys, "r");
		$attachment = fread($fp, filesize($fileSys));
		$attachment = chunk_split(base64_encode($attachment));
		fclose($fp);
		
		$msg = "--".$boundary."\n";
		$msg .= "Content-Type: application/octet-stream; name=\"".$fileSys."\"\n";
		$msg .= "Content-Transfer-Encoding: base64\n";
		$msg .= "Content-Disposition: inline; filename=\"".$filename."\"\n";
		$msg .= "\n";
		$msg .= $attachment . "\n";
		$msg .= "\n\n";
	}
	return $msg;
}

/**
 * @brief formate un mail avec piece jointe puis l'envoie
 *
 * @param strTitre       Titre du message
 * @param strCorps       Corps du message
 * @param strNomTo       Nom du destinataire
 * @param strMailTo      Adresse du destinataire
 * @param strNomFrom     Nnom de l'expediteur
 * @param strMailFrom    Adresse de l'expediteur
 * @param fileName       Nom du fichier qui apparait sur le mail (ou tableau de nom de fichier si plusieurs)
 * @param file	         Chemin complet du fichier � envoyer (ou tableau si plusieurs)
 * @param strNomReplyTo  Param option, nom de la personne � qui r�pondre, par d�faut le destinataire
 * @param strMailReplyTo Param option, mail de la personne � qui r�pondre, par d�faut celui du destinataire
 */
function SendMailFileTo($strTitre, $strCorps,
                        $strNomTo, $strMailTo, 
                        $strNomFrom, $strMailFrom, 
                        $fileName, $file,
                        $strNomReplyTo="", $strMailReplyTo="")
{
	$boundary = "-----=".md5(uniqid (rand()));
	$header  = "MIME-Version: 1.0\n";
	$header .= "From: ".$strNomFrom." <".$strMailFrom.">\n";
	$header .= "To: ".$strNomTo." <".$strMailTo.">\n";
	$header .= "Reply-To: ".($strNomReplyTo!="" ? $strNomReplyTo : $strNomFrom).
    " <".($strMailReplyTo!="" ? $strMailReplyTo : $strMailFrom).">\n";
	$header .= "Content-Type: multipart/mixed; boundary=\"".$boundary."\"\n";
	$header .= "\n";

	$msg = "Je vous informe que ceci est un message au format MIME 1.0 multipart/mixed.\n";
	$msg .= "--".$boundary."\n";
	$msg .= "Content-Type: text/plain; charset=\"iso-8859-1\"\n";
	$msg .= "Content-Transfer-Encoding:8bit\n";
	$msg .= "\n";
	$msg .= $strCorps."\n";
	$msg .= "\n";

	if( is_array($file) && is_array($fileName) && count($file) == count($fileName) ) {
		for($i=0; $i<count($file); $i++) {
			$msg .= addFileMail($boundary, $fileName[$i], $file[$i]);
		}
	} elseif ( $file != "" && $fileName != "" )	{
		$msg .= addFileMail($boundary, $fileName, $file);
	}

	$msg .= "--".$boundary."--\n";


	if( !isset($_SESSION["mode_debug"]) )
		mail($strMailTo, $strTitre, $msg, $header);
}

?>
