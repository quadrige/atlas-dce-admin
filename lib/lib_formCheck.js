
function AlertMessageCheck(Ctrl, strMsg)
{
  alert(strMsg);
  return;
}

function isCheckboxRequire(oForm, oCtrl) {
    var nbElt = oCtrl.value;
    var bRes = false;
    for(var i=0; i<nbElt; i++) {
      var oCB = oForm.elements[oCtrl.name+"_"+i.toString()];
      bRes = bRes || oCB.checked;
    }
    return bRes;
}

function verifNbCheck(oForm, oCtrl, oMin, oMax)
{
  var nbElt = oCtrl.value;
  var nbCheck = 0;
  for(var i=0; i<nbElt; i++) {
    var oCB = oForm.elements[oCtrl.name+"_"+i.toString()];
    if (oCB.checked)
      nbCheck++;
  }
    
  reason = "";
 
  if( oMin != "" && nbCheck < oMin ) {
    reason = " n�cessite "+( oMin==oMax
                             ? "que "+oMin
                             : "qu'au moins "+oMin)+" case"+(oMin>1 ? "s" : "")+" soi"+(oMin>1 ? "ent" : "t")+" coch�e"+(oMin>1 ? "s" : "");
    return reason;
  }
  
  if( oMax != "" && nbCheck > oMax ) {
    reason = " n�cessite "+( oMin==oMax
                             ? "que "+oMax
                             : "qu'au plus "+oMax)+" case"+(oMax>1 ? "s" : "")+" soi"+(oMax>1 ? "ent" : "t")+" coch�e"+(oMax>1 ? "s" : "");
    return reason;    
  }

  return reason;
}

