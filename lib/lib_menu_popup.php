<?php
include_once("../../libconf/lib_affmenu.php");

/**
 * @brief Retourne l'ent�te html de la page
 *
 * @param tabEventBody  tableau contenant les couples nom de l'�v�nement / nom de la fonction 
 *                      javascript appel�e (pas n�cessaire d'ajouter 'javascript:') 
 */
function aff_menu_haut($tabEventBody=null)
{
  echo getHtmlHeader($tabEventBody);
}

/**
 * @brief Affiche le code html du bas de la page
 */
function aff_menu_bas()
{
  echo getHtmlFooter();
}

/**
 * @brief Retourne le code html du haut de la page popup
 *
 * @param tabEventBody  tableau contenant les couples nom de l'�v�nement / nom de la fonction 
 *                      javascript appel�e (pas n�cessaire d'ajouter 'javascript:') 
 * @return Retourne un string
 */
function getHtmlHeader($tabEventBody=null)
{
  global $oAppli, $cont_id, $cont_appli_id;

  $strBodyEvent = "";

  if( is_array($tabEventBody) ) {
    while( list($strEvent, $strFunctionJs) = each($tabEventBody) )
      $strBodyEvent .= " ".$strEvent."='".htmlentities($strFunctionJs)."'";
  } else {
    $strBodyEvent = " onload='onLoadMainInit(); onLoadInit()'";	
  }

  // affiche l'entete html
  $strHtml = getHtmlMainHeader().

    "<script language='javascript' src='../../lib/lib_js.js'></script>".
    "<script language='JavaScript'>".
    " navigateur = null;".
    " x = null;".
    " y = null;".
    " bWinLoaded = 0;".
    " menuBar = null;".
    " ALK_SIALKE_URL = '".ALK_SIALKE_URL."';".
    " ALK_URL_ASPIC = '".( defined("ALK_URL_ASPIC") ? ALK_URL_ASPIC : "")."';".
    " ALK_URL_ASPIC_V2 = '".( defined("ALK_URL_ASPIC_V2") ? ALK_URL_ASPIC_V2 : "")."';".
    " ALK_DIR_ANNU = '".ALK_DIR_ANNU."';".
    " ALK_DIR_ESPACE = '".ALK_DIR_ESPACE."';".
    " ALK_URL_IDENTIFICATION = '".ALK_URL_IDENTIFICATION."';".
    " ALK_ROOT_URL = '".ALK_ROOT_URL."';".
    (defined("ALK_URL_SERVEUR_DIST") ? "var ALK_URL_SERVEUR_DIST = '".ALK_URL_SERVEUR_DIST."';" : "") .
    " strTitrePage = '';".
    " strTitreAliasPage = '';".
    " cont_id = '".$cont_id."';".
    " cont_appli_id = '".$cont_appli_id."';".
    " strDefaultTarget= '';".
    
    /** Ajouter constantes javascript sp�cifiques pour l'appli + fonctions appel�es sur onload si besoin */
    ( is_object($oAppli) && method_exists($oAppli, "getHtmlJsInit") 
      ? $oAppli->getHtmlJsInit() 
      : ( function_exists("getHtmlJsInit") ? getHtmlJsInit() : "" )).
    
    " function onLoadInit() {".
    "   bWinLoaded = 1; document.onmousemove = position;".
    /** Actions sp�cifiques � l'appli sur body onload */
    ( is_object($oAppli) && method_exists($oAppli, "getHtmlJsOnLoad") 
      ? $oAppli->getHtmlJsOnLoad() 
      : ( function_exists("getHtmlJsOnLoad") ? getHtmlJsOnLoad() : "" )).
    " window.focus();".
    " } ".
    
    "</script>".
    "<script language='javascript' src='../../lib/lib_jsaddon.js'></script>".

    getHtmlMainPopupJs().

    /** inclusioon des librairies js sp�cifiques � l'appli */
    ( is_object($oAppli) && method_exists($oAppli, "getHtmlJsFileSource") 
      ? $oAppli->getHtmlJsFileSource() 
      : (function_exists("getHtmlJsFileSource") ? getHtmlJsFileSource() : "" )).

    "</head>".

    "<body bgcolor='#FFFFFF' text='#000000' ".
    " leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'".$strBodyEvent.">";

  return $strHtml;
}

/**
 * @brief Rtourne le code html du bas de la page
 *
 * @return Retourne un string
 */
function getHtmlFooter()
{
  // fin du contenu
  // fermeture de la charte
  $strHtml = getHtmlMainPopupFooter();

  return $strHtml;
}


//
// les fonctions ci-dessous sont appel�es � disparaitre
//

/** Affiche le r�sultat de la fonction getHtmlHeaderPage (lib_affmenu.php) */
function aff_page_haut($bModif, $strTitre, $iLarg="606")
{
  echo getHtmlHeaderPage($bModif, $strTitre, $iLarg);
}

/** Affiche le r�sultat de la fonction getHtmlLineFrame (lib_affmenu.php) */
function Aff_page_cadre_separateur($iLarg="606")
{
  echo getHtmlLineFrame($iLarg);
}

/** Affiche le r�sultat de la fonction getHtmlHeaderFrame (lib_affmenu.php) */
function Aff_page_cadre_haut($iLarg="606", $ihaut="5")
{
  echo getHtmlHeaderFrame($iLarg, $ihaut);
}

/** Affiche le r�sultat de la fonction getHtmlFooterFrame (lib_affmenu.php) */
function Aff_page_cadre_bas($ihaut="5")
{
  echo getHtmlFooterFrame($ihaut);
}

/** Affiche le r�sultat de la fonction getHtmlFooterPage (lib_affmenu.php) */
function aff_page_bas()
{
  echo getHtmlFooterPage();
}

?>