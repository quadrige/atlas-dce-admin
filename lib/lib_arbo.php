<?
///<comment>
///<summary>
/// retourne le tableau de noeud necessaire a l'affichage d'un arbre
///</summary>
///<params name="rsNoeud">
/// identifiant du recordSet contenant les infos sur les noeuds de l'arbre
/// Alias necessaires : ID, ID_PERE, NOM, DROIT (=1 : lecture, 2=ecriture), TMP
/// Ordonnees par niveau, puis par rang, puis par nom
///</params>
///<params name="rsFeuille" option>
/// identifiant du recordSet contenant les infos sur les feuilles de l'arbre
/// Alias necessaires : ID, ID_NOEUD, NOM, DROIT (=1 : lecture, 2=ecriture), TMP
/// Ordonnees par nom
///</params>
///<returns>Retourne le tableau de noeuds</returns>
///</comment>
function ConsTabNoeud($rsNoeud)
{
	$tabNoeud = array();   // contient l'ensemble des noeuds
	$tabIndex = array();   // contient l'index inverse id -> cpt

	$cpt = 0;
	while( ociFetch($rsNoeud) )
		{
			$idFils = ociResult($rsNoeud, "ID");
			$idPere = ociResult($rsNoeud, "ID_PERE");
			$strNom = ociResult($rsNoeud, "NOM");
			$iDroit = ociResult($rsNoeud, "DROIT");
			$strTmp = ociResult($rsNoeud, "TMP");

			$tabNoeud[$cpt]["FILS"] = array();
			$tabNoeud[$cpt]["FEUILLE"] = array();
			$tabNoeud[$cpt]["ID"] = $idFils;
			$tabNoeud[$cpt]["NOM"] = $strNom;
			$tabNoeud[$cpt]["TYPE"] = 0;
			$tabNoeud[$cpt]["DROIT"] = $iDroit;
			$tabNoeud[$cpt]["TMP"] = $strTmp;

			// met a jour la table index secondaire
			$tabIndex[$idFils] = $cpt;

			// ajoute l'id fils a l'id pere
			if( $idPere > 0 )
				if( array_key_exists($idPere, $tabIndex) == true )
					{
						$cptPere = $tabIndex[$idPere];
						array_push($tabNoeud[$cptPere]["FILS"], $cpt);
						$tabNoeud[$cpt]["PERE"] = $cptPere;
					}
			$cpt++;
		}

	$numArgs = func_num_args();
	if( $numArgs == 1 )
		return $tabNoeud;

	$rsFeuille = func_get_arg(1);

	while( ociFetch($rsFeuille) )
		{
			$id = ociResult($rsFeuille, "ID");
			$idNoeud = ociResult($rsFeuille, "ID_NOEUD");
			$strNom = ociResult($rsFeuille, "NOM");
			$iDroit = ociResult($rsFeuille, "DROIT");
			$strTmp = ociResult($rsFeuille, "TMP");

			$tabNoeud[$cpt]["FILS"] = array();
			$tabNoeud[$cpt]["FEUILLE"] = array();
			$tabNoeud[$cpt]["ID"] = $id;
			$tabNoeud[$cpt]["NOM"] = $strNom;
			$tabNoeud[$cpt]["TYPE"] = 1;
			$tabNoeud[$cpt]["DROIT"] = $iDroit;
			$tabNoeud[$cpt]["TMP"] = $strTmp;

			// ajoute l'id feuille a l'id noeud
			if( $idNoeud > 0 )
				if( array_key_exists($idNoeud, $tabIndex) == true )
					{
						$cptNoeud = $tabIndex[$idNoeud];
						array_push($tabNoeud[$cptNoeud]["FEUILLE"], $cpt);
						$tabNoeud[$cpt]["PERE"] = $cptNoeud;
					}
			
			$cpt++;
		}

	return $tabNoeud;
}


///<comment>
///<summary>
/// Construit puis retourne le code html representant une arborescence
///</summary>
///<params name="tabNoeud">tableau contenant les informations sur l'arbre</params>
///<params name="typeAction">0 : rien, 1:couper-coller, 2:couper/copier-coller</params>
///<params name="bAdmin">vrai si l'utilisateur est administrateur de conteneur ou principal</params>
///<params name="lg">largeur du tableau</params>
///<params name="cpt">doit etre 0 lors de l'appel</params>
///<params name="niv">doit etre 0 lors de l'appel</params>
///<returns>Retourne le code html correspondant a l'arbre</returns>
///</comment>
function GetArboHtml($tabNoeud, $iTypeAction, $bAdmin, $lg, $cpt, $niv, $bJS)
{
  global $noLigneArbo;

  $strHtml = "";
	if( sizeof($tabNoeud)==0 )
		return "";

  // algo recursif avec un parcours en profondeur de l'arbre
  // on descend dans les fils, puis les feuilles
  $id = $tabNoeud[$cpt]["ID"];
  $idPere = isset($tabNoeud[$cpt]["PERE"]) ? $tabNoeud[$cpt]["PERE"] : "";
  $strNom = $tabNoeud[$cpt]["NOM"];
  $iType = $tabNoeud[$cpt]["TYPE"];
  $iDroit = $tabNoeud[$cpt]["DROIT"];
  $strTmp = $tabNoeud[$cpt]["TMP"];

  $inter = 16*($iTypeAction+1);

  // affiche la ligne
  $strHtml .= "<table border=0 cellpadding=0 cellspacing=0><tr><td width=".($inter*$niv)."></td>".
    "<td width=".($lg-($inter*$niv)).">";

  // affichage du picto ouverture/fermeture de l'arbo
	
  if( $iType == 0)
    { // type noeud
      $nbEnfant = count($tabNoeud[$cpt]["FILS"]) + count($tabNoeud[$cpt]["FEUILLE"]);
      if( $nbEnfant == 0 )
	{ // pas de fils, ni de feuille
	  $strImg = ALK_URL_SI_IMAGES."icon_arbo_cant_open.gif";
	  $strHref = "#";
	}
      else
	if( true )
	  { // noeud ouvert, possibilite de le fermer
	    $strImg = ALK_URL_SI_IMAGES."icon_arbo_close.gif";
	    if( !$bJS ) 
	      $strHref = "#";
	    else
	      $strHref = "#";
	  }
	else
	  { // noeud ferme, possibilite de l'ouvrir
	    $strImg = ALK_URL_SI_IMAGES."icon_arbo_open.gif";
	    if( !$bJS ) 
	      $strHref = "#";
	    else
	      $strHref = "#";
	  }
    }
  else
    { // type feuille
			$strImg = GetIconeFeuille($strTmp);
			if( substr($strImg, 0, 4) != "http" )
				$strImg = $strImg;
      $strHref = "#";
    }
  if( $strHref != "#" )
    $strHtml .= "<a href=\"".$strHref."\"><img src=\"".$strImg."\" border=0 width=16 height=16 align=middle></a>";
  else
    $strHtml .= "<img src=\"".$strImg."\" border=0 width=16 height=16 align=middle>";

  // affichage du picto coller, si type=noeud et si droit=2
  if( $iTypeAction>0 && $iType==0 && $iDroit==2 )
    {
      $strImg = ALK_URL_SI_IMAGES."icon_paste.gif";
      $strHref = "javascript:ValideAction(".$id.")";
      $strHtml .= "<a href=\"".$strHref."\"><img src=\"".$strImg.
				"\" border=0 width=16 height=16 alt=Coller></a>";
    }


  // affichage du picto couper, si type=feuille et si droit=2
  if( $iTypeAction>1 && $iType==1 && $iDroit==2 )
    {			
      $strImg = ALK_URL_SI_IMAGES."icon_cut.gif";
      $strHref = "javascript:MemoriseDoc(".$id.", 'CUT', ".$noLigneArbo.", ".$idPere.")";
      $strHtml .= "<a href=\"".$strHref."\"><img src=\"".$strImg.
				"\" name=\"imgCut".$id."_".$noLigneArbo."\" border=0 width=16 height=16 alt=Copier></a>";
    }

  // affichage du picto copier, si type=feuille et si droit=2
  if( $iTypeAction==2 && $iType==1 && $iDroit==2 )
    {
      $strImg = ALK_URL_SI_IMAGES."icon_copy.gif";
      $strHref = "javascript:MemoriseDoc(".$id.", 'COPY', ".$noLigneArbo.", ".$idPere.")";
      $strHtml .= "<a href=\"".$strHref."\"><img src=\"".$strImg.
				"\" name=\"imgCopy".$id."_".$noLigneArbo."\" border=0 width=16 height=16 alt=Couper></a>";
    }

  $strHtml .= "&nbsp;";

  // affichage du lien si info fournie
  if( $iType == 0 )
    if( GetLienNoeud($id, $strTmp) != "" )
      $strHtml .= "<a class=aContenuLien href=\"".GetLienNoeud($id, $strTmp)."\"><b>".$strNom."</b></a>".
				"<span class=divContenuTexte>".getInfoNoeud($id, $strTmp)."</span>";
    else
      $strHtml .= "<span class=divContenuTexte><b>".$strNom."</b>".getInfoNoeud($id, $strTmp)."</span>";
  else 
    if( $iType == 1 )
    	if( GetLienFeuille($id, $strTmp) != "" )
				$strHtml .= "<a class=aContenuLien href=\"".GetLienFeuille($id, $strTmp)."\">".$strNom."</a>".
	  			"<span class=divContenuTexte>".getInfoFeuille($id, $strTmp)."</span>";
      else
				$strHtml .= "<span class=divContenuTexte>".$strNom.getInfoFeuille($id, $strTmp)."</span>";
  		$strHtml .= "</td></tr></table>";

  $noLigneArbo++;

  // parcours les feuilles
  while( list($key, $cptFils) = each($tabNoeud[$cpt]["FEUILLE"]) )
    $strHtml .= GetArboHtml($tabNoeud, $iTypeAction, $bAdmin, $lg, $cptFils, $niv+1, $bJS);

  // parcours les fils
  while( list($key, $cptFils) = each($tabNoeud[$cpt]["FILS"]) )
    $strHtml .= GetArboHtml($tabNoeud, $iTypeAction, $bAdmin, $lg, $cptFils, $niv+1, $bJS);

  return $strHtml;
}

///<comment>
///<summary>
/// Affiche le code javascript necessaire au traitement sur l'arbo
///</summary>
///<params name="strAction">url de la page effectuant les traitements de copier/couper/coller</params>
///</comment>
function AffArboDeb($strAction)
{
	if( $strAction != "#" ) 
		{
    ?>
<script language="javascript">
function MemoriseDoc(idFeuille, strAction, noLigneArbo, idNoeudSrc) 
{
	// idFeuille   = identifiant de la feuille pointee
	// strAction   = COPY ou CUT
	// noLigneArbo = numero de la ligne affichee (pour le nommage des images)
	// idPoleSrc   = identifiant du noeud source

	var aIdFeuille = document.formMoveDoc.idFeuille.value;
	var aStrAction = document.formMoveDoc.typeAction.value;
	var nIdFeuille = idFeuille.toString();
	var imgName = "imgCut" + nIdFeuille + "_" + noLigneArbo;
	var imgFileName = "icon_cut";
	if( strAction=="COPY" ) 
	{
		imgName = "imgCopy" + nIdFeuille + "_" + noLigneArbo;
		imgFileName = "icon_copy";
	}
	
	if( aIdFeuille != "" ) 
	{
		MM_swapImgRestore();
		document.formMoveDoc.idFeuille.value = "";
		document.formMoveDoc.typeAction.value = "";
		document.formMoveDoc.idNoeudSrc.value = "";
		if( aIdFeuille==nIdFeuille && (aStrAction==strAction || aStrAction=="") ) return;
	}
	document.formMoveDoc.idFeuille.value = nIdFeuille;
	document.formMoveDoc.typeAction.value = strAction;
	document.formMoveDoc.idNoeudSrc.value = idNoeudSrc;
	MM_swapImage(imgName, "", "<?=ALK_URL_SI_IMAGES?>" + imgFileName + "_rol.gif", 1);
}

function ValideAction(idNoeudDest) 
{
	// idPoleDest   = identifiant du pole destination
	// idRubDest    = identifiant de la rubrique destination

	var idFeuille = document.formMoveDoc.idFeuille.value;
	var strAction = document.formMoveDoc.typeAction.value;
	if( idFeuille != "" ) 
	{
		var bOk;
		if( strAction=="CUT" )
			bOk = window.confirm("Confirmer le d�placement de l'�l�ment s�lectionn� ?");
		else
			bOk = window.confirm("Confirmer la copie de l'�l�ment s�lectionn� ?");
		if( bOk ) {
			document.formMoveDoc.idNoeudDest.value = idNoeudDest.toString();

			// action effectue si Src <> Dest et si Dest <> 0
			if( document.formMoveDoc.idNoeudSrc.value != document.formMoveDoc.idNoeudDest.value && 
					document.formMoveDoc.idNoeudDest.value != 0 )
				document.formMoveDoc.submit();
		}
	}
}
</script>
<form name="formMoveDoc" action="<?=$strAction?>" method="post">
<input type=hidden name="idFeuille" value="">
<input type=hidden name="idNoeudSrc" value="0">
<input type=hidden name="idNoeudDest" value="0">
<input type=hidden name="typeAction" value="">

    <?
    }
  else
    {
	    echo "<form name=formMoveDoc action=\"<?=$strAction?>\" method=post>";
    }
}

///<comment>
///<summary>
/// Affiche le code html necessaire pour la fin de l'arbo
///</summary>
///</comment>
function AffArboFin()
{
	echo "</form>";
}

?>