//------------------------------------------------------------
// OpenWriteToLayer
// Initialise le support d'�criture sur le layer du doc
// Param�tres :
//   objDoc  : objet de type Document
//   idLayer : identifiant du layer
//------------------------------------------------------------
function OpenWriteToLayer(objDoc, idLayer)
{
  if( objDoc.all ) {
    objDoc.all[idLayer].innerHTML = "";
    return;
  }
  if( objDoc.layers ) {
    objDoc.layers[idLayer].clear();
    return;
  }
  var objLayer = objDoc.getElementById(idLayer);
  objLayer.innerHTML = "";
}

//------------------------------------------------------------
// WriteToLayer
// Ecrit le code html sur le layer du doc
// Param�tres :
//   objDoc  : objet de type Document
//   idLayer : identifiant du layer
//------------------------------------------------------------
function WriteToLayer(objDoc, idLayer, strHtml)
{
  if( objDoc.all ) {
    objDoc.all[idLayer].innerHTML = objDoc.all[idLayer].innerHTML + strHtml;
    return;
  }
  if( objDoc.layers ) {
    objDoc.layers[idLayer].write(strHtml);
    return;
  }
  var objLayer = objDoc.getElementById(idLayer);
  objLayer.innerHTML = objLayer.innerHTML + strHtml;
}
//------------------------------------------------------------
// CloseWriteToLayer
// Ferme le contexte d'�criture sur le layer du doc
// Param�tres :
//   objDoc  : objet de type Document
//   idLayer : identifiant du layer
//------------------------------------------------------------
function CloseWriteToLayer(objDoc, idLayer)
{
  if( objDoc.layers ) 
    objDoc.layers[idLayer].close();
}

//------------------------------------------------------------
// GetTxtHtml
// Retourne le code html d'un texte �ventuellement cliquable
// Param�tres :
//   strClassLink : nom de la classe CSS si url
//   strClassTxt  : nom de la classe CSS si Txt
//   strTxt       : texte affich�
//   strUrl       : url du lien, peut �tre vide
//------------------------------------------------------------
function GetTxtHtml(strClassLink, strClassTxt, strTxt, strUrl, strTarget, strId)
{
  if( strClassLink != "" )
    strClassLink = " class=\"" + strClassLink + "\" ";
  if( strClassTxt != "" )
    strClassTxt = " class=\"" + strClassTxt + "\" ";
  if( strTarget != "" )
    strTarget = " target=\"" + strTarget + "\" ";

  var strHtml = "";  
  if( strTxt != "" ) {
    if( strUrl != "" )
      strHtml = " <a id='" + strId + "'" + strClassLink + strTarget + " href=\"" + strUrl + "\">" + strTxt + "</a>";
    else
      strHtml = " <span id='" + strId + "'" + strClassTxt + ">" + strTxt + "</span>";;
  }
  return strHtml;
}
//------------------------------------------------------------
// GetActionFolderHtml
// Retourne le code html d'action ajout et suppr pour un noeud
// Param�tres :
//   strClassButton : nom de la classe du bouton
//   iAction        : identifiant de l'action = 0 non affich�
//                                            = 1 affich�
//   idFolder       : identifiant du noeud
//------------------------------------------------------------
function GetActionFolderHtml(strClassButton, iAction, idFolder)
{
  var strHtml = "";
  switch( iAction  ) {
  case 1 :
    strHtml = "&nbsp;<a class=\"" + strClassButton + 
      "\" href=\"javascript:onTreeviewAddFolder(" + idFolder + ")\">+</a>";
    break;
  case 2:
    strHtml = "&nbsp;<a class=\"" + strClassButton + 
      "\" href=\"javascript:onTreeviewDelFolder(" + idFolder + ")\">x</a>";
    break;
  case 3:
    strHtml = " " +
      "<a class=\"" + strClassButton + 
      "\" href=\"javascript:onTreeviewAddFolder(" + idFolder + ")\">+</a>" +
      "&nbsp;" +
      "<a class=\"" + strClassButton + 
      "\" href=\"javascript:onTreeviewDelFolder(" + idFolder + ")\">x</a>";
    break;
  }
  return strHtml;
}
//------------------------------------------------------------
// GetCheckBox
// Retourne le code html d'une case � cocher pour un noeud
// Param�tres :
//   iChecked : =-1, pas de checkbox
//              = 0, non coch�
//              = 1, coch�
//   idFolder : identifiant du noeud
//------------------------------------------------------------
function GetCheckBox(iChecked, idFolder, strFunctionName, strInputName)
{
  var strHtml = "";
  if( iChecked != -1 ) {
    strHtml = "<input type=checkbox name="+strInputName+ " ";
    if( iChecked==1 ) strHtml += " checked";
    strHtml += " onclick=\"javascript:"+strFunctionName+"('" + idFolder + "')\">";
  }
  return strHtml;
}
//------------------------------------------------------------
// GetImgHtml
// Retourne le code html d'une image �ventuellement cliquable
// Param�tres :
//   strSrc : url de l'image
//   strUrl : url du lien sur l'image, peut �tre vide.
//------------------------------------------------------------
function GetImgHtml(strSrc, strUrl, strName, strTitle, strSize)
{
  var strHtml = "";
  var strSizeImg = "";
  if( strSize != "" )
    strSizeImg = " width=" + strSize;
  if( strSrc != "" ) {
    strHtml = "<img src=\"" + strSrc + "\" align=absmiddle border=0 name=\"" + 
      strName + "\"" + strSizeImg + ">";
    if( strUrl != "" )
      strHtml = "<a title=\""+ strTitle + "\" href=\"" + strUrl + "\">" + strHtml + "</a>";
  }
  return strHtml;
}
//------------------------------------------------------------
// GetTableDebHtml et GetTableFinHtml
// Retourne le code html du tableau contenant la ligne d'arbre
// Param�tres :
//   lg    : largeur du tableau contenant l'arbre
//   iNiv  : niveau d'arborescence � afficher
//   inter : largeur en pixel d'un intervalle de niveau
//------------------------------------------------------------
function GetTableDebHtml(lg, iNiv, inter)
{
  var iLgTab = 0;
  if( iNiv > 0 ) 
    iLgTab = (inter*2)*(iNiv-1)+inter;
  var iLgTxt = lg - iLgTab;
  var strHtml = "<table border=0 cellpadding=0 cellspacing=0><tr>" +
        "<td width=" + iLgTab.toString() + "></td>" +
    "<td width=" + iLgTxt.toString() + ">";  
  return strHtml;
}
function GetTableFinHtml()
{
  var strHtml = "</td></tr></table>\n";
  return strHtml;
}


//--------------------------------------------------------------------------------
// class TreeNodeDoc
//--------------------------------------------------------------------------------
TreeNodeDoc.prototype.constructor = TreeNodeDoc;
function TreeNodeDoc(objTreeview, idDoc, strLib, iDroitLink, iDroitAction, strImgIcone, strUrlTxt, strTarget, iChecked, inputName)
{
  // Definition des attributs
  this.id = idDoc;
  this.idDoc = idDoc.substr(1);
  this.strLib = strLib;
  this.iDroitLink = iDroitLink;
  this.iDroitAction = iDroitAction;
  this.iNiv = 0;
  this.strImgIcone = strImgIcone;
  this.strUrlTxt = strUrlTxt;
  this.strTarget = strTarget;
  this.objTreeview = objTreeview;
  this.objTreeNodeParent = null;
  this.iChecked = iChecked;
  this.inputName = inputName;
  this.strHtml = "";
}

TreeNodeDoc.prototype.SetParent = function(objTreeNodeFolderParent)
{
  this.objTreeNodeParent = objTreeNodeFolderParent;
  if( this.objTreeNodeParent ) {
    this.objTreeNodeParent.AddDoc(this);
    this.iNiv = this.objTreeNodeParent.iNiv + 1;
    this.objTreeNodeParent.strHtml = "";
  }
  this.strHtml = "";
}

TreeNodeDoc.prototype.SetImageRoll = function(strCmd, bOn)
{
  if( this.objTreeview.imgNodeDocSelect != "" ) {
    if( bOn == true )
      MM_swapImage("imgDoc" + this.id, "", this.objTreeview.imgNodeDocSelect, 1);
    else
      MM_swapImgRestore();
  }
}

TreeNodeDoc.prototype.Draw = function()
{
  // affiche la ligne
  var strHtml = this.strHtml;
  if( strHtml == "" ) {
    var strUrlTxt = "";
    var strUrlIcone = "";
    if( this.iDroitLink==2 && this.objTreeview.bDocNameLink == true )
      strUrlTxt = "javascript:onTreeviewDocNameClick(" + this.idDoc.toString() + ")";
    else
      strUrlTxt = this.strUrlTxt;
      
    if( this.iDroitAction==2 && this.objTreeview.imgNodeDocSelect != "" )
      strUrlIcone = "javascript:"+this.objTreeview.strNameObj+".CutDoc('" + this.id + "')";

    strHtml = GetTableDebHtml(this.objTreeview.iTabLg, this.iNiv, this.objTreeview.iTabulation);  
    strHtml += GetImgHtml(this.strImgIcone, strUrlIcone, "imgDoc" + this.id, "", this.objTreeview.iSizeIcon);
    strHtml += GetCheckBox(this.iChecked, this.id, "onTreeviewCheckboxDoc", this.inputName);
    strHtml += GetTxtHtml(this.objTreeview.classLinkDoc, this.objTreeview.classTextDoc, this.strLib, strUrlTxt, this.strTarget, this.idDoc.toString());
    strHtml += GetTableFinHtml();
    this.strHtml = strHtml;
  }
  return strHtml;
}


//--------------------------------------------------------------------------------
// class TreeNodeSelect
//--------------------------------------------------------------------------------
TreeNodeSelect.prototype.constructor = TreeNodeSelect;
function TreeNodeSelect(objTreeview, idSelect, strLib, iDroitLink, iDroitAction, strImgIcone, strUrlTxt, strTarget, iChecked, inputName)
{
  // Definition des attributs
  this.id = idSelect;
  this.idDoc = idDoc.substr(1);
  this.strLib = strLib;
  this.iDroitLink = iDroitLink;
  this.iDroitAction = iDroitAction;
  this.iNiv = 0;
  this.strImgIcone = strImgIcone;
  this.strUrlTxt = strUrlTxt;
  this.strTarget = strTarget;
  this.objTreeview = objTreeview;
  this.objTreeNodeParent = null;
  this.iChecked = iChecked;
  this.inputName = inputName;
  this.strHtml = "";
}

TreeNodeSelect.prototype.SetParent = function(objTreeNodeFolderParent)
{
  this.objTreeNodeParent = objTreeNodeFolderParent;
  if( this.objTreeNodeParent ) {
    this.objTreeNodeParent.AddDoc(this);
    this.iNiv = this.objTreeNodeParent.iNiv + 1;
    this.objTreeNodeParent.strHtml = "";
  }
  this.strHtml = "";
}

TreeNodeSelect.prototype.SetImageRoll = function(strCmd, bOn)
{
  if( this.objTreeview.imgNodeDocSelect != "" ) {
    if( bOn == true )
      MM_swapImage("imgDoc" + this.id, "", this.objTreeview.imgNodeDocSelect, 1);
    else
      MM_swapImgRestore();
  }
}

TreeNodeSelect.prototype.Draw = function()
{
  // affiche la ligne
  var strHtml = this.strHtml;
  if( strHtml == "" ) {
    var strUrlTxt = "";
    var strUrlIcone = "";
    if( this.iDroitLink==2 && this.objTreeview.bDocNameLink == true )
      strUrlTxt = "javascript:onTreeviewDocNameClick(" + this.idDoc.toString() + ")";
    else
      strUrlTxt = this.strUrlTxt;
      
    if( this.iDroitAction==2 && this.objTreeview.imgNodeDocSelect != "" )
      strUrlIcone = "javascript:"+this.objTreeview.strNameObj+".CutDoc('" + this.id + "')";

    strHtml = GetTableDebHtml(this.objTreeview.iTabLg, this.iNiv, this.objTreeview.iTabulation);  
    strHtml += GetImgHtml(this.strImgIcone, strUrlIcone, "imgDoc" + this.id, "", this.objTreeview.iSizeIcon);
    strHtml += GetCheckBox(this.iChecked, this.id, "onTreeviewCheckboxDoc", this.inputName);
    strHtml += GetTxtHtml(this.objTreeview.classLinkDoc, this.objTreeview.classTextDoc, this.strLib, strUrlTxt, this.strTarget, this.idDoc.toString());
    strHtml += GetTableFinHtml();
    this.strHtml = strHtml;
  }
  return strHtml;
}



//--------------------------------------------------------------------------------
// class TreeNodeFolder
//--------------------------------------------------------------------------------
TreeNodeFolder.prototype.constructor = TreeNodeFolder;
function TreeNodeFolder(objTreeview, idFolder, strLib, iNiv, iDroitLink, iDroitAction, strUrlTxt, 
                        strTarget, iDroitAction2, iChecked, strClassLink, strClassText, inputName, strImgIcone)
{
  // Definition des attributs
  this.objTreeview = objTreeview;
  this.id = idFolder;
  this.idFolder = idFolder.substr(1);
  this.strLib = strLib;
  this.iDroitAction = iDroitAction;
  this.iDroitAction2 = iDroitAction2;
  this.iDroitLink = iDroitLink; 
  this.iChecked = iChecked;
  this.iNiv = iNiv;
  this.beforeChilds = false;
  this.bOpen = false;
  if( iNiv>this.objTreeview.iNivOpen ) this.bOpen = false;
  this.strUrlTxt = strUrlTxt;
  this.strTarget = strTarget;
  this.objTreeNodeParent = null;
  this.objTreeNodeFolders = new Array();
  this.iNbFolders = 0;
  this.objTreeNodeDocs = new Array();
  this.iNbDocs = 0;
  this.strHtml = "";
  this.inputName = inputName;
  this.strImgIcone = strImgIcone;
  // class CSS appliqu�e sur le lien d'un texte folder
  this.classLinkFolder = this.objTreeview.classLinkFolder;
  // class CSS appliqu�e sur le texte folder
  this.classTextFolder = this.objTreeview.classTextFolder;
  if ( ! (strClassLink == ''))
      this.classLinkFolder = strClassLink;
  if ( ! (strClassText == ''))
      this.classTextFolder = strClassText;
}

TreeNodeFolder.prototype.SetParent = function(objTreeNodeFolderParent)
{
  this.objTreeNodeParent = objTreeNodeFolderParent;
  this.objTreeNodeParent.AddFolder(this);
  this.iNiv = objTreeNodeFolderParent.iNiv + 1;
  this.strHtml = "";
}

TreeNodeFolder.prototype.CanOpenFolder = function()
{
  // retourne vrai si le noeud peut �tre ouvert
  // un noeud est ouvrable s'il poss�de des feuilles ou un sous-arbre
  if( this.iNbFolders == 0 && this.iNbDocs == 0 )  return false;
  return true;
}

TreeNodeFolder.prototype.AddFolder = function(objTreeNodeFolder)
{
  this.objTreeNodeFolders[objTreeNodeFolder.id] = objTreeNodeFolder;
  this.iNbFolders++;
}

TreeNodeFolder.prototype.AddDoc = function(objTreeNodeDoc)
{
  this.objTreeNodeDocs[objTreeNodeDoc.id] = objTreeNodeDoc;
  this.iNbDocs++;
}

TreeNodeFolder.prototype.RemoveFolder = function(idFolder)
{
  
  var index = 0;
  var tabTmp = new Array();
  for(iTNF in this.objTreeNodeFolders) {
    if( iTNF != idFolder )
      tabTmp[iTNF] = this.objTreeNodeFolders[iTNF];
  }
  this.objTreeNodeFolders = tabTmp;
  this.iNbFolders--;
  this.strHtml = "";
}

TreeNodeFolder.prototype.RemoveDoc = function(idDoc)
{
  var index = 0;
  var tabTmp = new Array();
  for(iTND in this.objTreeNodeDocs) {
    if( iTND != idDoc )
      tabTmp[iTND] = this.objTreeNodeDocs[iTND];
  }
  this.objTreeNodeDocs = tabTmp;
  this.iNbDocs--;
}

TreeNodeFolder.prototype.SetImageRoll = function(strCmd, bOn)
{
  if( this.objTreeview.imgNodeFolderCut != "" && strCmd == "cut" ) {
    if( bOn == true )
      MM_swapImage("imgFolder" + this.id, "", this.objTreeview.imgNodeFolderCut, 1);
    else
      MM_swapImgRestore();
  } else
  if( this.objTreeview.imgNodeFolderCopy != "" && strCmd == "copy" ) {
    if( bOn == true )
      MM_swapImage("imgFolder" + this.id, "", this.objTreeview.imgNodeFolderCopy, 1);
    else
      MM_swapImgRestore();
  }
}

TreeNodeFolder.prototype.DrawSubFolder = function()
{
  strHtml = "";
  
  if (this.classLinkFolder == '')
    this.classLinkFolder = this.objTreeview.classLinkFolder;
  if (this.classTextFolder == '')
    this.classTextFolder = this.objTreeview.classTextFolder;
  
  for(iTNF in this.objTreeNodeFolders) { 
    var oFolder = this.objTreeNodeFolders[iTNF]; 
    var strUrlTxt = "javascript:AffDoc("+oFolder.idFolder+")";
    strHtml += 
      GetTxtHtml(this.classLinkFolder, this.classTextFolder,
                 "&nbsp;&nbsp;&gt;&gt;&nbsp;", "", "", "no"+oFolder.idFolder) +
      GetTxtHtml(this.classLinkFolder, this.classTextFolder,
                 oFolder.strLib, strUrlTxt, oFolder.strTarget, oFolder.idFolder) + "<br>";
  }
  return strHtml;
}

TreeNodeFolder.prototype.DrawLine = function()
{
  var strHtml = "";
  if (this.classLinkFolder == '')
    this.classLinkFolder = this.objTreeview.classLinkFolder;
  if (this.classTextFolder == '')
    this.classTextFolder = this.objTreeview.classTextFolder;
  
  var strImgIcone = this.objTreeview.imgIconCOC; //imgNode;
  var strUrlIcone = "";
  var strTitleIcone = "";
  if( this.CanOpenFolder() == true ) {
    strImgIcone = this.objTreeview.imgNodeLineClose;
    if( strImgIcone == "" ) strImgIcone = this.objTreeview.imgNodeClose;
    strUrlIcone = "javascript:ShowNode(" + this.idFolder.toString() + ");";
    strTitleIcone = this.objTreeview.strToolTipDrawLine;
  }
  var strUrlTxt = "";
  if( this.iDroitLink == 2 ) {
    if( this.objTreeview.bFolderNameLink == true )
      strUrlTxt = "javascript:onTreeviewFolderNameClick(" + this.idFolder.toString() + ")";
    else
      strUrlTxt = this.strUrlTxt;
  }

  if(  this.objTreeNodeParent != null )
    strHtml += GetTxtHtml(this.classLinkFolder, this.classTextFolder,
                          " &gt;&gt; ", "", "", "no"+this.idFolder.toString());
  strHtml += GetTxtHtml(this.classLinkFolder, this.classTextFolder,
                        this.strLib+" ", strUrlTxt, this.strTarget, this.idFolder.toString());
  strHtml += GetImgHtml(strImgIcone, strUrlIcone, "", strTitleIcone, this.objTreeview.iSizeIcon);

  if(  this.objTreeNodeParent != null )
    strHtml = this.objTreeNodeParent.DrawLine() + " " + strHtml;
 
  return strHtml;
}

TreeNodeFolder.prototype.Draw = function()
{
  if(  this.objTreeNodeParent != null )
    if( this.iNiv != this.objTreeNodeParent.iNiv + 1 ) {
      this.iNiv = this.objTreeNodeParent.iNiv + 1;
      this.strHtml = "";    
    }

  var strHtml = this.strHtml;
  if (this.classLinkFolder == '')
    this.classLinkFolder = this.objTreeview.classLinkFolder;
  if (this.classTextFolder == '')
    this.classTextFolder = this.objTreeview.classTextFolder;
  
  if( strHtml == "" ) {
    // affichage de la ligne 
    // determine l'icone � afficher
    var imgNodeFolder = "";
    var urlNodeFolder = "";
    var strTitleFolder = "";
    if( this.iDroitAction >= 2 && this.objTreeview.imgNodeFolder != "" ) {
      imgNodeFolder = this.objTreeview.imgNodeFolder;
      if( this.iDroitAction == 2 && this.objTreeview.imgNodeFolderCut!="" && this.objTreeview.imgNodeFolderCopy!="" ) {
        urlNodeFolder = "javascript:"+this.objTreeview.strNameObj+".CutOrCopyOrPasteFolder('" + this.id + "')";
         strTitleFolder = "";
      } else if( this.iDroitAction == 2 && this.objTreeview.imgNodeFolderCut=="" && this.objTreeview.imgNodeFolderCopy!="" ) {
        urlNodeFolder = "javascript:"+this.objTreeview.strNameObj+".CopyOrPasteFolder('" + this.id + "')";
        strTitleFolder = "";
      } else if( this.iDroitAction == 2 && this.objTreeview.imgNodeFolderCut!="" && this.objTreeview.imgNodeFolderCopy=="" ) {
        urlNodeFolder = "javascript:"+this.objTreeview.strNameObj+".CutOrPasteFolder('" + this.id + "')";
        strTitleFolder = "";
      } else {
        urlNodeFolder = "javascript:"+this.objTreeview.strNameObj+".PasteToFolder('" + this.id + "')";
        strTitleFolder = "Coller";
      }
    }
    if( this.iDroitAction == 1 && this.objTreeview.imgNodeFolder != "" ) {
      imgNodeFolder = this.objTreeview.imgNodeFolder;
      urlNodeFolder = this.strUrlTxt;
      strTitleFolder = this.objTreeview.strTitleNodeFolder;
    }

    var strImgIconOpenClose = this.objTreeview.imgIconCOC;
    var strUrlIconOpenClose = "";
    var strTitleIconOpenClose = "";
    var strImgIconNode = this.objTreeview.imgNode;
    var strUrlIconNode = "";
    var strTitleIconNode = "";
    if( this.CanOpenFolder() == true ) {
      if( this.bOpen == true ) {
          strImgIconOpenClose = this.objTreeview.imgIconO;
          strUrlIconOpenClose = "javascript:"+this.objTreeview.strNameObj+".CloseNode('" + this.id + "');";
          strTitleIconOpenClose = "Fermer";
          strImgIconNode = this.objTreeview.imgNodeOpen;
          if( strImgIconOpenClose == "" ) {
            strUrlIconNode = "javascript:"+this.objTreeview.strNameObj+".CloseNode('" + this.id + "');";
            strTitleIconNode = "Fermer";
          }
      } else {
        strImgIconOpenClose = this.objTreeview.imgIconC;
        strUrlIconOpenClose = "javascript:"+this.objTreeview.strNameObj+".OpenNode('" + this.id + "');";
        strTitleIconOpenClose = "Ouvrir";
        strImgIconNode = this.objTreeview.imgNodeClose;
        if( strImgIconOpenClose == "" ) {
          strUrlIconNode = "javascript:"+this.objTreeview.strNameObj+".OpenNode('" + this.id + "');";
          strTitleIconNode = "Ouvrir";
        }
      }
    }

    var strUrlTxt = "";
    if( this.iDroitLink == 2 ) {
      if( this.objTreeview.bFolderNameLink == true )
        strUrlTxt = "javascript:onTreeviewFolderNameClick(" + this.idFolder.toString() + ")";
      else
        strUrlTxt = this.strUrlTxt;
    }

    strHtml = GetTableDebHtml(this.objTreeview.iTabLg, this.iNiv, this.objTreeview.iTabulation);
    if( strImgIconOpenClose != "" ) 
      strHtml += GetImgHtml(strImgIconOpenClose, strUrlIconOpenClose, "", strTitleIconOpenClose, this.objTreeview.iSizeIcon);
    strHtml += GetImgHtml(strImgIconNode, strUrlIconNode, "", strTitleIconNode, this.objTreeview.iSizeIcon);
    if ( this.strImgIcone!="" )
      strHtml += GetImgHtml(this.strImgIcone, urlNodeFolder, "imgFolder" + this.id, strTitleFolder, this.objTreeview.iSizeIcon);
    else
      strHtml += GetImgHtml(imgNodeFolder, urlNodeFolder, "imgFolder" + this.id, strTitleFolder, this.objTreeview.iSizeIcon);
    strHtml += GetCheckBox(this.iChecked, this.id, "onTreeviewCheckbox", this.inputName);
    strHtml += GetTxtHtml(this.classLinkFolder, this.classTextFolder, this.strLib, strUrlTxt, this.strTarget, this.idFolder.toString());
    strHtml += GetActionFolderHtml(this.objTreeview.classLinkAction, this.iDroitAction2, this.id);
    strHtml += GetTableFinHtml();
    this.strHtml = strHtml;
  }
  
  if ((this.objTreeview.bShowRoot == false) && (this.objTreeview.objTreeNodeRoot == this))
  	strHtml = "";
    
  if( this.bOpen == true ) {
    if (this.beforeChilds){
      // affichage des fils
      for(iTNF in this.objTreeNodeFolders)
        strHtml += this.objTreeNodeFolders[iTNF].Draw();
    }
    
    // affichage des feuilles
    if( this.objTreeview.bShowNode == true ) {
      for(iTND in this.objTreeNodeDocs)
        strHtml += this.objTreeNodeDocs[iTND].Draw();
    }

    if (!this.beforeChilds){
      // affichage des fils
      for(iTNF in this.objTreeNodeFolders)
        strHtml += this.objTreeNodeFolders[iTNF].Draw();
    }
  }
  
  return strHtml;
}

//--------------------------------------------------------------------------------
// class Treeview
//--------------------------------------------------------------------------------
Treeview.prototype.constructor = Treeview;
function Treeview(objDoc, idLayer, iNivOpen, strNameObj)
{
  //Definition des attributs
  this.objDoc = objDoc;
  this.idLayer = idLayer;
  if( Treeview.arguments.length<4 )
    strNameObj = "objTreeview" ;
  this.strNameObj = strNameObj;
  this.bViewHeader = true ;

  // repr�sente la racine du treeview
  this.objTreeNodeRoot = null;

  // contient tous les noeuds du treeview
  this.objTreeNodeFolders = new Array();

  // contient toutes les feuilles du treeview
  this.objTreeNodeDocs = new Array();

  // largeur du tableau contenant le treeview
  this.iTabLg = 500;
  this.iTabulation = 16;

  // class CSS appliqu�e sur le lien d'un texte folder
  this.classLinkFolder = "";
  // class CSS appliqu�e sur le texte folder
  this.classTextFolder = "";
  // class CSS appliqu�e sur le lien d'un texte doc
  this.classLinkDoc = "";
  // class CSS appliqu�e sur le texte doc
  this.classTextDoc = "";
  // class CSS appliqu�e sur les liens action2
  this.classLinkAction = "";

  // img repr�sentant le noeud ouvert
  this.imgNodeOpen = "";
  // img noeud ferm�
  this.imgNodeClose = "";
  // img noeud ferm� sur ligne navigation
  this.imgNodeLineClose = "";
  // img noeud sans sous-arbre
  this.imgNode = "";

  // img noeud [+] ou [-] selon noeud ouvert ou non
  this.imgIconCOC = "";
  this.imgIconO = "";
  this.imgIconC = "";

  // img associ�e � un folder
  this.imgNodeFolder = "";

  // img roll d'un doc s�lectionn�
  this.imgNodeDocSelect = "";

  // img roll d'un folder s�lectionn� pour le cut
  this.imgNodeFolderCut = "";
  // img roll d'un folder s�lectionn� pour le copy
  this.imgNodeFolderCopy = "";

  // flag qui indique si le texte folder est cliquable ou non
  // si oui, la fonction onTreeviewFolderNameClick(idFolder) est appel�e. Cette fonction est � g�n�rer.
  this.bFolderNameLink = false;

  // flag qui indique si le texte doc est cliquable ou non
  // si oui, la fonction onTreeviewDocNameClick(idDoc) est appel�e. Cette fonction est � g�n�rer.
  this.bDocNameLink = false;

  // url de la page demandant la confirmation du d�placement
  this.urlConfirm = "";

  // niveau � ouvrir par d�faut
  this.iNivOpen = iNivOpen;

  // flag � vrai pour afficher les feuilles, � faux pour les noeuds uniquement
  this.bCanShowHideNode = false;
  this.bShowNode = false;
  this.strNodeName = "feuille";

  // m�morisation pour les d�placements de doc entre folder
  this.idFolderSrc = "";
  this.idDocCut = "";
  this.actionCur = "";

  // fournit la taille carr�e de l'icone, = vide implique dimension par d�faut
  this.iSizeIcon = "";

  this.strToolTipDrawLine = "";

  // vrai si arbre d'un fonds doc.
  this.bFdoc = false;
  
  // vrai si ajout racine en cas de p�re introuvable.
  this.bAddRoot = true ;
  
  // vrai si on affiche la racine
  this.bShowRoot = true ;

  // info bulle sur l'image nodefolder d'un noeud
  this.strTitleNodeFolder = "";
}

Treeview.prototype.AddFolder = function(idFolder, idFolderParent, strLib, iNiv, iDroitLink,
                                        iDroitAction, strUrlTxt, strTarget, iChecked, iDroitAction2, strClassLink, strClassText, inputName,
                                        strImgIcone)
{
  if( this.AddFolder.arguments.length<9 ) iChecked = -1;
  if( this.AddFolder.arguments.length<10 ) iDroitAction2 = 0;
  if( this.AddFolder.arguments.length<11 ) strClassLink='';
  if( this.AddFolder.arguments.length<12 ) strClassText='';  
  if( this.AddFolder.arguments.length<13 ) inputName = "c" + idFolder;
  if( this.AddFolder.arguments.length<14 ) strImgIcone = "";
  var oTnf = new TreeNodeFolder(this, idFolder, strLib, iNiv, iDroitLink, iDroitAction, 
                                strUrlTxt, strTarget, iDroitAction2, iChecked, strClassLink, strClassText, inputName, strImgIcone);

  if( this.objTreeNodeRoot == null ) {
    this.objTreeNodeRoot = oTnf;
    this.objTreeNodeFolders[idFolder] = oTnf;
  } else {
    if( this.objTreeNodeFolders[idFolderParent] ){
      this.objTreeNodeFolders[idFolder] = oTnf;
      oTnf.SetParent(this.objTreeNodeFolders[idFolderParent]);
    }
    else if (this.bAddRoot == true){
      // cr�er un parent commun
      this.objTreeNodeFolders[idFolder] = oTnf;
      var oOldRoot = this.objTreeNodeRoot;
      var oTnNewRoot = new TreeNodeFolder(this, idFolderParent, "Racine", 0, 0, 0, "", "", 0, -1);
      this.objTreeNodeRoot = oTnNewRoot;
      this.objTreeNodeFolders[idFolderParent] = oTnNewRoot;
      oOldRoot.SetParent(this.objTreeNodeFolders[idFolderParent]);
      oTnf.SetParent(this.objTreeNodeFolders[idFolderParent]);
    }
  }
}

Treeview.prototype.AddDoc = function(idDoc, idFolderParent, strLib, iDroitLink, 
                                     iDroitAction, strImgIcone, strUrlTxt, strTarget, iChecked, inputName)
{
  if( this.AddDoc.arguments.length<9 ) iChecked = -1;
  if( this.AddDoc.arguments.length<10 )inputName = "";
  var oTnd = new TreeNodeDoc(this, idDoc, strLib, iDroitLink, iDroitAction, 
                             strImgIcone, strUrlTxt, strTarget, iChecked, inputName);
  this.objTreeNodeDocs[idDoc] = oTnd;
  oTnd.SetParent(this.objTreeNodeFolders[idFolderParent]);
}

Treeview.prototype.DrawLine = function(idFolder)
{
  // dessine le parcours pour atteindre idDoc � partir de la racine
  OpenWriteToLayer(this.objDoc, this.idLayer);
  if( this.objTreeNodeRoot != null ) {
    var strIdFolder = "_"+idFolder;
    var oFolder = this.objTreeNodeFolders[strIdFolder];
    WriteToLayer(this.objDoc, this.idLayer, oFolder.DrawLine());
  }

  CloseWriteToLayer(this.objDoc, this.idLayer);
}

Treeview.prototype.DrawSubFolder = function(idLayer, idFolder)
{
  // dessine le parcours pour atteindre idDoc � partir de la racine
  OpenWriteToLayer(this.objDoc, idLayer);
  if( this.objTreeNodeRoot != null ) {
    var strIdFolder = "_"+idFolder;
    var oFolder = this.objTreeNodeFolders[strIdFolder];
    WriteToLayer(this.objDoc, idLayer, oFolder.DrawSubFolder());
  }
  CloseWriteToLayer(this.objDoc, idLayer);
}

Treeview.prototype.Draw = function()
{
  OpenWriteToLayer(this.objDoc, this.idLayer);
  if( this.objTreeNodeRoot != null ) {
    var  strHtml = "";
    if( this.bViewHeader == true ) {
      strHtml += GetTxtHtml(this.classLinkDoc, this.classTextDoc, "Tout d�ployer", "javascript:"+this.strNameObj+".OpenAll()", "", "openAll");
      strHtml += GetTxtHtml(this.classLinkDoc, this.classTextDoc, " / ", "", "");
      strHtml += GetTxtHtml(this.classLinkDoc, this.classTextDoc, "Tout refermer", "javascript:"+this.strNameObj+".CloseAll()", "", "closeAll");
      strHtml += GetTxtHtml(this.classLinkDoc, this.classTextDoc, " / ", "", "");
      if( this.bCanShowHideNode == true ) {
        var strTmp = "Afficher ";
        if( this.bShowNode == true ) strTmp = "Cacher ";
        strTmp += this.strNodeName;
        strHtml += GetTxtHtml(this.classLinkDoc, this.classTextDoc, strTmp, "javascript:"+this.strNameObj+".ShowHideNode()", "", "cacher");
        strHtml += GetTxtHtml(this.classLinkDoc, this.classTextDoc, " / ", "", "", "sp");
      }
      strHtml += GetTxtHtml(this.classLinkDoc, this.classTextDoc, "Actualiser", "javascript:document.location.reload()", "", "actualiser");
      strHtml += "<div style=\"height:10px\"></div>";
    }
    WriteToLayer(this.objDoc, this.idLayer, strHtml);
    WriteToLayer(this.objDoc, this.idLayer, this.objTreeNodeRoot.Draw());
  }
  CloseWriteToLayer(this.objDoc, this.idLayer);
}

Treeview.prototype.OpenNode = function(idNodeFolder)
{
  if (this.objTreeNodeFolders[idNodeFolder].bOpen) return;
  this.objTreeNodeFolders[idNodeFolder].bOpen = true;
  this.objTreeNodeFolders[idNodeFolder].strHtml = "";
  this.Draw();
}
Treeview.prototype.OpenNodeToRoot = function(idNodeFolder)
{
  var oFolder = this.objTreeNodeFolders[idNodeFolder];
  while( oFolder ) {
    oFolder.bOpen = true;
    oFolder.strHtml = "";
    oFolder = oFolder.objTreeNodeParent;
  }
  this.Draw();
}

Treeview.prototype.CloseNode = function(idNodeFolder)
{
  this.objTreeNodeFolders[idNodeFolder].bOpen = false;
  this.objTreeNodeFolders[idNodeFolder].strHtml = "";
  this.Draw();
}

Treeview.prototype.OpenAll = function()
{
  for(iTNF in this.objTreeNodeFolders) {
    this.objTreeNodeFolders[iTNF].bOpen = true;
    this.objTreeNodeFolders[iTNF].strHtml = "";
  }
  this.Draw();
}

Treeview.prototype.CloseAll = function()
{
  for(iTNF in this.objTreeNodeFolders) {
    this.objTreeNodeFolders[iTNF].bOpen = false;
    this.objTreeNodeFolders[iTNF].strHtml = "";
  }
  this.objTreeNodeRoot.bOpen = true;
  this.Draw();
}

Treeview.prototype.ShowHideNode = function()
{
  this.bShowNode = !this.bShowNode;
  this.Draw();
}

Treeview.prototype.CutDoc = function(idDoc)
{
  if( this.idDocCut != "" ) {
    // �teind le roll pr�c�dent
    this.objTreeNodeDocs[this.idDocCut].SetImageRoll("cut", false);
    if( this.idDocCut == idDoc ) {
      // correspond � un second clic sur le m�me picto : aucun effet.
      this.actionCur = "";
      this.idDocCut = "";
      this.idFolderSrc = "";
      return;
    }
  }
  this.idDocCut = idDoc;
  this.actionCur = "cutdoc";
  this.idFolderSrc = this.objTreeNodeDocs[idDoc].objTreeNodeParent.id;
  this.objTreeNodeDocs[idDoc].SetImageRoll("cut", true);
}

Treeview.prototype.CutFolder = function(idFolder)
{
  if( this.idFolderSrc != "" ) {
    // �teind le roll pr�c�dent
    this.objTreeNodeFolders[this.idFolderSrc].SetImageRoll("cut", false);
    if( this.idFolderSrc == idFolder ) {
      // correspond � un second clic sur le m�me picto : aucun effet.
      this.actionCur = "";
      this.idFolderSrc = "";
      return;
    }
  }
  this.idDocCut = "";
  this.actionCur = "cutfolder";
  this.idFolderSrc = idFolder;
  this.objTreeNodeFolders[idFolder].SetImageRoll("cut", true);
}

Treeview.prototype.CopyFolder = function(idFolder)
{
  if( this.idFolderSrc != "" ) {
    // �teind le roll pr�c�dent
    if( this.actionCur == "cutfolder" )
      this.objTreeNodeFolders[this.idFolderSrc].SetImageRoll("cut", false);
    else if( this.actionCur == "copyfolder" )
      this.objTreeNodeFolders[this.idFolderSrc].SetImageRoll("copy", false);
    else {
      // correspond � un second clic sur le m�me picto : aucun effet.
      this.actionCur = "";
      this.idFolderSrc = "";
      return;
    }
  }
  this.idDocCut = "";
  this.actionCur = "copyfolder";
  this.idFolderSrc = idFolder;
  this.objTreeNodeFolders[idFolder].SetImageRoll("copy", true);
}

// v�rifie que idFolderSrc n'est pas un parent de idFolderDest
// retourne null si c'est le cas, retourne FolderSrc sinon
Treeview.prototype.VerifLoop = function(idFolderDest, idFolderSrc)
{
  var objPar = this.objTreeNodeFolders[idFolderDest];
  while( objPar != null ) {
    if( objPar.id != idFolderSrc )
      objPar = objPar.objTreeNodeParent;
    else
      break;
  }
  return objPar;
}

Treeview.prototype.CutOrPasteFolder = function(idFolder)
{
  if( this.idFolderSrc=="" || this.idFolderSrc==idFolder )
    // nouvelle selection ou 2nd clic
    this.CutFolder(idFolder);
  else
    if( this.idDocCut == "" ) {
      // v�rifie que le dest n'est pas un fils de la source, pour �viter une boucle
      var objPar = this.VerifLoop(idFolder, this.idFolderSrc);
      if( objPar == null )
        this.CutPasteFolderToFolder(idFolder);
      else
        alert("Impossible de d�placer un �l�ment vers l'un de ses fils.");
    } else {
      this.PasteToFolder(idFolder);
    }
}

Treeview.prototype.CopyOrPasteFolder = function(idFolder)
{
  if( this.idFolderSrc=="" || this.idFolderSrc==idFolder )
    // nouvelle selection ou 2nd clic, evite de copier/coller sur lui-m�me
    this.CopyFolder(idFolder);
  else
    if( this.idDocCut == "" ) {
      // pas de v�rification particuliere
      this.CopyPasteFolderToFolder(idFolder);
    } else {
      this.CopyToFolder(idFolder);
    }
}

Treeview.prototype.CutOrCopyOrPasteFolder = function(idFolder)
{
  if( this.idFolderSrc=="" || (this.idFolderSrc!="" && this.idFolderSrc==idFolder) ) {
    if( this.actionCur == "" || this.actionCur == "copyfolder" ) {
      // selection du cut
      this.CutFolder(idFolder);
    } else {
      // selection du copy
      this.CopyFolder(idFolder);
    }
  } else
    if( this.idDocCut == "" ) {
      if( this.actionCur == "cutfolder" ) {
        // v�rifie que le dest n'est pas un fils de la source, pour �viter une boucle
        var objPar = this.VerifLoop(idFolder, this.idFolderSrc);
        if( objPar == null )
          this.CutPasteFolderToFolder(idFolder);
        else
          alert("Impossible de d�placer un �l�ment vers l'un de ses fils.");
      } else {
        this.CopyPasteFolderToFolder(idFolder);
      }
    } else {
      this.PasteToFolder(idFolder);
    }
}

Treeview.prototype.PasteToFolder = function(idFolderDest)
{
  if( this.idDocCut != "" ) {
    // d�place un doc
    var res = window.confirm("Confirmer le d�placement ?");
    if( res ) {
      if( this.urlConfirm != "" ) {
        var idDoc = this.objTreeNodeDocs[this.idDocCut].idDoc;
        var idFSrc = this.objTreeNodeFolders[this.idFolderSrc].idFolder;
        var idFDest = this.objTreeNodeFolders[idFolderDest].idFolder;
        window.open(this.urlConfirm+"?folderSrc_id="+idFSrc+"&folderDest_id="+idFDest+"&doc_id="+idDoc, 
                    "footerExec",
                    "status=no,scrollbars=no,resizable=no,height=1,width=1,top=0,left=-200");
      }
      this.objTreeNodeFolders[this.idFolderSrc].RemoveDoc(this.idDocCut);
      this.objTreeNodeDocs[this.idDocCut].SetParent(this.objTreeNodeFolders[idFolderDest]);
      this.Draw();
      this.idDocCut = "";
      this.actionCur = "";
      this.idFolderSrc = "";
    }
  } else {
   if( this.idFolderSrc != "" ) {
    if (this.actionCur == 'cutfolder')
      this.CutPasteFolderToFolder(idFolderDest);
    else if (this.actionCur == 'copyfolder')
      this.CopyPasteFolderToFolder(idFolderDest);
   }
  }
}

Treeview.prototype.CutPasteFolderToFolder = function(idFolderDest)
{
  if( this.idFolderSrc != "" ) {
    // d�place un folder
    var res = window.confirm("Confirmer le d�placement ?");
    if( res ) {
      if( this.urlConfirm != "" ) {
        var idFSrc = this.objTreeNodeFolders[this.idFolderSrc].idFolder;
        var idFDest = this.objTreeNodeFolders[idFolderDest].idFolder;
        window.open(this.urlConfirm+"?op=cut&folderSrc_id="+idFSrc+"&folderDest_id="+idFDest, 
                    "footerExec",
                    "status=no,scrollbars=no,resizable=no,height=1,width=1,top=0,left=-200");
      }
      this.objTreeNodeFolders[this.idFolderSrc].objTreeNodeParent.RemoveFolder(this.idFolderSrc);
      this.objTreeNodeFolders[this.idFolderSrc].SetParent(this.objTreeNodeFolders[idFolderDest]);
      this.objTreeNodeFolders[this.idFolderSrc].objTreeNodeParent.strHtml = "";
      this.Draw();
      this.idDocCut = "";
      this.actionCur = "";
      this.idFolderSrc = "";
    }
  }
}

Treeview.prototype.CopyPasteFolderToFolder = function(idFolderDest)
{
  if( this.idFolderSrc != "" ) {
    // copy un folder
    var res = window.confirm("Confirmer la copie ?");
    if( res ) {
      var iCopyAll = "0";
      var objPar = this.VerifLoop(idFolderDest, this.idFolderSrc);
      if( objPar == null ) {
        if( this.bFdoc == true )
          res = window.confirm("La copie duplique les rubriques, les th�mes et les droits.\n"+
                               "Les fiches documentaires ne seront pas dupliqu�es.\n\n"+
                               "Cliquez sur Ok pour recopier la rubrique et ses sous-rubriques.\n"+
                               "Cliquez sur Annuler pour uniquement recopier la rubrique s�lectionn�e.");
        else
          res = true;
        if( res ) iCopyAll = "1";
      } else {
        if( this.bFdoc == true ) 
          alert("La copie ne concernera que la rubrique s�lectionn�e\n"+
                "car elle poss�de un lien avec la rubrique destinatrice.");
      }
      if( this.urlConfirm != "" ) {
        var idFSrc = this.objTreeNodeFolders[this.idFolderSrc].idFolder;
        var idFDest = this.objTreeNodeFolders[idFolderDest].idFolder;
        window.open(this.urlConfirm+"?op=copy&all="+iCopyAll+"&folderSrc_id="+idFSrc+"&folderDest_id="+idFDest, 
                    "footerExec", 
                    "status=no,scrollbars=no,resizable=no,height=1,width=1,top=0,left=-200");
      }
      // urlConfirm doit recharger la page, puisque les noeuds copi�s poss�dent
      // des identifiants g�n�r�s par la base
      // code : opener.document.location.reload(); opener.focus(); window.close();
    }
  }
}