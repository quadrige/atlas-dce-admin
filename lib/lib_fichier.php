<?
///<comment>
///<summary>
/// Fichier contenant toutes les fonctionnalit�s
/// de manipulation de fichier.
///</summary>
///</comment>

///----------------------------------------------------------------------------------
///<comment>
///<summary>
///Supprime le fichier dont le chemin est pass� en param�tre.
///</summary>
///<param name="strNomFichier">chemin d'acc�s au fichier � supprimer.</param>
///<returns> Retourne vrai si ok, faux sinon </returns>
///</comment>
///----------------------------------------------------------------------------------
function SupprFichier($strNomFichier)
{
	$bSuppr = false;
	if( !(strpos($strNomFichier, "*") === false) ) {
		// suppression des fichiers correspondant au pattern
		$strDir = dirname($strNomFichier);
		$pattern = basename($strNomFichier);
		$pattern = str_replace(array("\*","\?"), array(".*","."), preg_quote($pattern));
		if( substr($strDir, -1) != "/" ) $strDir .= "/";
		if( is_dir($strDir) ) {
			$hDir = opendir($strDir);
			while( $strFileName = readdir($hDir) ) {
				if( is_file($strDir.$strFileName) && preg_match("/^".$pattern."$/", $strFileName) ) {
					//echo $strDir.$strFileName."<br>";
					unlink($strDir.$strFileName);
				}
			}
			closedir($hDir);
		}
	} else
		if( file_exists($strNomFichier) && is_file($strNomFichier) )
			$bSuppr = unlink($strNomFichier) ;
	return $bSuppr;
}

///----------------------------------------------------------------------------------
///<comment>
///<summary>
/// Copie le fichier source vers la destination
/// si la destination existe d�j�, elle est �cras�e
///</summary>
///<param name="strNomFichierSrc">chemin d'acc�s au fichier � copier (source)</param>
///<param name="strNomFichierDest">chemin d'acc�s au fichier copi� (destination)</param>
///<returns> Retourne vrai si ok, faux sinon </returns>
///</comment>
///----------------------------------------------------------------------------------
function CopieFichier($strNomFichierSrc, $strNomFichierDest)
{
	$bCopie = false;
  if( file_exists($strNomFichierSrc) && is_file($strNomFichierSrc) )
		$bCopie = copy($strNomFichierSrc, $strNomFichierDest);
	return $bCopie;
}
///----------------------------------------------------------------------------------
///<comment>
///<summary>
/// VerifyFileName
/// But : v�rifie le nommage du fichier :
///  - n'accepte que les caract�res [a-z][A-Z][0-9]_.-%
///<param name="strFileName">nom du fichier � traiter</param>
///<returns>Retourne le nom du fichier correcte</returns>
///<comment>
///----------------------------------------------------------------------------------
function VerifyFileName($strFileName, $bToLower=false)
{
  // passage en minuscule
  $strTmp = ( $bToLower ? strtolower($strFileName) : $strFileName );

  // remplace les caract�res accentu�s courant par leur �quivalent non accentu�
  // remplace l'espace par soulign�
  $strTmp = strtr($strTmp, " -���������������", "__eeeeaaauuuiiooc");

  // supprime tous les caract�res n'�tant pas : lettre, chiffre, point, tir� et soulign� et %
	$strTmp = preg_replace("/([^_a-zA-Z0-9\%\-\.])/", "", $strTmp);
	$strTmp = str_replace("\\", "", $strTmp);
	return $strTmp;
}

///----------------------------------------------------------------------------------
///<comment>
///<summary>
/// DoUpload
/// But : Effectue l'upload d'un fichier
///<param name="strPostVar">nom de la variable http post�e</param>
///<param name="strPrefixe">pr�fixe ajout� devant le nom du fichier</param>
///<param name="strPathUpload">chemin d'upload (� partir de la racine)</param>
///<param name="iDel">=1 pour supprimer l'ancien fichier, =0 par d�faut</param>
///<param name="strOldFileName">nom de l'ancien fichier � supprimer 
///                             si renseign� et si il existe</param>
///<returns>
/// Si upload uniquement, Retourne le nom du fichier
/// Si suppr uniquement, Retourne chaine vide
/// Si suppr + upload, Retourne le nom du fichier
/// Si rien, Retourne false
///</returns>
///----------------------------------------------------------------------------------
function DoUpload($strPostVar, $strPrefixe, $strPathUpload, $iDel, $strOldFileName)
{
	$bSuppr = false;
	$bUpload = false;
	$strFileName = "";

	if( isset($_FILES[$strPostVar]) == true )	{
    $tabFichier = $_FILES[$strPostVar];
    $bUpload = is_uploaded_file($tabFichier["tmp_name"]);
    // pas possible d'uploader des fichiers js, php et phtml
    $bUpload = ( $bUpload && 
                 strtolower(substr($tabFichier["name"], -3)) != ".js" &&
                 strtolower(substr($tabFichier["name"], -4)) != ".jsp" &&
                 strtolower(substr($tabFichier["name"], -4)) != ".inc" &&
                 strtolower(substr($tabFichier["name"], -4)) != ".php" &&
                 strtolower(substr($tabFichier["name"], -4)) != ".phtml"
                 ? true
                 : false );
  }

	if( $iDel==1 && $strOldFileName!="" || $bUpload==true ) {
    SupprFichier(ALK_SIALKE_PATH.$strPathUpload.$strOldFileName);
    $bSuppr = true;
  }

	if( $bUpload == true ) {
    $strFileName = $strPrefixe.VerifyFileName($tabFichier["name"]);
    $pathFile = ALK_SIALKE_PATH.$strPathUpload.$strFileName;
    move_uploaded_file($tabFichier["tmp_name"], $pathFile);
  }

	if( $bUpload==true || $bSuppr==true )
		return $strFileName;
	return false;
}


///<comment>
///<summary>
///Liste des constantes globales propres au fichier :
///
///FICHIER_MODE_LIRE : mode d'ouverture de fichier en lecture
///FICHIER_MODE_APPEND : mode d'ouverture de fichier en �criture (� la fin du fichier)
///
///FICHIER_ERREUR_AUCUNE : Pas d'erreur
///FICHIER_ERREUR_EXIST : Le fichier n'existe pas
///FICHIER_ERREUR_POINTEUR : Pointeur de fichier incorrect
///</summary>
///</comment>

$FICHIER_MODE_LIRE = "r" ;
$FICHIER_MODE_APPEND = "a" ;

$FICHIER_ERREUR_AUCUNE = "e0" ;
$FICHIER_ERREUR_EXIST = "e1" ;
$FICHIER_ERREUR_POINTEUR = "e2" ;

///<comment>
///<summary>
///Ouvre le fichier, dont le chemin est pass� en param�tre, 
///selon le mode pass� en param�tre
///</summary>
///<params name="strNomFichier">
///Chemin d'acc�s au fichier � ouvrir
///</params>
///<params name="strMode">
///Mode d'ouverture du fichier. Doit prendre l'une des valeurs des constantes FICHIER_MODE.
///</params>
///<returns>
/// Retourne le pointeur correspondant au fichier ouvert, ou le code erreur s'il y a une erreur.
///</returns>
///</comment>
function OpenFichier($strNomFichier, $strMode)
{ 
  if( $strMode == $GLOBALS["FICHIER_MODE_LIRE"] )
		{
			if( file_exists($strNomFichier) && is_file($strNomFichier) )
				return fopen($strNomFichier, $strMode);
			else 
				return $GLOBALS["FICHIER_ERREUR_EXIST"] ;
		} 
	else 
		return fopen($strNomFichier, $strMode);
}

///<comment>
///<summary>
///Ecrit une ligne dans le fichier dont le pointeur est pass� en param�tre.
///</summary>
///<params name="iNumFichier">
///Pointeur du fichier dans lequel �crire la ligne.
///</params>
///<params name="strLigne">
///Ligne � �crire dans le fichier
///</params>
///<returns>
/// Retourne le code erreur.
///</returns>
///</comment>
function EcritFichier($iNumFichier, $strLigne)
{
  if( $iNumFichier )
		{
			fwrite($iNumFichier, $strLigne);
			return $GLOBALS["FICHIER_ERREUR_AUCUNE"] ;
		} 
	else 
		return $GLOBALS["FICHIER_ERREUR_POINTEUR"] ;
}

///<comment>
///<summary>
///Ferme le fichier dont le pointeur est pass� en param�tre.
///</summary>
///<params name="iNumFichier">
///Pointeur du fichier dans lequel �crire la ligne.
///</params>
///<returns>
/// Retourne le code erreur.
///</returns>
///</comment>
function CloseFichier($iNumFichier)
{
  if( $iNumFichier )
		{
			fClose($iNumFichier) ;
			return $GLOBALS["FICHIER_ERREUR_AUCUNE"] ;
		} 
	else 
		return $GLOBALS["FICHIER_ERREUR_POINTEUR"] ;
}

///<comment>
///<summary>
///Fonctions concernant la manipulation de fichier gzip.
///N�cessite la compilation du module gzip dans Apache.
///</summary>
///</comment>
function OpenFichierZip($strNomFichier,$strMode)
{
  echo "OpenFichierZip" ;
  if( $strMode == $GLOBALS["FICHIER_MODE_LIRE"] )
		{
			if (file_exists($strNomFichier))
				return gzopen($strNomFichier,$strMode);
			else
				return $GLOBALS["FICHIER_ERREUR_EXIST"] ;
		} 
	else 
		{
			echo "fichier zip open" ;
			return gzopen($strNomFichier,$strMode);
		}
}

function EcritFichierZip($iNumFichier,$strLigne)
{
  if( $iNumFichier )
		{
			fwrite($iNumFichier, $strLigne);
			return $GLOBALS["FICHIER_ERREUR_AUCUNE"] ;
		} 
	else
		return $GLOBALS["FICHIER_ERREUR_POINTEUR"] ;
}

function CloseFichierZip($iNumFichier)
{
  if( $iNumFichier )
		{
			gzClose($iNumFichier) ;
			return $GLOBALS["FICHIER_ERREUR_AUCUNE"] ;
		} 
	else
		return $GLOBALS["FICHIER_ERREUR_POINTEUR"] ;
}

///<comment>
///<summary>
/// Zip un fichier $strPath.strBasefileName.strExtSrc vers $strPath.strBasefileName.strExtDest
///</summary>
///<params name="iCompress">Nombre de 1=rapide � 9=compression (6 par d�faut)</params>
///<params name="strPath">Chemin physique du fichier (/ � la fin)</params>
///<params name="strBasefileName">Nom de base du fichier</params>
///<params name="strExtSrc">Extension du fichier en entree(doit contenir le point)</params>
///<params name="strExtDest">Extension du fichier en sortie(doit contenir le point)</params>
///</comment>

// exemple
//GZIP(9, $_SESSION["pathPHPBase"].$_SESSION["pathUploadDocSig"], "test", ".svg", ".svgz");
function Fichier_GZIP($iCompress, $strPath, $strBasefileName, $strExtSrc, $strExtDest)
{
	//if( !(is_integer($iCompress) && $iCompress>0 && $iCompress<10) ) 
	//	$iCompress = 6;
	//echo "debut<br>$strPath.$strBasefileName.$strExtSrc -> $strPath.$strBasefileName.$strExtDest<br>";
	exec("gzip -c -q -".$iCompress." ".
			 $strPath.$strBasefileName.$strExtSrc.
			 " > ".
			 $strPath.$strBasefileName.$strExtDest);
	//echo "fin<br>";
}

function Rep_GZIP($iCompress, $strPath, $strDest)
{
	exec("tar -czf ".$strDest." -C ".$strPath." .");
}

///----------------------------------------------------------------------------------
///<comment>
///<summary>
///Cr�� le r�pertoire dont le chemin est pass� en param�tre.
///</summary>
///<param name="strNomR�pertoire">chemin d'acc�s au r�pertoire � supprimer.</param>
///<returns> Retourne vrai si ok, faux sinon </returns>
///</comment>
///----------------------------------------------------------------------------------
function CreeRepertoire($strNomRepertoire)
{
  $bCree = false;
  if( is_dir($strNomRepertoire)==false )
    $bCree = mkdir($strNomRepertoire) ;
  return $bCree;
}

///----------------------------------------------------------------------------------
///<comment>
///<summary>
///Supprime le r�pertoire dont le chemin est pass� en param�tre.
///</summary>
///<param name="strNomR�pertoire">chemin d'acc�s au r�pertoire � supprimer.</param>
///<returns> Retourne vrai si ok, faux sinon </returns>
///</comment>
///----------------------------------------------------------------------------------
function SupprRepertoire($strNomRepertoire)
{
  $bSuppr = false;
  if( file_exists($strNomRepertoire) && is_dir($strNomRepertoire) )
    $bSuppr = rmdir($strNomRepertoire) ;
  return $bSuppr;
}

/**
 * Copy a file, or recursively copy a folder and its contents
 *
 * @param       string   $source    Source path
 * @param       string   $dest      Destination path
 * @return      bool     Returns TRUE on success, FALSE on failure
 */
function copyr($source, $dest)
{
    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }
 
    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest);
    }
 
    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }
 
        // Deep copy directories
        if ($dest !== "$source/$entry") {
            copyr("$source/$entry", "$dest/$entry");
        }
    }
 
    // Clean up
    $dir->close();
    return true;
}

/**
 * renvoie un tableau des fichiers d'un r�pertoire
 *
 * @return      array     Returns array files
 */
function scanEntireDir($dir)
{
    $dh  = opendir($dir);
   
    while (false !== ($filename = readdir($dh))) {
        $files[] = $filename;
    }
   
    closedir($dh);
   
    return $files;
}
/**
 * supprime r�cursivement un r�pertoire et tout son contenu
 *
 * @return      bool     Returns TRUE on success, FALSE on failure
 */
function delTree($f)
{
    if (is_dir($f)) {
        foreach(scanEntireDir($f) as $item){
            if ( (!strcmp($item, '.')) OR (!strcmp($item, '..')) ) continue;
            delTree($f . "/" . $item); // recurision
        }
        rmdir($f);
    }
    else{
        unlink($f);
    }
   
    // check if deleted?
    if (is_dir($f)) return false;
    else return true;
}
/**
 * @brief Affiche l'entete html correspondant au type du fichier
 *        05_docpj_load.php
 *
 * @param strPathFileName Nom complet du fichier � t�l�chager
 * @param strFileName Nom du fichier fourni � l'utilisateur qui t�l�charge
 */
function AffHeaderFileDownload($strPathFileName, $strFileName, $iSize=0)
{
  if( function_exists("mime_content_type") )
    $strType = mime_content_type($strPathFileName);
  else {
    $tabFile = explode(".", $strFileName);
    $strExt = ( count($tabFile)>=2 ? strtolower($tabFile[1]) : "");
    $tabTypeMime = array("txt"  => "text/plain;charset=iso-8859-1",
                         "asc"  => "text/plain;charset=iso-8859-1",
                         "rtx"  => "text/richtext",
                         "rtf"  => "text/rtf",
                         "htm"  => "text/html",
                         "html" => "text/html",
                         "sgml" => "text/sgml",
                         "xml"  => "text/xml",
                         "xsl"  => "text/xml",
                         "css"  => "text/css",
                         
                         "pdf"  => "application/pdf",
                         "tar"  => "application/x-tar",
                         "gz"   => "application/zip",
                         "zip"  => "application/zip",
                         "ps"   => "application/postscript",
                         "ai"   => "application/postscript",
                         "eps"  => "application/postscript",
                         "doc"  => "application/msword",
                         "xls"  => "application/vnd.ms-excel",
                         "ppt"  => "application/vnd.ms-powerpoint",
                         
                         "mpga" => "audio/mpeg",
                         "mp2"  => "audio/mpeg",
                         "mp3"  => "audio/mpeg",
                         "wav"  => "audio/x-wav",
                         "ram"  => "audio/x-pn-realaudio",
                         "rm"   => "audio/x-pn-realaudio",
                         
                         "mpeg"  => "video/mpeg",
                         "mpg"   => "video/mpeg",
                         "mpe"   => "video/mpeg",
                         "qt"    => "video/quicktime",
                         "mov"   => "video/quicktime",
                         "avi"   => "video/x-msvideo",
                         "movie" => "video/x-sgi-movie",
                         
                         "wrl"  => "model/vrml",
                         "vrml" => "model/vrml",
                         
                         "bmp"  => "image/bmp",
                         "jpe"  => "image/jpeg",
                         "jpg"  => "image/jpeg",
                         "jpeg" => "image/jpeg",
                         "gif"  => "image/gif",
                         "png"  => "image/png",
                         "tif"  => "image/tiff",
                         "tiff" => "image/tiff");
    $strType = "application/octet-stream";
    if( array_key_exists($strExt, $tabTypeMime) )
      $strType = $tabTypeMime[$strExt];
  }
	
  if( file_exists($strPathFileName) && is_file($strPathFileName) ) {
    $iSize = filesize($strPathFileName);
  }
	

  
  $bIE = ( STRNAVIGATOR == NAV_IE );
  
  if( $bIE ) {
    header("Content-type: ".$strType."; name=\"".$strFileName."\";\r\n");
    header("Content-Transfer-Encoding: binary"); 
  header("Content-Length: ".$iSize.";\r\n");
    header("Content-Disposition: attachment; filename=".$strFileName.";\r\n");
  header("Expires: ".gmdate("D, d M Y H:i:s", time()-24*60*60)." GMT;\r\n");
  // HTTP/1.1
	//header("Cache-Control: must-revalidate, post-check=0,pre-check=0;\r\n");
	header("Cache-Control: must-revalidate;\r\n");
  // HTTP/1.0
  header("Pragma: public;\r\n");
  } else {
	header("Content-Type: application/force-download; name=\"$strFileName\"");
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: $iSize");
	header("Content-Disposition: attachment; filename=\"$strFileName\"");
	header("Expires: 0");
	header("Cache-Control: no-cache, must-revalidate");
	header("Pragma: no-cache"); 
  }
  
}

/**
 * @brief Retourne dans un tableau, la liste des sous-r�pertoires de strPath
 * @param strPath   chemin physique � lire
 * @param bRec      false par d�faut, vrai pour un parcours recursif
 * @param bAddPath  vrai pour ajouter le chemin complet pour la valeur, la cl� ne contenant que le nom du fichier, =false par d�faut
 * @return array
 */
function getTabDir($strPath, $bRec=false, $bAddPathValue=false, $bGetCount=false)
{
  if( $strPath!="" && $strPath[strlen($strPath)-1] != '/' )
    $strPath .= '/';

  if( !file_exists($strPath) )
    return array();

  $tabRes = array();
  $hDir = opendir($strPath);
  while( $strFile = readdir($hDir) ) {
    if( $strFile=='.' || $strFile=='..' )
      continue;
    if( @is_dir($strPath.$strFile) ) {
      $tabRes[$strFile]["dir"] =  ($bAddPathValue ? $strPath : "" ).$strFile;
      if( $bRec ) {
        $tabRes[$strFile]["subDir"] = getTabDir($strPath.$strFile, $bRec, $bAddPathValue);
      } elseif( !$bGetCount ) {
        $tabSubDir = getTabDir($strPath.$strFile, false, false, true);
        $tabRes[$strFile]["bSubDir"] = ( count($tabSubDir)> 0 );
      }
      
    }
  }
  closedir($hDir);
 
  return $tabRes;
}


/**
 * @brief Retourne dans un tableau, la liste des fichiers ayant l'une des extensions
 *        pr�sentes dans la chaine strExt
 * @param strPath   chemin physique � lire
 * @param tabExt    tableau contenant les extensions de filtre, tableau vide par d�faut
 * @param bRec      false par d�faut, vrai pour un parcours recursif
 * @param tabPrefix tableau contenant les pr�fixes de filtre, tableau vide par d�faut
 * @param bAddPath  vrai pour ajouter le chemin complet pour la valeur, la cl� ne contenant que le nom du fichier, =false par d�faut
 * @return array
 */
function getTabFilesByDir($strPath, $tabExt=array(), $bRec=false, $tabPrefix=array(), $bAddPathValue=false)
{
  if( $strPath!="" && $strPath[strlen($strPath)-1] != '/' )
    $strPath .= '/';

  if( !file_exists($strPath) )
    return array();

  $tabRes = array();
  $hDir = opendir($strPath);
  while( $strFile = readdir($hDir) ) {
    if( $strFile=='.' || $strFile=='..' )
      continue;
    if( $bRec && @is_dir($strPath.$strFile) ) {
      $tabRes = array_merge($tabRes, getTabFilesByDir($strPath.$strFile, $tabExt, $bRec, $tabPrefix, $bAddPathValue));
    } elseif( @is_file($strPath.$strFile) ) {
      $bRes = false;
      if( !empty($tabExt) || !empty($tabPrefix) ) {
        foreach($tabExt as $strExt) {
          $bRes = $bRes || (mb_substr($strFile, -mb_strlen($strExt)-1) == ".".$strExt );
        }
        foreach($tabPrefix as $strPrefix) {
          $bRes = $bRes || (mb_substr($strFile, 0, mb_strlen($strPrefix)) == $strPrefix );
        }
      } else {
        $bRes = true;
      }
      if( $bRes )
        $tabRes[$strFile] =  ($bAddPathValue ? $strPath : "" ).$strFile;
    }
  }
  closedir($hDir);
 
  return $tabRes;
}

/**
 * @brief Retourne la taille du fichier � l'�chelle la plus appropri�e
 *        <1024   : , xx octets
 *        <1024^2 : , xx.x Ko
 *        <1024^3 : , xx.x Mo
 *
 * @param iSize Taille du fichier en octet
 * @return retourne une chaine
 */
function getFileSizePhp5($iSize, $bSep=true)
{
  $iKo = 1024;
  $iMo = 1024*1024;
  $strRes = ($bSep == true ? ", " : "");
  if( $iSize < $iKo )
    $strRes .= $iSize."&nbsp;octets";
  elseif( $iSize < $iMo) {
    $iTmp = $iSize*10;
    $iTmp = round($iTmp/$iKo);
    $iTmp = $iTmp/10;
    $strRes .= $iTmp."&nbsp;Ko";
  } else {
    $iTmp = $iSize*10;
    $iTmp = round($iTmp/$iMo);
    $iTmp = $iTmp/10;
    $strRes .= $iTmp."&nbsp;Mo";
  }
  
  return $strRes;
}

/**
 * Retourne l'extension d'un fichier (sans r�pertoire et bas�e sur 1er . rencontr�)
 * @param strFile     Nom du fichier (sans le chemin)
 * @param bWithPoint  retourne l'extension avec le point si vrai (default false)
 * @param iCasse       Change la casse de caract. : -1=lowercase; 1=uppercase; 0=nochange (-1 default) 
 */
function getFileExtension($strFile, $bWithPoint=false, $iCasse=-1)
{
  $tabMatch = array();
  preg_match("!^(.+)\.(.+)$!usi", $strFile, $tabMatch);
  if ( count($tabMatch)<3 ) return "";

  $strExt = ($bWithPoint ? "." : "");
  switch ( $iCasse ){
  case -1 : // lower case
    $strExt .= mb_strtolower($tabMatch[2]);
    break;
  case 0 : // no change case
    $strExt .= $tabMatch[2];
    break;
  case 1 : // upper case
    $strExt .= mb_strtoupper($tabMatch[2]);
    break;
  }
  return $strExt;
}

/**
 * Retourne le type mime d'un fichier existant
 * Retourne text/plain par d�faut si non trouv�.
 * @param strPathFileName chemin complet et non du fichier � analyser
 * @return string
 */
function GetTypeMime($strPathFileName)
{
  $strRes = "text/plain";
  if( function_exists("mime_content_type") ) {
    $strRes = mime_content_type($strPathFileName);
  } else {

    switch( substr($strPathFileName, -2) ) {
    case ".z": $strRes='application/x-compress'; break; // Fichiers -
    }

    switch( substr($strPathFileName, -3) ) {
    case ".gz": $strRes='application/gzip'; break; // Fichiers GNU Zip
    case ".ps": 
    case ".ai":$strRes='application/postscript'; break; // Fichiers Adobe Postscript
    case ".js": $strRes='application/x-javascript'; break; // Fichiers JavaScript c�t� serveur
    case ".sh": $strRes='application/x-sh'; break; // Fichiers Bourne Shellscript
    case ".au": $strRes='audio/basic'; break; // Fichiers son
    case ".es": $strRes='audio/echospeech'; break; // Fichiers Echospeed
    case ".ra": $strRes='audio/x-pn-realaudio'; break; // Fichiers RealAudio
    case ".qt": $strRes='video/quicktime'; break; // Fichiers Quicktime
    case ".js": $strRes='text/javascript'; break; // Fichiers JavaScript
    
    }
    
    switch( substr($strPathFileName, -4) ) {
    case ".dwg": $strRes='application/acad'; break; // Fichiers AutoCAD (d'apr�s NCSA)
    case ".asd":
    case ".asn": $strRes='application/astound'; break; // Fichiers Astound
    case ".tsp": $strRes='application/dsptype'; break; // Fichiers TSP
    case ".dxf": $strRes='application/dxf'; break; // Fichiers AutoCAD (d'apr�s CERN)
    case ".spl": $strRes='application/futuresplash'; break; // Fichiers Flash Futuresplash
    case "ptlk": $strRes='application/listenup'; break; // Fichiers Listenup
    case ".hqx": $strRes='application/mac-binhex40'; break; // Fichiers binaires Macintosh 
    case ".mbd": $strRes='application/mbedlet'; break; // Fichiers Mbedlet
    case ".mif": $strRes='application/mif'; break; // Fichiers FrameMaker Interchange Format
    case ".xls":
    case ".xla": $strRes='application/msexcel'; break; // Fichiers Microsoft Excel
    case ".hlp":
    case ".chm": $strRes='application/mshelp'; break; // Fichiers d'aide Microsoft Windows
    case ".ppt":
    case ".ppz":
    case ".pps":
    case ".pot": $strRes='application/mspowerpoint'; break; // Fichiers Microsoft Powerpoint
    case ".doc":
    case ".dot": $strRes='application/msword'; break; // Fichiers Microsoft Word
    case ".bin":
    case ".exe":
    case ".com":
    case ".dll":
    case "lass": $strRes='application/octet-stream'; break; // Fichiers ex�cutables
    case ".oda": $strRes='application/oda'; break; // Fichiers Oda
    case ".pdf": $strRes='application/pdf'; break; // Fichiers Adobe PDF
    case ".eps": $strRes='application/postscript'; break; // Fichiers Adobe Postscript
    case ".rtc": $strRes='application/rtc'; break; // Fichiers RTC
    case ".rtf": $strRes='application/rtf'; break; // Fichiers Microsoft RTF
    case ".smp": $strRes='application/studiom'; break; // Fichiers Studiom
    case ".tbk": $strRes='application/toolbook'; break; // Fichiers Toolbook
    case ".vmd": $strRes='application/vocaltec-media-desc'; break; // Fichiers Vocaltec Mediadesc
    case ".vmf": $strRes='application/vocaltec-media-file'; break; // Fichiers Vocaltec Media
    case "cpio": $strRes='application/x-cpio'; break; // Fichiers CPIO
    case ".csh": $strRes='application/x-csh'; break; // Fichiers C-Shellscript
    case ".dcr":
    case ".dir":
    case ".dxr": $strRes='application/x-director'; break; // Fichiers -
    case ".dvi": $strRes='application/x-dvi'; break; // Fichiers DVI
    case ".evy": $strRes='application/x-envoy'; break; // Fichiers Envoy
    case "gtar": $strRes='application/x-gtar'; break; // Fichiers archives GNU tar
    case ".hdf": $strRes='application/x-hdf'; break; // Fichiers HDF
    case ".php": $strRes='application/x-httpd-php'; break; // Fichiers PHP
    case "atex": $strRes='application/x-latex'; break; // Fichiers source Latex
    case ".bin": $strRes='application/x-macbinary'; break; // Fichiers binaires Macintosh
    case ".mif": $strRes='application/x-mif'; break; // Fichiers FrameMaker Interchange Format
    case ".nsc": $strRes='application/x-nschat'; break; // Fichiers NS Chat
    case "shar": $strRes='application/x-shar'; break; // Fichiers atchives Shell
    case ".swf":
    case ".cab": $strRes='application/x-shockwave-flash'; break; // Fichiers Flash Shockwave
    case ".sit": $strRes='application/x-stuffit'; break; // Fichiers Stuffit
    case ".sca": $strRes='application/x-supercard'; break; // Fichiers Supercard
    case ".tar": $strRes='application/x-tar'; break; // Fichiers archives tar
    case ".tcl": $strRes='application/x-tcl'; break; // Fichiers script TCL
    case ".tex": $strRes='application/x-tex'; break; // Fichiers TEX
    case ".src": $strRes='application/x-wais-source'; break; // Fichiers source WAIS
    case ".zip": $strRes='application/zip'; break; // Fichiers archives ZIP
    case ".snd": $strRes='audio/basic'; break; // Fichiers son
    case ".tsi": $strRes='audio/tsplayer'; break; // Fichiers TS-Player
    case ".vox": $strRes='audio/voxware'; break; // Fichiers Vox
    case ".aif":
    case "aiff":
    case "aifc": $strRes='audio/x-aiff'; break; // Fichiers son AIFF
    case ".dus":
    case ".cht": $strRes='audio/x-dspeeh'; break; // Fichiers parole
    case ".mid":
    case "midi": $strRes='audio/x-midi'; break; // Fichiers MIDI
    case ".mp2": $strRes='audio/x-mpeg'; break; // Fichiers MPEG
    case ".ram": $strRes='audio/x-pn-realaudio'; break; // Fichiers RealAudio
    case ".rpm": $strRes='audio/x-pn-realaudio-plugin'; break; // Fichiers plugin RealAudio
    case "ream": $strRes='audio/x-qt-stream'; break; // Fichiers -
    case ".wav": $strRes='audio/x-wav'; break; // Fichiers Wav
    case ".dwf": $strRes='drawing/x-dwf'; break; // Fichiers Drawing
    case ".cod": $strRes='image/cis-cod'; break; // Fichiers CIS-Cod
    case ".ras": $strRes='image/cmu-raster'; break; // Fichiers CMU-Raster
    case ".fif": $strRes='image/fif'; break; // Fichiers FIF
    case ".gif": $strRes='image/gif'; break; // Fichiers GIF
    case ".ief": $strRes='image/ief'; break; // Fichiers IEF
    case "jpeg":
    case ".jpg":
    case ".jpe": $strRes='image/jpeg'; break; // Fichiers JPEG
    case "tiff":
    case ".tif": $strRes='image/tiff'; break; // Fichiers TIFF
    case ".mcf": $strRes='image/vasa'; break; // Fichiers Vasa
    case "wbmp": $strRes='image/vnd.wap.wbmp'; break; // Fichiers Bitmap (WAP)
    case ".pnm": $strRes='image/x-portable-anymap'; break; // Fichiers PBM Anymap
    case ".pbm": $strRes='image/x-portable-bitmap'; break; // Fichiers Bitmap PBM
    case ".pgm": $strRes='image/x-portable-graymap'; break; // Fichiers PBM Graymap
    case ".ppm": $strRes='image/x-portable-pixmap'; break; // Fichiers PBM Pixmap
    case ".rgb": $strRes='image/x-rgb'; break; // Fichiers RGB
    case ".xwd": $strRes='image/x-windowdump'; break; // X-Windows Dump
    case ".xbm": $strRes='image/x-xbitmap'; break; // Fichiers XBM
    case ".xpm": $strRes='image/x-xpixmap'; break; // Fichiers XPM
    case ".wrl": $strRes='model/vrml'; break; // Visualisation de mondes virtuels
    case ".csv": $strRes='text/comma-separated-values'; break; // Fichiers de donn�es s�par�es par des virgules
    case ".css": $strRes='text/css'; break; // Fichiers de feuilles de style CSS
    case ".htm":
    case "html": $strRes='text/html'; break; // Fichiers -
    case ".txt": $strRes='text/plain'; break; // Fichiers pur texte
    case ".rtx": $strRes='text/richtext'; break; // Fichiers texte enrichi (Richtext)
    case ".rtf": $strRes='text/rtf'; break; // Fichiers Microsoft RTF
    case ".tsv": $strRes='text/tab-separated-values'; break; // Fichiers de donn�es s�par�es par des tabulations
    case ".wml": $strRes='text/vnd.wap.wml'; break; // Fichiers WML (WAP)
    case "wmlc": $strRes='application/vnd.wap.wmlc'; break; // Fichiers WMLC (WAP)
    case "wmls": $strRes='text/vnd.wap.wmlscript'; break; // Fichiers script WML (WAP)
    case ".etx": $strRes='text/x-setext'; break; // Fichiers SeText
    case ".sgm":
    case "sgml": $strRes='text/x-sgml'; break; // Fichiers SGML
    case "mpeg":
    case ".mpg":
    case ".mpe": $strRes='video/mpeg'; break; // Fichiers MPEG
    case ".mov": $strRes='video/quicktime'; break; // Fichiers Quicktime
    case ".viv":
    case "vivo": $strRes='video/vnd.vivo'; break; // Fichiers Vivo
    case ".avi": $strRes='video/x-msvideo'; break; // Fichiers Microsoft AVI
    case "ovie": $strRes='video/x-sgi-movie'; break; // Fichiers Movie
    case ".vts":
    case "vtts": $strRes='workbook/formulaone'; break; // Fichiers FormulaOne
    case "3dmf":
    case ".3dm":
    case "qd3d":
    case ".qd3": $strRes='x-world/x-3dmf'; break; // 3Fichiers DMF
    case ".wrl": $strRes='x-world/x-vrml'; break; // Fichiers VRML
    }

  }
   
  /*if( file_exists($strPathFileName) && is_file($strPathFileName) ) {
    ob_start();
    system("file -i -b ". $strPathFileName);
    $type = ob_get_clean();
    $parts = explode(';', $type);
    $parts = explode(' ', $parts[0]);

    $strRes = trim($parts[0]);
  }*/
  return $strRes;
}

?>
