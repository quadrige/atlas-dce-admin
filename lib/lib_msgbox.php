<?php

//
// Fonction qui affiche le contenu d'une fen�tre popup
// de type msgbox. La taille minimale de la fen�tre 350x100
// Retourne le code HTML de celle-ci
//
function GetMsgBoxHtml($strTitle, $strMsg, $strWindow="window", $bReload=false, $strOpenerLocation="")
{
  $strHtml = "<html>".
    "<title>".$strTitle."</title>".
    "<script language='javascript'>".
    "function LoadWindow() {".
    " if( ".$strWindow.".opener ) { this.focus(); ".$strWindow.".opener.nbClickVal = 0; } }".
    "function CloseWindow() {".
    " if( ".$strWindow.".opener ) { ";
  if( $bReload == true ) {
    if( $strOpenerLocation == "" )
      $strHtml .= $strWindow.".opener.document.location.reload(); ";
    else
      $strHtml .= $strWindow.".opener.document.location.href='".$strOpenerLocation."'; ";
  }
  $strHtml .= $strWindow.".opener.focus(); ".
    $strWindow.".opener.nbClickVal = 0; } ".$strWindow.".close(); }".
    "</script>".
    "<body bgcolor='#e6e6e6' style='padding:0px; margin:0px' onload='LoadWindow()'>".
    "<table border='0' width='100%' height='100' cellpadding='10' cellspacing='0'>".
    "<tr>".
    "<td valign='top'>".
    "<img src='".ALK_URL_SI_MEDIA."pictos/picto_info_doc.gif'></td>".
    "<td width='100%' valign='top' height='100%'>".
    "<div style='color:black; font-size:12px; font-family:arial'>".
    $strMsg."</div>".
    "</td>".
    "</tr>".
    "<tr>".
    "<td colspan='2' align='center'>".
    "<a style='color:black; font-size:12px; font-family:arial; text-align:center;".
    " padding-left:30px; padding-right:30px; padding-top:3px; padding-bottom:3px;".
    " background-color:#e6e6e6; border:solid black 1px; text-decoration:none;'".
    " href='javascript:CloseWindow()'>OK</a>".
    "</td>".
    "</tr>".
    "</table>".
    "</body>".
    "</html>";

  return $strHtml;
}

?>