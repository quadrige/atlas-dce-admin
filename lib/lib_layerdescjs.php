//--------------------------------------------
// montre un popup description
// n�cessite d'avoir un calque nomm� layerDesc
// calque positionn� par rapport � la souris
// A appeler sur onMouseOver
//--------------------------------------------
function ShowDesc(strDesc, strBackColor, strBorderColor, strCss)
{
	if( strDesc == "" ) return;

  var strHtml = "";
  if( typeof(strCss) != 'undefined'  ) 
    strHtml = "<table border=0 cellspacing=4 cellpadding=0>" +
      "<tr><td valign=top width=400 class="+strCss+">" + strDesc + "</td></tr></table>";
  else
    strHtml = "<table border=0 cellspacing=4 cellpadding=0 bgcolor=\""+strBackColor+"\" " +
      "style=\"BORDER: "+strBorderColor+" 1px solid;\">" +
      "<tr><td valign=top width=400><span class=sMenu style=\"font-size:9pt; font-weight:normal\">" + strDesc + 
      "</span></td></tr></table>";

	OpenWriteToLayer(document, "layerDesc");
	WriteToLayer(document, "layerDesc", strHtml);
	CloseWriteToLayer(document, "layerDesc");

	// variables globales
	// x = (navigator.appName.substring(0,3) == "Net") ? e.pageX : event.x;
  // y = (navigator.appName.substring(0,3) == "Net") ? e.pageY : event.y;

	if( document.all ) { 
		layerDesc.style.visibility = "visible";
		layerDesc.style.top = y + "px";
		layerDesc.style.left = x + "px";
	}	else if( document.layers ) {
		var objLayer = document.layers["layerDesc"];
		objLayer.style.visibility = "show";
		objLayer.style.top = y + "px";
		objLayer.style.left = x + "px";
	} else	{
		var objLayer = document.getElementById("layerDesc");
		objLayer.style.visibility = "visible";
		objLayer.style.top = y + "px";
		objLayer.style.left = x + "px";
	}
}
//---------------------------
// Cache le calque layerDesc
// A appeler sur onMouseOut
//---------------------------
function HideDesc()
{
	if( document.all ) { 
		layerDesc.style.visibility = "hidden";
	}	else if( document.layers ) {
		var objLayer = document.layers["layerDesc"];
		objLayer.style.visibility = "hide";
	} else	{
		var objLayer = document.getElementById("layerDesc");
		objLayer.style.visibility = "hidden";
	}
}

function getLeft(l)
{
  if (l.offsetParent) return (l.offsetLeft + getLeft(l.offsetParent));
  else return (l.offsetLeft);
}
function getTop(l)
{
  if (l.offsetParent) return (l.offsetTop + getTop(l.offsetParent));
  else return (l.offsetTop);
}

function LayerEvt(idLayerToMove,idTdRef,iX, iY) {
    this.idLayerToMove=idLayerToMove;
    this.idTdRef=idTdRef;
    this.iX=iX;
    this.iY=iY;
}

//--------------------------------------------
// D�place tous les layers contenu dans le tableau tabEvt
// A appeler sur onload
//--------------------------------------------
function MoveLayers()
{
  for (i=0; i<tabEvt.length; i++){
    if( document.all ) { 
      var objTdRef = eval("document.all."+tabEvt[i].idTdRef);
      var objLayer = eval("document.all."+tabEvt[i].idLayerToMove);
      objLayer.style.visibility = "visible";
      objLayer.style.top = (tabEvt[i].iY + getTop(objTdRef)) + "px";
      objLayer.style.left = (tabEvt[i].iX + getLeft(objTdRef)) + "px";
    } else if( document.layers ) {
      var objTdRef = document.layers[tabEvt[i].idTdRef];
      var objLayer = document.layers[tabEvt[i].idLayerToMove];
      objLayer.style.visibility = "show";
      objLayer.style.top = (tabEvt[i].iY + getTop(objTdRef)) + "px";
      objLayer.style.left = (tabEvt[i].iX + getLeft(objTdRef)) + "px";
    } else  {
      var objTdRef = document.getElementById(tabEvt[i].idTdRef);
      var objLayer = document.getElementById(tabEvt[i].idLayerToMove);
      objLayer.style.visibility = "visible";
      objLayer.style.top = (tabEvt[i].iY + getTop(objTdRef)) + "px";
      objLayer.style.left = (tabEvt[i].iX + getLeft(objTdRef)) + "px";
    }
  } 
}

//--------------------------------------------
// D�place un layer par rapport � un layer r�f�rence
//--------------------------------------------
function ShowMoveLayer(strLayer, strLayerRef, iLeft, iTop)
{
  if( document.all ) { 
    var objTdRef = eval("document.all."+strLayerRef);
    var objLayer = eval("document.all."+strLayer);
    if (objTdRef && objLayer ){
      objLayer.style.visibility = "visible";
      objLayer.style.top = (getTop(objTdRef)+iTop) + "px";
      objLayer.style.left = (getLeft(objTdRef)+iLeft) + "px";
    }
  } else if( document.layers ) {
    var objTdRef = document.layers[strLayerRef];
    var objLayer = document.layers[strLayer];
    if (objTdRef && objLayer ){
      objLayer.style.visibility = "show";
      objLayer.style.top = (getTop(objTdRef)+iTop) + "px";
      objLayer.style.left = (getLeft(objTdRef)+iLeft) + "px";
    }
  } else  {
    var objTdRef = document.getElementById(strLayerRef);
    var objLayer = document.getElementById(strLayer);
    window.status = 'abscisse = '+ getTop(objTdRef);
    if (objTdRef && objLayer ){
      objLayer.style.visibility = "visible";
      objLayer.style.top = (getTop(objTdRef)+iTop) + "px";
      objLayer.style.left = (getLeft(objTdRef)+iLeft) + "px";
    }
  }
}

function winReload() {
  MoveLayers();  
}
