function AlertMessageNum(Ctrl, iBorneInf, iBorneSup)
{
  var strBorne = ""
  if( !(iBorneInf==0 && iBorneSup==0) )
    strBorne = " compris entre " + iBorneInf + " et " + iBorneSup;
  else
    strBorne = ".";
   
  alert("Ce champ doit �tre un nombre" + strBorne);
  Ctrl.focus();
  return;
}
function isNumValid(strNum, iBorneInf, iBorneSup) 
{
  if (strNum == "") return false;
  var bOk = false;
  strNum = strNum.replace(/,/i, ".");
  var n = new Number(strNum);
  if( n.toString() == "NaN" ) {
    bOk = false;
  } else {
    if( !(iBorneInf == 0 && iBorneSup == 0) )
      if( iBorneInf <= n && n <= iBorneSup )
        bOk = true;
      else
        bOk = false;
    else
      bOk = true;
  }

  return bOk;
}
function ControlNumber(Objet, iBorneInf, iBorneSup) 
{
  var strNb = Objet.value;
  if( isNumValid(strNb, iBorneInf, iBorneSup) )
    return true;
  else {
    AlertMessageNum(Objet, iBorneInf, iBorneSup);
    return false;
  }
}
function verifNumValid(strNum, iBorneInf, iBorneSup, bStrict) 
{
  if( typeof(bStrict)=="undefined" ) bStrict = true;
  var strReason = ".";
  if (strNum == "") return strReason;
  var iInfBorne = ( iBorneInf == "" ? 0 :  new Number(iBorneInf.replace(/,/i, ".")) );
  var iSupBorne = ( iBorneSup == "" ? 0 :  new Number(iBorneSup.replace(/,/i, ".")) );

  var bOk = false;
  strNum = strNum.replace(/,/i, ".");
  var n = new Number(strNum);
  if( n.toString() == "NaN" ) {
    bOk = false;
  } else {
    bOk = true;
    strReason = "";
  }
 
  if( bOk == true && ( iBorneInf != "" || iBorneSup != "" ) ) {
    strReason = "";
    if( iBorneInf=="" && iBorneSup!=""){
      if( (bStrict && n >= iSupBorne) || (!bStrict && n > iSupBorne) ) {
    	strReason = " inf�rieur"+(bStrict ? " strictement" : "")+" � "+iBorneSup+"."; 
      }
    } else if( iBorneInf!="" && iBorneSup=="" ) {
      if ( (bStrict && iInfBorne >= n) || (!bStrict && iInfBorne > n) ) {
        strReason = " sup�rieur"+(bStrict ? " strictement" : "")+" � "+iBorneInf+".";
      }
    } else if( (bStrict && (n<=iInfBorne || n>=iSupBorne)) || (!bStrict && (n<iInfBorne || n>iSupBorne)) ) {
      strReason = "\n compris"+(bStrict ? " strictement" : "")+" entre "+iBorneInf+" et "+iBorneSup+".";
    }
  }

  return strReason;
}
