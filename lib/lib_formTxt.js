String.prototype.isTextAlpha = function() {
   return /^[A-Za-z����������������������\-' ]+$/.test(this);
}
String.prototype.isTextNum = function() {
   return /^\d+$/.test(this);
}
String.prototype.trim = function() {
  return this.replace(/^\s+|\s+$/, '');
}
function isFieldEmpty(obj, strMsg)
{
  var strTmp = "Ce champ est obligatoire.";
  
  if( typeof(strMsg) != 'undefined' ) strTmp = strMsg;          

  if( obj.value.trim() == "" ) {
    alert(strTmp);
    obj.focus();
    return true;
  }
  return false;
}
function isChaineVide(obj, strMsg) 
{ 
  return isFieldEmpty(obj, strMsg); 
}
function AlertMessage(Ctrl, strMsg) 
{
  alert(strMsg);
  Ctrl.focus();
  return;
}
function AlkReplaceSpecialChar(oCtrl) {
  if( typeof(oCtrl) != "undefined" && 
      typeof(oCtrl.value) != "undefined" ) {
    var strTexte = new String(oCtrl.value);
    var strNew = "";
    for (j=0; j<strTexte.length; j++) {
      if( strTexte.charCodeAt(j) == 8364 ) strNew = strNew + "�";
      else if( strTexte.charCodeAt(j) == 8217 ) strNew = strNew + "'";
      else if( strTexte.charCodeAt(j) == 339 ) strNew = strNew + "oe";
      else if( strTexte.charCodeAt(j) == 338 ) strNew = strNew + "OE";
      else if( strTexte.charCodeAt(j) > 255 ) strNew = strNew + " ";
      else strNew = strNew + strTexte.charAt(j);
    }
    oCtrl.value = strNew;
  }
}
function verifMailValid(adresse) {
	reason = "doit �tre de la forme 'x@y.z' o� x, y, z sont des cha�nes de caract�res non vides";
  if( adresse.length && adresse.length>2 ) {
    var place = adresse.indexOf("@",1);
    //reason = "doit avoir au moins un caract�re avant le caract�re '@'";
    if( place >= 1 ) {
      //reason = "ne peut avoir qu'un seul caract�re '@'";
      if( adresse.indexOf("@", place+1) == -1 ) {
        var point = adresse.lastIndexOf(".");
				var ptInterm = adresse.indexOf(".",place+1);						
        //reason = "doit avoir au moins un caract�re entre le caract�re '@' et le caract�re '.'";
        if (point>-1 && point>place ) {
          //reason = "doit avoir au moins un caract�re apr�s le caract�re '.'";
          if( point < adresse.length-1 ) {
            if( ptInterm!=-1 && ptInterm!=point ) {
              //reason = "doit avoir au moins un caract�re entre le caract�re '@' et le(s) caract�re(s) '.'";
							if( ptInterm >= place+1 ) {
                //reason = "ne peut comporter deux caract�res '.' cons�cutifs";
								if( ptInterm < point-1 )	
                  return "";
							}
						} else {
              return "";
            }
					}
				}
      } 
    }
  }
  reason = "L'adresse "+reason;
  return reason;
}
function verifTextNum(strValue, oMin, oMax)
{
  reason = "";
  var strV = new String(strValue);
  if( strV.isTextNum() == false ) {
    reason = " n'accepte que des chiffres.";
    return reason;
  }
  if( oMin != "" && strV.length < oMin ) {
    reason = " n�cessite "+( oMin==oMax 
                             ? oMin+" chiffre"+(oMin>1 ? "s" : "")
                             : "au moins "+oMin+" chiffre"+(oMin>1 ? "s" : ""));
    return reason;
  }
  if( oMax != "" && strV.length > oMax ) {
    reason = " n�cessite "+( oMin==oMax 
                             ? oMax+" chiffre"+(oMax>1 ? "s" : "")
                             : "au plus "+oMax+" chiffre"+(oMax>1 ? "s" : ""));
    return reason;    
  }
  return reason;
}
function verifTextAlpha(strValue, oMin, oMax)
{
  reason = "";
  var strV = new String(strValue);
  if( strV.isTextAlpha() == false ) {
    reason = " n'accepte que des lettres, l'apostrophe, le tiret et l'espace.";
    return reason;
  }
  if( oMin != "" && strV.length < oMin ) {
    reason = " n�cessite "+( oMin==oMax 
                             ? oMin+" chiffre"+(oMin>1 ? "s" : "")
                             : "au moins "+oMin+" caract�re"+(oMin>1 ? "s" : ""));
    return reason;
  }
  if( oMax != "" && strV.length > oMax ) {
    reason = " n�cessite "+( oMin==oMax 
                             ? oMax+" chiffre"+(oMax>1 ? "s" : "")
                             : "au plus "+oMax+" caract�re"+(oMax>1 ? "s" : ""));
    return reason;    
  }
  return reason;
}

function verifTextUrl(strValue, iTypeUrl)
{
  reason = "";
  var strV = new String(strValue);
  
  if(iTypeUrl == 0) {
    reason = commence_par(strValue, "http://");
  }
  if(iTypeUrl == 1) {
    reason = commence_par(strValue, "https://");
  }
  if(iTypeUrl == 2) {
    reason = commence_par(strValue, "ftp://");
  }
    
  return reason;
}


function commence_par(strValue, strDebut) {
   var reason = "";
   strValue = strValue.toString();
   if (strValue.indexOf(strDebut) != 0) reason = "la valeur doit commencer par '" + strDebut + "'";
   return reason;
}
