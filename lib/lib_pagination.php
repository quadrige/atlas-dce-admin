<?php
/**
 * @brief Affichage de la pagination d'une liste de r�sultats
 *
 * @param tabRes le tableau � deux dimensions contenant le r�sultat de la requ�te
 * @param nbEltParPage nombre d'�l�ments � afficher dans une page
 * @param page num�ro page courante
 * @param urlPlus Param�tre Optionnel. Variables � ajouter � l'url lors du changement de page.
 * @param tabAlign Param�tre Optionnel. Tableau d'alignement des cellules.
 * 
 * @example :  pagine($tabConf, $nbEltParPage, $page, "&conf_id=$conf_id")
 */
function pagine($tabRes, $nbEltParPage, $page)
{
  $numargs = func_num_args();
  if( $numargs >= 4 ) $urlPlus = func_get_arg(3); else $urlPlus = "";
  if ($numargs >= 5)  $tabAlign = func_get_arg (4); else $tabAlign = array();

  $nblignes = count($tabRes);
	$nbcols = 0;
	if( $nblignes>0 )
		$nbcols = count($tabRes[1]);

	if( ALK_B_PRINT==true && isset($_GET["print"]) && $_GET["print"] == "1" )
		$nbEltParPage = $nblignes;

  if( $nbEltParPage==0 )
    $nbEltParPage = 1;

  $nbTotalPages = floor($nblignes / $nbEltParPage) + (($nblignes % $nbEltParPage==0) ? 0 : 1);

  if( $page>1 )
    $debut = ($page-1)*$nbEltParPage;
  else
    $debut = 0;
    
  if( $tabRes )	{
    $liste_resultat = array_slice($tabRes, $debut, $nbEltParPage);

    foreach($liste_resultat as $key => $val) {
      echo "<tr" . (($key % 2 == 0) ? " class=\"trPair1\">" : " class=\"trImpair1\">");

      for($i=0; $i<$nbcols; $i++)	{
        echo "<td " .
          (($key % 2 == 0) ? " class=\"tdPair1\" " : " class=\"tdImpair1\" ") .
          ((count($tabAlign) > 0) ? " align=\"".$tabAlign[$i+1]."\">" : ">");

        if( is_array($val[$i]) ) {
          $iNBTab=count($val[$i]);
          switch($iNBTab)	{
          case 3:
            aff_bouton($val[$i][0], $val[$i][1], $val[$i][2]);
            break;
          case 4:
            aff_bouton($val[$i][0], $val[$i][1], $val[$i][2], $val[$i][3]);
            break;
          case 2:
            if ($val[$i][1]!="") {
              echo "<a " . (($key % 2 == 0)
                            ? " class=\"aTabLienPair1\""
                            : " class=\"aTabLienImpair1\"");
              echo " href=\"".$val[$i][1]."\">".$val[$i][0]."</a>";
            } else {
              echo "<span " . (($key % 2 == 0)
                               ? " class=\"divTabTextePair1\">"
                               : " class=\"divTabTexteImpair1\">");
              echo $val[$i][0]."</span>";
            }
            break;
          }
        } else {
          echo "<span " . (($key % 2 == 0)
                           ? " class=\"divTabTextePair1\">"
                           : " class=\"divTabTexteImpair1\">");
          echo $val[$i]."</span>";
        }
        echo "</td>" ;
      }
      echo "</tr>";
    }

    echo "</table>";

    AffPagination($nblignes, $nbEltParPage, $page, $urlPlus);
  }	else {
    echo "</table><br>";
    echo "<div align=\"center\" class=\"divContenuTexte\">Aucun r�sultat</div>";
  }
}

/**
 * @brief Contruit puis retourne le code html des numeros de pages et les boutons precedent et suivant
 *        en fonction des parametres.
 *        Pas de pagination affichee si une seule page resultat.
 *        Le bouton precedent n'est pas affiche si la page courante est la premiere
 *        Le bouton suivant n'est pas affiche si la page courante est la derniere
 *        si l'url commence par "javascript:", plac� un param�tre � la fonction js nomm� "page"
 *        pour remplacer ce mot cl� par les num�ros de chaque page.
 *
 * @param nbElt nombre d'elements au total.
 * @param nbEltParPage nombre d'elements a afficher dans une page.
 * @param noPage numero de la page courante.
 * @param strUrl url de changement de page, sans le parametre noPage.
 * @return Le code HTML correspondant
 */
function GetPagination($nbElt, $nbEltParPage, $noPage, $strUrl)
{
	$strHtml = "";

  $bUrlJs = ( strtolower(substr($strUrl,0, 11)) == "javascript:" ? true : false );
	
	if ( $noPage == "" )
		$noPage = 1;
	$noPageSuiv = $noPage + 1;
	$noPagePrec = $noPage - 1;
  $nbTotalPages = floor($nbElt / $nbEltParPage) + ($nbElt % $nbEltParPage==0 ? 0 : 1);

  $noPageDebut = $noPage - ($noPage%10);
  if ( $noPageDebut == 0 )
  	$noPageDebut = 1;
	
	if ( $noPageDebut == 1 )
		$noPageFin = $noPageDebut + 8;
	else
		$noPageFin = $noPageDebut + 9;
	
  if ( $noPageFin > $nbTotalPages )
  	$noPageFin = $nbTotalPages;

	// affiche la pagination si resultat sur plusieurs pages
	if( $nbTotalPages>1 && $noPage<=$nbTotalPages && $noPage>0 ) {
  	$strHtml .= "<table class='tabPagine'>".
      "<tr><td width=560 height=15></td></tr>".
      "<tr><td align=center height=30>";

  	if( $noPagePrec > 0 ) {
      $strBtUrl = $strUrl;
      $strBtUrl = ( $bUrlJs == true
                    ? str_replace("page", $noPagePrec, $strBtUrl)
                    : $strUrl."&page=".$noPagePrec);
      $oBtPrec = new HtmlLink($strBtUrl, "Page Pr�c�dente", "precedent.gif", "precedent.gif");
			$strHtml .= $oBtPrec->getHtml();
    }

	  if( $nbTotalPages>1 ) {
	  	$strHtml .= "&nbsp;&nbsp;&nbsp;<span class=divNavPage>Pages&nbsp;:&nbsp;</span>";
      for($i=$noPageDebut; $i<=$noPageFin; $i++)
		  	if( $noPage == $i ) {
		    	$strHtml .= " <span class=divNavPage>".$i."</span>";
		  	} else {
          $strBtUrl = $strUrl;
          $strBtUrl = ( $bUrlJs == true
                        ? str_replace("page", $i, $strBtUrl)
                        : $strUrl."&page=".$i);
		    	$strHtml .= " <a class=aNavPage href=\"".$strBtUrl."\">".$i."</a>";
        }
			$strHtml .= "&nbsp;&nbsp;&nbsp;";
	  }

	  if( $noPageSuiv <= $nbTotalPages ) {
      $strBtUrl = $strUrl;
      $strBtUrl = ( $bUrlJs == true
                    ? str_replace("page", $noPageSuiv, $strBtUrl)
                    : $strUrl."&page=".$noPageSuiv);
      $oBtSuiv = new HtmlLink($strBtUrl, "Page Suivante", "suivant.gif", "suivant.gif");
	  	$strHtml .= $oBtSuiv->getHtml();
    }
	  $strHtml .= "</td></tr></table>";
	  
	  return $strHtml;
	}
}

/**
 * @brief Affiche les numeros de pages et les boutons precedent et suivant
 *        en fonction des parametres.
 *        Pas de pagination affichee si une seule page resultat.
 *        Le bouton precedent n'est pas affiche si la page courante est la premiere
 *        Le bouton suivant n'est pas affiche si la page courante est la derniere
 *
 * @param nbElt nombre d'elements au total.
 * @param nbEltParPage nombre d'elements a afficher dans une page.
 * @param noPage numero de la page courante.
 * @param strUrl url de changement de page, sans le parametre noPage.
 */
function AffPagination($nbElt, $nbEltParPage, $noPage, $strUrl)
{
  $strHtml = GetPagination($nbElt, $nbEltParPage, $noPage, $strUrl);
  echo $strHtml;
}


/**
 * @brief Deuxieme version de la fonction pagine
 *
 * @param tabRes le tableau � deux dimensions contenant le r�sultat de la requ�te
 * @param nblignes Nombre de ligne au total
 * @param nbEltParPage nombre d'�l�ments � afficher dans une page
 * @param page num�ro page courante
 * @param urlPlus Param�tre Optionnel. Variables � ajouter � l'url lors du changement de page.
 * @param tabAlign Param�tre Optionnel. Tableau d'alignement des cellules.
 */
function ListePagine($tabRes, $nblignes, $nbEltParPage, $page)
{
  $numargs = func_num_args();
  if( $numargs >= 5 ) $urlPlus = func_get_arg(4); else $urlPlus = "";
  if ($numargs >= 6)  $tabAlign = func_get_arg(5); else $tabAlign = array();

  echo  getHtmlListePagine($tabRes, $nblignes, $nbEltParPage, $page, $urlPlus, $tabAlign);
  /*	$nbcols = 0;
	if( $nblignes>0 )
		$nbcols = count($tabRes[0]);
  
  if( ALK_B_PRINT==true && isset($_GET["print"]) && $_GET["print"] == "1" )
		$nbEltParPage = $nblignes;

  if( is_array($tabRes) && count($tabRes)>0 )	{
    foreach($tabRes as $key => $val) {
      echo "<tr" . (($key % 2 == 0) ? " class=\"trPair1\">" : " class=\"trImpair1\">");
//       if (count($val)!=1) {
        for($i=0; $i<$nbcols; $i++) {
          echo "<td " .
            (($key % 2 == 0) ? " class=\"tdPair1\" " : " class=\"tdImpair1\" ") .
            ((count($tabAlign) > 0) ? " align=\"".$tabAlign[$i+1]."\">" : ">");

          if( is_array($val[$i]) ) {
            $iNBTab = count($val[$i]);
            switch( $iNBTab )	{
            case 3:
              aff_bouton($val[$i][0], $val[$i][1], $val[$i][2]);
              break;
            case 4:
              aff_bouton($val[$i][0], $val[$i][1], $val[$i][2], $val[$i][3]);
              break;
            case 2:
              if ($val[$i][1]!="") {
                echo "<a ".(($key % 2 == 0)
                            ? " class=\"aTabLienPair1\""
                            : " class=\"aTabLienImpair1\"");
                echo " href=\"".$val[$i][1]."\">".$val[$i][0]."</a>";
              }	else {
                echo "<span ".(($key % 2 == 0)
                               ? " class=\"divTabTextePair1\">"
                               : " class=\"divTabTexteImpair1\">");
                echo $val[$i][0]."</span>";
              }
              break;
            }
          } else {
            echo "<span ".(($key % 2 == 0)
                           ? " class=\"divTabTextePair1\">"
                           : " class=\"divTabTexteImpair1\">");
            echo $val[$i]."</span>";
          }
          echo "</td>" ;
        }

//       } else {
//         echo "<td " .
//             (($key % 2 == 0) ? " class=\"tdPair1\" " : " class=\"tdImpair1\" ") .
//             ((count($tabAlign) > 0) ? " align=\"".$tabAlign[$i+1]."\">" : ">");
//         echo "<a ".(($key % 2 == 0)
//                     ? " class=\"aTabLienPair1\""
//                     : " class=\"aTabLienImpair1\"");
//         echo " href=\"".$val[1]."\">".$val[0]."</a>";
//         echo "</td>" ;
//       }

      echo "</tr>";
    }
    
    echo "</table>";

    AffPagination($nblignes, $nbEltParPage, $page, $urlPlus);
  }	else {
    echo "</table><br>";
    echo "<div align=\"center\" class=\"divContenuTexte\">Aucun r�sultat</div>";
  }*/
}

/**
 * @brief Deuxieme version de la fonction pagine
 *
 * @param tabRes le tableau � deux dimensions contenant le r�sultat de la requ�te
 * @param nblignes Nombre de ligne au total
 * @param nbEltParPage nombre d'�l�ments � afficher dans une page
 * @param page num�ro page courante
 * @param urlPlus Param�tre Optionnel. Variables � ajouter � l'url lors du changement de page.
 * @param tabAlign Param�tre Optionnel. Tableau d'alignement des cellules.
 */
function getHtmlListePagine($tabRes, $nblignes, $nbEltParPage, $page)
{
  $numargs = func_num_args();
  if( $numargs >= 5 ) $urlPlus = func_get_arg(4); else $urlPlus = "";
  if ($numargs >= 6)  $tabAlign = func_get_arg(5); else $tabAlign = array();

	$nbcols = 0;
	if( $nblignes>0 )
		$nbcols = count($tabRes[0]);
  
  $strHtml = "";

  if( ALK_B_PRINT==true && isset($_GET["print"]) && $_GET["print"] == "1" )
		$nbEltParPage = $nblignes;

  if( is_array($tabRes) && count($tabRes)>0 )	{
    foreach($tabRes as $key => $val) {    
      if ($key == 0)
        $strHtml .= "<tbody>";
          
      $strHtml .= "<tr" . ( ($key % 2 == 0) 
                            ? " class='trPair1'>" 
                            : " class='trImpair1'>" );
      for($i=0; $i<$nbcols; $i++) {
        $strHtml .= "<td " .
          ( ($key % 2 == 0) 
            ? " class='tdPair1' " 
            : " class='tdImpair1' ") .
          ( (count($tabAlign) > 0) 
            ? " align='".$tabAlign[$i+1]."'>" 
            : ">");
        
        if( is_array($val[$i]) ) {
          $iNBTab = count($val[$i]);
          switch( $iNBTab )	{
          case 3:
            // bouton
            $oBt = new HtmlLink($val[$i][2], "", $val[$i][0], $val[$i][1]);
            $strHtml .= $oBt->getHtml();
            //aff_bouton($val[$i][0], $val[$i][1], $val[$i][2]);
            break;
          case 4:
            // bouton
            $oBt = new HtmlLink($val[$i][2], $val[$i][3], $val[$i][0], $val[$i][1]);
            $strHtml .= $oBt->getHtml();
            
            //aff_bouton($val[$i][0], $val[$i][1], $val[$i][2], $val[$i][3]);
            break;
          case 2:
            if( $val[$i][1] != "" ) {
              $strHtml .= "<a ".( ($key % 2 == 0)
                                  ? " class='aTabLienPair1'"
                                  : " class='aTabLienImpair1'");
              $strHtml .= " href=\"".$val[$i][1]."\">".$val[$i][0]."</a>";
            }	else {
              $strHtml .= "<span ".( ($key % 2 == 0)
                                     ? " class='divTabTextePair1'>"
                                     : " class='divTabTexteImpair1'>");
              $strHtml .= $val[$i][0]."</span>";
            }
            break;
          }
        } else {
          $strHtml .= "<span ".( ($key % 2 == 0)
                                 ? " class='divTabTextePair1'>"
                                 : " class='divTabTexteImpair1'>");
          $strHtml .= $val[$i]."</span>";
        }
        $strHtml .= "</td>" ;
      }
      
      $strHtml .= "</tr>";      
    }
    
    $strHtml .= "</tbody></table>";
    $strHtml .= GetPagination($nblignes, $nbEltParPage, $page, $urlPlus);
  }	else {
    $strHtml .= "</table><br>";
    $strHtml .= "<div align='center' class='divContenuTexte'>Aucun r�sultat</div>";
  }
  
  return $strHtml;
}

?>
