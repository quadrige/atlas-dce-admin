<?

///<comment>
///<summary>
/// Supprime les donn�es SIG associ�s � la base documentaire
///</summary>
///<params name="doc_id">Identifiant du document � supprimer</params>
///</comment>
function SupprDocSIG($doc_id)
{
	global $dbConn;

	// m�morise les analyses liees a l'asso projet-couche lie au doc
	$strSql = "select ANALYSE_ID from SIG_01_COUCHE_PROJET cp ".
		"where COUCHE_ID in (select COUCHE_ID from SIG_01_COUCHE where DOC_ID=".$doc_id.")".
		" or PROJET_ID in (select PROJET_ID from SIG_01_PROJET where DOC_ID=".$doc_id.")";
	$dsAnalyse = $dbConn->initDataSet($strSql);
	$strListeId = "";
	$strListeIdAnal = "";
	while( $drAnalyse = $dsAnalyse->getRowIter() )
	{
		$strListeId .= ",".$drAnalyse->getValueName("ANALYSE_ID");
		if( $drAnalyse->getValueName("ANALYSE_ID") != 0 )
			$strListeIdAnal .= ",".$drAnalyse->getValueName("ANALYSE_ID");
	}
	$strListeId = "(-1".$strListeId.")";
	$strListeIdAnal = "(-1".$strListeIdAnal.")";

	//Suppression des jointures li�es au projet ou � la couche
	$tabFichier = array() ;
	$strSql = "select NOM_FICHIER from SIG_01_JOINTURE_PROJET j, SIG_01_PROJET p ".
		"where p.PROJET_ID=j.PROJET_ID and p.DOC_ID=".$doc_id;
	$dsPj = $dbConn->initDataSet($strSql);
	while( $drPj = $dsPj->getRowIter() )
		{
			$tabFichier[count($tabFichier)] = $drPj->getValueName("NOM_FICHIER");
		}	

	$strSql = "select NOM_FICHIER from SIG_01_JOINTURE_PROJET j, SIG_01_COUCHE c ".
		"where c.COUCHE_ID=j.COUCHE_ID and c.DOC_ID=".$doc_id;
	$dsPj = $dbConn->initDataSet($strSql);
	while( $drPj = $dsPj->getRowIter() )
		{
			$tabFichier[count($tabFichier)] = $drPj->getValueName("NOM_FICHIER");
		}	

	$strSql = "delete from SIG_01_JOINTURE_PROJET where couche_id in ".
		"(select couche_id from SIG_01_COUCHE where DOC_ID=".$doc_id.")";
	$dbConn->executeSql($strSql);

	$strSql = "delete from SIG_01_JOINTURE_PROJET where projet_id in ".
		"(select projet_id from SIG_01_PROJET where DOC_ID=".$doc_id.")";
	$dbConn->executeSql($strSql);

	for ($i=0; $i<count($tabFichier); $i++)
		{
			$strSql = "select * from SIG_01_JOINTURE_PROJET ".
				"where NOM_FICHIER = '".$tabFichier[$i]."'";
			$dsFichier = $dbConn->initDataSet($strSql);
			if( $drFichier = $dsFichier->getRowIter() )
				{		
					$pathFile = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_SIG_JOINTURES.$tabFichier[$i];
					if( file_exists($pathFile) && is_file($pathFile) )
						unlink($pathFile);
				}	
		}
			

	// suppression des liens projet-couche
	$strSql = "delete from SIG_01_COUCHE_PROJET ".
		"where COUCHE_ID in (select COUCHE_ID from SIG_01_COUCHE where DOC_ID=".$doc_id.")".
		" or PROJET_ID in (select PROJET_ID from SIG_01_PROJET where DOC_ID=".$doc_id.")";
	$dbConn->executeSql($strSql);

	// suppression des classes liees aux analyses
	$strSql = "delete from SIG_01_CLASSE where ANALYSE_ID in ".$strListeId;
	$dbConn->executeSql($strSql);

	// suppression des symbols lies aux analyses
	$strSql = "delete from SIG_01_SYMBOL where ANALYSE_ID in ".$strListeId;
	$dbConn->executeSql($strSql);	

	// suppression des analyses
	$strSql = "delete from SIG_01_ANALYSE where ANALYSE_ID in ".$strListeIdAnal;
	$dbConn->executeSql($strSql);

	// suppression des pj li�es aux couches
	$strSql = "select pj.PJ_NOM_SIG_PJ from SIG_01_PJ pj, SIG_01_COUCHE c ".
		"where c.COUCHE_ID=pj.COUCHE_ID and c.DOC_ID=".$doc_id;
	$dsPj = $dbConn->initDataSet($strSql);
	while( $drPj = $dsPj->getRowIter() )
		{
			$strFileName = $drPj->getValueName("PJ_NOM_SIG_PJ");
			$pathFile = ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_SIG.$strFileName;
			if( file_exists($pathFile) && is_file($pathFile) )
				unlink($pathFile);
		}						
	$strSql = "delete from SIG_01_PJ where couche_id in ".
		"(select couche_id from SIG_01_COUCHE where DOC_ID=".$doc_id.")";
	$dbConn->executeSql($strSql);

	// suppression de la couche li�e au doc
	$strSql = "delete from SIG_01_COUCHE where DOC_ID=".$doc_id;
	$dbConn->executeSql($strSql);

	// suppression du projet li� au doc
	$strSql = "delete from SIG_01_PROJET where DOC_ID=".$doc_id;
	$dbConn->executeSql($strSql);

}

?>
