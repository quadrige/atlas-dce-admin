<?php
/**
 * @brief Lit les valeurs strLogin, strMdp et iDept enregistr�es dans le cookie nomm� "ALKANTE"
 *        Si ALK_SET_COOKIE d�finie et vrai et si le cookie nomm� "ALKANTE" existe : initialise les identifiants de connexion
 * @param & strLogin    Le login � initialiser � partir du cookie
 * @param & strMdp      Le mot de passe � initialiser � partir du cookie
 * @param & iDept       Le num�ro de d�partement � initialiser � partir du cookie
 */
function GetCookieConn(&$strLogin, &$strMdp, &$iDept)
{
  if  ( defined("ALK_SET_COOKIE") && ALK_SET_COOKIE==true ){
    if ( array_key_exists("ALKANTE", $_COOKIE) ){
      $tabValue = explode("|", $_COOKIE["ALKANTE"]);
      if ( count($tabValue)==3 ){
        $login = strrev(Decode($tabValue[0]));
        $mdp   = strrev(Decode($tabValue[1]));
        $dept  = strrev(Decode($tabValue[2]));
        if ( $login!="" && $mdp!="" ){
          $strLogin = $login;
          $strMdp = $mdp;
          $iDept = $dept;
        }
      }     
    }
  }
}

/**
 * @brief Si ALK_SET_COOKIE d�finie et vraie, cr�e un cookie nomm� "ALKANTE" avec les identifiants de connexion
 * @param strLogin    Le login � enregistrer dans le cookie
 * @param strMdp      Le mot de passe � enregistrer dans le cookie
 * @param iDept       Le num�ro de d�partement � enregistrer dans le cookie
 */
function SetCookieConn($strLogin, $strMdp, $iDept)
{
  if  ( defined("ALK_SET_COOKIE") && ALK_SET_COOKIE==true ){
    if ( !array_key_exists("ALKANTE", $_COOKIE) || (array_key_exists("ALKANTE_HASCOOKIE", $_COOKIE) && $_COOKIE["ALKANTE_HASCOOKIE"]=="false") ){
      $value = Encode(strrev($strLogin))."|".Encode(strrev($strMdp))."|";
      CreateCookie("ALKANTE", $value, (defined("ALK_COOKIE_EXPIRE_DAYS") ? ALK_COOKIE_EXPIRE_DAYS : 30));
      CreateCookie("ALKANTE_HASCOOKIE", "true", (defined("ALK_COOKIE_EXPIRE_DAYS") ? ALK_COOKIE_EXPIRE_DAYS : 30));
    }
  }
}

/**
 * @brief En cas d'�chec d'authentification suite � la lecture des valeurs d'un cookie, suppression du cookie "ALKANTE"
 */
function DelCookieConn()
{
  if  ( defined("ALK_SET_COOKIE") && ALK_SET_COOKIE==true ){
    if ( array_key_exists("ALKANTE", $_COOKIE) ){
      CreateCookie("ALKANTE", false, (defined("ALK_COOKIE_EXPIRE_DAYS") ? ALK_COOKIE_EXPIRE_DAYS : 30));
      CreateCookie("ALKANTE_HASCOOKIE", "false", (defined("ALK_COOKIE_EXPIRE_DAYS") ? ALK_COOKIE_EXPIRE_DAYS : 30));
    }
  }
}

function CreateCookie($name, $value, $days_expire=30)
{
  $expire = time()*60*60*24*$days_expire;
  $url = parse_url(ALK_SIALKE_URL);
  $domain = $url["host"];
  setcookie($name, $value, $expire, "/", ".".$domain);
}
?>
