<?php
$g_strLogin = "";

/**
 * @brief Retourne l'ent�te html de la page
 *
 * @param tabNav       Chaine ou tableau pour la navigation en profondeur
 * @param iDroitAppli  droit utilisateur sur l'appli
 * @param tabAppliMenu tableau contenant les �l�ments d'un sous menu popup
 * @param bAdminPart   Partie consultation = false, partie administration = true
 */
function aff_menu_haut($tabNav="", $iDroitAppli=-1, $tabAppliMenu = array(), $bAdminPart=false)
{
  echo getHtmlHeader($tabNav, $iDroitAppli, $tabAppliMenu, $bAdminPart);
}

/**
 * @brief Affiche le code html du bas de la page
 */
function aff_menu_bas()
{
  echo getHtmlFooter();
}

/**
 * @brief Retourne le code html du haut de la page (Logo + bandeau haut + menu gauche)
 *
 * @param tabNav       Tableau associatif contenant les liens de navigation
 * @param iDroitAdmin  Droit sur l'appli en cours =4 publicateur, =2 admin, =1 lecture, =0 aucun
 * @param tabAppliMenu Tableau associatif contenant les infos statiques sur des �l�ments de menu � inclure
 * @param bAdminPart   Partie consultation = false, partie administration = true
 * @return Retourne un string
 */
function getHtmlHeader($tabNav=array(), $iDroitAdmin=-1, $tabAppliMenu = array(), $bAdminPart=false)
{
  global $g_strLogin;
  global $_sit_Type_Appli, $_sit_urlHomePage, $_sit_targetHomePage, $_sit_menuAdminAppli;
  global $_sit_tabMenu, $_sit_tabMenuFils, $_sit_tabMenuEspace, $_sit_tabMenuEspaceFils;
  global $oSpace, $oAppli;
  //global $iProfilCont;
  global $_bAffMenuSitGauche;
  global $arbreMenuAppli;
  
  $_bAffMenuSitGauche = false;

  $cont_id = $oSpace->cont_id;
  $cont_appli_id = $oSpace->appli_id;
  //if( !($iProfilCont!="" && is_numeric($iProfilCont)) )
    $iProfilCont = $oSpace->iProfilCont;

  // R�cup�res les param�tres
  if( is_bool($tabAppliMenu) ) 
    $bAdminPart = $tabAppliMenu;

  $strUrlAccueil = ( !isset($_SESSION["sit_HomePageUser"]) ? "" : $_SESSION["sit_HomePageUser"] );
  $strDefaultTarget = ( defined("ALK_DEFAULT_TARGET")==true ? ALK_DEFAULT_TARGET : "");
  $strTargetAccueil = ( !isset($_SESSION["sit_TargetHomePageUser"]) ? $strDefaultTarget : $_SESSION["sit_TargetHomePageUser"] );

  $tabUrl = $oSpace->getTabUrl(ALK_SIALKE_URL, ALK_DIR_ESPACE, ALK_DIR_ANNU, 
                               ALK_SI_MAIL, ALK_URL_APPLI, ALK_URL_ON_LOGO, $strUrlAccueil, $strTargetAccueil);

  $tabAppli = $oSpace->setAppliProperty($_sit_Type_Appli);
  //Droit d'admin sur l'annuaire pour anim espace si ALK_B_ANIM_ADMIN_ANNU
  if ( $_sit_Type_Appli == 1 && $oSpace->iDroitAnnu < 3 )
    $iDroitAdmin = 2;
  
  // --------------------------------------------
  // url sommaire conteneur + titre + admin lien
  // --------------------------------------------
  $g_strLogin = $tabUrl["agent"]["login"];

  $strTitrePage      = $oSpace->getProperty("CONT_INTITULE");
  $strTitreAliasPage = $oSpace->getProperty("CONT_INTITULE_COURT");
  $iConteneurPublic  = $oSpace->getProperty("CONT_PUBLIC");
  $strTitrePhoto     = $oSpace->getProperty("CONT_LOGO");
  $strTitrePhoto2    = $oSpace->getProperty("CONT_LOGO2");
  if( $strTitrePhoto != "" )
    $strTitrePhoto = ALK_SIALKE_URL.ALK_PATH_UPLOAD_SPACE_LOGO.$strTitrePhoto;
  if( $strTitrePhoto2 != "" )
    $strTitrePhoto2 = ALK_SIALKE_URL.ALK_PATH_UPLOAD_SPACE_LOGO.$strTitrePhoto2;

  $_sit_menuAdminAppli = $oSpace->getTabAdminAppli(ALK_SIALKE_URL, ALK_PATH_UPLOAD_APPLI_LOGO, $iDroitAdmin);

  // D�termine la page d'accueil pour l'utilisateur 
  $_sit_urlHomePage = $tabUrl["accueil"];
  $_sit_targetHomePage = $tabUrl["targetAcc"];
  $_sit_urlLogoSite = $tabUrl["logo"];
  $_SESSION["sit_HomePageUser"] = $tabUrl["accueil"];
  $_SESSION["sit_TargetHomePageUser"] = $_sit_targetHomePage;

  $urlAdminGen = $tabUrl["admin"];

  //chemin de navigation des conteneurs
  $strChemin = getLienNav($tabNav, $oSpace->cont_id, $strDefaultTarget);

  // liste des applications
  $tabMenuAppli = $oSpace->getTabMenuAppli($tabAppliMenu, ALK_SIALKE_URL, ALK_PATH_UPLOAD_APPLI_LOGO);
  $_sit_tabMenu = $tabMenuAppli[0];
  $_sit_tabMenuFils = $tabMenuAppli[1];

  // liste des espaces et sous-espaces
  if( defined("ALK_TYPE_INTERTACE") && ALK_TYPE_INTERTACE=="SIT" ||
      defined("ALK_TYPEMENU_SPACE") && ALK_TYPEMENU_SPACE=="FILS" ) {
    $tabMenuEspace = $oSpace->getTabMenuEspace(ALK_SIALKE_URL, ALK_DIR_ESPACE, 
                                               ALK_PATH_UPLOAD_SPACE_LOGO, $strDefaultTarget);    
  } else { // alkanet
    $tabMenuEspace = $oSpace->getTabMenuEspaceFrere(ALK_SIALKE_URL, ALK_DIR_ESPACE, 
                                                    ALK_PATH_UPLOAD_SPACE_LOGO, $strDefaultTarget);
  }
  $_sit_tabMenuEspace = $tabMenuEspace[0];
  $_sit_tabMenuEspaceFils = $tabMenuEspace[1];

  // Enregistre les statistiques
  if ( ALK_B_APPLI_STAT == true ) {
    MajTabStat(1, $cont_id);
    MajTabStat(2, $cont_appli_id);
  }
  
  // affiche l'entete html
  $strHtml = getHtmlMainHeader().

    "<script language='javascript' src='../../lib/lib_js.js'></script>".
    "<script language='JavaScript'>".
    " navigateur = null;".
    " x = null;".
    " y = null;".
    " bWinLoaded = 0;".
    " menuBar = null;".
    " ALK_SIALKE_URL = '".ALK_SIALKE_URL."';".
    " ALK_URL_ASPIC = '".( defined("ALK_URL_ASPIC") ? ALK_URL_ASPIC : "")."';".
    " ALK_URL_ASPIC_V2 = '".( defined("ALK_URL_ASPIC_V2") ? ALK_URL_ASPIC_V2 : "")."';".
    " ALK_DIR_ANNU = '".ALK_DIR_ANNU."';".
    " ALK_DIR_ESPACE = '".ALK_DIR_ESPACE."';".
    " ALK_URL_IDENTIFICATION = '".ALK_URL_IDENTIFICATION."';".
    " ALK_ROOT_URL = '".ALK_ROOT_URL."';".
    (defined("ALK_URL_SERVEUR_DIST") ? "var ALK_URL_SERVEUR_DIST = '".ALK_URL_SERVEUR_DIST."';" : "") .
    " strTitrePage = \"".$strTitrePage."\";".
    " strTitreAliasPage = \"".$strTitreAliasPage."\";".
    " cont_id = '".$cont_id."';".
    " cont_appli_id = '".$cont_appli_id."';".
    " strDefaultTarget= '".$strDefaultTarget."';".
    
    /** Ajouter constantes javascript sp�cifiques pour l'appli + fonctions appel�es sur onload si besoin */
    ( is_object($oAppli) && method_exists($oAppli, "getHtmlJsInit") 
      ? $oAppli->getHtmlJsInit() 
      : ( function_exists("getHtmlJsInit") ? getHtmlJsInit() : "" )).
    
    " function onLoadInit() {".
    "   bWinLoaded = 1; document.onmousemove = position;".
    /** Actions sp�cifiques � l'appli sur body onload */
    ( is_object($oAppli) && method_exists($oAppli, "getHtmlJsOnLoad") 
      ? $oAppli->getHtmlJsOnLoad() 
      : ( function_exists("getHtmlJsOnLoad") ? getHtmlJsOnLoad() : "" )).
    " } ".
    
    getHtmlAideJS().
    
    "</script>".
    "<script language='javascript' src='../../lib/lib_jsaddon.js'></script>".

    getHtmlMainJs().

    /** inclusioon des librairies js sp�cifiques � l'appli */
    ( is_object($oAppli) && method_exists($oAppli, "getHtmlJsFileSource") 
      ? $oAppli->getHtmlJsFileSource() 
      : (function_exists("getHtmlJsFileSource") ? getHtmlJsFileSource() : "" )).

    "</head>".

    "<body bgcolor='#FFFFFF' text='#000000' ".
    " leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'".
    " onload=\"onLoadMainInit(); onLoadInit();\">".
 
    getHtmlMainTitleBar($cont_id, $cont_appli_id, $strUrlAccueil, $strTitrePhoto, $strTitrePhoto2, $strTitrePage, $strTitreAliasPage, 
                        $iProfilCont, $urlAdminGen, $oSpace->agent_id, $g_strLogin, $strChemin,
                        $_sit_tabMenuEspace, $_sit_tabMenuEspaceFils, $_sit_tabMenu, $_sit_tabMenuFils, $oAppli->oAlkArbre);
  
  return $strHtml;
    
  // fin menu haut - debut du contenu
}

/**
 * @brief Rtourne le code html du bas de la page
 *
 * @return Retourne un string
 */
function getHtmlFooter()
{
  // fin du contenu
  // fermeture de la charte
  $strHtml = getHtmlMainFrameFooter().
    getHtmlMainFooter();

  return $strHtml;
}


/**
 * @brief Retourne la liste des liens a afficher. Classes de style utilisees : menuPath, menuPathBD
 *
 * @param tab     Tableau associatif contenant les liens-texte de navigation
 * @param cont_id Identifiant dy conteneur courant
 * @return Retourne une chaine HTML contenant les liens de navigation
 */
function getLienNav($tab, $cont_id, $strDefaultTarget="")
{
  global $queryCont;
  global $cont_appli_id; 
  global $_sit_urlHomePage;
  global $oSpace;

  $strNav = "";
  $strSepar = " > ";
  
  // ajoute les espaces successifs
  
  $strListeCont="";
  $dsCont = $queryCont->GetDs_ficheEspaceById($cont_id);
  if ($drCont = $dsCont->GetRowIter() ) {
    $strListeCont = $drCont->getValueName("CONT_ARBRE");
    $strListeCont = substr($strListeCont,1,strlen($strListeCont)-2);
    $strListeCont = str_replace("-",",",$strListeCont);
  }
  
  if ($strListeCont != ""){
    $dsCont = $queryCont->GetDs_listeEspaceNavByAgent($_SESSION["sit_idUser"],$strListeCont, 0);
    while( $drCont = $dsCont->GetRowIter() ) {
      $iDroit = $drCont->getValueName("DROIT");
      $icont_id = $drCont->getValueName("CONT_ID");
      $icont_intitule = $drCont->getValueName("CONT_INTITULE");
      $iAppliDefaut = $drCont->getValueName("DEFAUT");
      $idAppliDefaut = $drCont->getValueName("CONT_APPLI_DEFAUT");
      $strUrlAppli = $drCont->getValueName("ATYPE_URL");
      $strTargetAppli = $drCont->getValueName("ATYPE_URL_TARGET");
      $icont_appli_defaut = ( $iAppliDefaut==0 ? 0 : $idAppliDefaut );
      $strTargetAppli = ( $strTargetAppli!="" ? $strTargetAppli : $strDefaultTarget );

      if( $iDroit == 1 ) {
        if( $drCont->getValueName("ATYPE_URL")=="" || $icont_appli_defaut==0 )
          $strUrl = ALK_SIALKE_URL."scripts/".ALK_DIR_ESPACE."sommaire.php?cont_id=".$icont_id;
        else
          if( $oSpace->_testUrlJs($strUrlAppli) )
            $strUrl = $oSpace->_getUrlJS($strUrlAppli, $icont_id, $icont_appli_defaut, "'".$strTargetAppli."'");
          else
            $strUrl = ALK_SIALKE_URL."scripts/".$strUrlAppli.
              "?cont_id=".$icont_id."&cont_appli_id=".$icont_appli_defaut;
      
        $strNav .= "<a target=\"".$strTargetAppli.
          "\" class=\"menuPathBD\" href=\"".$strUrl."\">".$icont_intitule."</a>";   
        if( $cont_id != $icont_id ) 
          $strNav .= $strSepar;
      }
    }
  }

  // ajoute l'arbo des applications
  if( is_array($tab) ) {
    if( count($tab)>0 ) 
      foreach($tab as $t) {
        if( $strNav != "" ) 
          $strNav .= $strSepar;
        if( $t["url"] != "" ) {
          $strTarget = ( array_key_exists("target", $t)==true ? "target=\"".$t["target"]."\"" : "");
          $strNav .= "<a ".$strTarget." class=\"menuPath\" href=\"".$t["url"]."\">".$t["titre"]."</a>";
        } else {
          $strNav .= "<span class=\"menuPath\">".($t["titre"])."</span>";
        }
      }
  } else
    $strNav .= $tab;
  
  return $strNav;
}

///<comment>
///<summary>
/// D�clenche l'ouverture de la popup agenda si l'utilisateur � des �venements le concernant dans l'agenda du conteneur
/// ou si des �venements sont � consulter aujourd'hui
///</summary>
///<param name="cont_id">identifiant du conteneur courant</param>
///<returns>Retourne une chaine contenant le code javascript d�clenchant l'ouverture de la popup</returns>
///</comment>
function AffPopupAgenda($cont_id)
{
  global $conn;
  global $cont_appli_id; 
  global $_sit_urlHomePage;
  
  $strNav = "";
  $tabTemp = $_SESSION["tabAffAgendaPopup"];
  
  $strSql = "select cont_appli_defaut from sit_conteneur ".
    "where cont_id=".$cont_id;
  $rs = initrecordset($strSql, $conn);
  
  if( OCIFetch($rs) ) 
  {
    if (ociresult($rs, "CONT_APPLI_DEFAUT")==$cont_appli_id)
    {
      //construction de la requ�te
      $strFrom = " from sit_appli a, sit_appli_cont ac, sit_appli_type aty, agenda_01_evt ae, agenda_01_evt_agent aea";
      
      $strWhere = " where a.atype_id=aty.atype_id".
        " and a.appli_id=ac.appli_id".
        " and a.appli_id=ae.appli_id".
        " and ae.evt_id=aea.evt_id_evt_agent(+)".
        " and aty.atype_id=5".
        " and ac.cont_id=".$cont_id.
        " and ( to_date(ae.evt_date_debut,'DD/MM/YY')<=to_date('". date('d/m/Y') ."', 'DD/MM/YY')".
        " and to_date(ae.evt_date_fin,'DD/MM/YY')>=to_date('". date('d/m/Y') ."', 'DD/MM/YY') )";
      
      if ( $_SESSION["sit_idUserProfil"] != 1)
      {
        $strFrom .= ", sit_agent_appli aa";
        $strWhere .= " and a.appli_id=aa.appli_id".
        " and (aa.appli_droit_id=1 or aa.appli_droit_id=2)".
        " and aa.agent_id=".$_SESSION["sit_idUser"];;
      }
      
      $strSql = "select distinct ae.evt_id, ae.appli_id, aea.agent_id_evt_agent".
        $strFrom.
        $strWhere;
      //echo $strSql;
      //exit();
      $rsEvt  = initrecordset($strSql, $conn);
      
      $bAgentLie = true;
      $bAffPopup = true;
      $cpt = 0;
      
      // on v�rifie si l'utilisateur n'est pas rattach� � 1 des �venements
      // ou si un �venement est � consulter aujourd'hui dans l'agenda
      // si pour un agenda la popup � d�ja �t� ouverte on ne l'ouvre plus
      while ( OCIFetch($rsEvt) && $bAgentLie && $bAffPopup ) 
      {
        if ($cpt == 0)
        {
          // on ajoute l'identifiant de l'appli agenda dans le tableau
          // on incr�mente la variable cpt pour que l'ajout ne se fasse qu'une seule fois
          if (! in_array(ociresult($rsEvt, "APPLI_ID"), $tabTemp) )
          {
            array_push($tabTemp, ociresult($rsEvt, "APPLI_ID"));
          }
          else
          {
            $bAffPopup = false;
            $strNav = "";
          }
          $cpt = 1;
        }
        
        if ( $bAffPopup )
        {
          // on affecte la variable strNav par d�faut
          $strNav = "OpenPopupAgenda(0,".ociresult($rsEvt, "APPLI_ID").");";
          
          if ( ociresult($rsEvt, "AGENT_ID_EVT_AGENT") == $_SESSION["sit_idUser"]."" )
          {
            $strNav = "OpenPopupAgenda(1,".ociresult($rsEvt, "APPLI_ID").");";
            $bAgentLie = false;
          }
        }
      }
      
      $_SESSION["tabAffAgendaPopup"] = $tabTemp;
    }
  }
  
  return ($strNav);
}


/**
 * @brief Mise � jour des tableaux comptabilisant les connexions
 *        de l'utilisateur aux conteneurs et applications
 *
 * @param type 1=Conteneur 2=application
 * @param id   Identifiant de l'application ou du conteneur
 */
function MajTabStat($type, $id)
{
  global $queryCont, $queryContAnnuAction;
  
  if( $id != "" && $type != "" ) {
    $bStatConnectSIT = ( isset($_SESSION["bStatConnectSIT"]) ? $_SESSION["bStatConnectSIT"] : false );
    $tabStatAccesCont = ( isset($_SESSION["tabStatAccesCont"]) ? $_SESSION["tabStatAccesCont"] : array() );
    $tabStatAccesAppli = ( isset($_SESSION["tabStatAccesAppli"]) ? $_SESSION["tabStatAccesAppli"] : array() );
    
    // on ajoute l'identifiant de l'appli ou du conteneur consult� dans le tableau
    $bStat = false;
    switch( $type ) {
    case 1 : // espace
      if( !array_key_exists("_".$id, $tabStatAccesCont) ) {
        $tabStatAccesCont["_".$id] = true;
        $_SESSION["tabStatAccesCont"] = $tabStatAccesCont;
        $bStat = true;
      }
      break;

    case 2 : // application
      if( !array_key_exists("_".$id, $tabStatAccesAppli) ) {
        $tabStatAccesAppli["_".$id] = true;
        $_SESSION["tabStatAccesAppli"] = $tabStatAccesAppli;
        $bStat = true;
      }
      break;
    }

    if( $bStat == true ) {
      // verification de l'existence d'une occurence dans la base sit_statistique
      $dsTest = $queryCont->GetDs_listeStatsByAgent($_SESSION["sit_idUser"], $type, $id);
      if( $dsTest->iCountTotDr > 0 ) {
        // maj
        $queryContAnnuAction->maj_statsByAgent($_SESSION["sit_idUser"], $type, $id);
      } else { 
        // ajout
        $queryContAnnuAction->add_statsByAgent($_SESSION["sit_idUser"], $type, $id);
      }
    }

    if( $bStatConnectSIT == false ) {
      $_SESSION["bStatConnectSIT"] = true;
      $dsTest = $queryCont->GetDs_listeStatsByAgent($_SESSION["sit_idUser"], 0, 0);
      if( $dsTest->iCountTotDr > 0 ) {
        // maj
        $queryContAnnuAction->maj_statsByAgent($_SESSION["sit_idUser"], 0, 0);
      } else { 
        // ajout
        $queryContAnnuAction->add_statsByAgent($_SESSION["sit_idUser"], 0, 0);
      }
    }
  }
}
?>