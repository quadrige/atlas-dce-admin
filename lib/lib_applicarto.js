///////////////////////////////////////////////////////////
////						D�finition des constantes						  /////
///////////////////////////////////////////////////////////

var oldRadioCheckedPre = null;
var oldRadioCheckedPost = null;
  
var BOX_NONE = 0;
var BOX_CHECK = 1;
var BOX_RADIO = 2;

var ETAT_AUCUN = 0;
var ETAT_TOTAL = 1;
var ETAT_PARTIEL = 2;
var NODE_CLOSED = 0;
var NODE_OPENED = 1;

var tabImg1 = new Array();
tabImg1[NODE_CLOSED] = ALK_URL_SI_IMAGES+'icon_arbo_open.png'; 
tabImg1[NODE_OPENED] = ALK_URL_SI_IMAGES+'icon_arbo_close.png';

var tabImg2 = new Array();
tabImg2[ETAT_AUCUN] = ALK_URL_SI_IMAGES+'checkbox_notchecked.png'; 
tabImg2[ETAT_TOTAL] = ALK_URL_SI_IMAGES+'checkbox_checked.png'; 
tabImg2[ETAT_PARTIEL] = ALK_URL_SI_IMAGES+'checkbox_partialchecked.png';     

/**
 * @constructor TreeManager : permet de g�rer l'arbre
 * @param String  name : nom de l'arbre
 */
TreeManager.prototype.constructor = TreeManager;
function TreeManager(name){
  this.name = name;
  this.oRoot = new Tree(this, null, name, "", "", BOX_NONE, BOX_NONE, false, false, false, false, "", "", "", "", true);
  this.oRoot.mng_name = this.name;
  this.nodes = new Array();
  this.nodes[name] = this.oRoot;
  this.openedNode = new Array(name);
  this.is_drawing = false;
  this.iMaxNiv = 0;
  
  
  this.pre_box = BOX_NONE;
  this.post_box = BOX_NONE;
  this.oTable = null;
  
  this.pre_radio_select = null;
  this.post_radio_select = null;
}

TreeManager.prototype.AddNode = 
 function( parent_name, name, text, icone, 
           pre_box, post_box, pre_editable, post_editable, pre_selected, post_selected, 
           action, cssText, cssLink, cssLinkVisited, is_visible, actionOnCheck, actionOnRadio, piece_jointe )
{
    
  this.pre_box = Math.max(this.pre_box, pre_box);
  this.post_box = Math.max(this.post_box, post_box);
  var oTree = new Tree(this, null, name, text, icone, 
                       pre_box, post_box, pre_editable, post_editable, pre_selected, post_selected, 
                       action, cssText, cssLink, cssLinkVisited, is_visible, actionOnCheck, actionOnRadio, piece_jointe);
  oTree.mng_name = this.name;
  oTree.oTable = this.oTable;
  if (this.nodes[parent_name]){
    this.nodes[parent_name].AddChild(oTree);
  }
  else {
    this.oRoot.AddChild(oTree);
  }
  this.nodes[name] = oTree;
  this.iMaxNiv = Math.max(this.iMaxNiv, oTree.iNiv);
  return oTree;
}

TreeManager.prototype.SetTableMain = function(oTable){
  this.oTable = oTable;
  for (var tree in this.nodes){
    this.nodes[tree].oTable = oTable;
  }
}

TreeManager.prototype.UpdateMaxNiveau = function(){
  for (var tree in this.nodes){
    this.nodes[tree].iMaxNiv = this.iMaxNiv;
  }  
}

TreeManager.prototype.Draw = function(){
  this.UpdateMaxNiveau();
  if (this.is_drawing) return;
  if (document.getElementById(this.name))
   this.oTable = document.getElementById(this.name);
  else{
    this.oTable = document.getElementById(this.name);
  }
  this.SetTableMain(this.oTable);
  this.oRoot.CheckStateOpen();
  this.oRoot.Draw();
  this.is_drawing = true;
  this.oRoot.SelectNodeRoot(this.oRoot.name, true, true);
  this.oRoot.SelectNodeRoot(this.oRoot.name, false, true);
  this.oRoot.DisabledRadioRoot(true, true);
  this.oRoot.DisabledRadioRoot(false, true);
}

/**
 * @constructor Tree : D�finit un noeud de l'arbre qui est lui meme une racine d'arbre
 */
Tree.prototype.constructor = Tree;
function Tree( oManager, oParent, name, text, icone,
               pre_box, post_box, pre_editable, post_editable, pre_selected, post_selected,
               action, cssText, cssLink, cssLinkVisited, is_visible, actionOnCheck, actionOnRadio, piece_jointe)
{
  this.oManager = oManager;
  this.oParent = oParent;
  if (this.oParent) this.iNiv = this.oParent.iNiv + 1;
  else this.iNiv = 0;
  this.name = name;
  this.text = text;
  this.action = action;
  this.icone = icone;
  this.pre_box = pre_box;
  this.post_box = post_box;
  this.pre_editable = pre_editable;
  this.post_editable = post_editable;
  this.pre_selected = pre_selected;
  this.post_selected = post_selected;
  this.is_selected = (this.pre_selected || this.post_selected);
  this.is_open = is_visible;
  this.is_visible = (is_visible || this.is_selected);
  this.actionOnCheck = actionOnCheck;
  this.actionOnRadio = actionOnRadio;
  this.is_drawing = false;
  this.children = new Array();
  this.length = 0;
  this.nbSelectedPre = 0;(this.pre_selected ? 1 : 0);
  this.nbSelectedPost = 0;(this.post_selected ? 1 : 0);
  this.oTable = null;
  this.iMaxNiv = 0;
  this.etat_pre = ETAT_AUCUN;
  this.etat_post = ETAT_AUCUN;
  this.mng_name = "";
  this.cssText = cssText;
  this.cssLink = cssLink;
  this.cssLinkVisited = cssLinkVisited;
  this.radio_pre = null;
  this.radio_post = null;
  this.nb_child_open = 0;
  this.piece_jointe = piece_jointe;
}

Tree.prototype.AddChild = function(oTree){
  if (this.children[oTree.name]) return;
  this.children[oTree.name] = oTree;
  if (!oTree.pre_selected && this.etat_pre==ETAT_TOTAL) this.etat_pre = ETAT_PARTIEL;
  if (oTree.pre_selected) this.nbSelectedPre++;
  if (!oTree.post_selected && this.etat_post==ETAT_TOTAL) this.etat_post = ETAT_PARTIEL;
  if (oTree.post_selected) this.nbSelectedPost++;
  this.length++;
  if (this.nbSelectedPre==this.length) this.etat_pre = ETAT_TOTAL;
  if (this.nbSelectedPost==this.length) this.etat_post = ETAT_TOTAL;
  oTree.oParent = this;
  oTree.iNiv = this.iNiv + 1;
}

Tree.prototype.OpenNode = function(){
  if (arguments.length==0 && this.is_open) return;
  this.is_open = true;
  if (document.getElementById("imgopen_"+this.mng_name+"_"+this.name))
    document.getElementById("imgopen_"+this.mng_name+"_"+this.name).src = tabImg1[NODE_OPENED];
  for (var child in this.children){
    this.children[child].is_visible = true;
    this.children[child].DrawOpenClose();
  }
  if (arguments.length>0 && arguments[0]) this.Draw();
}

Tree.prototype.CloseNode = function(){
  if (arguments.length==0 && !this.is_open) return;
  this.is_open = false;
  if (document.getElementById("imgopen_"+this.mng_name+"_"+this.name))
    document.getElementById("imgopen_"+this.mng_name+"_"+this.name).src = tabImg1[NODE_CLOSED];
  for (var child in this.children){
    this.children[child].is_visible = false;
    this.children[child].DrawOpenClose();
    this.children[child].CloseNode(true);
  }
  if (arguments.length>0 && arguments[0]) this.Draw();
}

Tree.prototype.ShowHideNode = function(){
  if (this.is_open) this.CloseNode();
  else this.OpenNode();
}

Tree.prototype.DoOnclickRadio = function(oRadio){
  if (!oRadio.execute) return;
  var elements = document.getElementsByTagName("input");
  var bFound = false;
  
  var radio = new Object();
  radio.list = new Array();
  radio.length = 0;
  radio.add = function(element){
  	this.list[this.length] = element;
  	this.length++;
  }
  radio.item = function(index){
  	if (index>=0 && index<this.length){
  		return this.list[index];
  	}
  	return null;
  }
  for (var i=0; i<elements.length; i++){
  	var element = elements[i];
    if (element.type=="radio"){
      if (element.name.indexOf(oRadio.name)!=-1){
      	radio.add(element);
      }
    }
  }
  for (var i=0; i<radio.length; i++){
    radio.item(i).checked = (radio.item(i) == oRadio);
  }
}

Tree.prototype.SelectNode = function(is_pre, is_init){
  this.oManager.oRoot.SelectNodeRoot(this.name, is_pre, is_init);
  this.oManager.oRoot.DisabledRadioRoot(is_pre, is_init);
}

/**
 * @brief etablit la s�lection de l'arbre entier
 */
Tree.prototype.SelectNodeRoot = function(name, is_pre, is_init, next_state){
  if (this.isInSelection) return;
  this.isInSelection = true;
  
  if (arguments.length<2) is_init = false;
  if ( typeof is_init == "undefined" ) is_init = false;
  
  if (name==this.name && !is_init){
    if (is_pre){
      if (this.etat_pre==ETAT_PARTIEL) next_state = ETAT_TOTAL;
      else next_state = (this.etat_pre+1)%2;      
    }
    else {
      if (this.etat_post==ETAT_PARTIEL) next_state = ETAT_TOTAL;
      else next_state = (this.etat_post+1)%2;         
    }
  }
  this.nbSelectedPre = 0;
  this.nbSelectedPost = 0;
  for (var child in this.children){
    var oTree = this.children[child];
    oTree.SelectNodeRoot(name, is_pre, is_init, next_state); 
    if (oTree.etat_pre!=ETAT_AUCUN) this.nbSelectedPre++;
    if (oTree.etat_post!=ETAT_AUCUN) this.nbSelectedPost++;
  }
  
  if (is_pre){
    if (typeof next_state != "undefined" && next_state != "undefined"){
      this.etat_pre = next_state;
    }
    else if (is_init){
      if (this.length==0) this.etat_pre = (this.pre_selected ? ETAT_TOTAL : ETAT_AUCUN);
      else if (this.nbSelectedPre==this.length) this.etat_pre = ETAT_TOTAL;
      else if (this.nbSelectedPre==0) this.etat_pre = ETAT_AUCUN;
      else this.etat_pre = ETAT_PARTIEL;
    }
    else if (name==this.name){
      if (this.etat_pre==ETAT_PARTIEL) this.etat_pre = ETAT_TOTAL;
      else this.etat_pre = (this.etat_pre+1)%2;      
    }
    else {
      if (this.length==0) this.etat_pre = this.etat_pre;
      else if (this.nbSelectedPre==this.length) this.etat_pre = ETAT_TOTAL;
      else if (this.nbSelectedPre==0) this.etat_pre = ETAT_AUCUN;
      else this.etat_pre = ETAT_PARTIEL;      
    }
    if (document.getElementById('pre_selection_'+this.mng_name+'['+this.name+']'))
      document.getElementById('pre_selection_'+this.mng_name+'['+this.name+']').value = (this.etat_pre!=ETAT_AUCUN ? "1" : "0");
  }
  else {
    if (typeof next_state != "undefined" && next_state != "undefined"){
      this.etat_post = next_state;
    }
    else if (is_init){
      if (this.length==0) this.etat_post = (this.post_selected ? ETAT_TOTAL : ETAT_AUCUN);
      else if (this.nbSelectedPost==this.length) this.etat_post = ETAT_TOTAL;
      else if (this.nbSelectedPost==0) this.etat_post = ETAT_AUCUN;
      else this.etat_post = ETAT_PARTIEL;
    }
    else if (name==this.name){
      if (this.etat_post==ETAT_PARTIEL) this.etat_post = ETAT_TOTAL;
      else this.etat_post = (this.etat_post+1)%2;      
    }
    else {
      if (this.length==0) this.etat_post = this.etat_post;
      else if (this.nbSelectedPost==this.length) this.etat_post = ETAT_TOTAL;
      else if (this.nbSelectedPost==0) this.etat_post = ETAT_AUCUN;
      else this.etat_post = ETAT_PARTIEL;      
    }
    if (document.getElementById('post_selection_'+this.mng_name+'['+this.name+']'))
      document.getElementById('post_selection_'+this.mng_name+'['+this.name+']').value = (this.etat_post!=ETAT_AUCUN ? "1" : "0");    
  }
  
  this.DrawSelection(is_pre, is_init, name);
  
  this.isInSelection = false;
}

Tree.prototype.DrawSelection = function(is_pre, is_init, selection){
  var img = document.getElementById("imgcheck"+(is_pre ? "pre" : "post")+this.mng_name+'_'+this.name);
  if (!img) return;
  img.src = tabImg2[(is_pre ? this.etat_pre : this.etat_post)];
  
  /*
  var hasRadio = (is_pre && this.pre_box==BOX_CHECK && this.post_box==BOX_RADIO) || (!is_pre && this.pre_box==BOX_RADIO && this.post_box==BOX_CHECK);
  if (hasRadio) this.DisabledRadio(is_pre, is_init, selection);
  else {
    for (var child in this.children){
      var oTree = this.children[child];
      oTree.DisabledRadio(is_pre, is_init, oTree.name);
    }    
  }*/
}

Tree.prototype.DisabledRadioRoot = function(is_pre, is_init){
  this.DisabledRadioChild(is_pre, is_init); 
}

Tree.prototype.DisabledRadioChild = function(is_pre, is_init){
  this.DisabledRadio(is_pre, is_init); 
  for (var child in this.children){
    var oTree = this.children[child];
    oTree.DisabledRadioChild(is_pre, is_init); 
  }
}

Tree.prototype.DisabledRadio = function(is_pre, is_init){
  var elements = document.getElementsByTagName("input");
  var bDisabled = eval("this.etat_"+(is_pre ? "pre" : "post")+"==ETAT_AUCUN");
  var bFound = false;
  
  var radio = new Object();
  radio.list = new Array();
  radio.length = 0;
  radio.add = function(element){
  	this.list[this.length] = element;
  	this.length++;
  }
  radio.item = function(index){
  	if (index>=0 && index<this.length){
  		return this.list[index];
  	}
  	return null;
  }
  for (var i=0; i<elements.length; i++){
  	var element = elements[i];
    if (element.type=="radio"){
      if (element.name.indexOf((is_pre ? "post" : "pre")+"_selection_"+this.mng_name)!=-1){
      	radio.add(element);
      }
    }
  }
  
  for (var i=0; i<radio.length; i++){
    if (radio.item(i).value == this.name){
      radio.item(i).disabled = bDisabled;
      if ( radio.item(i).checked && bDisabled){        
        if (!is_init) radio.item(i).checked = false;
        
        var j=i;
        var cpt = 0;
        while (!bFound && cpt!=radio.length){
          if (!radio.item(j).disabled){            
            if (!is_init) radio.item(j).checked = true; 
            radio.item(j).execute = false;  
            if (radio.item(j).onclick)
              radio.item(j).onclick();  
            radio.item(j).execute = true;          
            bFound = true;
          }
          j = (j+1)%(radio.length);
          cpt++;
        }  
      }
    }
  }
}
Tree.prototype.CheckStateOpen = function(){
  var isOpen = this.is_open;
  var nbOpen = 0;
  var nbChildRoot = 0;
  for (var child in this.children){
    this.children[child].CheckStateOpen();
    var childOpen = (this.children[child].is_visible || this.children[child].is_open);
    isOpen = isOpen && childOpen;
    if (childOpen) nbOpen++;
    if (this.children[child].length!=0)
     nbChildRoot++;
  }
  if (this.length==0) this.is_open = this.is_visible;
  else {
    this.is_open = nbOpen>0 || (nbChildRoot==0 && this.is_open);
    this.is_visible = this.is_visible || this.is_open;
    
    if (nbOpen>0){
      for (var child in this.children){
        this.children[child].is_visible = true;
      }
    }
  }
}

Tree.prototype.Draw = function(){
  if (this.is_drawing){
    this.DrawOpenClose();
    return;
  }
  if (!this.oTable) return;
  
  if (this.iNiv==0) {
    this.is_drawing = true;  
  
    var nbColBef = 0;
    var nbColAft = 0;
    if (this.oManager.pre_box!=BOX_NONE)
      nbColBef++;
    if (this.oManager.post_box!=BOX_NONE)
      nbColAft++;
    var tr = this.oTable.insertRow(this.oTable.rows.length);
    for (var i=0; i<this.iMaxNiv+2+nbColBef; i++){
      var td = tr.insertCell(i);
      td.width = '16px';
      td.style.fontSize = '0px';
      td.style.height = '0px';
    }
    
    var td = tr.insertCell(this.iMaxNiv+2+nbColBef);
    td.width = '100%';
    td.style.fontSize = '0px';
    td.style.height = '0px';
    
    for (var i=0; i<nbColAft; i++){
      var td = tr.insertCell(i);
      td.width = '16px';
      td.style.fontSize = '0px';
      td.style.height = '0px';
    }
    
    for (var child in this.children){
      this.children[child].Draw();
    }
    return;
  }
  
  var nbCol = 0;
  var name = this.mng_name + "_" + this.name;
  
  var tr = this.oTable.insertRow(this.oTable.rows.length);
  tr.id = "node_"+name;
  
  for (var i=0; i<this.iNiv; i++){
    var td = tr.insertCell(tr.cells.length);
  }
  
  var td = tr.insertCell(tr.cells.length);
  td.style.textAlign = 'center';
    td.style.fontSize = '0';
  
  if (this.length>0){
    var a = document.createElement("a");
    a.href = "javascript:ShowHideNodeArbo('"+this.mng_name+"', '"+this.name+"')";
    
    var imgOpen = document.createElement("img");
    imgOpen.id = "imgopen_"+name;
    imgOpen.style.border = 'none';
    imgOpen.src = tabImg1[NODE_CLOSED];
    a.appendChild(imgOpen);
    a.style.width = imgOpen.width;
    a.style.height = imgOpen.height;
    td.appendChild(a);
  }
  
  if (this.pre_box==BOX_CHECK){
    var td = tr.insertCell(tr.cells.length);
    td.style.fontSize = '0';
    td.style.textAlign = 'center';
    var img = document.createElement("img");
    img.id = "imgcheckpre"+name;
    img.style.border = 'none';
    img.src = tabImg2[ETAT_AUCUN];
    img.oTree = this;
    if (this.pre_editable){
      img.onmouseover = function(){
        this.style.cursor = "hand"; 
        if (this.style.cursor!="hand") 
          this.style.cursor="pointer";
      }
      img.actionOnClick = this.actionOnCheck;
      img.onclick = function(){
       this.oTree.SelectNode(1);
       if (this.actionOnClick!="")
         eval(this.actionOnClick+"(this)");
     }
    }
    else {
     img.onclick = function(){
       alert("Cette s�lection n'est pas modifiable");
     }      
    }
    var hidden = document.createElement("input");
    hidden.type = 'hidden';
    hidden.name = 'pre_selection_'+this.mng_name+'['+this.name+']';
    hidden.value = (this.pre_selected ? 1 : 0);
    hidden.id = 'pre_selection_'+this.mng_name+'['+this.name+']';
    td.appendChild(img);
    td.appendChild(hidden);
  }
  else if (this.pre_box==BOX_RADIO){
    var td = tr.insertCell(tr.cells.length);
    td.style.textAlign = 'center';
    td.style.fontSize = '0';
    var radio = document.createElement("input");
    radio.setAttribute('readonly', !this.pre_editable);
    radio.type = 'radio';
    radio.name = 'pre_selection_'+this.mng_name;
    radio.value = this.name;
    radio.oTree = this;
    radio.execute = true;
    if (this.actionOnRadio!=""){
      radio.actionOnClick = this.actionOnRadio;
      radio.onclick = function(){
        eval(this.actionOnClick+"(this)");
        if (oldRadioCheckedPre!=null)
          oldRadioCheckedPre.checked = false;
        this.checked = true;
        oldRadioCheckedPre = this;
      }
    }
    else {
    	radio.onclick = function(){
    	  this.oTree.DoOnclickRadio(this);
    	}
    }
    td.appendChild(radio);
    radio.disabled = false;
    if (this.post_box==BOX_CHECK)
      radio.disabled = !this.post_selected;
    radio.disabled |= !this.pre_editable;
    radio.checked = this.pre_selected;
    if (radio.checked){
      oldRadioCheckedPre = radio;
    }
  }
  else {
    nbCol++;
  }
  
  if (this.icone!=""){
    var td = tr.insertCell(tr.cells.length);
    td.style.textAlign = 'left';
    td.style.fontSize = '0';
    td.width = 16;
    var img = document.createElement("img");
    img.style.border = 'none';
    img.src = this.icone;
    td.appendChild(img);
  }  
  else {
    nbCol++;
  }

  var td = tr.insertCell(tr.cells.length);
  td.colSpan = this.iMaxNiv - this.iNiv + nbCol + 1 ;
  if ( this.piece_jointe!="" ){
    td.innerHTML = this.piece_jointe+"&nbsp;";
  }
  
  if (this.action && this.action!=""){
    var a = document.createElement("a");
    a.id = "link_"+name;
    a.className = (this.is_selected ? this.cssLinkVisited : this.cssLink);
    a.href = (this.action.indexOf('javascript:')!=-1 ? "javascript:SelectNodeTree('"+this.mng_name+"', '"+this.name+"')" : this.action);
    a.innerHTML = "<nobr>"+this.text+"</nobr>";
    td.appendChild(a);
  }
  else {
    td.className = this.cssText;
    td.innerHTML += "<nobr>"+this.text+"</nobr>";
  }

  
  if (this.post_box==BOX_CHECK){
    var td = tr.insertCell(tr.cells.length);
    td.style.textAlign = 'center';
    var img = document.createElement("img");
    img.id = "imgcheckpost"+name;
    img.style.border = 'none';
    img.src = tabImg2[ETAT_AUCUN];
    img.oTree = this;
    if (this.post_editable){
      img.onmouseover = function(){
        this.style.cursor = "hand"; 
        if (this.style.cursor!="hand") 
          this.style.cursor="pointer";
      }
      img.actionOnClick = this.actionOnCheck;
      img.onclick = function(){
       this.oTree.SelectNode(0);
       if (this.actionOnClick!="")
         eval(this.actionOnClick+"(this)");
     }
    }
    else {
     img.onclick = function(){
       alert("Cette s�lection n'est pas modifiable");
     }      
    }
    var hidden = document.createElement("input");
    hidden.type = 'hidden';
    hidden.name = 'post_selection_'+this.mng_name+'['+this.name+']';
    hidden.value = (this.post_selected ? 1 : 0);
    hidden.id = 'post_selection_'+this.mng_name+'['+this.name+']';
    td.appendChild(img);
    td.appendChild(hidden);
  }
  else if (this.post_box==BOX_RADIO){
    var td = tr.insertCell(tr.cells.length);
    td.style.textAlign = 'center';
    td.style.width = '5';
    var radio = document.createElement("input");
    radio.setAttribute('readonly', !this.post_editable);
    radio.setAttribute('type', 'radio');
    radio.name = 'post_selection_'+this.mng_name;
    radio.value = this.name;
    radio.oTree = this;
    radio.is_pre = false;
    radio.execute = true;
    if (this.actionOnRadio!=""){
      radio.actionOnClick = this.actionOnRadio;
      radio.onclick = function(){
        eval(this.actionOnClick+"(this)");
        if (oldRadioCheckedPost!=null)
          oldRadioCheckedPost.checked = false;
        this.checked = true;
        oldRadioCheckedPost = this;
      }
    }
    else {
    	radio.onclick = function(){
    	  this.oTree.DoOnclickRadio(this);
    	}
    }
    td.appendChild(radio);
    radio.disabled = false;
    if (this.pre_box==BOX_CHECK)
      radio.disabled = !this.pre_selected;
    radio.disabled |= !this.post_editable;
    radio.checked = this.post_selected;
    if (radio.checked){
      oldRadioCheckedPost = radio;
    }
  }
  
  this.DrawOpenClose();
  this.is_drawing = true;  
  for (var child in this.children){
    this.children[child].Draw();
  }
}

Tree.prototype.DrawOpenClose = function(){
  var name = this.mng_name + "_" + this.name;
  if (document.getElementById("imgopen_"+name))
    document.getElementById("imgopen_"+name).src = tabImg1[(this.is_open ? NODE_OPENED : NODE_CLOSED)];
  if (!this.is_open){     
    this.CloseNode();
  }
  document.getElementById("node_"+name).style.display = (this.is_visible ? '' : 'none');
}

function SelectNodeTree(mngTreeName, nodeName){
  var oTreeMng = eval("oTreeMng_"+mngTreeName);
  if (!oTreeMng) return;
  var oTreeSelected = null;
  for (var node in oTreeMng.nodes){
    var oTree = oTreeMng.nodes[node];
    var oLink = document.getElementById("link_"+mngTreeName+"_"+node);
    if (oLink && (oTree.is_selected || node==nodeName)){
      oTree.is_selected = (node==nodeName);
      if (oTree.is_selected) oTreeSelected = oTree;
      oLink.className = (oTree.is_selected ? oTree.cssLinkVisited : oTree.cssLink);
    }    
  }
  if (oTreeSelected){
    var action = oTreeSelected.action;
    action = action.replace("javascript:", "");
    eval(action);
  }
}
function ShowHideNodeArbo(mngTreeName, nodeName){
  var oTreeMng = eval("oTreeMng_"+mngTreeName);
  if (!oTreeMng) return;
  var oTreeSelected = null;
  for (var node in oTreeMng.nodes){
    var oTree = oTreeMng.nodes[node];
    if (node==nodeName){
      oTree.ShowHideNode();
    }
  }
}