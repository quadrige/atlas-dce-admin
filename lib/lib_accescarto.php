<?php
/*
 * Created on 31 oct. 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
define("BOX_NONE", 0);
define("BOX_CHECK", 1);
define("BOX_RADIO", 2);
$TAB_BOX = array(BOX_NONE=>"BOX_NONE", BOX_CHECK=>"BOX_CHECK", BOX_RADIO=>"BOX_RADIO", );

class AlkRequestDefinition{
  var $name;
  var $tabType = array();
  var $primary_key = "";
  var $value = -1;
  var $text = "";
  
  var $is_visible;
  var $is_open;
  
  var $pre_box;
  var $post_box;
  
  var $pre_values;
  var $post_values;
  
  var $pre_values_nonedit;
  var $post_values_nonedit;
  
  var $action = "";
  var $actionOnCheck = "";
  var $actionOnRadio = "";
  var $cssText = "divContenuTexte";
  var $cssLink = "divContenuTexte";
  var $cssLinkVisited = "divContenuTexteGras";
  
  var $strEvalOpen;
  var $strEvalVisible;
  
  function AlkRequestDefinition($name, $primary_key, $tabType=array(), $value=-1){
    $this->name = $name;
    $this->tabType = $tabType;
    $this->primary_key = $primary_key;
    $this->value = $value;
    $this->is_visible = true;
    $this->is_open = true;
    $this->pre_box = BOX_NONE;
    $this->post_box = BOX_NONE;
    $this->pre_values = array();
    $this->post_values = array();
    $this->pre_values_nonedit = array();
    $this->post_values_nonedit = array();
    
    $this->strEvalOpen = "";
    $this->strEvalVisible = "";
  }
  
  function SetBoxes($pre_box, $post_box){
    $this->pre_box = $pre_box;
    $this->post_box = $post_box;
  }
  
  function SetValues($pre_values, $post_values){
    $this->pre_values = $pre_values;
    $this->post_values = $post_values;
  } 
  
  function SetNonEditablesValues($pre_values, $post_values){
    $this->pre_values_nonedit = $pre_values;
    $this->post_values_nonedit = $post_values;
  } 
  
  function isSelected($bPre, $value){
    if ($bPre)
      return (is_array($this->pre_values) && in_array($value, $this->pre_values) ? "true" : "false");
    return (is_array($this->post_values) && in_array($value, $this->post_values) ? "true" : "false");
  }
  
  function GetAction($identifiant){
    $action = $this->action;
    if ($this->action!=""){
      if (!preg_match("!javascript:!", $this->action)){
        $action = $this->action.$identifiant;
      }
      else {
        $action = preg_replace("!(\);?$)!", "'".$identifiant."'$1", $this->action);
      }
    }
    return $action;
  }
  
  function isEditable($bPre, $value){
    if ($bPre)
      return (in_array($value, $this->pre_values_nonedit) ? "false" : "true");
    else
      return (in_array($value, $this->post_values_nonedit) ? "false" : "true");        
  }
  
  function GetInfoText($identifiant){
    return "\"".$this->GetAction($identifiant)."\", '".$this->cssText."', '".$this->cssLink."', '".$this->cssLinkVisited."', ";
  }
  
  function GetEqualValue(){
    if (is_array($this->value)){
      return " in ('".implode("', '", $this->value)."')";
    }
    return "=".$this->value;      
  }
  
  function HasValue(){
    if (is_array($this->value)){
      return !empty($this->value);
    }
    return $this->value!="-1";      
  }
  
  function GetHtmlNode(&$bContinue, $name, $pere, $ident, $strIntitule, $strIcone, $value, $is_open=true, $strPJ=""){
    global $TAB_BOX;

    if ($this->strEvalVisible!=""){
      if ( !eval($this->strEvalVisible) ){
        $bContinue = true;
        return "";
      }
    }
    if ($this->strEvalOpen!=""){
      $is_open = $is_open && eval($this->strEvalOpen);
    } 
    $bContinue = false;
    return "oTreeMng_".$name.".AddNode('".$pere."', '".$ident."'" .
                ", \"".addslashes($strIntitule)."\", '".$strIcone."'," .
                $TAB_BOX[$this->pre_box].", ".$TAB_BOX[$this->post_box].
                ", ".$this->isEditable(true, $value).", ".$this->isEditable(false, $value).
                ", ".$this->isSelected(true, $value).", ".$this->isSelected(false, $value).
                ", ".$this->GetInfoText($value)." ".($this->is_open && $is_open ? "true" : "false").
								", '".$this->actionOnCheck."'" .
							  ", '".$this->actionOnRadio."'" .
							  ", '".addslashes($strPJ)."');\n";
  }
  
}

function Tree($name, &$oAppli, &$oRubrique, &$oDroitAgent, &$oCarte, &$oCouche, $oDefault=null, $oAtlas=null, $oProjet=null, $oCoucheDoc=null){
  global $dbConn;
  global $nbLignesTree;
  global $oSpace;
  if (!isset($nbLignesTree)) $nbLignesTree = 0;
  
  $formName = "";
  $strHtml = "";  
  $strHtml .= "<script language='javascript'>" .
    "var ALK_URL_SI_IMAGES = '".ALK_URL_SI_IMAGES."';</script>";
  $strHtml .= "<script language='javascript' src='../../lib/lib_applicarto.js'></script>";
  
  $strFromCarte = " SIT_APPLI a" .
    " left join FDOC_01_RUBRIQUE r on ( a.APPLI_ID = r.APPLI_ID_FDOC_RUB )" .
    " left join FDOC_01_DROIT agent on ( agent.RUB_ID_DROIT = r.RUB_ID )" .
    " left join FDOC_01_THEME t on ( t.RUB_ID = r.RUB_ID )" .
    " left join FDOC_01_DOC carte on ( carte.THEME_ID = t.THEME_ID ) ".
    " left join SIG_01_PROJET p on ( p.DOC_ID=carte.DOC_ID )" .
    " left join SIG_01_COUCHE co on (co.DOC_ID=carte.DOC_ID)";
  $strFromCouche = " SIT_APPLI a" .
    " left join FDOC_01_RUBRIQUE r on ( a.APPLI_ID = r.APPLI_ID_FDOC_RUB )" .
    " left join FDOC_01_DROIT agent on ( agent.RUB_ID_DROIT = r.RUB_ID )" .
    " left join FDOC_01_THEME t on ( t.RUB_ID = r.RUB_ID )" .
    " left join FDOC_01_DOC couche on ( couche.THEME_ID = t.THEME_ID ) ";
  $strWhereCarte = "carte.DOC_CORBEILLE=0";
  $strWhereCouche = "couche.DOC_CORBEILLE=0";
  
  $tabCond = array("a."=>$oAppli, "r."=>$oRubrique, "agent."=>$oDroitAgent, "carte."=>$oCarte, "couche."=>$oCouche);
  
  if (!is_null($oProjet)){
    $tabCond["p."] = $oProjet;
  }
  if (!is_null($oCoucheDoc)){
    $tabCond["co."] = $oCoucheDoc;
  }

  if (!is_null($oAtlas)){
    $tabCond[$oAtlas->name."."] = $oAtlas;
    switch ($oAtlas->name){
      case "atlas" :
        $strFromCarte .= " LEFT join ATLAS_01_ATLAS_DOC atlas on (atlas.DOC_ID=carte.DOC_ID)";
      break;
      case "geoloc" :
        $strFromCarte .= " LEFT join GEOLOC_01_PARAMETRAGE geoloc on (geoloc.PZONAGEDOC_ID=carte.DOC_ID and geoloc.FDOC_APPLI_ID=a.APPLI_ID and geoloc.RUB_ID=r.RUB_ID)";
      break;
      default :
        unset($tabCond[$oAtlas->name."."]);
      break;
    }
  }
  
  foreach ($tabCond as $prefixe=>$oObject){
    if (!is_null($oObject)){
      foreach ($oObject->tabType as $type=>$tabType){
        if (!is_array($tabType)) $tabType = array($tabType);
        if ($oObject->name!="couche")
          $strWhereCarte .= " and ".$prefixe.$type." in (".implode(", ", $tabType).")";
        
        if ($oObject->name!="carte" && $oObject->name!="p" && $oObject->name!="co" && (!is_null($oAtlas) ? $oObject->name!=$oAtlas->name : true))
          $strWhereCouche .= " and ".$prefixe.$type." in (".implode(", ", $tabType).")";
      }
      
      if ($oObject->HasValue() && $oObject->primary_key!=""){
        if ($oObject->name!="couche")
          $strWhereCarte .= " and ".$prefixe.$oObject->primary_key.$oObject->GetEqualValue();
        if ($oObject->name!="carte"  && (!is_null($oAtlas) ? $oObject->name!=$oAtlas->name : true) )
          $strWhereCouche .= " and ".$prefixe.$oObject->primary_key.$oObject->GetEqualValue();
      }
    }
  }  
  
  $tabMatch = array();
  if (!is_null($oAppli) && $oAppli->is_visible){
    $appli_action = $oAppli->action;
    if (preg_match("!\\$\w+!", $oAppli->action, $tabMatch)){
      if (isset($tabMatch[0])) {
        $oAppli->action = eval("return \"".$oAppli->action."\";");
      }
    }
    
    $strHtml .= "<table id='".$name."' border='0' cellspacing='2' cellpadding='1' width='100%' style='z-index:1000'></table>";
    $strHtml .= "<script language='javascript'>";
    $strHtml .= "function DrawTree_".$name."(){" .
        " oTreeMng_".$name." = new TreeManager('".$name."');\n ";
    $pere = "root";
    if (!is_null($oDefault)){  
      $nbLignesTree++;    
      $bContinue = false;
      $strHtml .= $oDefault->GetHtmlNode($bContinue, $name, $pere, $oDefault->primary_key, $oDefault->text, "", $oDefault->value);
      
    }
    $strSql = "select distinct a.APPLI_ID, a.APPLI_INTITULE from ".$strFromCarte." where ".$strWhereCarte .
        " union select distinct a.APPLI_ID, a.APPLI_INTITULE from ".$strFromCouche." where ".$strWhereCouche;
    $dsAppli = $dbConn->InitDataset($strSql);
    if ($dsAppli->bEof) return "";
    while ($drAppli = $dsAppli->GetRowIter()){
      $appli_id = $drAppli->GetValueName("APPLI_ID");
      $appli_intitule = $drAppli->GetValueName("APPLI_INTITULE");
      
      $pere1 = "appli_".$appli_id;
      $nbLignesTree++;    
      
      $strIcone = ALK_ROOT_URL.ALK_URL_SI_IMAGES."icon_dossier_cant_open.png";
      $bContinue = false;
      $strHtml .= $oAppli->GetHtmlNode($bContinue, $name, $pere, $pere1, $appli_intitule, $strIcone, $appli_id);
      if ($bContinue) continue;
      
      $tabPere = array("appli_".$appli_id);
      $tabNiv = array();
      if (!is_null($oRubrique) && $oRubrique->is_visible){
        $rub_action = $oRubrique->action;
        if (preg_match("!\\$\w+!", $oRubrique->action, $tabMatch)){
          if (isset($tabMatch[0])) {
            $oRubrique->action = eval("return \"".$oRubrique->action."\";");
          }
        }
        
        $strSql = "select distinct r.RUB_ID, RUB_INTITULE, r.RUB_PERE, RUB_NIVEAU from ".$strFromCarte.
            " where ".$strWhereCarte ." and a.".$oAppli->primary_key."=".$appli_id.
            " order by RUB_PERE, RUB_NIVEAU, RUB_RANG";

        $rub_niveau = 1;
        $dsRub = $dbConn->InitDataset($strSql);
        while ($drRub = $dsRub->GetRowIter()){
          $rub_pere = "rub_".$drRub->GetValueName("RUB_PERE");
          $rub_niveau = "rub_".$drRub->GetValueName("RUB_NIVEAU");
          $rub_id = $drRub->GetValueName("RUB_ID");
          $rub_intitule = $drRub->GetValueName("RUB_INTITULE");
          
          $strSql = "select distinct r.RUB_ID, r.RUB_INTITULE, r.RUB_NIVEAU, r.RUB_PERE" .
              " from FDOC_01_RUBRIQUE r, FDOC_01_RUBRIQUE r2".
              " where r2.RUB_ARBRE  like ".$dbConn->GetConcat("'%-'", "r.RUB_ID", "'-%'").
              " and r2.RUB_ID=".$rub_id.
              " and r.RUB_NIVEAU>1" .
              " and r2.RUB_ID<>r.RUB_ID" .
              " order by r.RUB_NIVEAU, r.RUB_RANG";
              
          $dsRubP = $dbConn->InitDataset($strSql);
          
          if (in_array($rub_pere, $tabPere)){
            $pere1 = $rub_pere;
            $tabNiv["rub_".$rub_id] = $tabNiv[$pere1]+1;
          }
          else if ($dsRubP->bEof){
            $pere1 = "appli_".$appli_id;
            $tabNiv["rub_".$rub_id] = 1;
          }
          else {
            while ($drRubP = $dsRubP->GetRowIter()){
              $rubp_intitule = $drRubP->GetValueName("RUB_INTITULE");
              $rubp_id = $drRubP->GetValueName("RUB_ID");
              $rubp_pere = "rub_".$drRubP->GetValueName("RUB_PERE");
              $rubp_niveau = $drRubP->GetValueName("RUB_NIVEAU");
              
              if (in_array("rub_".$rubp_id, $tabPere) ){
                continue;
              }
              else if ($rubp_niveau==2){
                $ppere1 = "appli_".$appli_id;
              }
              else{
                $ppere1 = $rubp_pere;
              }
              $tabNiv["rub_".$rubp_id] = "rub_".$rubp_niveau;
              $ppere2 = "rub_".$rubp_id;
              $tabPere[] = $ppere2;
              $strIcone = ALK_ROOT_URL.ALK_URL_SI_IMAGES."icon_dossier_cant_open.png";       
               
              $bContinue = false;      
              $strHtml .= $oRubrique->GetHtmlNode($bContinue, $name, $ppere1, $ppere2, $rubp_intitule, $strIcone, $rubp_id, true);
              if ($bContinue) continue;
              
              $pere1 = "rub_".$rubp_id;
              $tabNiv["rub_".$rub_id] = $rubp_niveau+1;
            }
          }
          
          $nbLignesTree++;
                        
          $strIcone = ALK_ROOT_URL.ALK_URL_SI_IMAGES."icon_dossier_cant_open.png";
          
          $pere2 = "rub_".$rub_id;
          $tabPere[] = $pere2;
          
          $bSeeAll = isset($oRubrique->bSeeAll) && $oRubrique->bSeeAll;
          
          $bContinue = false;
          $strHtml .= $oRubrique->GetHtmlNode($bContinue, $name, $pere1, $pere2, $rub_intitule, $strIcone, $rub_id, ($bSeeAll ? true : ($tabNiv["rub_".$rub_id]==1)));
          if ($bContinue) continue;
                
          if (!is_null($oCarte) && $oCarte->is_visible){
            $carte_action = $oCarte->action;
            if (preg_match("!\\$\w+!", $oCarte->action, $tabMatch)){
              if (isset($tabMatch[0])) {
                $oCarte->action = eval("return \"".$oCarte->action."\";");
              }
            }
            
            $strSql = "select distinct carte.DOC_ID, carte.DOC_INTITULE, p.PROJET_ID, carte.DOC_TYPE " .
                " from ".$strFromCarte.
                " left join SIG_01_COUCHE_PROJET cp on cp.PROJET_ID=p.PROJET_ID".
                " left join SIG_01_COUCHE c on (cp.COUCHE_ID=c.COUCHE_ID)" .
                ($oCouche->is_visible ? " , (select couche.DOC_ID from ".$strFromCouche." where ".$strWhereCouche.") couche " : "") .
                " where ".$strWhereCarte. 
                ($oCouche->is_visible ? " and c.DOC_ID=couche.DOC_ID" : "") .
                " and r.".$oRubrique->primary_key."=".$rub_id .
                " and a.".$oAppli->primary_key."=".$appli_id ;

            $dsDoc = $dbConn->InitDataset($strSql);
            while ($drDoc = $dsDoc->GetRowIter()){
              $doc_id = $drDoc->GetValueName("DOC_ID");
              $doc_intitule = $drDoc->GetValueName("DOC_INTITULE");
              $projet_id = $drDoc->GetValueName("PROJET_ID");
              $doc_type = $drDoc->GetValueName("DOC_TYPE");    
              
              $strPj="";
		          if(defined("ALK_SIG_B_ATLAS_JOINDATA") && ALK_SIG_B_ATLAS_JOINDATA){
		            $dsPj = $dbConn->InitDataset("select  pj.PJ_ID_FDOC_PJ as pj_id, pj.PJ_NOM_FDOC_PJ as filename,".
		                    $dbConn->GetSubstring("pj.PJ_NOM_FDOC_PJ", "2+length(".$dbConn->GetConcat("pj.DOC_ID", "pj.PJ_ID_FDOC_PJ").")")." as filename_aff, LG_ID".
		                    " from FDOC_01_PJ pj where DOC_ID = ".$doc_id);
		            while ($drPj = $dsPj->getRowIter()){
		            
		              $pj_id = $drPj->getValueName("pj_id");
		              $pjfile = $drPj->getValueName("filename");
		              $pjfile_aff = $drPj->getValueName("filename_aff");
		              $icon = _GetFileIcon(ALK_SIALKE_PATH.ALK_PATH_UPLOAD_DOC_IMG.$pjfile);
		              $strPj .="<a href='javascript:Telecharger(".$oSpace->cont_id.", ".$appli_id.", ".$doc_id.", ".$pj_id.")'>" .
		                                 "<img src='".ALK_URL_SI_IMAGES.$icon."' title='T�l�charger ".$pjfile_aff."' border='0' height='16' width='16'/></a>";
		            
		            	
		            }
		          }    
              
              $strIcone = ALK_ROOT_URL.ALK_URL_SI_IMAGES."icon_doc_8".$doc_type.".gif";
              $pere3 = "doc_".$doc_id;
              
              $nbLignesTree++;
              
              $bContinue = false;
              $strHtml .= $oCarte->GetHtmlNode($bContinue, $name, $pere2, $pere3, $doc_intitule, $strIcone, $doc_id, true, $strPj);
              //$strHtml .= $strPj;
              
              
              if ($bContinue) continue;
          
              if (!is_null($oCouche) && $oCouche->is_visible){
                $couche_action = $oCouche->action;
                if (preg_match("!\\$\w+!", $oCouche->action, $tabMatch)){
                  if (isset($tabMatch[0])) {
                    $oCouche->action = eval("return \"".$oCouche->action."\";");
                  }
                }
                $strSql = "select distinct ".$dbConn->GetConcat("'".$doc_id."_'", "couche.DOC_ID")." DOC_ID, couche.DOC_INTITULE, couche.DOC_TYPE " .
                    " from ".$strFromCouche.
                    " left join SIG_01_COUCHE c on (couche.DOC_ID=c.DOC_ID)" .
                    " left join SIG_01_COUCHE_PROJET cp on (c.COUCHE_ID=cp.COUCHE_ID)" .
                    " where ".$strWhereCouche .
                    " and cp.PROJET_ID=".$projet_id .
                  //  " and a.".$oAppli->primary_key."=".$appli_id.
                    " order by cp.COUCHE_RANG" ;
                    
                $dsCouche = $dbConn->InitDataset($strSql);
                while ($drCouche = $dsCouche->GetRowIter()){
                  $couchedoc_id = $drCouche->GetValueName("DOC_ID");
                  $couchedoc_intitule = $drCouche->GetValueName("DOC_INTITULE");
                  $couchedoc_type = $drCouche->GetValueName("DOC_TYPE");
                  
                  
                  
                  $strIcone = ALK_ROOT_URL.ALK_URL_SI_IMAGES."icon_doc_8".$couchedoc_type.".gif";
              
                  $nbLignesTree++;
                  
                  $pere4 = "cch_".$couchedoc_id;
                  $bContinue = false;
                  $strHtml .= $oCouche->GetHtmlNode($bContinue, $name, $pere3, $pere4, $couchedoc_intitule, $strIcone, $couchedoc_id);
                  if ($bContinue) continue;
                  
                }//while Couche
                $oCouche->action = $couche_action;  
              }//if Couche
            }//while Carte            
            $oCarte->action = $carte_action;   
          }//if Carte
        }//while rub
        $oRubrique->action = $rub_action; 
      }//if rub      
    }//while appli
    $strHtml .= "oTreeMng_".$name.".Draw(); }";
    $strHtml .= "</script>";
    $oAppli->action = $appli_action; 
  }//if appli
  else return "";
  return $strHtml;
}

 /** @brief Retourne le nom de l'image icone en fonction de l'extension du fichier
   *
   * @param strFileName nom du fichier
   * @return Nom du fichier icone
   */
  function _GetFileIcon($strFileName)
  {
    // icone par d�faut
    $strIcon = "icon_doc_0.gif";
    $iFWidth = strlen($strFileName);
    if( $iFWidth > 2 ) {
      $iPos = strrpos($strFileName, ".");
      if( !($iPos===false) ) {
        $strExt = strtolower(substr($strFileName, $iPos+1));
      
        switch( $strExt ) {
        case "xls": case "csv":  $strIcon = "icon_doc_1.gif"; break;
        case "doc": case "rtf":  $strIcon = "icon_doc_2.gif"; break;
        case "pdf":              $strIcon = "icon_doc_3.gif"; break;
        case "png": case "tif":
        case "jpg": case "gif":
        case "bmp": case "jpeg": $strIcon = "icon_doc_4.gif"; break;
        case "txt":              $strIcon = "icon_doc_5.gif"; break;
        case "htm": case "html": $strIcon = "icon_doc_6.gif"; break;
        case "zip":              $strIcon = "icon_doc_7.gif"; break;
        case "svg": case "jsvg": 
        case "js":  case "frm":   
        case "style":            $strIcon = "icon_doc_8.gif"; break;
        case "ppt":              $strIcon = "icon_doc_10.gif"; break;
        case "mdb":              $strIcon = "icon_doc_11.gif"; break;
        case "mp3": case "mp2":
        case "mp4": case "mpg": 
        case "mpeg":case "avi":  
        case "wma": case "wmv":
        case "midi":case "mid":
        case "rmi": case "wav":
        case "m3u": case "asf":  $strIcon = "icon_doc_12.gif"; break;
        case "ram": case "rm":   $strIcon = "icon_doc_13.gif"; break;
        case "mov":              $strIcon = "icon_doc_14.gif"; break;
        }
      }
    }
    return $strIcon;
  }


function DataDeploy(&$strScript, $tabData, $tabRub, $iMode, $iMaxNiveau, $default_doc, $doc_id, $width, $iRubDep=0, $iNivPere=0){
  global $bWithJavascript;
  if (!isset($bWithJavascript)) $bWithJavascript = true;
  $strHtml = "";
  if (!(array_key_exists($iRubDep, $tabData)) && is_array($tabData[$iRubDep])) return $strHtml;
  foreach ($tabData[$iRubDep] as $fils=>$tabFils){
    if ($iMode==1 && !in_array("r_0", $doc_id) && !in_array($fils, $doc_id)) continue;
    $bIsDoc = preg_match("!d_\d+!", $fils);
    $bRadio = ($bIsDoc || $tabRub["r_".$iRubDep]>0 || $tabRub[$fils]>0);
    $id = $tabFils["id"];
    $iNiveau = (array_key_exists("niv", $tabFils) ? $tabFils["niv"] : $iNivPere);
    $text = $tabFils["text"];
    $strData = "\ntabData['".$fils."']";
    if ($iMode==0)
      $strScript .= $strData." = new Object();" .
        $strData.".isDirectory = ".($bIsDoc ? "false" : "true").";" .
        ($bRadio ? $strData.".index = tabData.length;tabData.length++;" : "").
        $strData.".pere = 'r_".$iRubDep."';" .
        $strData.".etat = ETAT_AUCUN;" .
        $strData.".node = NODE_OPENED;" .
        $strData.".fils = new Array();" .
        $strData.".filsSelected = new Array();" .
        $strData.".nbFilsSelect = 0;" .
        "if(tabData['r_".$iRubDep."']){" .
          " tabData['r_".$iRubDep."'].fils.push('".$fils."');" .
          " tabData['r_".$iRubDep."'].filsSelected['".$fils."'] = false;" .
          " tabData['r_".$iRubDep."'].nbFils = tabData['r_".$iRubDep."'].fils.length;" .
        "};";
        
    if ($bRadio)
      $oRadio = new HtmlRadio($iMode, "default_doc", $fils, $default_doc);
    else 
      $oRadio = new Html("&nbsp;");

    $strIcone = ALK_ROOT_URL.ALK_URL_SI_IMAGES.($bIsDoc ? "icon_doc_83.gif" : "icon_dossier_cant_open.png");
    $oHideCheck = new HtmlHidden("doc_selected[".$fils."]", (in_array($fils, $doc_id) ? 1 : 0));
    $oHideCheck->AddHidden("rub_id[".$fils."]", ($bIsDoc ? $iRubDep : $id));
    $strHtml .= "<tr>" .
        ($bIsDoc ? "" : "<td width='20' height='0'></td>") .
      //  ($iMode==0 ? "<td width='20'></td>" : "") .
        "<td  width='15'"./*width='".($width-15-(!$bIsDoc ? -20 : 0)-($iMode==0 ? 20 :0)-($iNiveau*20))."'*/"></td>" .
        "<td colspan='".(2*$iMaxNiveau-$iNiveau)."'></td>" .
        "</tr>";
    $strHtml .= "<tr>" .
        ($bIsDoc 
        ? "" 
        : "<td width='20'>" .
            ($bWithJavascript ? "<a href='javascript:ShowHideNode(".$id.")'>" : "").
            "<img align=absmiddle border=0 id='img1_".$fils."' src='".ALK_ROOT_URL.ALK_URL_SI_IMAGES."icon_arbo_close.png'>" .
            ($bWithJavascript ? "</a>" : "").
          "</td>") .
        ($iMode==0 
          ? "<td width='20'>&nbsp;<a href=\"javascript:SelectNode('".$fils."');\">".
          "<img align=absmiddle border=0 id='img2_".$fils."' src='".ALK_ROOT_URL.ALK_URL_SI_IMAGES."checkbox_notchecked.png'></a></td>"
        : "") .
        "<td class='divContenuTexte' colspan='".($iMaxNiveau+$iNiveau)."'>" .($iMaxNiveau." ".$iNiveau)." ".($iMaxNiveau-$iNiveau).
            "\t<img align=absmiddle border=0 src='".$strIcone."'>".
          ($iMode==0 ? $oHideCheck->GetHtml() : "")."&nbsp;".$text."</td>" .
        "<td width='15' align='right'>\t" .$oRadio->GetHtml() ."</td>" .
        "</tr>";
    if (array_key_exists($id, $tabData) && !$bIsDoc){
      $strHtml .= "<tr id='table_".$id."'><td width='20'>a&nbsp;</td>".
      "<td colspan='".($iMaxNiveau+$iNiveau)."'>";
      $strHtml .= "<table border='1' cellspacing='0' cellpadding='0' width='100%'>";
      $strHtml .= DataDeploy($strScript, $tabData, $tabRub, $iMode, $iMaxNiveau, $default_doc, $doc_id, $width, $id, $iNiveau+1);
      $strHtml .= "</table>";
      $strHtml .= "</td></tr>";
    }
  }
  return $strHtml;
}


function GetHtmlArboCartoDefine(&$strScript, $tabData, $tabRub, $iMode, $iMaxNiveau, $default_doc, $doc_id, $width="", $iRubDep=0, $iNivPere=0){
  global $formName;
  global $bWithJavascript;
  if (!isset($bWithJavascript)) $bWithJavascript = true;
  
  $oRadio = new HtmlRadio($iMode, "default_doc", "r_0", $default_doc);
  
  $strHtml = "";
  if ($bWithJavascript){
    $strHtml .= "<script language='javascript'>" .
      "var ALK_URL_SI_IMAGES = '".ALK_URL_SI_IMAGES."';var formName = '".$formName."';</script>";
    $strHtml .= "<script language='javascript' src='../../lib/lib_applicarto.js'></script>";
  }
  
  $width = ($width=="" ? "575" : $width);
  
  $strHtml .= "<table border='0' cellspacing='0' cellpadding='0' width='".$width."'>";
    $strHtml .= "<tr>" .
        "<td width='20' height='0'></td>" .
        ($iMode==0 ? "<td width='20'></td>" : "") .
        "<td width='".($width+5-($iMode==0 ? 20 :0))."'></td>" .
        "<td width='15'></td>" .
        "</tr>";
  $strHtml .= "<tr>" .
      "<td width='20'>&nbsp;</td>" .
      ($iMode==0 ? "<td width='20'>&nbsp;</td>" : "") .
      "<td class='divContenuTexte'>&nbsp;Aucun document</td>" .
      "<td width='15'>" .$oRadio->GetHtml() ."</td>" .
      "</tr>";      
  $strHtml .= DataDeploy($strScript, $tabData, $tabRub, $iMode, $iMaxNiveau, $default_doc, $doc_id, $width, $iRubDep, $iNivPere);
  $strHtml .= "</table>";
  return $strHtml;
}


function DocsDeploy(&$strTabBody, &$strScript, $tabData, $tabRub, $iMaxNiveau, $default_doc, $doc_id, $profil_id, $tabDroits, $width, $iRubDep=0, $iNivPere=0){
  if (!isset($bWithJavascript)) $bWithJavascript = true;
  $strHtml = "";
  $strCss = "Pair";
  if (!(array_key_exists($iRubDep, $tabData)) && is_array($tabData[$iRubDep])) return $strHtml;
  
  $bFound = false;
  reset($tabData);
  while ( !$bFound && current($tabData) ){
    $bFound = (key($tabData)==$iRubDep);
    if (!$bFound) next($tabData);
    else break;
  }
  $bHasNext = (next($tabData)!==false);

  foreach ($tabData[$iRubDep] as $fils=>$tabFils){
    if (!in_array("r_0", $doc_id) && !in_array($fils, $doc_id)) continue;
    $bIsDoc = preg_match("!d_\d+!", $fils);
    $bRadio = ($bIsDoc || $tabRub["r_".$iRubDep]>0 || $tabRub[$fils]>0);
    $id = $tabFils["id"];
    $iNiveau = (array_key_exists("niv", $tabFils) ? $tabFils["niv"] : $iNivPere);
    $text = $tabFils["text"];
    
    if ($bIsDoc){
      $oCheckDoc = new HtmlCheckbox(0, "doc_all[".$id."]", "1");
      $oCheckDoc->AddEvent("onclick", "CheckLine(this, ".$id.")");
      $strScript .= "tabDocIds.push(".$id.");";
      $strCss = "Pair";
      $strCssProf = "Pair";
    }
    else {
      $strCss = "Impair";
      $strCssProf = "Impair";
    }
    $strIcone = ALK_URL_SI_IMAGES.($bIsDoc ? "icon_doc_83.gif" : "icon_dossier_cant_open.png");
    
    $strTabBody .= "<tr>";
    $w = intval($width/count($profil_id))+5;
    foreach ($profil_id as $idProfil){
      if (!array_key_exists($idProfil, $tabDroits)) continue;
      $oCheck = new HtmlCheckbox(0, "select[".$id."][".$idProfil."]", (in_array($id, $tabDroits[$idProfil]) ? 1 : 0));
      $oCheck->AddEvent("onclick", "CheckOne(this)");
      $strTabBody .= "<td height='21' width='".$w."' class='td".$strCssProf."1' align='center'>".
                    ($bIsDoc ? $oCheck->GetHtml() : "&nbsp;")."</td>";
      $strTabBody .= "<td width='2'></td>";
    }
    $strTabBody .= "</tr>";
    if ( $bHasNext )
      $strTabBody .= "<tr><td height='2' colspan='".(count($profil_id)*2)."'></td></tr>";
 
    $strHtml .= "<tr>" .
        str_repeat("<td>&nbsp;</td>", $iNiveau-1) .
        "<td width='20'><img align=absmiddle border=0 src='".$strIcone."'></td>" .
        "<td class='td".$strCss."1' colspan='".($bIsDoc ? ($iMaxNiveau-$iNiveau+2) : ($iMaxNiveau-$iNiveau+2))."'><span class='divContenuTexte'>".$text."</span></td>" .
        "<td width='5'></td>" .
        "<td width='20' class='td".($bIsDoc ? "Entete" : $strCss)."1'>".($bIsDoc ? $oCheckDoc->GetHtml() : "&nbsp;")."</td>" .
        "<td width='5'></td>" .
        "</tr>";
    
    if ( $bHasNext )
      $strHtml .= "<tr><td height='2' colspan='".($iNiveau+4)."'></td></tr>";

    if (array_key_exists($id, $tabData) && !$bIsDoc){
      $strHtml .= "<tr id='table_".$id."'>";
      $strHtml .= DocsDeploy($strTabBody, $strScript, $tabData, $tabRub, $iMaxNiveau, $default_doc, $doc_id, $profil_id, $tabDroits, $width, $id, $iNiveau+1);
      $strHtml .= "</tr>";
    }
  }
  return $strHtml;
}

function GetHtmlArboCarto($dsDocInAppli, $cont_id, $cont_appli_id, $doc_id){
  global $strCss;
  global $queryFdoc;
  global $querySigConsult;
  
  $atlas_id = 0;
  $tabRub = array();
  $tabDoc = array();
  while ( $drDoc = $dsDocInAppli->GetRowIter() ){
    $rubPere = $drDoc->GetValueName("RUB_PERE");
    $rubNiveau = $drDoc->GetValueName("RUB_NIVEAU");
    $iMaxNiveau = max($iMaxNiveau, $rubNiveau);
    $is_default = $drDoc->GetValueName("IS_DEFAULT");
    $atlas_id = $drDoc->GetValueName("ATLAS_ID");
    $doc_id = $drDoc->GetValueName("DOC_ID");
    $rub_id = $drDoc->GetValueName("RUB_ID");
    
    $tabDoc[] = $doc_id;
    $tabRub[] = $rub_id;
        
  }
  
  $oDroitAgent = new AlkRequestDefinition("agent", "AGENT_ID_FDOC_DROIT", array("DROIT_TYPE_FDOC"=>array("1", "2", "4")), $_SESSION["sit_idUser"]);
  
  $oAppli = new AlkRequestDefinition("a", "APPLI_ID", array("ATYPE_ID"=>array("4", "51")), $cont_appli_id);
  $oAppli->SetBoxes(BOX_NONE, BOX_NONE);
  
  $oRubrique = new AlkRequestDefinition("r", "RUB_ID", array(), $tabRub);
  $oRubrique->SetBoxes(BOX_NONE, BOX_NONE);
  
  $oCarte = new AlkRequestDefinition("carte", "DOC_ID", array("DOC_TYPE"=>array(ALK_TFDOC_FDPSIG, ALK_TFDOC_PSIG, ALK_TFDOC_CZONSIG)), $tabDoc);
  $oCarte->SetValues(array($doc_id), array());
  $oCarte->cssLinkVisited = "PtTxtBleu";
  $oCarte->SetBoxes(BOX_NONE, BOX_NONE);
  $oCarte->action = "javascript:AffCarte(\$rub_id, )";
  
  $oCouche = new AlkRequestDefinition("couche", "DOC_ID", array("DOC_TYPE"=>array(ALK_TFDOC_CSIG, ALK_TFDOC_CZONSIG)));
  $oCouche->SetBoxes(BOX_NONE, BOX_NONE);
  $oCouche->is_visible = false;
  
  
  $oSIGAPPLI = new AlkRequestDefinition("atlas", "ATLAS_ID", array(), $atlas_id);
    
  
  $iMode = 0;
  $strHtml = Tree("tableDocs", $oAppli, $oRubrique, $oDroitAgent, $oCarte, $oCouche, null, $oSIGAPPLI);
  $strHtml .= "<script language='javascript'>" .
      "DrawTree_tableDocs();".
      "</script>";
  
  return $strHtml;
}

function GetHtmlArboSelect($dsAppliFdoc, $tabDocs, $formName, $parametrage_type, $bAffCouches=false){
  global $queryFDoc;
  global $querySig;
  global $suffixe;
  $strScript = "";
  $strScript2 = "";
  $suffixe = $parametrage_type;
  $doc_id = array("r_0");
  $width = "300";
  $iMode = 0;
  $default_doc = -1;
  $strHtml = "";
  $strHtml .= "<table border='0' cellspacing='0' cellpadding='0' width='".$width."'>";
  $strHtml .= "<tr>" .
      "<td width='20' height='0'></td>" .
      ($iMode==0 ? "<td width='20'></td>" : "") .
      "<td width='".($width+5-($iMode==0 ? 20 :0))."'></td>" .
      "</tr>";
  
  while ( $drAppli = $dsAppliFdoc->GetRowIter() ){
    $appli_id = $drAppli->GetValueName("APPLI_ID");
    $cont_id = $drAppli->GetValueName("CONT_ID");
    
    $dsNoeud = $queryFDoc->GetDs_listeRubriqueByAppliForArbo($appli_id);
    $dsFeuille = $queryFDoc->GetDs_listeDocsRech($_SESSION["sit_idUser"], $cont_id, $appli_id);
    
    $strParamBefore = $parametrage_type."_".$appli_id."_";
    
    $tabNiveaux = array();
    $tabData[0] = array();
    $tabRub[0] = 0;
    $tabPere = array();
    $tabPere[0] = array(0);
    while( $drNoeud = $dsNoeud->getRowIter() ) {
      $idFils = $drNoeud->getValueName("ID");
      $doc_id[] = "r_".$idFils;
      $idPere = $drNoeud->getValueName("ID_PERE");
      $iNiv   = $drNoeud->getValueName("NIV");
      $strNom = $drNoeud->getValueName("NOM");
      if ($strNom=="Couches") continue;
      $strUrl = "";
      $iDroitLink = 0;
      $iDroitAction = 0;
      
      $tabNiveaux[] = $iNiv;
      if (!array_key_exists($idPere, $tabPere)){
        $tabPere[$idFils] = array($idPere, $idFils);
      }
      else {
        $tabPere[$idFils] = array_merge($tabPere[$idPere], array($idFils));
      }
      if (!array_key_exists($idPere, $tabData))
        $tabData[$idPere] = array();
      $tabData[$idPere]["r_".$strParamBefore.$idFils] = array("id"=>$strParamBefore.$idFils, "text"=>$strNom, "niv"=>$iNiv,);
      $tabRub[$idFils] = 0;
      
    }
    $iMaxNiveau = count(array_unique($tabNiveaux))+1;
    
    while( $drFeuille = $dsFeuille->getRowIter() ) {
      $id = $drFeuille->getValueName("DOC_ID");
      $doc_id[] = "d_".$id;
      $idNoeud = $drFeuille->getValueName("RUB_ID");
      $strNom = $drFeuille->getValueName("DOC_INTITULE");
      $strUrl = "javascript:AffInfo(".$cont_id.", ".$appli_id.", ".$idNoeud.", ".$id.")";
      $strIcone = "0";
      $doc_type = $drFeuille->getValueName("DOC_TYPE");
      if ($doc_type==ALK_TFDOC_PSIG || $doc_type==ALK_TFDOC_FDPSIG){
        $tabData[$strParamBefore.$idNoeud]["d_".$strParamBefore.$id] = array("id"=>$strParamBefore.$id, "text"=>$strNom);
        if ($bAffCouches){
          $tabParam["doc_id"] = $id;          
          $dsCouchesProjet = $querySig->getDs_listeCouchesProjet($tabParam);
          while ( $drCouche = $dsCouchesProjet->GetRowIter() ){
            $couchedoc_type = $drCouche->GetValueName("DOC_TYPE");
            if ($couchedoc_type!=ALK_TFDOC_CZONSIG) continue;
            $couchedoc_id           = $drCouche->getValueName("DOC_ID");
            $doc_intitule     = $drCouche->getValueName("DOC_INTITULE");
            $type_couche_nom  = $drCouche->getValueName("TYPE_COUCHE_NOM");
            if ($type_couche_nom!="ALK_SIG_TYPE_COUCHE_POLYGONE") continue;
            
            $strParamBefore2 = $strParamBefore.$id."_";
            
            $tabData["d_".$strParamBefore.$id]["c_".$strParamBefore2.$couchedoc_id]
              = array("id"=>$strParamBefore2.$couchedoc_id, "text"=>$doc_intitule);
            if (array_key_exists($couchedoc_type, $tabDocs) && in_array($id."_".$couchedoc_id, $tabDocs[$couchedoc_type]))
              $strScript2 .= "SelectNode('c_".$strParamBefore2.$couchedoc_id."', '".$parametrage_type."');";
          }
          if (!array_key_exists("d_".$strParamBefore.$id, $tabData)){
            unset($tabData[$strParamBefore.$idNoeud]["d_".$strParamBefore.$id]);
            continue;
          }            
        }        
        
        foreach ($tabPere[$idNoeud] as $idPere){
          if (!array_key_exists($idPere, $tabRub))
            $tabRub[$idPere] = 0;
          $tabRub[$idPere] = $tabRub[$idPere] + 1;
        }       
        if (array_key_exists($doc_type, $tabDocs) && in_array($id, $tabDocs[$doc_type]))
          $strScript2 .= "SelectNode('"."d_".$strParamBefore.$id."', '".$parametrage_type."');";
      }      
    }
    foreach ($tabRub as $rub_id=>$nb_cartes){
      if ($nb_cartes==0 && array_key_exists($rub_id, $tabData)){
        foreach ($tabPere[$rub_id] as $idPere){
          if (array_key_exists($idPere, $tabData))
            unset($tabData[$idPere]["r_".$rub_id]);
          if ($idPere==$rub_id) unset($tabData[$rub_id]);
        }
        unset($tabRub[$rub_id]);
      }
    }
    if (count($tabData)>0){
      $strHtml .= DataDeploy(&$strScript, $tabData, $tabRub, $iMode, $iMaxNiveau, $default_doc, $tabDocs, $width, false);
    }
  }
  $tabScript2 = explode(";", $strScript2);
  $tabScript2 = array_unique($tabScript2);
  $strScript2 = implode(";", $tabScript2);
  $strHtml .= "</table>";
  $strHtml .= "<script language='javascript'>" .$strScript. $strScript2. "</script>";
  return $strHtml;
}

function GetServeurCarto($serveur_carto){
  if (defined ("ALK_SIG_SYNDIC_HTTP")&& ALK_SIG_SYNDIC_HTTP){
  	//if ((strpos($serveur_carto, "https://")===true))
      $serveur_carto = str_replace("https://", "http://", $serveur_carto);
  }else{
    if ((strpos($serveur_carto, "http://")===false) && (strpos($serveur_carto, "https://")===false))
      $serveur_carto = "http://".$serveur_carto;
  }
  return $serveur_carto;
}

function CompressSigAppliIdAndProfilId($sigappli_id, $profil_id){
  $rand = rand(2, 9);
  $rand2 = pow($rand, 2);
  while (preg_match("!".$rand2."!", ($sigappli_id*$rand)) || preg_match("!".$rand2."!", ($profil_id*$rand))){
    $rand = rand(2, 9);
    $rand2 = pow($rand, 2);
  }
  return $rand.(($sigappli_id * $rand)).$rand2.(($profil_id * $rand));
}

function DecompressSigAppliIdAndProfilId($cpt_id){
  $rand = intval(substr($cpt_id, 0, 1));
  if ($rand==0)
    return array(-1, -1);
  $tmp = intval(substr($cpt_id, 1));
  $tmp = explode(pow($rand, 2), $tmp);
  $sigappli_id = $tmp[0]/$rand;
  $synd_profil_id = $tmp[1]/$rand;
  return array($sigappli_id, $synd_profil_id);
}

function GetJsFunctionSyndic($doc_id=0, $bRedirect=false){
  $strJs = "<script language='javascript'>\n" .
    "  function OpenCartoExterne(h, a){\n" .
    "    var H = 670; var W = 1000;\n" .
    "    var T = (screen.height - H) / 2;\n" .
    "    var L = (screen.width - W) / 2;\n" .
    "    var wind = window.open(".(!$bRedirect ? GetUrlOpenSig("h+'", "'+a", $doc_id) : GetUrlRedirection("h+'", "'+a", $doc_id)).", 'sig_site',\n" .
    "      'status=yes,scrollbars=no,resizable=no,menubar=no,'+\n" .
    "      'height='+H+',width='+W+',top='+T+',left='+L);\n" .
    "    wind.focus();\n" .
    "    return (wind ? true : false);\n" .
    "  }\n" .
    "</script>";
  return $strJs;
}

function GetJsCallSyndic($serveur_carto, $computed_sigappli_id, $bIgnore=true, $doc_id=0){
  return ($bIgnore ? "void(" : "")."OpenCartoExterne('".$serveur_carto."', ".$computed_sigappli_id.")".($bIgnore ? ")" : "").";";
}

function GetUrlOpenSig($serveur_carto, $computed_sigappli_id, $doc_id=0){
  return $serveur_carto."scripts/atlas/open_sig.php?a=".$computed_sigappli_id.($doc_id!=0 ? "+'0000".$doc_id."'" : "");
}

function GetUrlRedirection($serveur_carto, $computed_sigappli_id, $doc_id=0){
  return $serveur_carto."/scripts/atlas/00_atlas.phtml?open_sig=".$computed_sigappli_id.($doc_id!=0 ? "+'&doc_id=".$doc_id."'" : "");
}

function GetCartoLink($sigappli_intitule, $href_url, $target_url){
  return "<a href=\"".$href_url."\">\n".
          $sigappli_intitule." (".ALK_APP_TITLE.")" .
          "\n</a>";
}
?>
