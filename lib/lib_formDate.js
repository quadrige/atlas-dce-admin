function alertMsgDate(Ctrl, strMsg)
{
  alert(strMsg);
  Ctrl.value = "";
  Ctrl.focus();
  return;
}

function isDateValidJJMMAAAA(strDate) 
{
  if( strDate.length != 10 ) return false;
  var bOk = false;
  var bBisex = false;
  var dd = new Number(strDate.substring(0, 2));
  var mm = new Number(strDate.substring(3, 5));
  var aaaa = new Number(strDate.substring(6, 10));
  if( strDate.charAt(2) != "\/" || strDate.charAt(5) != "\/" ) 
    bOk = false;
  else
    if( aaaa.toString() == "NaN" || aaaa<1900 || aaaa>2500 ) 
      bOk = false;
    else {
      if( (aaaa%4==0 && aaaa%100!=0) || (aaaa%400==0 && aaaa%100==0) ) bBisex = true;
      if( mm.toString() == "NaN" || mm<1 || mm>12 )
        bOk = false;
      else
        if( dd.toString() == "NaN" ||
          (dd<1 || dd>31) ||
          ((dd<1 || dd>30) && (mm==4 || mm==6 || mm==9 || mm==11)) ||
          ((dd<1 || dd>29) && mm==2 && bBisex==true) ||
          ((dd<1 || dd>28) && mm==2 && bBisex==false) )
          bOk = false;
        else
          bOk = true;
    }
  delete dd;
  delete mm;
  delete aaaa;
  return bOk;
}
function isDateValidJJMM(strDate) 
{
  if( strDate.length != 5 ) return false;
  var bOk = false;
  var dd = new Number(strDate.substring(0, 2));
  var mm = new Number(strDate.substring(3, 5));
  if( strDate.charAt(2) != "\/" ) 
    bOk = false;
  else {
    if( mm.toString() == "NaN" || mm<1 || mm>12 )
      bOk = false;
    else {
      if( dd.toString() == "NaN" ||
        (dd<1 || dd>31) ||
        ((dd<1 || dd>30) && (mm==4 || mm==6 || mm==9 || mm==11)) ||
        ((dd<1 || dd>29) && mm==2) )
        bOk = false;
      else
        bOk = true;
    }
  }
  delete dd;
  delete mm;
  return bOk;
}
function isDateValidMMAAAA(strDate) 
{
  if( strDate.length != 7 ) return false;
  var bOk = false;
  var mm = new Number(strDate.substring(0, 2));
  var aaaa = new Number(strDate.substring(3, 7));
  if( strDate.charAt(2) != "\/" ) 
    bOk = false;
  else {
    if( mm.toString() == "NaN" || mm<1 || mm>12 )
      bOk = false;
    else if( aaaa.toString() == "NaN" || aaaa<1900 || aaaa>2500 ) 
      bOk = false;
    else 
      bOk = true;
  }
  delete aaaa;
  delete mm;
  return bOk;
}
function isMonthValidMM(strMonth) 
{
  if( strMonth.length != 2 ) return false;
  var bOk = false;
  var mm = new Number(strMonth);
  if( mm.toString() == "NaN" || mm<1 || mm>12 )
	bOk = false;
  else 
	bOk = true;
  delete mm;
  return bOk;
}
function isYearValidAAAA(strYear) 
{
  if( strYear.length != 4 ) return false;
  var bOk = false;
  var aaaa = new Number(strYear);
  if( aaaa.toString() == "NaN" || aaaa<1900 || aaaa>2500 ) 
    bOk = false;
  else 
    bOk = true;
  delete aaaa;
  return bOk;
}
function ControleDate(Objet, lg)
{
  var strDate = Objet.value;
  if( strDate == "" )
    return true;
  if( lg == 5 ) {
    if( isDateValidJJMM(strDate) )
      return true;
    else {
      alertMsgDate(Objet, "La date doit �tre dans un format JJ/MM valide !");
      return false;
    }
  } else {
    if( isDateValidJJMMAAAA(strDate) )
      return true;
    else {
      alertMsgDate(Objet, "La date doit �tre dans un format JJ/MM/AAAA valide !");
      return false;
    }
  }
}


function isHeureValideHHMN(strHeure) 
{
  if( strHeure.length != 5 ) return false;
  var bOk = false;
  var hh = new Number(strHeure.substring(0, 2));
  var mn = new Number(strHeure.substring(3, 5));
  if( strHeure.charAt(2) != ":" ) 
    bOk = false;
  else {
    if( hh.toString() == "NaN" || hh<0 || hh>23 )
      bOk = false;
    else {
      if( mn.toString() == "NaN" || mn<0 || mn>59 )
        bOk = false;
      else
        bOk = true;
    }
  }
  delete hh;
  delete mn;
  return bOk;
}
function ControleHeure(Objet) 
{
  var strHeure = Objet.value;
  if( strHeure=="" || isHeureValideHHMN(strHeure) )
    return true;
  else {
    alertMsgDate(Objet, "L'heure doit �tre dans un format HH:MN valide !");
    return false;
  }
}
function verifDateValidJJMMAAAA(strDate, strDateMin, strDateMax) 
{
  var strReason = " au format JJ/MM/AAAA.";
  if( strDate.length != 10 ) return strReason;
  
  var strMinDate = ( strDateMin == "" ? "00010101" : strDateMin.substring(6, 10)+strDateMin.substring(3, 5)+strDateMin.substring(0, 2));
  var strMaxDate = ( strDateMax == "" ? "99991231" : strDateMax.substring(6, 10)+strDateMax.substring(3, 5)+strDateMax.substring(0, 2));

  var bOk = false;
  var bBisex = false;
  var dd = new Number(strDate.substring(0, 2));
  var mm = new Number(strDate.substring(3, 5));
  var aaaa = new Number(strDate.substring(6, 10));

  if( strDate.charAt(2) != "\/" || strDate.charAt(5) != "\/" ) 
    bOk = false;
  else
    if( aaaa.toString() == "NaN" ) 
      bOk = false;
    else {
      if( (aaaa%4==0 && aaaa%100!=0) || (aaaa%400==0 && aaaa%100==0) ) bBisex = true;
      if( mm.toString() == "NaN" || mm<1 || mm>12 )
        bOk = false;
      else
        if( dd.toString() == "NaN" ||
          (dd<1 || dd>31) ||
          ((dd<1 || dd>30) && (mm==4 || mm==6 || mm==9 || mm==11)) ||
          ((dd<1 || dd>29) && mm==2 && bBisex==true) ||
          ((dd<1 || dd>28) && mm==2 && bBisex==false) )
          bOk = false;
        else {
          bOk = true;
          strReason = "";
        }
    }

  if( bOk == true && ( strDateMin != "" || strDateMax != "" ) ) {
    var strCurDate = strDate.substring(6, 10)+strDate.substring(3, 5)+strDate.substring(0, 2);
    strReason = "";
    if( !(strMinDate<=strCurDate && strCurDate<=strMaxDate) ) {
      if( strDateMin == "" ) {
        strReason = " ant�rieure au "+strDateMax+".";        
      } else if( strDateMax == "" ) {
        strReason = " post�rieure au "+strDateMin+".";
      } else {
        strReason = "\n comprise entre "+strDateMin+" et "+strDateMax+".";
      }      
    }
  }

  delete dd;
  delete mm;
  delete aaaa;

  return strReason;
}

function verifHeureValidHHMN(strHeure,strHeureMin, strHeureMax)
{
  var strReason = " au format HH:MN.";
  if( strHeure.length != 5 ) return strReason;

  var strMinHeure = ( strHeureMin == "" ? "0000" : strHeureMin.substring(0, 2)+strHeureMin.substring(3, 5) );
  var strMaxHeure = ( strHeureMax == "" ? "2359" : strHeureMax.substring(0, 2)+strHeureMax.substring(3, 5) );

  var bOk = false;
  var hh = new Number(strHeure.substring(0, 2));
  var mn = new Number(strHeure.substring(3, 5));
  if( strHeure.charAt(2) != ":" ) {
    bOk = false;
  } else {
    if( hh.toString() == "NaN" || hh<0 || hh>23 ) {
      bOk = false;
    } else {
      if( mn.toString() == "NaN" || mn<0 || mn>59 ) {
        bOk = false;
       } else {
        bOk = true;
        strReason="";
       }
    }
  }
  
   if( bOk == true && ( strMinHeure != "" || strMaxHeure != "" ) ) {
    var strCurHeure = strHeure.substring(0, 2)+strHeure.substring(3, 5);
    strReason = "";
    if( !(strMinHeure<=strCurHeure && strCurHeure<=strMaxHeure) ) {
      if( strHeureMin == "" ) {
        strReason = " ant�rieure � "+strHeureMax+".";        
      } else if( strHeureMax == "" ) {
        strReason = " post�rieure � "+strHeureMin+".";
      } else {
        strReason = "\n comprise entre "+strHeureMin+" et "+strHeureMax+".";
      }      
    }
  }
  delete hh;
  delete mn;
  return strReason;
}
function verifDateTimeValidJJMMAAAA_HHMN(strDateTime, oMin, oMax, bStrict)
{
  var strReason = " au format JJ/MM/AAAA HH:MN.";
  if( strDateTime.length != 16 ) return strReason;
  
  if( typeof(bStrict)=="undefined" ) bStrict = false;

  var strDate = strDateTime.substring(0, 10);
  var strTime = strDateTime.substring(11, 16);

  if( !isDateValidJJMMAAAA(strDate) || !isHeureValideHHMN(strTime) )
     return strReason;

  strReason = "";
  if( oMin != "" || oMax != "" ) {
    var strDateMin = ( oMin == "" ? "" : oMin.substring(0, 10) );
    var strDateMax = ( oMax == "" ? "" : oMax.substring(0, 10) );
    var strHeureMin = ( oMin == "" ? "" : oMin.substring(11, 16) );
    var strHeureMax = ( oMax == "" ? "" : oMax.substring(11, 16) );

    var strMinDate = ( strDateMin == "" ? "00010101" : strDateMin.substring(6, 10)+strDateMin.substring(3, 5)+strDateMin.substring(0, 2));
    var strMaxDate = ( strDateMax == "" ? "99991231" : strDateMax.substring(6, 10)+strDateMax.substring(3, 5)+strDateMax.substring(0, 2));
    var strMinHeure = ( strHeureMin == "" ? "0000" : strHeureMin.substring(0, 2)+strHeureMin.substring(3, 5) );
    var strMaxHeure = ( strHeureMax == "" ? "2359" : strHeureMax.substring(0, 2)+strHeureMax.substring(3, 5) );
  
    strMinDate = strMinDate + "" + strMinHeure;
    strMaxDate = strMaxDate + "" + strMaxHeure;
    var strCurDate = strDate.substring(6, 10)+strDate.substring(3, 5)+strDate.substring(0, 2)+strTime.substring(0, 2)+strTime.substring(3, 5);

    if( (!bStrict && !(strMinDate<=strCurDate && strCurDate<=strMaxDate)) || 
        (bStrict && !(strMinDate<strCurDate && strCurDate<strMaxDate)) ) {
      if( strDateMin == "" ) {
        strReason = " ant�rieure au "+strDateMax+".";        
      } else if( strDateMax == "" ) {
        strReason = " post�rieure au "+strDateMin+".";
      } else {
        strReason = "\n comprise entre "+strDateMin+" et "+strDateMax+".";
      }      
    }
  }

  return strReason;
}
