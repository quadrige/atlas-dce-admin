<?
//---------------------------------------------------------------
//
//---------------------------------------------------------------
function form_Test_JS()
{
?>
<script language="javascript">
function confirmation(strLien, strMsg) 
{
  var strTmp = "Confirmer la suppression ?";
  
  if( typeof(strMsg) != 'undefined' ) strTmp = strMsg;
  var res = window.confirm(strTmp);
  if( res )
    window.document.location.href = strLien;
  return;
}

function isChaineVide(obj, strMsg)
{
  var strTmp = 'Ce Champ ne peut �tre vide';
  
  if( typeof(strMsg) != 'undefined' ) strTmp = strMsg;          

  if( obj.value == "" )
    {
      alert(strTmp);
      obj.focus();
      return true;
    }
  return false;
}

function AlertMessage(Ctrl, strMessage) 
{
  alert(strMessage);
  Ctrl.focus();
  return;
}

function AlertMessage2(Ctrl, strMessage) 
{
  alert(strMessage);
  Ctrl.value = "";
  Ctrl.focus();
  return;
}
function ReplaceSpecialChar(form)
{
  var ctrl;
  var strTexte, strNew;
  for(i=0; i<form.length; i++) {
    oCtrl = form.elements[i];
    if( oCtrl.type=='text' || oCtrl.type=='textarea' ) {
      strTexte = new String(oCtrl.value);
      strNew = "";
      for(j=0; j<strTexte.length; j++) {
        if( strTexte.charCodeAt(j)==8217 ) { strNew = strNew + "'"; }
        else if( strTexte.charCodeAt(j)=="8364" ) { strNew = strNew + "&euro;"; }
        else if( strTexte.charCodeAt(j)>255 ) { strNew = strNew + " "; }
        else { strNew = strNew + strTexte.charAt(j); }
      }
      oCtrl.value = strNew;
    }
  }
}
function CaracMax(texte, max)
{
  if (texte.value.length >= max)
  {
    alert('Vous ne pouvez pas saisir plus de ' + max + ' caract�re(s) !!!') ;
    texte.value = texte.value.substr(0, max - 1) ;
  }
}
</script>
<?
}



//*********************************************************************
// Module regroupant les fonctions affichant les fonctions javascript
//*********************************************************************

//------------------------------------------------
// AffFonctionSelectliste
//
// Affiche le code javascript li� au controle de saisie select
//   - selectionner de tous les elements de la liste
//   - Supprimer des elements selectionnes
//   - ajouter un elements a la liste
//------------------------------------------------
function AffFonctionSelectliste()
{
  ?>

function SelectAllValues(objSelect) 
{
  // selectionne tous les elements de liste
  for(var i=0; i<objSelect.length-1; i++)
    objSelect.options[i].selected = true;
  // retire le dernier element vide de la selection
  if( objSelect.length > 0 )
    objSelect.options[objSelect.length-1].selected = false;
}

function SupprVal(objSelect) 
{
  // supprime de la liste, l'element de la liste d'indice idSuppr
  var idSuppr = objSelect.selectedIndex;
  var nbElt = objSelect.length;
  if( idSuppr==-1 || idSuppr==nbElt-1 || nbElt<2 )
    return ;

  objSelect.selectedIndex = -1;

  objSelect.options[nbElt-1].value = "";
  objSelect.options[nbElt-1].text = "";

  for(var i=idSuppr; i<objSelect.length-1; i++) {
    objSelect.options[i].value = objSelect.options[i+1].value;
    objSelect.options[i].text = objSelect.options[i+1].text;
   }
   objSelect.length = nbElt - 1;
   objSelect.selectedIndex = nbElt - 2;
}

  <?
} 


//------------------------------------------------
// AffFonctionCtrlNum
//
// Affiche le code javascript li� au controle de saisie
// d'un nombre
//------------------------------------------------
function AffFonctionCtrlNum()
{
  ?>
  function AlertMessageNum(Ctrl, iBorneInf, iBorneSup)
  {
    var strBorne = ""
    if( !(iBorneInf==0 && iBorneSup==0) )
      strBorne = " compris entre " + iBorneInf + " et " + iBorneSup;
    else
      strBorne = ".";
    
    alert("Ce champ n�cessite un nombre" + strBorne);
    Ctrl.focus();
    return;
  }

  function isNumValid(strNum, iBorneInf, iBorneSup) {

    var bOk = false;
    strNum = strNum.replace(/,/i, ".");
    var n = new Number(strNum);

    if( n.toString() == "NaN" )
      bOk = false;
    else
      if( !(iBorneInf == 0 && iBorneSup == 0) )
        if( iBorneInf <= n && n <= iBorneSup )
          bOk = true;
        else
          bOk = false;
      else
        bOk = true;

    return bOk;
  }

  function ControleNombre(Objet, iBorneInf, iBorneSup) {

    var strNb = Objet.value;
    if( isNumValid(strNb, iBorneInf, iBorneSup) )
      return true;
    else {
      AlertMessageNum(Objet, iBorneInf, iBorneSup);
      return false;
    }
    
  }

function isDateHeure(ob, strMsg)
{
  var bok = true;
  var strTmp =  "Format d'une date : jj/mm/aaaa hh:mm";

  if( ob.value.length == 0 ) return true;
  if( ob.value.length != 16 ) 
  {
    bok=false;
  }
  else
  {    
    bok = (isNumValid(ob.value.substr(0,2),1,31))&&(isNumValid(ob.value.substr(3,2),1,12))&&(isNumValid(ob.value.substr(6,4),1900,2100))&&(isNumValid(ob.value.substr(11,2),0,23))&&(isNumValid(ob.value.substr(14,2),0,59));
    bok = (bok)&&(ob.value.charAt(2)=="/")&&(ob.value.charAt(5)=="/")&&(ob.value.charAt(10)==" ")&&(ob.value.charAt(13)==":");
  }
  
  if (! bok) 
  {
    if( typeof(strMsg) != 'undefined' ) strTmp = strMsg;
    alert(strTmp);
    ob.focus();
    return false;
  }
  return bok;
}

  <?

}

//------------------------------------------------
// AffFonctionCtrlDate
//
// Affiche le code javascript li� au controle de saisie
// d'une date
//------------------------------------------------
function AffFonctionCtrlDate()
{
  ?>

  function isDateValidJJMMAAAA(strDate) {

    if( strDate.length != 10 ) return false;
    var bOk = false;
    var bBisex = false;
    var dd = new Number(strDate.substring(0, 2));
    var mm = new Number(strDate.substring(3, 5));
    var aaaa = new Number(strDate.substring(6, 10));
    if( strDate.charAt(2) != "\/" || strDate.charAt(5) != "\/" ) 
      bOk = false;
    else
      if( aaaa.toString() == "NaN" || aaaa<1800 || aaaa>2200 ) 
        bOk = false;
      else {
        if( (aaaa%4==0 && aaaa%100!=0) || (aaaa%400==0 && aaaa%100==0) ) bBisex = true;
        if( mm.toString() == "NaN" || mm<1 || mm>12 )
          bOk = false;
        else
          if( dd.toString() == "NaN" ||
            (dd<1 || dd>31) ||
            ((dd<1 || dd>30) && (mm==4 || mm==6 || mm==9 || mm==11)) ||
            ((dd<1 || dd>29) && mm==2 && bBisex==true) ||
            ((dd<1 || dd>28) && mm==2 && bBisex==false) )
            bOk = false;
          else
            bOk = true;
      }
    delete dd;
    delete mm;
    delete aaaa;
    return bOk;
  }

	function isDateValidMMAAAA(strDate) {

		if( strDate.length != 7 ) return false;
		var bOk = false;
		var mm = new Number(strDate.substring(0, 2));
		var aaaa = new Number(strDate.substring(3, 7));
		if( strDate.charAt(2) != "\/" ) 
			bOk = false;
		else
			if( aaaa.toString() == "NaN" || aaaa<1900 || aaaa>2500 ) 
				bOk = false;
			else
				if( mm.toString() == "NaN" || mm<1 || mm>12 )
					bOk = false;
				else
					bOk = true;
		delete dd;
		delete mm;
		delete aaaa;
		return bOk;
	}
	
  function isDateValidJJMM(strDate) {

    if( strDate.length != 5 ) return false;
    var bOk = false;
    var dd = new Number(strDate.substring(0, 2));
    var mm = new Number(strDate.substring(3, 5));
    if( strDate.charAt(2) != "\/" ) 
      bOk = false;
    else {
      if( mm.toString() == "NaN" || mm<1 || mm>12 )
        bOk = false;
      else {
        if( dd.toString() == "NaN" ||
          (dd<1 || dd>31) ||
          ((dd<1 || dd>30) && (mm==4 || mm==6 || mm==9 || mm==11)) ||
          ((dd<1 || dd>29) && mm==2) )
          bOk = false;
        else
          bOk = true;
      }
    }
    delete dd;
    delete mm;
    return bOk;
  }

  function ControleDate(Objet, lg)
  {
    var strDate = Objet.value;
    if( strDate == "" )
      return true;
    if( lg == 5 ) {
      if( isDateValidJJMM(strDate) )
        return true;
      else {
        AlertMessage2(Objet, "La date doit �tre dans un format JJ/MM valide !");
        return false;
      }
    } else {
      if( isDateValidJJMMAAAA(strDate) )
        return true;
      else {
        AlertMessage2(Objet, "La date doit �tre dans un format JJ/MM/AAAA valide !");
        return false;
      }
    }
  }
  <?

}

//------------------------------------------------
// AffFonctionCtrlHeure
//
// Affiche le code javascript li� au controle de saisie
// d'une heure
//------------------------------------------------
function AffFonctionCtrlHeure()
{
  ?>
  
  function isHeureValideHHMN(strHeure) {
    
    if( strHeure.length != 5 ) return false;
    var bOk = false;
    var hh = new Number(strHeure.substring(0, 2));
    var mn = new Number(strHeure.substring(3, 5));
    if( strHeure.charAt(2) != ":" ) 
      bOk = false;
    else {
      if( hh.toString() == "NaN" || hh<0 || hh>23 )
        bOk = false;
      else {
        if( mn.toString() == "NaN" || mn<0 || mn>59 )
          bOk = false;
        else
          bOk = true;
      }
    }
    delete hh;
    delete mn;
    return bOk;
  }

  function ControleHeure(Objet) 
  {
    var strHeure = Objet.value;
    if( strHeure=="" || isHeureValideHHMN(strHeure) )
      return true;
    else {
      AlertMessage2(Objet, "L'heure doit �tre dans un format HH:MN valide !");
      return false;
    }
  }

  <?
}

//------------------------------------------------
// AffFonctionCtrlMaxLength
//
// Affiche le code javascript li� au controle de la longueur d'un champs textarea
// 
// Conseil: utiliser cette fonction avec un onkeyup dans le textarea
//------------------------------------------------
function AffFonctionCtrlMaxLength()
{
  ?>
  
  function controlMaxLength(obj, maxlength)
  {
    var textLength;  
    var objText;
    
    objText = eval(obj);
    textLength = objText.value.length;
    
    if (textLength>maxlength)
    {
      alert('Ce champs est limit� � '+ maxlength +' caract�res');
      objText.value = objText.value.slice(0, (maxlength));
      objText.focus();
    }
  }
  
  <?
}
?>