<?

/**
 * @brief Fonction d'affichage du menu propre � l'application
 */
function aff_menu_appli($arbreMenu, $cont_id, $appli_id, $idCourant, $strParam="")
{
  $iNiveauMenuAppli = 0;
?>
 <table border="0" cellspacing="0" cellpadding="0" height="300" background="">
   <tr>
      <td valign=top width=140>
         <table width=100% border="0" cellspacing="0" cellpadding="0" background="">
           <tr>
             <td valign="top">
               <table width=100% border="0" cellspacing="0" cellpadding="0" background="">
               <?
                 //Parcours de l'arbre
                 $arbreMenu->remonter();
                 aff_fils($arbreMenu,$cont_id, $appli_id, $idCourant,  $strParam);
               ?>
               </table>
             </td>
           </tr>
         </table>
      </td>
      <td width=1 height=1 bgcolor="<?=ALK_COL_STR_COLOR_LINE?>"><img width=1 height=1></td>
      <td width=639 bgcolor="#FFFFFF" valign=top align=center>
<?
}


function aff_fils($arbreMenu, $cont_id, $appli_id, $idCourant,  $strParam="")
{
  global $iNiveauMenuAppli;

  $iNiveauMenuAppli++;
  $nbFils = $arbreMenu->nb_fils();
  for ($i=1; $i<=$nbFils; $i++){
    $arbreMenu->descendre($i);
    $tabMenu = $arbreMenu->valeur();
    $id = $tabMenu["id"];
    $libelle = $tabMenu["nom"];
    if (strpos($tabMenu["lien"],"?")>0)
      $lien = ALK_SIALKE_URL."scripts/".$tabMenu["lien"]."&cont_id=".$cont_id."&cont_appli_id=".$appli_id."&menu_appli_id=".$id.$strParam;
    else
      $lien = ALK_SIALKE_URL."scripts/".$tabMenu["lien"]."?cont_id=".$cont_id."&cont_appli_id=".$appli_id."&menu_appli_id=".$id.$strParam;
    
    $iLgIndent = ($iNiveauMenuAppli) * 10;
    $strParamTd = ($id == $idCourant)?
                  "bgcolor=".ALK_COL_BG_COLOR_MENU_ROLL." valign=middle class=menu":
                  "bgcolor=".ALK_COL_BG_COLOR_MENU." valign=middle class=menu onMouseOver=\"this.bgColor='".
                  ALK_COL_BG_COLOR_MENU_ROLL."'\" onMouseOut=\"this.bgColor='".ALK_COL_BG_COLOR_MENU."';\"";

    echo "<tr>".
           "<td width=1 height=20 bgcolor=".ALK_COL_STR_COLOR_LINE."><img width=1 height=1></td>".
           "<td ".$strParamTd.">".
              "<table cellpadding=0 cellspacing=0 border=0>".
                "<tr>".
                  "<td width=1><img src=\"".ALK_URL_SI_MEDIA."transp.gif\" border=0 width=".$iLgIndent." height=1></td>".
                  "<td class=\"menu\" valign=middle>>&nbsp;</td>".
                  "<td class=\"menu\"><a class=\"menu\" href=\"".$lien."\">".$libelle."</a></td>".
                "</tr>".
              "</table>".
           "</td>".
         "</tr>".
         "<tr>".
           "<td colspan=2 width=1 height=1 bgcolor=".ALK_COL_STR_COLOR_LINE."><img width=1 height=1></td>".
         "</tr>";
         
    aff_fils($arbreMenu, $cont_id, $appli_id, $idCourant, $strParam);
    $arbreMenu->remonter();
    $iNiveauMenuAppli--;
  }
}

function aff_fin_menu_appli()
{
?>
      </td>
    </tr>
  </table>
<?
}
?>