<?php
/**
 * @brief retourne une cha�ne g�n�r�e al�atoirement respectant le masque pass� en param�tre
 *
 * @param strMask chaine compos� des lettres suivantes : c pour obtenir un chiffre, L lettre en maj, l lettre en min
 */
function getStrGeneratedPwd($strMask="cLLlclcL")
{
	$strPwd = "";
	for ($i=0; $i<strlen($strMask); $i++) {
			switch ($strMask[$i]) {
				case "c":
					$iCharCode = mt_rand(48,57);
					break;
				case "l":
					$iCharCode = mt_rand(97,122);
					break;
				case "L":
					$iCharCode = mt_rand(65,90);
					break;						
			}
			$strPwd .= chr($iCharCode); 
	}
	return $strPwd;	
}
?>