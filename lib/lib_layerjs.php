//------------------------------------------------------------
// OpenWriteToLayer
// Initialise le support d'�criture sur le layer du doc
// Param�tres :
//   objDoc  : objet de type Document
//   idLayer : identifiant du layer
//------------------------------------------------------------
function OpenWriteToLayer(objDoc, idLayer)
{
	if( objDoc.all )
	{
		objDoc.all[idLayer].innerHTML = "";
		return;
	}
	if( objDoc.layers ) 
	{
		objDoc.layers[idLayer].clear();
		return;
	}
	else
	{
		var objLayer = objDoc.getElementById(idLayer);
		objLayer.innerHTML = "";
	}
}

//------------------------------------------------------------
// WriteToLayer
// Ecrit le code html sur le layer du doc
// Param�tres :
//   objDoc  : objet de type Document
//   idLayer : identifiant du layer
//------------------------------------------------------------
function WriteToLayer(objDoc, idLayer, strHtml)
{
	if( objDoc.all ) 
	{
		objDoc.all[idLayer].innerHTML = objDoc.all[idLayer].innerHTML + strHtml;
		return;
	}
	if( objDoc.layers ) 
	{
		objDoc.layers[idLayer].write(strHtml);
		return;
	}
	else
	{
		var objLayer = objDoc.getElementById(idLayer);
		objLayer.innerHTML = objLayer.innerHTML + strHtml;
	}
}
//------------------------------------------------------------
// CloseWriteToLayer
// Ferme le contexte d'�criture sur le layer du doc
// Param�tres :
//   objDoc  : objet de type Document
//   idLayer : identifiant du layer
//------------------------------------------------------------
function CloseWriteToLayer(objDoc, idLayer)
{
	if( objDoc.layers ) 
		objDoc.layers[idLayer].close();
}
