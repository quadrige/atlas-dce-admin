<?

///<comment>
///<summary>
/// Renvoie soit la valeur de la variable POST s'il existe une valeur pour cette variable et si celle-ci
/// est num�rique, sinon renvoie la valeur par d�faut
///</summary>
///<param name="strVariable">Nom de la variable POST</param>
///<param name="iDefaut">valeur par d�faut</param>
///<return>valeur � affecter</return>
///</comment>
function getValeurPostNumeric ( $strVariable, $iDefaut )
{
	return (isset($_POST[$strVariable]) && is_numeric($_POST[$strVariable])) ? $_POST[$strVariable] : $iDefaut;
}

///<comment>
///<summary>
/// Renvoie soit la valeur de la variable POST s'il existe une valeur pour cette variable et si celle-ci
/// est non vide, sinon renvoie la valeur par d�faut
///</summary>
///<param name="strVariable">Nom de la variable POST</param>
///<param name="iDefaut">valeur par d�faut</param>
///<return>valeur � affecter</return>
///</comment>
function getValeurPostString ( $strVariable, $strDefaut )
{
	return (isset($_POST[$strVariable]) && ($_POST[$strVariable] != "")) ? $_POST[$strVariable] : $strDefaut;
}

///<comment>
///<summary>
/// Renvoie soit la valeur de la variable POST s'il existe une valeur pour cette variable et si celle-ci
/// est non vide puis r�alise l'analyseSql, sinon renvoie la valeur par d�faut
///</summary>
///<param name="conn">Objet de connection � la base de donn�es</param>
///<param name="strVariable">Nom de la variable POST</param>
///<param name="iDefaut">valeur par d�faut</param>
///<return>valeur � affecter</return>
///</comment>
function getValeurPostStringAnalyse ( $conn, $strVariable, $strDefaut )
{
	return (isset($_POST[$strVariable]) && ($_POST[$strVariable] != "")) ? AnalyseSQL($_POST[$strVariable]) : $strDefaut;
}

///<comment>
///<summary>
/// Renvoie soit la valeur de la variable GET s'il existe une valeur pour cette variable et si celle-ci
/// est num�rique, sinon renvoie la valeur par d�faut
///</summary>
///<param name="strVariable">Nom de la variable GET</param>
///<param name="iDefaut">valeur par d�faut</param>
///<return>valeur � affecter</return>
///</comment>
function getValeurGetNumeric ( $strVariable, $iDefaut )
{
	return (isset($_GET[$strVariable]) && is_numeric($_GET[$strVariable])) ? $_GET[$strVariable] : $iDefaut;
}

///<comment>
///<summary>
/// Renvoie soit la valeur de la variable GET s'il existe une valeur pour cette variable et si celle-ci
/// est non vide, sinon renvoie la valeur par d�faut
///</summary>
///<param name="strVariable">Nom de la variable GET</param>
///<param name="iDefaut">valeur par d�faut</param>
///<return>valeur � affecter</return>
///</comment>
function getValeurGetString ( $strVariable, $strDefaut )
{
	return (isset($_GET[$strVariable]) && ($_GET[$strVariable] != "")) ? $_GET[$strVariable] : $strDefaut;
}

///<comment>
///<summary>
/// Renvoie soit la valeur de la variable GET s'il existe une valeur pour cette variable et si celle-ci
/// est non vide puis r�alise l'analyseSql, sinon renvoie la valeur par d�faut
///</summary>
///<param name="conn">Objet de connection � la base de donn�es</param>
///<param name="strVariable">Nom de la variable GET</param>
///<param name="iDefaut">valeur par d�faut</param>
///<return>valeur � affecter</return>
///</comment>
function getValeurGetStringAnalyse ( $conn, $strVariable, $strDefaut )
{
	return (isset($_GET[$strVariable]) && ($_GET[$strVariable] != "")) ? AnalyseSQL($_GET[$strVariable]) : $strDefaut;
}

?>