<?php
/**
 * @brief Retourne l'ent�te html de la page
 *
 * @param tabNav       Chaine ou tableau pour la navigation en profondeur
 * @param iDroitAppli  droit utilisateur sur l'appli
 * @param tabAppliMenu tableau contenant les �l�ments d'un sous menu popup
 * @param bAdminPart   Partie consultation = false, partie administration = true
 */
function aff_menu_haut($tabNav="", $iDroitAppli=-1, $tabAppliMenu = array(), $bAdminPart=false)
{
  echo getHtmlHeader($tabNav, $iDroitAppli, $tabAppliMenu, $bAdminPart);
}

/**
 * @brief Affiche le bas de la page
 */
function aff_menu_bas()
{
  echo getHtmlFooter();
}

/**
 * @brief Retourne le code html du haut de la page (Logo + bandeau haut + menu gauche)
 *
 * @param tabNav       Tableau associatif contenant les liens de navigation
 * @param iDroitAdmin  Droit sur l'appli en cours =4 publicateur, =2 admin, =1 lecture, =0 aucun
 * @param tabAppliMenu Tableau associatif contenant les infos statiques sur des �l�ments de menu � inclure
 * @param bAdminPart   Partie consultation = false, partie administration = true
 * @return Retourne un string
 */
function getHtmlHeader($tabNav=array(), $iDroitAdmin=-1, $tabAppliMenu = array(), $bAdminPart=false)
{
  global $g_strLogin;
  global $_sit_Type_Appli, $_sit_urlHomePage, $_sit_targetHomePage, $_sit_menuAdminAppli;
  global $_sit_tabMenu, $_sit_tabMenuFils, $_sit_tabMenuEspace, $_sit_tabMenuEspaceFils;
  global $oSpace, $oAppli;
  //global $iProfilCont;
  global $_bAffMenuSitGauche;
  global $arbreMenuAppli;

  $cont_id = $oSpace->cont_id;
  $cont_appli_id = $oSpace->appli_id;
  //  if( !($iProfilCont!="" && is_numeric($iProfilCont)) )
  $iProfilCont = $oSpace->iProfilCont;

  $strUrlAccueil = ( !isset($_SESSION["sit_HomePageUser"]) ? "" : $_SESSION["sit_HomePageUser"] );
  $strDefaultTarget = ( defined("ALK_DEFAULT_TARGET")==true ? ALK_DEFAULT_TARGET : "");
  $strTargetAccueil = ( !isset($_SESSION["sit_TargetHomePageUser"]) ? $strDefaultTarget : $_SESSION["sit_TargetHomePageUser"] );

  $strTitrePage      = $oSpace->getProperty("CONT_INTITULE");
  $strTitreAliasPage = $oSpace->getProperty("CONT_INTITULE_COURT");

  //chemin de navigation des conteneurs
  $strChemin = getLienNav($tabNav, $oSpace->cont_id);

  $strHtml = getHtmlMainHeader().
    "<script language='JavaScript'>".
    " navigateur = null;".
    " x = null;".
    " y = null;".
    " bWinLoaded = 0;".
    " menuBar = null;".
    " ALK_SIALKE_URL = '".ALK_SIALKE_URL."';".
    " ALK_URL_ASPIC = '".( defined("ALK_URL_ASPIC") ? ALK_URL_ASPIC : "")."';".
    " ALK_URL_ASPIC_V2 = '".( defined("ALK_URL_ASPIC_V2") ? ALK_URL_ASPIC_V2 : "")."';".
    " ALK_DIR_ANNU = '".ALK_DIR_ANNU."';".
    " ALK_DIR_ESPACE = '".ALK_DIR_ESPACE."';".
    " ALK_URL_IDENTIFICATION = '".ALK_URL_IDENTIFICATION."';".
    " ALK_ROOT_URL = '".ALK_ROOT_URL."';".
    (defined("ALK_URL_SERVEUR_DIST") ? "var ALK_URL_SERVEUR_DIST = '".ALK_URL_SERVEUR_DIST."';" : "") .
    " strTitrePage = \"".$strTitrePage."\";".
    " strTitreAliasPage = \"".$strTitreAliasPage."\";".
    " cont_id = '".$cont_id."';".
    " cont_appli_id = '".$cont_appli_id."';".
    " strDefaultTarget= '".$strDefaultTarget."';".
    " function SelectAndPrint() {".
    "  javascript:window.focus();".
    "  if( window.print )".
	  "    window.print();".
    "  else".
		"    alert(\"Utilisez l'impression du navigateur pour imprimer cette page.\");".
    " } ".

    /** Ajouter constantes javascript sp�cifiques pour l'appli + fonctions appel�es sur onload si besoin */
    ( is_object($oAppli) && method_exists($oAppli, "getHtmlJsInit") 
      ? $oAppli->getHtmlJsInit() 
      : ( function_exists("getHtmlJsInit") ? getHtmlJsInit() : "" )).
    
    " function onLoadInit() {".
    "   bWinLoaded = 1; document.onmousemove = position;".
    /** Actions sp�cifiques � l'appli sur body onload */
    ( is_object($oAppli) && method_exists($oAppli, "getHtmlJsOnLoad") 
      ? $oAppli->getHtmlJsOnLoad() 
      : ( function_exists("getHtmlJsOnLoad") ? getHtmlJsOnLoad() : "" )).
    " } ".
    
    //getHtmlAideJS().
    
    "</script>".
    "<script language='javascript' src='../../lib/lib_jsaddon.js'></script>".

    getHtmlMainJs().

    /** inclusioon des librairies js sp�cifiques � l'appli */
    ( is_object($oAppli) && method_exists($oAppli, "getHtmlJsFileSource") 
      ? $oAppli->getHtmlJsFileSource() 
      : (function_exists("getHtmlJsFileSource") ? getHtmlJsFileSource() : "" )).

    "</head>".

    "<body bgcolor='#FFFFFF' text='#000000' ".
    " leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'".
    " onload=\"onLoadMainInit(); onLoadInit(); SelectAndPrint();\">".

    "<table border='0' cellspacing='0' cellpadding='0'>".
    "<tr>".
    "<td valign='top' width='606' style='border:1px solid;'>".$strChemin."</td>".
    "</tr>".
    "<tr>".
    "<td valign=top>";

  return $strHtml;
}


/**
 * @brief Rtourne le code html du bas de la page
 *
 * @return Retourne un string
 */
function getHtmlFooter()
{
  // fin du contenu
  // fermeture de la charte
  $strHtml = getHtmlMainFrameFooter().
    getHtmlMainFooter();

  return $strHtml;
}

/**
 * @brief Retourne la liste des liens a afficher. Classes de style utilisees : menuPath, menuPathBD
 *
 * @param tab     Tableau associatif contenant les liens-texte de navigation
 * @param cont_id Identifiant dy conteneur courant
 * @return Retourne une chaine HTML contenant les liens de navigation
 */
function getLienNav($tab, $cont_id, $strDefaultTarget="")
{
  /*
  global $queryCont;
  global $cont_appli_id; 
  global $_sit_urlHomePage;
  global $oSpace;

  $strNav = "";
  $strSepar = " > ";
  
  // ajoute les espaces successifs
  
  $strListeCont="";
  $dsCont = $queryCont->GetDs_ficheEspaceById($cont_id);
  if ($drCont = $dsCont->GetRowIter() ) {
    $strListeCont = $drCont->getValueName("CONT_ARBRE");
    $strListeCont = substr($strListeCont,1,strlen($strListeCont)-2);
    $strListeCont = str_replace("-",",",$strListeCont);
  }
  
  if ($strListeCont != ""){
    $dsCont = $queryCont->GetDs_listeEspaceNavByAgent($_SESSION["sit_idUser"],$strListeCont, 0);
    while( $drCont = $dsCont->GetRowIter() ) {
      $iDroit = $drCont->getValueName("DROIT");
      $icont_id = $drCont->getValueName("CONT_ID");
      $icont_intitule = $drCont->getValueName("CONT_INTITULE");
      $iAppliDefaut = $drCont->getValueName("DEFAUT");
      $idAppliDefaut = $drCont->getValueName("CONT_APPLI_DEFAUT");
      $strUrlAppli = $drCont->getValueName("ATYPE_URL");
      $strTargetAppli = $drCont->getValueName("ATYPE_URL_TARGET");
      $icont_appli_defaut = ( $iAppliDefaut==0 ? 0 : $idAppliDefaut );
      $strTargetAppli = ( $strTargetAppli!="" ? $strTargetAppli : $strDefaultTarget );

      if( $iDroit == 1 ) {
        if( $drCont->getValueName("ATYPE_URL")=="" || $icont_appli_defaut==0 )
          $strUrl = ALK_SIALKE_URL."scripts/".ALK_DIR_ESPACE."sommaire.php?cont_id=".$icont_id;
        else
          if( $oSpace->_testUrlJs($strUrlAppli) )
            $strUrl = $oSpace->_getUrlJS($strUrlAppli, $icont_id, $icont_appli_defaut, "'".$strTargetAppli."'");
          else
            $strUrl = ALK_SIALKE_URL."scripts/".$strUrlAppli.
              "?cont_id=".$icont_id."&cont_appli_id=".$icont_appli_defaut;
      
        $strNav .= "<a target=\"".$strTargetAppli.
          "\" class=\"menuPathBD\" href=\"".$strUrl."\">".$icont_intitule."</a>";   
        if( $cont_id != $icont_id ) 
          $strNav .= $strSepar;
      }
    }
  }

  // ajoute l'arbo des applications
  if( is_array($tab) ) {
    if( count($tab)>0 ) 
      foreach($tab as $t) {
        if( $strNav != "" ) 
          $strNav .= $strSepar;
        if( $t["url"] != "" ) {
          $strTarget = ( array_key_exists("target", $t)==true ? "target=\"".$t["target"]."\"" : "");
          $strNav .= "<a ".$strTarget." class=\"menuPath\" href=\"".$t["url"]."\">".$t["titre"]."</a>";
        } else {
          $strNav .= "<span class=\"menuPath\">".($t["titre"])."</span>";
        }
      }
  } else
    $strNav .= $tab;
  
  return $strNav;
  */
  return "";
}


?>