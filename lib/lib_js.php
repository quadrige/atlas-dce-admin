<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet::Library
Librairie js et php globale à Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/

/**
 * @file lib_js.php
 * @package Alkanet_Library
 * @brief Déclaration des constantes php en javascript
 */

session_start();
require_once("../libconf/app_conf.php");

// récupération des constantes PHP
$tabConstInit = get_defined_constants();

// constante utilisée pour ne pas avoir à prendre en compte la constante ALK_ONE_ADMIN
if( !defined("ALK_LIBJS_PHP") ) {
  define("ALK_LIBJS_PHP", true);
}

require_once(ALK_ALKANET_ROOT_PATH."lib/app_conf_alkanet.php");
require_once(ALK_ALKANET_ROOT_PATH."lib/app_conf_sig.php");

// récupération des constantes SLM
$tabConstNew = get_defined_constants();

// calcul la différence des 2 tableaux pour ne garder que les constantes utilisateur
$tabConst = array_diff_assoc($tabConstNew, $tabConstInit);
$tabConst["ALK_ALKANET_ROOT_URL"]  = $tabConstInit["ALK_ALKANET_ROOT_URL"];
if( defined("ALK_SEARCH") ) {
  $tabConst["ALK_SEARCH"]         = $tabConstInit["ALK_SEARCH"];
  $tabConst["ALK_SEARCH_ALKANET"] = $tabConstInit["ALK_SEARCH_ALKANET"];
  $tabConst["ALK_SEARCH_SOLR"]    = $tabConstInit["ALK_SEARCH_SOLR"];
}

$tabExclude = array("_ADMIN_", "PWD", "ALK_ATYPE_ABREV_", "ALK_VERIF_", "ALK_SQL_", "RIGHT", "ALK_PRIV_", "ALK_B_", "ALK_APP_", "ALK_INTERFACE_");
// affichage js des constantes
foreach($tabConst as $strConstName => $strConstValue ) {
  // évite d'écrire le compte admin dans le code
  $bAdd = true;
  foreach ($tabExclude as $namePart){
    $bAdd = ( mb_strpos($strConstName, $namePart) === false );
    if ( !$bAdd )
      break;
  }
  if ( $bAdd )
    echo "var ".$strConstName." = \"".str_replace('"', '', $strConstValue)."\";\n";
}

// mémorise le décalage en heure du serveur / GMT du serveur (<0 à l'est, >0 à l'ouest) 
echo "var ALK_DELTAGMT_SERV = ".(-date("Z", time())/3600).";\n";

?>