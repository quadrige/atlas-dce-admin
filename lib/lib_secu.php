<?php
/**
 * @brief Verifie l'etat de session. 
 *        Si la session est fermee, redirection vers la page d'authentification
 */
function isConnected()
{
  if( $_SESSION["sit_idUser"] == "" ) {
    session_destroy();
    header("location: ".ALK_URL_IDENTIFICATION);  
  }
}


/**
 * @brief Retourne l'identifiant du profil utilisateur par rapport � l'espace courant
 *        Si le profil utilisateur est superieur � iProfilValide, 
 *        Alors redirection vers la page d'identification
 *
 * @param iProfilValide Identifiant du profil minimum requis
 *
 * @return Retourne l'identifiant du profil utilisateur par rapport � l'espace en cours
 *          99 : aucun droit
 *          1  : administrateur principal
 *          2  : animateur espace
 *          3  : utilisateur
 */
function getProfilCont()
{
  global $oSpace;
  return $oSpace->iProfilCont;
}


/**
 * Ancienne fonction
 */
function getProfilAdminActu($cont_appli_id)
{
  getProfilContAppli($cont_appli_id);
}


/**
 * @brief Retourne l'identifiant du profil utilisateur par rapport a l'application courante
 *        Si le profil utilisateur est superieur � iProfilValide, 
 *        Alors redirection vers la page d'identification
 *
 * @params iProfilValide Identifiant du profil minimum requis
 * @return Retourne l'identifiant du profil utilisateur par rapport � l'application en cours
 *          0 : aucun droit sur l'appli
 *          1 : utilisateur de l'appli (droit lecture sur sit_agent_appli)
 *          2 : administrateur de l'appli (droit ecriture sur sit_agent_appli)
 */
function getProfilContAppli()
{
  global $oSpace;
  return $oSpace->iDroitAppli;
}


/**
 * @brief Se charge de placer une redirection en javascript vers l'authentification
 *        dans le cas ou l'entete HTML a deja ete transmis
 */
function go_login()
{
  echo "<script language=javascript>document.location.href=\"".ALK_URL_IDENTIFICATION."\";</script>";
  die();
}
?>