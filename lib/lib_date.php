<?
///<comment>
///<summary>
/// Ajoute un nombre number d'interval interval � une date date
///</summary>
///<params name="interval">
// 	yyyy	ann�e
// 	q			Trimestre
// 	m			Mois
// 	y			jour de l'ann�e
// 	d			jour
// 	w			jour de la semaine
// 	ww    Semaine de l'ann�e
// 	h			Heure
// 	n			Minute
// 	s			seconde
///</params>
///<params name="number" option>
/// Nombre d'interval � ajouter � la date
///</params>
///<params name="date" option>
/// Date de r�f�rence
///</params>
///<returns>
/// une date de type timestamp
///</returns>
///</comment>


function DateAdd ($interval,  $number, $date) {

	$date_time_array  = getdate($date);
	
	$hours =  $date_time_array["hours"];
	$minutes =  $date_time_array["minutes"];
	$seconds =  $date_time_array["seconds"];
	$month =  $date_time_array["mon"];
	$day =  $date_time_array["mday"];
	$year =  $date_time_array["year"];
	
	switch ($interval)
	{
	  case "yyyy":
	    $year +=$number;
	    break;        
	  case "q":
			$year +=($number*3);
			break;        
	  case "m":
			$month +=$number;
			break;        
	  case "y":
	  case "d":
	  case "w":
			$day+=$number;
			break;        
	  case "ww":
			$day+=($number*7);
			break;        
	  case "h":
			$hours+=$number;
			break;        
	  case "n":
			$minutes+=$number;
			break;        
	  case "s":
			$seconds+=$number;
			break;        
	}
	
	$timestamp =  mktime($hours ,$minutes, $seconds,$month ,$day, $year);
	return $timestamp;
}

///<comment>
///<summary>
/// Convertit un jour ou un mois en fran�ais
///</summary>
///<params name="bMode"">
//  0     jour
//  1     mois
///</params>
///<params name="iValue">
/// Valeur � traduire
///</params>
///<returns>
/// la valeur traduite
///</returns>
///</comment>
function ConvertDateToFrench($bMode, $iValue)
{
   $tabJour = array ( 1=>"Lundi",
                      2=>"Mardi",
                      3=>"Mercredi",
                      4=>"Jeudi",
                      5=>"Vendredi",
                      6=>"Samedi",
                      7=>"Dimanche");
                      
   $tabMois = array ( 1=>"janvier",
                      2=>"f�vrier",
                      3=>"mars",
                      4=>"avril",
                      5=>"mai",
                      6=>"juin",
                      7=>"juillet",
                      8=>"aout",
                      9=>"septembre",
                      10=>"octobre",
                      11=>"novembre",
                      12=>"d�cembre");

   if ( $bMode==1 )
   {
    if ($iValue>0 && $iValue<13)
      return ($tabMois[$iValue]);
   }
   else
   {
    if ($iValue>0 && $iValue<8)
      return($tabJour[$iValue]);
   }
                      
}

?>
