<?

///<comment>
///<summary>
/// Ajoute les animateurs de l'espace en cours dans la liste des autoris�s pour le forum en cours
/// s'ils n'y sont pas d�j�.
///</summary>
///<param name="cont_id">identifiant de l'espace</param>
///<param name="forum_id">identifiant du forum</param>
///</comment>
function AddDroitAnimateur2Forum($cont_id, $forum_id)
{
  global $conn;

	$strSql = "select AGENT_ID from FORUM_01_DROIT where FORUM_ID=".$forum_id." and AGENT_ID in ".
		"(select AGENT_ID from SIT_AGENT_CONT where cont_id=".$cont_id." and cont_admin=1)";
	$rsDroit = initRecordSet($strSql, $conn);

	$nbLigne = ocifetchstatement($rsDroit, $tabRes);

	if( $nbLigne > 0 )
		while( $column = each($tabRes))
			$strListId = "(".implode(",", $column['value']).")";
	else
		$strListId = "(-1)";

 // On ne l'ajoute que s'il n'est pas d�j� pr�sent
  $strSql = "insert into FORUM_01_DROIT (FORUM_ID, AGENT_ID, EMAIL) ".
    "select ".$forum_id.", a.AGENT_ID, '' ".
    "from SIT_AGENT a, SIT_AGENT_CONT ac ".
    "where a.AGENT_VALIDE>-1 and a.AGENT_ID=ac.AGENT_ID and ac.CONT_ID=".$cont_id.
	  " and ac.CONT_ADMIN=1 and a.AGENT_ID not in ".$strListId;
  executeSql($strSql, $conn);
}

///<comment>
///<summary>
/// Ajoute un agent dans la liste de autoris�s pour 1 forum s'il n'y est pas d�j�
///</summary>
///<param name="forum_id">identifiant du forum</param>
///<param name="agent_id">identifiant de l'agent</param>
///<param name="strEmail">email de l'agent pour l'abonn� automatiquement</param>
///</comment>
function AddDroitAgent2Forum($forum_id, $agent_id, $strEmail)
{
  global $conn;

	// v�rifie existance
  $strSql = "select * from FORUM_01_DROIT where FORUM_ID=".$forum_id." and AGENT_ID=".$agent_id;
	$rsDroit = initRecordSet($strSql, $conn);
	if( !ociFetch($rsDroit) )
		{
			// ajout
			$strSql = "insert into FORUM_01_DROIT (FORUM_ID, AGENT_ID, EMAIL) values (".
				$forum_id.", ".$agent_id.", ".ora_analyseSql($strEmail).")";
			executeSql($strSql, $conn);
		}
}

///<comment>
///<summary>
/// Retire tous les agents non abonn�s de la liste des droits
/// Appel� lorsque le forum devient public. 
///</summary>
///<param name="forum_id">identifiant du forum</param>
///</comment>
function RetireAgentNonAbonneForum($forum_id)
{
  global $conn;

  $strSql = "delete from FORUM_01_DROIT where FORUM_ID=".$forum_id." and (EMAIL is null or EMAIL='')";
	executeSql($strSql, $conn);
}
?>
