<?php
include_once("lib_fichier.php");

///<comment>
///<summary>
///Fichier contenant toutes les fonctionnalit�s
///li�es �l'application cartoexcel
///</summary>
///</comment>

///<comment>
///<summary>
///Liste des constantes globales propres au fichier :
///
///SIG_ERREUR_AUCUNE : Pas d'erreur
///</summary>
///</comment>

$SIG_ERREUR_AUCUNE = "e0" ;
$SIG_ERREUR_EXECUTE = "e1" ;
$SIG_ERREUR_COUCHE_NOEXIST = "e2" ;
$SIG_ERREUR_FICHIER_NOEXIST = "e3" ;
$SIG_ERREUR_PROJET_NOCOUCHE = "e4" ;

$SEPARATEUR = chr(9) ;


function GetStylePointCartoExcel()
{
	$strRetour = 	".stPoint1CartoExcel {fill:red;stroke:black;stroke-width:0.3%;}" ;
	return $strRetour ;
}

function GetRepresentationPointCartoExcel( $iId, $iX, $iY )
{
	$strRetour = 	"<circle id=\"$iId\" class=\"stPoint1CartoExcel\" cx=\"$iX\" cy=\"$iY\" r=\"0.8%\"/>" ;
	return $strRetour ;
}

function GetLongueursProportionnelles( $iLongueur, $iHauteur )
{
	$tabRetour = array() ;
	$iRatio = 5/6 ;
	if ($iHauteur>($iLongueur*$iRatio))
	{
		$iLongueur = $iHauteur/$iRatio ;
	} else {
		$iHauteur = $iLongueur*$iRatio ;
	}
	$tabRetour[0] = $iLongueur ;
	$tabRetour[1] = $iHauteur ;
	return $tabRetour ;
}

///<comment>
///<summary>
/// Extrait le contenu d'un fichier SVG,
/// g�n�r� depuis notre appli d'extraction
/// MapInfo ou ArcView, dans 3 fichiers
/// distincts : 1 fichier .js qui ne contiendra
/// que les donn�es attributaire de la couche,
/// 1 fichier .frm qui contiendra les donn�es de forme,
/// et 1 fichier .style qui contiendra les styles
/// � appliquer aux formes.
///</summary>
///<params name="strFichierSource">
///Chemin d'acc�s au fichier � s�parer
///</params>
///<params name="strRep">
/// R�pertoire dans lequel se mettront les fichiers une fois g�n�r�s.
///</params>
///<params name="strNouveauNom">
/// Nom � donner aux fichiers g�n�r�s
///</params>
///<params name="idCouche">
/// identifiant de la couche cr��e.
///</params>
///<params name="conn">
/// Connection � la base de donn�es.
///</params>
///<returns>
/// Renvoie le code erreur de l'ex�cution
///</returns>
///</comment>

function ExtractCoucheCartoExcel($strNomFichier, $strRep, $strNouveauNom, $idCouche, $conn) 
{
	global $SEPARATEUR ;

  //V�rification si le fichier existe
  if (file_exists($strNomFichier))
  {
    $bProprietes = 0 ;
    $tabLignes = file($strNomFichier) ;
    $bStyle = 0 ;
    $bEcrireJs = 0 ;
    $bEcrireSvg = 0 ;
    $bEcrireStyle = 0 ;
    $iNumFichierJs = 0 ;
    $iNumFichierSvg = 0 ;
    $iNumFichierStyle = 0 ;
    
    $tabChamps = array() ;
    $iNbChamps = 0 ;
    $tabRows = array() ;
    $iNbRows = 0 ;
    $strIndiceInsee = "ENTITE_INSEE" ;
    $strIndiceType = "ENTITE_TYPE" ;
    
    //Enregistrement des noms des champs
    if ( count($tabLignes)>0 )
    {
    	$strLigne = preg_replace( "/\r/", "", preg_replace( "/\n/", "", $tabLignes[0] )) ;
    	$tabListeChamps = preg_split("/".$SEPARATEUR."/", $strLigne) ;
    	for ($i=0; $i<count($tabListeChamps); $i++)
    	{
    		if  ( $tabListeChamps[$i] != "" )
    		{
	    		$tabChamps[count($tabChamps)] = $tabListeChamps[$i] ;
	    		$iNbChamps += 1 ;
	    		//if  ( strtoupper($tabListeChamps[$i]) == "ENTITE_INSEE" ) { $strIndiceInsee = $i ; }
	    		//if  ( strtoupper($tabListeChamps[$i]) == "ENTITE_TYPE" ) { $strIndiceType = $i ; }
	    	}
    	}
        
    	for( $i=1; $i<count($tabLignes); $i++ )
    	{
    		$strLigne = preg_replace( "/\r/", "", preg_replace( "/\n/", "", $tabLignes[$i] )) ;
    		$tabRows[$iNbRows] = array() ;
    		$tabSplitRows = preg_split("/".$SEPARATEUR."/", $strLigne) ;
    		$iNumChamp = 0 ;
    		for ( $j=0; $j<count($tabSplitRows); $j++ )
    		{
    			if ($tabChamps[$j]!="")
    			{
    				$tabRows[$iNbRows][$tabChamps[$j]] = $tabSplitRows[$j] ;
    				$iNumChamp += 1 ;
    			}
    		}
    		$iNbRows += 1 ;
    	}
    	
    	//cr�ation du fichier .js
			SupprFichier($strRep.$strNouveauNom.".js") ;
			$iNumFichierJs = OpenFichier($strRep.$strNouveauNom.".js",$GLOBALS["FICHIER_MODE_APPEND"]) ;

			//Cr�ation du fichier .frm
			SupprFichier($strRep.$strNouveauNom.".frm") ;
      $iNumFichierSvg = OpenFichier($strRep.$strNouveauNom.".frm",$GLOBALS["FICHIER_MODE_APPEND"]) ;

    	//Cr�ation du fichier .style
    	SupprFichier($strRep.$strNouveauNom.".style") ;
      $iNumFichierStyle = OpenFichier($strRep.$strNouveauNom.".style",$GLOBALS["FICHIER_MODE_APPEND"]) ;
			
			//Ecriture du fichier .style
			$iErreur = EcritFichier($iNumFichierStyle,getStylePointCartoExcel()) ;
			
			//Ecriture du fichier .frm
			$iMinX = 10000000 ;
			$iMinY = 10000000 ;
			$iMaxX = 0 ;
			$iMaxY = 0 ;
			$iErreur = EcritFichier($iNumFichierSvg,"<g id=\"CartoExcel".$idCouche."\">") ;
			for ($i=0; $i<count($tabRows); $i++)
			{
				$strSql = "select e.* ".
											"from CEX_01_ENTITE e, CEX_01_TYPE t ".
											"where e.ENTITE_TYPE = t.TYPE_ID ".
											"and t.TYPE_INTITULE = '".$tabRows[$i][$strIndiceType]."' ".
											"and e.ENTITE_INSEE = '".$tabRows[$i][$strIndiceInsee]."' " ;
				$rsCoord = InitRecordset($strSql, $conn) ;
    		$ociRes = ociFetch($rsCoord) ;
				if ($ociRes)
				{
					$tabRows[$i]["ENTITE_CARTOEXCEL_NOM"] = ociresult($rsCoord,"ENTITE_NOM") ;
					$tabRows[$i]["ENTITE_CARTOEXCEL_INSEE"] = ociresult($rsCoord,"ENTITE_INSEE") ;
        	$tabRows[$i]["CARTOEXCEL_X"] = ociresult($rsCoord,"ENTITE_X") ;
        	$tabRows[$i]["CARTOEXCEL_Y"] = ociresult($rsCoord,"ENTITE_Y") ;
        	if ($tabRows[$i]["CARTOEXCEL_X"]>$iMaxX) { $iMaxX = $tabRows[$i]["CARTOEXCEL_X"] ; }
        	if ($tabRows[$i]["CARTOEXCEL_Y"]>$iMaxY) { $iMaxY = $tabRows[$i]["CARTOEXCEL_Y"] ; }
        	if ($tabRows[$i]["CARTOEXCEL_X"]<$iMinX) { $iMinX = $tabRows[$i]["CARTOEXCEL_X"] ; }
        	if ($tabRows[$i]["CARTOEXCEL_Y"]<$iMinY) { $iMinY = $tabRows[$i]["CARTOEXCEL_Y"] ; }
				} else {
					$tabRows[$i]["CARTOEXCEL_X"] = 0 ;
        	$tabRows[$i]["CARTOEXCEL_Y"] = 0 ;
				}
				$iErreur = EcritFichier($iNumFichierSvg,GetRepresentationPointCartoExcel( ($i) , preg_replace("/,/",".",$tabRows[$i]["CARTOEXCEL_X"]), preg_replace("/,/",".",$tabRows[$i]["CARTOEXCEL_Y"] ))) ;
			}
			$iErreur = EcritFichier($iNumFichierSvg,"</g>") ;

			//Ecriture du fichier .js
			$iErreur = EcritFichier($iNumFichierJs,"[[\"CartoExcel".$idCouche."\",\"points\",null,null,0,null,1,'".$iMinX."','".$iMinY."','".$iMaxX."','".$iMaxY."',0]\n") ;
			$iErreur = EcritFichier($iNumFichierJs,",[\n") ;
			$strJs = "[\"MINX\",\"MINY\",\"WIDTH\",\"HEIGHT\",\"CARTOEXCEL_ID\",\"CARTOEXCEL_NOM\"" ;
			$iNbChamps = 0 ;
			for ($i=0; $i<count($tabChamps); $i++)
			{
				if ( (strtolower($tabChamps[$i]) != "ENTITE_INSEE") && (strtolower($tabChamps[$i]) != "ENTITE_TYPE") )
				{						
					$strJs .= ",\"".$tabChamps[$i]."\"" ;
					$iNbChamps += 1 ;
				}
			}
			$strJs .= "]\n" ;
			$iErreur = EcritFichier($iNumFichierJs,$strJs) ;
			$strJs=",[3,3,3,3,2,1,1" ;
			for (  $i=0; $i<$iNbChamps; $i++ )
			{
				$strJs .= ",1" ;
			}
			$strJs .= "]\n" ;
			$iErreur = EcritFichier($iNumFichierJs,$strJs) ;
			for ($i=0; $i<count($tabRows); $i++)
			{
				$strJs = ",[".preg_replace("/,/",".",$tabRows[$i]["CARTOEXCEL_X"]).",".preg_replace("/,/",".",$tabRows[$i]["CARTOEXCEL_Y"]).",2000,2000" ;
				$strJs .= ",\"".$tabRows[$i]["ENTITE_CARTOEXCEL_INSEE"]."\"" ;
				$strJs .= ",\"".$tabRows[$i]["ENTITE_CARTOEXCEL_NOM"]."\"" ;
				for ($j=0; $j<count($tabChamps); $j++)
				{
					if ( (strtolower($tabChamps[$j]) != "element_insee") && (strtolower($tabChamps[$j]) != "element_type") )
					{						
						$strJs .= ",\"".$tabRows[$i][$tabChamps[$j]]."\"" ;
					}
				}
				$strJs .= "]\n" ;
				$iErreur = EcritFichier($iNumFichierJs,$strJs) ;
			}
			$iErreur = EcritFichier($iNumFichierJs,"]]\n") ;

			//Fermeture du fichier .style
			$iErreur = CloseFichier($iNumFichierStyle) ;

			//Fermeture du fchier .js
			$iErreur = CloseFichier($iNumFichierJs) ;
			
			//Fermeture du fichier .frm
			$iErreur = CloseFichier($iNumFichierSvg) ;
			
			//Calcul de la viewbox
			$iLongueur = ($iMaxX-$iMinX) ;
			$iHauteur = ($iMaxY-$iMinY) ;
			//Respect de la proportionalit�
			$tabLongueurs = GetLongueursProportionnelles( $iLongueur, $iHauteur ) ;
			$iLongueur = $tabLongueurs[0] ;
			$iLargeur = $tabLongueurs[1] ;
			
			$iMinX = $iMinX - ($iLongueur*0.1) ;
			$iMinY = $iMinY - ($iHauteur*0.1) ;
			$iLongueur = ($iLongueur*1.2) ;
			$iHauteur = ($iHauteur*1.2) ;
			
			
			$tabProprietes = array() ;
			$tabProprietes[0] = 600 ; 	//largeur du svg
			$tabProprietes[1] = 500 ;	//hauteur du svg
			$tabProprietes[2] = "".$iMinX." ".$iMinY." ".$iLongueur." ".$iHauteur ;			
		}
	} else {
		return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"] ;
	}
  return SaveCouche($idCouche,$strNouveauNom,$strRep,$tabProprietes,$conn) ;
}

function SaveCouche($idCouche,$strNomFichier,$strRep,$tabProprietes,$conn)
{
  //requ�te pour d�terminer s'il existe d�ja une couche associ�e � ce document.
  $strSql = "select COUCHE_ID from SIG_01_COUCHE where COUCHE_ID=$idCouche";
  $rsExist = InitRecordSet($strSql, $conn);

  if (ocifetch($rsExist)) //si oui, on modifie la couche associ�e,
	{
		$strSql = "update SIG_01_COUCHE " ;
		$strSql .= "set COUCHE_NOMFICHIER='".$strNomFichier."', " ;
		$strSql .= "COUCHE_REPERTOIRE='".$strRep."', " ;
		$strSql .= "COUCHE_WIDTH='".$tabProprietes[0]."', " ;
		$strSql .= "COUCHE_HEIGHT='".$tabProprietes[1]."', " ;
		$strSql .= "COUCHE_VIEWBOX='".$tabProprietes[2]."' " ;
		$strSql .= "where COUCHE_ID=".$idCouche ;
		$iErreur = executeSql($strSql, $conn);
		if ($iErreur<0)
		{
			return $GLOBALS["SIG_ERREUR_EXECUTE"] ;
		} else {
			return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
		}
	} else {
		return $GLOBALS["SIG_ERREUR_COUCHE_NOEXIST"] ;
	}
}

function SaveFondCartoExcel ($couche_id, $couche_fond_id, $conn)
{
	$strSql = "delete from CEX_01_PROJET where COUCHE_ID = ".$couche_id ;
	$iErreur = executeSql($strSql, $conn);
	if ($couche_fond_id>0)
	{
		$strSql = "insert into CEX_01_PROJET (COUCHE_ID, COUCHE_FOND_ID) values ($couche_id,$couche_fond_id)" ;
		$iErreur = executeSql($strSql, $conn);
	}
	return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function GenereProjetSvg($tabProjet,$strCheminSvg,$tabCouches)
{
  $tabJavascript = array() ;
  $tabIdCouches = array() ;
  $tabStyle = array() ;
  $tabFormes = array() ;
  $tabListeCouches = array() ;
  $tabCouchesOK = array() ;
  for ($i=0; $i<count($tabCouches); $i++)
  {
    $tabCouche = $tabCouches[$i][0] ;
    if (file_exists($tabCouche[9].$tabCouche[8].".js"))
    {
      if (file_exists($tabCouche[9].$tabCouche[8].".frm"))
        {
          if (file_exists($tabCouche[9].$tabCouche[8].".style"))
            {
              $tabCouchesOK[count($tabCouchesOK)] = $tabCouches[$i] ;
              $tabListeCouches[count($tabListeCouches)] = $tabCouche ;
              $tabJavascript[count($tabJavascript)] = file($tabCouche[9].$tabCouche[8].".js") ;
              $tabIdCouches[count($tabIdCouches)] = $tabCouche[0] ;
              $tabStyle[count($tabStyle)] = file($tabCouche[9].$tabCouche[8].".style") ;
              $tabFormes[count($tabFormes)] = file($tabCouche[9].$tabCouche[8].".frm") ;
            } else {
            	echo $tabCouche[9].$tabCouche[8].".style introuvable !<br>" ;
              return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
            }
        } else {
        	echo $tabCouche[9].$tabCouche[8].".frm introuvable !<br>" ;
          return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
        }
    } else {
    	echo $tabCouche[9].$tabCouche[8].".js introuvable !<br>" ;
      return $GLOBALS["SIG_ERREUR_FICHIER_NOEXIST"];
    }
  }
  if (count($tabJavascript)>0) //il y a donc des couches dans ce projet
  {
      SupprFichier($strCheminSvg) ;
      $iNumFichierNewSvg = OpenFichier($strCheminSvg,$GLOBALS["FICHIER_MODE_APPEND"]) ;
      $iErreur = EcritEnteteSvg($iNumFichierNewSvg,$tabProjet[0],$tabProjet[1],$tabProjet[2]) ;
      $iErreur = EcritContenuJavaScript($iNumFichierNewSvg,$tabJavascript,$tabListeCouches,true) ;
      $iErreur = EcritContenuStyle($iNumFichierNewSvg,$tabStyle) ;
      $iErreur = EcritContenuFormes($iNumFichierNewSvg,$tabFormes) ;
      $iErreur = EcritJavascriptProjet($iNumFichierNewSvg,$tabProjet[4],$tabCouchesOK) ;
      $iErreur = EcritFinSvg($iNumFichierNewSvg) ;
      $iErreur = CloseFichier($iNumFichierNewSvg) ;
  } else {
    return $GLOBALS["SIG_ERREUR_PROJET_NOCOUCHE"] ;
  }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function EcritEnteteSvg($iNumFichier,$iWidth,$iHeight,$strViewBox)
{
  $strNoViewSource = "".
      "<defs>".
      "<menu id=\"CustomMenu\">".
      "<header>Carte en SVG</header>".
      "<item action=\"Help\">Aide...</item>".
      "<item action=\"About\">A propos de SVG Viewer...</item>".
      "</menu>".
      "</defs>".
      "<script>".
      "<![CDATA[".
      "var newMenuRoot = parseXML( printNode( document.getElementById( 'CustomMenu' ) ), contextMenu );".
      "contextMenu.replaceChild( newMenuRoot, contextMenu.firstChild );".
      "]]>".
      "</script>" ;

  if ($iNumFichier)
    {
      $iErreur = EcritFichier($iNumFichier,"<?xml version='1.0' encoding='iso-8859-1'?>") ;
      $iErreur = EcritFichier($iNumFichier,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\" >") ;
      $iErreur = EcritFichier($iNumFichier,"<svg width='$iWidth' height='$iHeight' viewBox='$strViewBox' zoomAndPan='disable' onload='LoadThisSvg(evt)'>") ;
      //$iErreur = EcritFichier($iNumFichier,$strNoViewSource) ;
      $iErreur = EcritFichier($iNumFichier,"<script><![CDATA[") ;
      $iErreur = EcritFichier($iNumFichier,"var bIsLoaded=0;") ;
      $iErreur = EcritFichier($iNumFichier,"function LoadThisSvg(evt) { bIsLoaded=1; }") ;
      $iErreur = EcritFichier($iNumFichier,"function IsLoaded() { return bIsLoaded==1; }") ;
      $iErreur = EcritFichier($iNumFichier,"]]></script>") ;
    }
  //  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

function EcritContenuJavaScript($iNumFichier,$tableau,$tabCouches,$bProjet)
{
  if ($iNumFichier)
  {
    $iErreur = EcritFichier($iNumFichier,"<script><![CDATA[") ;
    $iErreur = EcritFichier($iNumFichier,"tabLayers = new Array(") ;
    for ($i=0; $i<count($tableau); $i++)
    {
      if ($i>0)
      {
        $iErreur = EcritFichier($iNumFichier,",") ;
      }
      $n = count($tableau[$i]) ;
      for ($j=0; $j<$n; $j++)
      {
        if ($j==0)
        {
          if ($bProjet==true)
          {
            $tabSplit = preg_split("/,/",$tableau[$i][$j]) ;
            $tabSplit[0] = "[[\"".$tabCouches[$i][2]."\"" ; //Nom de la couche
            if ($tabCouches[$i][4]=="")
            {
              $tabSplit[2] = "null" ; //Echelle min
            } else {
              $tabSplit[2] = "\"".$tabCouches[$i][4]."\"" ; //Echelle min
            }
            if ($tabCouches[$i][5]=="")
            {
              $tabSplit[3] = "null" ; //Echelle max
            } else {
              $tabSplit[3] = "\"".$tabCouches[$i][5]."\"" ; //Echelle max
            }
            $tabSplit[4] = $tabCouches[$i][3] ; // Dans la localisation ?
            if ($tabCouches[$i][6]=="")
            {
              $tabSplit[5] = "null" ; // infobulles
            } else {
              $tabSplit[5] = "\"".$tabCouches[$i][6]."\"" ; // infobulles
            }
            $tabSplit[6] = $tabCouches[$i][1] ; // Visible ?
            $tabSplit[count($tabSplit)-1] = $tabCouches[$i][0]."]" ; // num�ro de couche dans Oracle
            $tableau[$i][$j] = "" ;
            for ($k=0; $k<count($tabSplit); $k++)
            {
              if ($k>0)
              {
                $tableau[$i][$j] .= "," ;
              }
              $tableau[$i][$j] .= $tabSplit[$k] ;
            }
          } else {
            $tabParamJs = preg_split("/,/",$tableau[$i][$j]) ;
            //Remplacement de l'�chelle Min
            if ($tabCouches[$i][1]!="")
            {
              $tabParamJs[2] = $tabCouches[$i][1] ;
            }  
            //Remplacement de l'�chelle Max
            if ($tabCouches[$i][2]!="")
            {
              $tabParamJs[3] = $tabCouches[$i][2] ;
            }
            //Remplacement de l'id de la couche
            //$tabParamJs[count($tabParamJs)-1] = ereg_replace(",0]",",".$tabCouches[$i][0]."]",$tabParamJs[count($tabParamJs)-1]) ;
            $tabParamJs[count($tabParamJs)-1] = $tabCouches[$i][0]."]" ;
            $tableau[$i][$j] = join(",",$tabParamJs) ;
          }
        }
        $iErreur = EcritFichier($iNumFichier,$tableau[$i][$j]) ;
      }
    }
    $iErreur = EcritFichier($iNumFichier,");]]></script>") ;
  }
  //  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

function GetContenuJavaScript($tableau)
{
  echo "<script><![CDATA[\n" ;
  echo "tabLayers = new Array(\n" ;
  for ($i=0; $i<count($tableau); $i++)
  {
    $n = count($tableau[$i]) ;
    for ($j=0; $j<$n; $j++)
    {
      echo $tableau[$i][$j] ;
    }
  }
  echo ");]]></script>\n" ;
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function EcritContenuStyle($iNumFichier,$tableau)
{
  if ($iNumFichier)
    {
      $iErreur = EcritFichier($iNumFichier,"<defs>") ;
      $iErreur = EcritFichier($iNumFichier,"<style type='text/css'>") ;
      $iErreur = EcritFichier($iNumFichier,"<![CDATA[") ;
      for ($i=0; $i<count($tableau); $i++)
        {
          $n = count($tableau[$i]) ;
          for ($j=0; $j<$n; $j++)
          {
            $iErreur = EcritFichier($iNumFichier,$tableau[$i][$j]) ;
          }
        }
      $iErreur = EcritFichier($iNumFichier,"]]>") ;
      $iErreur = EcritFichier($iNumFichier,"</style>") ;
      $iErreur = EcritFichier($iNumFichier,"</defs>") ;
    }
  //  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}
function GetContenuStyle($tableau)
{
  echo "<defs>" ;
  echo "<style type='text/css'>" ;
  echo "<![CDATA[" ;
  for ($i=0; $i<count($tableau); $i++)
  {
    $n = count($tableau[$i]) ;
    for ($j=0; $j<$n; $j++)
    {
      echo $tableau[$i][$j] ;
    }
  }
  echo "]]>" ;
  echo "</style>" ;
  echo "</defs>" ;
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function EcritContenuFormes($iNumFichier,$tableau)
{
  if ($iNumFichier)
    {
      $iErreur = EcritFichier($iNumFichier,"<g id=\"lyrLayers\">") ;
      for ($i=0; $i<count($tableau); $i++)
      {
        $n = count($tableau[$i]) ;
        for ($j=0; $j<$n; $j++)
        {
          $iErreur = EcritFichier($iNumFichier,$tableau[$i][$j]) ;
        }
      }
      $iErreur = EcritFichier($iNumFichier,"</g>") ;
    }
  //return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}
function GetContenuFormes($tableau)
{
  for ($i=0; $i<count($tableau); $i++)
  {
    $n = count($tableau[$i]) ;
    for ($j=0; $j<$n; $j++)
    {
      echo $tableau[$i][$j] ;
    }
  }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function EcritJavascriptProjet($iNumFichier,$iCoucheEditable,$tabListeCouches)
{
  if ($iNumFichier)
  {
    $iErreur = EcritFichier($iNumFichier,"<script><![CDATA[\n") ;
    $iErreur = EcritFichier($iNumFichier,"tabProjet = new Array(\n") ;
    $iErreur = EcritFichier($iNumFichier,"[".$iCoucheEditable."],") ;
    $iErreur = EcritFichier($iNumFichier,"[\n") ;
    $iNbAnalyse = 0 ;
    for ($i=0; $i<count($tabListeCouches); $i++)
    {
      if(count($tabListeCouches[$i][1])>0)
      {
        $tabAnalyse = $tabListeCouches[$i][1] ;

        //Ecriture d'une nouvelle analyse th�matique
        if ($iNbAnalyse>0)
        {
          $iErreur = EcritFichier($iNumFichier,",\n") ;
        }

        //Ecriture de l'identifiant de la couche
        $strAnalyse = "[[".$i."],\n" ;
        
        //Ecriture du tableau "tabAnalyse"
        $strAnalyse .=  "[" ;
        for ($j=0; $j<count($tabAnalyse); $j++)
        {
          if ($j>0)
          {
            $strAnalyse .= "," ;
          }
          $strAnalyse .= $tabAnalyse[$j] ;
        }
        $strAnalyse .= "],\n" ;
        
        //Ecriture du tableau "tabClasse"
        $strAnalyse .= "[" ;
        if(count($tabListeCouches[$i][2])>0)
        {
          $tabClasses = $tabListeCouches[$i][2] ;
          for ($j=0; $j<count($tabClasses); $j++)
          {
            if ($j>0)
            {
              $strAnalyse .= "," ;
            }
            $strAnalyse .= "[" ;  
            for ($k=0; $k<count($tabClasses[$j]); $k++)
            {
              if ($k>0)
              {
                $strAnalyse .= "," ;
              }
              $strAnalyse .= $tabClasses[$j][$k] ;
            }
            $strAnalyse .= "]" ;  
          }
        }
        $strAnalyse .= "],\n" ;

        //Ecriture du tableau tabSymbol
        $strAnalyse .=  "[" ;
        if(count($tabListeCouches[$i][3])>0)
        {
          $tabSymbol = $tabListeCouches[$i][3] ;
          for ($j=0; $j<count($tabSymbol); $j++)
          {
            if ($j>0)
            {
              $strAnalyse .= "," ;
            }
            $strAnalyse .= $tabSymbol[$j] ;
          }
        }
        $strAnalyse .= "]]\n" ;

        //Ecriture de la table 
        $iErreur = EcritFichier($iNumFichier,$strAnalyse) ;
        $iNbAnalyse += 1 ;
      }
    }
    $iErreur = EcritFichier($iNumFichier,"]") ;
    $iErreur = EcritFichier($iNumFichier,");]]></script>") ;
  }
  return $iErreur ;
}

function EcritFinSvg($iNumFichier)
{
  if ($iNumFichier)
    {
      $iErreur = EcritFichier($iNumFichier,"</svg>") ;
    }
  //  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
  return $iErreur ;
}

function GetFinSvg()
{
  echo "</svg>" ;
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function EcritEnteteSvgZip($iNumFichier,$iWidth,$iHeight,$strViewBox)
{
  if ($iNumFichier)
    {
      $iErreur = EcritFichierZip($iNumFichier,"<?xml version='1.0' encoding='iso-8859-1'?>") ;
      $iErreur = EcritFichierZip($iNumFichier,"<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 20000303 Stylable//EN' 'http://www.w3.org/TR/2000/03/WD-SVG-20000303/DTD/svg-20000303-stylable.dtd'>") ;
      $iErreur = EcritFichierZip($iNumFichier,"<svg width='$iWidth' height='$iHeight' viewBox='$strViewBox' onload='LoadThisSvg(evt)'>") ;
      $iErreur = EcritFichierZip($iNumFichier,"<script><![CDATA[") ;
      $iErreur = EcritFichierZip($iNumFichier,"var bIsLoaded=0;") ;
      $iErreur = EcritFichierZip($iNumFichier,"function LoadThisSvg(evt) { bIsLoaded=1; }") ;
      $iErreur = EcritFichierZip($iNumFichier,"function IsLoaded() { return bIsLoaded==1; }") ;
      $iErreur = EcritFichierZip($iNumFichier,"]]></script>") ;
    }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function EcritContenuJavaScriptZip($iNumFichier,$tableau)
{
  if ($iNumFichier)
    {
      $iErreur = EcritFichierZip($iNumFichier,"<script><![CDATA[") ;
      $iErreur = EcritFichierZip($iNumFichier,"tabLayers = new Array(") ;
      for ($i=0; $i<count($tableau); $i++)
        {
          $n = count($tableau[$i]) ;
          for ($j=0; $j<$n; $j++)
            {
              $iErreur = EcritFichierZip($iNumFichier,$tableau[$i][$j]) ;
            }
        }
      $iErreur = EcritFichierZip($iNumFichier,");]]></script>") ;
    }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function EcritContenuStyleZip($iNumFichier,$tableau)
{
  if ($iNumFichier)
    {
      $iErreur = EcritFichierZip($iNumFichier,"<defs>") ;
      $iErreur = EcritFichierZip($iNumFichier,"<style type='text/css'>") ;
      $iErreur = EcritFichierZip($iNumFichier,"<![CDATA[") ;
      for ($i=0; $i<count($tableau); $i++)
        {
          $n = count($tableau[$i]) ;
          for ($j=0; $j<$n; $j++)
            {
              $iErreur = EcritFichierZip($iNumFichier,$tableau[$i][$j]) ;
            }
        }
      $iErreur = EcritFichierZip($iNumFichier,"]]>") ;
      $iErreur = EcritFichierZip($iNumFichier,"</style>") ;
      $iErreur = EcritFichierZip($iNumFichier,"</defs>") ;
    }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function EcritContenuFormesZip($iNumFichier,$tableau)
{
  if ($iNumFichier)
    {
      for ($i=0; $i<count($tableau); $i++)
        {
          $n = count($tableau[$i]) ;
          for ($j=0; $j<$n; $j++)
            {
              $iErreur = EcritFichierZip($iNumFichier,$tableau[$i][$j]) ;
            }
        }
    }
  return $GLOBALS["SIG_ERREUR_AUCUNE"] ;
}

function EcritFinSvgZip($iNumFichier)
{
  if ($iNumFichier)
    {
      $iErreur = EcritFichierZip($iNumFichier,"</svg>") ;
    }
}

function SupprDocCEX($doc_id)
{
	global $conn ;
	$strSql = "delete from CEX_01_PROJET where COUCHE_ID IN (".
								"select COUCHE_ID from SIG_01_COUCHE where DOC_ID=".$doc_id.")";
	executeSql($strSql, $conn) ;
}
?>