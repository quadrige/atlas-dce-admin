<?php
include_once("../../libconf/lib_affmenu.php");

/**
 * @brief Affiche le haut de la page
 *
 * @param tabEventBody  tableau contenant les couples nom de l'�v�nement / nom de la fonction 
 *                      javascript appel�e (pas n�cessaire d'ajouter 'javascript:') 
 */
function aff_menu_haut($tabEventBody=null)
{
  global $cont_id, $cont_appli_id;

  echo getHtmlMainHeader();

  echo "<script language='javascript' src='../../lib/lib_js.js'></script>";
  echo "<script language='JavaScript'>".
    " var navigateur = null;".
    " var x = null;".
    " var y = null;".
    " var bWinLoaded = 0;".
    " var menuBar = null;".
    " var ALK_SIALKE_URL = '".ALK_SIALKE_URL."';".
    " var ALK_URL_ASPIC = '".ALK_URL_ASPIC."';".
    " var ALK_DIR_ANNU = '".ALK_DIR_ANNU."';".
    " var ALK_DIR_ESPACE = '".ALK_DIR_ESPACE."';".
    " var ALK_URL_IDENTIFICATION = '".ALK_URL_IDENTIFICATION."';".
    " var ALK_ROOT_URL = '".ALK_ROOT_URL."';".
    (defined("ALK_URL_SERVEUR_DIST") ? "var ALK_URL_SERVEUR_DIST = '".ALK_URL_SERVEUR_DIST."';" : "") .
    " var strTitrePage = '';".
    " var strTitreAliasPage = '';".
    " var cont_id = '".$cont_id."';".
    " var cont_appli_id = '".$cont_appli_id."';".

    " function onLoadInit() { bWinLoaded = 1; document.onmousemove = position; window.focus(); } ";
  
  echo "</script>".
    "<script language='javascript' src='../../lib/lib_jsaddon.js'></script>".
    "</head>".

    $strBodyEvent = "";
  
  if( is_array($tabEventBody) ) {
    while( list($strEvent, $strFunctionJs) = each($tabEventBody) )
      $strBodyEvent .= " ".$strEvent."=\"javascript:".$strFunctionJs."\"";
  } else {
    $strBodyEvent = " onload='onLoadInit()'";	
  }

  echo "<body bgcolor='#FFFFFF' text='#000000' ". 
    " leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' ".$strBodyEvent.">";
}

/**
 * @brief Affiche le bas de la page
 */
function aff_menu_bas()
{
  echo "</html>";
}

//
// les fonctions ci-dessous sont appel�es � disparaitre
//

/** Affiche le r�sultat de la fonction getHtmlHeaderPage (lib_affmenu.php) */
function aff_page_haut($bModif, $strTitre, $iLarg="606")
{
  echo "<table width=100% border=0 cellspacing=0 cellpadding=0>".
    "<tr>".
    "<td width=22 bgcolor=".ALK_COL_STR_BG_COLOR_2.">".
    "<img src=\"".ALK_URL_SI_MEDIA."pictos/".($bModif==true ? "picto_modif_doc.gif" : "picto_info_doc.gif")."\"".
    " width=22 height=22></td>".
    "<td bgcolor=".ALK_COL_STR_BG_COLOR_2." class=ContenuBlancBD>".$strTitre."</td>".
    "</tr>".
    "</table>".

    "<table width=100% border=0 cellspacing=0 cellpadding=0>".
    "<tr>".
    "<td width=20>".
    "<img src=\"".ALK_URL_SI_MEDIA."transp.gif\" width=20 height=1></td>".
    "<td width=1 bgcolor=".ALK_COL_STR_COLOR_LINE.">".
    "<img width=1 height=1></td>".
    "<td>";
}

/** Affiche le r�sultat de la fonction getHtmlLineFrame (lib_affmenu.php) */
function Aff_page_cadre_separateur($iLarg="606")
{
  echo "<table width=100% border=0 cellspacing=0 cellpadding=0>".
    "<tr>".
    "<td height=1 bgcolor=".ALK_COL_STR_COLOR_LINE.">".
    "<img width=1 height=1></td>".
    "</tr>".
    "</table>";
}

/** Affiche le r�sultat de la fonction getHtmlHeaderFrame (lib_affmenu.php) */
function Aff_page_cadre_haut($iLarg="606", $ihaut="5")
{
   echo "<table width=100% border=0 cellspacing=0 cellpadding=0 bgcolor=#ffffff>";
  if( $ihaut>0 )
    echo "<tr><td height=".$ihaut."><img width=1 height=1></td></tr>";
  echo "<tr><td align=left valign=top>";
}

/** Affiche le r�sultat de la fonction getHtmlFooterFrame (lib_affmenu.php) */
function Aff_page_cadre_bas($ihaut="5")
{
  echo "</tr>";
  if( $ihaut>0 )
    echo "<tr><td height=".$ihaut."></td></tr>";
  echo "</table>";
}

/** Affiche le r�sultat de la fonction getHtmlFooterPage (lib_affmenu.php) */
function aff_page_bas()
{
  echo "</td>".
    "<td width=1 bgcolor=".ALK_COL_STR_COLOR_LINE.">".
    "<img width=1 height=1></td>".
    "</tr>".
    "</table>";
}

?>