var g_oCtrlFocusOnError = null;
function isRadioRequire(oCtrl) {
    var nbElt = oCtrl.length;    var bRes = false;
    for(var i=0; i<nbElt; i++) bRes = bRes || oCtrl[i].checked;
    return bRes;
}
function AlkAlertMsg(oCtrl, strMsg)
{
	// affiche l'alerte
  alert(strMsg);
  
  // si le formulaire est de type AlkFormSheets, s�lectionne l'onglet
  if( oCtrl.alkFormSheetIndex != "-1" && AlkSheetOnClick )
	  AlkSheetOnClick(oCtrl.alkFormName, oCtrl.alkFormSheetIndex);
	
	// fixe le focus sur le ctrl fautif
  if( oCtrl.focus ) 
  	oCtrl.focus();
  	
  // m�morise le ctrl fautif
  g_oCtrlFocusOnError = oCtrl; 
  return;
}

/**
 * NB : si oCtrl.alkType=="select", oMin = liste des index interdits sous forme de chaine s�par�s par ','
 */
function AlkVerifCtrl(f)
{
  var nbCtrl = f.elements.length;
  for(i=0; i<nbCtrl; i++) {
    var oCtrl = f.elements[i];
    if( typeof( oCtrl) != "undefined" &&
        typeof( oCtrl.bAlk) != "undefined" ) {
      var strLabel = oCtrl.alkLabel;
      var bRequire = oCtrl.alkRequire;
      var strType  = oCtrl.alkType;
      var oMin = oCtrl.alkMin;
      var oMax = oCtrl.alkMax;
      var strMsgErr = oCtrl.alkMsgErr;
      var strExcept = oCtrl.alkExcept;
      var bStrict = oCtrl.alkStrict;

      if( oCtrl.type && oCtrl.type != "file" && window.AlkReplaceSpecialChar )
        AlkReplaceSpecialChar(oCtrl);

      if( bRequire == true ) {
        if( strType=="memo" || strType=="text" || strType=="mail" || strType=="heure5" ||
            strType=="date10" || strType=="int" || strType=="textnum" || strType=="textalpha" ) {
          if( String.trim ) {
            var oValue = new String(oCtrl.value);
            oCtrl.value = oValue.trim();
          }
          if( oCtrl.value=="" ) { 
             AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr : "Le champ "+strLabel+" est obligatoire.")); 
             return false; 
          }
        } else {
          if( strType == "radio" ) {
            if( isRadioRequire(oCtrl) == false ){
               AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr : "Le champ "+strLabel+" est obligatoire.")); 
               return false;
            }
          } else if( strType == "checkgroup" ) {
            if( isCheckboxRequire(f, oCtrl) == false ){
               AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr : "Le champ "+strLabel+" n�cessite qu'une case, au moins, soit coch�e."));
               return false; 
            }
          } else if ( strType == "select"){
            if( isSelectRequire(f, oCtrl, oMin) == false ){
               AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr : "Le champ "+strLabel+" est obligatoire."));
               return false; 
            }
          }
        } 
      }

      switch( strType ) {
        case "memo" :
        case "text" :
          if( oCtrl.value != "" ) {
            if( oMin.toString() != "" && oCtrl.value.length<oMin ) {
              AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr +"\nLe champ "+strLabel+" n�cessite au moins "+oMin.toString()+" caract�res." : "Le champ "+strLabel+" n�cessite au moins "+oMin.toString()+" caract�res."));
              return false;
            } 
            if( oMax.toString() != "" && oCtrl.value.length>oMax ) {
              AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr +"\nLe champ "+strLabel+" est limit� � "+oMax.toString()+" caract�res." : "Le champ "+strLabel+" est limit� � "+oMax.toString()+" caract�res."));
              return false;
            } 
          }
          break;

        case "textnum" :
          if( oCtrl.value != "" ) {
          var bExcept=false;
              
            if (strExcept != "") {
              var reg=new RegExp("[|]+", "g");
              var tabExcept=strExcept.split(reg);
              for (var j=0; j<tabExcept.length; j++) {
                if ( oCtrl.value == tabExcept[j]) {
                  bExcept=true;
                  break;
                }
              }
            }
            
            strReason = verifTextNum(oCtrl.value, oMin, oMax);
            if( strReason != "" && bExcept==false ) {
              AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr +"\nLe champ "+strLabel+" "+strReason : "Le champ "+strLabel+" "+strReason));
              return false;
            }
          }
          break;

        case "textalpha" :
          if( oCtrl.value != "" ) {
            strReason = verifTextAlpha(oCtrl.value, oMin, oMax);
            if( strReason != "" ) {
              AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr +"\nLe champ "+strLabel+" "+strReason : "Le champ "+strLabel+" "+strReason));
              return false;
            }
          }
          break;

        case "int" :
          if( oCtrl.value != "" ) {
            var bExcept=false;
              
            if (strExcept != "") {
              var reg=new RegExp("[|]+", "g");
              var tabExcept=strExcept.split(reg);
              for (var j=0; j<tabExcept.length; j++) {
                if ( oCtrl.value == tabExcept[j]) {
                  bExcept=true;
                  break;
                }
              }
            }

            strReason = verifNumValid(oCtrl.value, oMin, oMax, bStrict);
            if( strReason != "" && bExcept==false ) {
              AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr +"\nLe champ "+strLabel+" n�cessite un nombre"+strReason : "Le champ "+strLabel+" n�cessite un nombre"+strReason));
              return false;
            }
          }
          break;

       case "date10" : 
          if( oCtrl.value!="" ) {
            strReason = verifDateValidJJMMAAAA(oCtrl.value, oMin, oMax);
            if( strReason != "" ) {
              AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr +"\nLe champ "+strLabel+" n�cessite une date"+strReason : "Le champ "+strLabel+" n�cessite une date"+strReason)); 
              return false; 
            }
          }
          break;
       
       case "heure5" : 
          if( oCtrl.value!="" ) {
            strReason = verifHeureValidHHMN(oCtrl.value, oMin, oMax);
            if( strReason != "" ) {
              AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr +"\nLe champ "+strLabel+" n�cessite une heure"+strReason : "Le champ "+strLabel+" n�cessite une heure"+strReason)); 
              return false; 
            }
          }
          break;

       case "datetime16" : 
          if( oCtrl.value!="" ) {
            strReason = verifDateTimeValidJJMMAAAA_HHMN(oCtrl.value, oMin, oMax);
            if( strReason != "" ) {
              AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr +"\nLe champ "+strLabel+" n�cessite une date et une heure"+strReason : "Le champ "+strLabel+" n�cessite une date et une heure"+strReason)); 
              return false; 
            }
          }
          break;

       case "mail" : 
         if( oCtrl.value != "" ) {
           strReason = verifMailValid(oCtrl.value);
          if( strReason != "" ) {
            AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr +"/nLe champ "+strLabel+" n�cessite une adresse �lectronique valide.\n"+strReason : "Le champ "+strLabel+
                                                             " n�cessite une adresse �lectronique valide.\n"+strReason)); 
            return false;             
          }
        }
        break;
        
       case "checkgroup" :
         strReason = verifNbCheck(f, oCtrl, oMin, oMax);
         if( strReason != "" ) {
           AlkAlertMsg(oCtrl, (strMsgErr!="" ? strMsgErr +"\nLe champ "+strLabel+" "+strReason : "Le champ "+strLabel+" "+strReason));
           return false;
         }
       break;
      }
    }
  }
  return true;
}

function AlkAddCtrl(formName, ctrlName, typeCtrl, bRequire, strLabel, oMin, oMax) 
{
  var strMsg = "";
  if( AlkAddCtrl.arguments.length > 7 ) {
    // r�cup�re le param optionnel : message d'erreur personnalis�
    strMsg = AlkAddCtrl.arguments[7];
  }
  
  var strExcept = "";
  if( AlkAddCtrl.arguments.length > 8 ) {
    // r�cup�re le param optionnel : strExcept
    strExcept = AlkAddCtrl.arguments[8];
  }

  var bStrict = true;
  if( AlkAddCtrl.arguments.length > 9 ) {
    // r�cup�re le param optionnel : bStrict
	  bStrict = AlkAddCtrl.arguments[9];
  }

  var iFormSheetIndex = "-1";
  if( AlkAddCtrl.arguments.length > 10 ) {
    // r�cup�re le param optionnel : iFormSheetIndex
	  iFormSheetIndex = AlkAddCtrl.arguments[10];
  }

  var f = eval("document."+formName);
  var oCtrl = ( f ? f.elements[ctrlName] : null );
  if( oCtrl ) {
    oCtrl.bAlk = true;
    oCtrl.alkType = typeCtrl;
    oCtrl.alkRequire = bRequire;
    oCtrl.alkLabel = strLabel;
    oCtrl.alkMin = oMin;
    oCtrl.alkMax = oMax;
    oCtrl.alkMsgErr = strMsg;
    oCtrl.alkExcept = strExcept;
    oCtrl.alkStrict = bStrict;
    oCtrl.alkFormName = formName;
    oCtrl.alkFormSheetIndex = iFormSheetIndex;
  }
}

/** 
 * Exemple de fonction de validation
 *
var AlkNbClickValid = 0;
function ValidateForm()
{
  var f = document.formName;
  if( AlkNbClickValid == 0 ) {
    var bRes = AlkVerifCtrl(f);
    if( bRes == true ) {
      AlkNbClickValid++;
      f.submit();
    }
  }
}
*/
