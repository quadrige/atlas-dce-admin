<?
/**
 * @brief Inclusion de la nouvelle API
 *
 * Variables globales initialis�es :
 *   $dbConn              Instance de la classe DbXxx
 *   $queryAnnu           Instance de la classe QueryAnnuXxx
 *   $queryCont           Instance de la classe QueryContXxx
 *   $queryContAnnuAction Instance de la classe QueryContAnnuActionXxx
 *  avec Xxx = Oracle, Mysql ou Pgsql
 *
 *   $oSpace              Instance de la classe Workspace
 *   $cont_id             Identifiant de l'espace en cours
 *   $appli_id et
 *   $cont_appli_id       Identifiant de l'application en cours
 *
 *   $conn                handle de connexion � la base de donn�es
 *
 * si ALK_B_AD
 *   $adldap             Instance de la classe adLDAP (avec v�rification de l'authentification)
 */
if( ALK_B_API_CONN_V2 == true ) {
  $dbConn = null;
  $dbConnSig = null;
  $queryAnnu = null;
  $queryCont = null;
  $queryContAnnuAction = null;

  $querySearch = null;
  $querySearchAction = null;

  include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."alkobject.class.php");
	include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."alkquery.class.php");
	include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."alkdataform.class.php");

  include_once("./../".ALK_DIR_ESPACE."classes/querycont.class.php");
  include_once("./../".ALK_DIR_ESPACE."classes/querycontannu_action.class.php");
  include_once("./../".ALK_DIR_ANNU."classes/queryannu.class.php");
  
  switch( ALK_TYPE_BDD ) {
  case SGBD_ORACLE:
    include_once("./../../classes/err/errOracle.class.php");
    include_once("./../../classes/db/droracle.class.php");
    include_once("./../../classes/db/dsoracle.class.php");
    include_once("./../../classes/db/dboracle.class.php");

    $dbConn = new DbOracle(ALK_ORA_LOGIN, ALK_ORA_PWD, ALK_ORA_SID);
    $dbConn->connect();
    break;

  case SGBD_MYSQL:
    include_once("./../../classes/err/errMysql.class.php");
    include_once("./../../classes/db/drmysql.class.php");
    include_once("./../../classes/db/dsmysql.class.php");
    include_once("./../../classes/db/dbmysql.class.php");
    
    $dbConn = new DbMySql(ALK_MYSQL_LOGIN,  ALK_MYSQL_HOST,  ALK_MYSQL_PWD,  ALK_MYSQL_BD,  "",  ALK_MYSQL_PORT);
    $dbConn->connect();
    break;
    
  case SGBD_POSTGRES:
    include_once("./../../classes/err/errPgsql.class.php");
    include_once("./../../classes/db/drpgsql.class.php");
    include_once("./../../classes/db/dspgsql.class.php");
    include_once("./../../classes/db/dbpgsql.class.php");
    
    $dbConn = new DbPgSql(ALK_POSTGRES_LOGIN,  ALK_POSTGRES_HOST,  ALK_POSTGRES_PWD,  ALK_POSTGRES_BD,  ALK_POSTGRES_PORT);
    $dbConn->connect();
    break;
  }

  if( defined("ALK_B_APPLI_SEARCH") && ALK_B_APPLI_SEARCH==true ) {
    include_once("./../search/classes/querysearch.class.php");
    include_once("./../search/classes/querysearch_action.class.php");

    $querySearch = new QuerySearch($dbConn);
    $querySearchAction = new QuerySearchAction($dbConn);
  }

  $queryCont = new QueryCont($dbConn);
  $queryAnnu = new QueryAnnu($dbConn);
  $queryContAnnuAction = new QueryContAnnuAction($dbConn);

  // par d�faut, m�me connexion entre base spatiale et sgbd
  $dbConnSig =& $dbConn;
  
  // cr�ation de l'objet workspace
  include_once("./../../classes/".ALK_DIR_CLASSE_APPLI."alkworkspace.class.php");

  if( !isset($cont_id) )
    $cont_id = Request("cont_id", REQ_GET, "-1", "is_numeric");
   if( !isset($cont_appli_id) )
     $appli_id = Request("cont_appli_id", REQ_GET, "-1", "is_numeric");
   else
     $appli_id = $cont_appli_id;
   $cont_appli_id = $appli_id;

   $oSpace = new AlkWorkSpace($cont_id, $appli_id, $dbConn, ALK_TYPE_BDD, 
                              $queryCont, $queryAnnu, $queryContAnnuAction,
                              (isset($_SESSION["sit_idUser"]) ? $_SESSION["sit_idUser"]: "-1"), 
                              (isset($_SESSION["sit_idUserProfil"]) ? $_SESSION["sit_idUserProfil"] : "-1"),
                              (defined("ALK_B_REMOVE_PRIV_PROFIL1_SPACE") ? ALK_B_REMOVE_PRIV_PROFIL1_SPACE : false),
                              (isset($_SESSION["sit_idUserAdminAnnu"]) ? $_SESSION["sit_idUserAdminAnnu"] : "0"));
   
  // cohabitation des 2 API : menu (APIv1) + contenu (APIv2)
  $conn = null;
  if( $dbConn != null ) 
    $conn =& $dbConn->conn;
}

//Cr�ation de la connexion Active directory
if (defined("ALK_B_AD") && ALK_B_AD == true){
  include_once("./../../classes/ldap/adLDAP.php");
  $tabdomainControllers = array (ALK_DOMAINE_CONTROL);
  $adldap = new adLDAP($tabdomainControllers,ALK_ACCOUNT_SUFFIX,ALK_BASE_DN);
  $usert = ALK_AD_USER;
  $pass = ALK_AD_PWD;  
  $adldap->authenticate($usert,$pass);
} 
  

/*----------------------------------------------------------------------------------
// InitConnection
// But : Ouvre une connexion Oracle
// Sans param�tre
// Retourne un entier correspondant � la connexion ouverte
//----------------------------------------------------------------------------------
function InitConnection()
{
  global $oraLogin, $oraPwd, $oraSID, $conn;
  if( !isset($conn) ) {
    $conn = ociLogon(ALK_ORA_LOGIN, ALK_ORA_PWD, ALK_ORA_SID);
    return $conn;
  }
  return $conn;
}

//----------------------------------------------------------------------------------
// Initialisation de la connexon pour APIv1 uniquement
//----------------------------------------------------------------------------------
if( ALK_B_API_CONN_V2 == false ) {
  $conn = InitConnection();
}


//----------------------------------------------------------------------------------
// Disconnect
// But : Ferme la connexion Oracle
// param�tre optionnel :
//    $conn   = entier correspondant � la connexion ouverte
// 
//----------------------------------------------------------------------------------
function Disconnect()
{
  global $conn;
  $numargs = func_num_args();
  if ($numargs>=1) $conn = func_get_arg(0);
  
  ociLogOff($conn);
}

//----------------------------------------------------------------------------------
// InitRecordSet
// But : Ex�cute une requ�te SQL de type s�lection
// Param�tres :
//    $strSQL = requ�te s�lection � ex�cuter
//    $conn   = entier correspondant � la connexion ouverte
// Retourne un entier correspondant au r�sultat de la s�lection
//----------------------------------------------------------------------------------
function InitRecordSet($strSQL, $conn)
{
  $resSel = ociparse($conn, $strSQL);
  $res = ociexecute($resSel);
  if( $error = ocierror($resSel) ) 
  { 
    die("<font color=red>ERREUR!! Execution de la requ�te non reussie  :<br></font>".$strSQL); 
  }
  return $resSel;
}

//----------------------------------------------------------------------------------
// InitRecordSet
// But : Ex�cute une requ�te SQL de type s�lection
//       Retourne le r�sultat dans un tableau � 2 dimensions ne contenant que les lignes
//       comprises entre numFirst et numLast (bornes comprises)
//       Le premier commence � 0, le dernier � n-1. (n=nombre d'�l�ments)
// Param�tres :
//    strSQL = requ�te s�lection � ex�cuter
//    conn   = entier correspondant � la connexion ouverte
//    numFirst = numero du premier enregistrement
//    numLast  = num�ro du dernier enregistrement
//    dsRes    = tableau contenant les (numLast-numFirst+1) enregistrements
// Retourne le nombre total d'enregistrements
//----------------------------------------------------------------------------------
function InitRecordSetPagine($strSQL, $conn, $numFirst, $numLast, &$dsRes)
{
  $resSel = ociparse($conn, $strSQL);
  $res = ociexecute($resSel);
  $dsRes = array();
  $nbRes = 0;
  if( $error = ocierror($resSel) ) 
    { 
      die("<font color=red>ERREUR!! Execution de la requ�te non reussie  :<br></font>".$strSQL); 
    }
  else
    {
      // collecte les �l�ments pagin�s
      ocifetchstatement($resSel, $dsRes, $numFirst, $numLast-$numFirst+1, OCI_ASSOC+OCI_FETCHSTATEMENT_BY_ROW);
      // recherche le nombre total d'enregistrements
      $strSql = "select count(*) nb from (".$strSQL.") r";
      $rsNb = InitRecordSet($strSql, $conn);
      if( ociFetch($rsNb) ) $nbRes = ociResult($rsNb, "NB");
    }
  return $nbRes;
}

//----------------------------------------------------------------------------------
// GetValue
// But : Ex�cute une requ�te SQL de type s�lection
//       Retourne le r�sultat dans un tableau � 2 dimensions ne contenant que les lignes
//       comprises entre numFirst et numLast (bornes comprises)
//       Le premier commence � 0, le dernier � n-1. (n=nombre d'�l�ments)
// Param�tres :
//    strSQL = requ�te s�lection � ex�cuter
//    conn   = entier correspondant � la connexion ouverte
//    numFirst = numero du premier enregistrement
//    numLast  = num�ro du dernier enregistrement
// Retourne un tableau contenant le r�sultat. Si aucun r�sultat, retourne tableau vide.
//----------------------------------------------------------------------------------
function GetValueName($obj, $strIndex, $valDefault="")
{
  if( isset($obj[$strIndex]) ) return $obj[$strIndex];
  return $valDefault;
}

//----------------------------------------------------------------------------------
// ExecuteSQL
// But : Ex�cute une requ�te SQL de type action (insert, delete, update)
// Param�tres :
//    $strSQL = requ�te s�lection � ex�cuter
//    $conn   = entier correspondant � la connexion ouverte
// Retourne un entier indiquant le nombre d'�l�ments concern�s par l'action
//----------------------------------------------------------------------------------
function ExecuteSQL($strSQL, $conn)
{
  //echo $strSQL."\n<br>";
  $resAct = ociparse($conn, $strSQL);
  $res = ociexecute($resAct);
  if( $error = ocierror($resAct) ) 
  { 
    return -1;
    //die("<font color=red>ERREUR!! Execution de la requ�te non reussie  :<br></font>".$strSQL);
  }
  return $resAct;
}

//----------------------------------------------------------------------------------
// AnalyseSQL
// But : Remplace les caract�res sp�ciaux d'un champ texte d'une requete SQL
// Param�tres :
//    $strString = valeur d'un champ de type texte d'une requete SQL
// Retourne la chaine modifi�e
//----------------------------------------------------------------------------------
function AnalyseSQL($strString)
{
  $strTmp=stripslashes($strString);
  return ereg_replace("'", "''", $strTmp);  
}


//----------------------------------------------------------------------------------
// AnalyseJS
// But : Remplace les caract�res sp�ciaux d'une chaine caract�res javascript
// Param�tres :
//    $strJS = valeur d'une chaine caract�res javascript
// Retourne la chaine modifi�e
//----------------------------------------------------------------------------------
function AnalyseJS($strJS)
{
  $tmp = ereg_replace("\"", "\\"."\"", $strJS);
  return ereg_replace("'", "\'", $tmp);
}


//----------------------------------------------------------------------------------
// ora_to_date
// But : Formate une date au format SQL d'Oracle
// Param�tres :
//    $strDate = valeur de la date
// Retourne la chaine format�e
//----------------------------------------------------------------------------------
function ora_to_date($strDate)
{
  if( strlen($strDate) != 10 )
  {
    $strTmp = "NULL";
  }
  else
  {
    $strTmp = "to_date('$strDate', 'DD/MM/YYYY')";
  }
  return $strTmp;  
}


//----------------------------------------------------------------------------------
// ora_to_date_heure
// But : Formate une date et une heure au format SQL d'Oracle
// Param�tres :
//    $strDate  = valeur de la date
//    $strHeure = valeur de l'heure
// Retourne la chaine format�e
//----------------------------------------------------------------------------------
function ora_to_date_heure($strDate, $strHeure)
{
  if( strlen($strDate)!=10)
  {
    $strTmp = "NULL";
  }
  else
  {
    if ( strlen($strHeure)!=5 )$strHeure="00:00";
    $strTmp = "to_date('$strDate$strHeure', 'DD/MM/YYYYHH24:MI')";
  }
  return $strTmp;
}


//----------------------------------------------------------------------------------
// ora_analyseSQL
// But : Formate une chaine caract�res pour int�gration dans une requ�te Oracle SQL
// Param�tres :
//    $strDate  = valeur de la date
//    $strHeure = valeur de l'heure
// Retourne la chaine modifi�e
//----------------------------------------------------------------------------------
function ora_analyseSQL($strString)
{
  $strTmp=stripslashes($strString);
  return "'" . ereg_replace("'", "''", $strTmp) . "'";  
  
}
//----------------------------------------------------------------------------------
// ora_checkbox
// But : Formate une une variable de typecheckbox
// Param�tres :
//    $strString  = valeur 
// Retourne la valeur checkbox
//----------------------------------------------------------------------------------
function ora_checkbox($strString)
{
  if( $strString != "" && $strString != "0" )
     return 1;
  return 0;
}
//----------------------------------------------------------------------------------
// ora_analyseNum
// But : v�rifie le format d'un nombre
// Param�tres :
//    $strNum  = valeur 
// Retourne la valeur format�e
//----------------------------------------------------------------------------------
function ora_analyseNum($strNum)
{
  if( $strNum != "" )
    return ereg_replace(",", ".", $strNum);
  return "null";
}


//----------------------------------------------------------------------------------
// ora_getNextID
// But : D�termine la valeur de la prochaine cl� primaire d'une table
// Param�tres :
//    $conn  = identifiant de connexion 
//    $table = nom de la table
//    $champ = nom du champ cl� primaire, de type entier
// Retourne la valeur de la cl� primaire pour la prochaine insertion
//----------------------------------------------------------------------------------
function ora_getNextID($conn, $table, $champ)
{
  $strSql = "select $champ from $table order by $champ desc";
  $rs = InitRecordset($strSql, $conn);
  if( ocifetch($rs) )
  {
    $tmp = ociresult($rs, strtoupper($champ))+1;  
  }
  else
  {
    $tmp = 1;
  }
  return $tmp;
}


//----------------------------------------------------------------------------------
// ora_getNextSeq
// But : Donne le numero de la sequence
// Param�tres :
//    $conn  = identifiant de connexion 
//    $seq = nom de la sequence
// Retourne le numero de la sequence
//----------------------------------------------------------------------------------
function ora_getNextSeq($conn, $seq)
{
  $strSql = "select ".$seq.".nextval from dual";
  $rs = InitRecordset($strSql, $conn);
  if( ocifetch($rs) )
  {
    $tmp = ociresult($rs,1);  
  }
  else
  {
    die("<font color=red>erreur dans la sequence $seq </font>");
  }
  return $tmp;
}

//----------------------------------------------------------------------------------
// MajRang
// But : Met a jour le champ rang d'une table en fonction des parametres
// Param�tres :
//    $conn       = identifiant de connexion
//              $strTable   = nom de la table 
//    $strChamp   = nom du champ
//              $strNewRang = indice du nouveau rang
//              $bAdd       = true si ajout, false si suppression
//              $strWhere   = condition supplementaire pour la selection du rang
//----------------------------------------------------------------------------------
function MajRang($conn, $strTable, $strChamp, $strNewRang, $bAdd, $strWhere)
{
  $bExist = true;
  $strSigne = "+";
  $strComp = ">=";
  if( $bAdd == false ) { $strSigne = "-"; $strComp=">"; }
  if( $strWhere == "" ) $strWhere = "1=1";
  if( $bAdd == true ) 
    {
      $strSql = "select ".$strChamp." from ".$strTable." where ".$strWhere." and ".$strChamp."=".$strNewRang;
      $rsRg = initRecordSet($strSql, $conn);
      if( !ocifetch($rsRg) )
        $bExist = false;
    }
  if( $bExist == true )
    {
      $strSql = "update ".$strTable." set ".$strChamp."=".$strChamp.$strSigne."1 where ".
        $strWhere." and ".$strChamp.$strComp.$strNewRang;
      executeSql($strSql, $conn);
    }
}


//----------------------------------------------------------------------------------
// GetCompareSQL2ASCII7
// But : retourne une chaine contenant la fonctionnalite Oracle de comparaison
///      de chaine sans tenir compte des caracteres francais (accent, etc...)
// Param�tres :
//    $strChamp = nom du champ de la table
//    $strOp    = operateur de test SQL : like, =
//    $strVal   = chaine de comparaison qui doit etre traitee par ora_analyseSQL auparavant
//    $bCase    = vrai si respect de la case
//----------------------------------------------------------------------------------
function GetStrConvert2ASCII7($strChamp, $strOp, $strVal)
{
  $strTmp = strtolower($strVal);
  $strTmp = strtr($strTmp, "���������������", "eeeeaaauuuiiooc");
  return "lower(convert($strChamp, 'us7ascii','WE8ISO8859P1')) $strOp $strTmp";
}

//----------------------------------------------------------------------------------
// GetLogin
// fonction a supprimer avec annuaire v2
// But : retourne une chaine correspondant au login d'un utilistateur
// Param�tres :
//    $strNom    = nom d'une personne
//    $strPrenom = prenom d'une personne
//    $iLgNom    = longueur max du nom a prendre en compte
//    $iLgPrenom = longueur max du nom a prendre en compte
//----------------------------------------------------------------------------------
/*function GetLogin($strNom, $strPrenom, $iLgNom, $iLgPrenom)
{
  $strTmpNom = trim(strtolower($strNom));
  $strTmpPrenom = trim(strtolower($strPrenom));

  // enleve les accents
  $strTmpNom = strtr($strTmpNom, "���������������", "eeeeaaauuuiiooc");
  $strTmpPrenom = strtr($strTmpPrenom, "���������������", "eeeeaaauuuiiooc");

  // enleve les caract�res non compris entre a et z.
  $strTmpNom2 = "";
  for($i=0; $i<strlen($strTmpNom); $i++) 
    {
      $cCar = substr($strTmpNom, $i, 1);
      if( ord($cCar)>=ord("a") && ord($cCar)<=ord("z") )
        $strTmpNom2 .= $cCar;
    }

  $strTmpPrenom2 = "";
  for($i=0; $i<strlen($strTmpPrenom); $i++) 
    {
      $cCar = substr($strTmpPrenom, $i, 1);
      if( ord($cCar)>=ord("a") && ord($cCar)<=ord("z") )
        $strTmpPrenom2 .= $cCar;
    }

  $strLogin = substr($strTmpPrenom2, 0, $iLgPrenom).substr($strTmpNom2, 0, $iLgNom);

  return $strLogin;
}
  */

if( file_exists("../../libconf/verif_session.php") ) {
  include_once("../../libconf/verif_session.php");
}

?>