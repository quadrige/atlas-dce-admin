/**
 * Necessary to implement onSubmitSelect(sndr) function.
 */
function clearDataSelect(oSelect)
{
  clearTimeout(oSelect.alk_nextTimeout);
  oSelect.alk_lock = undefined;
  oSelect.alk_index = undefined;
  oSelect.alk_initIndex = undefined;
  oSelect.alk_keystrokes = undefined;
  oSelect.alk_lockBlur = undefined;
}
function onTimeoutSelect(strFormName, strSelectName)
{
  var sndr = eval("document."+strFormName+".elements[strSelectName];");
  if( !sndr ) return;
  var iOldIndex = sndr.alk_index;
  if( sndr.alk_keystrokes!="" && iOldIndex >=0 && !(sndr.options[iOldIndex].text_old==undefined) ) {
    sndr.options[iOldIndex].text = sndr.options[iOldIndex].text_old;
    sndr.options[iOldIndex].text_old == undefined;
  }
  sndr.alk_keystrokes = "";
}
function onKeyPressSelect(ev)
{
  var sndr = null;
  var iKey = 0;
  if( window.event ) {
    ev =  window.event;
    sndr = ev.srcElement; 
    iKey = ev.keyCode;
  } else {
    sndr = ev.target;
    iKey = ev.which;
  }
  if( sndr.focus == false ) { return; }
  if( iKey == 0 ) { return; }
  if( iKey == 13 ) { onEnterSelect(sndr); return; }
  if( sndr.alk_lock == undefined ) sndr.alk_lock = 0;
  if( sndr.alk_lock > 0 ) return;
  sndr.alk_lock = sndr.alk_lock + 1;
  var sChar = String.fromCharCode(iKey);
  var sysdate = new Date();
  if( sndr.alk_initIndex == undefined ) {
    sndr.alk_initIndex = sndr.selectedIndex;
    sndr.alk_index = (sndr.selectedIndex == -1 ? 0 : sndr.selectedIndex);
    sndr.options[sndr.alk_index].text_old = sndr.options[sndr.alk_index].text;
    sndr.alk_keystrokes = "";
    sndr.alk_nextTimeout = setTimeout("onTimeoutSelect('"+sndr.form.name+"','"+sndr.name+"')", 2000);
  } else {
    clearTimeout(sndr.alk_nextTimeout);
    sndr.alk_nextTimeout = setTimeout("onTimeoutSelect('"+sndr.form.name+"','"+sndr.name+"')", 2000);
  }
  var iOldIndex = sndr.alk_index;
  if( iKey == 8 && sndr.alk_keystrokes=="" ) { sndr.alk_lock = sndr.alk_lock - 1; return; }
  var strSearch = ( iKey != 8 
                    ? sndr.alk_keystrokes + sChar
                    : sndr.alk_keystrokes.substr(0, sndr.alk_keystrokes.length-1) );
  var iLgSearch = strSearch.length;
  var iLow = 0;
  var iHigh = sndr.length;
  if( sndr.options[iHigh-1].text == "" ) iHigh--;
  var strTxtP = "";
  var iPivot = parseInt((iHigh+iLow) / 2);
  while( iLow < iHigh-1 ) {
    strTxtP = ( iPivot != iOldIndex
                ? sndr.options[iPivot].text.substr(0, iLgSearch).toLowerCase()
                : sndr.options[iPivot].text_old.substr(0, iLgSearch).toLowerCase() );
    if( strTxtP > strSearch.toLowerCase() ) iHigh = iPivot; else iLow = iPivot;
    iPivot = parseInt((iHigh+iLow) / 2);
  }
  strTxtP = ( iLow != iOldIndex
              ? sndr.options[iLow].text.substr(0, iLgSearch).toLowerCase()
              : sndr.options[iLow].text_old.substr(0, iLgSearch).toLowerCase() );
  if( strTxtP == strSearch.toLowerCase() ) { /* ok */
    if( iKey == 8 ) sndr.alk_keystrokes = strSearch; else sndr.alk_keystrokes += sChar;
    sndr.options[iOldIndex].text = sndr.options[iOldIndex].text_old;
    sndr.options[iOldIndex].text_old == undefined;
    if( iLgSearch > 0 ) {
      var strTxt = new String(sndr.options[iLow].text);
      sndr.options[iLow].text_old = sndr.options[iLow].text;
      sndr.options[iLow].text = "["+strTxt.substr(0, iLgSearch)+"]"+strTxt.substr(iLgSearch);
      sndr.selectedIndex = iLow;
      sndr.alk_index = iLow;
    } else {
      sndr.selectedIndex = iOldIndex;
      sndr.alk_index = iOldIndex;
    }
  } else { /* not found */
    sndr.selectedIndex = iOldIndex;
    sndr.alk_index = iOldIndex;
  }
  /* sink the keypress, ie don't pass it on to Windows or anything else */
  ev.returnValue = false;
  sndr.alk_lock = sndr.alk_lock - 1;
}
function onBlurSelect(ev)
{
  var sndr = null;
  if( window.event )
    sndr = window.event.srcElement; 
  else
    sndr = ev.target;
  if( !sndr ) return;
  if( sndr.focus == false ) { return; }
  if( sndr && sndr.alk_lockBlur == undefined ) sndr.alk_lockBlur = 0;
  if( sndr && sndr.alk_lockBlur > 0 ) return;
  sndr.alk_lockBlur = sndr.alk_lockBlur + 1;
  var iIndex = sndr.selectedIndex;
  if( sndr.alk_initIndex == undefined ) sndr.alk_initIndex = sndr.selectedIndex;
  if( iIndex != -1 ) {
    if( !(sndr.options[iIndex].text_old==undefined) ) {
      sndr.options[iIndex].text = sndr.options[iIndex].text_old;
      sndr.options[iIndex].text_old = undefined;
    }
    if( sndr.alk_initIndex != iIndex ) { /* commit new selection */
      sndr.alk_initIndex = iIndex;
      clearDataSelect(sndr);
      onSubmitSelect(sndr);
    }
  }
  sndr.alk_lockBlur = sndr.alk_lockBlur - 1;
}
function onEnterSelect(sndr)
{
  if( sndr.focus == false ) { return; }
  if( sndr.alk_lockBlur == undefined ) sndr.alk_lockBlur = 0;
  if( sndr.alk_lockBlur > 0 ) return;
  sndr.alk_lockBlur = sndr.alk_lockBlur + 1;
  var iIndex = sndr.selectedIndex;
  if( sndr.alk_initIndex == undefined ) sndr.alk_initIndex = sndr.selectedIndex;
  if( iIndex != -1 ) {
    if( !(sndr.options[iIndex].text_old==undefined) ) {
      sndr.options[iIndex].text = sndr.options[iIndex].text_old;
      sndr.options[iIndex].text_old = undefined;
    }
    if( sndr.alk_initIndex != iIndex ) { /* commit new selection */
      sndr.alk_initIndex = iIndex;
      clearDataSelect(sndr);
      onSubmitSelect(sndr);
    }
  }
  sndr.alk_lockBlur = sndr.alk_lockBlur - 1;
}
