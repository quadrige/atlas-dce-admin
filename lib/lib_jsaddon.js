function openFilexplorer(strPathEncode, strRootPath)
{
  var ht = 500;
  var lg = 950;
  var t = (screen.height - ht) / 2;
  var l = (screen.width - lg) / 2;
  wmaj = window.open("../filexplorer/index.php?r="+strRootPath+"&p="+strPathEncode+"&h="+ht+"&w="+lg, "windFilexplorer",
                     "status=yes,scrollbars=no,resizable=no,height="+ht+",width="+lg+",top="+t+",left="+l);
  wmaj.focus();
}

function OpenContactAdmin()
{ 
  wmaj = OpenWindow("../espace/contact_admin_form.php?cont_id="+cont_id, "430", "570", "windContact");
  wmaj.focus();  
}
function OpenSCIG14()
{
  var largeur = screen.width;
	var hauteur = screen.height;
  wmaj = window.open(ALK_SIALKE_URL+"scripts/scig14/index.php",
                     "winSCIG14", 
                     "resizable=yes,toolbar=yes,menu=yes,status=yes,top=0,left=0,width="+largeur+",height="+hauteur+"");
  wmaj.focus();
}
function OpenPopupAgenda(agent_lie, appli_id)
{
  var hauteur_popup=200;
  var largeur_popup=520;
  var H = (screen.height - hauteur_popup) / 2;
  var L = (screen.width - largeur_popup) / 2;
  window.open(ALK_SIALKE_URL+"scripts/agenda_01/01_agenda_popup.php?"+
    "cont_appli_id="+appli_id+"&cont_id="+cont_id+"&agent_lie="+agent_lie,
    "winAgenda",
    "status=no,scrollbars=yes,resizable=yes,height="+hauteur_popup+
    ",width="+largeur_popup+",top="+H+",left="+L);
}
function OpenAspic()
{
  if( OpenAspic.arguments.length == 1 ) {
    // site aspic
    var strUrl = ALK_URL_ASPIC_V2;
    var largeur = screen.width;
  	var hauteur = screen.height;
	  wmaj = window.open(strUrl + OpenAspic.arguments[0],
                       "Identification", 
                       "resizable=yes,toolbar=no,status=yes,top=0,left=0,width="+largeur+",height="+hauteur+"");
	  wmaj.focus();
  } else {
    // aspic v1
    var strUrl = ALK_URL_ASPIC;
    var largeur = screen.width;
  	var hauteur = screen.height;
    window.open(strUrl, "_blank", 
      "menubar=yes,toolbar=yes,status=yes,scrollbars=yes,"+
      "resizable=yes,width="+largeur+",height="+hauteur+",top=0,left=0");
  }
}
function OpenPopupFiche(bType, idEnt, iNiveau)
{
  var hauteur_popup=400;
  var largeur_popup=480;
  var H = (screen.height - hauteur_popup) / 2;
  var L = (screen.width - largeur_popup) / 2;
  var strUrl = "";
  var strNomFiche = "";

  if( bType=="agent" || bType == 0 ) {
    strUrl = ALK_SIALKE_URL+"scripts/"+ALK_DIR_ANNU+
      "01_user_fiche.php?cont_appli_id="+cont_appli_id+
      "&cont_id="+cont_id+"&agent_id="+idEnt+"&iNiveau="+iNiveau;
    strNomFiche = "fiche_agent";
    hauteur_popup = 400;
    largeur_popup = 480;
  } else {
    strUrl = ALK_SIALKE_URL+"scripts/"+ALK_DIR_ANNU+
      "01_service_fiche.php?cont_appli_id="+cont_appli_id+
      "&cont_id="+cont_id+"&service_id="+idEnt+"&iNiveau="+iNiveau;
    strNomFiche = "fiche_service";
    hauteur_popup = 350;
    largeur_popup = 650;
  }
  
  window.open(strUrl, strNomFiche,
    "status=no,scrollbars=yes,resizable=yes,height="+
    hauteur_popup+",width="+largeur_popup+",top="+H+",left="+L);
}
function CloseSession()
{
  var res = window.confirm("Vous allez fermer votre session. Confirmer ?");
  if( res ) top.document.location.href = ALK_URL_IDENTIFICATION+"?fin=1";
}
function OpenCarto()
{
var larg = (document.body.clientWidth);
var haut = (document.body.clientHeight);
  var hauteur_popup = 600;
  var largeur_popup = 800;
  var H = (screen.height - hauteur_popup) / 2;
  var L = (screen.width - largeur_popup) / 2;
  var cont_id = "1";
  var cont_appli_id = "";
  var doc_type = "";
  var doc_id = "";
  var tabArg = OpenCarto.arguments;
  if( tabArg.length >= 2 ) {
    cont_id = tabArg[0];
    cont_appli_id = tabArg[1];
  }
  if( tabArg.length == 4 ) {  
    doc_type = tabArg[2];
    doc_id = tabArg[3];
  }
  var strUrl = ALK_SIALKE_URL+
    "scripts/sig_01/00_appli_load.php?cont_id="+cont_id+
    "&cont_appli_id="+cont_appli_id+"&doc_type="+doc_type+"&doc_id="+doc_id;
  window.open(strUrl, "AppliCarto", 
    "status=no,scrollbars=no,resizable=yes,height="+
    hauteur_popup+",width="+largeur_popup+",top="+H+",left="+L);
}
function OpenCartoExcel()
{
  var hauteur_popup = 600;
  var largeur_popup = 800;
  var H = (screen.height - hauteur_popup) / 2;
  var L = (screen.width - largeur_popup) / 2;
  var cont_id = "1";
  var cont_appli_id = "";
  var doc_type = "";
  var doc_id = "";
  var tabArg = OpenCartoExcel.arguments;
  if( tabArg.length >= 2 ) {
    cont_id = tabArg[0];
    cont_appli_id = tabArg[1];
  }
  if( tabArg.length == 3 ) {
    doc_id = tabArg[2];
  }
  var strUrl = ALK_SIALKE_URL+
    "scripts/cex_01/00_appli_load.php?cont_id="+cont_id+
    "&cont_appli_id="+cont_appli_id+"&doc_id="+doc_id;
  window.open(strUrl, "AppliCartoExcel", 
    "status=no,scrollbars=no,resizable=no,height="+
    hauteur_popup+",width="+largeur_popup+",top="+H+",left="+L);
}
function OpenPrintPreview()
{
  var strUrl = location.href;
  if( strUrl.substr(-4)!=".php" )
    strUrl = strUrl +"&print=1";
  else
    strUrl = strUrl +"?print=1";
  var strName = "PrintPreview";
  var hauteur_popup = 400;
  var largeur_popup = 630;
  var H = (screen.height - hauteur_popup) / 2;
  var L = (screen.width - largeur_popup) / 2;
  popupWindow = window.open(strUrl, strName, 
    "menubar=yes,status=yes,scrollbars=yes,resizable=yes,height="+
    hauteur_popup+",width="+largeur_popup+",top="+H+",left="+L);
}
function AddFavorite()
{
  var bookmarkUrl = ALK_ROOT_URL+"/"+strTitreAliasPage+".htm";
  var bookmarkTitle = strTitrePage;
  if( bookmarkUrl != "" )
    if( document.all )
      window.external.AddFavorite(bookmarkUrl, bookmarkTitle);
    else
      OpenWindow(ALK_SIALKE_URL+"scripts/"+ALK_DIR_ESPACE+
        "favori_info.php?bu="+bookmarkUrl+"&bt="+bookmarkTitle,
        150, 750, "windFavoris");
  else
    alert("Vous n'avez pas le droit d'acc�der � cet espace.");
}

function OpenAppliDist()
{
  var hauteur_popup = 700;
  var largeur_popup = 1024;
  var H = (screen.height - hauteur_popup) / 2;
  var L = (screen.width - largeur_popup) / 2;
  var cont_id = "1";
  var cont_appli_id = "";
  var doc_type = "";
  var doc_id = "";
  var tabArg = OpenAppliDist.arguments;
  if( tabArg.length >= 2 ) {
    cont_id = tabArg[0];
    cont_appli_id = tabArg[1];
  }
  if( tabArg.length == 3 ) {
    doc_id = tabArg[2];
  }
  if (ALK_URL_SERVEUR_DIST){  	
    var strUrl = ALK_SIALKE_URL+
      "scripts/espace/call_connexion_dist.php?cont_id="+cont_id+
      "&cont_appli_id="+cont_appli_id+"&appli_id="+cont_appli_id+"&iMode=2";
    window.open(strUrl, "AppliDist", 
      "status=yes,scrollbars=yes,resizable=yes,directories=yes,toolbar=yes,menubar=yes,height="+
      hauteur_popup+",width="+largeur_popup+",top="+H+",left="+L);
      
  }
}

/**
 * h = hostname
 * a = fdoc_appli_id
 * b = agent_id
 * c = profil_id
 * d = rub_id
 * e = doc_id
 * f = strExtent : (tab[xmin]=xmin&tab[ymin]=ymin&tab[xmax]=xmax&tab[ymax]=ymax)
 */
function OpenSiteCarto(h, a, b, c, d, e, f){
  var hauteur_popup = 700;
  var largeur_popup = 1020;
  var H = (screen.height - hauteur_popup) / 2;
  var L = (screen.width - largeur_popup) / 2;
  
  var strUrl = "http://"+h+"/scripts/sig_site/00_gotocarte.php?a="+a;
  if (b!="")
    strUrl += "&b="+b;
  if (c!="")
    strUrl += "&c="+c;
  if (d!="")
    strUrl += "&d="+d;
  if (e!="")
    strUrl += "&e="+e;
  if (f!="")
    strUrl += "&"+f;
  window.open(strUrl, "sig_site", 
    "status=yes,scrollbars=yes,resizable=yes,height="+
    hauteur_popup+",width="+largeur_popup+",top="+H+",left="+L);
} 
function ShowHideFormAide(iNumImgAide)
{
  // rend visible ou non l'aide associ� � un controle de saisie

}

function OpenWindEspaceInvite(cont_id)
{
  wmaj = OpenWindow("../espace/contact_animateur_form.php?cont_id="+cont_id, "470", "570", "windContact");
  wmaj.focus();  
}

