
/** tableau m�morisant les instances XmlHttpRequest */
var tabXhr = new Array();

var reconnectExp = /\/\*ALK_RECONNECT\*\//;

AlkAjax.prototype.constructor = AlkAjax;
function AlkAjax(_name, onreadystate, methodSend, url, data)
{
  var tabMethod = new Array("GET", "POST");
  if( methodSend == 0 || methodSend == 1 ) 
    methodSend = tabMethod[methodSend];
  else if( methodSend.toUpperCase() == "GET" || methodSend.toUpperCase() == "POST" ) {
    methodSend = methodSend.toUpperCase();  
  } else {
    methodSend = "GET";
  }

  this._name        = _name;
  this.onreadystate = onreadystate;
  this.methodSend   = methodSend;
  this.url          = url;
  this.data         = data;
  this.async_mode   = true;
  this.returnValue  = null;
  this.postData  = null; 

  var iArg = 5;
  var errorMethod = "";
  var startMethod = "";
  var stopMethod = "";
  var returnValue = null; 

  if( arguments.length>iArg) this.async_mode = arguments[iArg++];
  if (arguments.length>iArg) errorMethod     = arguments[iArg++];
  if (arguments.length>iArg) startMethod     = arguments[iArg++];
  if (arguments.length>iArg) stopMethod      = arguments[iArg++];
  
  var oXhr = top.GetXMLHttpRequest();
  if(this.methodSend == "POST"){
      this.convertFormToParam();
      oXhr.open(this.methodSend, this.url, (this.async_mode ? true : false));
      oXhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      oXhr.setRequestHeader("Content-length", this.postData.length);
      oXhr.setRequestHeader("Connection", "keep-alive");
  }else{ 
      oXhr.open(this.methodSend, this.url, (this.async_mode ? true : false));
  }
  if( this.async_mode == true ) { 
    oXhr.onreadystatechange = function() {
      if( oXhr.readyState==1 && startMethod!="" ) {
        eval(startMethod+"();");
      } 
      if( oXhr.readyState == 4 ){ /*ready*/
        if ( oXhr.status == 200 ){ /* ok*/ 
          if ( reconnectExp.test(oXhr.responseText) ) {
            eval(oXhr.responseText);
            return;
          } else {
            if( onreadystate != "" ) {
              eval(onreadystate+"(oXhr.responseText);");
            }
          }
          if( stopMethod!="" ) {
            eval(stopMethod+"();");
          }
        } else {
          if( errorMethod!="" ) {
            eval(errorMethod+"(\"Error :\\nCall= "+this.url+"\\nResponse= "+oXhr.responseText+"\");");
          }
        }
      }
    }
  }
  
  
  if(this.methodSend == "POST"){
    oXhr.send(this.postData);
  }else{
    oXhr.send(this.data);
  }
  
  if( this.async_mode == false) {
    returnValue = oXhr.responseText;
  }
  
  this.returnValue = returnValue;  
}

/**
 * Retourne la valeur retourn�e par fonction onreadystate appel�e en backcall par la requ�te ajax
 * @return any
 */
AlkAjax.prototype.getReturnValue = function ()
{
  return this.returnValue;
}


/** 
 * @brief Retourne un r�f�rence sur l'objet XMLHttpRequest
 */
function GetXMLHttpRequest()
{  
  var xhr = null; 
     
  if( window.XMLHttpRequest ) {
    // Firefox 
    xhr = new XMLHttpRequest(); 
  } else if( window.ActiveXObject ) {
    // Internet Explorer 
    xhr = new ActiveXObject("Microsoft.XMLHTTP"); 
  } else { 
    // XMLHTTPRequest non support� par le navigateur 
    alert("Votre navigateur n'accepte pas de cr�er des requ�tes de type AJAX."); 
    return null; 
  } 
  return xhr;
}


/**
 * @brief Transforme les �l�ments d'un formulaire en param�tres de type '&name=value' et les concat�ne
 * 
 * Le r�sultat de la concat�nation est stock� dans la propri�t� Data
 */
AlkAjax.prototype.convertFormToParam = function ()
{       
        eval ("var oForm = document."+this.data); 
        var getstr = "";
        for (i=0; i<oForm.elements.length; i++) {
        if (oForm.elements[i].tagName == "INPUT") {
              if (oForm.elements[i].type == "text") {
                 getstr += oForm.elements[i].name + "=" + escape(oForm.elements[i].value) + "&";
              }
              if (oForm.elements[i].type == "hidden") {
                 getstr += oForm.elements[i].name + "=" + escape(oForm.elements[i].value) + "&";
              }
              if (oForm.elements[i].type == "checkbox") {
                 if (oForm.elements[i].checked) {
                    getstr += oForm.elements[i].name + "=" + escape(oForm.elements[i].value) + "&";
                 } else {
                    getstr += oForm.elements[i].name + "=&";
                 }
              }
              if (oForm.elements[i].type == "radio") {
                 if (oForm.elements[i].checked) {
                    getstr += oForm.elements[i].name + "=" + escape(oForm.elements[i].value) + "&";
                 }
              }
           }
           if (oForm.elements[i].type == "textarea") {
                 getstr += oForm.elements[i].name + "=" + escape(oForm.elements[i].value) + "&";
              }   
           if (oForm.elements[i].tagName == "SELECT") {
              var sel = oForm.elements[i];
              if ( sel.selectedIndex>=0 )
                getstr += sel.name + "=" + escape(sel.options[sel.selectedIndex].value) + "&";
           }
           
        }
  
  this.postData = getstr;
}





/**
 * @brief Fonction � appeler lorsque le r�sultat d'un appel Ajax n'est pas trait� 
 */
function NothingToDo(xhrResponseText){}
