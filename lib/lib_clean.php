<?
///<comment>
///<summary>
/// Supprimer le fichier si le chemin pass� en r�pertoire d�signe un fichier
/// Supprime le r�pertoire pass� en param�tre ainsi que les fichiers qu'il contient sinon
///</summary>
///<params name="pathDir">Chemin absolu du r�pertoire ou du fichier</params>
///</comment>
function rmDirFile($pathDir)
{
	if (is_file($pathDir))
	{
		unlink ($pathDir);
	}
	else
	{
		$dir = opendir($pathDir);
		
		while ($file = readdir($dir))
		{
			if ($file != "." && $file != "..")
			{
				if (is_dir($pathDir."/".$file))
				{
					rmDirFile($pathDir."/".$file);
				}
				else
				{
					unlink ($pathDir."/".$file);
				}
			}
		}
		
		closedir($dir);
		rmdir($pathDir);
	}
}
?>
