function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
function SetPositionCalc(strCalc, bShow) {
	if( (obj=MM_findObj(strCalc)) !=null ) { 
		if( obj.style ) obj = obj.style; 
		if( bShow ) {
		  obj.top = "70px";
			obj.left= "0px";
			obj.width= "600px";
		} else {
		  obj.top = "-10000px";
			obj.left= "0px";
			obj.width= "600px";
		}
	}
}
function SelectSsForm(strCalc) {
//	MM_showHideLayers(strCalcSelect, '', 'hide');
//  SetPositionCalc(strCalcSelect, false);
//	strCalcSelect = strCalc;
//	SetPositionCalc(strCalcSelect, true);
//	MM_showHideLayers(strCalcSelect, '', 'show');
//	document.location.href = "#haut";
}
function OpenWindow(strPage, hauteur, largeur, strName) {
	if( !strName ) strName = "_blank";
  var H = (screen.height - hauteur) / 2;
  var L = (screen.width - largeur) / 2;
  window.open(strPage, strName, 
		"status=yes,scrollbars=yes,resizable=yes,height="+hauteur+",width="+largeur+",top="+H+",left="+L);
}
function AddCtrl(formName, layerName, ctrlName, type, bReq, iDroit) {
	tabCtrl[nbCtrl] = new Array(layerName, ctrlName, type, bReq);
	nbCtrl++;
	var f = eval("document."+formName);
	var nCtrl = tabCtrl[nbCtrl-1][1];
	var nType = tabCtrl[nbCtrl-1][2];
	var nReq  = tabCtrl[nbCtrl-1][3];
	var oCtrl = eval("f.elements['"+nCtrl+"'];");
	oCtrl.numCtrl = nbCtrl-1;
	//oCtrl.value = nCtrl;
	if( nType != "check" && nType != "memo" && nType != "radio" ) { sizeCtrl = oCtrl.size; }

	if( nType!="check" && nType!="memo" && nType!="radio" ) 
		{ oCtrl.width = (sizeCtrl>10 ? sizeCtrl*6 : (sizeCtrl>5 ? sizeCtrl*8 : sizeCtrl*9)); }
	return nbCtrl-1;
}
function SetCalcul(oCtrlThis)
{
	var f = document.formBD;
	var nbTot = oCtrlThis.ctrlTot.length;
	for(var j=0; j<nbTot; j++) {
		var oCtrlTot = eval("f.elements['"+oCtrlThis.ctrlTot[j]+"'];");
		var nbElt = oCtrlTot.tabCalc.length;
		var tot = 0;
		for(var i=0; i<nbElt; i++) {
			var oCtrlNb = eval("f.elements['"+oCtrlTot.tabCalc[i]+"'];");
			var strNb = oCtrlNb.value;
			if( strNb.toString() != "" ) {
				var nb = parseInt(strNb.toString(), 10);
				if( nb.toString()==strNb ) tot = tot + nb;
			}
		}
		oCtrlTot.value = tot;
		if( oCtrlTot.tabLink ) SetValueLink(oCtrlTot);
		if( oCtrlTot.ctrlTot ) SetCalcul(oCtrlTot);
	}
}
function AddCalcul(ctrlNameTot) {
	var f = document.formBD;
	var oCtrlTot = eval("f.elements['"+ctrlNameTot+"'];");
	oCtrlTot.value = "0";
	var tabParams = AddCalcul.arguments;
	var nbParams = tabParams.length;
	oCtrlTot.tabCalc = new Array(nbParams-1);
	for(var i=1; i<nbParams; i++) {
		var oCtrlElt = eval("f.elements['"+tabParams[i]+"'];");
		if( !oCtrlElt.ctrlTot ) oCtrlElt.ctrlTot = new Array(ctrlNameTot)
		else oCtrlElt.ctrlTot = oCtrlElt.ctrlTot.concat(new Array(ctrlNameTot));
		//if( oCtrlElt.style ) oCtrlElt = oCtrlElt.style;
		//	oCtrlElt.backgroundColor = "#fff0ff";
		oCtrlTot.tabCalc[i-1] = tabParams[i];
	}
}
function SetValueLink(oCtrlThis) {
	if( oCtrlThis ) {
		var f = document.formBD;
		var nbLink = oCtrlThis.tabLink.length;
		for(i=0; i<nbLink; i++) {
			var oCtrlElt = eval("f.elements['"+oCtrlThis.tabLink[i]+"'];");
			oCtrlElt.value = oCtrlThis.value;
			if( oCtrlElt.ctrlTot ) SetCalcul(oCtrlElt);
		}
	}
}
function AddLink(ctrlNameRef) {
	var f = document.formBD;
	var oCtrlRef = eval("f.elements['"+ctrlNameRef+"'];");
	var tabParams = AddLink.arguments;
	var nbParams = tabParams.length;
	if( !oCtrlRef ) alert(ctrlNameRef);
	oCtrlRef.tabLink = new Array(nbParams-1);
	for(var i=1; i<nbParams; i++) {
		var oCtrlElt = eval("f.elements['"+tabParams[i]+"'];");
		oCtrlElt.readOnly = true;
		if( oCtrlElt.style ) oCtrlElt = oCtrlElt.style;
			oCtrlElt.backgroundColor = "#f0ffff";
		oCtrlRef.tabLink[i-1] = tabParams[i];
	}
}
function InitForm() {
	window.focus();
	var f = document.formBD;
	f.reset();
	MM_showHideLayers('Section000', '', 'hide');
	SetPositionCalc(strCalcSelect, true);
	MM_showHideLayers(strCalcSelect, '', 'show');
	document.location.href = "#haut";
}
function Modifier() {
	var f = document.formBD;
	iMode = "2";
	if( f.Text_002_01.value=="" ) { alertMsg(f.Text_002_01, "Section03", "L'identifiant est obligatoire"); return; }
	if( f.Text_002_02.value=="" ) { alertMsg(f.Text_002_02, "Section03", "Le mot de passe est obligatoire"); return; }
  var H = (screen.height - 200) / 2;
  var L = (screen.width - 400) / 2;
//	windRes = window.open(strUrlBase + "bd_read.php?form_id="+form_id+"&iMode="+iMode+
//		"&f=formBD&Text_002_01="+f.Text_002_01.value+"&Text_002_02="+f.Text_002_02.value, 
//		"windowRes", 
//   "status=yes,scrollbars=yes,resizable=yes,height=200,width=400,top="+H+",left="+L);
	windRes = window.open("bd_read.htm", "windowRes", 
	 	"status=yes,scrollbars=yes,resizable=yes,height=200,width=400,top="+H+",left="+L);
}
function ReadMaj(wPopup) {
	var f = document.formBD;
	f.reset();
	var fr = wPopup.document.formRead;
	if( fr ) {
		for(i=0; i<nbCtrl; i++) {
			var nCtrl = tabCtrl[i][1];
			var tCtrl = tabCtrl[i][2];
			var oCtrl = eval("f.elements['"+nCtrl+"'];");
			var oCtrlR = eval("fr.elements['"+nCtrl+"'];");
			if( oCtrlR ) {
				oCtrlR.visited = true;
				if( tCtrl=='text' || tCtrl=='int' || tCtrl=='date10' || tCtrl=='memo' ) {
					oCtrl.value = oCtrlR.value;
					if( nCtrl=="Text_002_01" || nCtrl=="Text_002_02" ) {
						oCtrl.readOnly = true; 
						if( oCtrl.style ) oCtrl=oCtrl.style; 
						oCtrl.backgroundColor = '#f0ffff';
					}
				} else if( tCtrl=='check' ) {
					oCtrl.checked = (oCtrlR.value == "" ? false : true);
				} else if( tCtrl=='radio' )
					for(j=0; j<oCtrl.length; j++) {
						if( oCtrl[j].value == oCtrlR.value )
							oCtrl[j].checked = true;
					}
			}
		}
		// met � jour les champs cach�s
		var nbCtrlR = fr.elements.length;
		for(i=0; i<nbCtrlR; i++) {
			var oCtrlR = fr.elements[i];
			if( !(oCtrlR.visited) && oCtrlR.name!="nbrCtrl" ) {
				var nCtrl = oCtrlR.name;
				var oCtrl = eval("f.elements['"+nCtrl+"'];");
				oCtrl.value = oCtrlR.value;
			}	
		}
	 	nbClickValid = 0;
	}
}
function NouveauDossier()
{
	var f = document.formBD;
	iMode = "1";
	if( f.Text_002_01.value=="" ) { alertMsg(f.Text_002_01, "Section03", "L'identifiant est obligatoire"); return; }
  var H = (screen.height - 200) / 2;
  var L = (screen.width - 400) / 2;
//	windRes = window.open(strUrlBase + "bd_read2.php?form_id="+form_id+"&iMode="+iMode+
//		"&f=formBD&Text_002_01="+f.Text_002_01.value, 
//		"windowRes", 
//    "status=yes,scrollbars=yes,resizable=yes,height=200,width=400,top="+H+",left="+L);
	windRes = window.open("bd_read2.htm", "windowRes", 
  	"status=yes,scrollbars=yes,resizable=yes,height=200,width=400,top="+H+",left="+L);
}

function Annuler() {
	var f = document.formBD;
	var res = window.confirm("Veuillez confirmer la r�initialisation de tout ce dossier ?");
	if( res ) {	
		nbClickValid = 0;	
		f.reset();
		oCtrl = f.Text_002_01;
		oCtrl.readOnly = false;
		if( oCtrl.style ) oCtrl = oCtrl.style; 
		oCtrl.backgroundColor = '#ffffff';
		oCtrl = f.Text_002_02;
		oCtrl.readOnly = false;
		if( oCtrl.style ) oCtrl = oCtrl.style; 
		oCtrl.backgroundColor = '#ffffff';
	}
}
