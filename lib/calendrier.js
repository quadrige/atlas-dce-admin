var dayNames = new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
var months = new Array("Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin",
"Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre");

var iDayStyle = 1;

var xp=0;
var yp=0;  

var CURSOR_HAND = "hand";
// Taille de la fenetre
if (navigator.userAgent.indexOf("IE") >= 0){   
  maxX = document.body.scrollLeft + document.body.clientWidth;
  maxY = document.body.scrollTop  + document.body.clientHeight ;
  CURSOR_HAND = "hand";
}
else{  
  // AVOIR pour le scroll
  CURSOR_HAND = "pointer";
  maxX = window.innerWidth;   
  maxY = window.innerHeight;  
}  
if (navigator.userAgent.indexOf("IE") >= 0){   
  var calxpos = "event.offsetX";
  var calypos = "event.offsetY";
}else{
  var calxpos = "e.pageX";
  var calypos = "e.pageY";
}

var nbYears = 10;

var dDate = new Date();
MIN_YEAR = 1900;
MAX_YEAR = dDate.getFullYear()+2;
var max_year = MAX_YEAR;

var select_day = "";
var select_month = "";
var select_year = "";

var cur_year = dDate.getFullYear();
var cur_month = dDate.getMonth()+1;
var cur_day = dDate.getDate();

Calendar.prototype.constructor = Calendar;
function Calendar(inputName, date_min, date_max){
  this.inputName = inputName;
  this.oDiv = _getElementById(this.inputName+"_calendarDiv");
  this.oInput = _getElementById("input_"+this.inputName);
  this.bOpen = false;
  
  this.oDateMin = null;
  this.oDateMax = null;
  if (date_min!=""){
    this.oDateMin = this.GetDateObj(date_min);
  }
  if (date_max!=""){
    this.oDateMax = this.GetDateObj(date_max);
  }

  this.closeOnChoose = true;
  if (arguments.length>3){
    this.closeOnChoose = arguments[3];
  }
  
}

Calendar.prototype.GetDateObj = function(date10){  
  return new Date(this.GetYearFromDate10(date10), this.GetMonthFromDate10(date10, -1), this.GetDayFromDate10(date10));  
}

Calendar.prototype.GetDayFromDate10 = function(date10){  
  var day = date10.substr(0, 2);
  if (day.charAt(0)=="0") day = day.charAt(1);  
  day = parseInt(day);
  return day;  
}

Calendar.prototype.GetMonthFromDate10 = function(date10, iDelta){
  if (typeof iDelta == "undefined") iDelta = 0;  
  var month = date10.substr(3, 2); 
  if (month.charAt(0)=="0") month = month.charAt(1);  
  month = parseInt(month);
  return month+iDelta;  
}

Calendar.prototype.GetYearFromDate10 = function(date10){  
  return parseInt(date10.substr(6, 4));
}

Calendar.prototype.fToggleColor = function (myElement, bOver) {
  if (!myElement) return;
  if (myElement.className.indexOf('invalid')!=-1) return;
  if (myElement.id.indexOf("calCell")!=-1 && myElement.className.indexOf('select')==-1) {
    myElement.className = (bOver ? myElement.className+' over' : myElement.className.replace(/ over/g, ""));
  }
}

Calendar.prototype.fSetSelectedDay = function (myElement){
  if (!myElement) return;
  
  if (myElement.id.indexOf(this.inputName+"_calCell")!=-1) {
    if (myElement.className.indexOf('invalid')!=-1) return;
    if (this.dateSelected>0){
      var element = _getElementById(this.inputName+"_calCell_"+this.dateSelected);
      if (element) element.className = element.className.replace(/ select/g, "");
      if (element) element.className = element.className.replace(/ over/g, "");
    }         
    
    myElement.className = myElement.className+' select';
    var index = myElement.id.lastIndexOf("_");
    var date = myElement.id.substr(index+1);
    this.dateSelected = date;

    if (!isNaN(parseInt(date))) {    
      var day = parseInt(date);
      var before_day = (day<10 ? "0" : "");
      var before_month = (cur_month<10 ? "0" : "");      
      if (this.oInput!=null) {
        this.oInput.value = before_day+day+"/"+before_month+cur_month+"/"+cur_year;
        if (this.oInput.onchange){
          if (arguments.length==1 || arguments[1]){
            this.oInput.onchange();
          }
        }
      }
      
      if (this.closeOnChoose) this.HideCalendar();
    }
    
  }
}
Calendar.prototype.fGetDaysInMonth = function (iMonth, iYear) {
  var dPrevDate = new Date(iYear, iMonth, 0);
  return dPrevDate.getDate();
}

Calendar.prototype.fGetFirstDayInMonth = function (iMonth, iYear) {
  var dCalDate = new Date(iYear, iMonth-1, 1);
  return dCalDate.getDay();
}

Calendar.prototype.getDayNames = function (iStyle){
  var strHtml = "";
  strHtml += "<tr>";
  for (var i in dayNames){
    var day = dayNames[i];
    switch (iStyle){
      case 0 : day = day.substr(0, 2); break;
      case 1 : day = day.substr(0, 3); break;
    }
    strHtml += 
      "<td align='center' width='14%' class='daytitle'>" + day + "</td>";
  }
  strHtml += "</tr>";
  
  return strHtml;
}

Calendar.prototype.getCalendar = function (iYear, iMonth){  
  var strHtml = "<table border='0' cellspacing='1' cellpadding='2' width='100%' class='popcalext'>";
  strHtml += this.getDayNames(iDayStyle);
  var iDayOfFirst = this.fGetFirstDayInMonth(iMonth, iYear);
  var iDaysInMonth = this.fGetDaysInMonth(iMonth, iYear);
  strHtml += "<tr>";
  for (var i=0; i<iDayOfFirst; i++){
		strHtml += "	<td class='empty'>";
		strHtml += "	</td>";
  }
  

  var dayOfWeek = iDayOfFirst;
  for (var date=1; date<=iDaysInMonth; date++){
  	strClass = "day";
    switch (dayOfWeek) {
    	case 0 : strClass = "weekend"; break;
    	case 6 : strClass = "weekend"; break;
    	case 7 : strHtml += "</tr>"; dayOfWeek = 0; strClass = "weekend"; break;    	
    }
    
    if (dayOfWeek==0){
      strHtml += "<tr>";
    }
    
    if (!isNaN(date)) {
   	   
   	 if (date == dDate.getDate() && iMonth==dDate.getMonth()+1 && iYear==dDate.getFullYear()){
   	   strClass += " today";
   	   this.dateSelected = date;
   	 }
   	
   	 var bInvalid = false;
	   var aDate = new Date(iYear, iMonth-1, date);
   	 if (this.oDateMin){
   	   if (aDate<this.oDateMin){
   	     strClass += " invalid";
   	     bInvalid = true;
   	   }
   	 }
   	 if (this.oDateMax){
   	   if (aDate>this.oDateMax && !bInvalid){
   	     strClass += " invalid";
   	     bInvalid = true;
   	   }
   	 }
   	 
   	 if (!bInvalid){
     	 if (date == select_day && iMonth==select_month && iYear==select_year){
     	   strClass += " select";
     	   this.dateSelected = date;
     	 }
   	 }
   	   
     strHtml += "	<td id='"+this.inputName+"_calCell_"+date+"' bgcolor='#333333' class='"+strClass+"' style='cursor:"+CURSOR_HAND+"' ";
     strHtml += " onMouseOver='oCalendar_"+this.inputName+".fToggleColor(this, true)'" +
    	          " onMouseOut='oCalendar_"+this.inputName+".fToggleColor(this, false)'" +
    	          " onclick='oCalendar_"+this.inputName+".fSetSelectedDay(this)'>";
     strHtml += "	"+ date;
     strHtml += "	</td>";	
	
      dayOfWeek++;
    } 
  }
  for (var i=dayOfWeek; i<7; i++){
		strHtml += "	<td class='empty'>";
		strHtml += "	</td>";
  }
	
  strHtml += "</tr>";
  strHtml += "</table>";  
  strHtml += "<table border='0' cellspacing='0' cellpadding='3' width='100%'>";
	strHtml += "	<tr>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"1\");' class='lienmois'>J</a></td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"2\");' class='lienmois'>F</td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"3\");' class='lienmois'>M</a></td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"4\");' class='lienmois'>A</a></td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"5\");' class='lienmois'>M</a></td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"6\");' class='lienmois'>J</a></td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"7\");' class='lienmois'>J</a></td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"8\");' class='lienmois'>A</a></td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"9\");' class='lienmois'>S</a></td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"10\");' class='lienmois'>O</a></td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"11\");' class='lienmois'>N</a></td>";
	strHtml += "	<td width='8%'><a href='javascript:oCalendar_"+this.inputName+".changeMonth(\"12\");' class='lienmois'>D</a></td>	";
	strHtml += "</tr>";
	strHtml += "	<tr>";
	strHtml += "	<td colspan='6' class='tdlienmoisL'><a href='javascript:oCalendar_"+this.inputName+".changeYear("+(cur_year-1)+");'  class='lienmois'>"+(cur_year-1)+"</a></td>";
	strHtml += "	<td colspan='6' class='tdlienmoisR'><a href='javascript:oCalendar_"+this.inputName+".changeYear("+(cur_year+1)+");'  class='lienmois'>"+(cur_year+1)+"</a></td>";
	strHtml += "	</tr>";
	strHtml += "</table>";
	
  return strHtml;
}

Calendar.prototype.decreaseMonth = function (){
  var old_month = cur_month;
  cur_month = (cur_month-1)%12;
  if (old_month==1) {
  	 cur_month = 12;
   	 cur_year = cur_year - 1;
  }
  _getElementById(this.inputName+"_months").innerHTML = months[cur_month-1]+" "+cur_year;
  this.changeCalendar();
}

Calendar.prototype.increaseMonth = function (){
  var old_month = cur_month;
  cur_month = (cur_month+1)%12;
  if (old_month==11) {
  	 cur_month = 12;
  }
  if (old_month==12) {
   	 cur_year = cur_year + 1;
  }
  _getElementById(this.inputName+"_months").innerHTML = months[cur_month-1]+" "+cur_year;
  this.changeCalendar();
}

Calendar.prototype.changeDay = function (iDay){
  var id = this.inputName+"_calCell_"+iDay;
  this.fSetSelectedDay( _getElementById(id), false );
}

Calendar.prototype.changeMonth = function (iMonth){
  cur_month = iMonth;
  _getElementById(this.inputName+"_months").innerHTML = months[cur_month-1]+" "+cur_year;
  this.changeCalendar();
}

Calendar.prototype.changeYear = function (iYear){
  cur_year = iYear;
  cur_month = 1;
  _getElementById(this.inputName+"_months").innerHTML = months[cur_month-1]+" "+cur_year;
  this.changeCalendar();
}

Calendar.prototype.changeCalendar = function (){
  this.dateSelected = 0;
  _getElementById(this.inputName+"_calendar").innerHTML = this.getCalendar(cur_year, cur_month);
}

Calendar.prototype.getCloseButton = function (){
  var strHtml = "<a href='javascript:oCalendar_"+this.inputName+".HideCalendar()'>";
  strHtml += "<img src='../../../media/admin/images/bt_close.gif' width=15px height=15pxm border=0>";
  strHtml += "</a>";
  return strHtml;
}

Calendar.prototype.OpenCloseCalendar = function (){
  if (this.bOpen) this.HideCalendar();
  else this.OpenCalendar();
}

Calendar.prototype.OpenCalendar = function (){  
  if (this.oInput) {
    this.oInput.defaultValue = this.oInput.value;
  }
  
  var dCurDate = new Date();
  
  if (this.oInput && this.oInput.defaultValue.match(new RegExp("[0-3][0-9]\/[0-1][0-9]\/[0-9]{4}"))){
    var fDate = this.oInput.value;
    dCurDate = this.GetDateObj(fDate); 
  }
  
  select_day = dCurDate.getDate();
  select_month = dCurDate.getMonth()+1;
  select_year = dCurDate.getFullYear();
  cur_day = select_day;
  cur_month = select_month;
  cur_year = select_year;
  
  var strCal = this.getCalendar(cur_year, cur_month, cur_day);
  
  var strHtml = "";
  strHtml += "<table width='220' cellspacing='0' cellpadding='4' class='popcal'>";  
  strHtml += "<tr>";
	strHtml += "<td valign='top' align='left' width='98%'>";
	strHtml += "<table border='0' cellspacing='0' cellpadding='3' width='100%' class='poptitle'>";
	strHtml += "	<tr>";
	strHtml += "		<td align='left'><a href='javascript:oCalendar_"+this.inputName+".decreaseMonth();'><img src='../../media/images/cal_prev.gif' width='16' height='16' alt='previous month' border='0' /></a></td>";
	strHtml += "	<td width='99%' align='center'><div id='"+this.inputName+"_months'>"+months[cur_month-1]+" "+cur_year+"</div></th>";
	strHtml += "		<td align='right'><a href='javascript:oCalendar_"+this.inputName+".increaseMonth();'><img src='../../media/images/cal_next.gif' width='16' height='16' alt='next month' border='0' /></a></td>";
	strHtml += "	</tr>";
	strHtml += "</table>";
  strHtml += "<div id='"+this.inputName+"_calendar'>";
  strHtml += strCal;
  strHtml += "</div>";
  strHtml += "</td></tr>";
  strHtml += "</table>";
  
  this.oDiv.innerHTML = strHtml; 
  if (oToday = _getElementById(this.inputName+'calCell'+cur_day)){
    oToday.oldClassName = oToday.className;
  }
  var calskn = this.oDiv.style;
  if (!(xp==0 && yp==0)){
    calskn.top  = yp+20;
    calskn.left = xp+10;   
  }
  calskn.display = "block";

  this.bOpen = true;
}

Calendar.prototype.HideCalendar = function (){
  var calskn = this.oDiv.style;
  calskn.display = "none";
  if (this.oInput) this.oInput.defaultValue = this.oInput.value;
  this.bOpen = false;
}




function FocusTexte(Obj) {
  // A l'entr�e dans le champ 
  // Si la valeur �gale la valeur par d�faut => on efface
  if (Obj.value == Obj.defaultValue && !isDateValidJJMMAAAA(Obj.value) )
    Obj.value = '';
}
function BlurTexte(Obj) { 
  // A la sortie du champ
  // Si la valeur est vide => on remet la valeur par d�faut
  if (Obj.value == '')
    Obj.value = Obj.defaultValue;
}


function _getElementById(elt_id)
{
  var oElt = null;
  if ( document.getElementById && document.getElementById( elt_id ) ) {// Pour les navigateurs r�cents
     oElt = document.getElementById( elt_id );
  }
  else if ( document.all && document.all[ elt_id ] ) {// Pour les veilles versions
    oElt = document.all[ elt_id ];
  }
  else if ( document.layers && document.layers[ elt_id ] ) {// Pour les tr�s veilles versions
    oElt = document.layers[ elt_id ];
  }
  return oElt;
}