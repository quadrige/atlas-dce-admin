<?php
session_start();

// r\351cupere l'url de d\351marrage
$strUrl = "";
if( isset($_GET["url"]) == true ) {
  $strUrl = $_GET["url"];
  if( $strUrl != "" )
    $strUrl = base64_decode($strUrl);
}
if( $strUrl == "" ) {
  unset($_SESSION["sit_idUser"]);
  unset($_SESSION["sit_idUserProfil"]);
  unset($_SESSION["sit_idUserService"]);
  unset($_SESSION["sit_HomePageUser"]);
  $strUrl = "../scripts/ident/identification.php";
}

header("location: ".$strUrl);
exit();
?>
